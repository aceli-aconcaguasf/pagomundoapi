#!/bin/bash
usage() { colorEcho r "Usage: $0 -v <version number {X.Y.Z}> -e <environment {qa|sandbox|prod}> [-m <mode {nginx|standalone}>] [-h]" 1>&2; }
usageAndExit() {
  usage
  exit 1
}

colorEcho() {
  local code="\033["
  case "$1" in
  black | bk) color="${code}0;30m" ;;
  red | r) color="${code}1;31m" ;;
  green | g) color="${code}1;32m" ;;
  yellow | y) color="${code}1;33m" ;;
  blue | b) color="${code}1;34m" ;;
  purple | p) color="${code}1;35m" ;;
  cyan | c) color="${code}1;36m" ;;
  gray | gr) color="${code}0;37m" ;;
  *) local text="$1" ;;
  esac
  [ -z "$text" ] && local text="$color${*:2}${code}0m"
  echo -e "$text"
}

while getopts ":h:e:v:p:m:" o; do
  case "$o" in
  h) usageAndExit ;;
  e) ENVIRONMENT=${OPTARG,,} ;;
  v) VERSION=${OPTARG,,} ;;
  p) PROFILE=${OPTARG,,} ;;
  m) MODE=${OPTARG,,} ;;
  *) colorEcho r "" ;;
  esac
done
shift $((OPTIND - 1))

if [[ -z "${ENVIRONMENT}" ]]; then usageAndExit; fi
if [[ -z "${VERSION}" ]]; then usageAndExit; fi
if [[ -z "${MODE}" ]]; then MODE="nginx"; fi
if [[ -z "${PROFILE}" ]]; then PROFILE="prod,swagger"; fi

case "${ENVIRONMENT}" in
prod)
  DBHOST="pagomundodb.c3ldo65yww1c.us-east-1.rds.amazonaws.com"
  DBUSER="root"
  DBPASS='5Bq_D(VtEK9J5A**'
  ESURI="https://search-aoess-pagomundo-lwdjgvtyhactzouiqag34pjkoi.us-east-1.es.amazonaws.com"
  ESPORT=9200
  ESUSER="root"
  ESPASS="}g2qJ-U/#gS]p3=X"
  ;;
sandbox)
  DBHOST="sandboxpagomundodb.c3ldo65yww1c.us-east-1.rds.amazonaws.com"
  DBUSER="root"
  DBPASS='Romp920708$'
  ESURI="https://search-aoess-sandbox-pagomundo-3-iatyr2tqx3aftdt7s4zxivb7dy.us-east-1.es.amazonaws.com/"
  ESUSER="root"
  ESPASS="tcKS.+J!7'4'F;QC"
  ESPORT=9200
  ;;
qa)
  DBHOST="pagomundo-database"
  DBUSER="root"
  DBPASS='pagomundo'
  ESURI="http://elasticsearch"
  ESPORT=9200
  ;;
*) usageAndExit ;;
esac

BUCKETPATH='/home/ubuntu/fileBucket/'

DBPORT="3306"
DBNAME="pagomundo"

APPLICATION_NAME="${ENVIRONMENT}-pmi-api"
NETWORK_NAME="${ENVIRONMENT}-pmi-net"
IMAGE_NAME="${APPLICATION_NAME}"
TAG_NAME="${VERSION}.${ENVIRONMENT}"
PORT="[managed by nginx]"

if [[ ${MODE} == "standalone" ]]; then
  PORT=8080
  if [[ ${ENVIRONMENT} != "qa" && ${ENVIRONMENT} != "test" ]]; then
    TAG_NAME=${TAG_NAME}-${MODE}
    APPLICATION_NAME=${APPLICATION_NAME}-${MODE}
    IMAGE_NAME=${IMAGE_NAME}-${MODE}
  fi
fi
IMAGE_NAME="${IMAGE_NAME}:${VERSION}"

tput reset
echo
usage
echo
echo
colorEcho p "--> ENVIRONMENT:"
colorEcho r "    ${ENVIRONMENT}"
colorEcho b "    Mode = ${MODE}"

echo
colorEcho p "${ENVIRONMENT} Application:"
colorEcho b "    Name = ${APPLICATION_NAME}"
colorEcho b "    Version = ${VERSION}"
colorEcho b "    Profile = ${PROFILE}"
colorEcho b "    Git tag = ${TAG_NAME}"
colorEcho b "    Bucket path = ${BUCKETPATH}"

echo
colorEcho p "${ENVIRONMENT} Docker:"
colorEcho b "    Image = ${IMAGE_NAME}"
colorEcho b "    Network name = ${NETWORK_NAME}"
colorEcho b "    Port = ${PORT}"

echo
colorEcho p "${ENVIRONMENT} Database & ES:"
colorEcho b "    Host = ${DBHOST}:${DBPORT}"
colorEcho b "    Name = ${DBNAME}"
colorEcho b "    User = ${DBUSER}"
colorEcho b "    ES = ${ESURI}"

echo
read -r -p "--> Press enter to start deploying ${IMAGE_NAME} or CTRL+C to abort"

echo
colorEcho p "--> Removing target folder..."
rm -rf target/

colorEcho p "--> Compiling with prifile..."
colorEcho b "    ${PROFILE}"
./mvnw -P${PROFILE} -DskipTests package

colorEcho p "--> Copying WAR file to docker folder..."
cp target/*.war docker/app.war

colorEcho p "--> Building docker application..."
colorEcho b "    ${IMAGE_NAME}"
docker build -t "${IMAGE_NAME}" docker/
colorEcho g "--> Application built! <--"
echo

colorEcho p "--> Renaming docker application..."
if [ "$(docker ps -qa -f name="$APPLICATION_NAME")" ]; then
  colorEcho b "    Docker image '${APPLICATION_NAME}' exists."
  if [ "$(docker container inspect -f '{{.State.Running}}' "$APPLICATION_NAME")" == "true" ]; then
    DOCKER_STOP="docker stop ${APPLICATION_NAME}"
    ($DOCKER_STOP)
    colorEcho b "    Docker '${APPLICATION_NAME}' stopped."
  fi

  APPLICATION_RENAME=$APPLICATION_NAME-$(date +%Y.%m.%d.%H.%M)
  DOCKER_RENAME="docker rename ${APPLICATION_NAME} ${APPLICATION_RENAME}"
  ($DOCKER_RENAME)
  colorEcho b "    Docker '${APPLICATION_NAME}' renamed to ${APPLICATION_RENAME}."
else
  colorEcho b "    Docker '${APPLICATION_NAME}' doesn't exist."
fi

if [[ ${PORT} != "[managed by nginx]" ]]; then APPLICATION_PORT=" -p 8080:${PORT}"; else APPLICATION_PORT=""; fi

DOCKER_DEPLOY="docker run --add-host=desavataico.abccapital.com.mx:10.99.60.114 --add-host=desavbancaapp.abccapital.com.mx:10.99.60.181 --name ${APPLICATION_NAME} --restart unless-stopped --network ${NETWORK_NAME}${APPLICATION_PORT} -e ENVIRONMENT_NAME=${ENVIRONMENT} -e ES_URI=${ESURI} -e ES_USER=${ESUSER} -e ES_PASS=${ESPASS} -e DB_HOST=${DBHOST} -e DB_PORT=${DBPORT} -e DB_NAME=${DBNAME} -e DB_USER=${DBUSER} -e DB_PASS=${DBPASS} -v ${BUCKETPATH}:/home/jhipster/fileBucket/ -d ${IMAGE_NAME}"
#DOCKER_DEPLOY="docker run --name ${APPLICATION_NAME} --restart unless-stopped --network ${NETWORK_NAME}${APPLICATION_PORT} --add-host prod.stpmex.com:172.26.1.151 -e ENVIRONMENT_NAME=${ENVIRONMENT} -e ES_URI=${ESURI}:${ESPORT} -e DB_HOST=${DBHOST} -e DB_PORT=${DBPORT} -e DB_NAME=${DBNAME} -e DB_USER=${DBUSER} -e DB_PASS=${DBPASS} -v ${BUCKETPATH}:/home/jhipster/fileBucket/ -d ${IMAGE_NAME}"

colorEcho p "--> Deploying docker..."
($DOCKER_DEPLOY)

echo
colorEcho g "--> ${ENVIRONMENT} application deployed! <--"
echo

colorEcho p "--> Creating git tag..."
colorEcho b "    ${TAG_NAME}"
git tag "${TAG_NAME}"
git push origin --tags

echo
colorEcho g "--> Application deployed successfully! <--"
echo
