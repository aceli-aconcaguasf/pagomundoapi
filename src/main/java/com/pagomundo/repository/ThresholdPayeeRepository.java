package com.pagomundo.repository;

import com.pagomundo.domain.ThresholdPayee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the ThresholdPayee entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ThresholdPayeeRepository extends JpaRepository<ThresholdPayee, Long> {

    Optional<List<ThresholdPayee>> findAllByCountryId(Long countryId);

}
