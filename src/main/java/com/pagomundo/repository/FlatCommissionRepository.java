package com.pagomundo.repository;

import com.pagomundo.domain.FlatCommission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the FlatCommission entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FlatCommissionRepository extends JpaRepository<FlatCommission, Long> {

    List<FlatCommission> findAllByExtendedUserIdOrderByUpToAsc(Long merchantId);
}
