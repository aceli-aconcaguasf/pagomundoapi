package com.pagomundo.repository;

import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.ExtendedUserRelation;
import com.pagomundo.domain.User;
import com.pagomundo.domain.enumeration.Status;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the ExtendedUserRelation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ExtendedUserRelationRepository extends JpaRepository<ExtendedUserRelation, Long> {
    List<ExtendedUserRelation> findAllByStatusAndExtendedUser_Role(Status status, String role);

    List<ExtendedUserRelation> findAllByExtendedUser_RoleAndStatus(String role, Status status);

    List<ExtendedUserRelation> findAllByExtendedUserAndUserRelated_Role(ExtendedUser extendedUser, String role);

    List<ExtendedUserRelation> findAllByStatusAndUserRelatedAndExtendedUser_Role(Status status, User user, String role);

    List<ExtendedUserRelation> findAllByUserRelated(User user);

    List<ExtendedUserRelation> findAllByUserRelatedId(Long id);

    Optional<ExtendedUserRelation> findOneByUserRelatedAndExtendedUser(User user, ExtendedUser extendedUser);

    Optional<ExtendedUserRelation> findOneByUserRelatedIdAndExtendedUserIdAndStatus(Long userRelatedId, Long extendedUserId, Status status);

    Optional<ExtendedUserRelation> findOneByUserRelatedIdAndExtendedUser_EmailAndStatus(Long userRelatedId, String email, Status status);

    Optional<ExtendedUserRelation> findOneByStatusAndRelatedByIdAndUserRelated_RoleAndUserRelated_Email(Status status, Long relatedById, String role, String email);

    List<ExtendedUserRelation> findAllByExtendedUserIdAndNickname(Long extendedUserId, String nickname);

    List<ExtendedUserRelation> findAllByUserRelatedIdAndNicknameAndStatus(Long userRelatedId, String name, Status status);

    List<ExtendedUserRelation> findAllByUserRelatedIdAndExtendedUser_EmailAndStatus(Long userRelatedId, String email, Status status);

    List<ExtendedUserRelation> findAllByUserRelatedIdAndNicknameAndStatusAndIdNot(Long userRelatedId, String name, Status status, Long id);

    Optional<List<ExtendedUserRelation>> findAllByExtendedUserId(Long id);

    Optional<List<ExtendedUserRelation>> findAllByExtendedUserResidenceCountryId(Long id);

    Optional<List<ExtendedUserRelation>> findAllByExtendedUserPostalCountryId(Long id);

    Page<ExtendedUserRelation> findAllByUserRelatedNullOrExtendedUserNull(Pageable pageable);

}
