package com.pagomundo.repository;

import com.pagomundo.domain.ExtendedUserGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the ExtendedUserGroup entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ExtendedUserGroupRepository extends JpaRepository<ExtendedUserGroup, Long> {
    Optional<List<ExtendedUserGroup>> findAllByAdminId(Long id);

    Optional<List<ExtendedUserGroup>> findAllByBankId(Long id);
}
