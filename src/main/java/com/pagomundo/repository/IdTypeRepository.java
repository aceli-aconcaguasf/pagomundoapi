package com.pagomundo.repository;

import com.pagomundo.domain.IdType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the IdType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IdTypeRepository extends JpaRepository<IdType, Long> {
    Optional<List<IdType>> findAllByCountryId(Long id);

}
