package com.pagomundo.repository.search;

import com.pagomundo.domain.Bank;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link Bank} entity.
 */
public interface BankSearchRepository extends ElasticsearchRepository<Bank, Long> {
}
