package com.pagomundo.repository.search;

import com.pagomundo.domain.IdType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link IdType} entity.
 */
public interface IdTypeSearchRepository extends ElasticsearchRepository<IdType, Long> {
}
