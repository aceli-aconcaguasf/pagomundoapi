package com.pagomundo.repository.search;

import com.pagomundo.domain.TransactionStatusChange;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data Elasticsearch repository for the {@link TransactionStatusChange} entity.
 */
public interface TransactionStatusChangeSearchRepository extends ElasticsearchRepository<TransactionStatusChange, Long> {
    Optional<List<TransactionStatusChange>> findAllByIdIn(List<Long> ids);
}
