package com.pagomundo.repository.search;

import com.pagomundo.domain.IntegrationAuditExtraResponse;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link IntegrationAuditExtraResponse} entity.
 */
public interface IntegrationAuditExtraResponseSearchRepository extends ElasticsearchRepository<IntegrationAuditExtraResponse, Long> {
}
