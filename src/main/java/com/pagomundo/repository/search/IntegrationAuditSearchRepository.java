package com.pagomundo.repository.search;

import com.pagomundo.domain.IntegrationAudit;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link IntegrationAudit} entity.
 */
public interface IntegrationAuditSearchRepository extends ElasticsearchRepository<IntegrationAudit, Long> {
}
