package com.pagomundo.repository.search;

import com.pagomundo.domain.TransactionGroup;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link TransactionGroup} entity.
 */
public interface TransactionGroupSearchRepository extends ElasticsearchRepository<TransactionGroup, Long> {
}
