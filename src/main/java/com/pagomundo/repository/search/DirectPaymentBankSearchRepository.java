package com.pagomundo.repository.search;

import com.pagomundo.domain.DirectPaymentBank;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link DirectPaymentBank} entity.
 */
public interface DirectPaymentBankSearchRepository extends ElasticsearchRepository<DirectPaymentBank, Long> {
}
