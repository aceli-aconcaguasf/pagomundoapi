package com.pagomundo.repository.search;
import com.pagomundo.domain.FlatCommission;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link FlatCommission} entity.
 */
public interface FlatCommissionSearchRepository extends ElasticsearchRepository<FlatCommission, Long> {
}
