package com.pagomundo.repository.search;

import com.pagomundo.domain.NotificationReceiver;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link NotificationReceiver} entity.
 */
public interface NotificationReceiverSearchRepository extends ElasticsearchRepository<NotificationReceiver, Long> {
}
