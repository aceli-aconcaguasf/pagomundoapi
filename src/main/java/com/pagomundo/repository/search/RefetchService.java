package com.pagomundo.repository.search;

import com.pagomundo.repository.BankAccountRepository;
import com.pagomundo.repository.BankRepository;
import com.pagomundo.repository.CityRepository;
import com.pagomundo.repository.CountryRepository;
import com.pagomundo.repository.CurrencyRepository;
import com.pagomundo.repository.DirectPaymentBankRepository;
import com.pagomundo.repository.ExtendedUserFundsRepository;
import com.pagomundo.repository.ExtendedUserGroupRepository;
import com.pagomundo.repository.ExtendedUserRelationRepository;
import com.pagomundo.repository.ExtendedUserRepository;
import com.pagomundo.repository.ExtendedUserStatusChangeRepository;
import com.pagomundo.repository.FlatCommissionRepository;
import com.pagomundo.repository.IdTypeRepository;
import com.pagomundo.repository.NotificationReceiverRepository;
import com.pagomundo.repository.NotificationRepository;
import com.pagomundo.repository.NotificationStatusChangeRepository;
import com.pagomundo.repository.TransactionGroupRepository;
import com.pagomundo.repository.TransactionRepository;
import com.pagomundo.repository.TransactionStatusChangeRepository;
import com.pagomundo.repository.UserRepository;
import com.pagomundo.repository.ThresholdPayeeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RefetchService {
    private final Logger log = LoggerFactory.getLogger(RefetchService.class);

    private final ExtendedUserRepository extendedUserRepository;
    private final ExtendedUserSearchRepository extendedUserSearchRepository;
    private final ExtendedUserFundsRepository extendedUserFundsRepository;
    private final ExtendedUserFundsSearchRepository extendedUserFundsSearchRepository;
    private final ExtendedUserGroupRepository extendedUserGroupRepository;
    private final ExtendedUserGroupSearchRepository extendedUserGroupSearchRepository;
    private final ExtendedUserRelationRepository extendedUserRelationRepository;
    private final ExtendedUserRelationSearchRepository extendedUserRelationSearchRepository;
    private final ExtendedUserStatusChangeRepository extendedUserStatusChangeRepository;
    private final ExtendedUserStatusChangeSearchRepository extendedUserStatusChangeSearchRepository;
    private final TransactionRepository transactionRepository;
    private final TransactionSearchRepository transactionSearchRepository;
    private final TransactionGroupRepository transactionGroupRepository;
    private final TransactionGroupSearchRepository transactionGroupSearchRepository;
    private final TransactionStatusChangeRepository transactionStatusChangeRepository;
    private final TransactionStatusChangeSearchRepository transactionStatusChangeSearchRepository;
    private final NotificationRepository notificationRepository;
    private final NotificationSearchRepository notificationSearchRepository;
    private final NotificationStatusChangeRepository notificationStatusChangeRepository;
    private final NotificationStatusChangeSearchRepository notificationStatusChangeSearchRepository;
    private final NotificationReceiverRepository notificationReceiverRepository;
    private final NotificationReceiverSearchRepository notificationReceiverSearchRepository;
    private final BankAccountRepository bankAccountRepository;
    private final BankAccountSearchRepository bankAccountSearchRepository;
    private final BankRepository bankRepository;
    private final BankSearchRepository bankSearchRepository;
    private final DirectPaymentBankRepository directPaymentBankRepository;
    private final DirectPaymentBankSearchRepository directPaymentBankSearchRepository;
    private final CityRepository cityRepository;
    private final CitySearchRepository citySearchRepository;
    private final CountryRepository countryRepository;
    private final CountrySearchRepository countrySearchRepository;
    private final CurrencyRepository currencyRepository;
    private final CurrencySearchRepository currencySearchRepository;
    private final IdTypeRepository idTypeRepository;
    private final IdTypeSearchRepository idTypeSearchRepository;
    private final FlatCommissionRepository flatCommissionRepository;
    private final FlatCommissionSearchRepository flatCommissionSearchRepository;
    private final UserRepository userRepository;
    private final UserSearchRepository userSearchRepository;
    private final ThresholdPayeeRepository thresholdPayeeRepository;
    private final ThresholdPayeeSearchRepository thresholdPayeeSearchRepository;


    public RefetchService(
        ExtendedUserRepository extendedUserRepository, ExtendedUserSearchRepository extendedUserSearchRepository,
        ExtendedUserFundsRepository extendedUserFundsRepository, ExtendedUserFundsSearchRepository extendedUserFundsSearchRepository,
        ExtendedUserGroupRepository extendedUserGroupRepository, ExtendedUserGroupSearchRepository extendedUserGroupSearchRepository,
        ExtendedUserRelationRepository extendedUserRelationRepository, ExtendedUserRelationSearchRepository extendedUserRelationSearchRepository,
        ExtendedUserStatusChangeRepository extendedUserStatusChangeRepository, ExtendedUserStatusChangeSearchRepository extendedUserStatusChangeSearchRepository,
        TransactionRepository transactionRepository, TransactionSearchRepository transactionSearchRepository,
        TransactionGroupRepository transactionGroupRepository, TransactionGroupSearchRepository transactionGroupSearchRepository,
        TransactionStatusChangeRepository transactionStatusChangeRepository, TransactionStatusChangeSearchRepository transactionStatusChangeSearchRepository,
        NotificationRepository notificationRepository, NotificationSearchRepository notificationSearchRepository,
        NotificationStatusChangeRepository notificationStatusChangeRepository, NotificationStatusChangeSearchRepository notificationStatusChangeSearchRepository,
        NotificationReceiverRepository notificationReceiverRepository, NotificationReceiverSearchRepository notificationReceiverSearchRepository,
        BankAccountRepository bankAccountRepository, BankAccountSearchRepository bankAccountSearchRepository,
        BankRepository bankRepository, BankSearchRepository bankSearchRepository,
        DirectPaymentBankRepository directPaymentBankRepository, DirectPaymentBankSearchRepository directPaymentBankSearchRepository,
        CityRepository cityRepository, CitySearchRepository citySearchRepository,
        CountryRepository countryRepository, CountrySearchRepository countrySearchRepository,
        CurrencyRepository currencyRepository, CurrencySearchRepository currencySearchRepository,
        IdTypeRepository idTypeRepository, IdTypeSearchRepository idTypeSearchRepository,
        FlatCommissionRepository flatCommissionRepository, FlatCommissionSearchRepository flatCommissionSearchRepository,
        UserRepository userRepository, UserSearchRepository userSearchRepository,
        ThresholdPayeeRepository thresholdPayeeRepository,
        ThresholdPayeeSearchRepository thresholdPayeeSearchRepository) {
        this.extendedUserRepository = extendedUserRepository;
        this.extendedUserSearchRepository = extendedUserSearchRepository;
        this.extendedUserFundsRepository = extendedUserFundsRepository;
        this.extendedUserFundsSearchRepository = extendedUserFundsSearchRepository;
        this.extendedUserGroupRepository = extendedUserGroupRepository;
        this.extendedUserGroupSearchRepository = extendedUserGroupSearchRepository;
        this.extendedUserRelationRepository = extendedUserRelationRepository;
        this.extendedUserRelationSearchRepository = extendedUserRelationSearchRepository;
        this.extendedUserStatusChangeRepository = extendedUserStatusChangeRepository;
        this.extendedUserStatusChangeSearchRepository = extendedUserStatusChangeSearchRepository;
        this.transactionRepository = transactionRepository;
        this.bankAccountRepository = bankAccountRepository;
        this.transactionSearchRepository = transactionSearchRepository;
        this.transactionGroupRepository = transactionGroupRepository;
        this.transactionGroupSearchRepository = transactionGroupSearchRepository;
        this.transactionStatusChangeRepository = transactionStatusChangeRepository;
        this.transactionStatusChangeSearchRepository = transactionStatusChangeSearchRepository;
        this.notificationRepository = notificationRepository;
        this.notificationSearchRepository = notificationSearchRepository;
        this.notificationStatusChangeRepository = notificationStatusChangeRepository;
        this.notificationStatusChangeSearchRepository = notificationStatusChangeSearchRepository;
        this.notificationReceiverRepository = notificationReceiverRepository;
        this.notificationReceiverSearchRepository = notificationReceiverSearchRepository;
        this.bankAccountSearchRepository = bankAccountSearchRepository;
        this.bankRepository = bankRepository;
        this.bankSearchRepository = bankSearchRepository;
        this.directPaymentBankRepository = directPaymentBankRepository;
        this.directPaymentBankSearchRepository = directPaymentBankSearchRepository;
        this.cityRepository = cityRepository;
        this.citySearchRepository = citySearchRepository;
        this.countryRepository = countryRepository;
        this.countrySearchRepository = countrySearchRepository;
        this.currencyRepository = currencyRepository;
        this.currencySearchRepository = currencySearchRepository;
        this.idTypeRepository = idTypeRepository;
        this.idTypeSearchRepository = idTypeSearchRepository;
        this.flatCommissionRepository = flatCommissionRepository;
        this.flatCommissionSearchRepository = flatCommissionSearchRepository;
        this.userRepository = userRepository;
        this.userSearchRepository = userSearchRepository;
        this.thresholdPayeeRepository = thresholdPayeeRepository;
        this.thresholdPayeeSearchRepository = thresholdPayeeSearchRepository;
    }

    public void refetchAll() {
        bankSearchRepository.saveAll(bankRepository.findAll(Pageable.unpaged()).getContent());
        log.info("ES -> Bank refetch done.");
        bankAccountSearchRepository.saveAll(bankAccountRepository.findAll(Pageable.unpaged()).getContent());
        log.info("ES -> BankAccount refetch done.");
        citySearchRepository.saveAll(cityRepository.findAll(Pageable.unpaged()).getContent());
        log.info("ES -> City refetch done.");
        countrySearchRepository.saveAll(countryRepository.findAll(Pageable.unpaged()).getContent());
        log.info("ES -> Country refetch done.");
        currencySearchRepository.saveAll(currencyRepository.findAll(Pageable.unpaged()).getContent());
        log.info("ES -> Currency refetch done.");
        directPaymentBankSearchRepository.saveAll(directPaymentBankRepository.findAll(Pageable.unpaged()).getContent());
        log.info("ES -> DirectPaymentBank refetch done.");
        extendedUserSearchRepository.saveAll(extendedUserRepository.findAll(Pageable.unpaged()).getContent());
        log.info("ES -> ExtendedUser refetch done.");
        idTypeSearchRepository.saveAll(idTypeRepository.findAll(Pageable.unpaged()).getContent());
        log.info("ES -> IdType refetch done.");
        extendedUserFundsSearchRepository.saveAll(extendedUserFundsRepository.findAll(Pageable.unpaged()).getContent());
        log.info("ES -> ExtendedUserFunds refetch done.");
        extendedUserGroupSearchRepository.saveAll(extendedUserGroupRepository.findAll(Pageable.unpaged()).getContent());
        log.info("ES -> ExtendedUserGroup refetch done.");
        extendedUserRelationSearchRepository.saveAll(extendedUserRelationRepository.findAll(Pageable.unpaged()).getContent());
        log.info("ES -> ExtendedUserRelation refetch done.");
        extendedUserStatusChangeSearchRepository.saveAll(extendedUserStatusChangeRepository.findAll(Pageable.unpaged()).getContent());
        log.info("ES -> ExtendedUserStatusChange refetch done.");
        transactionSearchRepository.saveAll(transactionRepository.findAll(Pageable.unpaged()).getContent());
        log.info("ES -> Transaction refetch done.");
        transactionGroupSearchRepository.saveAll(transactionGroupRepository.findAll(Pageable.unpaged()).getContent());
        log.info("ES -> TransactionGroup refetch done.");
        transactionStatusChangeSearchRepository.saveAll(transactionStatusChangeRepository.findAll(Pageable.unpaged()).getContent());
        log.info("ES -> TransactionStatusChange refetch done.");
        thresholdPayeeSearchRepository.saveAll(thresholdPayeeRepository.findAll(Pageable.unpaged()).getContent());
        log.info("ES -> ThresholdPayee refetch done.");
        flatCommissionSearchRepository.saveAll(flatCommissionRepository.findAll(Pageable.unpaged()).getContent());
        log.info("ES -> FlatCommission refetch done.");
        notificationSearchRepository.saveAll(notificationRepository.findAll(Pageable.unpaged()).getContent());
        log.info("ES -> Notification refetch done.");
        notificationReceiverSearchRepository.saveAll(notificationReceiverRepository.findAll(Pageable.unpaged()).getContent());
        log.info("ES -> NotificationReceiver refetch done.");
        notificationStatusChangeSearchRepository.saveAll(notificationStatusChangeRepository.findAll(Pageable.unpaged()).getContent());
        log.info("ES -> NotificationStatusChange refetch done.");
        
    }


    public void refetchExtendedUser(Long id) {
        extendedUserRepository.findById(id).ifPresent(extendedUserSearchRepository::save);

        extendedUserRepository.findAllByResellerId(id).ifPresent(extendedUserSearchRepository::saveAll);
        extendedUserFundsRepository.findAllByExtendedUserId(id).ifPresent(extendedUserFundsSearchRepository::saveAll);
        //extendedUserFundsRepository.findTop3ByOrderByExtendedUserIdDesc(id).ifPresent(extendedUserFundsSearchRepository::saveAll);
        extendedUserGroupRepository.findAllByAdminId(id).ifPresent(extendedUserGroupSearchRepository::saveAll);
        extendedUserRelationRepository.findAllByExtendedUserId(id).ifPresent(extendedUserRelationSearchRepository::saveAll);
        extendedUserStatusChangeRepository.findAllByExtendedUserId(id).ifPresent(extendedUserStatusChangeSearchRepository::saveAll);

        transactionRepository.findAllByMerchantId(id).ifPresent(transactionSearchRepository::saveAll);
        transactionRepository.findAllByResellerId(id).ifPresent(transactionSearchRepository::saveAll);
        transactionRepository.findAllByPayeeId(id).ifPresent(transactionSearchRepository::saveAll);
        transactionRepository.findAllByAdminId(id).ifPresent(transactionSearchRepository::saveAll);

        transactionGroupRepository.findAllByAdminId(id).ifPresent(transactionGroupSearchRepository::saveAll);

        transactionStatusChangeRepository.findAllByUserId(id).ifPresent(transactionStatusChangeSearchRepository::saveAll);

        notificationRepository.findAllBySenderId(id).ifPresent(notificationSearchRepository::saveAll);
        notificationRepository.findAllByExtendedUserId(id).ifPresent(notificationSearchRepository::saveAll);
        notificationRepository.findAllByResponsibleId(id).ifPresent(notificationSearchRepository::saveAll);

        notificationStatusChangeRepository.findAllByUserId(id).ifPresent(notificationStatusChangeSearchRepository::saveAll);

        notificationReceiverRepository.findAllBySenderId(id).ifPresent(notificationReceiverSearchRepository::saveAll);
        notificationReceiverRepository.findAllByReceiverId(id).ifPresent(notificationReceiverSearchRepository::saveAll);

        extendedUserRepository.findById(id).ifPresent(extendedUser -> refetchUser(extendedUser.getUser().getId()));
    }

    public void refetchTransaction(Long id) {
        transactionRepository.findById(id).ifPresent(transactionSearchRepository::save);
        transactionStatusChangeRepository.findAllByTransactionId(id).ifPresent(transactionStatusChangeSearchRepository::saveAll);
    }

    public void refetchBankAccount(Long id) {
        bankAccountRepository.findById(id).ifPresent(bankAccountSearchRepository::save);
        transactionRepository.findAllByBankAccountId(id).ifPresent(transactionSearchRepository::saveAll);
        transactionGroupRepository.findAllByBankAccountId(id).ifPresent(transactionGroupSearchRepository::saveAll);
    }

    public void refetchBank(Long id) {
        bankRepository.findById(id).ifPresent(bankSearchRepository::save);
        bankAccountRepository.findAllByBankId(id).ifPresent(bankAccountSearchRepository::saveAll);
        directPaymentBankRepository.findAllByDestinyBankId(id).ifPresent(directPaymentBankSearchRepository::saveAll);
        directPaymentBankRepository.findAllByOriginBankId(id).ifPresent(directPaymentBankSearchRepository::saveAll);
        extendedUserRepository.findAllByBankId(id).ifPresent(extendedUserSearchRepository::saveAll);
        extendedUserRepository.findAllByDirectPaymentBankId(id).ifPresent(extendedUserSearchRepository::saveAll);
        extendedUserGroupRepository.findAllByBankId(id).ifPresent(extendedUserGroupSearchRepository::saveAll);
    }

    public void refetchCity(Long id) {
        cityRepository.findById(id).ifPresent(citySearchRepository::save);
        directPaymentBankRepository.findAllByDestinyCityId(id).ifPresent(directPaymentBankSearchRepository::saveAll);
        extendedUserRepository.findAllByPostalCityId(id).ifPresent(extendedUserSearchRepository::saveAll);
        extendedUserRepository.findAllByResidenceCityId(id).ifPresent(extendedUserSearchRepository::saveAll);
        extendedUserRepository.findAllByDirectPaymentCityId(id).ifPresent(extendedUserSearchRepository::saveAll);
    }

    public void refetchCountry(Long id) {
        countryRepository.findById(id).ifPresent(countrySearchRepository::save);
        bankRepository.findAllByCountryId(id).ifPresent(bankSearchRepository::saveAll);
        cityRepository.findAllByCountryId(id).ifPresent(citySearchRepository::saveAll);
        currencyRepository.findAllByCountryId(id).ifPresent(currencySearchRepository::saveAll);
        idTypeRepository.findAllByCountryId(id).ifPresent(idTypeSearchRepository::saveAll);
        extendedUserRepository.findAllByPostalCountryId(id).ifPresent(extendedUserSearchRepository::saveAll);
        extendedUserRepository.findAllByResidenceCountryId(id).ifPresent(extendedUserSearchRepository::saveAll);
        extendedUserRelationRepository.findAllByExtendedUserResidenceCountryId(id).ifPresent(extendedUserRelationSearchRepository::saveAll);
        extendedUserRelationRepository.findAllByExtendedUserPostalCountryId(id).ifPresent(extendedUserRelationSearchRepository::saveAll);
    }

    public void refetchCurrency(Long id) {
        currencyRepository.findById(id).ifPresent(currencySearchRepository::save);
        transactionRepository.findAllByCurrencyId(id).ifPresent(transactionSearchRepository::saveAll);
    }

    public void refetchIdTye(Long id) {
        idTypeRepository.findById(id).ifPresent(idTypeSearchRepository::save);
        directPaymentBankRepository.findAllByIdTypeId(id).ifPresent(directPaymentBankSearchRepository::saveAll);
        extendedUserRepository.findAllByIdTypeId(id).ifPresent(extendedUserRepository::saveAll);
    }

    public void refetchThresholdPayee(Long id) {
        thresholdPayeeRepository.findById(id).ifPresent(thresholdPayeeSearchRepository::save);
    }

    public void refetchFlatCommission(Long id) {
        flatCommissionRepository.findById(id).ifPresent(flatCommissionSearchRepository::save);
    }

    public void refetchNotificationService(Long id) {
        notificationRepository.findById(id).ifPresent(notificationSearchRepository::save);
        notificationStatusChangeRepository.findAllByNotificationId(id).ifPresent(notificationStatusChangeSearchRepository::saveAll);
        notificationReceiverRepository.findAllByNotificationId(id).ifPresent(notificationReceiverSearchRepository::saveAll);
    }

    public void refetchNotificationReceiverService(Long id) {
        notificationReceiverRepository.findById(id).ifPresent(notificationReceiverSearchRepository::save);
    }

    public void refetchUser(Long id) {
        userRepository.findById(id).ifPresent(userSearchRepository::save);
        extendedUserRelationRepository.findAllByUserRelatedId(id).forEach(extendedUserRelationSearchRepository::save);
    }

    public void refetchExtendedUserFunds(Long id) {
        extendedUserFundsRepository.findById(id).ifPresent(extendedUserFundsSearchRepository::save);
    }

}
