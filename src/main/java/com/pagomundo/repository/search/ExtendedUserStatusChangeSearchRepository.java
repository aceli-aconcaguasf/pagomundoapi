package com.pagomundo.repository.search;

import com.pagomundo.domain.ExtendedUserStatusChange;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data Elasticsearch repository for the {@link ExtendedUserStatusChange} entity.
 */
public interface ExtendedUserStatusChangeSearchRepository extends ElasticsearchRepository<ExtendedUserStatusChange, Long> {
    Optional<List<ExtendedUserStatusChange>> findAllByIdIn(List<Long> ids);
}
