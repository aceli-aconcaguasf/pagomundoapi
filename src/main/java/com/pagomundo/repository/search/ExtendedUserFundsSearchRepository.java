package com.pagomundo.repository.search;

import com.pagomundo.domain.ExtendedUserFunds;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link ExtendedUserFunds} entity.
 */
public interface ExtendedUserFundsSearchRepository extends ElasticsearchRepository<ExtendedUserFunds, Long> {
}
