package com.pagomundo.repository.search;

import com.pagomundo.domain.ExtendedUserGroup;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link ExtendedUserGroup} entity.
 */
public interface ExtendedUserGroupSearchRepository extends ElasticsearchRepository<ExtendedUserGroup, Long> {
}
