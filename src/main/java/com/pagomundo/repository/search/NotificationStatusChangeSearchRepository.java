package com.pagomundo.repository.search;

import com.pagomundo.domain.NotificationStatusChange;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data Elasticsearch repository for the {@link NotificationStatusChange} entity.
 */
public interface NotificationStatusChangeSearchRepository extends ElasticsearchRepository<NotificationStatusChange, Long> {
    Optional<List<NotificationStatusChange>> findAllByIdIn(List<Long> ids);
}
