package com.pagomundo.repository.search;

import com.pagomundo.domain.ThresholdPayee;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link ThresholdPayee} entity.
 */
public interface ThresholdPayeeSearchRepository extends ElasticsearchRepository<ThresholdPayee, Long> {
}
