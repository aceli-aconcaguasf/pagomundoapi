package com.pagomundo.repository.search;

import com.pagomundo.domain.ExtendedUserRelation;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link ExtendedUserRelation} entity.
 */
public interface ExtendedUserRelationSearchRepository extends ElasticsearchRepository<ExtendedUserRelation, Long> {
}
