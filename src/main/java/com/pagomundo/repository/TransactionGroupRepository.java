package com.pagomundo.repository;

import com.pagomundo.domain.TransactionGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the TransactionGroup entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TransactionGroupRepository extends JpaRepository<TransactionGroup, Long> {
    Optional<List<TransactionGroup>> findAllByAdminId(Long id);

    Optional<List<TransactionGroup>> findAllByBankAccountId(Long id);

}
