package com.pagomundo.repository;

import com.pagomundo.domain.NotificationReceiver;
import com.pagomundo.domain.enumeration.Status;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the NotificationReceiver entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NotificationReceiverRepository extends JpaRepository<NotificationReceiver, Long> {
    Page<NotificationReceiver> findAllByMustSendEmailTrueAndEmailSentFalseAndRetriesIsLessThan(Integer retries, Pageable pageable);

    Page<NotificationReceiver> findAllByStatusIsAndLastUpdatedIsLessThanAndSenderIsNotNull(Status status, ZonedDateTime lastUpdated, Pageable pageable);

    Optional<List<NotificationReceiver>> findAllBySenderId(Long id);

    Optional<List<NotificationReceiver>> findAllByReceiverId(Long id);

    Optional<List<NotificationReceiver>> findAllByNotificationId(Long id);
}
