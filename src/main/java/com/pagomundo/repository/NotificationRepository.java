package com.pagomundo.repository;

import com.pagomundo.domain.Notification;
import com.pagomundo.domain.enumeration.NotificationCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Notification entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NotificationRepository extends JpaRepository<Notification, Long> {

    Optional<List<Notification>> findAllByIdIn(List<Long> ids);

    Optional<Notification> findOneByIdAndSenderId(Long id, Long senderId);

    Optional<List<Notification>> findAllBySenderId(Long id);

    Optional<List<Notification>> findAllByExtendedUserId(Long id);

    Optional<List<Notification>> findAllByResponsibleId(Long id);

    Optional<List<Notification>> findAllByCategoryAndDateCreatedLessThan(NotificationCategory category, ZonedDateTime dateToErase);

}
