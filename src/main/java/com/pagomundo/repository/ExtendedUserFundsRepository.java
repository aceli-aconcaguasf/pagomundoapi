package com.pagomundo.repository;

import com.pagomundo.domain.ExtendedUserFunds;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the ExtendedUserFunds entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ExtendedUserFundsRepository extends JpaRepository<ExtendedUserFunds, Long> {

    @Query("select extendedUserFunds from ExtendedUserFunds extendedUserFunds where extendedUserFunds.user.login = ?#{principal.username}")
    List<ExtendedUserFunds> findByUserIsCurrentUser();

    Optional<List<ExtendedUserFunds>> findAllByExtendedUserId(Long id);

    //Optional<List<ExtendedUserFunds>> findTop3ByOrderByExtendedUserIdDesc(Long id);

}
