package com.pagomundo.repository;

import com.pagomundo.domain.IntegrationAudit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the IntegrationAudit entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IntegrationAuditRepository extends JpaRepository<IntegrationAudit, Long> {

    Optional<IntegrationAudit> findOneByProviderAndProviderId(String provider, String id);


    @Query(nativeQuery = true, value = "" +
        "SELECT ia.* " +
        "FROM  integration_audit ia INNER JOIN transaction entity ON ia.entity_id = entity.id " +
        "WHERE ia.request_date < date_sub(:now, interval :minutes minute) " +
        "  and entity.status = 'IN_PROCESS' " +
        "  and ia.status = 'IN_PROCESS' " +
        "  and provider = :provider " +
        "order by ia.id " +
        "limit :maxRows")
    Optional<List<IntegrationAudit>> findAllNoResponseByProvider(
        @Param("provider") String provider,
        @Param("minutes") Integer minutes,
        @Param("maxRows") Integer maxRows,
        @Param("now") ZonedDateTime now
    );
}
