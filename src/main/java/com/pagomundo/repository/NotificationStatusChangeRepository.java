package com.pagomundo.repository;

import com.pagomundo.domain.NotificationStatusChange;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the NotificationStatusChange entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NotificationStatusChangeRepository extends JpaRepository<NotificationStatusChange, Long> {

    Optional<List<NotificationStatusChange>> findAllByUserId(Long id);

    Optional<List<NotificationStatusChange>> findAllByNotificationId(Long id);

}
