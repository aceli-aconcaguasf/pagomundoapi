package com.pagomundo.repository;

import com.pagomundo.domain.IntegrationAuditExtraResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the IntegrationAuditExtraResponse entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IntegrationAuditExtraResponseRepository extends JpaRepository<IntegrationAuditExtraResponse, Long> {

    @Query(nativeQuery = true, value = "" +
        "SELECT iae.* " +
        "FROM integration_audit_extra iae " +
        "         INNER JOIN integration_audit ia ON iae.integration_audit_id = ia.id " +
        "         INNER JOIN transaction entity ON ia.entity_id = entity.id " +
        "WHERE iae.id = (SELECT MAX(id) FROM integration_audit_extra WHERE iae.integration_audit_id = integration_audit_id) " +
        "  and iae.message_date < date_sub(:now, interval :minutes minute) " +
        "  and entity.status = 'IN_PROCESS' " +
        "order by iae.integration_audit_id " +
        "limit :maxRows")
    Optional<List<IntegrationAuditExtraResponse>> findAllStpByQuery(
        @Param("minutes") Integer minutes,
        @Param("maxRows") Integer maxRows,
        @Param("now") ZonedDateTime now
    );

}
