package com.pagomundo.repository;

import com.pagomundo.domain.City;
import org.springframework.data.jpa.repository.JpaRepository;
// import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
// import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the City entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CityRepository extends JpaRepository<City, Long> {
    Optional<List<City>> findAllByCountryId(Long id);
    
    // @Query("from city c where c.country_id = :id and c.name like '%:substr%'")
    // Optional<List<City>> findAll(@Param("id") Long id, @Param("substr") String substr);

    Optional<List<City>> findByNameContainingAndCountryId(String infix, Long id);
    
    Optional<List<City>> findAllByCountryIdOrderByNameAsc(Long id);
}
