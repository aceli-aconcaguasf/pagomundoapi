package com.pagomundo.repository;

import com.pagomundo.domain.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the Transaction entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    Optional<List<Transaction>> findAllByIdIn(List<Long> ids);

    Optional<List<Transaction>> findAllByMerchantId(Long id);

    Optional<Transaction> findOneByIdAndMerchantId(Long id, Long merchantId);

    Optional<Transaction> findOneByIdAndResellerId(Long id, Long merchantId);

    Optional<List<Transaction>> findAllByResellerId(Long id);

    Optional<List<Transaction>> findAllByPayeeId(Long id);

    Optional<List<Transaction>> findAllByAdminId(Long id);

    Optional<List<Transaction>> findAllByBankAccountId(Long id);

    Optional<List<Transaction>> findAllByCurrencyId(Long id);

    Optional<List<Transaction>> findByDescription(String description);

}
