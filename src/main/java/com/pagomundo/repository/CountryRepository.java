package com.pagomundo.repository;

import com.pagomundo.domain.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Country entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {
    Optional<Country> findOneByCurrencyCode(String currencyCode);

    Optional<Country> findOneByIdAndForPayeeTrue(Long id);

    List<Country> findAllByForPayeeIsTrue();
}
