package com.pagomundo.repository;

import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.User;
import com.pagomundo.domain.enumeration.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the ExtendedUser entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ExtendedUserRepository extends JpaRepository<ExtendedUser, Long> {
    List<ExtendedUser> findAllByResellerIdAndStatusNot(Long id, Status status);

    Optional<List<ExtendedUser>> findAllByResellerId(Long id);

    Optional<ExtendedUser> findOneByEmailIgnoreCase(String email);

    Optional<ExtendedUser> findOneByIdAndResellerId(Long id, Long resellerId);

    Optional<ExtendedUser> findOneByEmailAndResellerId(String email, Long resellerId);

    List<ExtendedUser> findAllByRoleAndStatus(String role, Status status);

    ExtendedUser getByUser(User user);

    ExtendedUser getByUserId(Long id);

    Optional<List<ExtendedUser>> findAllByIdIn(List<Long> ids);

    List<ExtendedUser> findAllByRole(String role);

    List<ExtendedUser> findAllByRoleAndStatusIsNot(String role, Status status);

    Optional<List<ExtendedUser>> findAllByBankId(Long id);

    Optional<List<ExtendedUser>> findAllByDirectPaymentBankId(Long id);

    Optional<List<ExtendedUser>> findAllByPostalCityId(Long id);

    Optional<List<ExtendedUser>> findAllByResidenceCityId(Long id);

    Optional<List<ExtendedUser>> findAllByDirectPaymentCityId(Long id);

    Optional<List<ExtendedUser>> findAllByPostalCountryId(Long id);

    Optional<List<ExtendedUser>> findAllByResidenceCountryId(Long id);

    Optional<List<ExtendedUser>> findAllByIdTypeId(Long id);

    Optional<ExtendedUser> findOneByUser(User user);

    Optional<ExtendedUser> findOneByUserId(Long userId);
    
    List<ExtendedUser> findAllByBankAccountNumber(String bankAccountNumber);

}
