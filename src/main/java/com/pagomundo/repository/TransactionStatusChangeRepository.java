package com.pagomundo.repository;

import com.pagomundo.domain.TransactionStatusChange;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the TransactionStatusChange entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TransactionStatusChangeRepository extends JpaRepository<TransactionStatusChange, Long> {
    Optional<List<TransactionStatusChange>> findAllByUserId(Long id);

    Optional<List<TransactionStatusChange>> findAllByTransactionId(Long id);

}
