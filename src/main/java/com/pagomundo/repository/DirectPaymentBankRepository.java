package com.pagomundo.repository;

import com.pagomundo.domain.DirectPaymentBank;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the DirectPaymentBank entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DirectPaymentBankRepository extends JpaRepository<DirectPaymentBank, Long> {
    Optional<List<DirectPaymentBank>> findAllByDestinyBankId(Long id);

    Optional<List<DirectPaymentBank>> findAllByIdTypeId(Long id);

    Optional<List<DirectPaymentBank>> findAllByOriginBankId(Long id);

    Optional<List<DirectPaymentBank>> findAllByDestinyCityId(Long id);
}
