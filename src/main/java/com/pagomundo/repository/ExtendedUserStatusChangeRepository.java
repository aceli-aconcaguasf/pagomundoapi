package com.pagomundo.repository;

import com.pagomundo.domain.ExtendedUserStatusChange;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the ExtendedUserStatusChange entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ExtendedUserStatusChangeRepository extends JpaRepository<ExtendedUserStatusChange, Long> {
    Optional<List<ExtendedUserStatusChange>> findAllByExtendedUserId(Long id);

}
