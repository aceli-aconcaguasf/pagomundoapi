package com.pagomundo.service;

import com.pagomundo.domain.DTO.NotificationListDTO;
import com.pagomundo.domain.Notification;
import com.pagomundo.domain.enumeration.NotificationCategory;
import com.pagomundo.domain.enumeration.NotificationSubCategory;
import com.pagomundo.domain.enumeration.ReceiverGroup;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.repository.NotificationRepository;
import com.pagomundo.repository.search.NotificationSearchRepository;
import com.pagomundo.repository.search.RefetchService;
import com.pagomundo.web.rest.errors.BadRequestAlertException;
import com.pagomundo.web.rest.errors.NotFoundAlertException;
import com.pagomundo.web.rest.errors.UserNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link Notification}.
 */
@Service
@Transactional
public class NotificationService {
    public static final String ENTITY_NAME = "notification";
    public static final String NOT_FOUND = ENTITY_NAME + ".001";

    private final Logger log = LoggerFactory.getLogger(NotificationService.class);
    private final NotificationRepository notificationRepository;

    private final ExtendedUserService extendedUserService;
    private final NotificationStatusChangeService notificationStatusChangeService;
    private final NotificationSearchRepository notificationSearchRepository;
    private final RefetchService refetchService;
    private final TranslateService translateService;

    public NotificationService(NotificationRepository notificationRepository, ExtendedUserService extendedUserService, NotificationStatusChangeService notificationStatusChangeService, NotificationSearchRepository notificationSearchRepository, RefetchService refetchService, TranslateService translateService) {
        this.notificationRepository = notificationRepository;
        this.extendedUserService = extendedUserService;
        this.notificationStatusChangeService = notificationStatusChangeService;
        this.notificationSearchRepository = notificationSearchRepository;
        this.refetchService = refetchService;
        this.translateService = translateService;
    }

    /**
     * Save a notification.
     *
     * @param n the entity to save.
     * @return the persisted entity.
     */
    public Notification save(Notification n, boolean mustChangeStatus) {
        n.setLastUpdated(ZonedDateTime.now());
        Notification result = notificationRepository.save(n);
        notificationSearchRepository.save(result);
        log.debug("Saved Notification id: {}", result.getId());
        if (mustChangeStatus) notificationStatusChangeService.createStatusChange(result);

        return result;
    }

    /**
     * Save a notification.
     *
     * @param n the entity to save.
     * @return the persisted entity.
     */
    public Notification save(Notification n) {
        return save(n, true);
    }

    /**
     * Create a notification.
     *
     * @param n the entity to create.
     * @return the persisted entity.
     */
    public Notification create(Notification n) {
        
        log.info(">>>>>>>>>>>>>>>>>>>> Request to create one Notification : {}", n);

        n.setCategory(NotificationSubCategory.getNotificationCategory(n.getSubCategory()));

        if (n.getCategory().equals(NotificationCategory.ISSUE) || n.getCategory().equals(NotificationCategory.MESSAGE)) {
            if (n.getSubject() == null || n.getSubject().equals(""))
                throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.subject.missing"), ENTITY_NAME, "MissingSubject");
            if (n.getDescription() == null || n.getDescription().equals(""))
                throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.description.missing"), ENTITY_NAME, "MissingDescription");
        }
        try {
            n.setSender(n.getSender() != null ? n.getSender() : extendedUserService.getCurrentExtendedUser());
        } catch (UserNotFoundException unfe) {
            n.setSender(null);
        }

        if (n.getExtendedUser() != null)
            n.setExtendedUser(extendedUserService.findOne(n.getExtendedUser().getId()).orElse(null));

        if (n.getReceiverGroup() == null) n.setReceiverGroup(ReceiverGroup.getReceiverGroup(n.getCategory()));

        n.setStatus(Status.CREATED);
        n.setStatusReason("");
        n.setNotes("");
        n.setDateCreated(ZonedDateTime.now());

        return save(n);
    }

    /**
     * Update a notification.
     *
     * @param n the entity to save.
     * @return the persisted entity.
     */
    public Notification update(Notification n) {
        log.debug("Request to update one Notification : {}", n);

        Notification cn = findOne(n.getId()).orElseThrow(() ->
            new NotFoundAlertException(NotificationService.ENTITY_NAME + " id: " + n.getId(), ENTITY_NAME, NotificationService.NOT_FOUND));
        log.debug("Current Notification : {}", cn);

        if (n.getStatus() == null) {
            cn.setNotes(n.getNotes());
            return save(cn, false);
        }

        if (!validateStatusChange(n.getStatus(), cn.getStatus())) {
            throw new BadRequestAlertException(
                translateService.getMessageByCurrentUserParamsNull("error.message.exception.invalid.status") + cn.getStatus().toString() + " " + translateService.getMessageByCurrentUserParamsNull("error.message.exception.a") + " " + n.getStatus().toString()
                , ENTITY_NAME, "statusChange");
        }

        if (n.getStatus() == Status.REJECTED && (n.getStatusReason() == null || n.getStatusReason().equals(""))) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.missing.status.reason"), ENTITY_NAME, "MissingStatusReason");
        }

        if (n.getStatus() == Status.IN_PROCESS) {  // Take action
            cn.setResponsible(extendedUserService.getCurrentExtendedUser());
        }

        if (n.getStatus() == Status.CREATED) {  // Release action
            cn.setResponsible(null);
        }
        cn.setNotes(n.getNotes());
        cn.setStatus(n.getStatus());
        cn.setStatusReason(n.getStatusReason());

        return save(cn);
    }

    /**
     * Create several notifications.
     *
     * @param notificationListDTO the group of entity to create.
     * @return the persisted entity.
     */
    public NotificationListDTO create(NotificationListDTO notificationListDTO) {
        log.debug("Request to create the list of Notification : {}", notificationListDTO);
        Set<Notification> result = new HashSet<>();
        Set<Notification> resultWithErrors = new HashSet<>();
        notificationListDTO.getNotifications().forEach(notification -> {
            Notification n;
            try {
                n = create(notification);
                result.add(n);
            } catch (BadRequestAlertException brae) {
                notification.status(Status.CANCELLED);
                notification.statusReason(brae.getMessage());
                resultWithErrors.add(notification);
            }
        });
        notificationListDTO.setNotifications(result);
        notificationListDTO.setNotificationsWithErrors(resultWithErrors);
        return notificationListDTO;
    }

    /**
     * Update several notifications.
     *
     * @param notificationListDTO the group of entity to update.
     * @return the persisted entity.
     */
    public NotificationListDTO update(NotificationListDTO notificationListDTO) {
        log.debug("Request to update the list of Notification : {}", notificationListDTO);
        Set<Notification> result = new HashSet<>();
        Set<Notification> resultWithErrors = new HashSet<>();
        notificationListDTO.getNotifications().forEach(notification -> {
            Notification t;
            try {
                t = update(notification);
                result.add(t);
            } catch (BadRequestAlertException brae) {
                notification.status(Status.CANCELLED);
                notification.statusReason(brae.getMessage());
                resultWithErrors.add(notification);
            }
        });
        notificationListDTO.setNotifications(result);
        notificationListDTO.setNotificationsWithErrors(resultWithErrors);
        return notificationListDTO;
    }


    /**
     * Get all the notifications.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Notification> findAll(Pageable pageable) {
        log.debug("Request to get all Notifications");
        return notificationRepository.findAll(pageable);
    }


    /**
     * Get one notification by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Notification> findOne(Long id) {
        log.debug("Request to get Notification : {}", id);
        if (id == null) return Optional.empty();
        return notificationRepository.findById(id);
    }

    /**
     * Get one notification by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Notification> findOneByIdAndSenderId(Long id, Long senderId) {
        log.debug("Request to get Notification : {}", id);
        if (id == null || senderId == null) return Optional.empty();
        return notificationRepository.findOneByIdAndSenderId(id, senderId);
    }

    /**
     * Delete the notification by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Notification : {}", id);
        Notification cn = findOne(id).orElseThrow(() ->
            new NotFoundAlertException(ENTITY_NAME + " id: " + id, ENTITY_NAME, NOT_FOUND));
        log.debug("Cancelling current Notification : {}", cn);
        cn.setStatus(Status.CANCELLED);
        cn.setStatusReason("Logically DELETED");
        save(cn);
    }

    /**
     * Search for the notification corresponding to the query.
     *
     * @param query    the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Notification> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Notifications for query {}", query);
        return notificationSearchRepository.search(queryStringQuery(query), pageable);
    }

    @Transactional
    public void saveAll(List<Notification> notificationList) {
        if (!notificationList.isEmpty())
            notificationSearchRepository.saveAll(notificationList);
    }

    /**
     * Validate the new status.
     *
     * @param newStatus     status to validate.
     * @param currentStatus status of the current entity to validate.
     * @return boolean value according to validation.
     */
    boolean validateStatusChange(Status newStatus, Status currentStatus) {
        if (newStatus == null) return false;
        if (newStatus == currentStatus) return true;
        switch (currentStatus) {
            case CREATED:
                if (newStatus == Status.CANCELLED && extendedUserService.isAdmin(true)) {
                    throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.current.not.perform.action"), ENTITY_NAME, "WrongUserRole");
                }

                if (newStatus == Status.IN_PROCESS && !extendedUserService.isAdmin(true)) {
                    throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.current.not.perform.action"), ENTITY_NAME, "WrongUserRole");
                }
                return newStatus == Status.IN_PROCESS || newStatus == Status.CANCELLED;
            case IN_PROCESS:
                if (!extendedUserService.isAdmin(true)) {
                    throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.current.not.perform.action"), ENTITY_NAME, "WrongUserRole");
                }
                return newStatus == Status.ACCEPTED || newStatus == Status.REJECTED || newStatus == Status.CREATED;
            case ACCEPTED:
            case REJECTED:
            case CANCELLED:
            default:
                return false;
        }
    }

    @Async
    public void refetch(Long id) {
        refetchService.refetchNotificationService(id);
    }

    @Async
    public void refetchAll(Collection<Notification> elements) {
        elements.forEach(e -> refetch(e.getId()));
    }

    Optional<List<Notification>> findAllByDownloadedFileAndDateCreatedLessThan(ZonedDateTime dateToErase) {
        return notificationRepository.findAllByCategoryAndDateCreatedLessThan(NotificationCategory.DOWNLOADED_FILE, dateToErase);
    }

}
