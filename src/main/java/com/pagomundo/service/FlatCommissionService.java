package com.pagomundo.service;

import com.pagomundo.domain.Bank;
import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.FlatCommission;
import com.pagomundo.repository.FlatCommissionRepository;
import com.pagomundo.repository.search.FlatCommissionSearchRepository;
import com.pagomundo.repository.search.RefetchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link FlatCommission}.
 */
@Service
@Transactional
public class FlatCommissionService {

    public static final String ENTITY_NAME = "flatCommission";
    public static final BigDecimal MAX_COMMISSION = new BigDecimal(999999999);

    private final Logger log = LoggerFactory.getLogger(FlatCommissionService.class);
    private final FlatCommissionRepository flatCommissionRepository;

    private final FlatCommissionSearchRepository flatCommissionSearchRepository;

    private final RefetchService refetchService;

    public FlatCommissionService(FlatCommissionRepository flatCommissionRepository, FlatCommissionSearchRepository flatCommissionSearchRepository, RefetchService refetchService) {
        this.flatCommissionRepository = flatCommissionRepository;
        this.flatCommissionSearchRepository = flatCommissionSearchRepository;
        this.refetchService = refetchService;
    }

    /**
     * Save a flatCommission.
     *
     * @param flatCommission the entity to save.
     * @return the persisted entity.
     */
    public FlatCommission save(FlatCommission flatCommission) {
        log.debug("Request to save FlatCommission : {}", flatCommission);
        flatCommission.setLastUpdated(ZonedDateTime.now());
        flatCommission.setDateCreated(ZonedDateTime.now());
        FlatCommission result = flatCommissionRepository.saveAndFlush(flatCommission);
        flatCommissionSearchRepository.save(result);
        return result;
    }

    /**
     * Create a flatCommissionList set.
     *
     * @param flatCommissionList the entity to save.
     * @return the persisted entity.
     */
    public List<FlatCommission> create(List<FlatCommission> flatCommissionList, ExtendedUser merchant) {
        log.debug("Request to save List<FlatCommission> : {}", flatCommissionList);

        findAllByMerchant(merchant.getId()).forEach(flatCommission -> delete(flatCommission.getId()));

        List<FlatCommission> newFlatCommissionList = new ArrayList<>();

        flatCommissionList.forEach(fcDTO -> {
            fcDTO.setExtendedUser(merchant);
            if (fcDTO.getUpTo() == null) fcDTO.setUpTo(MAX_COMMISSION);
            newFlatCommissionList.add(save(fcDTO));
        });

        return newFlatCommissionList;
    }

    /**
     * Get all the flatCommissions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<FlatCommission> findAll(Pageable pageable) {
        log.debug("Request to get all FlatCommissions");
        return flatCommissionRepository.findAll(pageable);
    }

    /**
     * Get all the flatCommissions for one merchant.
     *
     * @param merchantId the merchant id.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<FlatCommission> findAllByMerchant(Long merchantId) {
        log.debug("Request to get all FlatCommissions for one merchant");
        return flatCommissionRepository.findAllByExtendedUserIdOrderByUpToAsc(merchantId);
    }


    /**
     * Get one flatCommission by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<FlatCommission> findOne(Long id) {
        log.debug("Request to get FlatCommission : {}", id);
        return flatCommissionRepository.findById(id);
    }

    /**
     * Delete the flatCommission by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete FlatCommission : {}", id);
        flatCommissionRepository.deleteById(id);
        flatCommissionSearchRepository.deleteById(id);
    }

    /**
     * Search for the flatCommission corresponding to the query.
     *
     * @param query    the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<FlatCommission> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of FlatCommissions for query {}", query);
        return flatCommissionSearchRepository.search(queryStringQuery(query), pageable);
    }

    @Transactional
    public void saveAll(List<FlatCommission> flatCommissionList) {
        if (!flatCommissionList.isEmpty())
            flatCommissionSearchRepository.saveAll(flatCommissionList);
    }

    @Async
    public void refetch(Long id) {
        refetchService.refetchIdTye(id);
    }

    @Async
    public void refetchAll(Collection<Bank> elements) {
        elements.forEach(e -> refetch(e.getId()));
    }


}
