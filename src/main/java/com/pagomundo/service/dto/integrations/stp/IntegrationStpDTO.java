package com.pagomundo.service.dto.integrations.stp;

import com.pagomundo.domain.enumeration.stp.StpAccountType;
import com.pagomundo.domain.enumeration.stp.StpBankId;
import com.pagomundo.domain.enumeration.stp.StpConversionFields;
import com.pagomundo.domain.enumeration.stp.StpPaymentType;
import io.swagger.annotations.ApiModelProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class IntegrationStpDTO {

    private final Logger log = LoggerFactory.getLogger(IntegrationStpDTO.class);


    @ApiModelProperty(hidden = true)
    private String stpId;

    @ApiModelProperty(example = "123456789", required = true)
    private String trackingId;

    @ApiModelProperty(required = true, example = "Pago mes en curso")
    private String description;

    @ApiModelProperty(required = true, example = "846180000400000001")
    private String accountNumberDestiny;

    @ApiModelProperty(hidden = true)
    private String accountNumberOrigin;

    @ApiModelProperty(hidden = true)
    private String invoiceOrigin;

    @ApiModelProperty(hidden = true)
    private String emailDestiny;

    @ApiModelProperty(required = true, example = "PMI_AMERICAS")
    private String company;

    @ApiModelProperty(hidden = true)
    private String operationDate;

    @ApiModelProperty(hidden = true)
    private String electronicSignature;

    @ApiModelProperty(required = true, example = "STPDEMO")
    private StpBankId bankIdDestiny;

    @ApiModelProperty(hidden = true)
    private StpBankId bankIdOrigin;

    @ApiModelProperty(required = true, example = "11.35")
    private String amount;

    @ApiModelProperty(required = true, example = "Payee de ejemplo")
    private String destinyName;

    @ApiModelProperty(hidden = true)
    private String originName;

    @ApiModelProperty(required = true, example = "1234567")
    private String referenceNumber;

    @ApiModelProperty(hidden = true)
    private String rfcCurpDestiny;

    @ApiModelProperty(hidden = true)
    private String rfcCurpOrigin;

    @ApiModelProperty(required = true, example = "CLABE")
    private StpAccountType accountTypeDestiny;

    @ApiModelProperty(hidden = true)
    private StpAccountType accountTypeOrigin;

    @ApiModelProperty(hidden = true)
    private StpPaymentType paymentType;

    @ApiModelProperty(hidden = true)
    private String topology;

    public String getInvoiceOrigin() {
        return invoiceOrigin;
    }

    public void setInvoiceOrigin(String invoiceOrigin) {
        this.invoiceOrigin = invoiceOrigin;
    }

    public String getStpId() {
        return stpId;
    }

    public void setStpId(String stpId) {
        this.stpId = stpId;
    }

    public String getTrackingId() {
        return trackingId;
    }

    public void setTrackingId(String trackingId) {
        this.trackingId = trackingId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAccountNumberDestiny() {
        return accountNumberDestiny;
    }

    public void setAccountNumberDestiny(String accountNumberDestiny) {
        this.accountNumberDestiny = accountNumberDestiny;
    }

    public String getAccountNumberOrigin() {
        return accountNumberOrigin;
    }

    public void setAccountNumberOrigin(String accountNumberOrigin) {
        this.accountNumberOrigin = accountNumberOrigin;
    }

    public String getEmailDestiny() {
        return emailDestiny;
    }

    public void setEmailDestiny(String emailDestiny) {
        this.emailDestiny = emailDestiny;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(String operationDate) {
        this.operationDate = operationDate;
    }

    public String getElectronicSignature() {
        return electronicSignature;
    }

    public void setElectronicSignature(String electronicSignature) {
        this.electronicSignature = electronicSignature;
    }

    public StpBankId getBankIdDestiny() {
        return bankIdDestiny;
    }

    public void setBankIdDestiny(StpBankId bankIdDestiny) {
        this.bankIdDestiny = bankIdDestiny;
    }

    public StpBankId getBankIdOrigin() {
        return bankIdOrigin;
    }

    public void setBankIdOrigin(StpBankId bankIdOrigin) {
        this.bankIdOrigin = bankIdOrigin;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDestinyName() {
        return destinyName;
    }

    public void setDestinyName(String destinyName) {
        this.destinyName = destinyName;
    }

    public String getOriginName() {
        return originName;
    }

    public void setOriginName(String originName) {
        this.originName = originName;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getRfcCurpDestiny() {
        return rfcCurpDestiny;
    }

    public void setRfcCurpDestiny(String rfcCurpDestiny) {
        this.rfcCurpDestiny = rfcCurpDestiny;
    }

    public String getRfcCurpOrigin() {
        return rfcCurpOrigin;
    }

    public void setRfcCurpOrigin(String rfcCurpOrigin) {
        this.rfcCurpOrigin = rfcCurpOrigin;
    }

    public StpAccountType getAccountTypeDestiny() {
        return accountTypeDestiny;
    }

    public void setAccountTypeDestiny(StpAccountType accountTypeDestiny) {
        this.accountTypeDestiny = accountTypeDestiny;
    }

    public StpAccountType getAccountTypeOrigin() {
        return accountTypeOrigin;
    }

    public void setAccountTypeOrigin(StpAccountType accountTypeOrigin) {
        this.accountTypeOrigin = accountTypeOrigin;
    }

    public StpPaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(StpPaymentType paymentType) {
        this.paymentType = paymentType;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        String newLine = ", ";

        result.append(this.getClass().getName());
        result.append(" Object {");

        //determine fields declared in this class only (no fields of superclass)
        Field[] fields = this.getClass().getDeclaredFields();

        //print field names paired with their values
        for (Field field : fields) {
            result.append(" \"");
            try {
                result.append(field.getName());
                result.append("\": \"");
                //requires access to private field:
                result.append(field.get(this));
                result.append("\"");
            } catch (IllegalAccessException ex) {
                log.error("IllegalAccessException: {}", ex.getLocalizedMessage());
            }
            result.append(newLine);
        }

        result.deleteCharAt(result.length() - 2);
        result.append("}");

        return result.toString();
    }

    private String getStpName(String field) {
        for (StpConversionFields fieldName : StpConversionFields.values()) {
            if (field.equals(fieldName.name())) {
                return fieldName.getValue();
            }
        }
        return null;
    }

    public String toStpJsonString() {
        StringBuilder result = new StringBuilder();
        String newLine = ", ";

        result.append("{");

        //determine fields declared in this class only (no fields of superclass)
        Field[] fields = this.getClass().getDeclaredFields();

        //print field names paired with their values
        for (Field field : fields) {
            try {
                if (field.get(this) != null && !field.getName().equals("log")) {
                    result.append(" \"");
                    result.append(getStpName(field.getName()));
                    result.append("\": \"");
                    if (field.getType().isEnum()) {
                        Class<?> clz = field.getType();
                        Object[] consts = clz.getEnumConstants();
                        Method mth = clz.getDeclaredMethod("getValue");
                        int index = 0;
                        for (Object item : consts) {
                            if (item.equals(field.get(this))) {
                                break;
                            }
                            index++;
                        }
                        String finalValue = (String) mth.invoke(consts[index]);
                        /* Prove it worked. */
                        result.append(finalValue);
                    } else {
                        result.append(field.get(this));
                    }
                    result.append("\"");
                    result.append(newLine);
                }
            } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException ex) {
                log.error("IllegalAccessException: {}", ex.getLocalizedMessage());
            }
        }
        result.deleteCharAt(result.length() - 2);
        result.append("}");

        return result.toString();
    }

    public String getTopology() {
        return topology;
    }

    public void setTopology(String topology) {
        this.topology = topology;
    }
}

