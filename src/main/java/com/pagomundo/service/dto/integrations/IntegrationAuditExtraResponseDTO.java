package com.pagomundo.service.dto.integrations;

import javax.persistence.Lob;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A DTO for the {@link com.pagomundo.domain.IntegrationAuditExtraResponse} entity.
 */
public class IntegrationAuditExtraResponseDTO implements Serializable {

    private Long id;

    private ZonedDateTime messageDate;

    @Lob
    private String message;


    private Long integrationAuditId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getMessageDate() {
        return messageDate;
    }

    public void setMessageDate(ZonedDateTime messageDate) {
        this.messageDate = messageDate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getIntegrationAuditId() {
        return integrationAuditId;
    }

    public void setIntegrationAuditId(Long integrationAuditId) {
        this.integrationAuditId = integrationAuditId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        IntegrationAuditExtraResponseDTO integrationAuditExtraResponseDTO = (IntegrationAuditExtraResponseDTO) o;
        if (integrationAuditExtraResponseDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), integrationAuditExtraResponseDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "IntegrationAuditExtraResponseDTO{" +
            "id=" + getId() +
            ", messageDate='" + getMessageDate() + "'" +
            ", message='" + getMessage() + "'" +
            ", integrationAudit=" + getIntegrationAuditId() +
            "}";
    }
}
