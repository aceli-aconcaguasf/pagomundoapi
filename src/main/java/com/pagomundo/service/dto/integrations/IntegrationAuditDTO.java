package com.pagomundo.service.dto.integrations;

import com.pagomundo.domain.enumeration.Status;

import javax.persistence.Lob;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A DTO for the {@link com.pagomundo.domain.IntegrationAudit} entity.
 */
public class IntegrationAuditDTO implements Serializable {

    private Long id;

    private ZonedDateTime requestDate;

    @Lob
    private String requestMessage;

    private String requestEndpoint;

    private ZonedDateTime responseDate;

    @Lob
    private String responseMessage;

    private Long entityId;

    private String entity;

    private String providerId;

    private String provider;

    private String extraResponseEndpoint;

    private Status status;

    @Lob
    private String statusReason;

    private Integer retries;

    private Integer maxRetries;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(ZonedDateTime requestDate) {
        this.requestDate = requestDate;
    }

    public String getRequestMessage() {
        return requestMessage;
    }

    public void setRequestMessage(String requestMessage) {
        this.requestMessage = requestMessage;
    }

    public String getRequestEndpoint() {
        return requestEndpoint;
    }

    public void setRequestEndpoint(String requestEndpoint) {
        this.requestEndpoint = requestEndpoint;
    }

    public ZonedDateTime getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(ZonedDateTime responseDate) {
        this.responseDate = responseDate;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getExtraResponseEndpoint() {
        return extraResponseEndpoint;
    }

    public void setExtraResponseEndpoint(String extraResponseEndpoint) {
        this.extraResponseEndpoint = extraResponseEndpoint;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getStatusReason() {
        return statusReason;
    }

    public void setStatusReason(String statusReason) {
        this.statusReason = statusReason;
    }

    public Integer getRetries() {
        return retries;
    }

    public void setRetries(Integer retries) {
        this.retries = retries;
    }

    public Integer getMaxRetries() {
        return maxRetries;
    }

    public void setMaxRetries(Integer maxRetries) {
        this.maxRetries = maxRetries;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        IntegrationAuditDTO integrationAuditDTO = (IntegrationAuditDTO) o;
        if (integrationAuditDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), integrationAuditDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "IntegrationAuditDTO{" +
            "id=" + getId() +
            ", requestDate='" + getRequestDate() + "'" +
            ", requestMessage='" + getRequestMessage() + "'" +
            ", requestEndpoint='" + getRequestEndpoint() + "'" +
            ", responseDate='" + getResponseDate() + "'" +
            ", responseMessage='" + getResponseMessage() + "'" +
            ", entityId=" + getEntityId() +
            ", entity='" + getEntity() + "'" +
            ", providerId='" + getProviderId() + "'" +
            ", provider='" + getProvider() + "'" +
            ", extraResponseEndpoint='" + getExtraResponseEndpoint() + "'" +
            ", status='" + getStatus() + "'" +
            ", statusReason='" + getStatusReason() + "'" +
            ", retries=" + getRetries() +
            ", maxRetries=" + getMaxRetries() +
            "}";
    }
}
