package com.pagomundo.service.dto.integrations.stp;

import io.swagger.annotations.ApiModelProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;

public class IntegrationResponseStpDTO {

    private final Logger log = LoggerFactory.getLogger(IntegrationResponseStpDTO.class);

    @ApiModelProperty(example = "7978958", required = true)
    private String id;

    @ApiModelProperty(required = true, example = "PMI_AMERICAS")
    private String empresa;

    @ApiModelProperty(example = "123456789", required = false)
    private String folioOrigen;

    @ApiModelProperty(required = true, example = "Éxito")
    private String estado;

    @ApiModelProperty(required = true, example = "no")
    private String causaDevolucion;

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        String newLine = ", ";

        result.append("{");

        //determine fields declared in this class only (no fields of superclass)
        Field[] fields = this.getClass().getDeclaredFields();

        //print field names paired with their values
        for (Field field : fields) {
            if (field.getName().equals("log"))
                continue;

            result.append(" \"");
            try {
                result.append(field.getName());
                result.append("\": \"");
                //requires access to private field:
                result.append(field.get(this));
                result.append("\"");
            } catch (IllegalAccessException ex) {
                log.error("IllegalAccessException: {}", ex.getLocalizedMessage());
            }
            result.append(newLine);
        }

        result.deleteCharAt(result.length() - 2);
        result.append("}");

        return result.toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getFolioOrigen() {
        return folioOrigen;
    }

    public void setFolioOrigen(String folioOrigen) {
        this.folioOrigen = folioOrigen;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCausaDevolucion() {
        return causaDevolucion;
    }

    public void setCausaDevolucion(String causaDevolucion) {
        this.causaDevolucion = causaDevolucion;
    }
}

