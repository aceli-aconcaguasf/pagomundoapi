package com.pagomundo.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.pagomundo.domain.Authority;
import com.pagomundo.domain.Bank;
import com.pagomundo.domain.City;
import com.pagomundo.domain.Country;
import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.ExtendedUserRelation;
import com.pagomundo.domain.IdType;
import com.pagomundo.domain.User;
import com.pagomundo.domain.enumeration.BankAccountType;
import com.pagomundo.domain.enumeration.Status;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A DTO representing a user, with his authorities.
 */
public class UserDTO {

    private Long id;

    @JsonIgnore
    @Size(min = 1, max = 50)
    private String login;

    @Size(max = 50)
    private String firstName;

    @Size(max = 50)
    private String lastName;

    @Size(min = 5, max = 254)
    private String email;

    private String role;

    @JsonIgnore
    @Size(max = 256)
    private String imageUrl;

    @JsonIgnore
    private boolean activated = false;

    @Size(min = 2, max = 10)
    private String langKey;

    @JsonIgnore
    private String createdBy;

    @JsonIgnore
    private Instant createdDate;

    @JsonIgnore
    private String lastModifiedBy;

    @JsonIgnore
    private Instant lastModifiedDate;

    private Set<String> authorities;

    //@JsonIgnore
    private ExtendedUser toWhom;

    @Enumerated(EnumType.STRING)
    private Status status = null;

    private String statusReason = "";

    @JsonIgnore
    private Boolean useDirectPayment;

    private String bankAccountNumber;

    @Enumerated(EnumType.STRING)
    private BankAccountType bankAccountType;

    @JsonIgnoreProperties(allowSetters = true, value = {"country"})
    private Bank directPaymentBank;

    @JsonIgnoreProperties(allowSetters = true,
        value = {"code", "extendedUsersForResidenceCities", "extendedUsersForPostalCities", "country"})
    private City directPaymentCity;

    //@JsonIgnore
    private Country country;

    @JsonIgnore
    private User user;

    private Boolean payeeAsCompany = false;

    private String taxId;

    private String idNumber;

    private String mobileNumber;

    private ZonedDateTime birthDate = null;

    private String company;

    private String alias;

    private String postalAddress;

    private String residenceAddress;

    private String nickname = "";

    private String bankBranch;

    @JsonIgnoreProperties(allowSetters = true,
        value = {
            "spanishName",
            "subRegionName",
            "regionName",
            "code",
            "forPayee",
            "currencyName",
            "currencyCode",
            "currencyValue",
            "currencyBase",
            "currencyLastUpdated",
            "banks",
            "cities",
            "currencies",
            "idTypes"
        })
    private Country postalCountry;

    @JsonIgnoreProperties(allowSetters = true,
        value = {
            "spanishName",
            "subRegionName",
            "regionName",
            "code",
            "forPayee",
            "currencyName",
            "currencyCode",
            "currencyValue",
            "currencyBase",
            "currencyLastUpdated",
            "banks",
            "cities",
            "currencies",
            "idTypes"
        })
    private Country residenceCountry;

    @JsonIgnoreProperties(allowSetters = true,
        value = {
            "code",
            "naturalOrLegal",
            "extendedUsers",
            "country"
        })
    private IdType idType;

    @JsonIgnoreProperties(allowSetters = true,
        value = {
            "code",
            "naturalOrLegal",
            "extendedUsers",
            "country"
        })
    private IdType idTypeTaxId;

    private Instant resetDate = null;

    private String resetKey = "";

    public UserDTO() {
        // Empty constructor needed for Jackson.
    }

    public UserDTO(ExtendedUserRelation eur) {
        this(eur.getUserRelated());
        this.nickname = eur.getNickname() != null ? eur.getNickname() : "";
    }

    public UserDTO(User user, String nickname) {
        this(user);
        this.nickname = nickname != null ? nickname : "";
    }

    public UserDTO(User user) {
        this.id = user.getId();
        this.login = user.getLogin();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.email = user.getEmail() != null ? user.getEmail() : user.getLogin();
        this.activated = user.getActivated();
        this.imageUrl = user.getImageUrl();
        this.langKey = user.getLangKey();
        this.createdBy = user.getCreatedBy();
        this.createdDate = user.getCreatedDate();
        this.lastModifiedBy = user.getLastModifiedBy();
        this.lastModifiedDate = user.getLastModifiedDate();
        this.authorities = user.getAuthorities().stream().map(Authority::getName).collect(Collectors.toSet());
        this.status = user.getStatus();
        this.statusReason = user.getStatusReason();
        this.payeeAsCompany = user.isPayeeAsCompany();
        this.role = user.getRole();
        this.resetDate = user.getResetDate();
        this.user = user;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }

    public IdType getIdType() {
        return idType;
    }

    public void setIdType(IdType idType) {
        this.idType = idType;
    }

    public IdType getIdTypeTaxId() {
        return idTypeTaxId;
    }

    public void setIdTypeTaxId(IdType idTypeTaxId) {
        this.idTypeTaxId = idTypeTaxId;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public ZonedDateTime getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(ZonedDateTime birthDate) {
        this.birthDate = birthDate;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(String postalAddress) {
        this.postalAddress = postalAddress;
    }

    public String getResidenceAddress() {
        return residenceAddress;
    }

    public void setResidenceAddress(String residenceAddress) {
        this.residenceAddress = residenceAddress;
    }

    public Country getPostalCountry() {
        return postalCountry;
    }

    public void setPostalCountry(Country postalCountry) {
        this.postalCountry = postalCountry;
    }

    public Country getResidenceCountry() {
        return residenceCountry;
    }

    public void setResidenceCountry(Country residenceCountry) {
        this.residenceCountry = residenceCountry;
    }

    public Boolean isPayeeAsCompany() {
        return payeeAsCompany;
    }

    public void setPayeeAsCompany(Boolean payeeAsCompany) {
        this.payeeAsCompany = payeeAsCompany;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public UserDTO setUserInfo(User user) {
        this.id = user.getId();
        this.login = user.getLogin();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.email = user.getEmail();
        this.activated = user.getActivated();
        this.imageUrl = user.getImageUrl();
        this.langKey = user.getLangKey();
        this.createdBy = user.getCreatedBy();
        this.createdDate = user.getCreatedDate();
        this.lastModifiedBy = user.getLastModifiedBy();
        this.lastModifiedDate = user.getLastModifiedDate();
        this.authorities = user.getAuthorities().stream().map(Authority::getName).collect(Collectors.toSet());
        this.status = user.getStatus();
        this.statusReason = user.getStatusReason();
        return this;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Boolean getUseDirectPayment() {
        return useDirectPayment;
    }

    public void setUseDirectPayment(Boolean useDirectPayment) {
        this.useDirectPayment = useDirectPayment;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public BankAccountType getBankAccountType() {
        return bankAccountType;
    }

    public void setBankAccountType(BankAccountType bankAccountType) {
        this.bankAccountType = bankAccountType;
    }

    public Bank getDirectPaymentBank() {
        return directPaymentBank;
    }

    public void setDirectPaymentBank(Bank directPaymentBank) {
        this.directPaymentBank = directPaymentBank;
    }

    public City getDirectPaymentCity() {
        return directPaymentCity;
    }

    public void setDirectPaymentCity(City directPaymentCity) {
        this.directPaymentCity = directPaymentCity;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getStatusReason() {
        return statusReason;
    }

    public void setStatusReason(String statusReason) {
        this.statusReason = statusReason;
    }

    public ExtendedUser getToWhom() {
        return toWhom;
    }

    public void setToWhom(ExtendedUser toWhom) {
        this.toWhom = toWhom;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public String getLangKey() {
        return langKey;
    }

    public void setLangKey(String langKey) {
        this.langKey = langKey;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Set<String> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<String> authorities) {
        this.authorities = authorities;
    }

    public Instant getResetDate() {
        return resetDate;
    }

    public void setResetDate(Instant resetDate) {
        this.resetDate = resetDate;
    }

    public String getResetKey() {
        return this.resetKey;
    }

    public void setResetKey(String resetKey) {
        this.resetKey = resetKey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserDTO)) {
            return false;
        }
        return id != null && id.equals(((UserDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
            "login='" + login + '\'' +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", email='" + email + '\'' +
            ", imageUrl='" + imageUrl + '\'' +
            ", activated=" + activated +
            ", langKey='" + langKey + '\'' +
            ", createdBy=" + createdBy +
            ", createdDate=" + createdDate +
            ", lastModifiedBy='" + lastModifiedBy + '\'' +
            ", lastModifiedDate=" + lastModifiedDate +
            ", authorities=" + authorities +
            "}";
    }
}
