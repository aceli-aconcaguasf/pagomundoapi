package com.pagomundo.service.dto;

/**
 * A DTO representing a email change required data - current and new email.
 */
public class EmailChangeDTO {
    private String currentEmail;
    private String newEmail;

    public EmailChangeDTO() {
        // Empty constructor needed for Jackson.
    }

    public EmailChangeDTO(String currentEmail, String newEmail) {
        this.currentEmail = currentEmail;
        this.newEmail = newEmail;
    }

    public String getCurrentEmail() {
        return currentEmail;
    }

    public void setCurrentEmail(String currentEmail) {
        this.currentEmail = currentEmail;
    }

    public String getNewEmail() {
        return newEmail;
    }

    public void setNewEmail(String newEmail) {
        this.newEmail = newEmail;
    }
}
