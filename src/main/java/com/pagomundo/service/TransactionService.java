package com.pagomundo.service;

import com.google.gson.JsonObject;
import com.pagomundo.domain.BankAccount;
import com.pagomundo.domain.Country;
import com.pagomundo.domain.Currency;
import com.pagomundo.domain.DTO.*;
import com.pagomundo.domain.DTO.Ripple.RippleResultDTO;
import com.pagomundo.domain.DTO.Ripple.RippleResultGroupDTO;
import com.pagomundo.domain.DirectPaymentBank;
import com.pagomundo.domain.ThresholdPayee;
import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.ExtendedUserRelation;
import com.pagomundo.repository.ExtendedUserRepository;
import com.pagomundo.domain.Transaction;
import com.pagomundo.domain.enumeration.ProcessType;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.repository.TransactionRepository;
import com.pagomundo.repository.search.RefetchService;
import com.pagomundo.repository.search.TransactionSearchRepository;
import com.pagomundo.web.rest.errors.BadRequestAlertException;
import com.pagomundo.web.rest.errors.NotFoundAlertException;
import com.pagomundo.repository.ThresholdPayeeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.math.*;
import java.math.BigDecimal;
import java.net.URI;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import java.sql.Timestamp;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import org.springframework.web.client.DefaultResponseErrorHandler;

/**
 * Service Implementation for managing {@link Transaction}.
 */
@Service
@Transactional
public class TransactionService {
    public static final String ENTITY_NAME = "transaction";
    public static final String NOT_FOUND = ENTITY_NAME + ".001";
    public static final String BAD_REQUEST = ENTITY_NAME + ".002";
    private final Logger log = LoggerFactory.getLogger(TransactionService.class);
    private final TransactionRepository transactionRepository;
    private final BankAccountService bankAccountService;
    private final ExtendedUserService extendedUserService;
    private final ExtendedUserRelationService extendedUserRelationsService;
    private final ExtendedUserFundsService extendedUserFundsService;
    private final TransactionSearchRepository transactionSearchRepository;
    private final TransactionStatusChangeService transactionStatusChangeService;
    private final FlatCommissionService flatCommissionService;
    private final DirectPaymentBankService directPaymentBankService;
    @Value("${payment.min-amount}")
    private float paymentMinAmount;
    private final RefetchService refetchService;
    private final TranslateService translateService;
    private final ExtendedUserRepository extendedUserRepository;
    private final ThresholdPayeeRepository thresholdPayeeRepository;
    
    @Value("#{'${merchants}'.split(',')}")
    private List<Long> allowedMerchants;

    @Value("${kushki}")
    private String kushki;

    @Value("${yaydooNotification}")
    private String yaydooNotification;

    private final float maxPaymentAmount;
    
    @Value("${ripple-url}")
    private String rippleURL;

    @Value("${ripple-client-id}")
    private String rippleClientId;

    @Value("${ripple-cliente-secret}")
    private String rippleClientSecret;


    public TransactionService(
        TransactionRepository transactionRepository,
        ExtendedUserRelationService extendedUserRelationsService, RefetchService refetchService, TransactionSearchRepository transactionSearchRepository,
        TransactionStatusChangeService transactionStatusChangeService,
        BankAccountService bankAccountService,
        ExtendedUserService extendedUserService,
        ExtendedUserFundsService extendedUserFundsService,
        FlatCommissionService flatCommissionService,
        TranslateService translateService,
        ExtendedUserRepository extendedUserRepository,
        ThresholdPayeeRepository thresholdPayeeRepository,
        DirectPaymentBankService directPaymentBankService) {
        this.transactionRepository = transactionRepository;
        this.extendedUserRelationsService = extendedUserRelationsService;
        this.refetchService = refetchService;
        this.transactionSearchRepository = transactionSearchRepository;
        this.transactionStatusChangeService = transactionStatusChangeService;
        this.bankAccountService = bankAccountService;
        this.extendedUserService = extendedUserService;
        this.extendedUserFundsService = extendedUserFundsService;
        this.flatCommissionService = flatCommissionService;
        this.translateService = translateService;
        this.extendedUserRepository = extendedUserRepository;
        this.thresholdPayeeRepository = thresholdPayeeRepository;
        this.maxPaymentAmount = 10000;
        this.directPaymentBankService = directPaymentBankService;
    }

    /**
     * Save a transaction.
     *
     * @param t the entity to save.
     * @param mustChangeStatus if a change of status must be registered.
     * @return the persisted entity.
     */
    public Transaction save(Transaction t, boolean mustChangeStatus) {
        
        boolean updateBalance = true;
        
        if(t.getStatus()==Status.REVERTED){
            updateBalance = false;
            t.setStatus(Status.CREATED);
        }
        
        if(t.getStatus()==Status.SETTLEMENT_DECLINED)t.setStatus(Status.IN_PROCESS);
        
        t.setLastUpdated(ZonedDateTime.now());
        
        Transaction result = transactionRepository.saveAndFlush(t);
        // Transaction result = transactionRepository.save(t);

        log.debug(">>>>>>> transactionRepository.saveAndFlush: {}", result);

        transactionSearchRepository.save(result);

        log.debug(">>>>>>> transactionSearchRepository: {}", result);

        if (mustChangeStatus) transactionStatusChangeService.createStatusChange(result);

        if (updateBalance)extendedUserFundsService.updateBalance(result);

        return result;
    }

    /**
     * Save a transaction.
     *
     * @param t the entity to save.
     * @return the persisted entity.
     */
    public Transaction save(Transaction t) {
        return save(t, true);
    }

    /**
     * Create a transaction.
     *
     * @param t the entity to create.
     * @return the persisted entity.
     */
    public Transaction create(Transaction t) {

        log.info(">>>>>> Request to create one Transaction : {}", t);

        if (!extendedUserService.isMerchant())
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.current.not.perform.action"),
                ENTITY_NAME, ExtendedUserService.WRONG_USER_ROLE);

                log.info(">>>>>> after WRONG_USER_ROLE validation {} ", t);

        if(!allowedMerchants.contains(extendedUserService.getCurrentExtendedUser().getId())) {
            
            if (t.getAmountBeforeCommission().floatValue() < paymentMinAmount || t.getAmountBeforeCommission().floatValue() > maxPaymentAmount)
                throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.wrong.amount"), ENTITY_NAME, BAD_REQUEST);

        }

        log.info(">>>>>> after getAmountBeforeCommission YAYDOO validation {} ", t);

        ExtendedUser merchant = extendedUserService.getCurrentExtendedUser();

        if (!Status.ACCEPTED.equals(merchant.getStatus()))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.merchant.incorrect.status") + " " + merchant.getStatus(),
                ENTITY_NAME, ExtendedUserService.INCORRECT_STATUS);

        log.info(">>>>>> after ACCEPTED mercahnt validation {}", t);

        if (!extendedUserService.merchantHasEnoughMoney(merchant, t.getAmountBeforeCommission()))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.merchant.money.payment"),
                ENTITY_NAME, BAD_REQUEST);

        log.info(">>>>>> after merchantHasEnoughMoney mercahnt validation {}", t);
        

        if (t.getPayee() == null)
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.payee.not.defined"), ENTITY_NAME, BAD_REQUEST);

        log.info(">>>>>> after getPayee null validation {}", t);

        ExtendedUserRelation eur = extendedUserRelationsService.findRelation(t.getPayee(), merchant).orElseThrow(() ->
            new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.payee.not.found"), ENTITY_NAME, ExtendedUserService.NOT_FOUND));

        log.info(">>>>>> after relation payee merchant validation {}", t);

        ExtendedUser payee = eur.getExtendedUser();

        if (payee == null)
            throw new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.payee.not.found"), ENTITY_NAME, ExtendedUserService.NOT_FOUND);
        
        log.debug(">>>>> {}: after relation payee ",t );

        if (!Status.ACCEPTED.equals(payee.getStatus()) && !payee.getPostalAddress().equals("Default"))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.payee.incorrect.status") + " " + payee.getStatus(),
                ENTITY_NAME, ExtendedUserService.INCORRECT_STATUS);

        log.info(">>>>>> after INCORRECT_STATUS validation {}", t);


        Country country = (payee.getPostalCountry() != null ? payee.getPostalCountry() : payee.getPostalCity().getCountry());
        Iterator<Currency> iterator = country.getCurrencies().iterator();
        if (iterator.hasNext()) t.setCurrency(iterator.next());

        // TODO: Revisar por que se asigna la ultima moneda a la transaccioón. Revisar detenidamente cuando un país tiene mas de un tipo de moneda!!

        t.setMerchant(merchant);
        t.setReseller(merchant.getReseller());
        t.setStatus(Status.CREATED);
        t.setDateCreated(ZonedDateTime.now());
        t.setPayee(payee);
        t.setExtendedUserRelation(eur);

        log.info(">>>>>> after set transacction {}", t);
        
        if(t.isMustNotifyPayee()==null)t.setMustNotifyPayee(Boolean.TRUE);
        
        return save(t);
    }

    /**
     * Create a transaction for Payees.
     *
     * @param t the entity to create.
     * @return the persisted entity.
     */
    public Transaction createTransactionForPayee(Transaction t) {
        log.debug("Request to create one Transaction : {}", t);

        t.setStatus(Status.CREATED);
        t.setDateCreated(ZonedDateTime.now());
        return save(t);
    }

    /**
     * Create several transactions.
     *
     * @param transactionListDTO the group of entity to create.
     * @return the persisted entity.
     */
    public TransactionListDTO create(TransactionListDTO transactionListDTO) {
        log.debug("Request to create the list of Transactions : {}", transactionListDTO);
        Set<Transaction> result = new HashSet<>();
        Set<Transaction> resultWithErrors = new HashSet<>();
        transactionListDTO.getTransactions().forEach(transaction -> {

            String payeeId = String.valueOf(transaction.getPayee().getId());
            float transactionAmount = transaction.getAmountBeforeCommission().floatValue();
            boolean limited = false;
            log.info("==_====> Transaction payeeId {}", payeeId);
            log.info("==_====> Transaction Amount {}", transactionAmount);
            int maxToProcess = 100;
            Pageable pageable = PageRequest.of(0, maxToProcess, new Sort(Sort.Direction.ASC, "id"));
            Optional<List<Transaction>> optionalAcceptedTransactions = transactionRepository.findAllByPayeeId(Long.parseLong(payeeId));
            List<Transaction> acceptedTransactions = new ArrayList<Transaction>();
            if(optionalAcceptedTransactions.isPresent()){
                acceptedTransactions = optionalAcceptedTransactions.get();

                long totalTransactions = acceptedTransactions.size();
                log.debug("=_===> Total transactions: {}", totalTransactions);
                Optional<ExtendedUser> optionalPayee = extendedUserRepository.findById(Long.parseLong(payeeId));
                ExtendedUser payee = new ExtendedUser();
                Country country = new Country();
                if(optionalPayee.isPresent()){
                    payee = optionalPayee.get();
                    country = payee.getPostalCountry();

                    if(country != null && payee != null){

                        log.debug("=_===> Transaction payee: {}", payee);
                        log.debug("=_===> Transaction country: {}", country.getId());

                        Optional<List<ThresholdPayee>> optionalThresholdList = thresholdPayeeRepository.findAllByCountryId(country.getId());

                        if(optionalThresholdList.isPresent()){

                            List<ThresholdPayee> thresholdList = optionalThresholdList.get();

                            log.debug("=_===> Threshold size: {}", thresholdList.size());

                            if(thresholdList.size() > 0) {

                                ThresholdPayee threshold = thresholdList.get(0);
                                float thresholdAmount = threshold.getThreshold();
                                String thresholdPeriod = threshold.getPeriod();
                                float cummulatedAmount = 0;

                                log.debug("=_===> Threshold amount: {}", thresholdAmount);
                                log.debug("=_===> Threshold period: {}", thresholdPeriod);
                                ZoneId zoneIdMx = ZoneId.of("America/Mexico_City");

                                 Date d = new Date();
                                 int year = d.getYear();
                                 int month = d.getMonth();

                                 ZonedDateTime currentYear = ZonedDateTime.now();;

                                 if(thresholdPeriod.equals("year")){

                                    currentYear = currentYear.withMonth(1);
                                    currentYear = currentYear.withDayOfMonth(1);
                                    currentYear = currentYear.withHour(0);
                                    currentYear = currentYear.withMinute(0);
                                    currentYear = currentYear.withSecond(0);
                                    currentYear = currentYear.withNano(0);

                                 } else if(thresholdPeriod.equals("month")){

                                    currentYear = currentYear.withDayOfMonth(1);
                                    currentYear = currentYear.withHour(0);
                                    currentYear = currentYear.withMinute(0);
                                    currentYear = currentYear.withSecond(0);
                                    currentYear = currentYear.withNano(0);

                                 }

                                 log.debug("=_===> Inf lim Date: {}", currentYear);

                                 for(int i = 0; i < acceptedTransactions.size(); i++){
                                    log.debug("=_===> Transaction Status: {}", acceptedTransactions.get(i).getStatus());
                                    if((acceptedTransactions.get(i).getDateCreated().compareTo(currentYear) > 0)){
                                        log.debug("=_===> Transaction position: {}", i+1);
                                        log.debug("=_===> Transaction ID: {}", acceptedTransactions.get(i).getId());
                                        log.debug("=_===> Transaction date: {}", acceptedTransactions.get(i).getDateCreated());
                                        log.debug("=_===> Transaction amount: {}", acceptedTransactions.get(i).getAmountBeforeCommission().floatValue());
                                        cummulatedAmount += acceptedTransactions.get(i).getAmountBeforeCommission().floatValue();
                                    }
                                }

                                log.debug("=_===> Cummulated amount: {}", cummulatedAmount);

                                if(cummulatedAmount + transactionAmount <= thresholdAmount){
                                    log.debug("==___========> Pago Aprobado ");
                                } else {

                                    limited = true;
                                    transaction.status(Status.CANCELLED);
                                    transaction.statusReason("Limite alcanzado");
                                    resultWithErrors.add(transaction);

                                }

                            }

                        } else{

                            log.debug("=_===> No hay Threshold registrado para el pais: {}", country.getId());

                        }

                    }

                }
            }

            try {
                if(!limited) result.add(create(transaction));
            } catch (BadRequestAlertException brae) {
                transaction.status(Status.CANCELLED);
                transaction.statusReason(brae.getMessage());
                resultWithErrors.add(transaction);
            }
        });
        transactionListDTO.setTransactions(result);
        transactionListDTO.setTransactionsWithErrors(resultWithErrors);
        return transactionListDTO;
    }

    /**
     * Create several payments.
     *
     * @param paymentListDTO the group of entity to create.
     * @return the persisted entity.
     */
    public PaymentListDTO create(PaymentListDTO paymentListDTO) {

        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        log.info(">>>>>> timestamp: {}", timestamp.getTime());

        log.info(">>>>>> {}: Inside of create the list of Payments {}", timestamp.getTime(), paymentListDTO.getPayments());

        List<TransactionForMerchantDTO> result = new ArrayList<>();
        List<TransactionForMerchantDTO> resultWithErrors = new ArrayList<>();

        paymentListDTO.getPayments().forEach(payment -> {

            Transaction tempPayment = TransactionForMerchantDTO.transactionForMerchantToTransaction(payment);

            log.info(">>>>>> {}: after transactionForMerchantToTransaction: {}", timestamp.getTime(), tempPayment);

            try {

                log.info(">>>>>> {}: start processing tx: {}", timestamp.getTime(), tempPayment);

                Transaction createdTransaction = create(tempPayment);

                log.info(">>>>>> {}: created tx to payeeid: {}", timestamp.getTime(), createdTransaction);

                log.info(">>>>>> {}: createdTransaction.getPayee ID: {}", timestamp.getTime(), createdTransaction.getPayee().getUser().getId());

                log.info(">>>>>> {}: createdTransaction.getMerchant ID: {}",  timestamp.getTime(), createdTransaction.getMerchant().getId());

                String nickname = extendedUserRelationsService.getPayeeNickname(createdTransaction.getPayee().getUser().getId(), createdTransaction.getMerchant().getId());

                log.info(">>>>>> {}: nickname : {}", timestamp.getTime(), nickname);

                result.add(new TransactionForMerchantDTO(createdTransaction, nickname));

                log.info(">>>>>> {}: nickname : {}", timestamp.getTime(), result);

            } catch (Exception e ) {

                payment.setStatus(Status.CANCELLED);
                payment.setStatusReason(e.getMessage());
                resultWithErrors.add(payment);
                log.info(">>>>>> {} ERROR payment: {}", timestamp.getTime(), payment);
            }

        });

        log.info(">>>>>> {} after process list of trasactions", timestamp.getTime());
        
        paymentListDTO.setPayments(result);
        paymentListDTO.setPaymentsWithErrors(resultWithErrors);

        return paymentListDTO;
    }

    /**
     * Update several transactions.
     *
     * @param transactionListDTO the group of entity to update.
     * @return the persisted entity.
     */
    public TransactionListDTO update(TransactionListDTO transactionListDTO) {
        log.debug("Request to update the list of Transactions : {}", transactionListDTO);
        Set<Transaction> result = new HashSet<>();
        Set<Transaction> resultWithErrors = new HashSet<>();
        transactionListDTO.getTransactions().forEach(transaction -> {
            try {
                result.add(update(transaction));
            } catch (BadRequestAlertException brae) {
                transaction.status(Status.CANCELLED);
                transaction.statusReason(brae.getMessage());
                resultWithErrors.add(transaction);
            }
        });
        transactionListDTO.setTransactions(result);
        transactionListDTO.setTransactionsWithErrors(resultWithErrors);
        return transactionListDTO;
    }

    /**
     * Update a transaction.
     *
     * @param t the entity to save.
     * @return the persisted entity.
     */
    public Transaction update(Transaction t) {
        log.debug("Request to update Transaction : {}", t);

        Transaction ct = findOne(t.getId()).orElseThrow(() ->
            new NotFoundAlertException(TransactionService.ENTITY_NAME + " id: " + t.getId(), ENTITY_NAME, TransactionService.NOT_FOUND));
        log.debug("Current Transaction : {}", ct);

        ct.setInfoChanged("");
        if (t.getStatus() == null) {
            ct.setMustNotify(false);
            ct.setNotes(t.getNotes());
            return save(ct, false);
        }

        boolean isApiCall = getProcessType(ct).equals(ProcessType.API_CALL);
        log.debug("isApiCall {}", isApiCall);

        if (!validateStatusChange(t.getStatus(), ct.getStatus(), isApiCall)) {
            throw new BadRequestAlertException(
                translateService.getMessageByCurrentUserParamsNull("error.message.exception.invalid.status") + " " + ct.getStatus().toString() + " to " + t.getStatus().toString()
                , ENTITY_NAME, "statusChange");
        }

        boolean mustChangeStatus = (!ct.getStatus().equals(t.getStatus())) && !(allowedMerchants.contains(ct.getMerchant().getId())); //
        ct.setMustNotify(mustChangeStatus);

        log.info("MerchantId"+ct.getMerchant().getId());
        log.info("PostalCountry"+ct.getPayee().getPostalCountry().getId());

        BankAccount bankAccount;
        switch (t.getStatus()) {
            case IN_PROCESS: // only admin (he or the bank) can process a payment
                if (t.getExchangeRate() == null || t.getExchangeRate() <= 0)
                    throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.wrong.exchange.rate"), ENTITY_NAME, "WrongExchangeRate");
                bankAccount = bankAccountService.findOne(t.getBankAccount().getId()).orElseThrow(() ->
                    new NotFoundAlertException(BankAccountService.ENTITY_NAME + " id: " + t.getBankAccount().getId(), ENTITY_NAME, BankAccountService.NOT_FOUND_CODE));

                ct.setExchangeRate(t.getExchangeRate());
                ct.setStatus(t.getStatus());
                ct.setAdmin(extendedUserService.getCurrentExtendedUser());
                ct.setBankAccount(bankAccount);
                ct.setFileName(t.getFileName());
                ct.setTransactionGroup(t.getTransactionGroup());
                if (ct.getMerchant().isUseMerchantCommission() != null && ct.getMerchant().isUseMerchantCommission()) {
                    ct.setBankCommission(ct.getMerchant().getBankCommission());
                    ct.setFxCommission(ct.getMerchant().getFxCommission()+(bankAccount.getIsRemitee()?bankAccount.getFxCommission():0f));
                    ct.setRechargeCost(ct.getMerchant().getRechargeCost());
                } else if (ct.getMerchant().isUseFlatCommission() != null && ct.getMerchant().isUseFlatCommission()) {
                    ct.setBankCommission(0f);
                    ct.setFxCommission(bankAccount.getIsRemitee()?bankAccount.getFxCommission():0f);
                    ct.setRechargeCost(calculateFlatCommission(ct));
                } else {
                    ct.setBankCommission(bankAccount.getBankCommission());
                    ct.setFxCommission(bankAccount.getFxCommission());
                    ct.setRechargeCost(bankAccount.getRechargeCost());
                }
                if (allowedMerchants.contains(ct.getMerchant().getId()) && ct.getPayee().getPostalCountry().getId().equals(2L)) {
                    ct.setAmountAfterCommission(ct.getAmountAfterCommission());
                }else{
                    ct.setAmountAfterCommission(calculateAmountAfterCommission(ct));
                }
                ct.setAmountLocalCurrency(calculateAmountLocalCurrency(ct));
                ct.setProcessType(getProcessType(ct));
                break;
            case FAILED:
                if (t.getStatusReason() == null || t.getStatusReason().equals(""))
                    throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.missing.status.reason"), ENTITY_NAME, "MissingStatusReason");
                ct.setStatusReason(t.getStatusReason());
                ct.setStatus(t.getStatus());
                ct.setAdmin(isApiCall ? ct.getAdmin() : extendedUserService.getCurrentExtendedUser());
                break;
            case CANCELLED: // only merchant can cancel a payment
                if (!mustChangeStatus) return ct;
                if (t.getStatusReason() == null || t.getStatusReason().equals(""))
                    throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.missing.status.reason"), ENTITY_NAME, "MissingStatusReason");
                ct.setStatusReason(t.getStatusReason());
                ct.setMerchant(extendedUserService.getCurrentExtendedUser());
                ct.setStatus(t.getStatus());
                break;
            case REJECTED: // only admin or a provider can reject a payment
                if (t.getStatusReason() == null || t.getStatusReason().equals(""))
                    throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.missing.status.reason"), ENTITY_NAME, "MissingStatusReason");
                ct.setStatusReason(t.getStatusReason());
                ct.setAdmin(isApiCall ? ct.getAdmin() : extendedUserService.getCurrentExtendedUser());
                ct.setBankReference(t.getBankReference());
                ct.setStatus(t.getStatus());
                if( allowedMerchants.contains( ct.getMerchant().getId() )) {
                    if (ct.getPayee().getPostalCountry().getId().equals(2L)) dispersionNotificationV2(ct, Status.REJECTED);
                }
                break;
            case ACCEPTED: // only admin or a provider can accept a payment
                ct.setAdmin(isApiCall ? ct.getAdmin() : extendedUserService.getCurrentExtendedUser());
                ct.setBankReference(t.getBankReference());
                ct.setStatusReason(t.getStatusReason());
                ct.setStatus(t.getStatus());
                ct.setResellerCommission(calculateResellerCommission(ct));
                ct.setResellerFixedCommission(ct.getReseller() != null ? ct.getReseller().getResellerFixedCommission() : null);
                ct.setResellerPercentageCommission(ct.getReseller() != null ? ct.getReseller().getResellerPercentageCommission() : null);
                if( allowedMerchants.contains( ct.getMerchant().getId() )) {
                    if(ct.getPayee().getPostalCountry().getId().equals(2L)) dispersionNotificationV2(ct, Status.ACCEPTED);
                }
                break;
            case REVERTED:
                ct.setAdmin(isApiCall ? ct.getAdmin() : extendedUserService.getCurrentExtendedUser());
                ct.setStatus(t.getStatus());
                ct.setExchangeRate(null);
                ct.setAmountLocalCurrency(null);
                ct.setTransactionGroup(null);
            default:
                break;
        }

        dispersionProcess(ct);
        if(ct.isMustNotifyPayee()==null)ct.setMustNotifyPayee(Boolean.TRUE);
        return save(ct, mustChangeStatus);
    }

    private void dispersionNotificationV2(Transaction t, Status status){
        String notificationUri=yaydooNotification+"/webhooks/pmi";
        String accessToken="WH0S84R-07P42QR-QXSEEA4-SG71SNA";
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime myDateObj = LocalDateTime.now();

        String statusDispersion=status.equals(Status.ACCEPTED) ? "success" : "error";
        DispersionDTO disperionNotify = new DispersionDTO();

        disperionNotify.setTransactionId(t.getDescription());
        disperionNotify.setAmount(t.getAmountAfterCommission());
        disperionNotify.setType("dispersion");
        disperionNotify.setStatus(statusDispersion);
        disperionNotify.setDate(myDateObj.format(myFormatObj));
        disperionNotify.setCurrency(t.getCurrency().getCode());
        disperionNotify.setInternalTransactionId(t.getId());
        disperionNotify.setMessage(t.getStatusReason());

        log.info("DispersionDTO: "+disperionNotify.toString());
        log.info("Yaydoo Endpoint: "+notificationUri);


        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer "+accessToken);
        HttpEntity<DispersionDTO> httpEntity = new HttpEntity<>(disperionNotify, headers);
        ResponseEntity serviceResult =restTemplate.postForEntity(notificationUri,httpEntity,DispersionDTO.class);
        log.info("Codigo de la notificacion" +serviceResult.getStatusCode().toString());
    }


    private void dispersionProcess(Transaction ct){
        String description=ct.getDescription();
        DispersionDTO disp=new DispersionDTO();
        disp.setAmount(ct.getAmountAfterCommission());
        log.debug("DispersionObject: "+disp.toString());


    }

    private ProcessType getProcessType(Transaction ct) {
        log.debug("IS AUTO {}", ct);
        if (ct.getTransactionGroup() != null &&
            ct.getTransactionGroup().getBankAccount() != null &&
            ct.getTransactionGroup().getBankAccount().getBank() != null &&
            ct.getTransactionGroup().getBankAccount().getBank().getId() != null &&
            ct.getTransactionGroup().getBankAccount().getBank().getId().intValue() == 30)
            return ProcessType.API_CALL;
        else return ProcessType.FILE_EXPORT;
    }

    /**
     * Calculate the amount of the reseller commission
     *
     * @param t the transaction.
     * @return the value calculated .
     */
    private BigDecimal calculateResellerCommission(Transaction t) {
        if (t == null || t.getReseller() == null ||
            t.getReseller().getResellerFixedCommission() == null ||
            t.getReseller().getResellerPercentageCommission() == null
        ) return BigDecimal.ZERO;

        return BigDecimal.valueOf(t.getReseller().getResellerFixedCommission() +
            t.getAmountBeforeCommission().floatValue() * t.getReseller().getResellerPercentageCommission()
        );
    }

    /**
     * Calculate the amount after the commissions
     *
     * @param t the transaction.
     * @return the value calculated after commissions.
     */
    private BigDecimal calculateAmountAfterCommission(Transaction t) {
        log.debug("--> calculateAmountAfterCommission: {}", t);
        if(allowedMerchants.contains(t.getMerchant().getId())) return BigDecimal.valueOf(t.getAmountBeforeCommission().floatValue());
        float bankCommission = t.getAmountBeforeCommission().floatValue() * t.getBankCommission();
        float fXCommission = t.getAmountBeforeCommission().floatValue() * t.getFxCommission();
        float afterCommission = t.getAmountBeforeCommission().floatValue()
            - bankCommission - fXCommission - t.getRechargeCost();
        return BigDecimal.valueOf(afterCommission);
    }

    /**
     * Calculate the recharge cost for flat commission
     *
     * @param t the transaction.
     * @return the value calculated for flat commission.
     */
    private float calculateFlatCommission(Transaction t) {
        log.debug("--> calculateFlatCommission: {}", t);
        AtomicReference<Float> commission = new AtomicReference<>(0f);
        AtomicBoolean found = new AtomicBoolean(false);

        flatCommissionService.findAllByMerchant(t.getMerchant().getId()).forEach(flatCommission -> {
            if (!found.get() && t.getAmountBeforeCommission().floatValue() <= flatCommission.getUpTo().floatValue()) {
                commission.set(flatCommission.getCommission());
                found.set(true);
            }
        });
        log.debug("--> calculateFlatCommission: {}", commission.get());
        return commission.get();
    }

    /**
     * Calculate the amount in local currency
     *
     * @param t the transaction.
     * @return the value calculated for local currency.
     */
    private BigDecimal calculateAmountLocalCurrency(Transaction t) {
        log.debug("--> calculateAmountLocalCurrency: {}", t);
        float localCurrency = t.getAmountAfterCommission().floatValue() * t.getExchangeRate();
        return BigDecimal.valueOf(localCurrency);
    }

    /**
     * Get all the transactions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Transaction> findAll(Pageable pageable) {
        log.debug("Request to get all Transactions");
        return transactionRepository.findAll(pageable);
    }

    /**
     * Get one transaction by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Transaction> findOne(Long id) {
        log.debug("Request to get Transaction : {}", id);
        if (id == null) return Optional.empty();
        return transactionRepository.findById(id);
    }

    /**
     * Get one transaction by id and merchantId.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Transaction> findOneByIdAndMerchantId(Long id, Long merchantId) {
        log.debug("findOneByIdAndMerchantId - Request to get Transaction : {}", id);
        if (id == null || merchantId == null) return Optional.empty();
        return transactionRepository.findOneByIdAndMerchantId(id, merchantId);
    }

    /**
     * Get all transactions by description
     *
     * @param description the description of the entity.
     * @return the Transaction List.
     */
    @Transactional(readOnly = true)
    public TransactionListDTO findAllByDescription(String description) {
        
        log.debug("findAllByDescription - Request to get Transaction : {}", description);

        if (description == null) return null;

        TransactionListDTO response=new TransactionListDTO();
        List<Transaction> t = transactionRepository.findByDescription(description).orElse(new ArrayList<>());

        log.info("findAllByDescription - result: {} ", t);

        Set<Transaction> transactionsFound = new HashSet<Transaction>(t);
        response.setTransactions(transactionsFound);
        
        log.info("findAllByDescription: {}", response.getTransactions());

        return response;
    }

    /**
     * Get one transaction by id and resellerId.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Transaction> findOneByIdAndResellerId(Long id, Long resellerId) {
        log.debug("Request to get Transaction : {}", id);
        if (id == null || resellerId == null) return Optional.empty();
        return transactionRepository.findOneByIdAndResellerId(id, resellerId);
    }

    /**
     * Delete the transaction by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Transaction : {}", id);
        transactionRepository.deleteById(id);
        transactionSearchRepository.deleteById(id);
    }

    /**
     * Search for the transaction corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Transaction> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Transactions for query {}", query);
        return transactionSearchRepository.search(queryStringQuery(query), pageable);
    }

    @Transactional
    public void saveAll(List<Transaction> transactionList) {
        if (!transactionList.isEmpty())
            transactionSearchRepository.saveAll(transactionList);
    }

    boolean validateStatusChange(Status newStatus, Status currentStatus) {
        return validateStatusChange(newStatus, currentStatus, false);
    }

    /**
     * Validate the new status.
     *
     * @param newStatus status to validate.
     * @param currentStatus status of the current entity to validate.
     * @return boolean value according to validation.
     */
    private boolean validateStatusChange(Status newStatus, Status currentStatus, boolean isApiCall) {
        if (newStatus == null) return false;
        if (newStatus == currentStatus) return true;
        switch (currentStatus) {
            case CREATED:
                if (newStatus == Status.CANCELLED && !extendedUserService.isMerchant())
                    throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.current.not.perform.action"), ENTITY_NAME, "WrongUserRole");
                if ((newStatus == Status.IN_PROCESS || newStatus == Status.REJECTED) && !extendedUserService.isAdmin(true))
                    throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.current.not.perform.action"), ENTITY_NAME, "WrongUserRole");
                return newStatus == Status.IN_PROCESS || newStatus == Status.CANCELLED || newStatus == Status.REJECTED || newStatus == Status.FAILED;
            case FAILED:
                if (!extendedUserService.isAdmin(true))
                    throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.current.not.perform.action"), ENTITY_NAME, "WrongUserRole");
                return newStatus == Status.IN_PROCESS || newStatus == Status.REJECTED;
            case IN_PROCESS:
                if (!isApiCall && !extendedUserService.isAdmin(true))
                    throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.current.not.perform.action"), ENTITY_NAME, "WrongUserRole");
                return newStatus == Status.ACCEPTED || newStatus == Status.REJECTED || newStatus == Status.FAILED || newStatus == Status.REVERTED || newStatus == Status.SETTLEMENT_DECLINED;
            case ACCEPTED:
            case REJECTED:
            case CANCELLED:
            default:
                return false;
        }
    }

    @Async
    public void refetch(Long id) {
        log.info(">>>>>>>>>>>>>>>>>>>> Request to refetch transaction for id {}", id);
        // no se estaba guardando en el elastic el update de la trasaction
        refetchService.refetchTransaction(id);
    }

    @Async
    public void refetchAll(Collection<Transaction> elements) {
        elements.forEach(e -> {
            refetch(e.getId());
            extendedUserService.refetch(e.getMerchant().getId());
            if (e.getReseller() != null) extendedUserService.refetch(e.getReseller().getId());
        });
    }

    @Async
    public void refetchAll(List<TransactionForMerchantDTO> elements) {
        elements.forEach(e -> refetch(e.getId()));
    }

    /**
     * Search transaction by query
     *
     * @param query parameter by filter
     * @return iterable transaction
     */
    @Transactional(readOnly = true)
    public Iterable<Transaction> search(String query) {
        log.debug("Request to search for a page of Transactions for query {}", query);
        return transactionSearchRepository.search(queryStringQuery(query));
    }

    public Transaction updateTransaction(Long id, Status status, String statusReason) {
        Transaction transaction = new Transaction();
        transaction.setId(id);
        transaction.setStatus(status);
        transaction.setStatusReason(statusReason);
        Transaction result = null;
        try {
            result = update(transaction);
            log.info("-->> Updated transaction ID: {}, status '{}', statusReason: '{}'", result.getId(), result.getStatus(), result.getStatusReason());
            if (result.getReseller() != null) {
                extendedUserService.refetch(result.getReseller().getId());
                log.debug("Refetched Reseller ID: {} for transaction ID: {}", result.getReseller().getId(), result.getId());
            }
        } catch (BadRequestAlertException e) {
            log.error("!!! -> !!! Transaction NOT updated");
            log.error("!!! -> !!! {}", e.getLocalizedMessage());
        }
        return result;
    }

    public RippleResultGroupDTO getTranstactionQuotes(List<Transaction> transactions, Long idBankAccount) {
       RippleResultGroupDTO rippleResultGroup = new RippleResultGroupDTO();
        Set<RippleResultDTO> result = new HashSet<>();
        Set<RippleResultDTO> resultWithErrors = new HashSet<>();
        transactions.forEach(transaction -> {
            try {
                log.info(">>>>>>>>>>>>>>>> transaction : {}", transaction);
                Optional<Transaction> tx = findOne(transaction.getId());
                 if(tx != null && tx.isPresent()){
                   log.info(">>>>>>>>>>>>>>>> transaction isPresent");
                     // ***************************** Construccion de peticion
                     
                   String rippleRequest = createRippleQuoteRequest(tx.get(),idBankAccount);
                    // ***************************** Envio de la petición a servicio Ripple
                   RippleResultDTO rippleResult = rippleRequest!=null?rippleQuoteService(rippleRequest):rippleQuoteError(tx.get().getId());
                   
                   log.info(">>>>>>>>>>>>>>>> rippleResult : {}", rippleResult);
                   log.info(">>>>>>>>>>>>>>>> rippleResult.getStatus() : {}", rippleResult.getStatus());
                       
                      if("OK".equals(rippleResult.getStatus())){
                            result.add(rippleResult);
                        }
                        else{
                            resultWithErrors.add(rippleResult);
                        }   
                                      
                 }else{
                     log.info(">>>>>>>>>>>>>>>> no hay transacciones");
                 }
                
            } catch (BadRequestAlertException brae) {
                transaction.status(Status.CANCELLED);
                transaction.statusReason(brae.getMessage());
                // resultWithErrors.add(transaction);
            }
        });
        rippleResultGroup.setRippleResults(result);
        rippleResultGroup.setRippleResultsWithErrors(resultWithErrors);
        return rippleResultGroup;
    }

    private String createRippleQuoteRequest(Transaction transaction, Long idBankAccount) {
        log.info(">>>>>>>>>>>>>>>> createRippleQuoteRequest");
        String payeepmi_countrycode = transaction.getPayee().getResidenceCountry().getCode() != null ? transaction.getPayee().getResidenceCountry().getCode() : transaction.getPayee().getPostalCity().getCountry().getCode();
         if (payeepmi_countrycode == null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.country.incorrect.id"), ENTITY_NAME, "MissingCountryCode");
        }
       
        
        Map<String, String> countryCodesMap = new HashMap<>();
        
        countryCodesMap.put("ARG", "AR");
        countryCodesMap.put("BRA", "BR");
        countryCodesMap.put("CHL", "CL");
        countryCodesMap.put("PER", "PE");
        countryCodesMap.put("VEN", "VE");
        
        String currencyCode = countryCodesMap.get(payeepmi_countrycode);
        
        if("BR".equals(currencyCode)||"CL".equals(currencyCode)||"PE".equals(currencyCode)){
             long idDirectPaymentBank = extendedUserService.findOne(transaction.getPayee().getId()).get().getDirectPaymentBank().getId();
             DirectPaymentBank directPaymentBanck = directPaymentBankService.findOneByBank(idDirectPaymentBank);
             if(directPaymentBanck.getDestinyBankBicfi()==null||"".equals(directPaymentBanck.getDestinyBankBicfi())){
                 return null;
             }
         }
        
        setAmountAfterComissionQuote(transaction, idBankAccount);
        JsonObject jsonRipple = new JsonObject();
        jsonRipple.addProperty("TransactionId",transaction.getId());
        jsonRipple.addProperty("Amount",transaction.getAmountAfterCommission());
        jsonRipple.addProperty("Country",currencyCode);
        
         
        
        
        
        return jsonRipple.toString();
    }

    private RippleResultDTO rippleQuoteService(String rippleRequest) {
        log.info(">>>>>>>>>>>>>>>> rippleService");

        RippleResultDTO rippleResult=new RippleResultDTO();
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
            @Override
            public boolean hasError(HttpStatus statusCode) {
                log.info("statusCode: {} ", statusCode);
                return false;
            }
        });

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("client_id", rippleClientId);
        headers.set("client_secret", rippleClientSecret);

        String uri = rippleURL + "/v1/payment/quote";

        try {

            final URI endpoint = new URI(uri);
            final HttpEntity<String> httpEntity = new HttpEntity<String>(rippleRequest, headers);

            log.info("REQUEST: " + httpEntity.toString());
            log.info("URL: " + endpoint.toString());

            ResponseEntity<RippleResultDTO> serviceResult = restTemplate.postForEntity(endpoint, httpEntity, RippleResultDTO.class);

            log.info("serviceResult: {}", serviceResult);
            log.info("Charge HTTP CODE: {}", serviceResult.getStatusCode());

            // if(!serviceResult.getStatusCode().equals(HttpStatus.CREATED)) {
            //     throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.exist"), "USER", "id");
            // }

            rippleResult = serviceResult.getBody();

        }catch(Exception e){
            log.info("ERROR: " + e.toString());
        }

        return rippleResult;
    }

    private void setAmountAfterComissionQuote(Transaction transaction, Long idBankAccount) {
        
       BankAccount bankAccount = bankAccountService.findOne(idBankAccount).orElseThrow(() ->
                    new NotFoundAlertException(BankAccountService.ENTITY_NAME + " id: " + transaction.getBankAccount().getId(), ENTITY_NAME, BankAccountService.NOT_FOUND_CODE));
        
       if (transaction.getMerchant().isUseMerchantCommission() != null && transaction.getMerchant().isUseMerchantCommission()) {
                    transaction.setBankCommission(transaction.getMerchant().getBankCommission());
                    transaction.setFxCommission(transaction.getMerchant().getFxCommission()+(bankAccount.getIsRemitee()?bankAccount.getFxCommission():0f));
                    transaction.setRechargeCost(transaction.getMerchant().getRechargeCost());
                } else if (transaction.getMerchant().isUseFlatCommission() != null && transaction.getMerchant().isUseFlatCommission()) {
                    transaction.setBankCommission(0f);
                    transaction.setFxCommission(bankAccount.getIsRemitee()?bankAccount.getFxCommission():0f);
                    transaction.setRechargeCost(calculateFlatCommission(transaction));
                } else {
                    transaction.setBankCommission(bankAccount.getBankCommission());
                    transaction.setFxCommission(bankAccount.getFxCommission());
                    transaction.setRechargeCost(bankAccount.getRechargeCost());
                }
                if (allowedMerchants.contains(transaction.getMerchant().getId()) && transaction.getPayee().getPostalCountry().getId().equals(2L)) {
                    transaction.setAmountAfterCommission(transaction.getAmountAfterCommission());
                }else{
                    transaction.setAmountAfterCommission(calculateAmountAfterCommission(transaction));
                }
    }
    
    private RippleResultDTO rippleQuoteError(Long id) {
        RippleResultDTO rippleResult=new RippleResultDTO();
        rippleResult.setStatus("ERROR");
        rippleResult.setTransaction_id(String.valueOf(id));
        rippleResult.setMessage("");
        rippleResult.setRate("0");
        
        return rippleResult;
    }
}
