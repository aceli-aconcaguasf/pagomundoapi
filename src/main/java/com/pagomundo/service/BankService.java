package com.pagomundo.service;

import com.pagomundo.domain.Bank;
import com.pagomundo.repository.BankRepository;
import com.pagomundo.repository.search.BankSearchRepository;
import com.pagomundo.repository.search.RefetchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link Bank}.
 */
@Service
@Transactional
public class BankService {
    public static final String ENTITY_NAME = "Bank";
    public static final String NOT_FOUND_CODE = ENTITY_NAME + ".001";
    public static final String SHOULD_BE_NULL_CODE = ENTITY_NAME + ".002";
    public static final String SHOULD_NOT_BE_NULL_CODE = ENTITY_NAME + ".003";

    private final Logger log = LoggerFactory.getLogger(BankService.class);

    private final BankRepository bankRepository;
    private final BankSearchRepository bankSearchRepository;
    private final RefetchService refetchService;

    public BankService(BankRepository bankRepository, BankSearchRepository bankSearchRepository, RefetchService refetchService) {
        this.bankRepository = bankRepository;
        this.bankSearchRepository = bankSearchRepository;
        this.refetchService = refetchService;
    }

    /**
     * Save a bank.
     *
     * @param bank the entity to save.
     * @return the persisted entity.
     */
    public Bank save(Bank bank) {
        log.debug("Request to save Bank : {}", bank);
        Bank result = bankRepository.save(bank);
        bankSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the banks.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Bank> findAll(Pageable pageable) {
        log.debug("Request to get all Banks");
        return bankRepository.findAll(pageable);
    }


    /**
     * Get one bank by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Bank> findOne(Long id) {
        log.debug("Request to get Bank : {}", id);
        if (id == null) return Optional.empty();
        return bankRepository.findById(id);
    }

    /**
     * Delete the bank by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Bank : {}", id);
        bankRepository.deleteById(id);
        bankSearchRepository.deleteById(id);
    }

    /**
     * Search for the bank corresponding to the query.
     *
     * @param query    the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Bank> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Banks for query {}", query);
        return bankSearchRepository.search(queryStringQuery(query), pageable);
    }

    @Transactional
    public void saveAll(List<Bank> bankList) {
        if (!bankList.isEmpty())
            bankSearchRepository.saveAll(bankList);
    }

    @Async
    public void refetch(Long id) {
        refetchService.refetchBank(id);
    }

    @Async
    public void refetchAll(Collection<Bank> elements) {
        elements.forEach(e -> refetch(e.getId()));
    }
}
