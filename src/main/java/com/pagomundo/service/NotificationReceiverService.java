package com.pagomundo.service;

import com.cronutils.descriptor.CronDescriptor;
import com.cronutils.model.definition.CronDefinitionBuilder;
import com.cronutils.parser.CronParser;
import com.pagomundo.domain.DTO.NotificationReceiverDTO;
import com.pagomundo.domain.DTO.TransactionForMerchantDTO;
import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.ExtendedUserFunds;
import com.pagomundo.domain.Notification;
import com.pagomundo.domain.NotificationReceiver;
import com.pagomundo.domain.Transaction;
import com.pagomundo.domain.User;
import com.pagomundo.domain.enumeration.FundType;
import com.pagomundo.domain.enumeration.NotificationCategory;
import com.pagomundo.domain.enumeration.NotificationSubCategory;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.repository.NotificationReceiverRepository;
import com.pagomundo.repository.search.NotificationReceiverSearchRepository;
import com.pagomundo.repository.search.RefetchService;
import com.pagomundo.security.AuthoritiesConstants;
import com.pagomundo.service.dto.EmailChangeDTO;
import com.pagomundo.service.dto.UserDTO;
import com.pagomundo.web.rest.errors.NotFoundAlertException;
import com.pagomundo.web.rest.errors.UserNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import static com.cronutils.model.CronType.QUARTZ;
import static com.pagomundo.security.AuthoritiesConstants.MERCHANT;
import static com.pagomundo.security.AuthoritiesConstants.RESELLER;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link NotificationReceiver}.
 */
@Service
@Transactional
public class NotificationReceiverService {
    public static final String ENTITY_NAME = "notificationReceiver";
    public static final String NOT_FOUND = ENTITY_NAME + ".001";

    private static final String SEND_NOTIFICATION_CRON_EXPRESSION = "*/10 * * * * ?";
    private static final String SEND_NOTIFICATION_CRON_DESCRIPTION = CronDescriptor.instance(Locale.UK).describe(new CronParser(CronDefinitionBuilder.instanceDefinitionFor(QUARTZ)).parse(SEND_NOTIFICATION_CRON_EXPRESSION));

    private static final String DELETE_CANCELLED_AND_SENDER_NOT_NULL_NOTIFICATION_CRON_EXPRESSION = "0 0 0 * * ?";
    private static final String DELETE_CANCELLED_AND_SENDER_NOT_NULL_NOTIFICATION_CRON_DESCRIPTION = CronDescriptor.instance(Locale.UK).describe(new CronParser(CronDefinitionBuilder.instanceDefinitionFor(QUARTZ)).parse(DELETE_CANCELLED_AND_SENDER_NOT_NULL_NOTIFICATION_CRON_EXPRESSION));

    private final Logger log = LoggerFactory.getLogger(NotificationReceiverService.class);
    private final NotificationReceiverRepository notificationReceiverRepository;
    private final NotificationReceiverSearchRepository notificationReceiverSearchRepository;
    private final ExtendedUserService extendedUserService;
    private final ExtendedUserFundsService extendedUserFundsService;
    private final UserService userService;
    private final NotificationService notificationService;
    private final MailService mailService;
    private final TransactionService transactionService;
    private final RefetchService refetchService;
    private final MessageSource messageSource;
    private final TranslateService translateService;

    @Value("${notification.sending-email.max-retries}")
    private Integer generalMaxRetries;

    @Value("${merchantWithEUR-userId}")
    private Long merchantWithEUR;

    public NotificationReceiverService(
        NotificationReceiverRepository notificationReceiverRepository,
        NotificationReceiverSearchRepository notificationReceiverSearchRepository,
        ExtendedUserService extendedUserService,
        ExtendedUserFundsService extendedUserFundsService,
        UserService userService,
        NotificationService notificationService,
        MailService mailService, TransactionService transactionService, RefetchService refetchService, MessageSource messageSource, TranslateService translateService) {
        this.notificationReceiverRepository = notificationReceiverRepository;
        this.notificationReceiverSearchRepository = notificationReceiverSearchRepository;
        this.extendedUserService = extendedUserService;
        this.extendedUserFundsService = extendedUserFundsService;
        this.userService = userService;
        this.notificationService = notificationService;
        this.mailService = mailService;
        this.transactionService = transactionService;
        this.refetchService = refetchService;
        this.messageSource = messageSource;
        this.translateService = translateService;
    }

    /**
     * Save a notificationReceiver.
     *
     * @param notificationReceiver the entity to save.
     * @return the persisted entity.
     */
    public NotificationReceiver save(NotificationReceiver notificationReceiver) {
        
        log.info(">>>>>>>>>>>>>>>>>>> Request to save NotificationReceiver : {}", notificationReceiver);

        notificationReceiver.setLastUpdated(ZonedDateTime.now());
        
        NotificationReceiver result = notificationReceiverRepository.save(notificationReceiver);

        log.info(">>>>>>>>>>>>>>>>>>> result : {}", result);

        notificationReceiverSearchRepository.save(result);

        log.info(">>>>>>>>>>>>>>>>>>> after save result on ESS: {}", result);

        return result;
    }

    /**
     * Update a notificationReceiver.
     *
     * @param nr the entity to update.
     * @return the persisted entity.
     */
    public NotificationReceiver update(NotificationReceiver nr) {
        log.debug("Request to update NotificationReceiver : {}", nr);
        NotificationReceiver cnr = findOne(nr.getId()).orElseThrow(() ->
            new NotFoundAlertException(NotificationReceiverService.ENTITY_NAME + " id: " + nr.getId(),
                ENTITY_NAME, NotificationReceiverService.NOT_FOUND));
        log.debug("Current NotificationReceiver : {}", cnr);

        if (nr.getStatus() != null) cnr.setStatus(nr.getStatus());
        if (nr.isMustSendEmail() != null) {
            cnr.setMustSendEmail(nr.isMustSendEmail());
            cnr.setMaxRetries(getMaxRetries());

            Integer retries = nr.getRetries() != null ? nr.getRetries() : 0;
            retries = retries > cnr.getMaxRetries() ? cnr.getMaxRetries() : retries;

            cnr.setRetries(retries);
            cnr.setEmailSent(false);
            if (cnr.getReceiver() != null) {
                cnr.setEmailTitleKey(getEmailTitleKey(cnr.getNotification(), cnr.getReceiver().getUser()));
                cnr.setEmailTemplate(getEmailTemplate(cnr.getNotification(), cnr.getReceiver().getUser()));
                cnr.setMustSendEmail(mustSendEmail(cnr.getNotification(), cnr.getReceiver().getUser()));
            } else {
                cnr.setEmailTitleKey(getEmailTitleKey(cnr.getNotification(), cnr.getUserReceiver()));
                cnr.setEmailTemplate(getEmailTemplate(cnr.getNotification(), cnr.getUserReceiver()));
                cnr.setMustSendEmail(mustSendEmail(cnr.getNotification(), cnr.getUserReceiver()));
            }
        }

        return save(cnr);
    }

    /**
     * Create a NotificationReceiver.
     *
     * @param nrDTO the entity to create.
     * @return the persisted entity.
     */
    public Notification create(NotificationReceiverDTO nrDTO) {

        log.info(">>>>>>>>>>>>>> inside function create : {}", nrDTO);

        if (nrDTO == null) return null;

        // se agrega validacion de getNotification null
        if (nrDTO.getNotification() == null){
            log.info(">>>>>>>>>>>>>> getNotification null");
            return null;
        }

        Notification n = notificationService.create(nrDTO.getNotification());

        if (n == null) return null;

        log.info(">>>>>>>>>>>>>>>>> after n null validation: {}" , n);

        nrDTO.setNotification(n);

        log.info(">>>>>>>>>>>>>>>>>>> after set notification");

        List<ExtendedUser> receivers = getReceivers(nrDTO);
        List<User> userReceivers = getUserReceivers(nrDTO);
        if (userReceivers.isEmpty() && receivers.isEmpty()) return null;

        log.info(">>>>>>>>>>>>>>>>>>> after userReceivers empty validation");

        receivers.forEach(receiver -> {
            
            ExtendedUser eu = extendedUserService.findOne(receiver.getId()).orElse(null);

            if (eu == null) return;

            log.info(">>>>>>>>>>>>>>>>>>> after eu null validation");

            if (n.getSender() != null && eu.getId().equals(n.getSender().getId())
                && NotificationSubCategory.DOWNLOADED_FILE.equals(n.getSubCategory())) return;

            log.info(">>>>>>>>>>>>>>>>>>> after validations receivers");

            NotificationReceiver receiverNR = getNotificationReceiver(n);

            receiverNR.setReceiver(eu);

            Notification notification = getSubjectAndDescription(n, eu.getUser());

            log.info(">>>>>>>>>>>>>>>>>>> notification {}", notification);

            receiverNR.setSubject(notification.getSubject());
            receiverNR.setDescription(notification.getDescription());
            receiverNR.setEmailTitleKey(getEmailTitleKey(n, eu.getUser()));
            receiverNR.setEmailTemplate(getEmailTemplate(n, eu.getUser()));
            receiverNR.setMustSendEmail(mustSendEmail(n, eu.getUser()));

            save(receiverNR);

        });


        log.info(">>>>>>>>>>>>>>>>>>> after save notifications for receivers.forEach");

        userReceivers.forEach(userReceiver -> {

            User user = userService.findOne(userReceiver.getId()).orElse(null);
            if (user == null) return;

            NotificationReceiver receiverNR = getNotificationReceiver(n);
            receiverNR.setUserReceiver(user);

            Notification notification = getSubjectAndDescription(n, user);
            receiverNR.setSubject(notification.getSubject());
            receiverNR.setDescription(notification.getDescription());
            receiverNR.setEmailTitleKey(getEmailTitleKey(n, user));
            receiverNR.setEmailTemplate(getEmailTemplate(n, user));
            receiverNR.setMustSendEmail(mustSendEmail(n, user));
            save(receiverNR);

        });

        log.info(">>>>>>>>>>>>>>>>>>> after save notifications for userReceivers.forEach");

        if (n.getSender() == null) return n;

        NotificationReceiver senderNR = getNotificationReceiver(n);
        senderNR.setSender(n.getSender());
        Notification notification = getSubjectAndDescription(n, n.getSender().getUser());
        senderNR.setSubject(notification.getSubject());
        senderNR.setDescription(notification.getDescription());
        senderNR.setStatus(getStatus(n));
        senderNR.mustSendEmail(false);
        senderNR.setEmailTitleKey(null);
        senderNR.setEmailTemplate(null);
        save(senderNR);

        log.info(">>>>>>>>>>>>>>>>>>> after save notifications for senderNR.forEach");

        return n;
    }

    private NotificationReceiver getNotificationReceiver(Notification n) {
        NotificationReceiver notificationReceiver = new NotificationReceiver();
        notificationReceiver.setNotification(n);
        notificationReceiver.setDateCreated(n.getDateCreated());
        notificationReceiver.setStatus(Status.CREATED);
        notificationReceiver.setReason("");
        notificationReceiver.setMustSendEmail(false);
        notificationReceiver.setEmailSent(false);
        notificationReceiver.setRetries(0);
        notificationReceiver.setMaxRetries(getMaxRetries());
        notificationReceiver.setEmailTitleKey(null);
        notificationReceiver.setEmailTemplate(null);
        notificationReceiver.setSender(null);
        notificationReceiver.setReceiver(null);
        notificationReceiver.setUserReceiver(null);
        return notificationReceiver;
    }

    private Status getStatus(Notification n) {
        switch (n.getCategory()) {
            case DOWNLOADED_FILE:
                return Status.CREATED;
            case ISSUE:
            case MESSAGE:
                return Status.ACCEPTED;
            case TRANSACTION:
            case FUND:
            case EXTENDED_USER:
            case USER_ACCOUNT:
            case WITHDRAWAL:
            case RELATION:
            default:
                return Status.CANCELLED;
        }
    }

    private List<ExtendedUser> getReceivers(NotificationReceiverDTO nrDTO) {
        switch (nrDTO.getNotification().getCategory()) {
            case DOWNLOADED_FILE:
                return nrDTO.getReceivers();
            case ISSUE:
                if (nrDTO.getNotification().getSubCategory().equals(NotificationSubCategory.ISSUE_CREATED))
                    return extendedUserService.getAllActiveAdmins();
                return Collections.emptyList();
            case TRANSACTION:
                switch (nrDTO.getNotification().getSubCategory()) {
                    case TRANSACTION_CREATED:
                    case TRANSACTION_SETTLEMENT_DECLINED:
                        return extendedUserService.getAllActiveAdmins();
                    case TRANSACTION_ACCEPTED:
                    case TRANSACTION_REJECTED:
                        return nrDTO.getReceivers();
                    case TRANSACTION_IN_PROCESS:
                    case TRANSACTION_CANCELLED:
                    default:
                        return Collections.emptyList();
                }
            case FUND:
                switch (nrDTO.getNotification().getSubCategory()) {
                    case FUND_CREATED:
                        return extendedUserService.getAllActiveAdmins();
                    case FUND_ACCEPTED:
                    case FUND_REJECTED:
                        return nrDTO.getReceivers();
                    default:
                        return Collections.emptyList();
                }
            case WITHDRAWAL:
                switch (nrDTO.getNotification().getSubCategory()) {
                    case WITHDRAWAL_CREATED:
                        return extendedUserService.getAllActiveSuperAdmins();
                    case WITHDRAWAL_ACCEPTED:
                    case WITHDRAWAL_REJECTED:
                        return nrDTO.getReceivers();
                    default:
                        return Collections.emptyList();
                }
            case EXTENDED_USER:
                switch (nrDTO.getNotification().getSubCategory()) {
                    case EXTENDED_USER_CREATED:
                        return extendedUserService.getAllActiveAdmins();
                    case EXTENDED_USER_ACCEPTED:
                    case EXTENDED_USER_REJECTED:
                    case EXTENDED_USER_CANCELLED:
                    case EXTENDED_USER_CHANGE_PROFILE:
                        return nrDTO.getReceivers();
                    case EXTENDED_USER_IN_PROCESS:
                    default:
                        return Collections.emptyList();
                }
            case MESSAGE:
                switch (nrDTO.getNotification().getReceiverGroup()) {
                    case ALL_ADMINS:
                        return extendedUserService.getAllActiveAdmins();
                    case ALL_RESELLERS:
                        return extendedUserService.getAllActiveResellers();
                    case ALL_MERCHANTS:
                        return extendedUserService.getAllActiveMerchants();
                    case ALL_PAYEES:
                        return extendedUserService.getAllActivePayees();
                    case ALL_MERCHANTS_FROM_RESELLER:
                        if (nrDTO.getNotification().getExtendedUser() != null)
                            return extendedUserService.getAllActiveMerchantsFromReseller(
                                nrDTO.getNotification().getExtendedUser().getId());
                        else return Collections.emptyList();
                    case ALL_MERCHANTS_FROM_PAYEE:
                        if (nrDTO.getNotification().getExtendedUser() != null)
                            return extendedUserService.getAllActiveMerchantsFromPayee(
                                nrDTO.getNotification().getExtendedUser().getId());
                        else return Collections.emptyList();
                    case ALL_PAYEES_FROM_MERCHANT:
                        if (nrDTO.getNotification().getExtendedUser() != null)
                            return extendedUserService.getAllActivePayeesFromMerchant(
                                nrDTO.getNotification().getExtendedUser().getId());
                        else return Collections.emptyList();
                    case RECEIVERS:
                    default:
                        return nrDTO.getReceivers();
                }
            case USER_ACCOUNT:
            default:
                return Collections.emptyList();
        }
    }

    private List<User> getUserReceivers(NotificationReceiverDTO nrDTO) {
        if (!nrDTO.getNotification().getCategory().equals(NotificationCategory.USER_ACCOUNT))
            return Collections.emptyList();
        switch (nrDTO.getNotification().getSubCategory()) {
            case USER_CREATED:
            case USER_FORGOT_PASSWORD:
            case USER_FORGOT_PASSWORD_NEW_ACCOUNT:
            case USER_EMAIL_CHANGE:
            case USER_EMAIL_CHANGE_CONFIRMATION:
                return nrDTO.getUserReceivers();
            default:
                return Collections.emptyList();
        }
    }

    public boolean mustSendEmail(Notification n, User user) {
        log.debug("MustSendEmail {}",n);
        log.debug("   ---------- {}",n.getSubCategory());
        switch (n.getSubCategory() != null ? n.getSubCategory() : NotificationSubCategory.MESSAGE) {
            case EXTENDED_USER_REJECTED:
            case EXTENDED_USER_ACCEPTED:
            case EXTENDED_USER_CHANGE_PROFILE:
            case TRANSACTION_ACCEPTED:
                return (userService.isPayee(user));

            case TRANSACTION_CREATED:
            	return false; // Se evita envio de email para admins de la plataforma par aaviso de transacciones
                //return (userService.isAdminOrSuperAdmin(user));
            case TRANSACTION_SETTLEMENT_DECLINED:
                return (userService.isAdminOrSuperAdmin(user));
            case USER_CREATED:
            case USER_FORGOT_PASSWORD:
            case USER_FORGOT_PASSWORD_NEW_ACCOUNT:
                return (userService.isMerchant(user) || userService.isPayee(user)|| userService.isAdmin(user)|| userService.isReseller(user));

            case TRANSACTION_REJECTED:
            case USER_EMAIL_CHANGE:
            case USER_EMAIL_CHANGE_CONFIRMATION:
            case EXTENDED_USER_CANCELLED:
            case EXTENDED_USER_CREATED:
            case FUND_CREATED:
            case WITHDRAWAL_CREATED:
            case ISSUE_CREATED:
            	return (userService.isMerchant(user) || userService.isPayee(user));
            	//return true;

            case ISSUE_IN_PROCESS:
            case ISSUE_ACCEPTED:
            case ISSUE_REJECTED:
            case ISSUE_CANCELLED:
            case EXTENDED_USER_IN_PROCESS:
            case TRANSACTION_IN_PROCESS:
            case TRANSACTION_CANCELLED:
            case FUND_ACCEPTED:
            case FUND_REJECTED:
            case FUND_CANCELLED:
            case WITHDRAWAL_ACCEPTED:
            case WITHDRAWAL_REJECTED:
            case WITHDRAWAL_CANCELLED:
            case MESSAGE:
            case DOWNLOADED_FILE:
            default:
                return false;
        }
    }

    private String getEmailTemplate(Notification n, User user) {
        switch (n.getSubCategory() != null ? n.getSubCategory() : NotificationSubCategory.MESSAGE) {
            case USER_CREATED:
                if (userService.isAdmin(user)) return "mail/activationEmailToAdmin";
                if (userService.isReseller(user)) return "mail/activationEmailToMerchant";
                if (userService.isMerchant(user)) return "mail/activationEmailToMerchant";
                if (userService.isPayee(user)) return "mail/activationEmailToPayee";
                return "";
            case USER_FORGOT_PASSWORD:
                return "mail/passwordResetEmail.html";
            case USER_FORGOT_PASSWORD_NEW_ACCOUNT:
                return "mail/passwordResetEmailNewAccount.html";
            case USER_EMAIL_CHANGE:
                return "mail/userEmailChange.html";
            case USER_EMAIL_CHANGE_CONFIRMATION:
                return "mail/userEmailChangeConfirmation.html";
            case EXTENDED_USER_ACCEPTED:
                if (userService.isPayee(user)) return "mail/userApprovedEmailToPayee";
                return "";
            case EXTENDED_USER_REJECTED:
                if (userService.isPayee(user)) return "mail/userRejectedEmailToPayee";
                return "";
            case EXTENDED_USER_CANCELLED:
                if (userService.isAdmin(user)) return "mail/userCancelledEmailToAdmin";
                if (userService.isReseller(user)) return "mail/userCancelledEmailToMerchant";
                if (userService.isMerchant(user)) return "mail/userCancelledEmailToMerchant";
                if (userService.isPayee(user)) return "mail/userCancelledEmailToPayee";
                return "";
            case EXTENDED_USER_CHANGE_PROFILE:
                if (userService.isPayee(user)) return "mail/userChangePaymentMethodEmailToPayee";
                return "";
            case TRANSACTION_ACCEPTED:
                if (userService.isPayee(user)) return "mail/paymentCompletedEmailToUser";
                return "";
            case TRANSACTION_REJECTED:
                if (userService.isMerchant(user) || userService.isPayee(user))
                    return "mail/paymentRejectedEmailToMerchant";
                return "";
            case EXTENDED_USER_CREATED:
                if (userService.isAdminOrSuperAdmin(user)) return "mail/extendedUserCreatedEmailToAdmin";
                return "";
            case FUND_CREATED:
                if (userService.isAdminOrSuperAdmin(user)) return "mail/fundCreatedEmailToAdmin";
                return "";
            case WITHDRAWAL_CREATED:
                if (userService.isAdminOrSuperAdmin(user)) return "mail/withdrawalCreatedEmailToAdmin";
                return "";

            case EXTENDED_USER_IN_PROCESS:
            case TRANSACTION_CREATED:
            case TRANSACTION_SETTLEMENT_DECLINED:
            case TRANSACTION_IN_PROCESS:
            case TRANSACTION_CANCELLED:
            case FUND_ACCEPTED:
            case FUND_REJECTED:
            case FUND_CANCELLED:
            case WITHDRAWAL_ACCEPTED:
            case WITHDRAWAL_REJECTED:
            case WITHDRAWAL_CANCELLED:
            case ISSUE_CREATED:
            case ISSUE_IN_PROCESS:
            case ISSUE_ACCEPTED:
            case ISSUE_REJECTED:
            case ISSUE_CANCELLED:
            case MESSAGE:
            case DOWNLOADED_FILE:
            default:
                return "";
        }
    }

    private String getEmailTitleKey(Notification n, User user) {
        switch (n.getSubCategory() != null ? n.getSubCategory() : NotificationSubCategory.MESSAGE) {
            case USER_CREATED:
                return "email.activation.title";
            case USER_FORGOT_PASSWORD:
                return "email.reset.title";
            case USER_FORGOT_PASSWORD_NEW_ACCOUNT:
                return "email.reset.new.account.title";
            case USER_EMAIL_CHANGE:
                return "email.user.change.title";
            case USER_EMAIL_CHANGE_CONFIRMATION:
                return "email.user.change.confirmation.title";
            case EXTENDED_USER_ACCEPTED:
                if (userService.isPayee(user)) return "email.user.approved.title";
                return "";
            case EXTENDED_USER_REJECTED:
                if (userService.isPayee(user)) return "email.user.rejected.title";
                return "";
            case EXTENDED_USER_CANCELLED:
                return "email.user.cancelled.title";
            case EXTENDED_USER_CHANGE_PROFILE:
                if (userService.isPayee(user)) return "email.profile.changed.title";
                return "";
            case TRANSACTION_ACCEPTED:
                if (userService.isPayee(user)) return "email.payment.completed.title";
                return "";
            case TRANSACTION_REJECTED:
                if (userService.isMerchant(user) || userService.isPayee(user)) return "email.payment.rejected.title";
                return "";
             case EXTENDED_USER_CREATED:
                if (userService.isAdminOrSuperAdmin(user)) return "email.extendedUser.created.title";
                return "";
            case FUND_CREATED:
                if (userService.isAdminOrSuperAdmin(user)) return "email.fund.created.title";
                return "";
            case WITHDRAWAL_CREATED:
                if (userService.isAdminOrSuperAdmin(user)) return "email.withdrawal.created.title";
                return "";

            case EXTENDED_USER_IN_PROCESS:
            case TRANSACTION_CREATED:
            case TRANSACTION_SETTLEMENT_DECLINED:
            case TRANSACTION_IN_PROCESS:
            case TRANSACTION_CANCELLED:
            case FUND_ACCEPTED:
            case FUND_REJECTED:
            case FUND_CANCELLED:
            case WITHDRAWAL_ACCEPTED:
            case WITHDRAWAL_REJECTED:
            case WITHDRAWAL_CANCELLED:
            case ISSUE_CREATED:
            case ISSUE_IN_PROCESS:
            case ISSUE_ACCEPTED:
            case ISSUE_REJECTED:
            case ISSUE_CANCELLED:
            case MESSAGE:
            case DOWNLOADED_FILE:
            default:
                return "";
        }
    }

    private Notification getSubjectAndDescription(Notification notification, User user) {
        Notification n = new Notification();
        n.setSubject(notification.getSubject());
        n.setDescription(notification.getDescription());
        if (user == null || user.getRole() == null || notification.getSubCategory() == null) return n;
        //Se quita PMIT 535
        String currency ="USD";
        if (AuthoritiesConstants.ADMIN.equals(user.getRole()) || AuthoritiesConstants.SUPER_ADMIN.equals(user.getRole())) {
            currency = "USD";
        } else if (merchantWithEUR.equals(user.getId())) {
            currency = "EUR";
        }        
        String finalCurrency = currency;
        log.info("finalCurrency notification_ {}",finalCurrency);
        Locale locale = Locale.forLanguageTag(user.getLangKey());
        switch (notification.getSubCategory()) {
            case TRANSACTION_CREATED:
                if (AuthoritiesConstants.ADMIN.equals(user.getRole()) || AuthoritiesConstants.SUPER_ADMIN.equals(user.getRole())) {
                    n.setSubject(messageSource.getMessage("notification.transaction.created.super.admin.subject", null, locale));
                    transactionService.findOne(notification.getEntityId()).ifPresentOrElse(
                        t -> n.setDescription(messageSource.getMessage("notification.transaction.created.super.admin.description.ispresent", new String[]{finalCurrency, t.getAmountBeforeCommission().toString()}, locale)),
                        () -> n.setDescription(messageSource.getMessage("notification.transaction.created.super.admin.description.isnotpresent", null, locale))
                    );
                }
                return n;
            case TRANSACTION_SETTLEMENT_DECLINED:
                if (AuthoritiesConstants.ADMIN.equals(user.getRole()) || AuthoritiesConstants.SUPER_ADMIN.equals(user.getRole())) {
                    n.setSubject(messageSource.getMessage("notification.transaction.settlementdeclined.super.admin.subject", null, locale));
                    transactionService.findOne(notification.getEntityId()).ifPresentOrElse(
                        t -> n.setDescription(messageSource.getMessage("notification.transaction.settlementdeclined.super.admin.description.ispresent", new String[]{finalCurrency, t.getAmountBeforeCommission().toString()}, locale)),
                        () -> n.setDescription(messageSource.getMessage("notification.transaction.settlementdeclined.super.admin.description.isnotpresent", null, locale))
                    );
                }
                return n;
            case TRANSACTION_ACCEPTED:
                if (RESELLER.equals(user.getRole())) {
                    n.setSubject(messageSource.getMessage("notification.transaction.accepted.reseller.payee.subject", null, locale));
                    transactionService.findOne(notification.getEntityId()).ifPresentOrElse(
                        t -> n.setDescription(messageSource.getMessage("notification.transaction.accepted.reseller.payee.description.ispresent", new String[]{t.getMerchant().getFullName(), finalCurrency, t.getAmountBeforeCommission().toString()}, locale)),
                        () -> n.setDescription(messageSource.getMessage("notification.transaction.accepted.reseller.payee.description.isnotpresent", null, locale))
                    );
                }
                if (AuthoritiesConstants.PAYEE.equals(user.getRole())) {
                    n.setSubject(messageSource.getMessage("notification.transaction.accepted.reseller.payee.subject", null, locale));
                    transactionService.findOne(notification.getEntityId()).ifPresentOrElse(
                        t -> n.setDescription(messageSource.getMessage("notification.transaction.accepted.reseller.payee.description.ispresent", new String[]{t.getMerchant().getFullName(), t.getCurrency().getCode(), t.getAmountLocalCurrency().toString()}, locale)),
                        () -> n.setDescription(messageSource.getMessage("notification.transaction.accepted.reseller.payee.description.isnotpresent", null, locale))
                    );
                }
                return n;
            case TRANSACTION_REJECTED:
                if (MERCHANT.equals(user.getRole()) || AuthoritiesConstants.PAYEE.equals(user.getRole())) {
                    n.setSubject(messageSource.getMessage("notification.transaction.rejected.merchant.payee.subject", null, locale));
                    n.setDescription(messageSource.getMessage("notification.transaction.rejected.merchant.payee.description", null, locale));
                }
                return n;
            case EXTENDED_USER_CHANGE_PROFILE:
                if (AuthoritiesConstants.PAYEE.equals(user.getRole())) {
                    n.setSubject(messageSource.getMessage("notification.extended.user.change.profile.payee.subject", null, locale));
                    n.setDescription(messageSource.getMessage("notification.extended.user.change.profile.payee.description", null, locale));
                }
                return n;
            case EXTENDED_USER_CREATED:
                if (AuthoritiesConstants.ADMIN.equals(user.getRole()) || AuthoritiesConstants.SUPER_ADMIN.equals(user.getRole())) {
                    n.setSubject(messageSource.getMessage("notification.extended.user.created.subject", null, locale));
                    n.setDescription(messageSource.getMessage("notification.extended.user.created.description", null, locale));
                }
                return n;
            case EXTENDED_USER_ACCEPTED:
                n.setSubject(messageSource.getMessage("notification.extended.user.accepted.subject", null, locale));
                n.setDescription(messageSource.getMessage("notification.extended.user.accepted.description", null, locale));
                return n;
            case EXTENDED_USER_REJECTED:
                n.setSubject(messageSource.getMessage("notification.extended.user.rejected.subject", null, locale));
                n.setDescription(messageSource.getMessage("notification.extended.user.rejected.description", null, locale));
                return n;
            case EXTENDED_USER_CANCELLED:
                n.setSubject(messageSource.getMessage("notification.extended.user.cancelled.subject", null, locale));
                n.setDescription(messageSource.getMessage("notification.extended.user.cancelled.description", null, locale));
                return n;
            case FUND_CREATED:
                n.setSubject(messageSource.getMessage("notification.fund.created.subject", null, locale));
                extendedUserFundsService.findOne(notification.getEntityId()).ifPresentOrElse(
                    euf -> {log.info("message fund body: {}",messageSource.getMessage("notification.fund.created.description.ispresent", new String[]{euf.getExtendedUser().getFullName(), finalCurrency, euf.getValue().toString()}, locale));
                    	   n.setDescription(messageSource.getMessage("notification.fund.created.description.ispresent", new String[]{euf.getExtendedUser().getFullName(), finalCurrency, euf.getValue().toString()}, locale));
                           },
                    () -> n.setDescription(messageSource.getMessage("notification.fund.created.description.isnotpresent", null, locale))
                );
                return n;
            case WITHDRAWAL_CREATED:
                n.setSubject(messageSource.getMessage("notification.withdrawal.created.subject", null, locale));
                extendedUserFundsService.findOne(notification.getEntityId()).ifPresentOrElse(
                    euf -> n.setDescription(messageSource.getMessage("notification.withdrawal.created.description.ispresent", new String[]{euf.getExtendedUser().getFullName(), finalCurrency, euf.getValue().toString()}, locale)),
                    () -> n.setDescription(messageSource.getMessage("notification.withdrawal.created.description.isnotpresent", null, locale))
                );
                return n;
            case FUND_ACCEPTED:
                n.setSubject(messageSource.getMessage("notification.fund.accepted.subject", null, locale));
                extendedUserFundsService.findOne(notification.getEntityId()).ifPresentOrElse(
                    euf -> n.setDescription(messageSource.getMessage("notification.fund.accepted.description.ispresent", new String[]{finalCurrency, euf.getValue().toString()}, locale)),
                    () -> n.setDescription(messageSource.getMessage("notification.fund.accepted.description.isnotpresent", null, locale))
                );
                return n;
            case WITHDRAWAL_ACCEPTED:
                n.setSubject(messageSource.getMessage("notification.withdrawal.accepted.subject", null, locale));
                extendedUserFundsService.findOne(notification.getEntityId()).ifPresentOrElse(
                    euf -> n.setDescription(messageSource.getMessage("notification.withdrawal.accepted.description.ispresent", new String[]{finalCurrency, euf.getValue().toString()}, locale)),
                    () -> n.setDescription(messageSource.getMessage("notification.withdrawal.accepted.description.isnotpresent", null, locale))
                );
                return n;
            case FUND_REJECTED:
                n.setSubject(messageSource.getMessage("notification.fund.rejected.subject", null, locale));
                extendedUserFundsService.findOne(notification.getEntityId()).ifPresentOrElse(
                    euf -> n.setDescription(messageSource.getMessage("notification.fund.rejected.description.ispresent", new String[]{finalCurrency, euf.getValue().toString()}, locale)),
                    () -> n.setDescription(messageSource.getMessage("notification.fund.rejected.description.isnotpresent", null, locale))
                );
                return n;
            case WITHDRAWAL_REJECTED:
                n.setSubject(messageSource.getMessage("notification.withdrawal.rejected.subject", null, locale));
                extendedUserFundsService.findOne(notification.getEntityId()).ifPresentOrElse(
                    euf -> n.setDescription(messageSource.getMessage("notification.withdrawal.rejected.description.ispresent", new String[]{finalCurrency, euf.getValue().toString()}, locale)),
                    () -> n.setDescription(messageSource.getMessage("notification.withdrawal.rejected.description.isnotpresent", null, locale))
                );
                return n;
            case DOWNLOADED_FILE:
                n.setSubject(messageSource.getMessage("notification.downloaded.file.subject", new String[]{notification.getSubject()}, locale));
                n.setDescription(messageSource.getMessage("notification.downloaded.file.description", new String[]{notification.getSubject(), notification.getDescription()}, locale));
                return n;
            case USER_FORGOT_PASSWORD:
            case USER_FORGOT_PASSWORD_NEW_ACCOUNT:
            case USER_EMAIL_CHANGE:
            case USER_EMAIL_CHANGE_CONFIRMATION:
            case USER_CREATED:
            case EXTENDED_USER_IN_PROCESS:
            case TRANSACTION_IN_PROCESS:
            case TRANSACTION_CANCELLED:
            case FUND_CANCELLED:
            case WITHDRAWAL_CANCELLED:
            case ISSUE_CREATED:
            case ISSUE_IN_PROCESS:
            case ISSUE_ACCEPTED:
            case ISSUE_REJECTED:
            case ISSUE_CANCELLED:
            case MESSAGE:
            default:
                return n;
        }
    }

    private int getMaxRetries() {
        return generalMaxRetries;
    }

    private Integer getGeneralMaxRetries() {
        return generalMaxRetries;
    }

    /**
     * Get all the notificationReceivers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<NotificationReceiver> findAll(Pageable pageable) {
        log.debug("Request to get all NotificationReceivers");
        return notificationReceiverRepository.findAll(pageable);
    }

    /**
     * Get one notificationReceiver by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<NotificationReceiver> findOne(Long id) {
        log.debug("Request to get NotificationReceiver : {}", id);
        if (id == null) return Optional.empty();
        return notificationReceiverRepository.findById(id);
    }

    /**
     * Delete the notificationReceiver by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete NotificationReceiver : {}", id);
        NotificationReceiver cnr = findOne(id).orElseThrow(() ->
            new NotFoundAlertException(ENTITY_NAME + " id: " + id, ENTITY_NAME, NOT_FOUND));
        log.debug("Cancelling current NotificationReceiver : {}", cnr);
        cnr.setStatus(Status.CANCELLED);
        cnr.setReason("Logically DELETED");
        save(cnr);
    }

    /**
     * Search for the notificationReceiver corresponding to the query.
     *
     * @param query    the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<NotificationReceiver> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of NotificationReceivers for query {}", query);
        return notificationReceiverSearchRepository.search(queryStringQuery(query), pageable);
    }

    @Transactional
    public void saveAll(List<NotificationReceiver> notificationReceiverList) {
        if (!notificationReceiverList.isEmpty())
            notificationReceiverSearchRepository.saveAll(notificationReceiverList);
    }

    @Scheduled(cron = SEND_NOTIFICATION_CRON_EXPRESSION)
    public void sendNotificationsReceivers() {

        Date startDate = Calendar.getInstance().getTime();
        int maxToProcess = 20;
        Pageable pageable = PageRequest.of(0, maxToProcess, new Sort(Sort.Direction.ASC, "id"));
        List<NotificationReceiver> list =
            notificationReceiverRepository.findAllByMustSendEmailTrueAndEmailSentFalseAndRetriesIsLessThan(
                getGeneralMaxRetries(), pageable).getContent();

        list.forEach(this::send);
        if (!list.isEmpty())
            log.info("sendNotification {} - Elapsed time: {} s - Processed: {} of {} [max]",
                SEND_NOTIFICATION_CRON_DESCRIPTION,
                (Calendar.getInstance().getTime().getTime() - startDate.getTime()) / 1000, list.size(), maxToProcess);
    }

    @Scheduled(cron = DELETE_CANCELLED_AND_SENDER_NOT_NULL_NOTIFICATION_CRON_EXPRESSION)
    public void deleteCancelledAndSenderNotNullNotificationsReceivers() {
        Date startDate = Calendar.getInstance().getTime();
        int maxToProcess = 100;
        Pageable pageable = PageRequest.of(0, maxToProcess, new Sort(Sort.Direction.ASC, "id"));
        ZonedDateTime dateTime = ZonedDateTime.now().minus(1, ChronoUnit.MONTHS);
        List<NotificationReceiver> list =
            notificationReceiverRepository.findAllByStatusIsAndLastUpdatedIsLessThanAndSenderIsNotNull(
                Status.CANCELLED, dateTime, pageable).getContent();
        list.forEach(notificationReceiver -> delete(notificationReceiver.getId()));
        if (!list.isEmpty())
            log.info("deleteCancelledAndSenderNotNullNotificationsReceivers {} LastUpdatedIsLessThan {} - Elapsed time: {} ms - Processed: {} of {} [max]",
                DELETE_CANCELLED_AND_SENDER_NOT_NULL_NOTIFICATION_CRON_DESCRIPTION,
                dateTime, Calendar.getInstance().getTime().getTime() - startDate.getTime(), list.size(), maxToProcess);
    }

    // @Async
    public void send(NotificationReceiver nr) {

        log.info(">>>>>>>>>>>>>>>>> send: {}", nr);

        if (!nr.isMustSendEmail()) return;
        if (nr.isEmailSent()) return;
        if (nr.getStatus()==null || nr.getStatus()==Status.CANCELLED) return;

        if (nr.getRetries() > nr.getMaxRetries()) {
            nr.setMustSendEmail(false);
            nr.setStatus(Status.REJECTED);
            nr.setReason("Maximum number of retries reached: " + nr.getRetries());
            nr.setEmailSent(false);
            save(nr);
            return;
        }
        try {
            nr.setRetries(nr.getRetries() + 1);

            String template = nr.getEmailTemplate();

            log.info("==_====> NotificationReceiver::send--> Template: {}", template);

            User user = (nr.getReceiver() != null) ? nr.getReceiver().getUser() : nr.getUserReceiver();

            log.info("==_====> NotificationReceiver::send--> User: {}", user);

            Transaction transaction = getTransactionFromNotification(nr.getNotification());

            log.info("==_====> NotificationReceiver::send--> Transaction: {}", transaction);

            String titleKeyStr = nr.getEmailTitleKey();


            if(user.getExtraURLParameter() != null && user.getExtraURLParameter().equals("&pagomundo=1") && titleKeyStr.equals("email.activation.title")) {
            	titleKeyStr = "email.activation.titlePagomundo";
            }

            if (template != null && !template.equals("")){
                log.info("********* ==_====> NotificationReceiver::send--> SENT FROM TREMPLATE");
                mailService.sendEmailFromTemplate(user, transaction, nr.getEmailTemplate(), titleKeyStr);
            } else{
                log.info("==_====> NotificationReceiver::send--> SENT ANOTHER WAY");
                mailService.sendEmail(
                    user.getEmail(), nr.getSubject(), nr.getDescription(), false, getIsHtmlBySubCategory(nr.getNotification().getSubCategory()));
            }

            log.info("==_====> NotificationReceiver::send--> SENT EMAIL CORRECTLY");
            nr.setReason(null);
            nr.setDebugReason(null);
            nr.setEmailSent(true);
            log.info("==_====> NotificationReceiver::send--> MARKED SENT EMAIL");
        } catch (Exception e) {
            log.warn("Something went wrong trying to send the email for notification {}, e: {}", nr, e.getMessage());
            nr.setDebugReason(e.getMessage().length() > 255 ? e.getMessage().substring(0, 255) : e.getMessage());
            nr.setReason("Error trying to send email for notification");
            nr.setStatus(Status.REJECTED);

            nr.setEmailSent(true);
            log.info("==_====> NotificationReceiver::send--> MARKED ERROR SENT EMAIL");
        } finally {
            save(nr);
        }
    }

    private Transaction getTransactionFromNotification(Notification n) {
        return (NotificationSubCategory.TRANSACTION_REJECTED.equals(n.getSubCategory()) && n.getEntityId() != null)
            ? transactionService.findOne(n.getEntityId()).orElse(null)
            : null;
    }

    /* Notifications for PAYMENTS */
    @Async
    public void sendTransactionNotification(Collection<Transaction> transactions, ExtendedUser sender) {

        log.info(">>>>>>>>>>>>>>>>> sendTransactionNotification");

        transactions.forEach(transaction -> sendTransactionNotification(transaction, sender));
    }

    @Async
    public void sendMultiPostTransactionNotification(Collection<Transaction> transactions, ExtendedUser sender) {

        log.info(">>>>>>>>>>>>>>>>> sendMultiPostTransactionNotification");

        if (sender == null) return;
        if (transactions.isEmpty()) return;
        create(createTransactionNotification(transactions, sender));


    }

    @Async
    public void sendMultiPostTransactionNotification(List<TransactionForMerchantDTO> transactionForMerchant, ExtendedUser sender) {

        log.info(">>>>>>>>>>>>>>>>> sendMultiPostTransactionNotification 2");


        log.info(">>>>>>>>>>>>>>>>> sendMultiPostTransactionNotification - transactionForMerchant: {}", transactionForMerchant);

        log.info(">>>>>>>>>>>>>>>>> sendMultiPostTransactionNotification - sender: {}", sender);

        if (sender == null) return;

        log.info(">>>>>>>>>>>>>>>>> after sender null validation ");

        if (transactionForMerchant.isEmpty()) return;

        log.info(">>>>>>>>>>>>>>>>> after transactionForMerchant isEmpty validation ");

        List<Transaction> transactions = new ArrayList<>();

        transactionForMerchant.forEach(

            transactionForMerchantDTO -> transactionService.findOne(transactionForMerchantDTO.getId())
                .ifPresent(transactions::add)

        );

        create(createTransactionNotification(transactions, sender));

        log.info(">>>>>>>>>>>>>>>>> after create sendMultiPostTransactionNotification");

    }

    @Async
    public void sendTransactionNotification(Transaction transaction, ExtendedUser sender) {

        log.info(">>>>>>>>>>>>>>>>> sendTransactionNotification");

        if (sender == null) return;
        create(createTransactionNotification(transaction, sender));
    }

    private NotificationReceiverDTO createTransactionNotification(Transaction transaction, ExtendedUser sender) {
        Notification n = getTransactionNotification(transaction);
        if (n == null) return null;
        n.setEntityId(transaction.getId());
        n.setSender(sender);
        NotificationReceiverDTO nrDTO = new NotificationReceiverDTO();
        nrDTO.setNotification(n);
        nrDTO.setReceivers(getTransactionReceivers(transaction));
        log.info(">>>>>>>>>>>>>>>>> Recievers List "+nrDTO.getReceivers().size());
        return nrDTO;
    }

    private NotificationReceiverDTO createTransactionNotification(Collection<Transaction> transactions, ExtendedUser sender) {

        log.info(">>>>>>>>>>>>>>>>> inside createTransactionNotification");

        BigDecimal totalAmount = transactions.stream().map(Transaction::getAmountBeforeCommission)
            .reduce(BigDecimal.ZERO, BigDecimal::add);

        int size = transactions.size();
        Notification n = new Notification();
        n.setEntityId(null);
        n.setSender(sender);
        n.setSubCategory(NotificationSubCategory.TRANSACTION_CREATED);
        n.setDescription("There" + (size > 1 ? " are " : " is ") + size + " payment" + (size > 1 ? "s" : "") +
            " pending to be processed. Total amount: " + totalAmount);

        log.info(">>>>>>>>>>>>>>>>> notification: {}", n);

        NotificationReceiverDTO nrDTO = new NotificationReceiverDTO();
        nrDTO.setNotification(n);

        return nrDTO;
    }

    private Notification getTransactionNotification(Transaction t) {
        if (!t.isMustNotify()) return null;
        Notification n = new Notification();
        switch (t.getStatus()) {
            case CREATED:
                n.setSubCategory(NotificationSubCategory.TRANSACTION_CREATED);
                return n;
            case ACCEPTED:
                n.setSubCategory(NotificationSubCategory.TRANSACTION_ACCEPTED);
                return n;
            case REJECTED:
                n.setSubCategory(NotificationSubCategory.TRANSACTION_REJECTED);
                return n;
            case IN_PROCESS:
            case CANCELLED:
            case SETTLEMENT_DECLINED:
                n.setSubCategory(NotificationSubCategory.TRANSACTION_SETTLEMENT_DECLINED);
                return n;
            default:
                return null;
        }
    }

    private List<ExtendedUser> getTransactionReceivers(Transaction t) {
        List<ExtendedUser> receivers = new ArrayList<>();
        switch (t.getStatus()) {
            case ACCEPTED:
                if(t.isMustNotifyPayee())receivers.add(t.getPayee());
                if (t.getReseller() != null) receivers.add(t.getReseller());
                return receivers;
            case REJECTED:
                if(t.isMustNotifyPayee())receivers.add(t.getPayee());
                receivers.add(t.getMerchant());
                return receivers;
            case CREATED:
            case SETTLEMENT_DECLINED:
                return extendedUserService.getAllActiveAdmins();

            case IN_PROCESS:
            case CANCELLED:
            default:
                return receivers;
        }
    }

    /* Notifications for EXTENDED_USERS */
    // @Async
    public void sendExtendedUserNotification(Collection<ExtendedUser> extendedUsers, ExtendedUser sender) {

        log.info(">>>>>>>>>>>>>>>>>> inside of sendExtendedUserNotification");

        extendedUsers.forEach(extendedUser -> sendExtendedUserNotification(extendedUser, sender));
    }

    // @Async
    public void sendExtendedUserNotification(ExtendedUser extendedUser, ExtendedUser sender) {


        log.info(">>>>>>>>>>>>>>>>>> inside of sendExtendedUserNotification - ExtendedUser");

        if (sender == null || extendedUser == null) return;

        log.info(">>>>>>>>>>>>>>>>>> after validation of sendExtendedUserNotification - ExtendedUser");
        log.info(">>>>>>>>>>>>>>>>>> ExtendedUser: {}", extendedUser);
        log.info(">>>>>>>>>>>>>>>>>> sender: {}", sender);

        if (userService.isPayee(extendedUser.getUser()) &&
            extendedUser.isMustNotify() != null && extendedUser.isMustNotify() &&
            extendedUser.getProfileInfoChanged() != null && !extendedUser.getProfileInfoChanged().equals("")) {

            log.info(">>>>>>>>>>>>>>>>>> isPayee and needs to be notified: {}", extendedUser.getEmail());

            Notification n = new Notification();
            n.setSubCategory(NotificationSubCategory.EXTENDED_USER_CHANGE_PROFILE);
            n.setEntityId(extendedUser.getId());
            n.setSender(sender);

            NotificationReceiverDTO nrDTO = new NotificationReceiverDTO();
            nrDTO.setNotification(n);
            List<ExtendedUser> receivers = new ArrayList<>();
            receivers.add(extendedUser);
            nrDTO.setReceivers(receivers);

            create(nrDTO);

        }
        else{

            log.info(">>>>>>>>>>>>>>>>>> isPayee not notified: {}", extendedUser.getEmail());

        }

        log.info(">>>>>>>>>>>>>>>>>> after first create  notification call");

        create(createExtendedUserNotification(extendedUser, sender));

    }

    private NotificationReceiverDTO createExtendedUserNotification(ExtendedUser extendedUser, ExtendedUser sender) {

        log.info(">>>>>>>>>>>>>>>>>> inside of createExtendedUserNotification");

        Notification n = null;

        log.info(">>>>>>>>>>>>>>>>>> extendedUser.isMustNotify() {}", extendedUser.isMustNotify());

        if (extendedUser.isMustNotify()) {

            log.info(">>>>>>>>>>>>>>>>>> inside of isMustNotify true");

            n = new Notification();
            n.setSubCategory(getExtendedUserNotification(extendedUser));
            n.setEntityId(extendedUser.getId());
            n.setSender(sender);
        }

        log.info(">>>>>>>>>>>>>>>>>> Notification n: {}", n);

        NotificationReceiverDTO nrDTO = new NotificationReceiverDTO();
        nrDTO.setNotification(n);
        nrDTO.setReceivers(getExtendedUserReceivers(extendedUser));

        return nrDTO;
    }

    private NotificationSubCategory getExtendedUserNotification(ExtendedUser extendedUser) {

        log.info(">>>>>>>>>>>>>>>>>> inside of getExtendedUserNotification");


        switch (extendedUser.getStatus()) {
            case CREATED:
                return NotificationSubCategory.EXTENDED_USER_CREATED;
            case ACCEPTED:
                return NotificationSubCategory.EXTENDED_USER_ACCEPTED;
            case REJECTED:
                return NotificationSubCategory.EXTENDED_USER_REJECTED;
            case CANCELLED:
                return NotificationSubCategory.EXTENDED_USER_CANCELLED;
            case IN_PROCESS:
            default:
                return null;
        }

    }

    private List<ExtendedUser> getExtendedUserReceivers(ExtendedUser extendedUser) {

        log.info(">>>>>>>>>>>>>>>>>> inside of getExtendedUserNotification");

        List<ExtendedUser> receivers = new ArrayList<>();
        switch (extendedUser.getStatus()) {
            case ACCEPTED:
            case REJECTED:
            case CANCELLED:
                receivers.add(extendedUser);
                return receivers;
            case CREATED:
                return extendedUserService.getAllActiveAdmins();
            case IN_PROCESS:
            default:
                return receivers;
        }
    }

    /* Notifications for USER_ACCOUNT | USER_FORGOT_PASSWORD */
    public void sendForgotPasswordNotification(User user) {
        if (user == null) return;
        Optional<ExtendedUser> sender = extendedUserService.getExtendedUserFromUser(user.getId());
        create(forgotPasswordNotification(user, sender.isPresent() ? sender.get() : getSenderSuperadmin()));
    }

    private ExtendedUser getSenderSuperadmin() {
        List<ExtendedUser> extendedUserList = extendedUserService.getAllActiveSuperAdmins();
        ExtendedUser extendedUser = extendedUserList != null ? extendedUserList.get(0) : null;
        return extendedUser;
    }

    private NotificationReceiverDTO forgotPasswordNotification(User user, ExtendedUser sender) {
        Notification n = new Notification();
        if (userService.verifyNewAccountByValidationKey(user.getResetKey())) {
            n.subCategory(NotificationSubCategory.USER_FORGOT_PASSWORD_NEW_ACCOUNT);
        } else {
            n.subCategory(NotificationSubCategory.USER_FORGOT_PASSWORD);
        }
        return getNotificationReceiverDTO(user, sender, n);
    }

    private NotificationReceiverDTO getNotificationReceiverDTO(User user, ExtendedUser sender, Notification n) {
        n.setEntityId(user.getId());
        n.setSender(sender);
        NotificationReceiverDTO nrDTO = new NotificationReceiverDTO();
        nrDTO.setNotification(n);
        nrDTO.setReceivers(null);
        nrDTO.setUserReceivers(getCreateUserReceivers(user));
        return nrDTO;
    }

    /* Notifications for USER_ACCOUNT | USER_CREATED */
    // @Async
    public void sendMultiUserNotification(Collection<UserDTO> users, ExtendedUser sender) {

        log.info(">>>>>>>>>>>>>>>>>> inside of sendMultiUserNotification");

        if (sender == null || users.isEmpty()) return;
        users.forEach(user -> sendCreateUserNotification(user.getUser(), sender));
    }

    public void sendMultiUserNotificationNoNotySuperAdmin(Collection<UserDTO> users, ExtendedUser sender) {
        log.info(">>>>>>>>>>>>>>>>>> inside of sendMultiUserNotificationNoNotySuperAdmin");
        if (sender == null || users.isEmpty()) return;
        for (UserDTO user: users){
            if (user.getRole().equalsIgnoreCase(RESELLER) || user.getRole().equalsIgnoreCase(MERCHANT) ){
                sendCreateUserNotification(user.getUser(), sender);
            }
        }

        users.forEach(user -> sendCreateUserNotification(user.getUser(), sender));
    }

    // @Async
    public void sendCreateUserNotification(User user, ExtendedUser sender) {

        log.info(">>>>>>>>>>>>>>>>>> inside of sendCreateUserNotification");

        if (sender == null) return;
        if (user == null) return;
        create(createUserNotification(user, sender));

    }

    private NotificationReceiverDTO createUserNotification(User user, ExtendedUser sender) {

        log.info(">>>>>>>>>>>>>>>>>> inside of createUserNotification");

        Notification n = getCreateUserNotification(user);
        return getNotificationReceiverDTO(user, sender, n);

    }

    private Notification getCreateUserNotification(User u) {
        if (u == null) return null;
        if (u.getStatus() != Status.CREATED) return null;

        Notification n = new Notification();
        n.subCategory(NotificationSubCategory.USER_CREATED);
        return n;
    }

    private List<User> getCreateUserReceivers(User u) {
        List<User> userReceivers = new ArrayList<>();
        userReceivers.add(u);
        return userReceivers;
    }

    /* Notifications for EXTENDED_USERS_FUNDS */
    @Async
    public void sendFundNotification(ExtendedUserFunds euf, ExtendedUser sender) {

        log.info(">>>>>>>>>>>>>>>>> sendFundNotification");

        if (sender == null) return;
        create(createFundNotification(euf, sender));
    }

    private NotificationReceiverDTO createFundNotification(ExtendedUserFunds euf, ExtendedUser sender) {
        Notification n = new Notification();
        n.setSubCategory(getFundNotification(euf));
        n.setEntityId(euf.getId());
        n.setSender(sender);
        NotificationReceiverDTO nrDTO = new NotificationReceiverDTO();
        nrDTO.setNotification(n);
        nrDTO.setReceivers(getFundReceivers(euf));
        return nrDTO;
    }

    private NotificationSubCategory getFundNotification(ExtendedUserFunds euf) {
        switch (euf.getStatus()) {
            case CREATED:
                return euf.getType().equals(FundType.FUNDING) ? NotificationSubCategory.FUND_CREATED : NotificationSubCategory.WITHDRAWAL_CREATED;
            case ACCEPTED:
                return euf.getType().equals(FundType.FUNDING) ? NotificationSubCategory.FUND_ACCEPTED : NotificationSubCategory.WITHDRAWAL_ACCEPTED;
            case REJECTED:
                return euf.getType().equals(FundType.FUNDING) ? NotificationSubCategory.FUND_REJECTED : NotificationSubCategory.WITHDRAWAL_REJECTED;
            case CANCELLED:
                return euf.getType().equals(FundType.FUNDING) ? NotificationSubCategory.FUND_CANCELLED : NotificationSubCategory.WITHDRAWAL_CANCELLED;
            default:
                return null;
        }
    }

    private List<ExtendedUser> getFundReceivers(ExtendedUserFunds euf) {
        List<ExtendedUser> receivers = new ArrayList<>();
        switch (euf.getStatus()) {
            case CREATED:
                return euf.getType().equals(FundType.FUNDING) ?
                    extendedUserService.getAllActiveAdmins() :
                    extendedUserService.getAllActiveSuperAdmins();
            case ACCEPTED:
            case REJECTED:
                receivers.add(euf.getExtendedUser());
                return receivers;
            case CANCELLED:
            default:
                return receivers;
        }
    }

    void sendDownloadedFileNotification(String chartTitle, String fileName, ExtendedUser sender) {
        if (sender == null || fileName == null || fileName.equals("")) return;
        create(createDownloadedFileNotification(chartTitle, fileName, sender));
    }

    private NotificationReceiverDTO createDownloadedFileNotification(String chartTitle, String fileName, ExtendedUser sender) {
        Notification notification = new Notification();
        notification.setSender(sender);
        notification.setSubCategory(NotificationSubCategory.DOWNLOADED_FILE);
        notification.setSubject(chartTitle);
        notification.setDescription(fileName);
        NotificationReceiverDTO notificationReceiverDTO = new NotificationReceiverDTO();
        notificationReceiverDTO.setNotification(notification);
        List<ExtendedUser> receivers = new ArrayList<>();
        receivers.add(sender);
        notificationReceiverDTO.setReceivers(receivers);

        return notificationReceiverDTO;
    }

    @Async
    public void refetch(Long id) {
        refetchService.refetchNotificationReceiverService(id);
    }

    @Async
    public void refetchAll(Collection<Notification> elements) {
        elements.forEach(e -> refetch(e.getId()));
    }

    // @Async
    public void sendChangeEmailNotification(EmailChangeDTO emailChangeDTO) {

        log.info(">>>>>>>>>>>>>>>>> sendChangeEmailNotification");

        Optional<User> user = userService.changeEmailNotification(emailChangeDTO);

        if (!user.isPresent())
            throw new UserNotFoundException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.account.user.email.not.registered") + " " + emailChangeDTO.getCurrentEmail());
        userService.refetch(user.get().getId());

        ExtendedUser sender = extendedUserService.getExtendedUserFromUser(user.get());
        if (sender == null) return;
        create(changeEmailNotification(user.get(), sender));
    }

    private NotificationReceiverDTO changeEmailNotification(User user, ExtendedUser sender) {

        log.info(">>>>>>>>>>>>>>>>> changeEmailNotification");

        Notification n = new Notification();
        n.subCategory(NotificationSubCategory.USER_EMAIL_CHANGE);
        n.setSubject("");
        return getNotificationReceiverDTO(user, sender, n);
    }

    // @Async
    public void sendChangeEmailConfirmationNotification(User user) {

        log.info(">>>>>>>>>>>>>>>>> sendChangeEmailConfirmationNotification");

        ExtendedUser sender = extendedUserService.getExtendedUserFromUser(user);
        if (sender == null) return;
        create(changeEmailConfirmationNotification(user, sender));
    }

    private NotificationReceiverDTO changeEmailConfirmationNotification(User user, ExtendedUser sender) {

        log.info(">>>>>>>>>>>>>>>>> changeEmailConfirmationNotification");

        Notification n = new Notification();
        n.subCategory(NotificationSubCategory.USER_EMAIL_CHANGE_CONFIRMATION);
        n.setSubject("");
        return getNotificationReceiverDTO(user, sender, n);
    }

    Optional<List<NotificationReceiver>> findAllByNotificationId(Long notificationId) {
        return notificationReceiverRepository.findAllByNotificationId(notificationId);
    }

    private boolean getIsHtmlBySubCategory(NotificationSubCategory notificationSubcategory) {
        boolean isHtml = true;
        if (NotificationSubCategory.TRANSACTION_CREATED.equals(notificationSubcategory)) {
            isHtml = false;
        }
        return isHtml;
    }
}
