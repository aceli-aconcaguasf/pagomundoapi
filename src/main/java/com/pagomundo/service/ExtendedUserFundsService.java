package com.pagomundo.service;

import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.ExtendedUserFunds;
import com.pagomundo.domain.Transaction;
import com.pagomundo.domain.enumeration.FundCategory;
import com.pagomundo.domain.enumeration.FundType;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.repository.ExtendedUserFundsRepository;
import com.pagomundo.repository.search.ExtendedUserFundsSearchRepository;
import com.pagomundo.repository.search.RefetchService;
import com.pagomundo.web.rest.errors.BadRequestAlertException;
import com.pagomundo.web.rest.errors.InvalidWithdrawalException;
import com.pagomundo.web.rest.errors.NotFoundAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link ExtendedUserFunds}.
 */
@Service
@Transactional
public class ExtendedUserFundsService {
    public static final String ENTITY_NAME = "ExtendedUserFunds";
    public static final String INCORRECT_VALUE = ENTITY_NAME + ".001";
    public static final String NOT_FOUND = ENTITY_NAME + ".002";
    public static final String SHOULD_BE_NULL_CODE = ENTITY_NAME + ".003";
    public static final String SHOULD_NOT_BE_NULL_CODE = ENTITY_NAME + ".004";
    public static final String BAD_REQUEST = ENTITY_NAME + ".005";

    private final Logger log = LoggerFactory.getLogger(ExtendedUserFundsService.class);

    private final RefetchService refetchService;
    private final ExtendedUserFundsRepository extendedUserFundsRepository;
    private final ExtendedUserService extendedUserService;
    private final ExtendedUserFundsSearchRepository extendedUserFundsSearchRepository;
    private final TranslateService translateService;

    @Value("#{'${merchants}'.split(',')}")
    private List<Long> allowedMerchants;

    public ExtendedUserFundsService(
        RefetchService refetchService, ExtendedUserFundsRepository extendedUserFundsRepository,
        ExtendedUserService extendedUserService,
        ExtendedUserFundsSearchRepository extendedUserFundsSearchRepository, TranslateService translateService) {
        this.refetchService = refetchService;
        this.extendedUserFundsRepository = extendedUserFundsRepository;
        this.extendedUserService = extendedUserService;
        this.extendedUserFundsSearchRepository = extendedUserFundsSearchRepository;
        this.translateService = translateService;
    }

    /**
     * Save a euf.
     *
     * @param euf the entity to save.
     * @return the persisted entity.
     */
    public ExtendedUserFunds save(ExtendedUserFunds euf) {
        
        log.info("Request to save fund: {}", euf);

        euf.setLastUpdated(ZonedDateTime.now());
        if (euf.getStatus().equals(Status.ACCEPTED)) {

            euf.setBalanceBefore(euf.getExtendedUser().getBalance());

            if (euf.getType() != null && euf.getType().equals(FundType.FUNDING)) {

                log.info(">>>>>>>>>> FUNDING: {}", euf);


                euf.setBalanceAfter(euf.getExtendedUser().getBalance().add(euf.getValue()));
            } else {

                log.info(">>>>>>>>>> Else FUNDING: {}", euf);

                if (euf.getValue() != null && euf.getValue().compareTo(euf.getExtendedUser().getBalance()) < 1) {

                    log.info(">>>>>>>>>> Else FUNDING - getBalance < 1: {}", euf);
                    
                    euf.setBalanceAfter(euf.getExtendedUser().getBalance().add(euf.getValue().negate()));

                } else {

                    log.info(">>>>>>>>>> Else FUNDING - withdrawal: {}", euf);

                    throw new InvalidWithdrawalException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.withdrawal.less.balance") + " " + euf.getExtendedUser().getBalance().toString() +
                        translateService.getMessageByCurrentUserParamsNull("error.message.exception.withdrawal") + " " + euf.getValue().toString() + ".", ExtendedUserFundsService.ENTITY_NAME,
                        ExtendedUserFundsService.INCORRECT_VALUE);
                }
            }
        }

        log.info(">>>>>>>>>> After validations of funding: {}", euf);

        
        ExtendedUserFunds result = extendedUserFundsRepository.save(euf);

        log.info(">>>>>>>>>> After extendedUserFundsRepository: {}", result);

        extendedUserFundsSearchRepository.save(result);

        log.info(">>>>>>>>>> After extendedUserFundsSearchRepository: {}", result);

        if (euf.getStatus().equals(Status.ACCEPTED)) {
            euf.getExtendedUser().setBalance(euf.getBalanceAfter());
            extendedUserService.save(euf.getExtendedUser(), false, false);
        }

        log.info(">>>>>>>>>> After euf.getStatus().equals(Status.ACCEPTED) if: {}", result);

        return result;
    }

    /**
     * Request a fund.
     *
     * @param euf the entity to save.
     * @return the persisted entity.
     */
    public ExtendedUserFunds request(ExtendedUserFunds euf) {
        euf.setDateCreated(ZonedDateTime.now());
        euf.setBalanceBefore(null);
        euf.setBalanceAfter(null);
        log.info(euf.getExtendedUser().getId().toString());
        if( allowedMerchants.contains( euf.getExtendedUser().getId() )) {
            euf.setStatus(Status.ACCEPTED);
        }else {
            euf.setStatus(extendedUserService.isAdmin(true) ? Status.ACCEPTED : Status.CREATED);
        }
        euf.setReason(null);
        euf.setCategory(FundCategory.REQUESTED);
        euf.setUser(extendedUserService.isAdmin(true) ?
            extendedUserService.getCurrentExtendedUser().getUser() :
            null);

        log.debug("Requested fund : {}", euf);
        return save(euf);
    }

    /**
     * Update a fund.
     *
     * @param euf the entity to save.
     * @return the persisted entity.
     */
    public ExtendedUserFunds update(ExtendedUserFunds euf) {
        ExtendedUserFunds ceuf = findOne(euf.getId()).orElseThrow(() ->
            new NotFoundAlertException(ExtendedUserFundsService.ENTITY_NAME + " id: " + euf.getId(),
                ExtendedUserFundsService.ENTITY_NAME, ExtendedUserFundsService.NOT_FOUND));
        log.debug("Current euf: {}", ceuf);

        if (!ceuf.getCategory().equals(FundCategory.REQUESTED))
            throw new NotFoundAlertException(ExtendedUserFundsService.ENTITY_NAME + " id: " + euf.getId(),
                ExtendedUserFundsService.ENTITY_NAME, ExtendedUserFundsService.NOT_FOUND);

        if (!validateStatusChange(euf.getStatus(), ceuf.getStatus()))
            throw new BadRequestAlertException(
                translateService.getMessageByCurrentUserParamsNull("error.message.exception.invalid.status") + " " + ceuf.getStatus().toString() + " to " + euf.getStatus().toString(),
                ExtendedUserFundsService.ENTITY_NAME, ExtendedUserFundsService.BAD_REQUEST);

        if (euf.getValue() != null && !euf.getValue().equals(ceuf.getValue()))
            throw new BadRequestAlertException(
                translateService.getMessageByCurrentUserParamsNull("error.message.exception.funding.change"),
                ExtendedUserFundsService.ENTITY_NAME, ExtendedUserFundsService.INCORRECT_VALUE);

        if (extendedUserService.isAdmin(true) &&
            (euf.getStatus() == Status.ACCEPTED || euf.getStatus() == Status.REJECTED))
            ceuf.setUser(extendedUserService.getCurrentExtendedUser().getUser());

        ceuf.setStatus(euf.getStatus());
        ceuf.setReason(euf.getReason());

        log.debug("Update fund : {}", ceuf);
        return save(ceuf);
    }

    /**
     * Validate the new status.
     *
     * @param newStatus     status to validate.
     * @param currentStatus status of the current entity to validate.
     * @return boolean value according to validation.
     */
    private boolean validateStatusChange(Status newStatus, Status currentStatus) {
        if (newStatus == null) return false;
        if (newStatus == currentStatus) return false;
        if (currentStatus != Status.CREATED) return false;

        if ((newStatus == Status.CANCELLED && !(extendedUserService.isMerchant() || extendedUserService.isReseller())) ||
            ((newStatus == Status.ACCEPTED || newStatus == Status.REJECTED) && !extendedUserService.isAdmin(true)))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.current.not.perform.action"),
                ExtendedUserFundsService.ENTITY_NAME, ExtendedUserService.WRONG_USER_ROLE);

        return newStatus == Status.ACCEPTED || newStatus == Status.CANCELLED || newStatus == Status.REJECTED;
    }

    public boolean isAutomatic(Transaction ct) {
        return (ct.getTransactionGroup() != null &&
            ct.getTransactionGroup().getBankAccount() != null &&
            ct.getTransactionGroup().getBankAccount().getBank() != null &&
            ct.getTransactionGroup().getBankAccount().getBank().getId() != null &&
            ct.getTransactionGroup().getBankAccount().getBank().getId().intValue() == 30);
    }

    /**
     * Update the merchant's balance according to the transaction's status
     *
     * @param t the transaction with the info to update the merchant's balance.
     */
    void updateBalance(Transaction t) {
        BigDecimal value;
        ExtendedUser eu;
        FundType fundType = FundType.FUNDING;
        switch (t.getStatus()) {
            case CREATED: // balance amount should decrease for merchant when transaction is created
                value = t.getAmountBeforeCommission().negate();
                eu = t.getMerchant();
                break;
            case CANCELLED: // balance amount should increase for merchant when transaction is cancelled or rejected
            case REJECTED:
                value = t.getAmountBeforeCommission();
                eu = t.getMerchant();
                break;
            case ACCEPTED: // balance amount should increase for reseller when transaction is accepted
                eu = t.getReseller();
                if (eu == null) return;
                value = t.getResellerCommission().negate();
                fundType = FundType.WITHDRAWAL;
                break;
            default:
                return;
        }

        ExtendedUserFunds euf = new ExtendedUserFunds();
        euf.setDateCreated(ZonedDateTime.now());
        euf.setValue(value);
        euf.setReason(t.getStatus().toString() + " payment " + t.getId());
        euf.setExtendedUser(eu);
        euf.setUser(isAutomatic(t) ? t.getAdmin().getUser() : extendedUserService.getCurrentUser());
        euf.setStatus(Status.ACCEPTED);
        euf.setCategory(FundCategory.AUTOMATIC);
        euf.setType(fundType);
        save(euf);
    }

    /**
     * Get all the extendedUserFunds.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ExtendedUserFunds> findAll(Pageable pageable) {
        log.debug("Request to get all ExtendedUserFunds");
        return extendedUserFundsRepository.findAll(pageable);
    }

    /**
     * Get one extendedUserFunds by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ExtendedUserFunds> findOne(Long id) {
        log.debug("Request to get ExtendedUserFunds : {}", id);
        if (id == null) return Optional.empty();
        return extendedUserFundsRepository.findById(id);
    }

    /**
     * Delete the extendedUserFunds by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ExtendedUserFunds : {}", id);
        extendedUserFundsRepository.deleteById(id);
        extendedUserFundsSearchRepository.deleteById(id);
    }

    /**
     * Search for the extendedUserFunds corresponding to the query.
     *
     * @param query    the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ExtendedUserFunds> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ExtendedUserFunds for query {}", query);
        return extendedUserFundsSearchRepository.search(queryStringQuery(query), pageable);
    }

    @Transactional
    public void saveAll(List<ExtendedUserFunds> extendedUserFundsList) {
        if (!extendedUserFundsList.isEmpty())
            extendedUserFundsSearchRepository.saveAll(extendedUserFundsList);
    }

    @Async
    public void refetch(Long id) {
        refetchService.refetchExtendedUserFunds(id);
    }

    @Async
    public void refetchAll(Collection<ExtendedUser> elements) {
        elements.forEach(e -> refetch(e.getId()));
    }
}
