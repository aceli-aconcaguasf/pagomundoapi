package com.pagomundo.service;

import com.cronutils.descriptor.CronDescriptor;
import com.cronutils.model.definition.CronDefinitionBuilder;
import com.cronutils.parser.CronParser;
import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.Notification;
import com.pagomundo.domain.NotificationReceiver;
import com.pagomundo.domain.Transaction;
import com.pagomundo.domain.enumeration.StatisticsTransactionColumnsReport;
import com.pagomundo.domain.enumeration.StatisticsUserColumnsReport;
import com.pagomundo.service.util.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import static com.cronutils.model.CronType.QUARTZ;

@Service
@Transactional
public class FileService {

    private static final Logger log = LoggerFactory.getLogger(FileService.class);

    // Statistics report folder
    private static final String DOWNLOAD_STATISTICS_FOLDER = "downloadedStatisticsReport/";
    //7 DAYS
    private static final long DELETE_TIME_DAYS = 7;

    //EVERY DAY AT MIDNIGHT - 12am
    private static final String DELETE_DOWNLOADED_DOCUMENT_CRON_EXPRESSION = "0 0 0 * * ?";
    private static final String DELETE_DOWNLOADED_DOCUMENT_CRON_DESCRIPTION = CronDescriptor.instance(Locale.UK).describe(new CronParser(CronDefinitionBuilder.instanceDefinitionFor(QUARTZ)).parse(DELETE_DOWNLOADED_DOCUMENT_CRON_EXPRESSION));
    private final NotificationReceiverService notificationReceiverService;
    private final NotificationService notificationService;
    private TranslateService translateService;

    @Value("${fileBucket.path}")
    private String fileBucketPath;

    public FileService(NotificationReceiverService notificationReceiverService, NotificationService notificationService, TranslateService translateService) {
        this.notificationReceiverService = notificationReceiverService;
        this.notificationService = notificationService;
        this.translateService = translateService;
    }

    /**
     * deleteDownloadedStatisticsReport
     *
     * Cron by delete old reports, runs every day at 00
     */
    @Scheduled(cron = DELETE_DOWNLOADED_DOCUMENT_CRON_EXPRESSION)
    public void deleteDownloadedStatisticsReport() {

        Date startDate = Calendar.getInstance().getTime();
        ZonedDateTime dateTime = ZonedDateTime.now().minus(1, ChronoUnit.MONTHS);
        String pathFolder = fileBucketPath + DOWNLOAD_STATISTICS_FOLDER;
        File folder = new File(pathFolder);
        FileFilter filtro = file -> {
            boolean deleteFile = false;

            if ((file.lastModified() + (DELETE_TIME_DAYS * 24 * 60 * 60 * 1000)) < System.currentTimeMillis()) {
                deleteFile = true;
            }

            return deleteFile;
        };
        File[] listFiles = folder.listFiles(filtro);

        if (listFiles != null) {
            for (File file : listFiles) {
                try {
                    Files.delete(file.toPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        if (listFiles != null) {
            log.info("deleteDownloadedStatisticsReport {} LastUpdatedIsLessThan {} - Elapsed time: {} ms - Processed: {}",
                DELETE_DOWNLOADED_DOCUMENT_CRON_DESCRIPTION,
                dateTime, Calendar.getInstance().getTime().getTime() - startDate.getTime(), listFiles.length);
        }

        deleteReportNotification();
    }

    /**
     * generateExcelTransaction
     *
     * Generate excel by transaction
     *
     * @param query used by filter transactions
     * @param chartTitle report title
     * @param iterableTransactions transaction filtered
     * @param user user who creates the report
     */
    @Async
    public void generateExcelTransaction(String query, String chartTitle, Iterable<Transaction> iterableTransactions, ExtendedUser user) {

        Map<String, Object[]> statisticsInfo = getStatisticsInfo(query, chartTitle);

        statisticsInfo.put("3", translateService.getArrayMessageByUserParamsNull(StatisticsTransactionColumnsReport.getColumnsHeadersByRole(user.getRole()), user.getUser()));

        int positionRow = 4;
        for (Transaction transaction : iterableTransactions) {
            statisticsInfo.put(String.valueOf(positionRow), StatisticsTransactionColumnsReport.getColumnsValuesByRole(user.getRole(), transaction));
            positionRow++;
        }

        createExcelAnsSendNotification(chartTitle, statisticsInfo, user, "#,##0.000");
    }

    /**
     * generateExcelExtendedUser
     *
     * Generate excel by extended user
     *
     * @param query used by filter extended user
     * @param chartTitle report title
     * @param iterableExtendedUser extended user filtered
     * @param dateRelatedMap related date map
     * @param user user who creates the report
     */
    @Async
    public void generateExcelExtendedUser(String query, String chartTitle, Iterable<ExtendedUser> iterableExtendedUser, Map<Long, ZonedDateTime> dateRelatedMap, ExtendedUser user) {

        Map<String, Object[]> statisticsInfo = getStatisticsInfo(query, chartTitle);

        statisticsInfo.put("3", translateService.getArrayMessageByUserParamsNull(StatisticsUserColumnsReport.getColumnsHeadersByChartTitle(user.getRole(), chartTitle.toUpperCase()), user.getUser()));

        int positionRow = 4;
        for (ExtendedUser extendedUser : iterableExtendedUser) {
            statisticsInfo.put(String.valueOf(positionRow), StatisticsUserColumnsReport.getColumnsValuesByChartTitle(extendedUser, user.getRole(), chartTitle.toUpperCase(), dateRelatedMap));
            positionRow++;
        }
        log.info("generateExcelExtendedUser");
        createExcelAnsSendNotification(chartTitle, statisticsInfo, user, null);
        log.info("chartTitle: {}",chartTitle);
    }

    /**
     * getStatisticsInfo
     *
     * Create headers by excel
     *
     * @param query used by extract filter date
     * @param chartTitle report title
     * @return a map with title and filter
     */
    private Map<String, Object[]> getStatisticsInfo(String query, String chartTitle) {

        String rangeFilterDate;
        rangeFilterDate = query.substring(query.indexOf("[") + 1);
        rangeFilterDate = rangeFilterDate.substring(0, rangeFilterDate.indexOf("]"));

        String[] arrayChartTitle = {chartTitle};
        String[] dateRangeFilter = {"From " + rangeFilterDate.toLowerCase()};
        String[] emptyColumn = {""};

        Map<String, Object[]> statisticsInfo = new LinkedHashMap<>();

        statisticsInfo.put("0", arrayChartTitle);
        statisticsInfo.put("1", dateRangeFilter);
        statisticsInfo.put("2", emptyColumn);

        return statisticsInfo;
    }

    /**
     * createExcelAnsSendNotification
     *
     * Create excel and send notification
     *
     * @param chartTitle report title
     * @param statisticsInfo data by report
     * @param user user who creates the report
     * @param doubleNumberFormat format for double number in excel
     */
    private void createExcelAnsSendNotification(String chartTitle, Map<String, Object[]> statisticsInfo, ExtendedUser user, String doubleNumberFormat) {

        String pattern = "yyyy_MM_dd_HH_mm_ss_SS";
        String actualDate = ZonedDateTime.now().format(DateTimeFormatter.ofPattern(pattern));

        String fileName = fileBucketPath + DOWNLOAD_STATISTICS_FOLDER + actualDate + "-" + user.getUser().getId() + "-" + chartTitle + ".xlsx";
        log.info("fileName_ {}",fileName);
        try {

            FileUtils.writeDownWorkbook(fileName, chartTitle, statisticsInfo, doubleNumberFormat);

            notificationReceiverService.sendDownloadedFileNotification(chartTitle, fileName, user);

        } catch (IOException e) {
            e.printStackTrace();
            log.info("Error createExcelAnsSendNotification: {}",e.getMessage());
        }
    }

    /**
     * deleteReportNotification
     *
     * delete notifocation where category is downloaded file and creation date is greater than 7 days
     */
    private void deleteReportNotification() {
        Optional<List<Notification>> notificationList = notificationService.findAllByDownloadedFileAndDateCreatedLessThan(ZonedDateTime.now().minus(DELETE_TIME_DAYS, ChronoUnit.DAYS));

        if (notificationList.isPresent()) {
            List<NotificationReceiver> notificationReceiverList = new ArrayList<>();
            notificationList.get().forEach(notification ->
                notificationReceiverList.addAll(addNotificationReceiverList(notification.getId())));

            if (!notificationReceiverList.isEmpty()) {
                notificationReceiverList.forEach(notificationReceiver -> notificationReceiverService.delete(notificationReceiver.getId()));
            }

            notificationList.get().forEach(notification -> notificationService.delete(notification.getId()));
        }
    }

    /**
     * addNotificationReceiverList
     *
     * Verify if notification receiver is not null
     *
     * @param notificationId notification id for filter notification receiver
     * @return a notification receiver list
     */
    private List<NotificationReceiver> addNotificationReceiverList(Long notificationId) {
        List<NotificationReceiver> notificationReceiverListToReturn = new ArrayList<>();
        Optional<List<NotificationReceiver>> notificationReceiverList = notificationReceiverService.findAllByNotificationId(notificationId);
        if (notificationReceiverList.isPresent()) {
            notificationReceiverListToReturn = notificationReceiverList.get();
        }
        return notificationReceiverListToReturn;
    }

}


