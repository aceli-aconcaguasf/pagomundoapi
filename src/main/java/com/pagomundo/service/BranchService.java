package com.pagomundo.service;

import com.pagomundo.domain.Branch;
import com.pagomundo.repository.BranchRepository;
import com.pagomundo.repository.search.BranchSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link Branch}.
 */
@Service
@Transactional
public class BranchService {

    private final Logger log = LoggerFactory.getLogger(BranchService.class);

    private final BranchRepository branchRepository;

    private final BranchSearchRepository branchSearchRepository;

    public BranchService(BranchRepository branchRepository, BranchSearchRepository branchSearchRepository) {
        this.branchRepository = branchRepository;
        this.branchSearchRepository = branchSearchRepository;
    }

    /**
     * Save a branch.
     *
     * @param branch the entity to save.
     * @return the persisted entity.
     */
    public Branch save(Branch branch) {
        log.debug("Request to save Branch : {}", branch);
        Branch result = branchRepository.save(branch);
        branchSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the branches.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Branch> findAll(Pageable pageable) {
        log.debug("Request to get all Branches");
        return branchRepository.findAll(pageable);
    }


    /**
     * Get one branch by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Branch> findOne(Long id) {
        log.debug("Request to get Branch : {}", id);
        if (id == null) return Optional.empty();
        return branchRepository.findById(id);
    }

    /**
     * Delete the branch by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Branch : {}", id);
        branchRepository.deleteById(id);
        branchSearchRepository.deleteById(id);
    }

    /**
     * Search for the branch corresponding to the query.
     *
     * @param query    the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Branch> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Branches for query {}", query);
        return branchSearchRepository.search(queryStringQuery(query), pageable);
    }
}
