package com.pagomundo.service;

import com.pagomundo.domain.BankAccount;
import com.pagomundo.repository.BankAccountRepository;
import com.pagomundo.repository.search.BankAccountSearchRepository;
import com.pagomundo.repository.search.RefetchService;
import com.pagomundo.web.rest.errors.NotFoundAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link BankAccount}.
 */
@Service
@Transactional
public class BankAccountService {
    public static final String ENTITY_NAME = "BankAccount";
    public static final String NOT_FOUND_CODE = ENTITY_NAME + ".001";
    public static final String SHOULD_BE_NULL_CODE = ENTITY_NAME + ".002";
    public static final String SHOULD_NOT_BE_NULL_CODE = ENTITY_NAME + ".003";

    private final Logger log = LoggerFactory.getLogger(BankAccountService.class);

    private final BankService bankService;
    private final BankAccountRepository bankAccountRepository;
    private final BankAccountSearchRepository bankAccountSearchRepository;
    private final RefetchService refetchService;

    public BankAccountService(
        BankAccountRepository bankAccountRepository,
        BankService bankService,
        BankAccountSearchRepository bankAccountSearchRepository,
        RefetchService refetchService) {
        this.bankAccountRepository = bankAccountRepository;
        this.bankService = bankService;
        this.bankAccountSearchRepository = bankAccountSearchRepository;
        this.refetchService = refetchService;
    }

    /**
     * Save a bankAccount.
     *
     * @param bankAccount the entity to save.
     * @return the persisted entity.
     */
    public BankAccount save(BankAccount bankAccount) {
        log.debug("Request to save BankAccount : {}", bankAccount);
        BankAccount result = bankAccountRepository.save(bankAccount);
        bankAccountSearchRepository.save(result);
        return result;
    }

    /**
     * Create a bankAccount.
     *
     * @param ba the entity to create.
     * @return the persisted entity.
     */
    public BankAccount create(BankAccount ba) {
        log.debug("Request to create one BankAccount : {}", ba);
        ba.setBank(bankService.findOne(ba.getBank().getId()).orElseThrow(() ->
            new NotFoundAlertException(BankService.ENTITY_NAME + " id: " + ba.getBank().getId(), ENTITY_NAME, BankService.NOT_FOUND_CODE)));
        return save(ba);
    }

    /**
     * Update a bankAccount.
     *
     * @param ba the entity to create.
     * @return the persisted entity.
     */
    public BankAccount update(BankAccount ba) {
        log.debug("Request to update one BankAccount : {}", ba);

        BankAccount cba = findOne(ba.getId()).orElseThrow(() ->
            new NotFoundAlertException(ENTITY_NAME + ": " + ba.getId(), ENTITY_NAME, NOT_FOUND_CODE));
        log.debug("Current BankAccount : {}", cba);

        if (ba.getAccountNumber() != null) cba.setAccountNumber(ba.getAccountNumber());
        if (ba.getBankCommission() != null) cba.setBankCommission(ba.getBankCommission());
        if (ba.getFxCommission() != null) cba.setFxCommission(ba.getFxCommission());
        if (ba.getRechargeCost() != null) cba.setRechargeCost(ba.getRechargeCost());

        if (ba.getBank() != null)
            cba.setBank(bankService.findOne(ba.getBank().getId()).orElseThrow(() ->
                new NotFoundAlertException(BankService.ENTITY_NAME + " id: " + ba.getBank().getId(), ENTITY_NAME, BankService.NOT_FOUND_CODE)));

        return save(cba);
    }

    /**
     * Get all the bankAccounts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<BankAccount> findAll(Pageable pageable) {
        log.debug("Request to get all BankAccounts");
        return bankAccountRepository.findAll(pageable);
    }


    /**
     * Get one bankAccount by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<BankAccount> findOne(Long id) {
        log.debug("Request to get BankAccount : {}", id);
        if (id == null) return Optional.empty();
        return bankAccountRepository.findById(id);
    }

    /**
     * Delete the bankAccount by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete BankAccount : {}", id);
        bankAccountRepository.deleteById(id);
        bankAccountSearchRepository.deleteById(id);
    }

    /**
     * Search for the bankAccount corresponding to the query.
     *
     * @param query    the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<BankAccount> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of BankAccounts for query {}", query);
        return bankAccountSearchRepository.search(queryStringQuery(query), pageable);
    }

    @Transactional
    public void saveAll(List<BankAccount> bankAccountList) {
        if (!bankAccountList.isEmpty())
            bankAccountSearchRepository.saveAll(bankAccountList);
    }

    @Async
    public void refetch(Long id) {
        refetchService.refetchBankAccount(id);
    }

    @Async
    public void refetchAll(Collection<BankAccount> elements) {
        elements.forEach(e -> refetch(e.getId()));
    }
}
