package com.pagomundo.service;

import com.pagomundo.domain.Bank;
import com.pagomundo.domain.Currency;
import com.pagomundo.repository.CurrencyRepository;
import com.pagomundo.repository.search.CurrencySearchRepository;
import com.pagomundo.repository.search.RefetchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link Currency}.
 */
@Service
@Transactional
public class CurrencyService {

    private final Logger log = LoggerFactory.getLogger(CurrencyService.class);

    private final CurrencyRepository currencyRepository;
    private final CurrencySearchRepository currencySearchRepository;
    private final RefetchService refetchService;

    public CurrencyService(CurrencyRepository currencyRepository, CurrencySearchRepository currencySearchRepository, RefetchService refetchService) {
        this.currencyRepository = currencyRepository;
        this.currencySearchRepository = currencySearchRepository;
        this.refetchService = refetchService;
    }

    /**
     * Save a currency.
     *
     * @param currency the entity to save.
     * @return the persisted entity.
     */
    public Currency save(Currency currency) {
        log.debug("Request to save Currency : {}", currency);
        Currency result = currencyRepository.save(currency);
        currencySearchRepository.save(result);
        return result;
    }

    /**
     * Get all the currencies.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Currency> findAll(Pageable pageable) {
        log.debug("Request to get all Currencies");
        return currencyRepository.findAll(pageable);
    }


    /**
     * Get one currency by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Currency> findOne(Long id) {
        log.debug("Request to get Currency : {}", id);
        if (id == null) return Optional.empty();
        return currencyRepository.findById(id);
    }

    /**
     * Delete the currency by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Currency : {}", id);
        currencyRepository.deleteById(id);
        currencySearchRepository.deleteById(id);
    }

    /**
     * Search for the currency corresponding to the query.
     *
     * @param query    the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Currency> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Currencies for query {}", query);
        return currencySearchRepository.search(queryStringQuery(query), pageable);
    }

    @Transactional
    public void saveAll(List<Currency> currencyList) {
        if (!currencyList.isEmpty())
            currencySearchRepository.saveAll(currencyList);
    }

    @Async
    public void refetch(Long id) {
        refetchService.refetchCurrency(id);
    }

    @Async
    public void refetchAll(Collection<Bank> elements) {
        elements.forEach(e -> refetch(e.getId()));
    }
}
