package com.pagomundo.service;

import com.pagomundo.config.Constants;
import com.pagomundo.domain.Authority;
import com.pagomundo.domain.Country;
import com.pagomundo.domain.DTO.UserListDTO;
import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.ExtendedUserRelation;
import com.pagomundo.domain.User;
import com.pagomundo.domain.enumeration.LanguageKeyUser;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.repository.AuthorityRepository;
import com.pagomundo.repository.ExtendedUserRepository;
import com.pagomundo.repository.UserRepository;
import com.pagomundo.repository.ExtendedUserRelationRepository;
import com.pagomundo.repository.search.RefetchService;
import com.pagomundo.repository.search.UserSearchRepository;
import com.pagomundo.security.AuthoritiesConstants;
import com.pagomundo.security.SecurityUtils;
import com.pagomundo.service.dto.EmailChangeDTO;
import com.pagomundo.service.dto.UserDTO;
import com.pagomundo.service.util.RandomUtil;
import com.pagomundo.web.rest.errors.BadRequestAlertException;
import com.pagomundo.web.rest.errors.NotFoundAlertException;
import com.pagomundo.web.rest.errors.UserNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class UserService {
    public static final String ENTITY_NAME = "User";
    public static final String USER_NOT_FOUND_CODE = ENTITY_NAME + ".001";
    public static final String EMAIL_ALREADY_EXISTS_CODE = ENTITY_NAME + ".002";
    public static final String EMAIL_NOT_REGISTERED_CODE = ENTITY_NAME + ".003";
    public static final String PASSWORD_INCORRECT_CODE = ENTITY_NAME + ".004";
    public static final String LOGIN_NAME_ALREADY_USED_CODE = ENTITY_NAME + ".005";
    public static final String USER_NOT_ACTIVATED_CODE = ENTITY_NAME + ".006";
    public static final String USER_KEY_NOT_FOUND_CODE = ENTITY_NAME + ".007";
    public static final String UNAUTHORIZED_USER_CODE = ENTITY_NAME + ".008";
    public static final String SHOULD_BE_NULL_CODE = ENTITY_NAME + ".009";
    public static final String EMAIL_INVALID_CODE = ENTITY_NAME + ".010";
    public static final String USER_KEY_EXPIRED_CODE = ENTITY_NAME + ".011";
    public static final String BAD_REQUEST = ENTITY_NAME + ".002";
    public static final String WRONG_AUTHORITY = "Wrong authority";
    public static final String MISSING_INFO = "Missing info";
    public static final String CREATION_LIMIT_EXCEEDED = "Creation limit exceeded";
    private static final String USER_NOT_FOUND = "User not found";
    private final Logger log = LoggerFactory.getLogger(UserService.class);

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserSearchRepository userSearchRepository;
    private final AuthorityRepository authorityRepository;
    private final ExtendedUserRepository extendedUserRepository;
    private final ExtendedUserRelationService extendedUserRelationService;
    private final RefetchService refetchService;
    private final TranslateService translateService;
    private final ExtendedUserRelationRepository extendedUserRelationRepository;

    private final String STRING_NEW_ACCOUNT = "-N";
    
    private final int EXPIRED_LINK_HOURS = 12;

    public UserService(UserRepository userRepository,
                        PasswordEncoder passwordEncoder,
                        UserSearchRepository userSearchRepository,
                        AuthorityRepository authorityRepository,
                        ExtendedUserRepository extendedUserRepository,
                        ExtendedUserRelationService extendedUserRelationService,
                        RefetchService refetchService,
                        TranslateService translateService,
                        ExtendedUserRelationRepository extendedUserRelationRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.userSearchRepository = userSearchRepository;
        this.authorityRepository = authorityRepository;
        this.extendedUserRepository = extendedUserRepository;
        this.extendedUserRelationService = extendedUserRelationService;
        this.refetchService = refetchService;
        this.translateService = translateService;
        this.extendedUserRelationRepository = extendedUserRelationRepository;
    }

    public Optional<User> activateRegistration(String key) {
        log.debug("Activating user for activation key {}", key);
        return userRepository.findOneByActivationKey(key)
            .map(user -> {
                // activate given user for the registration key.
                user.setActivated(true);
                user.setActivationKey(null);
                userSearchRepository.save(user);
                log.debug("Activated user: {}", user);
                return user;
            });
    }

    public Optional<User> completePasswordReset(String newPassword, String key) {
        log.debug("Reset user password for reset key {}", key);
        
        return userRepository.findOneByResetKey(key)
            .filter(user -> user.getResetDate().isAfter(Instant.now().minusSeconds(7 * 24 * 60 * 60L)))
            .map(user -> {
                user.setPassword(passwordEncoder.encode(newPassword));
                user.setResetKey(null);
                user.setResetDate(null);
                return user;
            });
    }

    public Optional<User> changeEmail(EmailChangeDTO emailChangeDTO) {
        return userRepository.findOneByEmailIgnoreCase(emailChangeDTO.getCurrentEmail())
            .filter(User::getActivated)
            .map(user -> {
                user.setLogin(emailChangeDTO.getNewEmail());
                user.setEmail(emailChangeDTO.getNewEmail());
                return user;
            });
    }

    public Optional<User> requestPasswordReset(String mail) {
        log.debug("------------>>>>>>>>MAIL", mail);
        return userRepository.findOneByEmailIgnoreCase(mail)
            .map(user -> {
                String resetKey = RandomUtil.generateResetKey();
                if (verifyNewAccountByValidationKey(user.getResetKey())) {
                    resetKey = resetKey.substring(0, resetKey.length() - 2);
                    user.setResetKey(resetKey + STRING_NEW_ACCOUNT);
                } else {
                    user.setResetKey(resetKey);
                }
                user.setResetDate(Instant.now());
                return user;
            });
    }

    public User registerUser(UserDTO userDTO, String password) {
        userRepository.findOneByLogin(userDTO.getLogin().toLowerCase()).ifPresent(existingUser -> {
            boolean removed = removeNonActivatedUser(existingUser);
            if (!removed) {
                throw new UsernameAlreadyUsedException(userDTO.getLogin());
            }
        });
        userRepository.findOneByEmailIgnoreCase(userDTO.getEmail()).ifPresent(existingUser -> {
            boolean removed = removeNonActivatedUser(existingUser);
            if (!removed) {
                throw new EmailAlreadyUsedException(userDTO.getEmail());
            }
        });

        log.debug("====> toWhom: {}", userDTO.getToWhom());

        User newUser = new User();
        String encryptedPassword = passwordEncoder.encode(password);
        newUser.setLogin(userDTO.getLogin().toLowerCase());
        // new user gets initially a generated password
        newUser.setPassword(encryptedPassword);
        newUser.setFirstName(userDTO.getFirstName());
        newUser.setLastName(userDTO.getLastName());
        newUser.setEmail(userDTO.getEmail().toLowerCase());
        newUser.setImageUrl(userDTO.getImageUrl());
        newUser.setLangKey(userDTO.getLangKey());
        // new user is not active
        newUser.setActivated(false);
        // new user gets registration key
        newUser.setActivationKey(RandomUtil.generateActivationKey());
        Set<Authority> authorities = new HashSet<>();
        authorityRepository.findById(AuthoritiesConstants.USER).ifPresent(authorities::add);
        newUser.setAuthorities(authorities);
        userRepository.save(newUser);
        userSearchRepository.save(newUser);
        log.debug("Created Information for User: {}", newUser);
        return newUser;
    }

    private boolean removeNonActivatedUser(User existingUser) {
        if (existingUser.getActivated()) return false;

        userRepository.delete(existingUser);
        userRepository.flush();
        return true;
    }

    private void validate(UserDTO userDTO) {
        if (userDTO.getEmail() == null || !userDTO.getEmail().matches(Constants.LOGIN_REGEX))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.account.user.email.regular.expression") + Constants.LOGIN_REGEX + "'", ENTITY_NAME, "Required");
        if (userDTO.getFirstName() == null || userDTO.getFirstName().length() <= 0)
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.first.name.required"), ENTITY_NAME, "Required");
        //if (userDTO.getLastName() == null)
        //    throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.last.name.required"), ENTITY_NAME, "Required");
    }

    private boolean isUserHasRole(UserDTO userDTO, User user) {
        String role = userDTO.getRole();
        if (user != null &&
            (
                (role.equals(AuthoritiesConstants.PAYEE) && isPayee(user)) ||
                    (role.equals(AuthoritiesConstants.MERCHANT) && isMerchant(user)) ||
                    (role.equals(AuthoritiesConstants.RESELLER) && isReseller(user)) ||
                    (role.equals(AuthoritiesConstants.ADMIN) && isAdmin(user)) ||
                    (role.equals(AuthoritiesConstants.SUPER_ADMIN) && isSuperAdmin(user))
            )) {
            return role.equals(AuthoritiesConstants.PAYEE);
        } else return false;
    }

    private UserDTO createUser(UserDTO userDTO) {

        log.info(">>>>>>>>>>>>>>>>>> inside createUser function {}" + userDTO);

        userDTO.setEmail(userDTO.getEmail().trim().toLowerCase());
        userDTO.setFirstName(userDTO.getFirstName().trim());
        userDTO.setLastName(userDTO.getLastName().trim());

        log.info("=__====> toWhom: {}", userDTO.getToWhom());

        try {
            validate(userDTO);
            log.info(">>>>>>>>>>>>>>>>>> after validate");

        } catch (BadRequestAlertException brae) {
            log.debug("userDTO data validation: {}", brae.getMessage());
            userDTO.setStatus(Status.CANCELLED);
            userDTO.setStatusReason(brae.getMessage());
            return userDTO;
        }

        Optional<User> optionalUser = userRepository.findOneByEmailIgnoreCase(userDTO.getEmail());

        log.info(">>>>>>>>>>>>>>>>>> optionalUser {}", optionalUser);

        if (optionalUser.isPresent()) {        	
            log.info(">>>>>>>>>>>>>>>>>> optionalUser isPresent {}", optionalUser.isPresent());
            User user = optionalUser.get();
            log.info("User found: {}",user.getLogin());
            userDTO.setId(user.getId());
            userDTO.setUser(user);

            if (isUserHasRole(userDTO, user)) {
                userDTO.setStatus(Status.ACCEPTED);
                userDTO.setStatusReason(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.already.exist.email") + " " + userDTO.getEmail().toLowerCase());
            } else {
                userDTO.setStatus(Status.REJECTED);
                userDTO.setStatusReason(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.already.exist.email") + " " + userDTO.getEmail().toLowerCase() + " " + translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.already.exist.email.not.associated"));
            }

            Optional<ExtendedUser> extUser=extendedUserRepository.findOneByEmailIgnoreCase(user.getEmail());
            if(extUser.isPresent() &&  extUser.get().getStatus().equals(Status.CANCELLED)) {
            	log.info("User cancelled: {}",user.getLogin());
            	userDTO.setStatus(Status.CANCELLED);
                userDTO.setStatusReason(translateService.getMessageByCurrentUserParams("error.message.exception.user.already.exist.cancelled.email",new String[]{user.getEmail()}));
            }

            return userDTO;
        }

        // validation for nickname
        if  (userDTO.getToWhom().getId() != null
            && userDTO.getNickname() != null
            && !userDTO.getNickname().equals("")) {

            List<ExtendedUserRelation> optionalEUR = extendedUserRelationRepository.findAllByExtendedUserIdAndNickname(userDTO.getToWhom().getId(), userDTO.getNickname());
            if (optionalEUR.size() > 0) {
                User user = optionalEUR.get(0).getUserRelated();
                userDTO.setId(user.getId());
                userDTO.setUser(user);
                if (isUserHasRole(userDTO, user)) {
                    userDTO.setStatus(Status.ACCEPTED);
                    userDTO.setStatusReason(translateService.getMessageByCurrentUserParamsNull("error.message.exception.payee.nickname.already.used") + " " + userDTO.getNickname().toLowerCase());
                } else {
                    userDTO.setStatus(Status.REJECTED);
                    userDTO.setStatusReason(translateService.getMessageByCurrentUserParamsNull("error.message.exception.payee.nickname.already.used") + " " + userDTO.getNickname().toLowerCase() + " " + translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.already.exist.email.not.associated"));
                }
                return userDTO;
            }

        }

        log.info(">>>>>>>>>>>>>>>>>> after validation for nickname");

        //modificaciones login
        User savedUser = userRepository.save(setUserData(userDTO));

        log.info(">>>>>>>>>>>>>>>>>> after save user");

        userSearchRepository.save(savedUser);

        log.info(">>>>>>>>>>>>>>>>>> after save user in elastic search");

        log.info("Created Information for User: {}", savedUser);

        log.debug("=====__======> Created Information for User ID: {}", savedUser.getId());

        userDTO.setUser(savedUser);
        userDTO.setId(savedUser.getId());

        log.info("=====__======> Created Information for DTO User: {}", userDTO);
        log.info("=====__======> Created Information for DTOUser ID: {}", userDTO.getId());

        userDTO.setStatus(Status.CREATED);
        userDTO.setResetKey(savedUser.getResetKey());

        log.info(">>>>>>>>>>>>>>>>>> after setStatus and set resetkey");
        
        return userDTO;
    }

    private User setUserData(UserDTO userDTO) {
        String resetKey = RandomUtil.generateResetKey();
        User user = new User();
        user.setRole(userDTO.getRole());
        user.setEmail(userDTO.getEmail());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setLogin(userDTO.getEmail());
        user.setPayeeAsCompany(userDTO.isPayeeAsCompany() != null ? userDTO.isPayeeAsCompany() : false);
        user.setImageUrl(userDTO.getImageUrl());
        user.setLangKey(userDTO.getRole().equals(AuthoritiesConstants.PAYEE) ? getLangKeyByCountry(userDTO.getCountry()) : Constants.DEFAULT_LANGUAGE);
        user.setAuthorities(userDTO.getAuthorities().stream().map(authorityRepository::findById)
            .filter(Optional::isPresent).map(Optional::get).collect(Collectors.toSet()));
        user.setPassword(passwordEncoder.encode(RandomUtil.generatePassword()));
        user.setResetKey(resetKey.substring(0, resetKey.length() - 2) + STRING_NEW_ACCOUNT);
        user.setActivationKey(user.getResetKey());
        user.setResetDate(Instant.now());
        //Se debe crear desactivado
        user.setActivated(false);
        user.setStatus(Status.CREATED);

        Long toWhomId = userDTO.getToWhom().getId();

        if(toWhomId == 3208 || toWhomId == 7217){

            log.debug("=__====> Creating user of NOBETEK LLC");
            user.setExtraURLParameter("&pagomundo=1");

        }else {

            Optional<ExtendedUser> optionalToWhomUser = extendedUserRepository.findById(toWhomId);
            ExtendedUser toWhomUser = new ExtendedUser();
            if(optionalToWhomUser.isPresent()){
                toWhomUser = optionalToWhomUser.get();
                Long toWhomUserId = toWhomUser.getUser().getId();
                log.debug("=__====> User ID {}", toWhomUserId);
                List<ExtendedUserRelation> eur = extendedUserRelationRepository.findAllByUserRelatedId(toWhomUserId);
                if(eur.size() > 0){
                    for(int i = 0; i < eur.size(); i++){
                        Long resellerId = eur.get(i).getExtendedUser().getId();

                        

                        log.debug("=__====> Reseller ID {}", resellerId);

                        if(resellerId == 3208 || toWhomId == 7217){
                            log.debug("=__====> Creating user related with NOBETEK LLC");
                            user.setExtraURLParameter("&pagomundo=1");
                            break;
                        } else {
                            user.setExtraURLParameter("");
                        }
                    }
                } else {
                    user.setExtraURLParameter("");
                }
            } else {
                user.setExtraURLParameter("");
            }
        }

        return user;
    }

    /**
     * Create several users.
     *
     * @param userDTOList the group of entity to create.
     * @return the persisted entity.
     */
    public UserListDTO create(UserListDTO userDTOList) {
        
        log.info(">>>>>>>>>>>>>>> Request to create the list of Users : {}", userDTOList);

        List<UserDTO> userNoCreated = userDTOList.getUserNotCreated();
        List<UserDTO> userListCreated = new ArrayList<>();
        List<UserDTO> userListAlreadyExist = new ArrayList<>();
        List<UserDTO> userExistingAndNotAssociated = new ArrayList<>();

        log.info(">>>>>>>>>>>>>>>>>> after userNoCreated cal {}l", userNoCreated);

        userDTOList.getUsers().forEach(userDTO -> {

                log.info("=============___=========> {}", userDTO);
                log.debug("=============___=========> {}", userDTO);
                userDTO.setAuthorities(new HashSet<>());
                userDTO.getAuthorities().add(userDTOList.getUsersRole());
                userDTO.setRole(userDTOList.getUsersRole());
                userDTO.setToWhom(userDTOList.getToWhom());
                userDTO.setCountry(userDTOList.getCountry());

                log.info("=============___=========> before created user");
                createUser(userDTO);
                log.info("=============___=========> after created user");

                log.info("=====__======> After Created Information for DTO User: {}", userDTO);
                log.info("=====__======> After Created Information for DTOUser ID: {}", userDTO.getId());

                switch ((userDTO.getStatus() != null ? userDTO.getStatus() : Status.CANCELLED)) {
                    case CREATED:
                        userListCreated.add(userDTO);
                        break;
                    case ACCEPTED:
                        userListAlreadyExist.add(userDTO);
                        break;
                    case REJECTED:
                        userExistingAndNotAssociated.add(userDTO);
                        break;
                    case CANCELLED:
                    default:
                        userNoCreated.add(userDTO);
                        break;
                }

                log.info(">>>>>>>>>>>>>>>>>> after set STATUS");
            }
        );

        if (userDTOList.getUsersRole().equals(AuthoritiesConstants.PAYEE) && isMerchant(userDTOList.getToWhom().getUser()) ||
            userDTOList.getUsersRole().equals(AuthoritiesConstants.MERCHANT) && isReseller(userDTOList.getToWhom().getUser())) {
            userListCreated.forEach(userDTO -> extendedUserRelationService.link(
                userDTO.getUser(), userDTOList.getToWhom(), getCurrentExtendedUser(), Status.ACCEPTED, userDTO.getNickname()));
            userListAlreadyExist.forEach(userDTO -> extendedUserRelationService.link(
                userDTO.getUser(), userDTOList.getToWhom(), getCurrentExtendedUser(), Status.ACCEPTED, userDTO.getNickname()));
        }

        userDTOList.setUsersCreatedAndAssociated(userListCreated);
        userDTOList.setUserExistingAndAssociated(userListAlreadyExist);
        userDTOList.setUserExistingAndNotAssociated(userExistingAndNotAssociated);
        userDTOList.setUserNotCreated(userNoCreated);

        for(UserDTO udto: userListCreated){
            log.info("=====__======> Before Return DTOUser ID: {}", udto.getId());
        }

        log.info("=====__======> userDTOList: {}", userDTOList.getUsersCreatedAndAssociated());

        return userDTOList;
    }

    private boolean isSuperAdmin() {
        return getUserWithAuthorities().map(UserDTO::new)
            .orElseThrow(() -> new UserNotFoundException(USER_NOT_FOUND))
            .getAuthorities().contains(AuthoritiesConstants.SUPER_ADMIN);
    }

    public boolean isAdmin() {
        return getUserWithAuthorities().map(UserDTO::new)
            .orElseThrow(() -> new UserNotFoundException(USER_NOT_FOUND))
            .getAuthorities().contains(AuthoritiesConstants.ADMIN);
    }

    public boolean isReseller() {
        return getUserWithAuthorities().map(UserDTO::new)
            .orElseThrow(() -> new UserNotFoundException(USER_NOT_FOUND))
            .getAuthorities().contains(AuthoritiesConstants.RESELLER);
    }

    public boolean isMerchant() {
        return getUserWithAuthorities().map(UserDTO::new)
            .orElseThrow(() -> new UserNotFoundException(USER_NOT_FOUND))
            .getAuthorities().contains(AuthoritiesConstants.MERCHANT);
    }

    public boolean isPayee() {
        return getUserWithAuthorities().map(UserDTO::new)
            .orElseThrow(() -> new UserNotFoundException(USER_NOT_FOUND))
            .getAuthorities().contains(AuthoritiesConstants.PAYEE);
    }

    private boolean isPayee(String login) {
        return getUserWithAuthorities(login).map(UserDTO::new)
            .orElseThrow(() -> new UserNotFoundException(login))
            .getAuthorities().contains(AuthoritiesConstants.PAYEE);
    }

    private boolean isMerchant(String login) {
        return getUserWithAuthorities(login).map(UserDTO::new)
            .orElseThrow(() -> new UserNotFoundException(login))
            .getAuthorities().contains(AuthoritiesConstants.MERCHANT);
    }

    private boolean isReseller(String login) {
        return getUserWithAuthorities(login).map(UserDTO::new)
            .orElseThrow(() -> new UserNotFoundException(login))
            .getAuthorities().contains(AuthoritiesConstants.RESELLER);
    }

    public boolean isMerchant(User user) {
        return isMerchant(user.getLogin());
    }

    public boolean isReseller(User user) {
        return isReseller(user.getLogin());
    }

    private boolean isAdmin(String login) {
        return getUserWithAuthorities(login).map(UserDTO::new)
            .orElseThrow(() -> new UserNotFoundException(login))
            .getAuthorities().contains(AuthoritiesConstants.ADMIN);
    }

    private boolean isSuperAdmin(String login) {
        return getUserWithAuthorities(login).map(UserDTO::new)
            .orElseThrow(() -> new UserNotFoundException(login))
            .getAuthorities().contains(AuthoritiesConstants.SUPER_ADMIN);
    }

    public boolean isAdminOrSuperAdmin(User user) {
        return isAdmin(user) || isSuperAdmin(user);
    }

    boolean isAdmin(User user) {
        return isAdmin(user.getLogin());
    }

    public boolean isSuperAdmin(User user) {
        return isSuperAdmin(user.getLogin());
    }

    boolean isAdmin(boolean orSuperAdmin) {
        if (orSuperAdmin) return isAdmin() || isSuperAdmin();
        return isAdmin();
    }

    boolean isPayee(User user) {
        return isPayee(user.getLogin());
    }

    /**
     * Update basic information (first name, last name, email, language) for the current user.
     *
     * @param firstName first name of user.
     * @param lastName last name of user.
     * @param email email id of user.
     * @param langKey language key.
     * @param imageUrl image URL of user.
     */
    public void updateUser(String firstName, String lastName, String email, String langKey, String imageUrl) {
        SecurityUtils.getCurrentUserLogin()
            .flatMap(userRepository::findOneByLogin)
            .ifPresent(user -> {
                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setEmail(email.toLowerCase());
                user.setLangKey(langKey);
                user.setImageUrl(imageUrl);
                userSearchRepository.save(user);
                log.debug("Changed Information for User: {}", user);
            });
    }

    /**
     * Update all information for a specific user, and return the modified user.
     *
     * @param user user to update.
     * @return updated user.
     */
    public User save(User user) {
        log.debug("Request to save user : {}", user);
        user.setLastModifiedDate(Instant.now());
        User result = userRepository.save(user);
        userSearchRepository.save(result);
        return result;
    }

    /**
     * Update all information for a specific user, and return the modified user.
     *
     * @param userDTO user to update.
     * @return updated user.
     */
    public Optional<UserDTO> updateUser(UserDTO userDTO) {
        return Optional.of(userRepository
            .findById(userDTO.getId()))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(user -> {
                user.setLogin(userDTO.getLogin().trim().toLowerCase());
                user.setFirstName(userDTO.getFirstName().trim());
                user.setLastName(userDTO.getLastName().trim());
                user.setEmail(userDTO.getEmail().trim().toLowerCase());
                user.setImageUrl(userDTO.getImageUrl());
                user.setActivated(userDTO.isActivated());
                user.setLangKey(userDTO.getLangKey());
                if (userDTO.getAuthorities() != null) {
                    Set<Authority> managedAuthorities = user.getAuthorities();
                    managedAuthorities.clear();
                    userDTO.getAuthorities().stream()
                        .map(authorityRepository::findById)
                        .filter(Optional::isPresent)
                        .map(Optional::get)
                        .forEach(managedAuthorities::add);
                }
                userSearchRepository.save(user);
                log.debug("Changed Information for User: {}", user);
                return user;
            })
            .map(UserDTO::new);
    }

    public void deleteUser(String login) {
        userRepository.findOneByLogin(login).ifPresent(user -> {
            userRepository.delete(user);
            userSearchRepository.delete(user);
            log.debug("Deleted User: {}", user);
        });
    }

    public void changePassword(String currentClearTextPassword, String newPassword) {
        SecurityUtils.getCurrentUserLogin()
            .flatMap(userRepository::findOneByLogin)
            .ifPresent(user -> {
                String currentEncryptedPassword = user.getPassword();
                if (!passwordEncoder.matches(currentClearTextPassword, currentEncryptedPassword)) {
                    throw new InvalidPasswordException();
                }
                String encryptedPassword = passwordEncoder.encode(newPassword);
                user.setPassword(encryptedPassword);
                log.debug("Changed password for User: {}", user);
            });
    }

    @Transactional(readOnly = true)
    public Page<UserDTO> getAllManagedUsers(Pageable pageable) {
        return userRepository.findAllByLoginNot(pageable, Constants.ANONYMOUS_USER).map(UserDTO::new);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthoritiesByLogin(String login) {
        return userRepository.findOneWithAuthoritiesByLogin(login);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthorities() {
        return SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneWithAuthoritiesByLogin);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthorities(String login) {
        if (login == null) return getUserWithAuthorities();
        return Optional.of(login).flatMap(userRepository::findOneWithAuthoritiesByLogin);
    }

    /**
     * Not activated users should be automatically deleted after 3 days.
     * <p>
     * This is scheduled to get fired everyday, at 01:00 (am).
     */
    @Scheduled(cron = "0 0 1 * * ?")
    public void removeNotActivatedUsers() {
        userRepository
            .findAllByActivatedIsFalseAndActivationKeyIsNotNullAndCreatedDateBefore(Instant.now().minus(3, ChronoUnit.DAYS))
            .forEach(user -> {
                log.debug("Deleting not activated user {}", user.getLogin());
                userRepository.delete(user);
                userSearchRepository.delete(user);
            });
    }

    /**
     * Gets a list of all the authorities.
     *
     * @return a list of all the authorities.
     */
    public List<String> getAuthorities() {
        return authorityRepository.findAll().stream().map(Authority::getName).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public ExtendedUser getCurrentExtendedUser() {
        UserDTO userDTO = getUserWithAuthorities()
            .map(UserDTO::new)
            .orElseThrow(() -> new UserNotFoundException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.account.user.not.found")));
        return extendedUserRepository.getByUserId(userDTO.getId());
    }

    /**
     * Get one user by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<User> findOne(Long id) {
        log.debug("Request to get User : {}", id);
        if (id == null) return Optional.empty();
        return userRepository.findById(id);
    }

    public String getRole(User user) {
        if (isSuperAdmin(user)) return AuthoritiesConstants.SUPER_ADMIN;
        if (isAdmin(user)) return AuthoritiesConstants.ADMIN;
        if (isMerchant(user)) return AuthoritiesConstants.MERCHANT;
        if (isReseller(user)) return AuthoritiesConstants.RESELLER;
        if (isPayee(user)) return AuthoritiesConstants.PAYEE;
        return AuthoritiesConstants.USER;
    }

    @Async
    public void refetch(Long id) {
        refetchService.refetchUser(id);
    }

    @Async
    public void refetchAll(Collection<User> elements) {
        elements.forEach(e -> refetch(e.getId()));
    }

    Optional<User> changeEmailNotification(EmailChangeDTO emailChangeDTO) {
        return userRepository.findOneByEmailIgnoreCase(emailChangeDTO.getCurrentEmail())
            .filter(User::getActivated)
            .map(user -> {
                user.setActivationKey(RandomUtil.generateResetKey());
                user.setNewEmail(emailChangeDTO.getNewEmail());
                return user;
            });
    }

    public Optional<User> changeEmailUserByActivationKey(String activationKey) {
        log.debug("Change user email for activation key {}", activationKey);
        return userRepository.findOneByActivationKey(activationKey)
            .map(user -> {
                user.setEmail(user.getNewEmail());
                user.setLogin(user.getNewEmail());
                user.setActivationKey(null);
                user.setNewEmail(null);
                return user;
            });
    }

    public User updateUserFirstNameLastNameAndLangKey(User user, String firstName, String lastName, Country country) {
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setLangKey(getLangKeyByCountry(country));
        save(user);
        refetch(user.getId());
        return user;
    }

    private String getLangKeyByCountry(Country country) {
        String langKey = LanguageKeyUser.LANGUAGE_ENGLISH.getLangKey();
        if (country != null) {
            if(country.getLangKey()!=null&&!"".equals(country.getLangKey().trim())){
                langKey = country.getLangKey();
            }else{
            switch (country.getId().intValue()) {
                case 1: // COLOMBIA
                case 2: // MEXICO
                case 77: // CHILE
                case 66: // ARGENTINA
                case 101: // PERU
                    langKey = LanguageKeyUser.LANGUAGE_SPANISH.getLangKey();
                    break;
                case 74: // Brasil
                    langKey = LanguageKeyUser.LANGUAGE_PORTUGUESE.getLangKey();
                    break;
                default:
                    break;
            }
            }
        }
        return langKey;
    }

    boolean verifyNewAccountByValidationKey(String validationKey) {
        boolean isNewAccount = false;
        if (validationKey != null) {
            String lastCharacters = validationKey.substring(validationKey.length() - 2);
            if (STRING_NEW_ACCOUNT.equals(lastCharacters)) {
                isNewAccount = true;
            }
        }
        return isNewAccount;
    }

    public void updateLangKeyByUser(Long userId, String langKey) {
        Optional<User> user = userRepository.findById(userId);
        if (!user.isPresent())
            throw new UserNotFoundException(translateService.getMessageByCurrentUserParamsNull("error.message.user.not.exist"));
        if (LanguageKeyUser.verifyLangKey(langKey)) {
            log.debug("Setting new language");
            user.get().setLangKey(langKey);
            log.debug("Language set");
            log.debug("Saving user with new language");
            userSearchRepository.save(user.get());
            log.debug("User saved");
            log.debug("Changed langKey for User: {}", user);
        } else {
            log.error("Error updating user with id: {},langKey {}", user.get().getId(), langKey != null ? langKey + " is not valid" : " can not be NULL");
        }
    }

    public User verifyKey(String key) {
        Optional<User> user = userRepository.findOneByResetKey(key);
        if(!user.isPresent()){
            throw new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.account.user.not.exist.reset.key") ,
                UserService.ENTITY_NAME, UserService.USER_KEY_NOT_FOUND_CODE);
        }
        else{
            if(user.get().getResetDate().compareTo(Instant.now().minus(EXPIRED_LINK_HOURS,ChronoUnit.HOURS))<0)throw new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.account.user.reset.key") ,
                UserService.ENTITY_NAME, UserService.USER_KEY_EXPIRED_CODE);
        }
        return user.get();
    }
    
    public User findUserByLogin(String login) {
    	log.info("buscando user");
    	Optional<User> user=userRepository.findOneByLogin(login);
    	return (user.isPresent()?user.get():null);    	
    }
    public void changeToken(String login, String newToken,Date validity) {
    	 log.info("try Changed token for login:{} validity: {}", login,validity);
    	 Calendar calendar = Calendar.getInstance();
    	    calendar.setTime(validity);
    	    calendar.add(Calendar.HOUR_OF_DAY, 1);
    	 Date aux=calendar.getTime();
    	 log.info("try validity 1 hour: {}", aux);
    	userRepository.findOneByLogin(login)
            .ifPresent(user -> {
                user.setCurrentToken(newToken);                
                user.setTokenValidity(aux);
                log.info("Changed token for User: {}", user);
            });
    }
}
