package com.pagomundo.service;

public class UsernameAlreadyUsedException extends RuntimeException {

    public UsernameAlreadyUsedException() {
        super("Login name already used!");
    }

    public UsernameAlreadyUsedException(String message) {
        super("Login name already used: " + message);
    }

}
