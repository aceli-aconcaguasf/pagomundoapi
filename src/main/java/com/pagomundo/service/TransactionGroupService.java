package com.pagomundo.service;

import com.pagomundo.domain.BankAccount;
import com.pagomundo.domain.DTO.*;
import com.pagomundo.domain.DTO.Ripple.*;
import com.pagomundo.domain.DirectPaymentBank;
import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.User;
import com.pagomundo.domain.Country;
import com.pagomundo.domain.Transaction;
import com.pagomundo.domain.TransactionGroup;
import com.pagomundo.domain.ThresholdPayee;
import com.pagomundo.domain.enumeration.BankAccountType;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.domain.enumeration.stp.StpAccountType;
import com.pagomundo.domain.enumeration.stp.StpBankId;
import com.pagomundo.repository.TransactionGroupRepository;
import com.pagomundo.repository.TransactionRepository;
import com.pagomundo.repository.search.TransactionGroupSearchRepository;
import com.pagomundo.repository.ThresholdPayeeRepository;
import com.pagomundo.repository.ExtendedUserRepository;
import com.pagomundo.repository.UserRepository;
import com.pagomundo.service.dto.integrations.stp.IntegrationStpDTO;
import com.pagomundo.service.integrations.stp.IntegrationStpService;
import com.pagomundo.service.util.FileUtils;
import com.pagomundo.web.rest.errors.BadRequestAlertException;
import com.pagomundo.web.rest.errors.NotFoundAlertException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import org.apache.commons.lang3.SerializationUtils;
import org.springframework.web.client.DefaultResponseErrorHandler;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;
import java.util.Arrays;
import java.time.LocalDateTime;
import com.google.gson.JsonObject;
import com.google.gson.JsonElement;

import java.util.HashMap;
import java.util.Map;

import java.net.URI;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

import java.time.Instant;
import java.util.Date;
import java.time.ZoneId;
import java.time.LocalDate;
import java.util.regex.Matcher;

/**
 * Service Implementation for managing {@link TransactionGroup}.
 */
@Service
@Transactional
public class TransactionGroupService {
    public static final String ENTITY_NAME = "transactionGroup";
    public static final String NOT_FOUND = ENTITY_NAME + ".001";
    
    private final Logger log = LoggerFactory.getLogger(TransactionGroupService.class);
    private final TransactionGroupRepository transactionGroupRepository;
    private final BankAccountService bankAccountService;
    private final TransactionService transactionService;
    private final ExtendedUserService extendedUserService;
    private final DirectPaymentBankService directPaymentBankService;
    private final TransactionGroupSearchRepository transactionGroupSearchRepository;
    private final IntegrationStpService integrationStpService;
    private final TranslateService translateService;
    /*private final ThresholdPayeeRepository thresholdPayeeRepository;
    private final TransactionRepository transactionRepository;
    private final ExtendedUserRepository extendedUserRepository;
    private final UserRepository userRepository;*/
    
    @Value("${fileBucket.path}")
    private String fileBucketPath;
    
    @Value("${colombia.ITAU.pmi-address}")
    private String pmiAddressForITAUColombia;
    
    @Value("${colombia.ITAU.pmi-phone-number}")
    private String pmiPhoneNumberForITAUColombia;
    
    @Value("${colombia.ITAU.pmi-client-number}")
    private String pmiClientNumberForITAUColombia;
    
    @Value("${colombia.ITAU.pmi-client-account-number}")
    private String pmiClientAccountNumberForITAUColombia;
    
    @Value("${argentina.directPayment.type-of-account}")
    private String argTypeOfAccount;
    
    @Value("${argentina.sender.id}")
    private String argSenderId;
    
    @Value("${argentina.sender.address}")
    private String argSenderAddress;
    
    @Value("${mexico.stp.operating-institution}")
    private String mexStpOperatingInstitution;
    
    @Value("${mexico.stp.business}")
    private String mexStpBusiness;
    
    @Value("${mexico.stp.payment-type}")
    private String mexStpPaymentType;
    
    @Value("${mexico.stp.type-of-account}")
    private String mexStpTypeOfAccount;
    
    @Value("${brazil.sender.id}")
    private String brazilSenderId;
    
    @Value("${brazil.sender.address}")
    private String brazilSenderAddress;
    
    @Value("${brazil.sender.name}")
    private String brazilSenderName;
    
    @Value("${brazil.sender.country}")
    private String brazilSenderCountry;
    
    @Value("${brazil.sender.state}")
    private String brazilSenderState;
    
    @Value("${brazil.sender.email}")
    private String brazilSenderEmail;
    
    @Value("${brazil.concept.code}")
    private String brazilConceptCode;
    
    @Value("${environment}")
    private String environment;
    
    @Value("${ripple-url}")
    private String rippleURL;
    
    @Value("${ripple-client-id}")
    private String rippleClientId;
    
    @Value("${ripple-cliente-secret}")
    private String rippleClientSecret;
    
    @Value("#{'${abcParameters}'.split(',')}")
    private List<String> abcParameters;
    
    @Value("${ripple-sender-correo}")
    private String rippleSenderCorreo;
    
    @Value("${ripple-sender-id}")
    private String rippleSenderId;
    
    @Value("${ripple-sender-FirstNm}")
    private String rippleSenderFirstNm;
    
    @Value("${ripple-sender-LastNm}")
    private String rippleSenderLastNm;
    
    @Value("${ripple-sender-AdrLine}")
    private String rippleSenderAdrLine;
    
    @Value("${abc-bank-account-id}")
    private Long abcBankAccountId;
    
    // @Value("${ripple-brazil-bank-account-id}")
    // private int ripple_brazil_bank_account;
    
    // @Value("${ripple-localpayments-comission}")
    // private float lp_comission;
    
    
    public TransactionGroupService(
            TransactionGroupRepository transactionGroupRepository,
            BankAccountService bankAccountService,
            TransactionService transactionService,
            ExtendedUserService extendedUserService,
            DirectPaymentBankService directPaymentBankService,
            TransactionGroupSearchRepository transactionGroupSearchRepository,
            IntegrationStpService integrationStpService,
            TranslateService translateService) {
        
        this.transactionGroupRepository = transactionGroupRepository;
        this.bankAccountService = bankAccountService;
        this.transactionService = transactionService;
        this.extendedUserService = extendedUserService;
        this.directPaymentBankService = directPaymentBankService;
        this.transactionGroupSearchRepository = transactionGroupSearchRepository;
        this.integrationStpService = integrationStpService;
        this.translateService = translateService;
        /*this.thresholdPayeeRepository = thresholdPayeeRepository;
        this.transactionRepository = transactionRepository;
        this.extendedUserRepository = extendedUserRepository;
        this.userRepository = userRepository;*/
        
    }
    
    /**
     * Save a transactionGroup.
     *
     * @param transactionGroup the entity to save.
     * @return the persisted entity.
     */
    public TransactionGroup save(TransactionGroup transactionGroup) {
        log.debug("Request to save TransactionGroup : {}", transactionGroup);
        TransactionGroup result = transactionGroupRepository.save(transactionGroup);
        transactionGroupSearchRepository.save(result);
        return result;
    }
    
    /**
     * Update a transactionGroup.
     *
     * @param tg the entity to update.
     * @return the persisted entity.
     */
    public TransactionGroup update(TransactionGroup tg) {
        log.debug("Request to save TransactionGroup : {}", tg);
        TransactionGroup ctg = findOne(tg.getId()).orElseThrow(() ->
                new NotFoundAlertException(TransactionGroupService.ENTITY_NAME + " id: " + tg.getId(), ENTITY_NAME, TransactionGroupService.NOT_FOUND));
        
        if (!validateStatusChange(tg.getStatus(), ctg.getStatus())) {
            throw new BadRequestAlertException(
                    "Invalid status cycle: from " + ctg.getStatus().toString() + " to " + tg.getStatus().toString()
                    , ENTITY_NAME, "statusChange");
        }
        ctg.setStatus(tg.getStatus());
        return save(ctg);
    }
    
    /**
     * Validate the new status.
     *
     * @param newStatus status to validate.
     * @param currentStatus status of the current entity to validate.
     * @return boolean value according to validation.
     */
    private boolean validateStatusChange(Status newStatus, Status currentStatus) {
        switch (currentStatus) {
            case CREATED:
                return newStatus == currentStatus ||
                        newStatus == Status.IN_PROCESS ||
                        newStatus == Status.CANCELLED;
            case IN_PROCESS:
                return newStatus == currentStatus ||
                        newStatus == Status.ACCEPTED ||
                        newStatus == Status.REJECTED;
            case ACCEPTED:
            case REJECTED:
            case CANCELLED:
            default:
                return
                        newStatus == currentStatus;
        }
    }
    
    /**
     * Get all the transactionGroups.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<TransactionGroup> findAll(Pageable pageable) {
        log.debug("Request to get all TransactionGroups");
        return transactionGroupRepository.findAll(pageable);
    }
    
    /**
     * Get one transactionGroup by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TransactionGroup> findOne(Long id) {
        log.debug("Request to get TransactionGroup : {}", id);
        if (id == null) return Optional.empty();
        return transactionGroupRepository.findById(id);
    }
    
    /**
     * Delete the transactionGroup by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TransactionGroup : {}", id);
        transactionGroupRepository.deleteById(id);
        transactionGroupSearchRepository.deleteById(id);
    }
    
    /**
     * Search for the transactionGroup corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<TransactionGroup> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of TransactionGroups for query {}", query);
        return transactionGroupSearchRepository.search(queryStringQuery(query), pageable);
    }
    
    @Transactional
    public void saveAll(List<TransactionGroup> transactionGroupList) {
        if (!transactionGroupList.isEmpty())
            transactionGroupSearchRepository.saveAll(transactionGroupList);
    }
    
    /**
     * Create a group of transaction.
     *
     * @param transactionGroupDTO the group of entity to create.
     * @return the persisted entity.
     */
    public TransactionListDTO create(TransactionListDTO transactionGroupDTO) {
        
        log.info(">>>>>>>>>>>>>>>> Request to create the list of Transactions : {}", transactionGroupDTO);
        
        BankAccount bankAccount = bankAccountService.findOne(transactionGroupDTO.getBankAccount().getId()).orElseThrow(() ->
                new NotFoundAlertException(BankAccountService.ENTITY_NAME + " id: " + transactionGroupDTO.getBankAccount().getId(), ENTITY_NAME, BankAccountService.NOT_FOUND_CODE));
        
        log.info(">>>>>>>>>>>>>>>> bankAccount : {}", bankAccount);
        
        TransactionGroup tg = new TransactionGroup();
        tg.setStatus(Status.CREATED);
        tg.setDateCreated(ZonedDateTime.now());
        tg.setBankAccount(bankAccount);
        tg.setExchangeRate(transactionGroupDTO.getExchangeRate());
        tg.setAdmin(extendedUserService.getCurrentExtendedUser());
        
        
        if (transactionGroupDTO.getDirectPayment() != null) {
            tg.setDirectPayment(transactionGroupDTO.getDirectPayment());
        } else {
            tg.setDirectPayment(false);
        }
        save(tg);
        
        Set<Transaction> transactions = new HashSet<>();
        Set<Transaction> transactionsWithErrors = new HashSet<>();
        
        transactionGroupDTO.getTransactions().forEach(t -> {
            
            Transaction tTemp = transactionService.findOne(t.getId()).orElse(null);
            
            String reason = nullIfItIsPossibleToProcess(tTemp, tg);
            
            log.info(">>>>>>>>>>>>>>>> reason if is not possible to process: {}", reason);
            
            if (reason == null) {
                
                log.info(">>>>>>>>>>>>>>>> set transaction");
                
                tTemp.setTransactionGroup(tg);
                tTemp.setAdmin(tg.getAdmin());
                tTemp.exchangeRate(tg.getExchangeRate());
                tTemp.bankAccount(tg.getBankAccount());
                tTemp.setStatus(Status.IN_PROCESS);
                tTemp.setStatusReason(null);
                transactions.add(transactionService.update(tTemp));
                
            } else {
                
                log.info(">>>>>>>>>>>>>>>> NO PROCESSED transaction");
                
                if (tTemp == null) {
                    tTemp = new Transaction();
                    tTemp.setId(t.getId());
                }
                tTemp.setStatus(Status.FAILED);
                tTemp.setStatusReason(reason);
                transactionsWithErrors.add(transactionService.update(tTemp));
            }
            
        });
        
        
        transactionGroupDTO.setTransactionsWithErrors(transactionsWithErrors);
        transactionGroupDTO.setTransactions(transactions);
        transactionGroupDTO.setId(tg.getId());
        
        log.info(">>>>>>>>>>>>>>>> after set transactionGroupDTO");
        
        if (transactions.isEmpty()) delete(tg.getId());
        else {
            tg.setTransactions(transactions);
            save(tg);
            
            log.info(">>>>>>>>>>>>>>>> sfter save ESS");
        }
        return transactionGroupDTO;
    }
    
    
    /**
     * process transaction for ripple and update status
     *
     * @param rippleTransactionGroupDTO the group of entity to create.
     * @return the persisted entity.
     */
    public RippleResultGroupDTO processAndUpdateRippleTrasaction(RippleTransacctionDTO rippleTransactionGroupDTO) {
        
        // BankAccount bankAccount = bankAccountService.findOne(transactionGroupDTO.getBankAccount().getId()).orElseThrow(() ->
        //     new NotFoundAlertException(BankAccountService.ENTITY_NAME + " id: " + transactionGroupDTO.getBankAccount().getId(), ENTITY_NAME, BankAccountService.NOT_FOUND_CODE));
        // log.info(">>>>>>>>>>>>>>>> bankAccount : {}", bankAccount);
        
        // *************** update process
        RippleResultGroupDTO rippleResultGroup = new RippleResultGroupDTO();
        rippleResultGroup.setId(rippleTransactionGroupDTO.getId());
        rippleTransactionGroupDTO.setRemitee(bankAccountService.findOne(rippleTransactionGroupDTO.getBankAccount().getId()).get().getIsRemitee());
        
        log.debug("Request to update the list of Transactions : {}", rippleTransactionGroupDTO);
        log.debug("is Remitee : {}", rippleTransactionGroupDTO.isRemitee());
        Set<RippleResultDTO> result = new HashSet<>();
        Set<RippleResultDTO> resultWithErrors = new HashSet<>();
        
        rippleTransactionGroupDTO.getTransactions().forEach(transaction -> {
            
            try {
                
                log.info(">>>>>>>>>>>>>>>> transaction : {}", transaction);
                Optional<Transaction> tx = transactionService.findOne(transaction.getId());
                
                if(tx != null && tx.isPresent()){
                    
                    log.info(">>>>>>>>>>>>>>>> transaction isPresent");
                    
                    Optional<ExtendedUser> eu_payee = extendedUserService.findOne(transaction.getPayee().getId());
                    
                    log.info(">>>>>>>>>>>>>>>> eu_payee: {}", eu_payee);
                    
                    if(eu_payee != null  && eu_payee.isPresent()){
                        
                        log.info(">>>>>>>>>>>>>>>> payee user isPresent");
                        log.info(">>>>>>>>>>>>>>>> eu_payee: {}", eu_payee.get());
                        log.info(">>>>>>>>>>>>>>>> eu_payee banckaccount number: {}", eu_payee.get().getBankAccountNumber());
                        log.info(">>>>>>>>>>>>>>>> mercahntId: {}", transaction.getMerchant().getId());
                        log.info(">>>>>>>>>>>>>>>> payeeId: {}", transaction.getPayee().getId());
                        
                        // ***************************** Construccion de peticion
                        String rippleStringRequest = createRippleRequest(transaction, eu_payee.get().getBankAccountNumber(),rippleTransactionGroupDTO.isRemitee() );
                        
                        // ***************************** Envio de la petición a servicio Ripple
                        RippleResultDTO rippleResult = rippleService(rippleStringRequest);
                        
                        log.info(">>>>>>>>>>>>>>>> rippleResult : {}", rippleResult);
                        log.info(">>>>>>>>>>>>>>>> rippleResult.getStatus() : {}", rippleResult.getStatus());
                        
                        if(rippleResult.getStatus() == "ACCEPTED" || rippleResult.getMessage() == "ACCEPTED" || rippleResult.getTransaction_id() != null){
                            
                            // transaction.setTransactionGroup(rippleTransactionGroupDTO);
                            // transaction.setAdmin(transaction.getAdmin());
                            // transaction.exchangeRate(transaction.getExchangeRate());
                            // transaction.bankAccount(transaction.getBankAccount());
                            transaction.setStatus(Status.IN_PROCESS);
                            transaction.setStatusReason(null);
                            
                            result.add(rippleResult);
                            
                        }
                        else{
                            
                            transaction.setStatus(Status.FAILED);
                            transaction.setStatusReason(rippleResult.getMessage());
                            
                            resultWithErrors.add(rippleResult);
                        }
                        
                    }
                    else{
                        
                        log.info(">>>>>>>>>>>>>>>> no hay payee asignado");
                        
                    }
                    
                }
                else{
                    log.info(">>>>>>>>>>>>>>>> no hay transacciones");
                }
                
                // TODO: update en BD y ESS
                // Transaction result = transactionService.update(transaction);
                // transactionService.refetch(result.getId());
                
                
            } catch (BadRequestAlertException brae) {
                transaction.status(Status.CANCELLED);
                transaction.statusReason(brae.getMessage());
                // resultWithErrors.add(transaction);
            }
            
        });
        
        rippleResultGroup.setRippleResults(result);
        rippleResultGroup.setRippleResultsWithErrors(resultWithErrors);
        
        return rippleResultGroup;
    }
    
    private String createRippleRequest(Transaction transaction, String bankAccountNumber, boolean isRemitee){
        
        log.info(">>>>>>>>>>>>>>>> createRippleRequest");
        
        String rippleStringRequest = "";
        
        JsonObject jsonRipple = new JsonObject();
        JsonObject jsonUserInfo = new JsonObject();
        
        JsonObject jsonCdtr = new JsonObject();
        JsonObject jsonDbtr = new JsonObject();
        JsonObject jsonCdtrAcct = new JsonObject();
        JsonObject jsonPurp = new JsonObject();
        JsonObject jsonCdtrAgt = new JsonObject();
        
        JsonObject jsonCdtrAcctId = new JsonObject();
        JsonObject jsonCdtrAcctIdOthr = new JsonObject();
        JsonObject jsonCdtrAcctTp = new JsonObject();
        
        JsonObject jsonPayee_BtrStrdNm = new JsonObject();
        JsonObject jsonPayee_BtrId = new JsonObject();
        JsonObject jsonPayee_BtrPstlAdr = new JsonObject();
        JsonObject jsonPayee_BtrCtctDtls = new JsonObject();
        
        JsonObject jsonPayee_BtrOrgId = new JsonObject();
        JsonObject jsonPayee_BtrOrgIdOthr = new JsonObject();
        JsonObject jsonPayee_BtrOrgIdOthrSchmeNm = new JsonObject();
        
        JsonObject jsonPayee_BrnchId = new JsonObject();
        JsonObject jsonPayee_FinInstnId = new JsonObject();
        JsonObject jsonPayee_ClrSysMmbId = new JsonObject();
        
        JsonObject jsonMerchant_BtrStrdNm = new JsonObject();
        JsonObject jsonMerchant_BtrId = new JsonObject();
        JsonObject jsonMerchant_BtrPstlAdr = new JsonObject();
        JsonObject jsonMerchant_BtrCtctDtls = new JsonObject();
        
        JsonObject jsonMerchant_BtrOrgId = new JsonObject();
        JsonObject jsonMerchant_BtrOrgIdOthr = new JsonObject();
        JsonObject jsonMerchant_BtrOrgIdOthrSchmeNm = new JsonObject();
        
        
        Map<String, String> countryCodesMap = new HashMap<String, String>();
        Map<String, String> currencyCodesMap = new HashMap<String, String>();
        
        countryCodesMap.put("ARG", "AR");
        countryCodesMap.put("COL", "CO");
        countryCodesMap.put("MEX", "MX");
        countryCodesMap.put("BRA", "BR");
        countryCodesMap.put("CHL", "CL");
        countryCodesMap.put("PER", "PE");
        countryCodesMap.put("VEN", "VE");
        
        currencyCodesMap.put("CLP", "CLP");
        currencyCodesMap.put("BRL", "BRL");
        currencyCodesMap.put("ARS", "ARS");
        currencyCodesMap.put("PER", "PER");
        currencyCodesMap.put("VEN", "VEN");
        
        String payeepmi_countrycode = transaction.getPayee().getResidenceCountry().getCode() != null ? transaction.getPayee().getResidenceCountry().getCode() : transaction.getPayee().getPostalCity().getCountry().getCode();
        if (payeepmi_countrycode == null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.country.incorrect.id"), ENTITY_NAME, "MissingCountryCode");
        }
        log.info(">>>>>>>>>>>>>>>> Despues de validacion datos payee");
        
        String merchantpmi_countrycode = transaction.getMerchant().getResidenceCountry().getCode() != null ? transaction.getMerchant().getResidenceCountry().getCode() : transaction.getPayee().getPostalCity().getCountry().getCode();
        
        if (merchantpmi_countrycode == null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.country.incorrect.id"), ENTITY_NAME, "MissingCountryCode");
        }
        log.info(">>>>>>>>>>>>>>>> Despues de validacion datos merchant");
        
        String countryCodePayee = countryCodesMap.get(payeepmi_countrycode);
        String countryCodeMerchant = countryCodesMap.get(merchantpmi_countrycode);
        String currencyCode = currencyCodesMap.get(transaction.getCurrency().getCode());
        /*String phoneCode = "";
        switch(countryCodePayee){
        case "AR" : phoneCode ="+54";
        break;
        case "BR" : phoneCode = "+55";
        break;
        case "CL" : phoneCode = "+56";
        break;
        case "PE" : phoneCode = "+51";
        break;
        case "VE" : phoneCode = "+58";
        break;
        default : phoneCode = "";
        break;
        
        }*/
        log.info(">>>>>>>>>>>>>>>> countryCodePayee: {}", countryCodePayee);
        log.info(">>>>>>>>>>>>>>>> countryCodeMerchant: {}", countryCodeMerchant);
        log.info(">>>>>>>>>>>>>>>> currencyCode: {}", currencyCode);
        String phoneNumber = transaction.getPayee().getPhoneNumber();
        String phoneCode = transaction.getPayee().getPostalCountry().getPhoneCode();
        if(phoneNumber!=null&&!"".equals(phoneNumber.trim())&&phoneCode!=null&&!"".equals(phoneCode.trim())){
        Pattern regexPhone = Pattern.compile("^\\+"+phoneCode+"+", Pattern.CANON_EQ);  // pattern for "<tag>...</tag>"
                Matcher regexMatcherPhone = regexPhone.matcher(phoneNumber.trim());
                if(!regexMatcherPhone.find()){
                    regexPhone = Pattern.compile("^"+phoneCode.replace("+","")+"+", Pattern.CANON_EQ);
                    regexMatcherPhone = regexPhone.matcher(phoneNumber.trim());
                    
                    if(!regexMatcherPhone.find()){
                        phoneNumber = phoneCode+phoneNumber;
                    }else{
                        phoneNumber = "+"+phoneNumber;
                    }
                }
        }
                log.info(">>>>>>>>>>>>>>>> phoneNumber: {}", phoneNumber);
        // ****** Contruyendo JSON del CDTR (payee - beneficiary)
        if(transaction.getPayee().getUser().isPayeeAsCompany()){
            
            jsonCdtr.addProperty("Nm",transaction.getPayee().getCompany());
            
            jsonPayee_BtrOrgIdOthrSchmeNm.addProperty("Cd",isRemitee&&"PE".equals(countryCodePayee)?"NIDN":"TXID");
            jsonPayee_BtrOrgIdOthr.add("SchmeNm",jsonPayee_BtrOrgIdOthrSchmeNm);
            jsonPayee_BtrOrgIdOthr.addProperty("Id",transaction.getPayee().getTaxId());
            jsonPayee_BtrOrgId.add("Othr",jsonPayee_BtrOrgIdOthr);
            jsonPayee_BtrId.add("OrgId",jsonPayee_BtrOrgId);
            jsonCdtr.add("Id",jsonPayee_BtrId);
            
            jsonPayee_BtrPstlAdr.addProperty("AdrLine",transaction.getPayee().getResidenceAddress());
            jsonPayee_BtrPstlAdr.addProperty("PstCd","");
            jsonPayee_BtrPstlAdr.addProperty("TwnNm",isRemitee?transaction.getPayee().getPostalCity().getName().replaceAll("\\[", "").replaceAll("]", ""):"");
            jsonPayee_BtrPstlAdr.addProperty("Ctry",countryCodePayee);
            jsonPayee_BtrPstlAdr.addProperty("CtrySubDvsn",isRemitee?transaction.getPayee().getPostalCity().getName().replaceAll("\\[", "").replaceAll("]", ""):"");
            jsonCdtr.add("PstlAdr",jsonPayee_BtrPstlAdr);
            
            jsonPayee_BtrCtctDtls.addProperty("EmailAdr",transaction.getPayee().getEmail());
            jsonPayee_BtrCtctDtls.addProperty("PhneNb",phoneNumber);
            
            
            jsonCdtr.add("CtctDtls",jsonPayee_BtrCtctDtls);
            
            log.info(">>>>>>>>>>>>>>>> after config payee company");
            
        }
        else{
            if(isRemitee&&"VE".equals(countryCodePayee)){
                Pattern regex = Pattern.compile("^[V|E][0-9]{7,8}$", Pattern.CANON_EQ);  // pattern for "<tag>...</tag>"
                Matcher regexMatcher = regex.matcher(transaction.getPayee().getIdNumber().trim());
                if (!regexMatcher.find()) {
                    log.info(">>>>>>>>>>>>>>>> Error Document ID Venezuela: {}",transaction.getPayee().getIdNumber());
                    throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.payee.missing.id"), ENTITY_NAME, "MissingOrWrongId");
                    
                }
            }
            jsonPayee_BtrStrdNm.addProperty("FirstNm",transaction.getPayee().getUser().getFirstName());
            jsonPayee_BtrStrdNm.addProperty("LastNm",transaction.getPayee().getUser().getLastName());
            
            jsonCdtr.add("StrdNm",jsonPayee_BtrStrdNm);
            
            jsonPayee_BtrOrgIdOthrSchmeNm.addProperty("Cd",isRemitee&&"PE".equals(countryCodePayee)?"NIDN":"TXID");
            jsonPayee_BtrOrgIdOthr.add("SchmeNm",jsonPayee_BtrOrgIdOthrSchmeNm);

            jsonPayee_BtrOrgIdOthr.addProperty("Id",transaction.getPayee().getIdNumber().trim());
            jsonPayee_BtrOrgId.add("Othr",jsonPayee_BtrOrgIdOthr);
            jsonPayee_BtrId.add("PrvtId",jsonPayee_BtrOrgId);
            jsonCdtr.add("Id",jsonPayee_BtrId);
            
            jsonPayee_BtrPstlAdr.addProperty("AdrLine",transaction.getPayee().getResidenceAddress());
            jsonPayee_BtrPstlAdr.addProperty("PstCd","");
            jsonPayee_BtrPstlAdr.addProperty("TwnNm",isRemitee?transaction.getPayee().getPostalCity().getName().replaceAll("\\[", "").replaceAll("]", ""):"");
            jsonPayee_BtrPstlAdr.addProperty("Ctry",countryCodePayee);
            jsonPayee_BtrPstlAdr.addProperty("CtrySubDvsn",isRemitee?transaction.getPayee().getPostalCity().getName().replaceAll("\\[", "").replaceAll("]", ""):"");
            jsonCdtr.add("PstlAdr",jsonPayee_BtrPstlAdr);
            
            jsonPayee_BtrCtctDtls.addProperty("EmailAdr",transaction.getPayee().getEmail());
            jsonPayee_BtrCtctDtls.addProperty("PhneNb",phoneNumber);
            
            jsonCdtr.add("CtctDtls",jsonPayee_BtrCtctDtls);
            
            log.info(">>>>>>>>>>>>>>>> after config payee person");
            
        }
        
        jsonUserInfo.add("Cdtr",jsonCdtr);
        
        // ***************************** Si aplica, agreganado CdtrAgt para Payee Brazil y Chile
        
        if(countryCodePayee == "BR" || countryCodePayee == "CL" || countryCodePayee == "PE"){
            
            if(countryCodePayee == "BR" ){
                // Obteniendo bank branch usuarios brazil
                log.info(">>>>>>>>>>>>>>>> payee BR, adding bank branch: {}", transaction.getPayee().getBankBranch());
                jsonPayee_BrnchId.addProperty("Id", transaction.getPayee().getBankBranch());
                jsonCdtrAgt.add("BrnchId",jsonPayee_BrnchId);
            }
            
            // Obteniendo bank code usuarios brazil y chile
            ExtendedUser eu_payee = extendedUserService.findOne(transaction.getPayee().getId()).orElseThrow(() ->
                    new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.payee") + " " + ExtendedUserService.ENTITY_NAME + " id: " + transaction.getPayee().getId(), ENTITY_NAME, ExtendedUserService.NOT_FOUND));
            
            DirectPaymentBank directPaymentBankOptional = null;
            if(eu_payee.getDirectPaymentBank() != null){
                
                log.info(">>>>>>>>>>>>>>>> payee.getDirectPaymentBank().getId(): {}", eu_payee.getDirectPaymentBank().getId());
                directPaymentBankOptional = directPaymentBankService.findOneByBank(eu_payee.getDirectPaymentBank().getId());
                
            }
            
            if(directPaymentBankOptional != null){
                
                log.info(">>>>>>>>>>>>>>>> payee.getDestinyBankCode: {}", directPaymentBankOptional.getDestinyBankCode());
                if(isRemitee){
                    if(directPaymentBankOptional.getDestinyBankBicfi()==null||"".equals(directPaymentBankOptional.getDestinyBankBicfi().trim())){
                        log.info(">>>>>>>>>>>>>>>> Error BICFI not found For Bank: {}", eu_payee.getDirectPaymentBank().getId());
                        throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.missing.bicfi"), ENTITY_NAME, "MissingDestinyBankBicfi");
                    }
                    jsonPayee_FinInstnId.addProperty("BICFI", directPaymentBankOptional.getDestinyBankBicfi());
                }else{
                    jsonPayee_ClrSysMmbId.addProperty("Mmbid", directPaymentBankOptional.getDestinyBankCode());
                    jsonPayee_FinInstnId.add("ClrSysMmbId", jsonPayee_ClrSysMmbId);
                }
                jsonCdtrAgt.add("FinInstnId", jsonPayee_FinInstnId);
                
            }
            
            jsonUserInfo.add("CdtrAgt",jsonCdtrAgt);
            
        }
        
        // ***************************** Contruyendo JSON del DBTR (merchant - sender)
        
        log.info(">>>>>>>>>>>>>>>> isMerchantAsCompany: {}", transaction.getMerchant().getUser().isPayeeAsCompany());
        
        
        if(transaction.getMerchant().getUser().isPayeeAsCompany()||isRemitee){
            
            if(isRemitee)jsonDbtr.addProperty("Nm", rippleSenderFirstNm + " " + rippleSenderLastNm);
            else jsonDbtr.addProperty("Nm", rippleSenderFirstNm + " " + rippleSenderAdrLine);
            
            jsonMerchant_BtrOrgIdOthrSchmeNm.addProperty("Cd","TXID");
            if(isRemitee)jsonMerchant_BtrOrgIdOthrSchmeNm.addProperty("Prtry","SA");
            jsonMerchant_BtrOrgIdOthr.add("SchmeNm",jsonMerchant_BtrOrgIdOthrSchmeNm);
            jsonMerchant_BtrOrgIdOthr.addProperty("Id", rippleSenderId);
            
            jsonMerchant_BtrOrgId.add("Othr", jsonMerchant_BtrOrgIdOthr);
            jsonMerchant_BtrId.add("OrgId", jsonMerchant_BtrOrgId);
            jsonDbtr.add("Id", jsonMerchant_BtrId);
            
            jsonMerchant_BtrPstlAdr.addProperty("AdrLine", rippleSenderAdrLine);
            jsonMerchant_BtrPstlAdr.addProperty("PstCd","");
            jsonMerchant_BtrPstlAdr.addProperty("TwnNm","CDMX");
            jsonMerchant_BtrPstlAdr.addProperty("Ctry", "MX");
            jsonMerchant_BtrPstlAdr.addProperty("CtrySubDvsn","CDMX");
            jsonDbtr.add("PstlAdr",jsonMerchant_BtrPstlAdr);
            
            jsonMerchant_BtrCtctDtls.addProperty("EmailAdr", rippleSenderCorreo);
            jsonDbtr.add("CtctDtls", jsonMerchant_BtrCtctDtls);
            
            log.info(">>>>>>>>>>>>>>>> after merchant company");
        } else {
            
            jsonMerchant_BtrStrdNm.addProperty("FirstNm", rippleSenderFirstNm);
            jsonMerchant_BtrStrdNm.addProperty("LastNm", rippleSenderLastNm);
            jsonDbtr.add("StrdNm", jsonMerchant_BtrStrdNm);
            
            jsonMerchant_BtrOrgIdOthrSchmeNm.addProperty("Cd","TXID");
            jsonMerchant_BtrOrgIdOthr.add("SchmeNm",jsonMerchant_BtrOrgIdOthrSchmeNm);
            jsonMerchant_BtrOrgIdOthr.addProperty("Id", rippleSenderId);
            
            jsonMerchant_BtrOrgId.add("Othr",jsonMerchant_BtrOrgIdOthr);
            jsonMerchant_BtrId.add("PrvtId",jsonMerchant_BtrOrgId);
            jsonDbtr.add("Id",jsonMerchant_BtrId);
            
            jsonMerchant_BtrPstlAdr.addProperty("AdrLine", rippleSenderAdrLine);
            jsonMerchant_BtrPstlAdr.addProperty("PstCd","");
            jsonMerchant_BtrPstlAdr.addProperty("TwnNm","CDMX");
            jsonMerchant_BtrPstlAdr.addProperty("Ctry","MX");
            jsonMerchant_BtrPstlAdr.addProperty("CtrySubDvsn","CDMX");
            jsonDbtr.add("PstlAdr",jsonMerchant_BtrPstlAdr);
            
            jsonMerchant_BtrCtctDtls.addProperty("EmailAdr", rippleSenderCorreo);
            jsonDbtr.add("CtctDtls",jsonMerchant_BtrCtctDtls);
            
            log.info(">>>>>>>>>>>>>>>> after merchant person");
        }
        
        jsonUserInfo.add("Dbtr",jsonDbtr);
        
        // ***************************** Contruyendo JSON Cuenta bancaria
        jsonCdtrAcctIdOthr.addProperty("Id", bankAccountNumber);
        jsonCdtrAcctId.add("Othr",jsonCdtrAcctIdOthr);
        jsonCdtrAcct.add("Id",jsonCdtrAcctId);
        
        if(transaction.getPayee().getBankAccountType() == BankAccountType.AHO){
            jsonCdtrAcctTp.addProperty("Cd","SVGS");
        }
        else{
            jsonCdtrAcctTp.addProperty("Cd",isRemitee?"CACC":"TRAN");
        }
        jsonCdtrAcct.add("Tp",jsonCdtrAcctTp);
        
        jsonUserInfo.add("CdtrAcct",jsonCdtrAcct);
        
        log.info(">>>>>>>>>>>>>>>> after config ripple_account");
        
        // ***************************** Contruyendo JSON Purp
        
        if(!isRemitee)jsonPurp.addProperty("Cd","PAYR");
        else jsonPurp.addProperty("Prtry","OTHER");
        jsonUserInfo.add("Purp",jsonPurp);
        
        // ***************************** Contruyendo JSON global
        
        jsonRipple.add("UserInfo",jsonUserInfo);
        jsonRipple.addProperty("TransactionId",transaction.getId());
        
        // BigDecimal amountWithLPTax =  transaction.getAmountLocalCurrency() lp_comission
        jsonRipple.addProperty("Amount",!isRemitee?transaction.getAmountLocalCurrency():transaction.getAmountAfterCommission());
        jsonRipple.addProperty("Currency",!isRemitee?currencyCode:"USD");
        
        
        log.info(">>>>>>>>>>>>>>>> jsonRipple : {}", jsonRipple.toString());
        
        return jsonRipple.toString();
        
    }
    
    
    private RippleResultDTO rippleService (String rippleRequest){
        
        log.info(">>>>>>>>>>>>>>>> rippleService");
        
        RippleResultDTO rippleResult=new RippleResultDTO();
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
            @Override
            public boolean hasError(HttpStatus statusCode) {
                log.info("statusCode: {} ", statusCode);
                return false;
            }
        });
        
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("client_id", rippleClientId);
        headers.set("client_secret", rippleClientSecret);
        
        String uri = rippleURL + "/v1/payment/initiate";
        
        try {
            
            final URI endpoint = new URI(uri);
            final HttpEntity<String> httpEntity = new HttpEntity<String>(rippleRequest, headers);
            
            log.info("REQUEST: " + httpEntity.toString());
            log.info("URL: " + endpoint.toString());
            
            ResponseEntity<RippleResultDTO> serviceResult = restTemplate.postForEntity(endpoint, httpEntity, RippleResultDTO.class);
            
            log.info("serviceResult: {}", serviceResult);
            log.info("Charge HTTP CODE: {}", serviceResult.getStatusCode());
            
            // if(!serviceResult.getStatusCode().equals(HttpStatus.CREATED)) {
            //     throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.exist"), "USER", "id");
            // }
            
            rippleResult = serviceResult.getBody();
            
        }catch(Exception e){
            log.info("ERROR: " + e.toString());
        }
        
        return rippleResult;
        
    }
    
    
    private String nullIfItIsPossibleToProcess(Transaction t, TransactionGroup tg) {
        if (tg == null)
            return translateService.getMessageByCurrentUserParamsNull("transaction.validation.group.is.valid");
        if (t == null)
            return translateService.getMessageByCurrentUserParamsNull("transaction.validation.payment.is.invalid");
        if (!transactionService.validateStatusChange(Status.IN_PROCESS, t.getStatus()))
            return translateService.getMessageByCurrentUserParamsNull("transaction.validation.payment.invalid.status") + " " + Status.getStringFromTransactionStatus(t.getStatus()) + " " + translateService.getMessageByCurrentUserParamsNull("error.message.exception.a") + " " +
                    Status.getStringFromTransactionStatus(Status.IN_PROCESS);
        if (t.getPayee() == null)
            return translateService.getMessageByCurrentUserParamsNull("transaction.validation.payee.payment.invalid");
        
        ExtendedUser payee = extendedUserService.findOne(t.getPayee().getId()).orElse(null);
        if (payee == null || payee.getUser() == null || payee.getUser().isPayeeAsCompany() == null)
            return translateService.getMessageByCurrentUserParamsNull("transaction.validation.payees.payments.invalid") + " ";
        
        switch (getBankIdByCountry(tg.getBankAccount().getBank().getId().intValue())) {
            case 1: // Pichincha Colombia
                return validatePayeeInfoForPichinchaCardPayment(payee);
            case 2: // Itau Colombia
                return tg.isDirectPayment() != null && tg.isDirectPayment()
                        ? validatePayeeInfoForItauDirectPayment(payee)  // DirectPayment
                        : validatePayeeInfoForItauCardPayment(payee); // Card payment
            case 3: // Mexico
                return validatePayeeInfoForMexicoCardPayment(payee);
            case 17:
                return validatePayeeInfoForDaviviendaDirectPayment(payee);
            case 30: // STP Mexico
                return validatePayeeInfoForSTPDirectPayment(payee);
            case 196: // ABC Mexico
                return validatePayeeInfoForSTPDirectPayment(payee);
            case 31: // Argentina bank
                return validatePayeeInfoForArgDirectPayment(payee);
            case 198: // Argentina Ripple
                return validatePayeeInfoForArgDirectPayment(payee);
            case 32: // Bancos de Brasil
                return validatePayeeInfoForBrazilDirectPayment(payee);
            case 199: // Brasil Ripple
                return validatePayeeInfoForBrazilDirectPayment(payee);
            case 186: // Banco Security (Chile)
                return validatePayeeInfoForChileDirectPayment(payee);
            case 197: // Chile Ripple
                return validatePayeeInfoForChileDirectPayment(payee);
            case 204: // Peru Ripple
                return validatePayeeInfoForPeruDirectPayment(payee);
            case 215: // Venezuela Ripple
                return validatePayeeInfoForVenDirectPayment(payee);
            default:
                return translateService.getMessageByCurrentUserParamsNull("transaction.validation.invalid.bank.selected");
        }
    }
    
    private String validatePayeeInfoForDaviviendaDirectPayment(ExtendedUser payee){
        String reason = "";
        if (!payee.isUseDirectPayment())
            reason += translateService.getMessageByCurrentUserParamsNull("transaction.validation.user.not.using.direct.payment") + " ";
        else{
            if (payee.getDirectPaymentBank() == null || payee.getDirectPaymentBank().getId() == null)
                reason += translateService.getMessageByCurrentUserParamsNull("transaction.validation.user.direct.payment.bank.invalid") + " ";
            if (payee.getBankAccountNumber() == null || payee.getBankAccountNumber().equals(""))
                reason += translateService.getMessageByCurrentUserParamsNull("transaction.validation.user.account.number.invalid") + " ";
        }
        
        if (reason.length() == 0) return null;
        
        reason = "Payee: " + reason;
        return reason;
    }
    
    private String validatePayeeInfoForItauDirectPayment(ExtendedUser payee) {
        String reason = "";
        if (!payee.isUseDirectPayment())
            reason += translateService.getMessageByCurrentUserParamsNull("transaction.validation.user.not.using.direct.payment") + " ";
        else {
            if (payee.getDirectPaymentBank() == null || payee.getDirectPaymentBank().getId() == null)
                reason += translateService.getMessageByCurrentUserParamsNull("transaction.validation.user.direct.payment.bank.invalid") + " ";
            if (payee.getDirectPaymentCity() == null || payee.getDirectPaymentCity().getId() == null)
                reason += translateService.getMessageByCurrentUserParamsNull("transaction.validation.user.direct.payment.city.invalid") + " ";
            if (payee.getBankAccountNumber() == null || payee.getBankAccountNumber().equals(""))
                reason += translateService.getMessageByCurrentUserParamsNull("transaction.validation.user.account.number.invalid") + " ";
            if (payee.getUser().isPayeeAsCompany()) {
                if (payee.getTaxId() == null || payee.getIdTypeTaxId() == null || payee.getIdTypeTaxId().getId() == null)
                    reason += translateService.getMessageByCurrentUserParamsNull("transaction.validation.user.taxid.invalid") + " ";
            } else if (payee.getIdNumber() == null || payee.getIdType() == null || payee.getIdType().getId() == null)
                reason += translateService.getMessageByCurrentUserParamsNull("transaction.validation.user.taxid.invalid") + " ";
        }
        
        if (reason.length() == 0) return null;
        
        reason = "Payee: " + reason;
        return reason;
    }
    
    private String validatePayeeInfoForItauCardPayment(ExtendedUser payee) {
        String reason = "";
        if (payee.isUseDirectPayment())
            reason += translateService.getMessageByCurrentUserParamsNull("transaction.validation.user.not.using.card.payment") + " ";
        else if (payee.getUser().isPayeeAsCompany())
            reason += translateService.getMessageByCurrentUserParamsNull("transaction.validation.user.should.not.be.a.company") + " ";
        else if (payee.getIdNumber() == null || payee.getIdType() == null || payee.getIdType().getId() == null)
            reason += translateService.getMessageByCurrentUserParamsNull("transaction.validation.user.invalid.id.number") + " ";
        
        if (reason.length() == 0) return null;
        
        reason = "Payee: " + reason;
        return reason;
    }
    
    private String validatePayeeInfoForPichinchaCardPayment(ExtendedUser payee) {
        return validatePayeeInfoForItauCardPayment(payee);
    }
    
    private String validatePayeeInfoForMexicoCardPayment(ExtendedUser payee) {
        String reason = "";
        if (payee.isUseDirectPayment())
            reason += translateService.getMessageByCurrentUserParamsNull("transaction.validation.user.not.using.card.payment") + " ";
        else if (payee.getUser().isPayeeAsCompany())
            reason += translateService.getMessageByCurrentUserParamsNull("transaction.validation.user.should.not.be.a.company") + " ";
        else if (payee.getCardNumber() == null || payee.getCardNumber().equals(""))
            reason += translateService.getMessageByCurrentUserParamsNull("transaction.validation.user.invalid.card.number") + " ";
        
        if (reason.length() == 0) return null;
        
        reason = "Payee: " + reason;
        return reason;
    }
    
    private String validatePayeeInfoForSTPDirectPayment(ExtendedUser payee) {
        
        String reason = "";
        
        if (!payee.isUseDirectPayment())
            reason += translateService.getMessageByCurrentUserParamsNull("transaction.validation.user.not.using.direct.payment") + " ";
        
        // TODO: Agregar validacion de clabe bancaria
        String bankAccountNumber = payee.getBankAccountNumber(); // environment.equalsIgnoreCase("prod") ? payee.getBankAccountNumber() : StpAccountType.ACCOUNT_NUMBER_FOR_DEMO.getValue();
        String upTo3Characters = StringUtils.left(bankAccountNumber, 3);
        StpBankId bankIdDestiny = StpBankId.findByDescription(upTo3Characters);
        
        log.info(">>>>>>>>>>>>>>>> validatePayeeInfoForSTPDirectPayment upTo3Characters: {}", upTo3Characters);
        log.info(">>>>>>>>>>>>>>>> validatePayeeInfoForSTPDirectPayment bankIdDestiny: {}", bankIdDestiny);
        
        if(bankIdDestiny == null || bankIdDestiny.getValue().equals("")){
            reason += translateService.getMessageByCurrentUserParamsNull("transaction.validation.user.invalid.account.number") + " ";
            log.info(">>>>>>>>>>>>>>>> Clabe incorrecta payee: {}", payee);
        }
        
        if (reason.length() == 0) return null;
        
        reason = "Payee: " + reason;
        return reason;
    }
    
    /**
     * Validates payee for transaction
     * @param payee ExtendedUser instance
     * @return string with the reason when payee is not valid, no reason detailed if it is valid
     */
    private String validatePayeeInfoForChileDirectPayment(ExtendedUser payee) {
        String reason = "";
        if (!payee.isUseDirectPayment())
            reason += translateService.getMessageByCurrentUserParamsNull("transaction.validation.user.not.using.direct.payment") + " ";
        else {
            if (payee.getDirectPaymentBank() == null || payee.getDirectPaymentBank().getId() == null)
                reason += translateService.getMessageByCurrentUserParamsNull("transaction.validation.user.direct.payment.bank.invalid") + " ";
            if (payee.getBankAccountNumber() == null || payee.getBankAccountNumber().equals(""))
                reason += translateService.getMessageByCurrentUserParamsNull("transaction.validation.user.account.number.invalid") + " ";
        }
        
        if (reason.length() == 0) return null;
        
        reason = "Payee: " + reason;
        return reason;
    }
    
    //@Async
    public void exportFile(TransactionGroup tg) throws IOException {
        
        log.info("exportFile ========>: Exporting file: {} ...", tg);
        
        tg.setStatus(Status.IN_PROCESS);
        log.info("exportFile ========>: Status 'IN PROGRESS' set");
        tg = save(tg);
        //tg.setFileName(generateFile(tg));
        log.info("BankAccount:"+tg.getBankAccount().getId());
        if(tg.getBankAccount().getId().equals(10L)){
            tg.setFileNameBex(generateFile(tg));
            tg.setFileBankAccount(generateAccountsFile(tg));
        } else{
            tg.setFileName(generateFile(tg));
        }
        
        /*if(tg.getBankAccount().getId() == 7 && tg.getFileName() != null){
        String[] aux = tg.getFileName().split(".");
        String fileNameBex = aux[0] + "_BEX." + aux[1];
        tg.setFileNameBex(fileNameBex);
        }*/
        
        
        if(tg.getBankAccount().getId().equals(10L)){
            tg.setStatus(tg.getFileNameBex() != null  || tg.getFileBankAccount() != null ? Status.ACCEPTED : Status.CANCELLED);
        }
        else{
            tg.setStatus(tg.getFileName() != null ? Status.ACCEPTED : Status.CANCELLED);
        }
        save(tg);
        log.debug("exportFile ========>: TransactionGroup saved");
    }
    
    /**
     * File name creation for the transactionGroup.
     *
     * @param tg the transactionGroup to make the fileName.
     * @return the file name of the transactionGroup.
     */
    private String createFileName(TransactionGroup tg, String fileExtension) {
        
        String fileName = "";
        
        if(getBankIdByCountry(tg.getBankAccount().getBank().getId().intValue())==196){
            
            String bankAccountPMI = "00000527090";
            
            String pattern = "yyyyMMdd";
            LocalDateTime now = LocalDateTime.now();
            String datetimeFn = now.format(DateTimeFormatter.ofPattern(pattern));
            
            String fileNameFn = "11";
            int consecutive = 1;
            Optional<List<TransactionGroup>> tgsOptional = transactionGroupRepository.findAllByBankAccountId(abcBankAccountId);
            
            Date date = new Date();
            Instant inst = date.toInstant();
            LocalDate localDate = inst.atZone(ZoneId.systemDefault()).toLocalDate();
            Instant dayInst = localDate.atStartOfDay(ZoneId.systemDefault()).toInstant();
            //Date today = Date.from(dayInst);
            
            if(tgsOptional.isPresent()){
                List<TransactionGroup> tgs = tgsOptional.get();
                for(int i = 0; i < tgs.size(); i++) {
                    if(tgs.get(i).getDateCreated().toInstant().compareTo(dayInst) < 0){
                        consecutive += 1;
                    }
                }
            }
            
            String consecutiveStr = String.format("%02d" , consecutive);
            
            fileName = fileNameFn + bankAccountPMI + datetimeFn + consecutiveStr + fileExtension;
            log.debug("transactionGroup file name: {}", fileName);
            
        } else {
            String pattern = "yyyy_MM_dd_HH_mm_ss_SS";
            String date = tg.getDateCreated().format(DateTimeFormatter.ofPattern(pattern));
            
            BankAccount bankAccount = bankAccountService.findOne(tg.getBankAccount().getId()).orElseThrow(() ->
                    new NotFoundAlertException(BankAccountService.ENTITY_NAME + " id: " + tg.getBankAccount().getId(), ENTITY_NAME, BankAccountService.NOT_FOUND_CODE));
            
            String bankAccountNumber = bankAccount.getAccountNumber();
            String bank = bankAccount.getBank().getName();
            String country = bankAccount.getBank().getCountry().getName();
            String directPayment = (tg.isDirectPayment() != null && tg.isDirectPayment() ? "-DirectPayment" : "");
            
            fileName = date + "-" + bankAccountNumber + "-" + bank + "-" + country + directPayment + fileExtension;
            log.debug("transactionGroup file name: {}", fileName);
        }
        
        return fileName;
    }
    
    private String createFileNameBankAccount(TransactionGroup tg, String fileExtension){
        
        String fileName = "";
        String clientNumber = "1000100232";
        
        String pattern = "yyyyMMdd";
        LocalDateTime now = LocalDateTime.now();
        String datetimeFn = now.format(DateTimeFormatter.ofPattern(pattern));
        
        String fileNameFn = "11";
        int consecutive = 1;
        Optional<List<TransactionGroup>> tgsOptional = transactionGroupRepository.findAllByBankAccountId(abcBankAccountId);
        
        Date date = new Date();
        Instant inst = date.toInstant();
        LocalDate localDate = inst.atZone(ZoneId.systemDefault()).toLocalDate();
        Instant dayInst = localDate.atStartOfDay(ZoneId.systemDefault()).toInstant();
        //Date today = Date.from(dayInst);
        
        if(tgsOptional.isPresent()){
            List<TransactionGroup> tgs = tgsOptional.get();
            for(int i = 0; i < tgs.size(); i++) {
                if(tgs.get(i).getDateCreated().toInstant().compareTo(dayInst) < 0){
                    consecutive += 1;
                }
            }
        }
        
        String consecutiveStr = String.format("%02d" , consecutive);
        
        fileName = fileNameFn + clientNumber + datetimeFn + consecutiveStr + fileExtension;
        log.debug("transactionGroup file name: {}", fileName);
        
        return fileName;
        
    }
    
    private String generateAccountsFile(TransactionGroup tg) throws IOException{
        log.info("Generacion de archivo de Cuentas ABC");
        String fileExtension = ".txt";
        char fieldSeparator = '|';
        Map<String, Object[]> data= new TreeMap<>();
        
        AtomicInteger index = new AtomicInteger(1);
        
        tg.getTransactions().forEach(t -> {
            ExtendedUser payee = extendedUserService.findOne(t.getPayee().getId()).orElseThrow(() ->
                    new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.payee") + " " + ExtendedUserService.ENTITY_NAME + " id: " + t.getPayee().getId(), ENTITY_NAME, ExtendedUserService.NOT_FOUND));
            
            String bankAccountNumberDestiny = payee.getBankAccountNumber();
            String destinyName = Boolean.TRUE.equals(payee.getUser().isPayeeAsCompany()) && payee.getCompany() != null && !"".equalsIgnoreCase(payee.getCompany()) ? payee.getCompany().replaceAll("[-+^]*", " ") : payee.getFullName().replaceAll("[-+^]*", " ");
            String accountType="40";
            String RFC="ND";
            
            if(!payee.getSentAbc()){
                data.put(String.valueOf(index.incrementAndGet()),
                        new Object[]{
                            accountType,
                            bankAccountNumberDestiny,
                            destinyName,
                            RFC
                        }
                );
                
            }
            
        });
        
        String fileName = fileBucketPath + "transaction/exportedFiles/" + "" + createFileNameBankAccount(tg, fileExtension);
        FileUtils.writeDownTXTFile(fileName, data, fieldSeparator);
        return fileName;
    }
    
    /**
     * File creation for the transactionGroup.
     *
     * @param tg the transactionGroup to make the XLS file.
     * @return the file name of the transactionGroup XLS file.
     */
    private String generateFile(TransactionGroup tg) throws IOException {
        
        log.info(">>>>>>>>>>>>>>>> in generateFile");
        
        log.info(">>>>>>>>>>>>>>>> tg: {}", tg);
        
        String fileExtension = ".txt";
        char fieldSeparator = '\0';
        String sheetName = null;
        Map<String, Object[]> data;
        
        switch (getBankIdByCountry(tg.getBankAccount().getBank().getId().intValue())) {
            case 1: // Pichincha Colombia
                log.info(">>>>>>>>>>>>>>>> exportar Colombia Pichincha");
                data = getDataPichincha(tg);
                sheetName = "Recharge";
                fileExtension = ".xlsx";
                break;
            case 2: // Itau Colombia
                log.info(">>>>>>>>>>>>>>>> exportar Colombia Itau");
                fileExtension = ".csv";
                data = tg.isDirectPayment() != null && tg.isDirectPayment()
                        ? getDataItauForDirectPayment(tg)
                        : getDataItau(tg);
                break;
            case 3: // Mexico
                log.info(">>>>>>>>>>>>>>>> exportar Mexico");
                data = getDataMexico(tg);
                break;
            case 17: // Colombia
                log.info(">>>>>>>>>>>>>>>> exportar Colombia Davivienda");
                fileExtension = ".csv";
                data = getDataDavivienda(tg);
                break;
            case 30: // STP Mexico
                log.info(">>>>>>>>>>>>>>>> exportar Mexico STP");
                data = getDataSTP(tg);
                fieldSeparator = '\t';
                break;
            case 196: // ABC Mexico
                log.info(">>>>>>>>>>>>>>>> exportar Mexico ABC");
                data = getDataABC(tg);
                fieldSeparator = '|';
                break;
            case 31: //Argentina
                log.info(">>>>>>>>>>>>>>>> exportar Argentina");
                data = getDataArgentina(tg);
                sheetName = "Direct payments";
                fileExtension = ".xlsx";
                break;
            case 198: //Argentina Ripple
                log.info(">>>>>>>>>>>>>>>> exportar Argentina Ripple");
                data = getDataArgentina(tg);
                sheetName = "Direct payments (Ripple)";
                fileExtension = ".xlsx";
                break;
            case 32:  //Brasil
                log.info(">>>>>>>>>>>>>>>> exportar Brasil");
                data = getDataBrazilDirectPayment(tg);
                sheetName = "Brazil direct payment";
                fileExtension = ".xlsx";
                break;
            case 199:  //Brasil Ripple
                log.info(">>>>>>>>>>>>>>>> exportar Brasil Ripple");
                data = getDataBrazilDirectPayment(tg);
                sheetName = "Brazil direct payment Ripple";
                fileExtension = ".xlsx";
                break;
            case 186: //Chile
                log.info(">>>>>>>>>>>>>>>> exportar chile");
                data = getDataBankSecurity(tg);
                sheetName = "Direct payments";
                fileExtension = ".xlsx";
                break;
            case 197: //Chile Ripple
                log.info(">>>>>>>>>>>>>>>> exportar chile Ripple");
                data = getDataBankSecurity(tg);
                sheetName = "Direct payments Ripple";
                fileExtension = ".xlsx";
                break;
            case 204: //Peru Ripple
                log.info(">>>>>>>>>>>>>>>> exportar peru Ripple");
                data = getDataBankSecurity(tg);
                sheetName = "Direct payments Ripple";
                fileExtension = ".xlsx";
                break;
            case 215: //Venezuela Ripple
                log.info(">>>>>>>>>>>>>>>> exportar venezuela Ripple");
                data = getDataBankSecurity(tg);
                sheetName = "Direct payments Ripple";
                fileExtension = ".xlsx";
                break;
            default:
                data = null;
                break;
        }
        
        if (data == null) return null;
        
        
        String fileName = fileBucketPath + "transaction/exportedFiles/" + createFileName(tg, fileExtension);
        /**fileName=StringUtils.deleteWhitespace(fileName);*/
        log.info("fileName: {}",fileName);
        if (fileExtension.equals(".xlsx")) {
            try {
                FileUtils.writeDownWorkbook(fileName, sheetName, data);}
            catch(Exception e) {
                log.info("error en escribir archivo: {}",e.getMessage());
                throw e;
            }
            
            if(getBankIdByCountry(tg.getBankAccount().getBank().getId().intValue()) == 32){
                
                String fileExtensionBex = "_BEX.csv";
                String fileNameBex = fileBucketPath + "transaction/exportedFiles/" + createFileName(tg, fileExtensionBex);
                
                AtomicInteger index = new AtomicInteger(1);
                Map<Integer, String[]> dataBex = new TreeMap<>();
                int N = dataBex.size();
                String[] header = new String[8];
                String[] auxRow = new String[8];
                
                header[0] = "Full name";
                header[1] = "Document (CPF)";
                header[2] = "E-mail";
                header[3] = "Bank code";
                header[4] = "Agency (without digit)";
                header[5] = "Account (with digit)";
                header[6] = "Is the amount in BRL?";
                header[7] = "Amount";
                
                dataBex.put(index.get(), header);
                
                for (Map.Entry<String, Object[]> entry : data.entrySet()){
                    
                    if(!entry.getKey().equals("1")){
                        
                        String cpf = String.valueOf(entry.getValue()[0]);
                        String bankBranch = String.valueOf(entry.getValue()[4]);
                        
                        auxRow[0] = String.valueOf(entry.getValue()[1]);
                        auxRow[1] = ("00000000000" + cpf).substring(cpf.length());
                        auxRow[2] = String.valueOf(entry.getValue()[6]);
                        auxRow[3] = String.valueOf(entry.getValue()[3]);
                        auxRow[4] = ("0000" + bankBranch).substring(bankBranch.length());
                        auxRow[5] = String.valueOf(entry.getValue()[5]).replaceAll("[^0-9]", "");
                        auxRow[6] = "Yes";
                        auxRow[7] = String.format( "%.2f", Float.parseFloat( String.valueOf(entry.getValue()[8]) ) );
                        
                        dataBex.put(index.incrementAndGet(), auxRow);
                        auxRow = new String[8];
                        
                    }
                    
                }
                
                FileUtils.writeDownCSVFileIntKey(fileNameBex, dataBex);
                tg.setFileNameBex(fileNameBex);
            }
            
        } else {
            
            if(getBankIdByCountry(tg.getBankAccount().getBank().getId().intValue()) == 2
                    || getBankIdByCountry(tg.getBankAccount().getBank().getId().intValue()) == 17){
                Map<Integer, String[]> data_1 = new TreeMap<>();
                for (Map.Entry<String, Object[]> entry : data.entrySet()){
                    int N = entry.getValue().length;
                    String[] aux = new String[N];
                    for(int i=0; i<N; i++) aux[i] = String.valueOf(entry.getValue()[i]);
                    data_1.put(Integer.parseInt(entry.getKey()), aux);
                }
                
                log.debug("=================================> Itau <=============================");
                System.out.println(data_1);
                log.debug("=================================> Itau <=============================");
                
                //FileUtils.writeDownTXTFileIntKey(fileName, data_1, fieldSeparator);
                FileUtils.writeDownCSVFileIntKey(fileName, data_1);
            } else {
                FileUtils.writeDownTXTFile(fileName, data, fieldSeparator);
            }
            
        }
        return fileName;
    }
    
    private int getBankIdByCountry(int bankId) {
        int bankIdRepresentation = bankId;
        if (bankId >= 32 && bankId <= 174) { //bankid entre 32 y 174 son de Brasil
            bankIdRepresentation = 32;
        }
        return bankIdRepresentation;
    }
    
    private Map<String, Object[]> getDataPichincha(TransactionGroup tg) {
        Map<String, Object[]> data = new TreeMap<>();
        AtomicInteger index = new AtomicInteger(1);
        data.put(String.valueOf(index.get()),
                new Object[]{
                    "Id. PERSONA",
                    "MONTO",
                    "Id. TRANSACTION"
                });
        tg.getTransactions().forEach(t -> {
            ExtendedUser payee = extendedUserService.findOne(t.getPayee().getId()).orElseThrow(() ->
                    new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.payee") + " " + ExtendedUserService.ENTITY_NAME + " id: " + t.getPayee().getId(), ENTITY_NAME, ExtendedUserService.NOT_FOUND));
            data.put(String.valueOf(index.incrementAndGet()),
                    new Object[]{
                        payee.getIdNumber(),
                        t.getAmountLocalCurrency().doubleValue(),
                        t.getId()
                    }
            );
        });
        return data;
    }
    
    private Map<String, Object[]> getDataBankSecurity(TransactionGroup tg) {
        
        log.info(">>>>>>>>>>>>>>>> exportar getDataBankSecurity");
        
        Map<String, Object[]> data = new TreeMap<>();
        AtomicInteger index = new AtomicInteger(1);
        
        int N = tg.getTransactions().size();
        Object[][] rows = new Object[N][8];
        
        Set<Transaction> transactions = tg.getTransactions();
        
        int indexRow = 0;
        for(Transaction t : transactions){
            
            ExtendedUser payee = extendedUserService.findOne(t.getPayee().getId()).orElseThrow(() ->
                    new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.payee") + " " + ExtendedUserService.ENTITY_NAME + " id: " + t.getPayee().getId(), ENTITY_NAME, ExtendedUserService.NOT_FOUND));
            
            Object[] currentRow = new Object[8];
            
            currentRow[0] = "2";
            
            String idNumberT = payee.getIdNumber();
            idNumberT = idNumberT.replaceAll("[^a-zA-Z0-9]", "");
            currentRow[1] = idNumberT;
            
            currentRow[2] = payee.getFullName();
            
            currentRow[3] = payee.getBankAccountNumber();
            currentRow[4] = t.getAmountLocalCurrency();
            
            log.info(">>>>>>>>>>>>>>>> transaction {}", t);
            log.info(">>>>>>>>>>>>>>>> getAmountLocalCurrency {}", t.getAmountLocalCurrency());
            log.info(">>>>>>>>>>>>>>>> getAmountBeforeCommission {}", t.getAmountBeforeCommission());
            log.info(">>>>>>>>>>>>>>>> getAmountAfterCommission {}", t.getAmountAfterCommission());
            
            log.info(">>>>>>>>>>>>>>>> amount doubleValue: {}", t.getAmountLocalCurrency().doubleValue());
            
            String amount = String.format("%.2f", t.getAmountLocalCurrency().doubleValue());
            /**amount = amount.replace(".", "").replace(",", "");*/
            amount = amount.replace(".", "").replace(",", "");
            
            log.info(">>>>>>>>>>>>>>>> amount: {}", amount);
            
            currentRow[4] = amount;
            
            currentRow[5] = payee.getBankAccountType() == BankAccountType.AHO ? "3": "1";
            
            
            DirectPaymentBank directPaymentBankOptional = null;
            
            log.info(">>>>>>>>>>>>>>>> ****** payee() {}", payee);
            log.info(">>>>>>>>>>>>>>>> ****** payee.getDirectPaymentBank() {}", payee.getDirectPaymentBank());
            
            if(payee.getDirectPaymentBank() != null){
                log.info(">>>>>>>>>>>>>>>> payee.getDirectPaymentBank().getId() {}", payee.getDirectPaymentBank().getId());
                // directPaymentBankOptional = directPaymentBankService.findOne(payee.getDirectPaymentBank().getId());
                directPaymentBankOptional = directPaymentBankService.findOneByBank(payee.getDirectPaymentBank().getId());
            }
            
            if(directPaymentBankOptional != null){
                currentRow[6] =  directPaymentBankOptional.getDestinyBankCode();
            } else {
                currentRow[6] = "";
            }
            
            currentRow[7] = payee.getEmail();
            
            rows[indexRow] = currentRow;
            indexRow ++;
            
        }
        
        for (int i = 0; i < N; i++){
            data.put(String.valueOf(index.incrementAndGet()), rows[i]);
        }
        
        return data;
        
    }
    
    
    private Map<String, Object[]> getDataDavivienda(TransactionGroup tg) {
        Map<String, Object[]> data = new TreeMap<>();
        AtomicInteger index = new AtomicInteger(1);
        
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("ddMMyyyy");
        LocalDateTime now = LocalDateTime.now();
        String today = dtf.format(now);
        
        int N = tg.getTransactions().size();
        
        Object[][] rows = new Object[N][6];
        
        int indexRow = 0;
        Set<Transaction> transactions = tg.getTransactions();
        
        for(Transaction t : transactions){
            
            
            ExtendedUser merchant = extendedUserService.findOne(t.getMerchant().getId()).orElseThrow(() ->
                        new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.merchant") + " " + ExtendedUserService.ENTITY_NAME + " id: " + t.getMerchant().getId(), ENTITY_NAME, ExtendedUserService.NOT_FOUND));
            
            ExtendedUser payee = extendedUserService.findOne(t.getPayee().getId()).orElseThrow(() ->
                    new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.payee") + " " + ExtendedUserService.ENTITY_NAME + " id: " + t.getPayee().getId(), ENTITY_NAME, ExtendedUserService.NOT_FOUND));
            
            String idNumberT = payee.getIdNumber();
            idNumberT = idNumberT.replaceAll("[^a-zA-Z0-9]", "");
            String verifDigitT = "";
            
            if(payee.getIdType() != null && payee.getIdType().getName().equals("NIT")){
                if(idNumberT.length() == 10){
                    verifDigitT = idNumberT.substring(9, 10);
                    idNumberT = idNumberT.substring(0, 9);
                }
            }
            
            Object[] currentRow = new Object[6];
            currentRow[0] = t.getId(); // Transaction ID
            currentRow[1] = merchant.getCompany(); // Merchant Company
            currentRow[2] = payee.getFullName(); // Nombre completo
            currentRow[3] = idNumberT; // Número Identificación Tercero del Cliente
            currentRow[4] = today; // Fecha de pago
            currentRow[5] = t.getAmountLocalCurrency(); // Local Currency (COP)
            
            rows[indexRow] = currentRow;
            indexRow ++;
            
        }
        
        for (int i = 0; i < N; i++){
            data.put(String.valueOf(index.incrementAndGet()), rows[i]);
        }
        
        return data;
        
    }
    
    
    private Map<String, Object[]> getDataABC(TransactionGroup tg) {
        Map<String, Object[]> data = new TreeMap<>();
        AtomicInteger index = new AtomicInteger(1);
        
        tg.getTransactions().forEach(t -> {
            ExtendedUser payee = extendedUserService.findOne(t.getPayee().getId()).orElseThrow(() ->
                    new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.payee") + " " + ExtendedUserService.ENTITY_NAME + " id: " + t.getPayee().getId(), ENTITY_NAME, ExtendedUserService.NOT_FOUND));
            
            String bankAccountNumber = environment.equalsIgnoreCase("prod") ? payee.getBankAccountNumber() : StpAccountType.ACCOUNT_NUMBER_FOR_DEMO.getValue();
            String upTo3Characters = StringUtils.left(bankAccountNumber, 3);
            String bankIdDestiny = StpBankId.findByDescription(upTo3Characters).getValue();
            
            String bankAccountNumberDestiny = payee.getBankAccountNumber();
            String referenceNumber = tg.getDateCreated().format(DateTimeFormatter.ofPattern("dHHmmss")).substring(0, 6);
            
            String destinyName = Boolean.TRUE.equals(payee.getUser().isPayeeAsCompany()) && payee.getCompany() != null && !"".equalsIgnoreCase(payee.getCompany()) ? payee.getCompany().replaceAll("[^a-zA-Z0-9]", " ") : payee.getFullName().replaceAll("[^a-zA-Z0-9]", " ");
            /**String amount = String.format("%.2f", t.getAmountAfterCommission().doubleValue()).replace(',', '.');*/
            String amount = String.format("%.2f", t.getAmountAfterCommission().doubleValue());
            
            String paymentConcept = abcParameters.get(0);
            String abcAccount= abcParameters.get(1) + "0";
            String accountType="40";
            
            log.info("abcAccount: "+abcAccount);
            log.info("payment Concept: "+paymentConcept);
            
            data.put(String.valueOf(index.incrementAndGet()),
                    new Object[]{
                        abcAccount,
                        bankAccountNumberDestiny,
                        amount,
                        paymentConcept,
                        referenceNumber,
                        accountType,
                        bankIdDestiny,
                        destinyName
                    }
            );
            
        });
        return data;
        
    }
    
    
    private Map<String, Object[]> getDataSTP(TransactionGroup tg) {
        
        log.info(">>>>>>>>>>>>>>>> dentro getDataSTP");
        
        Map<String, Object[]> data = new TreeMap<>();
        AtomicInteger index = new AtomicInteger(1);
        
        data.put(String.valueOf(index.get()),
                new Object[]{
                    "INSTITUCION_CONTRAPARTE",
                    "CLAVE_RASTREO",
                    "NOMBRE_BENEFICIARIO",
                    "TIPO_PAGO",
                    "TIPO_CUENTA_BENEFICIARIO",
                    "MONTO",
                    "CUENTA_BENEFICIARIO",
                    "CONCEPTO_PAGO",
                    "REFERENCIA_NUMERICA",
                    "EMAIL_BENEFICIARIO",
                    "INSTITUCION_OPERANTE",
                    "EMPRESA"
                });
        
        tg.getTransactions().forEach(t -> {
            
            log.info(">>>>>>>>>>>>>>>> t: {}", t);
            
            ExtendedUser payee = extendedUserService.findOne(t.getPayee().getId()).orElseThrow(() ->
                    new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.payee") + " " + ExtendedUserService.ENTITY_NAME + " id: " + t.getPayee().getId(), ENTITY_NAME, ExtendedUserService.NOT_FOUND));
            
            String bankAccountNumber = payee.getBankAccountNumber(); // environment.equalsIgnoreCase("prod") ? payee.getBankAccountNumber() : StpAccountType.ACCOUNT_NUMBER_FOR_DEMO.getValue();
            String upTo3Characters = StringUtils.left(bankAccountNumber, 3);
            StpBankId bankIdDestiny = StpBankId.findByDescription(upTo3Characters);
            
            log.info(">>>>>>>>>>>>>>>> upTo3Characters: {}", upTo3Characters);
            log.info(">>>>>>>>>>>>>>>> bankIdDestiny: {}", bankIdDestiny);
            
            if(bankIdDestiny == null || bankIdDestiny.getValue().equals("")){
                log.info(">>>>>>>>>>>>>>>> Clabe incorrecta de tx: {}", t);
                return;
            }
            log.info(">>>>>>>>>>>>>>>> Pasa validacion de Clabe interbancaria tx: {}", t);
            
            
            String referenceNumber = tg.getDateCreated().format(DateTimeFormatter.ofPattern("dHHmmss")).substring(0, 6);
            String trackingId = referenceNumber + t.getId();
            
            String destinyName = Boolean.TRUE.equals(payee.getUser().isPayeeAsCompany()) && payee.getCompany() != null && !"".equalsIgnoreCase(payee.getCompany()) ? payee.getCompany() : payee.getFullName();
            /**String amount = String.format("%.2f", t.getAmountLocalCurrency().doubleValue()).replace(',', '.');*/
            String amount = String.format("%.2f", t.getAmountLocalCurrency().doubleValue());
            
            String paymentConcept = t.getMerchant().getFullName();
            String beneficiaryEmail = payee.getEmail();
            
            data.put(String.valueOf(index.incrementAndGet()),
                    new Object[]{
                        bankIdDestiny.getValue(),
                        trackingId,
                        destinyName,
                        mexStpPaymentType,
                        mexStpTypeOfAccount,
                        amount,
                        bankAccountNumber,
                        paymentConcept,
                        referenceNumber,
                        beneficiaryEmail,
                        mexStpOperatingInstitution,
                        mexStpBusiness
                    }
            );
            
            /*IntegrationStpDTO integrationStpDTO = new IntegrationStpDTO();
            integrationStpDTO.setBankIdDestiny(bankIdDestiny);
            integrationStpDTO.setAccountNumberDestiny(bankAccountNumber);
            integrationStpDTO.setAccountTypeDestiny(StpAccountType.CLABE);
            integrationStpDTO.setBankIdDestiny(bankIdDestiny);
            integrationStpDTO.setAmount(amount);
            integrationStpDTO.setCompany("PMI_AMERICAS");
            integrationStpDTO.setDescription(paymentConcept);
            integrationStpDTO.setDestinyName(destinyName);
            integrationStpDTO.setReferenceNumber(referenceNumber);
            integrationStpDTO.setTrackingId(integrationStpDTO.getReferenceNumber() + t.getId());
            try {
            integrationStpService.create(integrationStpDTO);
            } catch (Exception e) {
            log.error("{}", e.getLocalizedMessage());
            }*/
        });
        return data;
    }
    
    private Map<String, Object[]> getDataItau(TransactionGroup tg) {
        Map<String, Object[]> data = new TreeMap<>();
        AtomicInteger index = new AtomicInteger(0);
        
        tg.getTransactions().forEach(t -> {
            ExtendedUser payee = extendedUserService.findOne(t.getPayee().getId()).orElse(null);
            if (payee == null) return;
            
            String idReg = StringUtils.leftPad("01", 2, "0");
            String idNumber = payee.getUser().isPayeeAsCompany() ? payee.getTaxId() : payee.getIdNumber();
            idNumber = idNumber.replaceAll("[^a-zA-Z0-9]", "");
            idNumber = StringUtils.leftPad(StringUtils.left(idNumber, 15), 15, "0");
            /**String value = StringUtils.leftPad(StringUtils.right(
             * getFormattedValue(t.getAmountLocalCurrency()), 18), 18, "0");*/
            String value = StringUtils.leftPad(StringUtils.right(
                    String.valueOf(t.getAmountLocalCurrency().doubleValue()), 18), 18, "0");
            String dateMovement = (t.getLastUpdated() != null) ?
                    (t.getLastUpdated().format(DateTimeFormatter.ofPattern("yyyyMMdd"))) :
                    (StringUtils.rightPad("", 8, " "));
            data.put(String.valueOf(index.incrementAndGet()), new String[]{idReg, idNumber, value, dateMovement});
        });
        return data;
    }
    
    private boolean isValidTransactionItauForDirectPayment(Transaction t) {
        return t != null && t.getPayee() != null;
    }
    
    private Map<String, Object[]> getDataItauForDirectPayment(TransactionGroup tg) {
        Map<String, Object[]> data = new TreeMap<>();
        AtomicInteger index = new AtomicInteger(0);
        
        tg.getTransactions().forEach(t -> {
            if (!isValidTransactionItauForDirectPayment(t)) return;
            
            ExtendedUser payee = extendedUserService.findOne(t.getPayee().getId()).orElse(null);
            if (payee == null) return;
            
            int seq = index.incrementAndGet();
            DirectPaymentBank dpbByBank = directPaymentBankService.findOneByBank(payee.getDirectPaymentBank().getId());
            DirectPaymentBank dpbByIdType = directPaymentBankService.findOneByIdType(payee.getUser().isPayeeAsCompany() ? payee.getIdTypeTaxId().getId() : payee.getIdType().getId());
            DirectPaymentBank dpbByCity = directPaymentBankService.findOneByCity(payee.getDirectPaymentCity().getId());
            String destinyBankCode = dpbByBank != null && dpbByBank.getDestinyBankCode() != null ? dpbByBank.getDestinyBankCode() : " ";
            String idTypeCode = dpbByIdType != null && dpbByIdType.getIdTypeCode() != null ? dpbByIdType.getIdTypeCode() : " ";
            String departmentCode = dpbByCity != null && dpbByCity.getDepartmentCode() != null ? dpbByCity.getDepartmentCode() : " ";
            String cityCode = dpbByCity != null && dpbByCity.getCityCode() != null ? dpbByCity.getCityCode() : " ";
            
            String idReg = StringUtils.leftPad("1", 1, " ");
            String sequence = StringUtils.leftPad(StringUtils.right(String.valueOf(seq), 5), 5, "0");
            String dateMovement = (t.getLastUpdated() != null) ?
                    (t.getLastUpdated().format(DateTimeFormatter.ofPattern("MMddyy"))) :
                    (StringUtils.rightPad("", 6, " "));
            String idType = StringUtils.leftPad(StringUtils.right(idTypeCode, 2), 2, "0");
            
            String idNumber = payee.getUser().isPayeeAsCompany() ? payee.getTaxId() : payee.getIdNumber();
            idNumber = idNumber.replaceAll("[^a-zA-Z0-9]", "");
            idNumber = StringUtils.rightPad(StringUtils.left(idNumber, 15), 15, " ");
            
            String fullName = payee.getUser().isPayeeAsCompany() ? payee.getCompany() : payee.getFullName();
            if (fullName == null) fullName = payee.getFullName() != null ? payee.getFullName() : " ";
            fullName = fullName.replaceAll("[^a-zA-Z0-9]", " ");
            fullName = StringUtils.rightPad(StringUtils.left(fullName, 22), 22, " ");
            
            String officialSignal = StringUtils.rightPad("N", 1, " ");
            String officeCode = StringUtils.rightPad("", 3, " ");
            String bankCode = StringUtils.leftPad(StringUtils.right(destinyBankCode, 3), 3, " ");
            String bankAccountCode = StringUtils.leftPad(StringUtils.right(String.valueOf(payee.getBankAccountType()), 3), 3, " ");
            
            String bankAccountNumber = payee.getBankAccountNumber() != null ? payee.getBankAccountNumber() : " ";
            bankAccountNumber = bankAccountNumber.replaceAll("[^a-zA-Z0-9]", "");
            bankAccountNumber = StringUtils.rightPad(StringUtils.left(bankAccountNumber, 17), 17, " ");
            
            String transactionType = StringUtils.rightPad("CR", 2, " ");
            /**String value = StringUtils.leftPad(StringUtils.right(getFormattedValue(t.getAmountLocalCurrency()), 14), 14, "0");*/
            String value = StringUtils.leftPad(StringUtils.right(String.valueOf(t.getAmountLocalCurrency().doubleValue()), 14), 14, "0");
            String reference = StringUtils.rightPad("", 20, " ");
            String observation = StringUtils.rightPad(StringUtils.left(t.getDescription(), 80), 80, " ");
            observation = observation.replaceAll("[^a-zA-Z0-9]", " ");
            String email = StringUtils.rightPad(StringUtils.left(payee.getEmail(), 100), 100, " ");
            String tel1 = pmiPhoneNumberForITAUColombia.replaceAll("[^a-zA-Z0-9]", "");
            tel1 = StringUtils.rightPad(StringUtils.left(tel1, 14), 14, " ");
            String tel2 = StringUtils.rightPad("", 14, " ");
            String fax1 = StringUtils.rightPad("", 14, " ");
            String address = StringUtils.rightPad(StringUtils.left(pmiAddressForITAUColombia, 40), 40, " ");
            address = address.replaceAll("[^a-zA-Z0-9]", " ");
            String country = StringUtils.rightPad("COL", 4, " ");
            String department = StringUtils.rightPad(departmentCode, 4, " ");
            String city = StringUtils.rightPad(cityCode, 5, " ");
            String clientId = StringUtils.leftPad("3", 2, "0");
            String clientNumber = StringUtils.rightPad(StringUtils.left(pmiClientNumberForITAUColombia, 15), 15, " ");
            String clientAccountType = StringUtils.rightPad("CTE", 3, " ");
            String clientAccountNumber = StringUtils.rightPad(StringUtils.left(
                    (tg.getBankAccount() != null) ? (tg.getBankAccount().getAccountNumber()) : (pmiClientAccountNumberForITAUColombia),
                    17), 17, " ");
            
            data.put(String.valueOf(seq),
                    new String[]{
                        idReg, sequence, dateMovement, idType, idNumber, fullName, officialSignal, officeCode, bankCode,
                        bankAccountCode, bankAccountNumber, transactionType, value, reference, observation, email, tel1, tel2,
                        fax1, address, country, department, city, clientId, clientNumber, clientAccountType, clientAccountNumber
                    });
        });
        return data;
    }
    
    /**
     * Removes decimal separator for a BigDecimal and converts it to a String value
     **/
    private String getFormattedValue(BigDecimal value) {
        value = value.multiply(new BigDecimal(100));
        return String.valueOf(value).split(Pattern.quote("."))[0];
    }
    
    private Map<String, Object[]> getDataMexico(TransactionGroup tg) {
        Map<String, Object[]> data = new TreeMap<>();
        AtomicInteger index = new AtomicInteger(0);
        
        // first row - header
        String productCode = "COM";
        data.put(String.valueOf(index.get()), new String[]{"STK.SALTIT." + productCode});
        
        // second row - company and total amount
        String secondRowFirstColumn = StringUtils.leftPad("5", 2, "0");
        String companyCode = StringUtils.leftPad("59610", 8, "0");
        String thirtyZeros = StringUtils.leftPad("", 30, "0");
        String totalEmployees = StringUtils.leftPad("3", 7, "0");
        
        BigDecimal totalAmount = tg.getTransactions().stream()
                .map(Transaction::getAmountLocalCurrency)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        
        String totalAmountString = StringUtils.leftPad(totalAmount.toString(), 12, "0");
        String productNumber = StringUtils.leftPad("13", 2, "0");
        data.put(String.valueOf(index.incrementAndGet()),
                new String[]{
                    secondRowFirstColumn + companyCode + thirtyZeros + totalEmployees + totalAmountString + productNumber
                });
        
        tg.getTransactions().forEach(t -> {
            ExtendedUser payee = extendedUserService.findOne(t.getPayee().getId()).orElseThrow(() ->
                    new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.payee") + " " + ExtendedUserService.ENTITY_NAME + " id: " + t.getPayee().getId(), ENTITY_NAME, ExtendedUserService.NOT_FOUND));
            
            int paymentIndex = index.incrementAndGet();
            String paymentFirstColumnCode = StringUtils.leftPad("6", 2, "0");
            String paymentIndexString = StringUtils.leftPad(String.valueOf(paymentIndex - 1), 7, "0");
            String cardNumber = StringUtils.leftPad(payee.getCardNumber(), 16, "0");
            String amount = StringUtils.leftPad(t.getAmountLocalCurrency().toString(), 10, "0");
            
            data.put(String.valueOf(paymentIndex),
                    new String[]{paymentFirstColumnCode + paymentIndexString + cardNumber + amount});
            
        });
        return data;
    }
    
    private Map<String, Object[]> getDataArgentina(TransactionGroup tg) {
        Map<String, Object[]> data = new TreeMap<>();
        AtomicInteger index = new AtomicInteger(1);
        data.put(String.valueOf(index.get()),
                new Object[]{
                    "NAME",
                    "CUIT | DNI",
                    "ACCOUNT NUMBER | CBU",
                    "ACCREDITATION DATE",
                    "AMOUNT",
                    "ACCOUNT TYPE",
                    "SUBMERCHANT",
                    "CURRENCY YTPE",
                    "MERCHANT ID",
                    "BENEFICIARY ADDRESS",
                    "BENEFICIARY CITY",
                    "BENEFICIARY COUNTRY",
                    "EMAIL",
                    "BIRTHDATE",
                    "SENDER ID",
                    "ADDRESS"
                });
        tg.getTransactions().forEach(t -> {
            ExtendedUser payee = extendedUserService.findOne(t.getPayee().getId()).orElseThrow(() ->
                    new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.payee") + " " + ExtendedUserService.ENTITY_NAME + " id: " + t.getPayee().getId(), ENTITY_NAME, ExtendedUserService.NOT_FOUND));
            data.put(String.valueOf(index.incrementAndGet()),
                    new Object[]{
                        getPayeeName(payee), // Nombre
                        getPayeeIdNumber(payee), // CUIT
                        payee.getBankAccountNumber(), // nro cuenta - CBU
                        t.getLastUpdated() != null ? t.getLastUpdated().format(DateTimeFormatter.ofPattern("yyyyMMdd")) : ZonedDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd")),// Fecha acreditacion
                        /**getFormattedValue(t.getAmountLocalCurrency()), // importe*/
                        t.getAmountLocalCurrency().toString(), // importe
                        argTypeOfAccount, // Tipo de cuenta
                        t.getMerchant().getCompany(),  // Submerchant
                        t.getCurrency().getCode(), // Tipo moneda
                        t.getMerchant().getTaxId(), // merchant id
                        payee.getPostalAddress(), // beneficiary address
                        payee.getPostalCity() != null ? payee.getPostalCity().getName() : null, // Beneficiary city
                        payee.getPostalCountry() != null ? payee.getPostalCountry().getName() : null, // Beneficiary country
                        payee.getEmail(), // email
                        payee.getBirthDate() != null ? payee.getBirthDate().format(DateTimeFormatter.ofPattern("yyyyMMdd")) : null, // birthdate
                        argSenderId, // sender id
                        argSenderAddress // address
                    }
            );
        });
        return data;
    }
    
    private String validatePayeeInfoForArgDirectPayment(ExtendedUser payee) {
        String reason = "";
        if (!payee.isUseDirectPayment()) {
            reason += translateService.getMessageByCurrentUserParamsNull("transaction.validation.user.not.using.direct.payment") + " ";
        } else {
            if (payee.getBankAccountNumber() == null || payee.getBankAccountNumber().equals(""))
                reason += translateService.getMessageByCurrentUserParamsNull("transaction.validation.user.account.number.invalid") + " ";
            if (payee.getUser().isPayeeAsCompany()) {
                if (payee.getTaxId() == null || payee.getIdTypeTaxId() == null || payee.getIdTypeTaxId().getId() == null)
                    reason += translateService.getMessageByCurrentUserParamsNull("transaction.validation.user.taxid.invalid") + " ";
            } else if (payee.getIdNumber() == null || payee.getIdType() == null || payee.getIdType().getId() == null) {
                reason += translateService.getMessageByCurrentUserParamsNull("transaction.validation.user.taxid.invalid") + " ";
            }
        }
        if (reason.length() == 0) return null;
        
        reason = "Payee: " + reason;
        return reason;
    }
    
    private String validatePayeeInfoForVenDirectPayment(ExtendedUser payee) {
        String reason = "";
        if (!payee.isUseDirectPayment()) {
            reason += translateService.getMessageByCurrentUserParamsNull("transaction.validation.user.not.using.direct.payment") + " ";
        } else {
            if (payee.getBankAccountNumber() == null || payee.getBankAccountNumber().equals(""))
                reason += translateService.getMessageByCurrentUserParamsNull("transaction.validation.user.account.number.invalid") + " ";
            if (payee.getUser().isPayeeAsCompany()) {
                if (payee.getTaxId() == null || payee.getIdTypeTaxId() == null || payee.getIdTypeTaxId().getId() == null)
                    reason += translateService.getMessageByCurrentUserParamsNull("transaction.validation.user.taxid.invalid") + " ";
            } else if (payee.getIdNumber() == null || payee.getIdType() == null || payee.getIdType().getId() == null) {
                reason += translateService.getMessageByCurrentUserParamsNull("transaction.validation.user.taxid.invalid") + " ";
            }
        }
        if (reason.length() == 0) return null;
        
        reason = "Payee: " + reason;
        return reason;
    }
    private String validatePayeeInfoForPeruDirectPayment(ExtendedUser payee) {
        String reason = "";
        if (!payee.isUseDirectPayment()) {
            reason += translateService.getMessageByCurrentUserParamsNull("transaction.validation.user.not.using.direct.payment") + " ";
        } else {
            if (payee.getBankAccountNumber() == null || payee.getBankAccountNumber().equals(""))
                reason += translateService.getMessageByCurrentUserParamsNull("transaction.validation.user.account.number.invalid") + " ";
            if (payee.getUser().isPayeeAsCompany()) {
                if (payee.getTaxId() == null || payee.getIdTypeTaxId() == null || payee.getIdTypeTaxId().getId() == null)
                    reason += translateService.getMessageByCurrentUserParamsNull("transaction.validation.user.taxid.invalid") + " ";
            } else if (payee.getIdNumber() == null || payee.getIdType() == null || payee.getIdType().getId() == null) {
                reason += translateService.getMessageByCurrentUserParamsNull("transaction.validation.user.taxid.invalid") + " ";
            }
        }
        if (reason.length() == 0) return null;
        
        reason = "Payee: " + reason;
        return reason;
    }
    private String validatePayeeInfoForBrazilDirectPayment(ExtendedUser payee) {
        String reason = "";
        if (!payee.isUseDirectPayment())
            reason += "The user is not using direct payment. ";
        else {
            if (payee.getDirectPaymentBank() == null || payee.getDirectPaymentBank().getId() == null)
                reason += "The user has an invalid direct payment bank. ";
            if (payee.getBankAccountNumber() == null || payee.getBankAccountNumber().equals(""))
                reason += "The user has an invalid account number. ";
            if (payee.getUser().isPayeeAsCompany()) {
                if (payee.getTaxId() == null || payee.getIdTypeTaxId() == null || payee.getIdTypeTaxId().getId() == null)
                    reason += "The user has an invalid taxId or type of taxId. ";
            } else if (payee.getIdNumber() == null || payee.getIdType() == null || payee.getIdType().getId() == null) {
                reason += "The user has an invalid idType or type of idType. ";
            }
            if (payee.getBankBranch() == null) {
                reason += "The user has an invalid bank branch value. ";
            }
            if (payee.getBankAccountType() == null) {
                reason += "The user has an invalid account type. ";
            }
        }
        
        if (reason.length() == 0) return null;
        
        reason = "Payee: " + reason;
        return reason;
    }
    
    private Map<String, Object[]> getDataBrazilDirectPayment(TransactionGroup tg) {
        Map<String, Object[]> data = new TreeMap<>();
        AtomicInteger index = new AtomicInteger(1);
        data.put(String.valueOf(index.get()),
                new Object[]{
                    "DOCUMENT ID",
                    "NAME",
                    "TYPE OF ACCOUNT",
                    "BANK CODE",
                    "BANK BRANCH",
                    "BEN BANK ACCOUNT STRUCTURE",
                    "BENEFICIARY EMAIL",
                    "ID MERCHANT",
                    "AMOUNT",
                    "PAYMENT DATE",
                    "CURRENCY",
                    "BENEFICIARY ADDRESS",
                    "CITY",
                    "COUNTRY",
                    "MERCHANT NAME",
                    "SENDER ID",
                    "SENDER ADDRESS",
                    "SENDER_NAME",
                    "SENDER_COUNTRY",
                    "SENDER_STATE",
                    "SENDER_EMAIL",
                    "SENDER_BIRTHDATE",
                    "CONCEPT CODE"
                });
        tg.getTransactions().forEach(t -> {
            ExtendedUser payee = extendedUserService.findOne(t.getPayee().getId()).orElseThrow(() ->
                    new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.payee") + " " + ExtendedUserService.ENTITY_NAME + translateService.getMessageByCurrentUserParamsNull("error.message.id") + t.getPayee().getId(), ENTITY_NAME, ExtendedUserService.NOT_FOUND));
            
            data.put(String.valueOf(index.incrementAndGet()),
                    new Object[]{
                        getPayeeIdNumber(payee), //DOCUMENT ID
                        getPayeeName(payee) != null ? getPayeeName(payee).replaceAll("\\W+"," ") : null, // NAME
                        BankAccountType.getStringFromBankAccountTypeBrazil(payee.getBankAccountType()),  // TYPE OF ACCOUNT
                        getBankCodeByBankId(payee.getDirectPaymentBank().getId()),  //BANK CODE
                        payee.getBankBranch(),  // BANK BRANCH
                        payee.getBankAccountNumber(), // BEN BANK ACCOUNT STRUCTURE
                        payee.getEmail(), //BENEFICIARY EMAIL
                        t.getMerchant().getTaxId() + t.getId(),  //ID MERCHANT
                        /**t.getAmountLocalCurrency() != null ? getFormattedValue(t.getAmountLocalCurrency()) : null,  //AMOUNT*/
                        t.getAmountLocalCurrency() != null ? t.getAmountLocalCurrency().toString() : null,  //AMOUNT*/
                        t.getLastUpdated() != null ? t.getLastUpdated().format(DateTimeFormatter.ofPattern("yyyyMMdd")) : null,  // PAYMENT DATE
                        t.getCurrency() != null ? t.getCurrency().getCode() : null,  // CURRENCY
                        payee.getPostalAddress(),  // BENEFICIARY ADDRESS
                        payee.getPostalCity() != null ? payee.getPostalCity().getName().replaceAll("\\W+"," ") : null,  // CITY
                        payee.getPostalCountry() != null ? payee.getPostalCountry().getName().replaceAll("\\W+"," ") : null,  // COUNTRY
                        t.getMerchant().getCompany() != null ? t.getMerchant().getCompany().replaceAll("\\W+"," ") : null,  // MERCHANT NAME
                        brazilSenderId, // SENDER ID
                        brazilSenderAddress != null ? brazilSenderAddress.replaceAll("\\W+"," ") : null, // SENDER ADDRESS
                        brazilSenderName != null ? brazilSenderName.replaceAll("\\W+"," ") : null, // SENDER_NAME
                        brazilSenderCountry,  // SENDER_COUNTRY
                        brazilSenderState,  // SENDER_STATE
                        brazilSenderEmail,  // SENDER_EMAIL
                        null,               //SENDER_BIRTHDATE
                        brazilConceptCode  // CONCEPT CODE
                    }
            );
            
        });
        return data;
    }
    
    private String getPayeeName(ExtendedUser payee) {
        String payeeName = null;
        if (payee.getUser().isPayeeAsCompany()) {
            if (payee.getCompany() != null) {
                payeeName = payee.getCompany();
            } else {
                payeeName = "missing company name";
            }
        } else {
            payeeName = payee.getFullName();
        }
        return payeeName;
    }
    
    private String getPayeeIdNumber(ExtendedUser payee) {
        String payeeDocument = null;
        if (payee.getUser().isPayeeAsCompany()) {
            payeeDocument = payee.getTaxId();
        } else {
            payeeDocument = payee.getIdNumber();
        }
        return payeeDocument;
    }
    
    private String getBankCodeByBankId(Long bankId) {
        DirectPaymentBank directPaymentBank = directPaymentBankService.findOneByBank(bankId);
        
        if(directPaymentBank != null){
            Long destinyBankCodeLong =  Long.parseLong(directPaymentBank.getDestinyBankCode());
            return String.format("%03d", destinyBankCodeLong);
        }
        
        return null;
    }
}
