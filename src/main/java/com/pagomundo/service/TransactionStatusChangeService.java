package com.pagomundo.service;

import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.Transaction;
import com.pagomundo.domain.TransactionStatusChange;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.repository.ExtendedUserRepository;
import com.pagomundo.repository.TransactionStatusChangeRepository;
import com.pagomundo.repository.search.TransactionStatusChangeSearchRepository;
import com.pagomundo.service.dto.UserDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link TransactionStatusChange}.
 */
@Service
@Transactional
public class TransactionStatusChangeService {

    private final Logger log = LoggerFactory.getLogger(TransactionStatusChangeService.class);

    private final TransactionStatusChangeRepository transactionStatusChangeRepository;

    private final TransactionStatusChangeSearchRepository transactionStatusChangeSearchRepository;
    private final UserService userService;
    private final ExtendedUserRepository extendedUserRepository;
    private final TranslateService translateService;

    public TransactionStatusChangeService(TransactionStatusChangeRepository transactionStatusChangeRepository, TransactionStatusChangeSearchRepository transactionStatusChangeSearchRepository, UserService userService, ExtendedUserRepository extendedUserRepository, TranslateService translateService) {
        this.transactionStatusChangeRepository = transactionStatusChangeRepository;
        this.transactionStatusChangeSearchRepository = transactionStatusChangeSearchRepository;
        this.userService = userService;
        this.extendedUserRepository = extendedUserRepository;
        this.translateService = translateService;
    }

    /**
     * Save a transactionStatusChange.
     *
     * @param transactionStatusChange the entity to save.
     * @return the persisted entity.
     */
    public TransactionStatusChange save(TransactionStatusChange transactionStatusChange) {
        log.debug("Request to save TransactionStatusChange : {}", transactionStatusChange);
        TransactionStatusChange result = transactionStatusChangeRepository.save(transactionStatusChange);
        transactionStatusChangeSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the transactionStatusChanges.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<TransactionStatusChange> findAll(Pageable pageable) {
        log.debug("Request to get all TransactionStatusChanges");
        return transactionStatusChangeRepository.findAll(pageable);
    }


    /**
     * Get one transactionStatusChange by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TransactionStatusChange> findOne(Long id) {
        log.debug("Request to get TransactionStatusChange : {}", id);
        if (id == null) return Optional.empty();
        return transactionStatusChangeRepository.findById(id);
    }

    /**
     * Delete the transactionStatusChange by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TransactionStatusChange : {}", id);
        transactionStatusChangeRepository.deleteById(id);
        transactionStatusChangeSearchRepository.deleteById(id);
    }

    /**
     * Search for the transactionStatusChange corresponding to the query.
     *
     * @param query    the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<TransactionStatusChange> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of TransactionStatusChanges for query {}", query);
        return transactionStatusChangeSearchRepository.search(queryStringQuery(query), pageable);
    }


    @Transactional
    public void saveAll(List<TransactionStatusChange> transactionStatusChangeList) {
        if (!transactionStatusChangeList.isEmpty())
            transactionStatusChangeSearchRepository.saveAll(transactionStatusChangeList);
    }

    /**
     * Create a new transactionStatusChange.
     *
     * @param transaction the entity about to create a transactionStatusChange.
     */
    @Transactional
    public void createStatusChange(Transaction transaction) {
        UserDTO userDTO = userService.getUserWithAuthorities()
            .map(UserDTO::new)
            .orElse(null);

        ExtendedUser currentExtendedUser = userDTO != null ? extendedUserRepository.getByUserId(userDTO.getId()) : transaction.getAdmin();

        createStatusChange(transaction.getStatus(), transaction.getStatusReason(), currentExtendedUser, transaction);
    }

    /**
     * Create a new transactionStatusChange.
     *
     * @param status       the current status of the transaction.
     * @param reason       the reason of the current status of the transaction.
     * @param extendedUser the user who change the transaction's status.
     * @param transaction  the transaction which changed the status.
     */
    @Transactional
    public void createStatusChange(Status status, String reason, ExtendedUser extendedUser, Transaction transaction) {
        
        log.debug("Creating a new TransactionStatusChange for transaction: {}", transaction);

        TransactionStatusChange tsc = new TransactionStatusChange();
        tsc.setTransaction(transaction);
        tsc.setDateCreated(transaction.getLastUpdated());
        tsc.setUser(extendedUser);
        tsc.setStatus(status);
        tsc.setReason(reason);
        
        TransactionStatusChange tscResult = transactionStatusChangeRepository.save(tsc);

        log.debug(">>>>>>> transactionStatusChangeRepository: {}", tscResult);

        transactionStatusChangeSearchRepository.save(tscResult);

        log.debug("transactionStatusChangeSearchRepository saved: {}", tscResult);

    }

    @Async
    public void refetchAllTransactionStatusChange(Collection<TransactionStatusChange> transactionStatusChanges) {
        if (!transactionStatusChanges.isEmpty())
            transactionStatusChangeSearchRepository.findAllByIdIn(transactionStatusChanges.stream().map(TransactionStatusChange::getId).collect(Collectors.toList()))
                .ifPresent(transactionStatusChangeSearchRepository::saveAll);
    }

}
