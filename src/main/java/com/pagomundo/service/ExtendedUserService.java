package com.pagomundo.service;

import com.pagomundo.domain.DTO.ExtendedUserListDTO;
import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.ExtendedUserRelation;
import com.pagomundo.domain.User;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.repository.ExtendedUserRelationRepository;
import com.pagomundo.repository.ExtendedUserRepository;
import com.pagomundo.repository.UserRepository;
import com.pagomundo.repository.search.ExtendedUserSearchRepository;
import com.pagomundo.repository.search.RefetchService;
import com.pagomundo.security.AuthoritiesConstants;
import com.pagomundo.service.dto.EmailChangeDTO;
import com.pagomundo.service.dto.UserDTO;
import com.pagomundo.service.util.FileUtils;
import com.pagomundo.web.rest.errors.BadRequestAlertException;
import com.pagomundo.web.rest.errors.NotFoundAlertException;
import com.pagomundo.web.rest.errors.UserNotFoundException;
import org.apache.commons.io.FilenameUtils;
import org.elasticsearch.index.query.Operator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link ExtendedUser}.
 */
@Service
@Transactional
public class ExtendedUserService {
    public static final String ENTITY_NAME = "ExtendedUser";
    public static final String NOT_FOUND = ENTITY_NAME + ".001";
    public static final String INCORRECT_STATUS = ENTITY_NAME + ".002";
    public static final String BAD_REQUEST = ENTITY_NAME + ".004";
    static final String WRONG_USER_ROLE = ENTITY_NAME + ".003";
    private static final String LOGO_NAME_SEPARATOR = "_";
    private final Logger log = LoggerFactory.getLogger(ExtendedUserService.class);
    private final ExtendedUserRepository extendedUserRepository;
    private final UserRepository userRepository;
    private final ExtendedUserRelationRepository extendedUserRelationRepository;
    private final ExtendedUserSearchRepository extendedUserSearchRepository;
    private final ExtendedUserStatusChangeService extendedUserStatusChangeService;
    private final CityService cityService;
    private final CountryService countryService;
    private final BankService bankService;
    private final IdTypeService idTypeService;
    private final UserService userService;
    private final ExtendedUserRelationService extendedUserRelationService;
    private final RefetchService refetchService;
    private final TranslateService translateService;
    
    @Value("${fileBucket.path}")
    private String fileBucketPath;
    
    public ExtendedUserService(
            ExtendedUserRepository extendedUserRepository,
            UserRepository userRepository,
            ExtendedUserRelationRepository extendedUserRelationRepository,
            ExtendedUserSearchRepository extendedUserSearchRepository,
            ExtendedUserStatusChangeService extendedUserStatusChangeService,
            CityService cityService,
            CountryService countryService,
            BankService bankService,
            IdTypeService idTypeService,
            UserService userService,
            ExtendedUserRelationService extendedUserRelationService,
            RefetchService refetchService, TranslateService translateService) {
        this.extendedUserRepository = extendedUserRepository;
        this.userRepository = userRepository;
        this.extendedUserRelationRepository = extendedUserRelationRepository;
        this.extendedUserSearchRepository = extendedUserSearchRepository;
        this.extendedUserStatusChangeService = extendedUserStatusChangeService;
        this.cityService = cityService;
        this.countryService = countryService;
        this.bankService = bankService;
        this.idTypeService = idTypeService;
        this.userService = userService;
        this.extendedUserRelationService = extendedUserRelationService;
        this.refetchService = refetchService;
        this.translateService = translateService;
    }
    
    /**
     * Save a extendedUser.
     *
     * @param extendedUser the entity to save.
     * @return the persisted entity.
     */
    public ExtendedUser save(ExtendedUser extendedUser, boolean mustChangeStatus, boolean mustUpdateUser) {
        log.debug("Request to save ExtendedUser : {}", extendedUser);
        extendedUser.setLastUpdated(ZonedDateTime.now());
        ExtendedUser result = extendedUserRepository.save(extendedUser);
        extendedUserSearchRepository.save(result);
        if (mustChangeStatus) extendedUserStatusChangeService.createStatusChange(result);
        if (mustUpdateUser && result.getUser() != null) {
            User user = userService.findOne(result.getUser().getId()).orElse(null);
            if (user != null) {
                user.setFirstName(result.getFirstName1());
                user.setLastName(result.getLastName1());
                userService.save(user);
            }
        }
        return result;
    }
    
    /**
     * Save a extendedUser.
     *
     * @param extendedUser the entity to save.
     * @return the persisted entity.
     */
    public ExtendedUser save(ExtendedUser extendedUser) {
        return save(extendedUser, true, false);
    }
    
    /**
     * Save a extendedUser.
     *
     * @param extendedUser the entity to save.
     * @return the persisted entity.
     */
    public ExtendedUser save(ExtendedUser extendedUser, boolean mustChangeStatus) {
        return save(extendedUser, mustChangeStatus, false);
    }
    
    private Status setCreatedUserStatus(ExtendedUser eu) {
        if (eu.getStatus() != null) return eu.getStatus();
        
        Status status;
        if (userService.isPayee(eu.getUser()) && eu.isUseDirectPayment() != null && eu.isUseDirectPayment())
            status = Status.ACCEPTED;
        else {
            if (userService.isAdminOrSuperAdmin(eu.getUser())) status = Status.ACCEPTED;
            else status = Status.CREATED;
        }
        return status;
    }
    
    /**
     * Create a extendedUser.
     *
     * @param eu the entity to save.
     * @return the persisted entity.
     */
    public ExtendedUser create(ExtendedUser eu) {
        
        log.debug("Request to save ExtendedUser : {}", eu);
        
        eu.setEmail(eu.getUser().getEmail());
        eu.setLastName1(eu.getUser().getLastName());
        eu.setFirstName1(eu.getUser().getFirstName());
        eu.setFullName(eu.getFirstName1() + " " + eu.getLastName1());
        eu.setBalance(new BigDecimal(0));
        
        eu.setStatus(setCreatedUserStatus(eu));
        
        eu.setRole(userService.getRole(eu.getUser()));
        
        eu.setDateCreated(ZonedDateTime.now());
        
        
        
        eu.setDirectPaymentCity(eu.getDirectPaymentCity() != null && eu.getDirectPaymentCity().getId() != null ?
                cityService.findOne(eu.getDirectPaymentCity().getId()).orElseThrow(() ->
                        new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.direct.payment") + " " + CityService.ENTITY_NAME + " id: " + eu.getDirectPaymentCity().getId(),
                                ENTITY_NAME, CityService.NOT_FOUND)) : null);
        
        eu.setDirectPaymentBank(eu.getDirectPaymentBank() != null && eu.getDirectPaymentBank().getId() != null ?
                bankService.findOne(eu.getDirectPaymentBank().getId()).orElseThrow(() ->
                        new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.direct.payment") + " " + BankService.ENTITY_NAME + " id: " + eu.getDirectPaymentBank().getId(),
                                ENTITY_NAME, BankService.NOT_FOUND_CODE)) : null);
        
        if (eu.getPostalCity() != null && eu.getPostalCity().getId() != null) {
            eu.setPostalCity(cityService.findOne(eu.getPostalCity().getId()).orElseThrow(() ->
                    new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.postal") + " " + CityService.ENTITY_NAME + " id: " + eu.getPostalCity().getId(), ENTITY_NAME, CityService.NOT_FOUND)));
            eu.setPostalCountry(eu.getPostalCity().getCountry());
        }
        
        if (eu.getResidenceCity() != null && eu.getResidenceCity().getId() != null) {
            eu.setResidenceCity(cityService.findOne(eu.getResidenceCity().getId()).orElseThrow(() ->
                    new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.residence") + " " + CityService.ENTITY_NAME + " id: " + eu.getResidenceCity().getId(), ENTITY_NAME, CityService.NOT_FOUND)));
            eu.setResidenceCountry(eu.getResidenceCity().getCountry());
        }
        
        // Se agrega validacion cuando no se envia cuenta bancaria
        // if(eu.getBankAccountNumber() == null || eu.getBankAccountNumber() == ""){
        //     throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.missing.bank.account"), ENTITY_NAME, "MissingBankAccount");
        // }
        
        eu.setPostalCountry(eu.getPostalCountry() != null && eu.getPostalCountry().getId() != null ?
                countryService.findOne(eu.getPostalCountry().getId()).orElseThrow(() ->
                        new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.postal") + " " + CountryService.ENTITY_NAME + " id: " + eu.getPostalCountry().getId(),
                                ENTITY_NAME, CountryService.NOT_FOUND)) : null);
        
        eu.setResidenceCountry(eu.getResidenceCountry() != null && eu.getResidenceCountry().getId() != null ?
                countryService.findOne(eu.getResidenceCountry().getId()).orElseThrow(() ->
                        new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.residence") + " " + CountryService.ENTITY_NAME + " id: " + eu.getResidenceCountry().getId(),
                                ENTITY_NAME, CountryService.NOT_FOUND)) : null);
        
        eu.setIdType(eu.getIdType() != null && eu.getIdType().getId() != null ?
                idTypeService.findOne(eu.getIdType().getId()).orElseThrow(() ->
                        new NotFoundAlertException(IdTypeService.ENTITY_NAME + " id: " + eu.getIdType().getId(),
                                ENTITY_NAME, IdTypeService.NOT_FOUND)) : null);
        
        eu.setIdTypeTaxId(eu.getIdTypeTaxId() != null && eu.getIdTypeTaxId().getId() != null ?
                idTypeService.findOne(eu.getIdTypeTaxId().getId()).orElseThrow(() ->
                        new NotFoundAlertException(IdTypeService.ENTITY_NAME + " tax id: " + eu.getIdTypeTaxId().getId(),
                                ENTITY_NAME, IdTypeService.NOT_FOUND)) : null);
        
        
        eu.setProfileInfoChanged("");
        if (eu.isMustNotify() == null) eu.setMustNotify(true);
        if (eu.isCanChangePaymentMethod() == null) eu.setCanChangePaymentMethod(true);
        if (eu.isUseDirectPayment() == null) eu.setUseDirectPayment(false);
        if (eu.isUseMerchantCommission() == null) eu.setUseMerchantCommission(false);
        if (eu.isUseFlatCommission() == null) eu.setUseFlatCommission(false);
        
        eu.setConfirmedProfile(getCurrentUser().equals(eu.getUser()));
        
        if (eu.getRole().equals(AuthoritiesConstants.RESELLER)) {
            eu.setResellerFixedCommission(0F);
            eu.setResellerPercentageCommission(0F);
            eu.setUseMerchantCommission(false);
            eu.setUseFlatCommission(false);
        }
        
        ExtendedUser finalExtendedUser = save(eu, true, false);
        
        createInvertedRelations(finalExtendedUser.getUser());
        
        return finalExtendedUser;
    }
    
    private void createInvertedRelations(User user) {
        if (user != null && (userService.isPayee(user) || userService.isMerchant(user) || userService.isReseller(user)))
            extendedUserRelationRepository.findAllByUserRelated(user)
                    .forEach(extendedUserRelationService::getInvertedRelation);
    }
    
    public List<ExtendedUser> createExtendedUserList(List<UserDTO> userListCreated) {
        ArrayList<ExtendedUser> euCreated = new ArrayList<>();
        userListCreated.forEach(userDTO -> {
            userDTO.setStatus(Status.ACCEPTED);
            switch (userDTO.getRole()) {
                case AuthoritiesConstants.PAYEE:
                    if (isValidToCreatePayee(userDTO))
                        create(createPayeeWithDirectPayment(userDTO));
                    break;
                case AuthoritiesConstants.MERCHANT:
                    if (isReseller()) userDTO.setStatus(Status.CREATED);
                    euCreated.add(create(createMerchantOrReseller(userDTO)));
                    break;
                case AuthoritiesConstants.RESELLER:
                    create(createMerchantOrReseller(userDTO));
                    break;
                case AuthoritiesConstants.ADMIN:
                case AuthoritiesConstants.SUPER_ADMIN:
                    create(basicUserDTO(userDTO));
                    break;
                default:
                    break;
            }
        });
        return euCreated;
    }
    
    private boolean isValidToCreatePayee(UserDTO userDTO) {
        boolean valid = false;
        if(userDTO.getBankAccountNumber() != null && userDTO.getCountry() != null && userDTO.getCountry().isForPayee()){
            
             switch (userDTO.getCountry().getId().intValue()) {
                case 1:
                    valid = userDTO.getDirectPaymentBank() != null && userDTO.getDirectPaymentCity() != null;
                    break;
                case 74:
                    valid = validateBrazilPayee(userDTO);
                    break;
                case 66:
                    valid =  validateArgPayee(userDTO);
                    break;
                default:
                    valid = true;
                    break;
            }
        }
        
        return valid;
        
        /*return userDTO.getBankAccountNumber() != null && userDTO.getCountry() != null &&
        (userDTO.getCountry().getId() == 2L || // Mexico
        userDTO.getCountry().getId() == 77L || // Chile
        (userDTO.getCountry().getId() == 1L && // Colombia
        userDTO.getDirectPaymentBank() != null &&
        userDTO.getDirectPaymentCity() != null) ||
        validateArgPayee(userDTO) ||  //Argentina
        validateBrazilPayee(userDTO) // Brazil
        ||userDTO.getCountry().getId() == 101L //Peru
        ); */
    }
    
    private ExtendedUser basicUserDTO(UserDTO userDTO) {
        ExtendedUser eu = new ExtendedUser();
        eu.setUser(userDTO.getUser());
        eu.setRole(userDTO.getRole());
        eu.setStatus(userDTO.getStatus());
        return eu;
    }
    
    private ExtendedUser createMerchantOrReseller(UserDTO userDTO) {
        ExtendedUser eu = basicUserDTO(userDTO);
        
        eu.setCanChangePaymentMethod(false);
        eu.setUseDirectPayment(false);
        eu.setTaxId(userDTO.getTaxId());
        eu.setIdNumber(userDTO.getIdNumber());
        eu.setMobileNumber(userDTO.getMobileNumber());
        eu.setBirthDate(userDTO.getBirthDate());
        eu.setCompany(userDTO.getCompany());
        eu.setAlias(userDTO.getAlias());
        eu.setPostalAddress(userDTO.getPostalAddress());
        eu.setResidenceAddress(userDTO.getResidenceAddress());
        eu.setPostalCountry(userDTO.getPostalCountry());
        eu.setResidenceCountry(userDTO.getResidenceCountry());
        return eu;
    }
    
    private ExtendedUser createPayeeWithDirectPayment(UserDTO userDTO) {
        ExtendedUser eu = basicUserDTO(userDTO);
        
        eu.setCanChangePaymentMethod(true);
        eu.setUseDirectPayment(true);
        eu.setDirectPaymentBank(userDTO.getDirectPaymentBank());
        eu.setDirectPaymentCity(userDTO.getDirectPaymentCity());
        eu.setBankAccountNumber(userDTO.getBankAccountNumber());
        eu.setBankAccountType(userDTO.getBankAccountType());
        eu.setPostalCountry(userDTO.getCountry());
        
        eu.setTaxId(userDTO.getTaxId());
        eu.setIdNumber(userDTO.getIdNumber());
        eu.setIdType(userDTO.getIdType());
        eu.setIdTypeTaxId(userDTO.getIdTypeTaxId());
        eu.setBankBranch(userDTO.getBankBranch());
        
        return eu;
    }
    
    /**
     * Create several extendedUser.
     *
     * @param extendedUserListDTO the group of entity to create.
     * @return the persisted entity.
     */
    public ExtendedUserListDTO create(ExtendedUserListDTO extendedUserListDTO) {
        
        log.debug("Request to create the list of extendedUser : {}", extendedUserListDTO);
        
        ArrayList<ExtendedUser> result = new ArrayList<>();
        ArrayList<ExtendedUser> resultWithErrors = new ArrayList<>();
        
        extendedUserListDTO.getExtendedUsers().forEach(extendedUser -> {
            
            User user = userService.findOne(extendedUser.getUser().getId()).orElse(null);
            
            if (user == null) {
                extendedUser.status(Status.CANCELLED);
                extendedUser.statusReason(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.missing") + " " + extendedUser.getUser().getId());
                resultWithErrors.add(extendedUser);
                return;
            }
            
            if (extendedUserRepository.getByUser(user) != null) {
                extendedUser.status(Status.CANCELLED);
                extendedUser.statusReason(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.exist.id") + " " + extendedUser.getUser().getId());
                resultWithErrors.add(extendedUser);
                return;
            }
            
            try {
                extendedUser.setUser(user);
                result.add(create(extendedUser));
            } catch (BadRequestAlertException brae) {
                extendedUser.status(Status.CANCELLED);
                extendedUser.statusReason(brae.getMessage());
                resultWithErrors.add(extendedUser);
            }
            
        });
        
        extendedUserListDTO.setExtendedUsers(result);
        extendedUserListDTO.setExtendedUsersWithErrors(resultWithErrors);
        return extendedUserListDTO;
    }
    
    
    /**
     * Update several extendedUser.
     *
     * @param extendedUserListDTO the group of entity to update.
     * @return the persisted entity.
     */
    public ExtendedUserListDTO update(ExtendedUserListDTO extendedUserListDTO) {
        log.debug("Request to update the list of extendedUser : {}", extendedUserListDTO);
        ArrayList<ExtendedUser> result = new ArrayList<>();
        ArrayList<ExtendedUser> resultWithErrors = new ArrayList<>();
        extendedUserListDTO.getExtendedUsers().forEach(extendedUser -> {
            ExtendedUser eu = new ExtendedUser();
            try {
                eu = update(extendedUser);
                result.add(eu);
            } catch (BadRequestAlertException brae) {
                eu.status(Status.CANCELLED);
                eu.statusReason(brae.getMessage());
                resultWithErrors.add(eu);
            }
        });
        extendedUserListDTO.setExtendedUsers(result);
        extendedUserListDTO.setExtendedUsersWithErrors(resultWithErrors);
        return extendedUserListDTO;
    }
    
    /**
     * Update a extendedUser.
     *
     * @param eu the entity to save.
     * @return the persisted entity.
     */
    public ExtendedUser update(ExtendedUser eu) {
        log.debug("Request to update ExtendedUser : {}", eu);
        
        ExtendedUser ceu = findOne(eu.getId()).orElseThrow(() ->
                new NotFoundAlertException(ExtendedUserService.ENTITY_NAME + " id: " + eu.getId(), ENTITY_NAME, ExtendedUserService.NOT_FOUND));
        
        log.debug("Current ExtendedUser : {}", ceu);
        
        if (!validateStatusChange(eu.getStatus(), ceu.getStatus())) {
            throw new BadRequestAlertException(
                    translateService.getMessageByCurrentUserParamsNull("error.message.exception.invalid.status") + " " + (ceu.getStatus() != null ? ceu.getStatus().toString() : "[NONE]") + " " + translateService.getMessageByCurrentUserParamsNull("error.message.exception.a") + " " + (eu.getStatus() != null ? eu.getStatus().toString() : "[NONE]")
                    , ENTITY_NAME, "statusChange");
        }
        
        boolean mustChangeStatus = eu.getStatus() != null && !ceu.getStatus().equals(eu.getStatus());
        boolean mustUpdateUser =
                (eu.getFirstName1() != null && !ceu.getFirstName1().equals(eu.getFirstName1()))
                || (eu.getLastName1() != null && !ceu.getLastName1().equals(eu.getLastName1()))
                || eu.getPostalCountry() != null && !ceu.getPostalCountry().getId().equals(eu.getPostalCountry().getId());
        
        if (eu.getStatus() != null) ceu.setStatus(eu.getStatus());
        if (eu.getStatusReason() != null) ceu.setStatusReason(eu.getStatusReason());
        
        if (eu.getBankCommission() != null) ceu.setBankCommission(eu.getBankCommission());
        if (eu.getFxCommission() != null) ceu.setFxCommission(eu.getFxCommission());
        if (eu.getRechargeCost() != null) ceu.setRechargeCost(eu.getRechargeCost());
        if (eu.isUseMerchantCommission() != null) ceu.setUseMerchantCommission(eu.isUseMerchantCommission());
        if (eu.isUseFlatCommission() != null) ceu.setUseFlatCommission(eu.isUseFlatCommission());
        
        if (eu.getLastName1() != null) ceu.setLastName1(eu.getLastName1());
        if (eu.getLastName2() != null) ceu.setLastName2(eu.getLastName2());
        if (eu.getFirstName1() != null) ceu.setFirstName1(eu.getFirstName1());
        if (eu.getFirstName2() != null) ceu.setFirstName2(eu.getFirstName2());
        ceu.setFullName(ceu.getFirstName1() + " " + ceu.getLastName1());
        
        if (eu.getIdNumber() != null) ceu.setIdNumber(eu.getIdNumber());
        if (eu.getTaxId() != null) ceu.setTaxId(eu.getTaxId());
        if (eu.getGender() != null) ceu.setGender(eu.getGender());
        if (eu.getMaritalStatus() != null) ceu.setMaritalStatus(eu.getMaritalStatus());
        if (eu.getResidenceAddress() != null) ceu.setResidenceAddress(eu.getResidenceAddress());
        if (eu.getPostalAddress() != null) ceu.setPostalAddress(eu.getPostalAddress());
        if (eu.getPhoneNumber() != null) ceu.setPhoneNumber(eu.getPhoneNumber());
        if (eu.getMobileNumber() != null) ceu.setMobileNumber(eu.getMobileNumber());
        if (eu.getBirthDate() != null) ceu.setBirthDate(eu.getBirthDate());
        if (eu.getCompany() != null) ceu.setCompany(eu.getCompany());
        if (eu.getAlias() != null) ceu.setAlias(eu.getAlias());
        if (eu.getCardNumber() != null) ceu.setCardNumber(eu.getCardNumber());
        if (eu.getBankAccountNumber() != null) ceu.setBankAccountNumber(eu.getBankAccountNumber());
        if (eu.getBankAccountType() != null) ceu.setBankAccountType(eu.getBankAccountType());
        if (eu.getBankBranch() != null) ceu.setBankBranch(eu.getBankBranch());
        if (eu.isCanChangePaymentMethod() != null) ceu.setCanChangePaymentMethod(eu.isCanChangePaymentMethod());
        
        if (eu.getDirectPaymentCity() != null && eu.getDirectPaymentCity().getId() != null)
            ceu.setDirectPaymentCity(cityService.findOne(eu.getDirectPaymentCity().getId()).orElseThrow(() ->
                    new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.direct.payment") + " " + CityService.ENTITY_NAME + " id: " + eu.getDirectPaymentCity().getId(), ENTITY_NAME, CityService.NOT_FOUND)));
        
        if (eu.getDirectPaymentBank() != null && eu.getDirectPaymentBank().getId() != null)
            ceu.setDirectPaymentBank(bankService.findOne(eu.getDirectPaymentBank().getId()).orElseThrow(() ->
                    new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.direct.payment") + " " + BankService.ENTITY_NAME + " id: " + eu.getDirectPaymentBank().getId(), ENTITY_NAME, BankService.NOT_FOUND_CODE)));
        
        if (eu.getBank() != null && eu.getBank().getId() != null)
            ceu.setBank(bankService.findOne(eu.getBank().getId()).orElseThrow(() ->
                    new NotFoundAlertException(BankService.ENTITY_NAME + " id: " + eu.getBank().getId(),
                            ENTITY_NAME, BankService.NOT_FOUND_CODE)));
        
        if (eu.getResidenceCity() != null && eu.getResidenceCity().getId() != null) {
            ceu.setResidenceCity(cityService.findOne(eu.getResidenceCity().getId()).orElseThrow(() ->
                    new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.residence") + " " + CityService.ENTITY_NAME + " id: " + eu.getResidenceCity().getId(), ENTITY_NAME, CityService.NOT_FOUND)));
            ceu.setResidenceCountry(ceu.getResidenceCity().getCountry());
        }
        
        if (eu.getPostalCity() != null && eu.getPostalCity().getId() != null) {
            ceu.setPostalCity(cityService.findOne(eu.getPostalCity().getId()).orElseThrow(() ->
                    new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.postal") + " " + CityService.ENTITY_NAME + " id: " + eu.getPostalCity().getId(), ENTITY_NAME, CityService.NOT_FOUND)));
            ceu.setPostalCountry(ceu.getPostalCity().getCountry());
        }
        
        if (eu.getPostalCountry() != null && eu.getPostalCountry().getId() != null)
            ceu.setPostalCountry(countryService.findOne(eu.getPostalCountry().getId()).orElseThrow(() ->
                    new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.postal") + " " + CountryService.ENTITY_NAME + " id: " + eu.getPostalCountry().getId(),
                            ENTITY_NAME, CountryService.NOT_FOUND)));
        
        if (eu.getResidenceCountry() != null && eu.getResidenceCountry().getId() != null)
            ceu.setResidenceCountry(countryService.findOne(eu.getResidenceCountry().getId()).orElseThrow(() ->
                    new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.residence") + " " + CountryService.ENTITY_NAME + " id: " + eu.getResidenceCountry().getId(),
                            ENTITY_NAME, CountryService.NOT_FOUND)));
        
        if (eu.getIdType() != null && eu.getIdType().getId() != null)
            ceu.setIdType(idTypeService.findOne(eu.getIdType().getId()).orElseThrow(() ->
                    new NotFoundAlertException(IdTypeService.ENTITY_NAME + " id: " + eu.getIdType().getId(),
                            ENTITY_NAME, IdTypeService.NOT_FOUND)));
        
        if (eu.getIdTypeTaxId() != null && eu.getIdTypeTaxId().getId() != null)
            ceu.setIdTypeTaxId(idTypeService.findOne(eu.getIdTypeTaxId().getId()).orElseThrow(() ->
                    new NotFoundAlertException(IdTypeService.ENTITY_NAME + " tax id: " + eu.getIdTypeTaxId().getId(),
                            ENTITY_NAME, IdTypeService.NOT_FOUND)));
        
        ceu.setMustNotify(mustChangeStatus);
        ceu.setProfileInfoChanged("");
        
        if (eu.isUseDirectPayment() != null && !eu.isUseDirectPayment().equals(ceu.isUseDirectPayment())) {
            if (ceu.isConfirmedProfile() == null || !ceu.isConfirmedProfile())
                throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.confirm.profile.confirm.changing.method")
                        , ENTITY_NAME, "paymentMethod");
            
            ceu.setUseDirectPayment(eu.isUseDirectPayment());
            ceu.setMustNotify(true);
            ceu.setProfileInfoChanged((ceu.getProfileInfoChanged().equals("") ? "" : ceu.getProfileInfoChanged() + "\n")
                    + translateService.getMessageByCurrentUserParamsNull("message.change.payment.method"));
        }
        
        if (ceu.isMustNotify() != null && ceu.isMustNotify())
            ceu.setMustNotify(!ceu.getId().equals(getCurrentExtendedUser().getId()));
        
        if (ceu.isConfirmedProfile() == null || !ceu.isConfirmedProfile())
            ceu.setConfirmedProfile(getCurrentUser().equals(ceu.getUser()));
        
        if (eu.getResellerFixedCommission() != null)
            ceu.setResellerFixedCommission(eu.getResellerFixedCommission());
        else ceu.setResellerFixedCommission(0F);
        if (eu.getResellerPercentageCommission() != null)
            ceu.setResellerPercentageCommission(eu.getResellerPercentageCommission());
        else ceu.setResellerPercentageCommission(0F);
        
        boolean mustUpdateLogo = ceu.getRole().equals(AuthoritiesConstants.RESELLER)
                && eu.getImageAddressUrl() != null && !eu.getImageAddressUrl().equalsIgnoreCase(ceu.getImageAddressUrl());
        if (mustUpdateLogo) ceu.setImageAddressUrl(eu.getImageAddressUrl());
        
        ExtendedUser result = save(ceu, mustChangeStatus, mustUpdateUser);
        if (mustUpdateLogo) extendedUserRelationService.updateLogo(result, result.getImageAddressUrl());
        return result;
    }
    
    /**
     * Get all the extendedUsers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ExtendedUser> findAll(Pageable pageable) {
        log.debug("Request to get all ExtendedUsers");
        return extendedUserRepository.findAll(pageable);
    }
    
    /**
     * Get one extendedUser by id or nickname for payments.
     *
     * @param payee the payee with id or nickname.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ExtendedUser> findPayeeForMerchant(ExtendedUser payee, ExtendedUser merchant) {
        log.debug("Request to get ExtendedUser : {}", payee);
        
        if (payee.getId() != null)
            return extendedUserRelationService.findAcceptedByUserIdAndExtendedUserId(merchant.getUser().getId(), payee.getId())
                    .map(ExtendedUserRelation::getExtendedUser);
        
        if (payee.getEmail() != null && !"".equalsIgnoreCase(payee.getEmail())) {
            List<ExtendedUserRelation> extendedUserRelationList =
                    extendedUserRelationService.findAllAcceptedByUserRelatedIdAndEmail(merchant.getUser().getId(), payee.getEmail());
            return extendedUserRelationList.size() == 1
                    ? Optional.of(extendedUserRelationList.get(0).getExtendedUser())
                    : Optional.empty();
        }
        
        if (payee.getNickname() != null && !"".equalsIgnoreCase(payee.getNickname())) {
            List<ExtendedUserRelation> extendedUserRelationList =
                    extendedUserRelationService.findAllAcceptedByUserRelatedIdAndNickname(merchant.getUser().getId(), payee.getNickname());
            return extendedUserRelationList.size() == 1
                    ? Optional.of(extendedUserRelationList.get(0).getExtendedUser())
                    : Optional.empty();
        }
        return Optional.empty();
    }
    
    
    /**
     * Get one extendedUser by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ExtendedUser> findOne(Long id) {
        log.debug("Request to get ExtendedUser : {}", id);
        if (id == null) return Optional.empty();
        return extendedUserRepository.findById(id);
    }
    
    /**
     * Get one paye by id for reseller.
     *
     * @param payeeId the id of the payee.
     * @param resellerId the id of the reseller.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ExtendedUser> findPayeeForReseller(Long payeeId, Long resellerId) {
        log.debug("Request to get ExtendedUser : {}", payeeId);
        if (payeeId == null || resellerId == null) return Optional.empty();
        return extendedUserRepository.findOneByIdAndResellerId(payeeId, resellerId);
    }
    
    /**
     * Get one paye by id for reseller.
     *
     * @param email the email of the payee.
     * @param resellerId the id of the reseller.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ExtendedUser> findPayeeForReseller(String email, Long resellerId) {
        log.debug("Request to get ExtendedUser : {}", email);
        if (email == null || "".equalsIgnoreCase(email) || resellerId == null) return Optional.empty();
        return extendedUserRepository.findOneByEmailAndResellerId(email, resellerId);
    }
    
    /**
     * Delete the extendedUser by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ExtendedUser : {}", id);
        extendedUserRepository.deleteById(id);
        extendedUserSearchRepository.deleteById(id);
    }
    
    /**
     * Search for the extendedUser corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ExtendedUser> search(String query, Pageable pageable) {       
        String query2=queryStringQuery(query).defaultOperator(Operator.AND).autoGenerateSynonymsPhraseQuery(false).toString();
        String query3=queryStringQuery(query).getName();
        log.info("ExtendedUsers for query exacto elastic: {} , name: {}", query2,query3);
        return extendedUserSearchRepository.search(queryStringQuery(query).defaultOperator(Operator.AND).autoGenerateSynonymsPhraseQuery(false), pageable);
    }
    
    @Transactional
    public void saveAll(List<ExtendedUser> extendedUserList) {
        if (!extendedUserList.isEmpty())
            extendedUserSearchRepository.saveAll(extendedUserList);
    }
    
    /**
     * Check if merchant has enough money to make a payment
     *
     * @param merchant the merchant to evaluate his balance.
     * @param amount the amount to be evaluated.
     * @return the list of entities.
     */
    boolean merchantHasEnoughMoney(ExtendedUser merchant, BigDecimal amount) {
        if (merchant.getBalance() == null) {
            merchant.setBalance(new BigDecimal(0));
            save(merchant, false, false);
        }
        return merchant.getBalance().compareTo(amount) >= 0;
    }
    
    @Async
    public void refetch(Long id) {
        refetchService.refetchExtendedUser(id);
    }
    
    @Async
    public void refetchAll(Collection<ExtendedUser> elements) {
        elements.forEach(e -> refetch(e.getId()));
    }
    
    /**
     * Validate the new status.
     *
     * @param newStatus status to validate.
     * @param currentStatus status of the current entity to validate.
     * @return boolean value according to validation.
     */
    boolean validateStatusChange(Status newStatus, Status currentStatus) {
        if (newStatus == null || newStatus == currentStatus) return true;
        switch (currentStatus) {
            case CREATED:
                return newStatus == Status.IN_PROCESS || newStatus == Status.CANCELLED;
            case FAILED:
                if (!isAdmin(true))
                    throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.current.not.perform.action"), ENTITY_NAME, "WrongUserRole");
                return newStatus == Status.IN_PROCESS || newStatus == Status.CANCELLED;
            case IN_PROCESS:
                if (!isAdmin(true))
                    throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.current.not.perform.action"), ENTITY_NAME, "WrongUserRole");
                return newStatus == Status.ACCEPTED || newStatus == Status.REJECTED;
            case ACCEPTED:
                if (isAdmin(true)) {
                    return newStatus == Status.CANCELLED || newStatus == Status.CREATED || newStatus == Status.REJECTED;
                } else if (isPayee()) {
                    return newStatus == Status.CREATED;
                } else
                    throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.current.not.perform.action"), ENTITY_NAME, "WrongUserRole");
            case REJECTED:
                if (!isAdmin(true))
                    throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.current.not.perform.action"), ENTITY_NAME, "WrongUserRole");
                return newStatus == Status.CANCELLED || newStatus == Status.ACCEPTED;
            case CANCELLED:
                if (!isAdmin(true))
                    throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.current.not.perform.action"), ENTITY_NAME, "WrongUserRole");
                return false;
            default:
                return false;
        }
    }
    
    @Transactional(readOnly = true)
    public ExtendedUser getCurrentExtendedUser() {
        UserDTO userDTO = userService.getUserWithAuthorities()
                .map(UserDTO::new)
                .orElseThrow(() -> new UserNotFoundException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.account.user.not.found")));
        return extendedUserRepository.getByUserId(userDTO.getId());
    }
    
    @Transactional(readOnly = true)
    public Optional<ExtendedUser> getExtendedUser(Long userId) {
        return extendedUserRepository.findOneByUserId(userId);
    }
    
    @Transactional(readOnly = true)
    public User getCurrentUser() {
        UserDTO userDTO = userService.getUserWithAuthorities()
                .map(UserDTO::new)
                .orElseThrow(() -> new UserNotFoundException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.account.user.not.found")));
        return userRepository.getOne(userDTO.getId());
    }
    
    boolean isAdmin(boolean orSuperAdmin) {
        return userService.isAdmin(orSuperAdmin);
    }
    
    public boolean isAdmin() {
        return userService.isAdmin();
    }
    
    public boolean isReseller() {
        return userService.isReseller();
    }
    
    public boolean isMerchant() {
        return userService.isMerchant();
    }
    
    public boolean isPayee() {
        return userService.isPayee();
    }
    
    private List<ExtendedUser> getByRole(String role) {
        return extendedUserRepository.findAllByRole(role);
    }
    
    public ExtendedUser getByUserId(Long id) {
        return extendedUserRepository.getByUserId(id);
    }
    
    private List<ExtendedUser> getByRoleAndStatusIsNot(String role, Status status) {
        return extendedUserRepository.findAllByRoleAndStatusIsNot(role, status);
    }
    
    public List<ExtendedUser> getAllAdmins() {
        List<ExtendedUser> allAdmins;
        allAdmins = getByRole(AuthoritiesConstants.ADMIN);
        allAdmins.addAll(getByRole(AuthoritiesConstants.SUPER_ADMIN));
        return allAdmins;
    }
    
    List<ExtendedUser> getAllActiveAdmins() {
        List<ExtendedUser> allAdmins;
        allAdmins = getByRoleAndStatusIsNot(AuthoritiesConstants.ADMIN, Status.CANCELLED);
        allAdmins.addAll(getByRoleAndStatusIsNot(AuthoritiesConstants.SUPER_ADMIN, Status.CANCELLED));
        return allAdmins;
    }
    
    List<ExtendedUser> getAllActiveSuperAdmins() {
        List<ExtendedUser> allAdmins;
        allAdmins = getByRoleAndStatusIsNot(AuthoritiesConstants.SUPER_ADMIN, Status.CANCELLED);
        return allAdmins;
    }
    
    List<ExtendedUser> getAllActiveResellers() {
        List<ExtendedUser> allAdmins;
        allAdmins = getByRoleAndStatusIsNot(AuthoritiesConstants.RESELLER, Status.CANCELLED);
        return allAdmins;
    }
    
    List<ExtendedUser> getAllActiveMerchants() {
        List<ExtendedUser> allAdmins;
        allAdmins = getByRoleAndStatusIsNot(AuthoritiesConstants.MERCHANT, Status.CANCELLED);
        return allAdmins;
    }
    
    List<ExtendedUser> getAllActivePayees() {
        List<ExtendedUser> allAdmins;
        allAdmins = getByRoleAndStatusIsNot(AuthoritiesConstants.PAYEE, Status.CANCELLED);
        return allAdmins;
    }
    
    public ExtendedUser getExtendedUserFromUser(User user) {
        return extendedUserRepository.getByUserId(user.getId());
    }
    
    Optional<ExtendedUser> getExtendedUserFromUser(Long userId) {
        return extendedUserRepository.findOneByUserId(userId);
    }
    
    List<ExtendedUser> getAllActiveMerchantsFromPayee(Long id) {
        if (id == null) return Collections.emptyList();
        ExtendedUser extendedUser = findOne(id).orElse(null);
        if (extendedUser == null) return Collections.emptyList();
        
        List<ExtendedUser> merchants = new ArrayList<>();
        extendedUserRelationRepository.findAllByUserRelatedId(extendedUser.getUser().getId()).forEach(extendedUserRelation -> {
            ExtendedUser eu = extendedUserRelation.getExtendedUser();
            if (eu != null && !eu.getStatus().equals(Status.CANCELLED) && eu.getRole().equals(AuthoritiesConstants.MERCHANT)) {
                merchants.add(extendedUserRelation.getExtendedUser());
            }
        });
        return merchants;
    }
    
    List<ExtendedUser> getAllActivePayeesFromMerchant(Long id) {
        if (id == null) return Collections.emptyList();
        ExtendedUser extendedUser = findOne(id).orElse(null);
        if (extendedUser == null) return Collections.emptyList();
        
        List<ExtendedUser> payees = new ArrayList<>();
        extendedUserRelationRepository.findAllByUserRelatedId(extendedUser.getUser().getId()).forEach(extendedUserRelation -> {
            ExtendedUser eu = extendedUserRelation.getExtendedUser();
            if (eu != null && !eu.getStatus().equals(Status.CANCELLED) && eu.getRole().equals(AuthoritiesConstants.PAYEE)) {
                payees.add(extendedUserRelation.getExtendedUser());
            }
        });
        return payees;
    }
    
    List<ExtendedUser> getAllActiveMerchantsFromReseller(Long id) {
        if (id == null) return Collections.emptyList();
        return new ArrayList<>(extendedUserRepository.findAllByResellerIdAndStatusNot(id, Status.CANCELLED));
    }
    
    public List<ExtendedUser> getAllExtendedUsers(String role, Status status) {
        List<ExtendedUser> extendedUserList = new ArrayList<>();
        
        List<ExtendedUser> tempList = status == null ?
                extendedUserRepository.findAllByRole(role) :
                extendedUserRepository.findAllByRoleAndStatus(role, status);
        tempList.forEach(extendedUser -> {
            if (!extendedUserList.contains(extendedUser)) extendedUserList.add(extendedUser);
        });
        return extendedUserList;
    }
    
    public Optional<ExtendedUser> changeEmail(EmailChangeDTO emailChangeDTO) {
        return extendedUserRepository.findOneByEmailIgnoreCase(emailChangeDTO.getCurrentEmail())
                .map(extendedUser -> {
                    extendedUser.setEmail(emailChangeDTO.getNewEmail());
                    save(extendedUser);
                    return extendedUser;
                });
    }
    
    public ExtendedUser uploadLogo(MultipartFile logo, ExtendedUser reseller) throws IOException {
        String filename = fileBucketPath + "logo/" +
                reseller.getId().toString() +
                (reseller.getImageAddressUrl() != null && reseller.getImageAddressUrl().contains(LOGO_NAME_SEPARATOR + "B.")
                ? LOGO_NAME_SEPARATOR + "A."
                : LOGO_NAME_SEPARATOR + "B.")
                + FilenameUtils.getExtension(logo.getOriginalFilename());
        
        String filePath = FileUtils.saveUploadedFile(filename, logo);
        return extendedUserRelationService.updateLogo(reseller, filePath);
    }
    
    /**
     * Search extendedUser by query
     *
     * @param query parameter for filter
     * @return Iterable extendedUser
     */
    @Transactional(readOnly = true)
    public Iterable<ExtendedUser> search(String query) {
        log.debug("Request to search for a page of ExtendedUsers for query {}", query);
        return extendedUserSearchRepository.search(queryStringQuery(query));
    }
    
    public Optional<ExtendedUser> changeExtendedUserEmail(User user, String newEmail) {
        return extendedUserRepository.findOneByUser(user).map(extendedUser -> {
            extendedUser.setEmail(newEmail);
            save(extendedUser, false, false);
            return extendedUser;
        });
    }
    
    public boolean isInvited(UserDTO userDTO) {
        if (userDTO == null || userDTO.getId() == null) return false;
        if (userDTO.getUser() == null) return false;
        Optional<ExtendedUser> euOpt = getExtendedUser(userDTO.getId());
        //return userDTO.getUser().getResetDate() != null || euOpt.isEmpty();
        return (userDTO.getUser().getResetDate() != null && ((!euOpt.isEmpty() && euOpt.get().getStatus() != Status.ACCEPTED))) || euOpt.isEmpty();
    }
    
    public UserDTO setInvitationStatus(UserDTO userDTO) {
        if (userDTO == null || userDTO.getId() == null) return userDTO;
        if (userDTO.getUser() == null) return userDTO;
        
        Optional<ExtendedUser> euOpt = getExtendedUser(userDTO.getId());
        
        if (userDTO.getUser().getResetDate() != null && ((!euOpt.isEmpty() && euOpt.get().getStatus() != Status.ACCEPTED) || euOpt.isEmpty()) ) {
            userDTO.setStatus(Status.INVITED);
            userDTO.setStatusReason("Invitation sent but not yet accepted");
            return userDTO;
        }
        
        //Optional<ExtendedUser> euOpt = getExtendedUser(userDTO.getId());
        if (euOpt.isEmpty() || !euOpt.get().isConfirmedProfile()) {
            userDTO.setStatus(Status.PENDING_PROFILE);
            userDTO.setStatusReason("Profile not complete");
        } else {
            userDTO.setStatus(euOpt.get().getStatus());
            userDTO.setStatusReason("Profile completed." + (userDTO.getStatusReason() != null ? " " + userDTO.getStatusReason() : ""));
        }
        return userDTO;
    }
    
    private boolean validateArgPayee(UserDTO userDTO) {
        return userDTO.getCountry().getId().equals(66L) // Argentina
                && ((!userDTO.isPayeeAsCompany() && userDTO.getIdType() != null && userDTO.getIdType().getId().equals(17L) && userDTO.getIdNumber() != null) // Si es persona que tenga DNI y idnumber
                || (userDTO.isPayeeAsCompany() && userDTO.getIdTypeTaxId() != null && userDTO.getIdTypeTaxId().getId().equals(18L) && userDTO.getTaxId() != null));  // Si es empresa que tenga CUIT y taxid
    }
    
    private boolean validateBrazilPayee(UserDTO userDTO) {
        return userDTO.getCountry().getId().equals(74L) // Brazil
                && ((!userDTO.isPayeeAsCompany() && userDTO.getIdType() != null && userDTO.getIdType().getId().equals(19L) && userDTO.getIdNumber() != null) // Si es persona que tenga CPF y idnumber
                || (userDTO.isPayeeAsCompany() && userDTO.getIdTypeTaxId() != null && userDTO.getIdTypeTaxId().getId().equals(20L) && userDTO.getTaxId() != null))    // Si es empresa que tenga CNPJ y taxid
                && userDTO.getBankBranch() != null         // bank branch no nulo
                && userDTO.getBankAccountType() != null;   // account type no nulo
    }
    public List<ExtendedUser> findAllByBankAccountNumber(String bankAccountNumber){
    	log.info("findAllByBankAccountNumber");
    	return extendedUserRepository.findAllByBankAccountNumber(bankAccountNumber);
    }
}
