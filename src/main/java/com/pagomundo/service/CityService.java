package com.pagomundo.service;

import com.pagomundo.domain.City;
import com.pagomundo.domain.DTO.CityDTO;
import com.pagomundo.repository.CityRepository;
import com.pagomundo.repository.search.CitySearchRepository;
import com.pagomundo.repository.search.RefetchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javassist.expr.NewArray;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.ArrayList;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link City}.
 */
@Service
@Transactional
public class CityService {
    public static final String ENTITY_NAME = "City";
    public static final String NOT_FOUND = ENTITY_NAME + ".001";

    private final Logger log = LoggerFactory.getLogger(CityService.class);

    private final CityRepository cityRepository;
    private final CitySearchRepository citySearchRepository;
    private final RefetchService refetchService;

    public CityService(CityRepository cityRepository, CitySearchRepository citySearchRepository, RefetchService refetchService) {
        this.cityRepository = cityRepository;
        this.citySearchRepository = citySearchRepository;
        this.refetchService = refetchService;
    }

    /**
     * Save a city.
     *
     * @param city the entity to save.
     * @return the persisted entity.
     */
    public City save(City city) {
        log.debug("Request to save City : {}", city);
        City result = cityRepository.save(city);
        citySearchRepository.save(result);
        return result;
    }

    /**
     * Get all the cities.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<City> findAll(Pageable pageable) {
        log.debug("Request to get all Cities");
        return cityRepository.findAll(pageable);
    }


    /**
     * Get one city by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<City> findOne(Long id) {
        log.debug("Request to get City : {}", id);
        if (id == null) return Optional.empty();
        return cityRepository.findById(id);
    }

    /**
     * Get all transactions by description
     *
     * @param description the description of the entity.
     * @return the Transaction List.
     */
    @Transactional(readOnly = true)
    public List<City> findAllBySubString(String subStr, Long id) {

        log.debug("Request to get findAllBySubString : {} - {}", subStr, id);

        Optional<List<City>> optcities = cityRepository.findByNameContainingAndCountryId(subStr, id);

        List<City> cities = new ArrayList<>();

        if(optcities.isPresent()){

            cities = optcities.get();
            log.info("result:" + cities.toString());

        }
        else{

            log.info("=_===> No hay ciudades para el substring: {}", subStr);
            //City city = new City();
            //city.setId(0L);
            //city.setName("No cities matched");
            //cities.add(city);
        }
        
        return cities;
    }

    /**
     * Delete the city by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete City : {}", id);
        cityRepository.deleteById(id);
        citySearchRepository.deleteById(id);
    }

    /**
     * Search for the city corresponding to the query.
     *
     * @param query    the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<City> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Cities for query {}", query);
        return citySearchRepository.search(queryStringQuery(query), pageable);
    }

    @Transactional
    public void saveAll(List<City> cityList) {
        if (!cityList.isEmpty())
            citySearchRepository.saveAll(cityList);
    }

    @Async
    public void refetch(Long id) {
        refetchService.refetchCity(id);
    }

    @Async
    public void refetchAll(Collection<City> elements) {
        elements.forEach(e -> refetch(e.getId()));
    }
    /**
     * Metodo para encontrar las ciudades por ciudad
     * @param id del pais a buscar  ciudades
     * @return
     */
    @Transactional(readOnly = true)
    public List<City> findAllByCountry(Long id) {

        log.debug("Request to get cities by country id :{}",  id);

        Optional<List<City>> optcities = cityRepository.findAllByCountryIdOrderByNameAsc( id);

        List<City> cities = new ArrayList<>();

        if(optcities.isPresent()){
            
            cities = optcities.get();
            log.info("size list cities for country: {}" , cities.size());

        }
        else{

            log.info(" No hay ciudades para el id  de conuntry: {}", id);
            
        }
        
        return cities;
    }
}
