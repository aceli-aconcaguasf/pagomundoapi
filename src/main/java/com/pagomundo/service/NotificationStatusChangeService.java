package com.pagomundo.service;

import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.Notification;
import com.pagomundo.domain.NotificationStatusChange;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.repository.ExtendedUserRepository;
import com.pagomundo.repository.NotificationStatusChangeRepository;
import com.pagomundo.repository.search.NotificationStatusChangeSearchRepository;
import com.pagomundo.service.dto.UserDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link NotificationStatusChange}.
 */
@Service
@Transactional
public class NotificationStatusChangeService {

    private final Logger log = LoggerFactory.getLogger(NotificationStatusChangeService.class);

    private final NotificationStatusChangeRepository notificationStatusChangeRepository;

    private final NotificationStatusChangeSearchRepository notificationStatusChangeSearchRepository;
    private final UserService userService;
    private final ExtendedUserRepository extendedUserRepository;

    public NotificationStatusChangeService(NotificationStatusChangeRepository notificationStatusChangeRepository, NotificationStatusChangeSearchRepository notificationStatusChangeSearchRepository, UserService userService, ExtendedUserRepository extendedUserRepository) {
        this.notificationStatusChangeRepository = notificationStatusChangeRepository;
        this.notificationStatusChangeSearchRepository = notificationStatusChangeSearchRepository;
        this.userService = userService;
        this.extendedUserRepository = extendedUserRepository;
    }

    /**
     * Save a notificationStatusChange.
     *
     * @param notificationStatusChange the entity to save.
     * @return the persisted entity.
     */
    public NotificationStatusChange save(NotificationStatusChange notificationStatusChange) {
        log.debug("Request to save NotificationStatusChange : {}", notificationStatusChange);
        NotificationStatusChange result = notificationStatusChangeRepository.save(notificationStatusChange);
        notificationStatusChangeSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the notificationStatusChanges.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<NotificationStatusChange> findAll(Pageable pageable) {
        log.debug("Request to get all NotificationStatusChanges");
        return notificationStatusChangeRepository.findAll(pageable);
    }


    /**
     * Get one notificationStatusChange by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<NotificationStatusChange> findOne(Long id) {
        log.debug("Request to get NotificationStatusChange : {}", id);
        if (id == null) return Optional.empty();
        return notificationStatusChangeRepository.findById(id);
    }

    /**
     * Delete the notificationStatusChange by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete NotificationStatusChange : {}", id);
        notificationStatusChangeRepository.deleteById(id);
        notificationStatusChangeSearchRepository.deleteById(id);
    }

    /**
     * Search for the notificationStatusChange corresponding to the query.
     *
     * @param query    the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<NotificationStatusChange> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of NotificationStatusChanges for query {}", query);
        return notificationStatusChangeSearchRepository.search(queryStringQuery(query), pageable);
    }

    @Transactional
    public void saveAll(List<NotificationStatusChange> notificationStatusChangeList) {
        if (!notificationStatusChangeList.isEmpty())
            notificationStatusChangeSearchRepository.saveAll(notificationStatusChangeList);
    }

    /**
     * Create a new notificationStatusChanges.
     *
     * @param notification the entity about to create a NotificationStatusChange.
     */
    @Transactional
    public void createStatusChange(Notification notification) {
        UserDTO userDTO = userService.getUserWithAuthorities().map(UserDTO::new).orElse(null);
        ExtendedUser currentExtendedUser = userDTO != null ? extendedUserRepository.getByUserId(userDTO.getId()) : notification.getSender();
        createStatusChange(notification.getStatus(), notification.getStatusReason(), currentExtendedUser, notification);
    }

    /**
     * Create a new NotificationStatusChange.
     *
     * @param status       the current status of the notification.
     * @param reason       the reason of the current status of the notification.
     * @param extendedUser the user who change the notification's status.
     * @param notification the notification which changed the status.
     */
    @Transactional
    public void createStatusChange(Status status, String reason, ExtendedUser extendedUser, Notification notification) {
        log.debug("Creating a new NotificationStatusChange for notification: {}", notification);
        NotificationStatusChange nsc = new NotificationStatusChange();
        nsc.setNotification(notification);
        nsc.setDateCreated(notification.getLastUpdated());
        nsc.setUser(extendedUser);
        nsc.setStatus(status);
        nsc.setReason(reason);
        NotificationStatusChange nscResult = notificationStatusChangeRepository.save(nsc);
        notificationStatusChangeSearchRepository.save(nscResult);
        log.debug("NotificationStatusChange saved: {}", nscResult);
    }

    @Async
    public void refetchAllNotificationStatusChange(Collection<NotificationStatusChange> notificationStatusChanges) {
        if (!notificationStatusChanges.isEmpty())
            notificationStatusChangeSearchRepository.findAllByIdIn(notificationStatusChanges.stream().map(NotificationStatusChange::getId).collect(Collectors.toList()))
                .ifPresent(notificationStatusChangeSearchRepository::saveAll);
    }

}
