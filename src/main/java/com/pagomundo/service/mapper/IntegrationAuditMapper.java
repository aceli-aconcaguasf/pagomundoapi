package com.pagomundo.service.mapper;

import com.pagomundo.domain.IntegrationAudit;
import com.pagomundo.service.dto.integrations.IntegrationAuditDTO;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity {@link IntegrationAudit} and its DTO {@link IntegrationAuditDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface IntegrationAuditMapper extends EntityMapper<IntegrationAuditDTO, IntegrationAudit> {


    default IntegrationAudit fromId(Long id) {
        if (id == null) {
            return null;
        }
        IntegrationAudit integrationAudit = new IntegrationAudit();
        integrationAudit.setId(id);
        return integrationAudit;
    }
}
