package com.pagomundo.service.mapper;

import com.pagomundo.domain.IntegrationAuditExtraResponse;
import com.pagomundo.service.dto.integrations.IntegrationAuditExtraResponseDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity {@link IntegrationAuditExtraResponse} and its DTO {@link IntegrationAuditExtraResponseDTO}.
 */
@Mapper(componentModel = "spring", uses = {IntegrationAuditMapper.class})
public interface IntegrationAuditExtraResponseMapper extends EntityMapper<IntegrationAuditExtraResponseDTO, IntegrationAuditExtraResponse> {

    @Mapping(source = "integrationAudit.id", target = "integrationAuditId")
    IntegrationAuditExtraResponseDTO toDto(IntegrationAuditExtraResponse integrationAuditExtraResponse);

    @Mapping(source = "integrationAuditId", target = "integrationAudit")
    IntegrationAuditExtraResponse toEntity(IntegrationAuditExtraResponseDTO integrationAuditExtraResponseDTO);

    default IntegrationAuditExtraResponse fromId(Long id) {
        if (id == null) {
            return null;
        }
        IntegrationAuditExtraResponse integrationAuditExtraResponse = new IntegrationAuditExtraResponse();
        integrationAuditExtraResponse.setId(id);
        return integrationAuditExtraResponse;
    }
}
