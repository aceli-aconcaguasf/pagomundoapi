package com.pagomundo.service;

import com.pagomundo.domain.Bank;
import com.pagomundo.domain.IdType;
import com.pagomundo.repository.IdTypeRepository;
import com.pagomundo.repository.search.IdTypeSearchRepository;
import com.pagomundo.repository.search.RefetchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link IdType}.
 */
@Service
@Transactional
public class IdTypeService {
    public static final String ENTITY_NAME = "IdType";
    public static final String NOT_FOUND = ENTITY_NAME + ".001";

    private final Logger log = LoggerFactory.getLogger(IdTypeService.class);

    private final IdTypeRepository idTypeRepository;
    private final IdTypeSearchRepository idTypeSearchRepository;
    private final RefetchService refetchService;

    public IdTypeService(IdTypeRepository idTypeRepository, IdTypeSearchRepository idTypeSearchRepository, RefetchService refetchService) {
        this.idTypeRepository = idTypeRepository;
        this.idTypeSearchRepository = idTypeSearchRepository;
        this.refetchService = refetchService;
    }

    /**
     * Save a idType.
     *
     * @param idType the entity to save.
     * @return the persisted entity.
     */
    public IdType save(IdType idType) {
        log.debug("Request to save IdType : {}", idType);
        IdType result = idTypeRepository.save(idType);
        idTypeSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the idTypes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<IdType> findAll(Pageable pageable) {
        log.debug("Request to get all IdTypes");
        return idTypeRepository.findAll(pageable);
    }

    /**
     * Get all the idTypes.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Iterable<IdType> findAll() {
        log.debug("Request to get all IdTypes");
        return idTypeRepository.findAll();
    }


    /**
     * Get one idType by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<IdType> findOne(Long id) {
        log.debug("Request to get IdType : {}", id);
        if (id == null) return Optional.empty();
        return idTypeRepository.findById(id);
    }

    /**
     * Delete the idType by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete IdType : {}", id);
        idTypeRepository.deleteById(id);
        idTypeSearchRepository.deleteById(id);
    }

    /**
     * Search for the idType corresponding to the query.
     *
     * @param query    the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<IdType> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of IdTypes for query {}", query);
        return idTypeSearchRepository.search(queryStringQuery(query), pageable);
    }


    @Transactional
    public void saveAll(List<IdType> idTypeList) {
        if (!idTypeList.isEmpty())
            idTypeSearchRepository.saveAll(idTypeList);
    }

    @Async
    public void refetch(Long id) {
        refetchService.refetchIdTye(id);
    }

    @Async
    public void refetchAll(Collection<Bank> elements) {
        elements.forEach(e -> refetch(e.getId()));
    }
}
