package com.pagomundo.service;

import com.pagomundo.domain.Bank;
import com.pagomundo.domain.DTO.ExtendedUserGroupDTO;
import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.ExtendedUserGroup;
import com.pagomundo.domain.enumeration.Gender;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.repository.ExtendedUserGroupRepository;
import com.pagomundo.repository.search.ExtendedUserGroupSearchRepository;
import com.pagomundo.service.util.FileUtils;
import com.pagomundo.web.rest.errors.NotFoundAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link ExtendedUserGroup}.
 */
@Service
@Transactional
public class ExtendedUserGroupService {
    private static final String ENTITY_NAME = "extendedUserGroup";
    private final Logger log = LoggerFactory.getLogger(ExtendedUserGroupService.class);

    private final ExtendedUserGroupRepository extendedUserGroupRepository;
    private final ExtendedUserService extendedUserService;
    private final BankService bankService;

    private final ExtendedUserGroupSearchRepository extendedUserGroupSearchRepository;
    @Value("${fileBucket.path}")
    private String fileBucketPath;

    public ExtendedUserGroupService(ExtendedUserGroupRepository extendedUserGroupRepository, ExtendedUserService extendedUserService, BankService bankService, ExtendedUserGroupSearchRepository extendedUserGroupSearchRepository) {
        this.extendedUserGroupRepository = extendedUserGroupRepository;
        this.extendedUserService = extendedUserService;
        this.bankService = bankService;
        this.extendedUserGroupSearchRepository = extendedUserGroupSearchRepository;
    }

    /**
     * Save a extendedUserGroup.
     *
     * @param extendedUserGroup the entity to save.
     * @return the persisted entity.
     */
    public ExtendedUserGroup save(ExtendedUserGroup extendedUserGroup) {
        log.debug("Request to save ExtendedUserGroup : {}", extendedUserGroup);
        ExtendedUserGroup result = extendedUserGroupRepository.save(extendedUserGroup);
        extendedUserGroupSearchRepository.save(result);
        return result;
    }

    /**
     * Create a extendedUserGroup.
     *
     * @param extendedUserGroupDTO the entity to save.
     * @return the persisted entity.
     */
    public ExtendedUserGroupDTO create(ExtendedUserGroupDTO extendedUserGroupDTO) {
        log.debug("Request to create the list of ExtendedUsers : {}", extendedUserGroupDTO);
        Bank bank = bankService.findOne(extendedUserGroupDTO.getBank().getId()).orElseThrow(() ->
            new NotFoundAlertException(BankService.ENTITY_NAME + " id: " + extendedUserGroupDTO.getBank().getId(), ENTITY_NAME, BankService.NOT_FOUND_CODE));

        ExtendedUserGroup eug = new ExtendedUserGroup();
        eug.setStatus(Status.CREATED);
        eug.setDateCreated(ZonedDateTime.now());
        eug.setAdmin(extendedUserService.getCurrentExtendedUser());
        eug.setBank(bank);
        save(eug);

        Set<ExtendedUser> extendedUser = new HashSet<>();
        Set<ExtendedUser> extendedUserWithErrors = new HashSet<>();
        extendedUserGroupDTO.getExtendedUsers().forEach(eu -> {
            ExtendedUser euTemp = extendedUserService.findOne(eu.getId()).orElse(null);
            String reason = nullIfItIsPossibleToProcess(euTemp, eug);
            if (reason == null) {
                euTemp.setExtendedUserGroup(eug);
                euTemp.setStatus(Status.IN_PROCESS);
                euTemp.setBank(bank);
                extendedUser.add(extendedUserService.update(euTemp));
            } else {
                if (euTemp == null) {
                    euTemp = new ExtendedUser();
                    euTemp.setId(eu.getId());
                }
                euTemp.setStatus(Status.FAILED);
                euTemp.setStatusReason(reason);
                extendedUserWithErrors.add(extendedUserService.update(euTemp));
            }
        });
        extendedUserGroupDTO.setExtendedUsersWithErrors(extendedUserWithErrors);
        extendedUserGroupDTO.setExtendedUsers(extendedUser);
        extendedUserGroupDTO.setId(eug.getId());

        if (extendedUser.isEmpty()) delete(eug.getId());
        else {
            eug.setExtendedUsers(extendedUser);
            save(eug);
        }
        return extendedUserGroupDTO;
    }

    private String nullIfItIsPossibleToProcess(ExtendedUser eu, ExtendedUserGroup eug) {
        if (eug == null) return "User group is invalid.";
        if (eu == null) return "User is invalid.";
        if (!extendedUserService.validateStatusChange(Status.IN_PROCESS, eu.getStatus()))
            return "Invalid user status cycle: from " + Status.getStringFromUserStatus(eu.getStatus()) + " to " +
                Status.getStringFromUserStatus(Status.IN_PROCESS);

        switch (eug.getBank().getId().intValue()) {
            case 1: // Pichincha Colombia
                return validateExtendedUserInfoForPichincha(eu);
            case 2: // Itau Colombia
                return validateExtendedUserInfoForItau(eu);
            case 3: // Mexico
                return validateExtendedUserInfoForMexico(eu);
            case 77: //chile
                return null;
            case 30: // STP Mexico
            default:
                return "Invalid bank selected to process the users.";
        }
    }

    private String validateExtendedUserInfoForPichincha(ExtendedUser eu) {
        String reason = "";
        if (!eu.getUser().isPayeeAsCompany() && eu.getFullName() == null) {
            reason += "The user is not a company and s/he has an invalid full name. ";
            if (eu.getIdNumber() == null || (eu.getIdType() == null || eu.getIdType().getId() == null))
                reason += "The user has an invalid idNumber or type of idNumber. ";
        }
        if (eu.getPostalCity() == null) reason += "The user has an invalid postal city. ";
        if (eu.getResidenceCity() == null) reason += "The user has an invalid residence city. ";
        if (eu.getBirthDate() == null) reason += "The user has an invalid birth date. ";

        if (reason.length() == 0) return null;

        reason = "Payee: " + reason;
        return reason;
    }

    private String validateExtendedUserInfoForItau(ExtendedUser eu) {
        String reason = "";
        if (eu.getUser().isPayeeAsCompany() && eu.getCompany() == null)
            reason += "The user is a company and s/he has an invalid company name. ";
        if (!eu.getUser().isPayeeAsCompany() && eu.getFullName() == null)
            reason += "The user is not a company and s/he has an invalid full name. ";
        if (eu.getPostalCity() == null) reason += "The user has an invalid postal city. ";

        if (reason.length() == 0) return null;

        reason = "Payee: " + reason;
        return reason;
    }

    private String validateExtendedUserInfoForMexico(ExtendedUser eu) {
        String reason = "";
        if (!eu.getUser().isPayeeAsCompany() && (eu.getIdNumber() == null || (eu.getIdType() == null || eu.getIdType().getId() == null)))
            reason += "The user has an invalid idNumber or type of idNumber. ";
        if (eu.getBirthDate() == null) reason += "The user has an invalid birth date. ";

        if (reason.length() == 0) return null;

        reason = "Payee: " + reason;
        return reason;
    }

    @Async
    public void exportFile(ExtendedUserGroup eug) throws IOException {
        eug.setStatus(Status.IN_PROCESS);
        eug = save(eug);

        switch (eug.getBank().getId().intValue()) {
            case 1: // Pichincha Colombia
            case 2: // Itau Colombia
                eug.setFileName(generateSpreadSheet(eug));
                eug.setStatus(Status.ACCEPTED);
                break;
            case 3: // Banco de Mexico
                eug.setFileName(generateCSVfile(eug));
                eug.setStatus(Status.ACCEPTED);
                break;
            default:
                eug.setStatus(Status.CANCELLED);
                break;
        }
        save(eug);
    }

    /**
     * File name creation for the extendedUserGroup.
     *
     * @param eug the extendedUserGroup to make the fileName.
     * @return the file name of the extendedUserGroup.
     */
    private String createFileName(ExtendedUserGroup eug, String fileExtension) {
        String pattern = "yyyy_MM_dd_HH_mm_ss_SS";
        String date = eug.getDateCreated().format(DateTimeFormatter.ofPattern(pattern));

        Bank bank = bankService.findOne(eug.getBank().getId()).orElseThrow(() ->
            new NotFoundAlertException(BankService.ENTITY_NAME + " id: " + eug.getBank().getId(), ENTITY_NAME, BankService.NOT_FOUND_CODE));

        String bankName = bank.getName();
        String countryName = bank.getCountry().getName();

        String fileName = date + "-" + bankName + "-" + countryName + fileExtension;
        log.debug("extendedUserGroup file name: {}", fileName);
        return fileName;
    }

    /**
     * CSV file creation for the extendedUserGroup.
     *
     * @param eug the extendedUserGroup to make the XLS file.
     * @return the file name of the extendedUserGroup XLS file.
     */
    private String generateCSVfile(ExtendedUserGroup eug) throws IOException {
        String fileName = fileBucketPath + "extendedUser/exportedFiles/" + createFileName(eug, ".csv");
        Map<String, String[]> data = getDataMexico(eug);

        FileUtils.writeDownCSVFile(fileName, data);
        return fileName;
    }

    /**
     * Spread sheet creation for the extendedUserGroup.
     *
     * @param eug the extendedUserGroup to make the XLS file.
     * @return the file name of the extendedUserGroup XLS file.
     */
    private String generateSpreadSheet(ExtendedUserGroup eug) throws IOException {
        String fileName = fileBucketPath + "extendedUser/exportedFiles/" + createFileName(eug, ".xlsx");
        String sheetName = "Recharge";
        Map<String, Object[]> data;

        if (eug.getBank().getId() == 1L) { // Pichincha Colombia
            data = getDataPichincha(eug);
        } else { // Itau Colombia
            data = getDataItau(eug);
        }

        FileUtils.writeDownWorkbook(fileName, sheetName, data);
        return fileName;
    }

    private Map<String, Object[]> getDataPichincha(ExtendedUserGroup eug) {
        Map<String, Object[]> data = new TreeMap<>();
        AtomicInteger index = new AtomicInteger(1);
        Object[] cabecera = new Object[]{
            "No. TARJETA",
            "SUCURSAL",
            "TIPO ID",
            "Id. PERSONA",
            "PRIMER APELLIDO",
            "SEGUNDO APELLIDO",
            "PRIMER NOMBRE",
            "SEGUNDO NOMBRE",
            "FECHA NACIMIENTO",
            "ESTADO CIVIL",
            "SEXO",
            "DIRECCIÓN RESIDENCIA",
            "CIUDAD RESIDENCIA",
            "DIRECCIÓN CORRESPONDENCIA",
            "CIUDAD CORRESPONDENCIA",
            "TELÉFONO FIJO",
            "TELÉFONO CELULAR",
            "CORREO ELECTRONICO"
        };
        data.put(String.valueOf(index.get()), cabecera);

        eug.getExtendedUsers().forEach(eu -> {
            Object[] row = new Object[]{
                "", // "No. TARJETA",
                "", // "SUCURSAL",
                eu.getIdType().getCode(), // "TIPO ID",
                eu.getIdNumber(), // "Id. PERSONA",
                eu.getLastName1(), // "PRIMER APELLIDO",
                eu.getLastName2(), // "SEGUNDO APELLIDO",
                eu.getFirstName1(), // "PRIMER NOMBRE",
                eu.getFirstName2(), // "SEGUNDO NOMBRE",
                eu.getBirthDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")), // "FECHA NACIMIENTO",
                eu.getMaritalStatus(), // "ESTADO CIVIL",
                eu.getGender(), // "SEXO",
                eu.getResidenceAddress(), // "DIRECCIÓN RESIDENCIA",
                eu.getResidenceCity().getCode(), // "CIUDAD RESIDENCIA",
                eu.getPostalAddress(), // "DIRECCIÓN CORRESPONDENCIA",
                eu.getPostalCity().getCode(), // "CIUDAD CORRESPONDENCIA",
                eu.getPhoneNumber(), // "TELÉFONO FIJO",
                eu.getMobileNumber(), // "TELÉFONO CELULAR",
                eu.getUser().getEmail(), // "CORREO ELECTRONICO"
            };
            data.put(String.valueOf(index.incrementAndGet()), row);
        });

        return data;
    }

    private Map<String, Object[]> getDataItau(ExtendedUserGroup eug) {
        Map<String, Object[]> data = new TreeMap<>();
        AtomicInteger index = new AtomicInteger(1);
        Object[] cabecera = new Object[]{
            "Tp Identificacion",
            "Numero de Identificacion",
            "Primer Apellido",
            "Segundo Apellido",
            "Primer Nombre",
            "Segundo Nombre",
            "Nombre de la Tarjeta",
            "Tipo de Novedad",
            "Direccion Oficina",
            "Codigo C.",
            "Telefono Oficina",
            "Resultado"
        };
        data.put(String.valueOf(index.get()), cabecera);

        eug.getExtendedUsers().forEach(eu -> {
            Object[] row = new Object[]{
                "01", // "Tp Identificación",
                eu.getIdNumber(), // "Numero de Identificación",
                eu.getLastName1(), // "Primer Apellido",
                eu.getLastName2(), // "Segundo Apellido",
                eu.getFirstName1(), // "Primer Nombre",
                eu.getFirstName2(), // "Segundo Nombre",
                eu.getUser().isPayeeAsCompany() ? eu.getCompany() : eu.getFullName(), // "Nombre de la Tarjeta",
                "CRE", // "Tipo de Novedad",
                eu.getPostalAddress() + " " + eu.getPostalCity().getName(), // "Direccion Oficina",
                "11001", // "Codigo C.",
                eu.getMobileNumber(), // "Telefono Oficina"
                "" // resultado
            };
            data.put(String.valueOf(index.incrementAndGet()), row);
        });
        return data;
    }

    private Map<String, String[]> getDataMexico(ExtendedUserGroup eug) {
        Map<String, String[]> data = new TreeMap<>();
        AtomicInteger index = new AtomicInteger(0);
        eug.getExtendedUsers().forEach(eu -> {
            String[] row = new String[]{
                eu.getLastName1(), // "Primer Apellido",
                eu.getLastName2(), // "Segundo Apellido",
                eu.getFirstName1(), // "Primer Nombre",
                eu.getIdNumber(), // "Numero de Identificación",
                eu.getBirthDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")), // "Fecha de nacimiento",
                Gender.getGenderCode(eu.getGender()) // "Sexo"
            };
            data.put(String.valueOf(index.incrementAndGet()), row);
        });
        return data;
    }

    /**
     * Get all the extendedUserGroups.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ExtendedUserGroup> findAll(Pageable pageable) {
        log.debug("Request to get all ExtendedUserGroups");
        return extendedUserGroupRepository.findAll(pageable);
    }

    @Transactional
    public void saveAll(List<ExtendedUserGroup> extendedUserStatusChangeList) {
        if (!extendedUserStatusChangeList.isEmpty())
            extendedUserGroupSearchRepository.saveAll(extendedUserStatusChangeList);
    }


    /**
     * Get one extendedUserGroup by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ExtendedUserGroup> findOne(Long id) {
        log.debug("Request to get ExtendedUserGroup : {}", id);
        if (id == null) return Optional.empty();
        return extendedUserGroupRepository.findById(id);
    }

    /**
     * Delete the extendedUserGroup by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ExtendedUserGroup : {}", id);
        extendedUserGroupRepository.deleteById(id);
        extendedUserGroupSearchRepository.deleteById(id);
    }

    /**
     * Search for the extendedUserGroup corresponding to the query.
     *
     * @param query    the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ExtendedUserGroup> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ExtendedUserGroups for query {}", query);
        return extendedUserGroupSearchRepository.search(queryStringQuery(query), pageable);
    }
}
