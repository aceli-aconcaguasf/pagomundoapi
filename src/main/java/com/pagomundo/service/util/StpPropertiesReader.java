package com.pagomundo.service.util;

import org.springframework.beans.factory.annotation.Value;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class StpPropertiesReader implements PropertiesReader {

    private Properties appProps;

    @Value("${stpConfig.path}")
    private String stpConfigPath;

    @Override
    public void initialize() throws IOException {
        String appConfigPath = "./stpConfig/" + "access.properties";
        appProps = new Properties();
        appProps.load(new FileInputStream(appConfigPath));
    }

    @Override
    public String getValue(String property) {
        return appProps.getProperty(property);
    }
}
