package com.pagomundo.service.util;

import com.pagomundo.service.dto.integrations.stp.IntegrationStpDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Signature;
import java.security.SignatureException;
import java.security.interfaces.RSAPrivateKey;
import java.util.Base64;

public class StpCryptoHandler {
    private final Logger log = LoggerFactory.getLogger(StpCryptoHandler.class);

    private String path = "C:\\desarrollos\\PagoMundo\\pagomundoapi\\stpConfig\\";
    private String fileName = path + "prueba.jks";
    private String password = "12345678";
    private String alias = "stpCert";

    public StpCryptoHandler(StpPropertiesReader stpPropertiesReader) {
        String certPath = stpPropertiesReader.getValue("path");
        this.path = certPath != null ? certPath : this.path;
        String certFileName = stpPropertiesReader.getValue("certificateFileName");
        this.fileName = certFileName != null ? path + certFileName : this.fileName;
        String certPassword = stpPropertiesReader.getValue("password");
        this.password = certPassword != null ? certPassword : this.password;
        String certAlias = stpPropertiesReader.getValue("alias");
        this.alias = certAlias != null ? certAlias : this.alias;
    }

    public String signRequest(IntegrationStpDTO integrationStpDTO) {
        String signature = null;
        try {
            signature = sign(setChainToEncrypt(integrationStpDTO));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return signature;
    }

    private String setChainToEncrypt(IntegrationStpDTO integrationStpDTO) {
        String chain = "||" +
            integrationStpDTO.getBankIdDestiny().getValue() + "|" +
            integrationStpDTO.getCompany() + "|" +
            (integrationStpDTO.getOperationDate() == null ? "" : integrationStpDTO.getOperationDate()) + "|" +
            (integrationStpDTO.getInvoiceOrigin() == null ? "" : integrationStpDTO.getInvoiceOrigin()) + "|" +
            (integrationStpDTO.getTrackingId() == null ? "" : integrationStpDTO.getTrackingId()) + "|" +
            (integrationStpDTO.getBankIdOrigin() == null ? "" : integrationStpDTO.getBankIdOrigin().getValue()) + "|" +
            (integrationStpDTO.getAmount() == null ? "" : integrationStpDTO.getAmount()) + "|" +
            (integrationStpDTO.getPaymentType().getValue() == null ? "" : integrationStpDTO.getPaymentType().getValue()) + "|" +
            (integrationStpDTO.getAccountTypeOrigin() == null ? "" : integrationStpDTO.getAccountTypeOrigin().getValue()) + "|" +
            (integrationStpDTO.getOriginName() == null ? "" : integrationStpDTO.getOriginName()) + "|" +
            (integrationStpDTO.getAccountNumberOrigin() == null ? "" : integrationStpDTO.getAccountNumberOrigin()) + "|" +
            (integrationStpDTO.getRfcCurpOrigin() == null ? "" : integrationStpDTO.getRfcCurpOrigin()) + "|" +
            (integrationStpDTO.getAccountTypeDestiny() == null ? "" : integrationStpDTO.getAccountTypeDestiny().getValue()) + "|" +
            (integrationStpDTO.getDestinyName() == null ? "" : integrationStpDTO.getDestinyName()) + "|" +
            (integrationStpDTO.getAccountNumberDestiny() == null ? "" : integrationStpDTO.getAccountNumberDestiny()) + "|" +
            (integrationStpDTO.getRfcCurpDestiny() == null ? "" : integrationStpDTO.getRfcCurpDestiny()) + "|" +
            (integrationStpDTO.getEmailDestiny() == null ? "" : integrationStpDTO.getEmailDestiny()) + "|" +
            "|" + // beneficiario2 is not in documentation
            "|" + // getNombreBeneficiario2 is not in documentation
            "|" + // getCuentaBeneficiario2 is not in documentation
            "|" + // getRfcCurpBeneficiario2 is not in documentation
            (integrationStpDTO.getDescription() == null ? "" : integrationStpDTO.getDescription()) + "|" +
            "|" + // getConceptoPago2 is not in documentation
            "|" + // getClaveCatUsuario1 is not in documentation
            "|" + // getClaveCatUsuario2 is not in documentation
            "|" + // getClavePago is not in documentation
            "|" + // getReferenciaCobranza is not in documentation
            (integrationStpDTO.getReferenceNumber() == null ? "" : integrationStpDTO.getReferenceNumber()) + "|" +
            "|" + // getTipoOperacion is not in documentation
            (integrationStpDTO.getTopology() == null ? "" : integrationStpDTO.getTopology()) + "|" + // getTopologia
            "|" + // getUsuario is not in documentation
            "|" + // getMedioEntrega is not in documentation
            "|" + // getPrioridad is not in documentation
            "||" // getIva is not in documentation
            ;
        log.debug("Chain to encrypt: {}", chain);
        return chain;
    }

    public String sign(String chain) throws Exception {
        String retVal;
        try {
            Signature signature = Signature.getInstance("SHA256withRSA");
            RSAPrivateKey privateKey = getCertified(fileName, password, alias);
            signature.initSign(privateKey);
            byte[] bytes = chain.getBytes(StandardCharsets.UTF_8);
            signature.update(bytes, 0, bytes.length);

            retVal = Base64.getEncoder().encodeToString(signature.sign());
            log.debug("Signature: {}", retVal);
        } catch (NoSuchAlgorithmException e) {
            throw new NoSuchAlgorithmException("NoSuchAlgorithmException", e);
        } catch (InvalidKeyException e) {
            throw new InvalidKeyException("InvalidKeyException: ", e);
        } catch (SignatureException e) {
            throw new SignatureException("SignatureException", e);
        } catch (NoSuchProviderException e) {
            throw new NoSuchProviderException(e.getLocalizedMessage());
        }
        return retVal;
    }

    private RSAPrivateKey getCertified(String keystoreFilename, String password, String alias) throws Exception {
        RSAPrivateKey privateKey;
        try (FileInputStream keystoreFileInputStream = new FileInputStream(keystoreFilename)) {
            KeyStore keystore = KeyStore.getInstance("JKS");
            keystore.load(keystoreFileInputStream, password.toCharArray());
            privateKey = (RSAPrivateKey) keystore.getKey(alias, password.toCharArray());
        } catch (FileNotFoundException ex) {
            throw new FileNotFoundException(keystoreFilename);
        } catch (IOException ex) {
            throw new IOException("IOException", ex);
        } catch (Exception ex) {
            throw new Exception("Exception", ex);
        }
        return privateKey;
    }
}

