package com.pagomundo.service.util;

import liquibase.util.csv.opencsv.CSVWriter;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class FileUtils {

    private FileUtils() {
    }

    /**
     * Saves a file into the files bucket
     *
     * @param filePath the path of the file to save
     * @param file     the file to save
     * @return the saved file public url
     * @throws IOException saving file error
     */
    public static String saveUploadedFile(String filePath, MultipartFile file) throws IOException {
        if (file.isEmpty()) return null;
        Files.write(Paths.get(filePath), file.getBytes());
        return filePath;
    }

    /**
     * Saves a file into the files bucket
     *
     * @param fileName the name of the file to be saved
     * @param data     the data to be saved in the file
     * @throws IOException saving file error
     */
    public static void writeDownCSVFile(String fileName, Map<String, String[]> data) throws IOException {

        CSVWriter csvWriter = null;

        if(fileName.indexOf("_BEX") != -1){
            // Separator for bex is ";"
            csvWriter = new CSVWriter(
                new FileWriter(fileName),
                ';',
                CSVWriter.NO_QUOTE_CHARACTER,
                CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                CSVWriter.DEFAULT_LINE_END
            );   
        }
        else{
            csvWriter = new CSVWriter(
                new FileWriter(fileName),
                CSVWriter.DEFAULT_SEPARATOR,
                CSVWriter.NO_QUOTE_CHARACTER,
                CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                CSVWriter.DEFAULT_LINE_END
            );
        }

        Set<String> keySet = data.keySet();
        for (String key : keySet) {
            csvWriter.writeNext(data.get(key));
        }
        csvWriter.close();
    }

    /**
     * Saves a file into the files bucket
     *
     * @param fileName the name of the file to be saved
     * @param data     the data to be saved in the file
     * @throws IOException saving file error
     */
    public static void writeDownCSVFileIntKey(String fileName, Map<Integer, String[]> data) throws IOException {

        CSVWriter csvWriter = null;

        if(fileName.indexOf("_BEX") != -1){
            // Separator for bex is ";"
            csvWriter = new CSVWriter(
                new FileWriter(fileName),
                ';',
                CSVWriter.NO_QUOTE_CHARACTER,
                CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                CSVWriter.DEFAULT_LINE_END
            );
        }
        else{
            csvWriter = new CSVWriter(
                new FileWriter(fileName),
                CSVWriter.DEFAULT_SEPARATOR,
                CSVWriter.NO_QUOTE_CHARACTER,
                CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                CSVWriter.DEFAULT_LINE_END
            );
        }

        Set<Integer> keySet = data.keySet();
        for (Integer key : keySet) {
            csvWriter.writeNext(data.get(key));
        }
        csvWriter.close();
    }

    /**
     * Saves a file into the files bucket
     *
     * @param fileName the name of the file to be saved
     * @param data     the data to be saved in the file
     * @throws IOException saving file error
     */
    public static void writeDownTXTFile(String fileName, Map<String, Object[]> data, Character fieldSeparator) throws IOException {
        if (data == null) return;
        PrintWriter writer = new PrintWriter(new FileWriter(fileName));
        Set<String> keySet = data.keySet();
        for (String key : keySet) {
            if (fieldSeparator != null && fieldSeparator != '\0')
                writer.println(StringUtils.join(data.get(key), fieldSeparator));
            else writer.println(StringUtils.join(data.get(key)));
        }
        writer.close();
    }

    /**
     * Saves a file into the files bucket
     *
     * @param fileName the name of the file to be saved
     * @param data     the data to be saved in the file
     * @throws IOException saving file error
     */
    public static void writeDownTXTFileIntKey(String fileName, Map<Integer, Object[]> data, Character fieldSeparator) throws IOException {
        if (data == null) return;
        PrintWriter writer = new PrintWriter(new FileWriter(fileName));
        Set<Integer> keySet = data.keySet();
        for (Integer key : keySet) {
            if (fieldSeparator != null && fieldSeparator != '\0')
                writer.println(StringUtils.join(data.get(key), fieldSeparator));
            else writer.println(StringUtils.join(data.get(key)));
        }
        writer.close();
    }

    public static void writeDownWorkbook(String fileName, String sheetName, Map<String, Object[]> data) throws IOException {
        writeDownWorkbook(fileName, sheetName, data, null);
    }
    /**
     * Saves a file into the files bucket
     *
     * @param fileName  the name of the file to be saved
     * @param sheetName the name of the sheet to be created
     * @param data      the data to be saved in the file
     * @param doubleNumberFormat format for number double in excel
     * @throws IOException saving file error
     */
    public static void writeDownWorkbook(String fileName, String sheetName, Map<String, Object[]> data, String doubleNumberFormat) throws IOException {
        if (data == null) return;

        //Blank workbook
        XSSFWorkbook workbook = new XSSFWorkbook();

        //Create a blank sheet
        XSSFSheet sheet = workbook.createSheet(sheetName);
        //Iterate over data and write to sheet
        Set<String> keySet = data.keySet();

        CellStyle style = workbook.createCellStyle();
        DataFormat format = workbook.createDataFormat();

        if (doubleNumberFormat == null) {
            doubleNumberFormat = "#,##0.00";
        }

        int rowNumber = 0;
        for (String key : keySet) {
            Row row = sheet.createRow(rowNumber++);
            Object[] objArr = data.get(key);
            int cellNumber = 0;
            for (Object obj : objArr) {
                Cell cell = row.createCell(cellNumber++);
                if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Integer) {
                    cell.setCellValue((Integer) obj);
                } else if (obj instanceof Long) {
                    cell.setCellValue((Long) obj);
                } else if (obj instanceof Double) {
                    cell.setCellValue((Double) obj);
                    style.setDataFormat(format.getFormat(doubleNumberFormat));
                    cell.setCellStyle(style);
                }
            }
        }

        //Write the workbook in file system
        try (FileOutputStream out = new FileOutputStream(new File(fileName))) {
            workbook.write(out);
        } finally {
            workbook.close();
        }
    }


    public static Map<Long, Map<Long, Object>> readFromWorkBook(String fileName) throws IOException {
        try (FileInputStream file = new FileInputStream(new File(fileName))) {

            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Get first/desired sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);

            Map<Long, Map<Long, Object>> data = new TreeMap<>();
            long rowIndex = 0;
            //Iterate through each rows one by one
            for (Row row : sheet) {
                Map<Long, Object> rowValues = new TreeMap<>();
                long cellIndex = 0;
                //For each row, iterate through all the columns
                Iterator<Cell> cellIterator = row.cellIterator();
                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    //Check the cell type and format accordingly
                    switch (cell.getCellTypeEnum()) {
                        case NUMERIC:
                            rowValues.put(cellIndex++, cell.getNumericCellValue());
                            break;
                        case STRING:
                            rowValues.put(cellIndex++, cell.getStringCellValue());
                            break;
                        case _NONE:
                        case ERROR:
                        case BOOLEAN:
                        case BLANK:
                        case FORMULA:
                        default:
                            break;
                    }
                }
                data.put(rowIndex++, rowValues);
            }
            return data;
        }
    }


}

