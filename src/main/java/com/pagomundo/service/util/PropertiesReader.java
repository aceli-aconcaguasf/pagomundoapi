package com.pagomundo.service.util;

import java.io.IOException;

public interface PropertiesReader {

    void initialize() throws IOException;

    String getValue(String property);
}
