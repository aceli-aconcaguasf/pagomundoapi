package com.pagomundo.service.util;

import java.util.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class ABCTokenAuth {

    //private String urlBase = "https://10.99.60.181/WebApiTokenManager/";
    private final Logger log = LoggerFactory.getLogger(ABCTokenAuth.class);
    private String urlBase = "https://desavbancaapp.abccapital.com.mx/WebApiTokenManager/";
    
    public ABCTokenAuth(){

    }

    public String generateCode(int transactionType, String customerNumber, String deviceId,
                                    String idNotificationType, String notifyTo, String tokenNumber,
                                    int idAplicacion, String idCompany){
        
        String path = "GenerateCode";
        String output = "";

        String responseLine = null;
        StringBuilder response = new StringBuilder();
        
        try{

            log.info(" ===> Begining generateCode");

            URL url = new URL(urlBase + path);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json; utf-8");
            conn.setRequestProperty("Accept", "application/json");

            conn.setDoOutput(true);


            String input = "{";

            input = input + "\"transactionType\":" + transactionType + ", ";

            if(customerNumber != null) {
                input = input + "\"customerNumber\": \"" + customerNumber + "\", ";    
            }

            input = input + "\"deviceId\": \"" + deviceId + "\", ";

            input = input + "\"idNotificationType\": \"" + idNotificationType + "\", ";
            input = input + "\"notifyTo\": \"" + notifyTo + "\", ";

            if(tokenNumber != null) {
                input = input + "\"tokenNumber\": \"" + tokenNumber + "\", ";
            }

            if(idAplicacion != 0) {
                input = input + "\"idAplicacion\": " + idAplicacion + ", ";
            }

            input = input + "\"idCompany\": \"" + idCompany + "\"";

            input = input + "}";

            log.info(" ===> GenerateCode Request: {}", input);

            OutputStream os = conn.getOutputStream();
            os.write(input.getBytes());
            os.flush();

            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("Failed : HTTP error code : "
                    + conn.getResponseCode());
            }

            /*BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            log.info("Output from Server .... \n");
            while ((output = output + br.readLine()) != null) {
                log.info(output);
            }*/

            try(BufferedReader br = new BufferedReader(
              new InputStreamReader(conn.getInputStream(), "utf-8"))) {
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                log.info(response.toString());
            }

            conn.disconnect();



        } catch (MalformedURLException e) {

            e.printStackTrace();

        }catch (IOException e) {

            e.printStackTrace();

         }

        return response.toString();

    }


    public String authenticate(String idNotify, String deviceId, String idNotificationType,
                                String notifyTo, String keygen){
        
        String path = "Authenticate";
        String output = "";

        String responseLine = null;
        StringBuilder response = new StringBuilder();
        
        try{
            URL url = new URL(urlBase + path);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json; utf-8");
            conn.setRequestProperty("Accept", "application/json");

            String input = "{";
            input = input + "\"idNotify\": \"" + idNotify + "\", ";
            input = input + "\"deviceId\": \"" + deviceId + "\", ";
            input = input + "\"notifyTo\": \"" + notifyTo + "\", ";
            input = input + "\"idNotificationType\": \"" + idNotificationType + "\", ";
            input = input + "\"keygen\": \"" + keygen + "\"";
            input = input + "}";

            log.info("===> AuthenticateCode Request: {}", input);

            OutputStream os = conn.getOutputStream();
            os.write(input.getBytes());
            os.flush();

            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("Failed : HTTP error code : "
                    + conn.getResponseCode());
            }

            /*BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));
            log.info("Output from Server .... \n");
            while ((output = output + br.readLine()) != null) {
                log.info(output);
            }*/

            try(BufferedReader br = new BufferedReader(
              new InputStreamReader(conn.getInputStream(), "utf-8"))) {
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                log.info(response.toString());
            }

            conn.disconnect();



        } catch (MalformedURLException e) {

            e.printStackTrace();

        }catch (IOException e) {

            e.printStackTrace();

         }

        return response.toString();

    }
}
