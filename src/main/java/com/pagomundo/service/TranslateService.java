package com.pagomundo.service;

import com.pagomundo.domain.User;
import com.pagomundo.repository.UserRepository;
import com.pagomundo.security.SecurityUtils;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Optional;

@Service
@Transactional
public class TranslateService {

    private final UserRepository userRepository;
    private final MessageSource messageSource;

    public TranslateService(UserRepository userRepository, MessageSource messageSource) {
        this.userRepository = userRepository;
        this.messageSource = messageSource;
    }

    /**
     * getMessageByCurrentUser
     *
     * return message by current user
     *
     * @return message
     */
    public String getMessageByCurrentUserParamsNull(String message) {
        Optional<User> user = SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneWithAuthoritiesByLogin);
        Locale locale = user.isPresent() ? Locale.forLanguageTag(user.get().getLangKey()) : Locale.forLanguageTag("en");
        return messageSource.getMessage(message, null, locale);
    }

    public String[] getArrayMessageByUserParamsNull(String[] arrayMessage, User user) {
        String[] messageArrayTranslate = null;
        if (arrayMessage != null) {
            Locale locale = user != null ? Locale.forLanguageTag(user.getLangKey()) : Locale.forLanguageTag("en");
            LinkedHashSet<String> finalMessageTranslateList = new LinkedHashSet<>();
            Arrays.asList(arrayMessage).forEach(message -> {
                finalMessageTranslateList.add(messageSource.getMessage(message, null, locale));
            });
            messageArrayTranslate = new String[finalMessageTranslateList.size()];
            messageArrayTranslate = finalMessageTranslateList.toArray(messageArrayTranslate);
        }
        return messageArrayTranslate;
    }
    public String getMessageByCurrentUserParams(String message, Object[] args) {
        Optional<User> user = SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneWithAuthoritiesByLogin);
        Locale locale = user.isPresent() ? Locale.forLanguageTag(user.get().getLangKey()) : Locale.forLanguageTag("en");
        return messageSource.getMessage(message, args, locale);
    }
}
