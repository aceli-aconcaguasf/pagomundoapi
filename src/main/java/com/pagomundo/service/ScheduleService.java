package com.pagomundo.service;

import com.cronutils.descriptor.CronDescriptor;
import com.cronutils.model.definition.CronDefinitionBuilder;
import com.cronutils.parser.CronParser;
import com.google.gson.JsonObject;
import com.pagomundo.domain.Transaction;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.service.dto.integrations.IntegrationAuditDTO;
import com.pagomundo.service.integrations.IntegrationAuditService;
import com.pagomundo.service.integrations.stp.IntegrationStpService;
import com.pagomundo.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import static com.cronutils.model.CronType.QUARTZ;

@Service
public class ScheduleService {
    private static final String CHANGE_TRANSACTION_STATUS_CRON_EXPRESSION = "*/11 * * * * ?";
    private static final String CHANGE_TRANSACTION_STATUS_NO_RESPONSE_CRON_EXPRESSION = "*/19 * * * * ?";
    private static final String CHANGE_TRANSACTION_STATUS_CRON_DESCRIPTION = CronDescriptor.instance(Locale.UK).describe(new CronParser(CronDefinitionBuilder.instanceDefinitionFor(QUARTZ)).parse(CHANGE_TRANSACTION_STATUS_CRON_EXPRESSION));
    private static final String CHANGE_TRANSACTION_STATUS_NO_RESPONSE_CRON_DESCRIPTION = CronDescriptor.instance(Locale.UK).describe(new CronParser(CronDefinitionBuilder.instanceDefinitionFor(QUARTZ)).parse(CHANGE_TRANSACTION_STATUS_NO_RESPONSE_CRON_EXPRESSION));

    private final Logger log = LoggerFactory.getLogger(ScheduleService.class);
    private final TransactionService transactionService;
    private final ExtendedUserService extendedUserService;
    private final NotificationReceiverService notificationReceiverService;
    private final IntegrationStpService integrationStpService;
    private final IntegrationAuditService integrationAuditService;


    public ScheduleService(TransactionService transactionService, ExtendedUserService extendedUserService, NotificationReceiverService notificationReceiverService, IntegrationStpService integrationStpService, IntegrationAuditService integrationAuditService) {
        this.transactionService = transactionService;
        this.extendedUserService = extendedUserService;
        this.notificationReceiverService = notificationReceiverService;
        this.integrationStpService = integrationStpService;
        this.integrationAuditService = integrationAuditService;
    }

    @Scheduled(cron = CHANGE_TRANSACTION_STATUS_NO_RESPONSE_CRON_EXPRESSION)
    public void changeTransactionNoResponseStatus() {
        Date startDate = Calendar.getInstance().getTime();

        Map<IntegrationAuditDTO, String> list = integrationStpService.getLatestIntegrationAuditWithoutResponses();
        list.forEach((integrationAuditDTO, statusReason) -> {
            updateTransaction(integrationAuditDTO.getEntityId(), Status.FAILED, statusReason);
            integrationAuditService.updateIntegrationAudit(integrationAuditDTO, Status.FAILED, statusReason);
        });
        if (list.size() > 0)
            log.info("changeTransactionNoResponseStatus {} - Elapsed time: {} ms - Processed: {}",
                CHANGE_TRANSACTION_STATUS_NO_RESPONSE_CRON_DESCRIPTION,
                Calendar.getInstance().getTime().getTime() - startDate.getTime(), list.size());
    }

    @Scheduled(cron = CHANGE_TRANSACTION_STATUS_CRON_EXPRESSION)
    public void changeTransactionStatus() {
        Date startDate = Calendar.getInstance().getTime();
        Map<IntegrationAuditDTO, JsonObject> list = integrationStpService.getLatestIntegrationAuditExtraResponses();
        list.forEach((integrationAuditDTO, json) -> {
            String statusSTP = json.get("estado").getAsString();
            String statusReason = json.get("causaDevolucion").getAsString();
            Status status;
            if ("ACCEPTED".equalsIgnoreCase(statusSTP)) {
                status = Status.ACCEPTED;
                updateTransaction(integrationAuditDTO.getEntityId(), status, null);
            } else status = Status.REJECTED;
            integrationAuditService.updateIntegrationAudit(integrationAuditDTO, status, statusReason);
        });
        if (list.size() > 0)
            log.info("changeTransactionStatus {} - Elapsed time: {} ms - Processed: {}",
                CHANGE_TRANSACTION_STATUS_CRON_DESCRIPTION,
                Calendar.getInstance().getTime().getTime() - startDate.getTime(), list.size());
    }

    private void updateTransaction(Long id, Status status, String statusReason) {
        Transaction transaction = new Transaction();
        transaction.setId(id);
        transaction.setStatus(status);
        transaction.setStatusReason(statusReason);
        Transaction result;
        try {
            result = transactionService.update(transaction);
            log.info("-->> Updated transaction ID: {}, status '{}', statusReason: '{}'", result.getId(), result.getStatus(), result.getStatusReason());

            if (result.getReseller() != null) {
                extendedUserService.refetch(result.getReseller().getId());
            }
            notificationReceiverService.sendTransactionNotification(result, result.getAdmin());
        } catch (BadRequestAlertException e) {
            log.error("!!! -> !!! Transaction NOT updated");
            log.error("!!! -> !!! {}", e.getLocalizedMessage());
        }
    }
}
