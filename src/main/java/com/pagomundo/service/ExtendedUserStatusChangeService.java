package com.pagomundo.service;

import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.ExtendedUserStatusChange;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.repository.ExtendedUserRepository;
import com.pagomundo.repository.ExtendedUserStatusChangeRepository;
import com.pagomundo.repository.UserRepository;
import com.pagomundo.repository.search.ExtendedUserStatusChangeSearchRepository;
import com.pagomundo.security.SecurityUtils;
import com.pagomundo.service.dto.UserDTO;
import com.pagomundo.web.rest.errors.UserNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link ExtendedUserStatusChange}.
 */
@Service
@Transactional
public class ExtendedUserStatusChangeService {

    private final Logger log = LoggerFactory.getLogger(ExtendedUserStatusChangeService.class);

    private final ExtendedUserStatusChangeRepository extendedUserStatusChangeRepository;

    private final ExtendedUserStatusChangeSearchRepository extendedUserStatusChangeSearchRepository;
    private final UserRepository userRepository;
    private final ExtendedUserRepository extendedUserRepository;
    private final TranslateService translateService;

    public ExtendedUserStatusChangeService(ExtendedUserStatusChangeRepository extendedUserStatusChangeRepository, ExtendedUserStatusChangeSearchRepository extendedUserStatusChangeSearchRepository, UserRepository userRepository, ExtendedUserRepository extendedUserRepository, TranslateService translateService) {
        this.extendedUserStatusChangeRepository = extendedUserStatusChangeRepository;
        this.extendedUserStatusChangeSearchRepository = extendedUserStatusChangeSearchRepository;
        this.userRepository = userRepository;
        this.extendedUserRepository = extendedUserRepository;
        this.translateService = translateService;
    }

    /**
     * Save a extendedUserStatusChange.
     *
     * @param extendedUserStatusChange the entity to save.
     * @return the persisted entity.
     */
    public ExtendedUserStatusChange save(ExtendedUserStatusChange extendedUserStatusChange) {
        log.debug("Request to save ExtendedUserStatusChange : {}", extendedUserStatusChange);
        ExtendedUserStatusChange result = extendedUserStatusChangeRepository.save(extendedUserStatusChange);
        extendedUserStatusChangeSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the extendedUserStatusChanges.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ExtendedUserStatusChange> findAll(Pageable pageable) {
        log.debug("Request to get all ExtendedUserStatusChanges");
        return extendedUserStatusChangeRepository.findAll(pageable);
    }


    /**
     * Get one extendedUserStatusChange by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ExtendedUserStatusChange> findOne(Long id) {
        log.debug("Request to get ExtendedUserStatusChange : {}", id);
        if (id == null) return Optional.empty();
        return extendedUserStatusChangeRepository.findById(id);
    }

    /**
     * Delete the extendedUserStatusChange by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ExtendedUserStatusChange : {}", id);
        extendedUserStatusChangeRepository.deleteById(id);
        extendedUserStatusChangeSearchRepository.deleteById(id);
    }

    /**
     * Search for the extendedUserStatusChange corresponding to the query.
     *
     * @param query    the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ExtendedUserStatusChange> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ExtendedUserStatusChanges for query {}", query);
        return extendedUserStatusChangeSearchRepository.search(queryStringQuery(query), pageable);
    }

    @Transactional
    public void saveAll(List<ExtendedUserStatusChange> extendedUserStatusChangeList) {
        if (!extendedUserStatusChangeList.isEmpty())
            extendedUserStatusChangeSearchRepository.saveAll(extendedUserStatusChangeList);
    }

    /**
     * Create a new transactionStatusChange.
     *
     * @param eu the entity about to create a transactionStatusChange.
     */
    @Transactional
    public void createStatusChange(ExtendedUser eu) {
        UserDTO userDTO = SecurityUtils.getCurrentUserLogin()
            .flatMap(userRepository::findOneWithAuthoritiesByLogin)
            .map(UserDTO::new)
            .orElseThrow(() -> new UserNotFoundException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.account.extended.user.not.found")));
        ExtendedUser currentExtendedUser = extendedUserRepository.getByUserId(userDTO.getId());

        createStatusChange(eu.getStatus(), eu.getStatusReason(), currentExtendedUser, eu);
    }

    /**
     * Create a new extendedUserStatusChange.
     *
     * @param status              the current status of the extendedUser.
     * @param reason              the reason of the current status of the extendedUser.
     * @param currentExtendedUser the user who change the extendedUser's status.
     * @param extendedUser        the extendedUser which changed the status.
     */
    @Transactional
    public void createStatusChange(Status status, String reason, ExtendedUser currentExtendedUser, ExtendedUser extendedUser) {
        log.debug("Creating a new extendedUserStatusChange for extendedUser: {}", extendedUser);
        ExtendedUserStatusChange eusc = new ExtendedUserStatusChange();
        eusc.setExtendedUser(extendedUser);
        eusc.setDateCreated(extendedUser.getLastUpdated());
        eusc.setUser(currentExtendedUser);
        eusc.setStatus(status);
        eusc.setReason(reason);
        ExtendedUserStatusChange result = extendedUserStatusChangeRepository.save(eusc);
        extendedUserStatusChangeSearchRepository.save(result);
        log.debug("ExtendedUserStatusChange saved: {}", result);
    }

    @Async
    public void refetchAllTransactionStatusChange(Collection<ExtendedUserStatusChange> transactionStatusChanges) {
        if (!transactionStatusChanges.isEmpty())
            extendedUserStatusChangeSearchRepository.findAllByIdIn(transactionStatusChanges.stream().map(ExtendedUserStatusChange::getId).collect(Collectors.toList()))
                .ifPresent(extendedUserStatusChangeSearchRepository::saveAll);
    }
}
