package com.pagomundo.service;

import com.cronutils.descriptor.CronDescriptor;
import com.cronutils.model.definition.CronDefinitionBuilder;
import com.cronutils.parser.CronParser;
import com.pagomundo.domain.Authority;
import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.ExtendedUserRelation;
import com.pagomundo.domain.User;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.repository.ExtendedUserRelationRepository;
import com.pagomundo.repository.ExtendedUserRepository;
import com.pagomundo.repository.search.ExtendedUserRelationSearchRepository;
import com.pagomundo.repository.search.ExtendedUserSearchRepository;
import com.pagomundo.repository.search.RefetchService;
import com.pagomundo.security.AuthoritiesConstants;
import com.pagomundo.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import static com.cronutils.model.CronType.QUARTZ;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link ExtendedUserRelation}.
 */
@Service
@Transactional
public class ExtendedUserRelationService {
    public static final String ENTITY_NAME = "extendedUserRelationService";
    public static final String NOT_FOUND = ENTITY_NAME + ".001";
    public static final String SHOULD_NOT_BE_NULL_CODE = ENTITY_NAME + ".003";
    public static final String SHOULD_BE_NULL_CODE = ENTITY_NAME + ".004";
    private static final String DELETE_NULL_EXTENDED_USER_RELATIONS_CRON_EXPRESSION = "0 0 1 * * ?";
    private static final String DELETE_NULL_EXTENDED_USER_RELATIONS_CRON_DESCRIPTION = CronDescriptor.instance(Locale.UK).describe(new CronParser(CronDefinitionBuilder.instanceDefinitionFor(QUARTZ)).parse(DELETE_NULL_EXTENDED_USER_RELATIONS_CRON_EXPRESSION));

    private final Logger log = LoggerFactory.getLogger(ExtendedUserRelationService.class);
    private final ExtendedUserRelationRepository extendedUserRelationRepository;
    private final ExtendedUserRepository extendedUserRepository;
    private final ExtendedUserSearchRepository extendedUserSearchRepository;
    private final ExtendedUserRelationSearchRepository extendedUserRelationSearchRepository;
    private final RefetchService refetchService;
    private final TranslateService translateService;

    public ExtendedUserRelationService(
        ExtendedUserRelationRepository extendedUserRelationRepository,
        ExtendedUserRepository extendedUserRepository,
        ExtendedUserSearchRepository extendedUserSearchRepository, ExtendedUserRelationSearchRepository extendedUserRelationSearchRepository,
        RefetchService refetchService, TranslateService translateService) {
        this.extendedUserRelationRepository = extendedUserRelationRepository;
        this.extendedUserRepository = extendedUserRepository;
        this.extendedUserSearchRepository = extendedUserSearchRepository;
        this.extendedUserRelationSearchRepository = extendedUserRelationSearchRepository;
        this.refetchService = refetchService;
        this.translateService = translateService;
    }

    /**
     * Save a extendedUserRelation.
     *
     * @param extendedUserRelation the entity to save.
     * @return the persisted entity.
     */
    public ExtendedUserRelation save(ExtendedUserRelation extendedUserRelation) {
        log.debug("Request to save ExtendedUserRelation : {}", extendedUserRelation);
        ExtendedUserRelation result = extendedUserRelationRepository.save(extendedUserRelation);
        extendedUserRelationSearchRepository.save(result);
        return result;
    }

    /**
     * Save a extendedUser.
     *
     * @param extendedUser the entity to save.
     * @return the persisted entity.
     */
    private ExtendedUser saveExtendedUser(ExtendedUser extendedUser) {
        extendedUser.setLastUpdated(ZonedDateTime.now());
        ExtendedUser result = extendedUserRepository.save(extendedUser);
        extendedUserSearchRepository.save(result);
        return result;
    }

    /**
     * Update a extendedUserRelation.
     *
     * @param eur the extendedUserRelation to be updated.
     * @return the persisted entity.
     */
    public ExtendedUserRelation update(ExtendedUserRelation eur, ExtendedUser currentExtendedUser) {
        log.debug("Request to update  ExtendedUserRelation.");

        ExtendedUserRelation ceur = eur.getId() != null ?
            findOne(eur.getId()).orElseGet(() ->
                new ExtendedUserRelation(eur.getUserRelated(), eur.getExtendedUser(), eur.getNickname())) :
            findByByUserRelatedAndExtendedUser(eur.getExtendedUser(), eur.getUserRelated()).orElseGet(() ->
                new ExtendedUserRelation(eur.getUserRelated(), eur.getExtendedUser(), eur.getNickname()));

        if (isNicknameAlreadyInUse(ceur, eur.getNickname()))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.payee.nickname.already.used"),
                UserService.ENTITY_NAME, UserService.BAD_REQUEST);

        if (eur.getNickname() != null) ceur.setNickname(eur.getNickname());
        ceur.setStatus(eur.getStatus() != null ? eur.getStatus() : Status.ACCEPTED);
        ceur.setStatusReason(eur.getStatusReason());

        ExtendedUserRelation extendedUserRelation;
        switch (ceur.getStatus()) {
            case ACCEPTED:
                ceur.setRelatedBy(eur.getRelatedBy() != null ? eur.getRelatedBy() : currentExtendedUser);
                ceur.setDateRelated(eur.getDateRelated() != null ? eur.getDateRelated() : ZonedDateTime.now());
                extendedUserRelation = save(ceur);
                getInvertedRelation(extendedUserRelation);
                break;
            case CANCELLED:
                ceur.setUnrelatedBy(eur.getUnrelatedBy() != null ? eur.getUnrelatedBy() : currentExtendedUser);
                ceur.setDateUnrelated(ZonedDateTime.now());
                extendedUserRelation = save(ceur);
                break;
            default:
                extendedUserRelation = save(ceur);
        }
        changeInvertedRelation(extendedUserRelation);
        updateMerchantWithReseller(extendedUserRelation);
        updateLogoIfNecessary(extendedUserRelation);
        return extendedUserRelation;
    }

    private void updateMerchantWithReseller(ExtendedUserRelation eur) {
        ExtendedUser eu = eur.getExtendedUser();
        User u = eur.getUserRelated();
        if (eu == null || u == null) return;

        ExtendedUser euRelated = extendedUserRepository.getByUser(u);
        if (euRelated == null) return;

        if (eu.getRole().equals(AuthoritiesConstants.MERCHANT) && euRelated.getRole().equals(AuthoritiesConstants.RESELLER)) {
            eu.setReseller(eur.getStatus().equals(Status.ACCEPTED) ? euRelated : null);
            ExtendedUser result = saveExtendedUser(eu);
            refetchService.refetchExtendedUser(result.getId());
        }
    }

    private void updateLogoIfNecessary(ExtendedUserRelation eur) {
        ExtendedUser euDestination = extendedUserRepository.getByUser(eur.getUserRelated());
        if (euDestination == null) return;
        ExtendedUser eu = eur.getExtendedUser();
        ExtendedUser reseller = getReseller(eu);
        updateLogo(eu, reseller != null ? reseller.getImageAddressUrl() : "");
    }

    private ExtendedUser getReseller(ExtendedUser eu) {
        switch (eu.getRole()) {
            case AuthoritiesConstants.RESELLER:
                return eu;
            case AuthoritiesConstants.MERCHANT:
                return eu.getReseller();
            case AuthoritiesConstants.PAYEE:
                List<ExtendedUser> merchantList = findAllByUserRelatedAndExtendedUserRole(eu.getUser(), AuthoritiesConstants.MERCHANT);
                if (merchantList.size() == 1) return merchantList.get(0).getReseller();
                else return null;
            default:
                return null;
        }
    }

    ExtendedUser updateLogo(ExtendedUser eu, String pathToImage) {
        if (pathToImage == null || eu == null) return eu;
        switch (eu.getRole()) {
            case AuthoritiesConstants.RESELLER:
                if (!pathToImage.equalsIgnoreCase(eu.getImageAddressUrl())) {
                    eu.setImageAddressUrl(pathToImage);
                    eu = saveExtendedUser(eu);
                }
                findAllByUserRelatedAndExtendedUserRole(eu.getUser(), AuthoritiesConstants.MERCHANT)
                    .forEach(extendedUser -> updateLogo(extendedUser, pathToImage));
                break;
            case AuthoritiesConstants.MERCHANT:
                if (!pathToImage.equalsIgnoreCase(eu.getImageAddressUrl())) {
                    eu.setImageAddressUrl(pathToImage);
                    eu = saveExtendedUser(eu);
                }
                findAllByUserRelatedAndExtendedUserRole(eu.getUser(), AuthoritiesConstants.PAYEE)
                    .forEach(extendedUser -> updateLogo(extendedUser, pathToImage));
                break;
            case AuthoritiesConstants.PAYEE:
                int howManyMerchants = findAllByUserRelatedAndExtendedUserRole(eu.getUser(), AuthoritiesConstants.MERCHANT).size();
                eu.setImageAddressUrl(howManyMerchants == 1 ? pathToImage : null);
                eu = saveExtendedUser(eu);
                break;
            default:
                break;
        }
        return eu;
    }

    public boolean isNicknameAlreadyInUse(ExtendedUserRelation eur, String nickname) {
        if (eur.getUserRelated() == null || eur.getUserRelated().getId() == null || nickname == null || "".equalsIgnoreCase(nickname))
            return false;

        return (eur.getId() != null)
            ? !findAllAcceptedByUserRelatedIdAndNicknameAndIdNot(eur.getUserRelated().getId(), nickname, eur.getId()).isEmpty()
            : !findAllAcceptedByUserRelatedIdAndNickname(eur.getUserRelated().getId(), nickname).isEmpty();
    }

    /**
     * Create a extendedUserRelation.
     *
     * @param user         the user to be related to an extendedUser.
     * @param extendedUser the extendedUser to be related to a user.
     * @param relatedBy    the extendedUser who make the relation.
     * @return the persisted entity.
     */
    public ExtendedUserRelation link(User user, ExtendedUser extendedUser, ExtendedUser relatedBy, Status status, String nickname) {
        return extendedUserRelationRepository.findOneByUserRelatedAndExtendedUser(user, extendedUser).orElseGet(() ->
            update(new ExtendedUserRelation(user, extendedUser, status, nickname), relatedBy));
    }

    /**
     * get the inverted relation of a extendedUserRelation.
     *
     * @param eur the extendedUserRelation to get the inverted one.
     * @return the persisted entity.
     */
    ExtendedUserRelation getInvertedRelation(ExtendedUserRelation eur) {
        if (eur == null) return null;

        ExtendedUserRelation ceur = findOne(eur.getId()).orElse(null);
        if (ceur == null) return null;

        ExtendedUser extendedUser = extendedUserRepository.getByUser(ceur.getUserRelated());
        if (extendedUser == null) return null;

        User user = ceur.getExtendedUser().getUser();
        return extendedUserRelationRepository.findOneByUserRelatedAndExtendedUser(user, extendedUser).orElseGet(() ->
            link(user, extendedUser, ceur.getRelatedBy(), ceur.getStatus(), ceur.getNickname()));
    }

    /**
     * get the inverted relation of a extendedUserRelation.
     *
     * @param eur the extendedUserRelation to get the inverted one.
     * @return the persisted entity.
     */
    private ExtendedUserRelation changeInvertedRelation(ExtendedUserRelation eur) {
        if (eur == null) return null;

        ExtendedUserRelation ceur = findOne(eur.getId()).orElse(null);
        if (ceur == null) return null;

        ExtendedUserRelation invertedRelation = getInvertedRelation(ceur);
        if (invertedRelation == null || invertedRelation.getStatus() == null) return null;

        if (invertedRelation.getStatus().equals(eur.getStatus())) return invertedRelation;

        invertedRelation.setStatus(eur.getStatus());
        invertedRelation.setStatusReason(ceur.getStatusReason());
        return update(invertedRelation, eur.getUnrelatedBy());
    }

    /**
     * Get all the extendedUserRelations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ExtendedUserRelation> findAll(Pageable pageable) {
        log.debug("Request to get all ExtendedUserRelations");
        return extendedUserRelationRepository.findAll(pageable);
    }

    /**
     * Get one extendedUserRelation by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ExtendedUserRelation> findOne(Long id) {
        log.debug("Request to get ExtendedUserRelation : {}", id);
        if (id == null) return Optional.empty();
        return extendedUserRelationRepository.findById(id);
    }

    /**
     * Delete the extendedUserRelation by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ExtendedUserRelation : {}", id);
        extendedUserRelationRepository.deleteById(id);
        extendedUserRelationSearchRepository.deleteById(id);
    }

    /**
     * Delete the extendedUserRelation.
     *
     * @param eur the entity.
     */
    public void delete(ExtendedUserRelation eur) {
        delete(eur.getId());
    }

    /**
     * Search for the extendedUserRelation corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ExtendedUserRelation> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ExtendedUserRelations for query {}", query);
        return extendedUserRelationSearchRepository.search(queryStringQuery(query), pageable);
    }

    @Transactional
    public void saveAll(List<ExtendedUserRelation> extendedUserRelationList) {
        if (!extendedUserRelationList.isEmpty())
            extendedUserRelationSearchRepository.saveAll(extendedUserRelationList);
    }

    @Async
    public void refetchAllByExtendedUserId(Long id) {
        extendedUserRelationRepository.findAllByExtendedUserId(id).ifPresent(extendedUserRelationSearchRepository::saveAll);
    }

    @Async
    @Scheduled(cron = DELETE_NULL_EXTENDED_USER_RELATIONS_CRON_EXPRESSION)
    public void cronDeleteNullExtendedUserRelations() {
        Date startDate = Calendar.getInstance().getTime();
        int maxToProcess = 100;
        int size = deleteNullExtendedUserRelations(maxToProcess);
        Date endDate = Calendar.getInstance().getTime();
        if (size > 0)
            log.info("deleteNullExtendedUserRelations {} - Elapsed time: {} ms - Processed: {} of {}",
                DELETE_NULL_EXTENDED_USER_RELATIONS_CRON_DESCRIPTION,
                endDate.getTime() - startDate.getTime(),
                size,
                maxToProcess);
    }

    private int deleteNullExtendedUserRelations(Integer maxToProcess) {
        if (maxToProcess == null || maxToProcess == 0 || maxToProcess > 100) maxToProcess = 100;
        Pageable pageable = PageRequest.of(0, maxToProcess, new Sort(Sort.Direction.ASC, "id"));
        List<ExtendedUserRelation> list =
            extendedUserRelationRepository.findAllByUserRelatedNullOrExtendedUserNull(pageable).getContent();
        list.forEach(this::delete);
        return list.size();
    }

    public int deleteNullExtendedUserRelations() {
        return deleteNullExtendedUserRelations(null);
    }

    String getPayeeNickname(Long relatedUserId, Long extendedUserId) {
        
        Optional<ExtendedUserRelation> eurOptional = findAcceptedByUserIdAndExtendedUserId(relatedUserId, extendedUserId);
        
        log.debug("eurOptional: {}", eurOptional);

        if (eurOptional.isEmpty()) return "";

        log.debug("getNickname: {}", eurOptional.get().getNickname());

        return eurOptional.get().getNickname();
    }

    List<ExtendedUserRelation> findAllAcceptedByUserRelatedIdAndNickname(Long relatedUserId, String nickname) {
        return extendedUserRelationRepository.findAllByUserRelatedIdAndNicknameAndStatus(relatedUserId, nickname, Status.ACCEPTED);
    }

    List<ExtendedUserRelation> findAllAcceptedByUserRelatedIdAndEmail(Long relatedUserId, String email) {
        return extendedUserRelationRepository.findAllByUserRelatedIdAndExtendedUser_EmailAndStatus(relatedUserId, email, Status.ACCEPTED);
    }

    private List<ExtendedUserRelation> findAllAcceptedByUserRelatedIdAndNicknameAndIdNot(Long relatedUserId, String nickname, Long id) {
        return extendedUserRelationRepository.findAllByUserRelatedIdAndNicknameAndStatusAndIdNot(relatedUserId, nickname, Status.ACCEPTED, id);
    }

    List<ExtendedUserRelation> findAllAcceptedByUserRelatedIdAndNickname(Long relatedUserId, String nickname, Long id) {
        return extendedUserRelationRepository.findAllByUserRelatedIdAndNicknameAndStatusAndIdNot(relatedUserId, nickname, Status.ACCEPTED, id);
    }

    public Optional<ExtendedUserRelation> findAcceptedByUserIdAndExtendedUserId(Long relatedUserId, Long extendedUserId) {
        return extendedUserRelationRepository.findOneByUserRelatedIdAndExtendedUserIdAndStatus(relatedUserId, extendedUserId, Status.ACCEPTED);
    }

    public Optional<ExtendedUserRelation> findAcceptedByUserIdAndExtendedUserEmail(Long relatedUserId, String email) {
        return extendedUserRelationRepository.findOneByUserRelatedIdAndExtendedUser_EmailAndStatus(relatedUserId, email.trim(), Status.ACCEPTED);
    }

    public Optional<ExtendedUserRelation> findAcceptedPayeeByRelatedByIdAndUserEmail(Long relatedById, String email) {
        return extendedUserRelationRepository.findOneByStatusAndRelatedByIdAndUserRelated_RoleAndUserRelated_Email(
            Status.ACCEPTED, relatedById, AuthoritiesConstants.PAYEE, email);
    }

    private Optional<ExtendedUserRelation> findByByUserRelatedAndExtendedUser(ExtendedUser extendedUser, User userRelated) {
        return extendedUserRelationRepository.findOneByUserRelatedAndExtendedUser(userRelated, extendedUser);
    }

    public List<ExtendedUserRelation> findAllByUserRelated(User user) {
        return extendedUserRelationRepository.findAllByUserRelated(user);
    }

    private List<ExtendedUser> findAllByUserRelatedAndExtendedUserRole(User user, String role) {
        List<ExtendedUser> extendedUserList = new ArrayList<>();
        extendedUserRelationRepository.findAllByStatusAndUserRelatedAndExtendedUser_Role(Status.ACCEPTED, user, role)
            .forEach(eur -> extendedUserList.add(eur.getExtendedUser()));
        return extendedUserList;
    }

    public List<ExtendedUser> getPayeesRelatedToReseller(User userReseller) {
        List<ExtendedUser> euPayeeList = new ArrayList<>();
        List<ExtendedUser> euMerchantList = getExtendedUserRelatedToUser(userReseller, AuthoritiesConstants.MERCHANT);
        euMerchantList.forEach(extendedUser -> euPayeeList.addAll(getExtendedUserRelatedToUser(extendedUser.getUser(), AuthoritiesConstants.PAYEE)));
        return euPayeeList;
    }

    private List<ExtendedUser> getExtendedUserRelatedToUser(User user, String euRole) {
        List<ExtendedUser> euList = new ArrayList<>();
        if (user == null) return euList;

        List<ExtendedUserRelation> eurList = extendedUserRelationRepository.findAllByUserRelated(user);
        eurList.forEach(extendedUserRelation -> {
            if (euRole == null && extendedUserRelation.getExtendedUser() != null) {
                euList.add(extendedUserRelation.getExtendedUser());
            } else {
                if (extendedUserRelation.getExtendedUser() != null &&
                    extendedUserRelation.getExtendedUser().getRole() != null &&
                    extendedUserRelation.getExtendedUser().getRole().equals(euRole))
                    euList.add(extendedUserRelation.getExtendedUser());
            }
        });
        return euList;
    }

    public List<ExtendedUser> getExtendedUsersRelated(String euRoleFrom, String euRoleTo, Status status) {
        List<ExtendedUser> extendedUsersRelated = new ArrayList<>();
        extendedUserRelationRepository.findAllByExtendedUser_RoleAndStatus(euRoleFrom, status).forEach(extendedUserRelation -> {
            if (!extendedUsersRelated.contains(extendedUserRelation.getExtendedUser()) &&
                extendedUserRelation.getUserRelated().getAuthorities().contains(Authority.authoritiesFromStrings(euRoleTo)))
                extendedUsersRelated.add(extendedUserRelation.getExtendedUser());
        });
        return extendedUsersRelated;
    }

    /**
     * Search transaction by query
     *
     * @param query parameter by filter
     * @return iterable transaction
     */
    @Transactional(readOnly = true)
    public Iterable<ExtendedUserRelation> search(String query) {
        log.debug("Request to search for a page of ExtendedUserRelations for query {}", query);
        return extendedUserRelationSearchRepository.search(queryStringQuery(query));
    }

    /**
     * Get one extendedUserRelation by id or nickname for payments.
     *
     * @param payee the payee with id or email or nickname.
     * @param merchant the merchant with id.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ExtendedUserRelation> findRelation(ExtendedUser payee, ExtendedUser merchant) {
        
        log.debug("Request to get ExtendedUserRelation : payee: {} merchant: {}", payee, merchant);

        if (payee.getId() != null)
            return findAcceptedByUserIdAndExtendedUserId(merchant.getUser().getId(), payee.getId());

        if (payee.getEmail() != null && !"".equalsIgnoreCase(payee.getEmail())) {

            List<ExtendedUserRelation> extendedUserRelationList =
                findAllAcceptedByUserRelatedIdAndEmail(merchant.getUser().getId(), payee.getEmail());
            
            return extendedUserRelationList.size() == 1
                ? Optional.of(extendedUserRelationList.get(0))
                : Optional.empty();

        }

        if (payee.getNickname() != null && !"".equalsIgnoreCase(payee.getNickname())) {

            List<ExtendedUserRelation> extendedUserRelationList =
                findAllAcceptedByUserRelatedIdAndNickname(merchant.getUser().getId(), payee.getNickname());
            return extendedUserRelationList.size() == 1
                ? Optional.of(extendedUserRelationList.get(0))
                : Optional.empty();

        }


        return Optional.empty();
    }


}
