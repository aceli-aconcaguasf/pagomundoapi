package com.pagomundo.service;

import com.pagomundo.domain.Transaction;
import com.pagomundo.domain.User;
import io.github.jhipster.config.JHipsterProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
// import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Locale;

import com.sendgrid.*;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;

/**
 * Service for sending emails.
 * <p>
 * We use the {@link Async} annotation to send emails asynchronously.
 */
@Service
public class MailService {
    private static final String USER = "user";
    private static final String TRANSACTION = "transaction";
    private static final String BASE_URL = "baseUrl";
    private static final String WEB_PAGE = "webPage";
    private static final String SUPPORT_EMAIL_ADDRESS = "supportEmailAddress";
    private static final String APPLICATION_TITLE = "applicationTitle";
    private static final String FINAL_GREETING = "finalGreeting";
    private static final String NAME_PAYMENT_COMPLETED = "paymentCompletedEmailToUser";

    private final Logger log = LoggerFactory.getLogger(MailService.class);
    private final JHipsterProperties jHipsterProperties;
    private final JavaMailSender javaMailSender;
    private final MessageSource messageSource;
    private final SpringTemplateEngine templateEngine;

    @Value("${environment}")
    private String environment;

    @Value("${pmi-americas-url}")
    private String pmiAmericasUrl;

    @Value("${pmi-support-email-address}")
    private String pmiSupportEmailAddress;

    @Value("${pmi-support-email-name}")
    private String pmiSupportEmailName;

    @Value("${sendgrid-key}")
    private String sendgridKey;

    @Value("${pagomundo-url}")
    private String pagoMundoUrl;

    @Value("${pagomundo-email-address}")
    private String pagoMundoEmailAddress;

    @Value("${pagomundo-email-name}")
    private String pagoMundoEmailName;

    @Value("${base-url}")
    private String baseURL;

    @Value("${pagomundo-base-url}")
    private String pagoMundoBaseURL;

    @Value("${pmi-email-final-greeting}")
    private String pmiFinalGreeting;
    
    @Value("${pagomundo-email-final-greeting}")
    private String pagoMundoFinalGreeting;
    
    @Value("${pmi-payment-completed-email-address}")
    private String pmiPaymentCompletedEmailAddress;

    @Value("${pmi-payment-completed-email-name}")
    private String pmiPaymentCompletedEmailName;

    public MailService(JHipsterProperties jHipsterProperties, JavaMailSender javaMailSender, MessageSource messageSource, SpringTemplateEngine templateEngine) {
        this.jHipsterProperties = jHipsterProperties;
        this.javaMailSender = javaMailSender;
        this.messageSource = messageSource;
        this.templateEngine = templateEngine;
    }



    void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml) throws MessagingException, IOException {

    	log.debug("************ DENTRO sendEmail");
        log.debug("Environment {}", environment);
        log.debug("pmiAmericasUrl {}", pmiAmericasUrl);
        log.debug("pmiSupportEmailAddress {}", pmiSupportEmailAddress);
        log.debug("pmiSupportEmailName {}", pmiSupportEmailName);
        log.debug("sendgridKey {}", sendgridKey);
        log.debug("pagoMundoEmailAddress {}", pagoMundoEmailAddress);
        log.debug("pagoMundoEmailName {}", pagoMundoEmailName);

        Email from_sg = new Email(pmiSupportEmailAddress, pmiSupportEmailName);

    	if(subject.contains("PagoMundo")){
        	// Se cambia correo a pagomundo
        	from_sg = new Email(pagoMundoEmailAddress, pagoMundoEmailName);
    	}

    	String subject_sg = (environment==null||!environment.equalsIgnoreCase("prod") ? "[" + environment + "] " : "") + subject;

    	Email to_sg = new Email(to);
    	Content content_sg = new Content("text/html", content);
    	Mail mail_sg = new Mail(from_sg, subject_sg, to_sg, content_sg);

    	SendGrid sg = new SendGrid(sendgridKey);

    	Request request = new Request();

    	try {

          request.setMethod(Method.POST);
          request.setEndpoint("mail/send");
          request.setBody(mail_sg.build());
          Response response = sg.api(request);

          log.debug("getStatusCode {}", response.getStatusCode());
          log.debug("getBody {}", response.getBody());
          log.debug("getHeaders {}", response.getHeaders());

          log.debug("Sent email to User '{}'", to);

        } catch (IOException ex) {
        	log.debug("Error sending EMAIL '{}'", to);
          throw ex;
        }

    	/*
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, StandardCharsets.UTF_8.name());
        message.setTo(to);
        InternetAddress ia = new InternetAddress();
        ia.setAddress(pmiSupportEmailAddress);
        try {

            if(subject.contains("PagoMundo")){
                ia.setPersonal("PagoMundo");
            } else {
                ia.setPersonal(pmiSupportEmailName);
            }
        } catch (UnsupportedEncodingException e) {
            log.warn("Something went wrong trying to set InternetAddress personal value: {}", pmiSupportEmailName);
        }
        message.setFrom(ia);
        message.setSubject((environment==null||!environment.equalsIgnoreCase("prod") ? "[" + environment + "] " : "") + subject);
        message.setText(content, isHtml);
        javaMailSender.send(mimeMessage);
*/
    }


    public void sendEmailFromTemplate(User user, Transaction transaction, String templateName, String titleKey) throws MessagingException, IOException {
        log.info("*************** sendEmailFromTemplate");
        log.debug("Environment {}", environment);
        log.debug("pmiAmericasUrl {}", pmiAmericasUrl);
        log.debug("pmiSupportEmailAddress {}", pmiSupportEmailAddress);
        log.debug("pmiSupportEmailName {}", pmiSupportEmailName);
        log.debug("sendgridKey {}", sendgridKey);
        log.debug("pagoMundoEmailAddress {}", pagoMundoEmailAddress);
        log.debug("pagoMundoEmailName {}", pagoMundoEmailName);

        log.debug("jHipsterProperties {}", jHipsterProperties);
        log.debug("javaMailSender {}", javaMailSender);
        log.debug("messageSource {}", messageSource);
        log.debug("templateEngine {}", templateEngine);

        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        log.info("==_====> MailService::sendEmailFromTemplate--> User Set: {}", user);

        if (transaction != null) context.setVariable(TRANSACTION, transaction);

        log.info("==_====> MailService::sendEmailFromTemplate--> Transaction Set: {}", transaction);

        String applicationTitle = "PMI Americas";
        String emailURL = pmiAmericasUrl;
        String supportEmail = pmiSupportEmailAddress;
        String baseURLMail = pmiAmericasUrl;
        String finalGreeting = pmiFinalGreeting;
        
        if(user.getExtraURLParameter() != null && (user.getExtraURLParameter().equals("&pagomundo=1")||user.getExtraURLParameter().equals("&pagomundo=1"))) {
            applicationTitle = "PagoMundo";

            emailURL = pagoMundoUrl;
            baseURLMail = pagoMundoBaseURL;
            supportEmail = pagoMundoEmailAddress;
            finalGreeting = pagoMundoFinalGreeting;
        }
        context.setVariable(BASE_URL, baseURLMail);
        context.setVariable(WEB_PAGE, emailURL);

        log.info("==_====> MailService::sendEmailFromTemplate--> Application Title Set: {}", applicationTitle);
        log.info("==_====> MailService::sendEmailFromTemplate--> Base URL Set: {}", baseURL);
        log.info("==_====> MailService::sendEmailFromTemplate--> Email URL Set: {}", emailURL);
        log.info("==_====> MailService::sendEmailFromTemplate--> Support Email Set: {}", supportEmail);
        log.info("==_====> MailService::sendEmailFromTemplate--> Final Greeting Set: {}", finalGreeting);
        
        context.setVariable(SUPPORT_EMAIL_ADDRESS, supportEmail);
        context.setVariable(APPLICATION_TITLE, applicationTitle);
        context.setVariable(FINAL_GREETING, finalGreeting);
        String content = templateEngine.process(templateName, context);
        Object[] args = new Object[] {applicationTitle};
        String subject = messageSource.getMessage(titleKey, args, locale);
        log.info("==_====> MailService::sendEmailFromTemplate--> Content: {}", content);

        log.info("*************** Antes de enviar Email, template name:{}",templateName);
        log.info("*************** Antes de enviar Email, subject:{}",subject);
        if(templateName.contains(NAME_PAYMENT_COMPLETED)) {
        	 log.info("*************** Se cambia el from del correo a :{}",pmiPaymentCompletedEmailAddress); 
        	sendEmailPayment(user.getEmail(), subject, content,templateName,applicationTitle);
        }else {
        	sendEmail(user.getEmail(), subject, content, false, true);
        }
       
        log.info("*************** Despues de enviar Email");

    }


    public String sendEmailFromTemplateStr(User user, Transaction transaction, String templateName, String titleKey) throws MessagingException, IOException {
        log.info("*************** sendEmailFromTemplateStr");
        log.debug("Environment {}", environment);
        log.debug("pmiAmericasUrl {}", pmiAmericasUrl);
        log.debug("pmiSupportEmailAddress {}", pmiSupportEmailAddress);
        log.debug("pmiSupportEmailName {}", pmiSupportEmailName);
        log.debug("sendgridKey {}", sendgridKey);
        log.debug("pagoMundoEmailAddress {}", pagoMundoEmailAddress);
        log.debug("pagoMundoEmailName {}", pagoMundoEmailName);

        Locale locale = Locale.forLanguageTag(user.getLangKey());
        Context context = new Context(locale);
        context.setVariable(USER, user);
        log.info("==_====> MailService::sendEmailFromTemplateStr--> User Set: {}", user);

        if (transaction != null) context.setVariable(TRANSACTION, transaction);

        log.info("==_====> MailService::sendEmailFromTemplateStr--> Transaction Set: {}", transaction);

        String applicationTitle = "PMI Americas";
        String emailURL = pmiAmericasUrl;
        String supportEmail = pmiSupportEmailAddress;

        if(user.getExtraURLParameter() != null && (user.getExtraURLParameter().equals("&pagomundo=1")||user.getExtraURLParameter().equals("&pagomundo=1"))) {
            applicationTitle = "PagoMundo";
            emailURL = pagoMundoUrl;
            baseURL = pagoMundoBaseURL;
            supportEmail = pagoMundoEmailAddress;
        }
        context.setVariable(BASE_URL, baseURL);
        context.setVariable(WEB_PAGE, emailURL);

        log.info("==_====> MailService::sendEmailFromTemplateStr--> Base URL Set: {}", baseURL);
        log.info("==_====> MailService::sendEmailFromTemplate--> Email URL Set: {}", emailURL);
        context.setVariable(SUPPORT_EMAIL_ADDRESS, supportEmail);
        context.setVariable(APPLICATION_TITLE, applicationTitle);
        String content = templateEngine.process(templateName, context);
        Object[] args = new Object[] {applicationTitle};
        String subject = messageSource.getMessage(titleKey, args, locale);
        log.info("==_====> MailService::sendEmailFromTemplateStr--> Content: {}", content);

        log.info("*************** Antes getEmail");
//        sendEmail(user.getEmail(), subject, content, false, true);
        log.info("*************** Despues getEmail");
        return "<!--"+subject+"-->"+"\n"+content;
    }
    
    void sendEmailPayment(String to, String subject, String content,String template,String titleApp) throws IOException {

    	log.debug("************ DENTRO sendEmail paymentComplete");
        log.debug("Environment {}", environment);
        log.debug("pmiAmericasUrl {}", pmiAmericasUrl);
        log.debug("pmiPaymentCompleteEmailAddress {}", pmiPaymentCompletedEmailAddress);
        log.debug("pmiPaymentCompleteEmailName {}", pmiPaymentCompletedEmailName);
        log.debug("pmiSupportEmailAddress {}", pmiSupportEmailAddress);
        log.debug("pmiSupportEmailName {}", pmiSupportEmailName);
        log.debug("sendgridKey {}", sendgridKey);
        log.debug("pagoMundoEmailAddress {}", pagoMundoEmailAddress);
        log.debug("pagoMundoEmailName {}", pagoMundoEmailName);
        log.debug("template {}", template);

        Email fromsg = new Email(pmiSupportEmailAddress, pmiSupportEmailName);
        
    	if(titleApp.contains("PagoMundo")){
        	// Se cambia correo a pagomundo
    		log.info("es para pagomundo plantilla {},subject:{}", template,subject);
        	fromsg = new Email(pmiPaymentCompletedEmailAddress, pmiPaymentCompletedEmailName);
    	}

    	String subjectsg = (environment==null||!environment.equalsIgnoreCase("prod") ? "[" + environment + "] " : "") + subject;

    	Email tosg = new Email(to);
    	Content contentsg = new Content("text/html", content);
    	Mail mailsg = new Mail(fromsg, subjectsg, tosg, contentsg);

    	SendGrid sg = new SendGrid(sendgridKey);

    	Request request = new Request();

    	try {

          request.setMethod(Method.POST);
          request.setEndpoint("mail/send");
          request.setBody(mailsg.build());
          Response response = sg.api(request);

          log.debug("getStatusCode {}", response.getStatusCode());
          log.debug("getBody {}", response.getBody());
          log.debug("getHeaders {}", response.getHeaders());

          log.debug("Sent email to User '{}'", to);

        } catch (IOException ex) {
        	log.debug("Error sending EMAIL '{}'", to);
          throw ex;
        }

    	
    }

}
