package com.pagomundo.service.integrations;

import com.pagomundo.domain.IntegrationAudit;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.repository.IntegrationAuditRepository;
import com.pagomundo.repository.search.IntegrationAuditSearchRepository;
import com.pagomundo.service.dto.integrations.IntegrationAuditDTO;
import com.pagomundo.service.mapper.IntegrationAuditMapper;
import com.pagomundo.web.rest.errors.NotFoundAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link IntegrationAudit}.
 */
@Service
@Transactional
public class IntegrationAuditService {
    public static final String ENTITY_NAME = "integrationAuditService";
    public static final String NOT_FOUND = ENTITY_NAME + ".001";

    private final Logger log = LoggerFactory.getLogger(IntegrationAuditService.class);

    private final IntegrationAuditRepository integrationAuditRepository;
    private final IntegrationAuditMapper integrationAuditMapper;
    private final IntegrationAuditSearchRepository integrationAuditSearchRepository;

    public IntegrationAuditService(IntegrationAuditRepository integrationAuditRepository, IntegrationAuditMapper integrationAuditMapper, IntegrationAuditSearchRepository integrationAuditSearchRepository) {
        this.integrationAuditRepository = integrationAuditRepository;
        this.integrationAuditMapper = integrationAuditMapper;
        this.integrationAuditSearchRepository = integrationAuditSearchRepository;
    }

    /**
     * Save a integrationAudit.
     *
     * @param ia the entity to save.
     * @return the persisted entity.
     */
    public IntegrationAudit save(IntegrationAudit ia) {
        log.debug("Request to save IntegrationAudit : {}", ia);
        IntegrationAudit result = integrationAuditRepository.saveAndFlush(ia);
        integrationAuditSearchRepository.save(result);
        return result;
    }

    /**
     * Save a integrationAudit.
     *
     * @param iaDTO the entity to save.
     * @return the persisted entity.
     */
    public IntegrationAuditDTO save(IntegrationAuditDTO iaDTO) {
        log.debug("Request to save IntegrationAuditDTO : {}", iaDTO);
        return integrationAuditMapper.toDto(save(integrationAuditMapper.toEntity(iaDTO)));
    }

    /**
     * Update a integrationAudit.
     *
     * @param iaDTO the entity to save.
     * @return the persisted entity.
     */
    public IntegrationAuditDTO update(IntegrationAuditDTO iaDTO) {
        log.debug("Request to update IntegrationAuditDTO : {}", iaDTO);
        return integrationAuditMapper.toDto(update(integrationAuditMapper.toEntity(iaDTO)));
    }

    /**
     * Update a integrationAudit.
     *
     * @param ia the entity to save.
     * @return the persisted entity.
     */
    public IntegrationAudit update(IntegrationAudit ia) {
        log.debug("Request to update IntegrationAudit : {}", ia);
        IntegrationAudit cia = findOneEntity(ia.getId()).orElseThrow(() ->
            new NotFoundAlertException(IntegrationAuditService.ENTITY_NAME + " id: " + ia.getId(),
                ENTITY_NAME, IntegrationAuditService.NOT_FOUND));

        cia.setResponseMessage(ia.getResponseMessage());
        cia.setProviderId(ia.getProviderId());
        cia.setExtraResponseEndpoint(ia.getExtraResponseEndpoint());
        cia.setResponseDate(ia.getResponseDate());
        cia.setStatus(ia.getStatus());
        cia.setStatusReason(ia.getStatusReason());
        cia.setRetries(ia.getRetries());
        cia.setMaxRetries(ia.getMaxRetries());

        log.debug("Current IntegrationAudit : {}", cia);
        return save(cia);
    }

    /**
     * Get all the integrationAudits.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<IntegrationAuditDTO> findAll(Pageable pageable) {
        log.debug("Request to get all IntegrationAudits");
        return integrationAuditRepository.findAll(pageable)
            .map(integrationAuditMapper::toDto);
    }

    /**
     * Get one integrationAudit by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<IntegrationAudit> findOneEntity(Long id) {
        log.debug("Request to get IntegrationAudit : {}", id);
        return integrationAuditRepository.findById(id);
    }

    /**
     * Get one integrationAudit by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<IntegrationAuditDTO> findOne(Long id) {
        log.debug("Request to get IntegrationAudit : {}", id);
        return integrationAuditRepository.findById(id)
            .map(integrationAuditMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Optional<IntegrationAuditDTO> findByProviderAndProviderId(String provider, String id) {
        log.debug("Request to get IntegrationAudit : {} id {}", provider, id);
        return integrationAuditRepository.findOneByProviderAndProviderId(provider, id)
            .map(integrationAuditMapper::toDto);
    }

    /**
     * Delete the integrationAudit by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete IntegrationAudit : {}", id);
        integrationAuditRepository.deleteById(id);
        integrationAuditSearchRepository.deleteById(id);
    }

    /**
     * Search for the integrationAudit corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<IntegrationAuditDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of IntegrationAudits for query {}", query);
        return integrationAuditSearchRepository.search(queryStringQuery(query), pageable)
            .map(integrationAuditMapper::toDto);
    }

    @Transactional(readOnly = true)
    public List<IntegrationAuditDTO> findAllByQueryNoResponse(String provider, Integer minutes, Integer maxRows) {
        Optional<List<IntegrationAudit>> values = integrationAuditRepository.findAllNoResponseByProvider(provider, minutes, maxRows, ZonedDateTime.now());
        List<IntegrationAuditDTO> returnList = new ArrayList<>();
        values.ifPresent(integrationAuditValues ->
            integrationAuditValues.forEach(integrationAudit -> {
                returnList.add(integrationAuditMapper.toDto(integrationAudit));
            })
        );
        return returnList;
    }

    public void updateIntegrationAudit(IntegrationAuditDTO integrationAuditDTO, Status status, String statusReason) {
        integrationAuditDTO.setStatus(status);
        integrationAuditDTO.setStatusReason(statusReason);
        IntegrationAuditDTO result = update(integrationAuditDTO);
        log.info("--> Updated IntegrationAudit - id: {} - status: '{}' - status: '{}' - entityId: {}",
            result.getId(), result.getStatus(), result.getStatusReason(), result.getEntityId());
    }
}
