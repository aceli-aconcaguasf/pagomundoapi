package com.pagomundo.service.integrations;

import com.pagomundo.domain.IntegrationAuditExtraResponse;
import com.pagomundo.repository.IntegrationAuditExtraResponseRepository;
import com.pagomundo.repository.search.IntegrationAuditExtraResponseSearchRepository;
import com.pagomundo.service.dto.integrations.IntegrationAuditExtraResponseDTO;
import com.pagomundo.service.mapper.IntegrationAuditExtraResponseMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link IntegrationAuditExtraResponse}.
 */
@Service
@Transactional
public class IntegrationAuditExtraResponseService {
    private final Logger log = LoggerFactory.getLogger(IntegrationAuditExtraResponseService.class);
    private final IntegrationAuditExtraResponseRepository integrationAuditExtraResponseRepository;
    private final IntegrationAuditExtraResponseMapper integrationAuditExtraResponseMapper;
    private final IntegrationAuditExtraResponseSearchRepository integrationAuditExtraResponseSearchRepository;

    public IntegrationAuditExtraResponseService(
        IntegrationAuditExtraResponseRepository integrationAuditExtraResponseRepository,
        IntegrationAuditExtraResponseMapper integrationAuditExtraResponseMapper,
        IntegrationAuditExtraResponseSearchRepository integrationAuditExtraResponseSearchRepository) {
        this.integrationAuditExtraResponseRepository = integrationAuditExtraResponseRepository;
        this.integrationAuditExtraResponseMapper = integrationAuditExtraResponseMapper;
        this.integrationAuditExtraResponseSearchRepository = integrationAuditExtraResponseSearchRepository;
    }

    /**
     * Save a integrationAuditExtraResponse.
     *
     * @param integrationAuditExtraResponseDTO the entity to save.
     * @return the persisted entity.
     */
    public IntegrationAuditExtraResponseDTO save(IntegrationAuditExtraResponseDTO integrationAuditExtraResponseDTO) {
        log.debug("Request to save IntegrationAuditExtraResponse : {}", integrationAuditExtraResponseDTO);
        IntegrationAuditExtraResponse integrationAuditExtraResponse =
            integrationAuditExtraResponseMapper.toEntity(integrationAuditExtraResponseDTO);
        integrationAuditExtraResponse = integrationAuditExtraResponseRepository.save(integrationAuditExtraResponse);
        IntegrationAuditExtraResponseDTO result =
            integrationAuditExtraResponseMapper.toDto(integrationAuditExtraResponse);
        integrationAuditExtraResponseSearchRepository.save(integrationAuditExtraResponse);
        return result;
    }

    /**
     * Get all the integrationAuditExtraResponses.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<IntegrationAuditExtraResponseDTO> findAll(Pageable pageable) {
        log.debug("Request to get all IntegrationAuditExtraResponses");
        return integrationAuditExtraResponseRepository.findAll(pageable)
            .map(integrationAuditExtraResponseMapper::toDto);
    }

    /**
     * Get one integrationAuditExtraResponse by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<IntegrationAuditExtraResponseDTO> findOne(Long id) {
        log.debug("Request to get IntegrationAuditExtraResponse : {}", id);
        return integrationAuditExtraResponseRepository.findById(id)
            .map(integrationAuditExtraResponseMapper::toDto);
    }

    /**
     * Delete the integrationAuditExtraResponse by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete IntegrationAuditExtraResponse : {}", id);
        integrationAuditExtraResponseRepository.deleteById(id);
        integrationAuditExtraResponseSearchRepository.deleteById(id);
    }

    /**
     * Search for the integrationAuditExtraResponse corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<IntegrationAuditExtraResponseDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of IntegrationAuditExtraResponses for query {}", query);
        return integrationAuditExtraResponseSearchRepository.search(queryStringQuery(query), pageable)
            .map(integrationAuditExtraResponseMapper::toDto);
    }

    /**
     * Get all the integrationAuditExtraResponses.
     *
     * @param minutes the waiting time to stp status change.
     * @param maxRows the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<IntegrationAuditExtraResponseDTO> findAllByQuery(Integer minutes, Integer maxRows) {
        Optional<List<IntegrationAuditExtraResponse>> values =
            integrationAuditExtraResponseRepository.findAllStpByQuery(minutes, maxRows, ZonedDateTime.now());
        List<IntegrationAuditExtraResponseDTO> returnList = new ArrayList<>();
        values.ifPresent(integrationAuditExtraResponses ->
            integrationAuditExtraResponses.forEach(integrationAuditExtraResponse -> {
                returnList.add(integrationAuditExtraResponseMapper.toDto(integrationAuditExtraResponse));
            })
        );
        return returnList;
    }
}
