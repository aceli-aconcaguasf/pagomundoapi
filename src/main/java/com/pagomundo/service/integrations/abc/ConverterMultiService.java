package com.pagomundo.service.integrations.abc;


import com.pagomundo.domain.DTO.abc.*;
import com.pagomundo.domain.integration.abc.converterMulti.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import javax.xml.bind.JAXBException;
import java.util.List;

@Service
@Transactional
/**
 * This class connects to ConverterMulti service on the abc bank side.
 * It is meant to query information about balances, movements, and neighborhoods.
 */
public class ConverterMultiService extends WebServiceGatewaySupport {
    private static final Logger log = LoggerFactory.getLogger(ConverterMultiService.class);
    private final ObjectFactory objectFactory = new ObjectFactory();

    private final String serviceUrl = "https://desavdigital.abccapital.com.mx:9098/";

    /**
     * Get list of neighborhoods and its ids
     * @param neighborhoodQueryDTO Object with zip code and neighborhood name
     * @return List of NeighborhoodResponseDTO
     */
    public NeighborhoodResponseDTO getNeighborhoodId(NeighborhoodQueryDTO neighborhoodQueryDTO) throws JAXBException {
        MCITWSInvoke request = objectFactory.createMCITWSInvoke();
        request.setArg0(objectFactory.createString(MCITWSInvokeAssembler.neighborhoodQueryDTO2XMLrequest(neighborhoodQueryDTO)));

        log.info("Requesting to abc neighborhood Ids ");

        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("com.pagomundo.domain.integration.abc.converterMulti");
        setMarshaller(marshaller);
        setUnmarshaller(marshaller);

        MCITWSInvokeResponse response = (MCITWSInvokeResponse) getWebServiceTemplate()
            .marshalSendAndReceive(serviceUrl, request, new SoapActionCallback("http://tempuri.org/IConverterMulti"));

        NeighborhoodResponseDTO result = MCITWSInvokeResponseAssembler.mCITWSInvokeResponse2NeighborhoodResponseDTO(response);
        return result;
    }

    public BalanceResponseDTO getAccountBalance(BalanceQueryDTO balanceQueryDTO) throws JAXBException {
        MCITWSInvoke request = objectFactory.createMCITWSInvoke();
        request.setArg0(objectFactory.createString(MCITWSInvokeAssembler.balanceQueryDTO2XMLrequest(balanceQueryDTO)));

        log.info("Requesting to abc neighborhood Ids ");


        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("com.pagomundo.domain.integration.abc.converterMulti");
        setMarshaller(marshaller);
        setUnmarshaller(marshaller);

        MCITWSInvokeResponse response = (MCITWSInvokeResponse) getWebServiceTemplate()
            .marshalSendAndReceive(serviceUrl, request, new SoapActionCallback("http://tempuri.org/IConverterMulti/"));

        BalanceResponseDTO result = MCITWSInvokeResponseAssembler.mCITWSInvokeResponse2BalanceResponseDTO(response);
        return result;
    }

    public AccountMovementsResponseDTO getAccountMovements(AccountMovementsQueryDTO accountMovementsQueryDTO) throws JAXBException {
        MCITWSInvoke request = objectFactory.createMCITWSInvoke();
        request.setArg0(objectFactory.createString(MCITWSInvokeAssembler.accountMovementsQueryDTO2XMLrequest(accountMovementsQueryDTO)));

        log.info("Requesting to abc neighborhood Ids ");

        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("com.pagomundo.domain.integration.abc.converterMulti");
        setMarshaller(marshaller);
        setUnmarshaller(marshaller);

        MCITWSInvokeResponse response = (MCITWSInvokeResponse) getWebServiceTemplate()
            .marshalSendAndReceive(serviceUrl, request, new SoapActionCallback("http://tempuri.org/IConverterMulti/"));

        AccountMovementsResponseDTO result = MCITWSInvokeResponseAssembler.mCITWSInvokeResponse2AccountMovementsResponseDTO(response);
        return result;
    }

    public AccountPendingMovementsResponseDTO getAccountPendingMovements(AccountPendingMovementsQueryDTO accountPendingMovementsQueryDTO) throws JAXBException {
        MCITWSInvoke request = objectFactory.createMCITWSInvoke();
        request.setArg0(objectFactory.createString(MCITWSInvokeAssembler.accountPendingMovementsQueryDTO2XMLrequest(accountPendingMovementsQueryDTO)));

        log.info("Requesting to abc neighborhood Ids ");

        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("com.pagomundo.domain.integration.abc.converterMulti");
        setMarshaller(marshaller);
        setUnmarshaller(marshaller);

        MCITWSInvokeResponse response = (MCITWSInvokeResponse) getWebServiceTemplate()
            .marshalSendAndReceive(serviceUrl, request, new SoapActionCallback("http://tempuri.org/IConverterMulti/"));

        AccountPendingMovementsResponseDTO result = MCITWSInvokeResponseAssembler.mCITWSInvokeResponse2AccountPendingMovementsResponseDTO(response);
        return result;
    }

}
