/**
 * ConverterMultiLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.pagomundo.service.integrations.abc.ConverterMuliServiceAxis.org.tempuri;

public class ConverterMultiLocator extends org.apache.axis.client.Service implements com.pagomundo.service.integrations.abc.ConverterMuliServiceAxis.org.tempuri.ConverterMulti {

    public ConverterMultiLocator() {
    }


    public ConverterMultiLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ConverterMultiLocator(String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for BasicHttpsBinding_IConverterMulti
    private String BasicHttpsBinding_IConverterMulti_address = "https://desavdigital.abccapital.com.mx:9098/MCI";

    public String getBasicHttpsBinding_IConverterMultiAddress() {
        return BasicHttpsBinding_IConverterMulti_address;
    }

    // The WSDD service name defaults to the port name.
    private String BasicHttpsBinding_IConverterMultiWSDDServiceName = "BasicHttpsBinding_IConverterMulti";

    public String getBasicHttpsBinding_IConverterMultiWSDDServiceName() {
        return BasicHttpsBinding_IConverterMultiWSDDServiceName;
    }

    public void setBasicHttpsBinding_IConverterMultiWSDDServiceName(String name) {
        BasicHttpsBinding_IConverterMultiWSDDServiceName = name;
    }

    public com.pagomundo.service.integrations.abc.ConverterMuliServiceAxis.org.tempuri.IConverterMulti getBasicHttpsBinding_IConverterMulti() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(BasicHttpsBinding_IConverterMulti_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getBasicHttpsBinding_IConverterMulti(endpoint);
    }

    public com.pagomundo.service.integrations.abc.ConverterMuliServiceAxis.org.tempuri.IConverterMulti getBasicHttpsBinding_IConverterMulti(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.pagomundo.service.integrations.abc.ConverterMuliServiceAxis.org.tempuri.BasicHttpsBinding_IConverterMultiStub _stub = new com.pagomundo.service.integrations.abc.ConverterMuliServiceAxis.org.tempuri.BasicHttpsBinding_IConverterMultiStub(portAddress, this);
            _stub.setPortName(getBasicHttpsBinding_IConverterMultiWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setBasicHttpsBinding_IConverterMultiEndpointAddress(String address) {
        BasicHttpsBinding_IConverterMulti_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.pagomundo.service.integrations.abc.ConverterMuliServiceAxis.org.tempuri.IConverterMulti.class.isAssignableFrom(serviceEndpointInterface)) {
                com.pagomundo.service.integrations.abc.ConverterMuliServiceAxis.org.tempuri.BasicHttpsBinding_IConverterMultiStub _stub = new com.pagomundo.service.integrations.abc.ConverterMuliServiceAxis.org.tempuri.BasicHttpsBinding_IConverterMultiStub(new java.net.URL(BasicHttpsBinding_IConverterMulti_address), this);
                _stub.setPortName(getBasicHttpsBinding_IConverterMultiWSDDServiceName());
                return _stub;
            }
        }
        catch (Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        String inputPortName = portName.getLocalPart();
        if ("BasicHttpsBinding_IConverterMulti".equals(inputPortName)) {
            return getBasicHttpsBinding_IConverterMulti();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://tempuri.org/", "ConverterMulti");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://tempuri.org/", "BasicHttpsBinding_IConverterMulti"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(String portName, String address) throws javax.xml.rpc.ServiceException {

if ("BasicHttpsBinding_IConverterMulti".equals(portName)) {
            setBasicHttpsBinding_IConverterMultiEndpointAddress(address);
        }
        else
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
