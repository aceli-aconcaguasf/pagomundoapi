/**
 * ConverterMulti.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.pagomundo.service.integrations.abc.ConverterMuliServiceAxis.org.tempuri;

public interface ConverterMulti extends javax.xml.rpc.Service {
    public String getBasicHttpsBinding_IConverterMultiAddress();

    public com.pagomundo.service.integrations.abc.ConverterMuliServiceAxis.org.tempuri.IConverterMulti getBasicHttpsBinding_IConverterMulti() throws javax.xml.rpc.ServiceException;

    public com.pagomundo.service.integrations.abc.ConverterMuliServiceAxis.org.tempuri.IConverterMulti getBasicHttpsBinding_IConverterMulti(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
