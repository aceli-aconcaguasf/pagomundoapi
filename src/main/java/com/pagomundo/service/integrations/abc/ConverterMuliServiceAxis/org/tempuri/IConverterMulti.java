/**
 * IConverterMulti.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.pagomundo.service.integrations.abc.ConverterMuliServiceAxis.org.tempuri;

public interface IConverterMulti extends java.rmi.Remote {
    public String MCITWSInvoke(String arg0) throws java.rmi.RemoteException;
}
