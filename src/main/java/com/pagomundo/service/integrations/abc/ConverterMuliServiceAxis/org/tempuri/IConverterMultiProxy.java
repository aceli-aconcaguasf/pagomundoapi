package com.pagomundo.service.integrations.abc.ConverterMuliServiceAxis.org.tempuri;

public class IConverterMultiProxy implements com.pagomundo.service.integrations.abc.ConverterMuliServiceAxis.org.tempuri.IConverterMulti {
  private String _endpoint = null;
  private com.pagomundo.service.integrations.abc.ConverterMuliServiceAxis.org.tempuri.IConverterMulti iConverterMulti = null;

  public IConverterMultiProxy() {
    _initIConverterMultiProxy();
  }

  public IConverterMultiProxy(String endpoint) {
    _endpoint = endpoint;
    _initIConverterMultiProxy();
  }

  private void _initIConverterMultiProxy() {
    try {
      iConverterMulti = (new com.pagomundo.service.integrations.abc.ConverterMuliServiceAxis.org.tempuri.ConverterMultiLocator()).getBasicHttpsBinding_IConverterMulti();
      if (iConverterMulti != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)iConverterMulti)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)iConverterMulti)._getProperty("javax.xml.rpc.service.endpoint.address");
      }

    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }

  public String getEndpoint() {
    return _endpoint;
  }

  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (iConverterMulti != null)
      ((javax.xml.rpc.Stub)iConverterMulti)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);

  }

  public com.pagomundo.service.integrations.abc.ConverterMuliServiceAxis.org.tempuri.IConverterMulti getIConverterMulti() {
    if (iConverterMulti == null)
      _initIConverterMultiProxy();
    return iConverterMulti;
  }

  public String MCITWSInvoke(String arg0) throws java.rmi.RemoteException{
    if (iConverterMulti == null)
      _initIConverterMultiProxy();
    return iConverterMulti.MCITWSInvoke(arg0);
  }


}
