package com.pagomundo.service.integrations.abc;


import com.pagomundo.domain.DTO.abc.*;
import com.pagomundo.domain.integration.abc.converterMulti.*;
import com.pagomundo.service.integrations.abc.ConverterMuliServiceAxis.org.tempuri.IConverterMulti;
import com.pagomundo.service.integrations.abc.ConverterMuliServiceAxis.org.tempuri.IConverterMultiProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import javax.xml.bind.JAXBException;
import java.rmi.RemoteException;
import java.util.List;

@Service
@Transactional
/**
 * This class connects to ConverterMulti service on the abc bank side.
 * It is meant to query information about balances, movements, and neighborhoods.
 */
public class ConverterMultiServiceAxis extends WebServiceGatewaySupport {
    private static final Logger log = LoggerFactory.getLogger(ConverterMultiServiceAxis.class);
    private final ObjectFactory objectFactory = new ObjectFactory();

    /**
     * Get list of neighborhoods and its ids
     * @param neighborhoodQueryDTO Object with zip code and neighborhood name
     * @return List of NeighborhoodResponseDTO
     */
    public NeighborhoodResponseDTO getNeighborhoodId(NeighborhoodQueryDTO neighborhoodQueryDTO) throws RemoteException, JAXBException {
        IConverterMulti iConverterMulti = new IConverterMultiProxy().getIConverterMulti();
        String mCITWSInvoke = MCITWSInvokeAssembler.neighborhoodQueryDTO2XMLrequest(neighborhoodQueryDTO);

        log.info("Requesting to abc neighborhood Ids ");

        String mCITWSInvokeResponse = iConverterMulti.MCITWSInvoke(mCITWSInvoke);

        log.info("Result from service ");

        NeighborhoodResponseDTO result = MCITWSInvokeResponseAssembler.mCITWSInvokeResponse2NeighborhoodResponseDTO(mCITWSInvokeResponse);
        return result;
    }

    public BalanceResponseDTO getAccountBalance(BalanceQueryDTO balanceQueryDTO) throws JAXBException, RemoteException {
        IConverterMulti iConverterMulti = new IConverterMultiProxy().getIConverterMulti();
        String mCITWSInvoke = MCITWSInvokeAssembler.balanceQueryDTO2XMLrequest(balanceQueryDTO);

        log.info("Requesting to abc Account Balance ");

        String mCITWSInvokeResponse = iConverterMulti.MCITWSInvoke(mCITWSInvoke);

        log.info("Result from service ");

        BalanceResponseDTO result = MCITWSInvokeResponseAssembler.mCITWSInvokeResponse2BalanceResponseDTO(mCITWSInvokeResponse);
        return result;
    }

    public AccountMovementsResponseDTO getAccountMovements(AccountMovementsQueryDTO accountMovementsQueryDTO) throws JAXBException, RemoteException {
        IConverterMulti iConverterMulti = new IConverterMultiProxy().getIConverterMulti();
        String mCITWSInvoke = MCITWSInvokeAssembler.accountMovementsQueryDTO2XMLrequest(accountMovementsQueryDTO);

        log.info("Requesting to abc Account Movements ");

        String mCITWSInvokeResponse = iConverterMulti.MCITWSInvoke(mCITWSInvoke);

        log.info("Result from service ");

        AccountMovementsResponseDTO result = MCITWSInvokeResponseAssembler.mCITWSInvokeResponse2AccountMovementsResponseDTO(mCITWSInvokeResponse);
        return result;
    }

    public AccountPendingMovementsResponseDTO getAccountPendingMovements(AccountPendingMovementsQueryDTO accountPendingMovementsQueryDTO) throws JAXBException, RemoteException {
        IConverterMulti iConverterMulti = new IConverterMultiProxy().getIConverterMulti();
        String mCITWSInvoke = MCITWSInvokeAssembler.accountPendingMovementsQueryDTO2XMLrequest(accountPendingMovementsQueryDTO);

        log.info("Requesting to abc Account Pending Movements ");

        String mCITWSInvokeResponse = iConverterMulti.MCITWSInvoke(mCITWSInvoke);

        log.info("Result from service ");

        AccountPendingMovementsResponseDTO result = MCITWSInvokeResponseAssembler.mCITWSInvokeResponse2AccountPendingMovementsResponseDTO(mCITWSInvokeResponse);
        return result;
    }

}
