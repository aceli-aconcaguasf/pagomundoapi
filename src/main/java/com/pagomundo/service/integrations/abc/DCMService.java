package com.pagomundo.service.integrations.abc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pagomundo.domain.integration.abc.service1.ObjectFactory;
import com.pagomundo.domain.integration.abc.service1.Recibedoc;
import com.pagomundo.domain.integration.abc.service1.RecibedocResponse;
import com.pagomundo.domain.integration.abc.service1.Subirdocumentometafield;
import com.pagomundo.domain.integration.abc.service1.SubirdocumentometafieldResponse;
import com.pagomundo.domain.integration.abc.service1.Subirregistrodedoc;
import com.pagomundo.domain.integration.abc.service1.SubirregistrodedocResponse;
import com.pagomundo.domain.integration.abc.service1.Existlineandcuc;
import com.pagomundo.domain.integration.abc.service1.ExistlineandcucResponse;
import com.pagomundo.domain.integration.abc.service1.Existscuc;
import com.pagomundo.domain.integration.abc.service1.ExistscucResponse;
import com.pagomundo.domain.integration.abc.service1.Insertlineandcuc;
import com.pagomundo.domain.integration.abc.service1.InsertlineandcucResponse;
import com.pagomundo.domain.integration.abc.service1.NuevoCliente;
import com.pagomundo.domain.integration.abc.service1.NuevoClienteResponse;

@Service
@Transactional
/**
 * This class connects to DCM (digital content manager) service on the abc bank side.
 * It is meant to support the load of digital documents.
 */
public class DCMService extends WebServiceGatewaySupport {
    private static final Logger log = LoggerFactory.getLogger(DCMService.class);
    private final ObjectFactory objectFactory = new ObjectFactory();
    private final String serviceUrl = "https://desavataico.abccapital.com.mx/Service1.svc";
    // business line must be set to 8, it is a constant given by the provider to use their service
    private final Integer businessLine = 8;
    private final Integer canal = 11;

    /**
     * Checks wether a unique client number exists
     * @param cuc client code, alphanumeric string of len 10
     * @return Array of strings
     */
    public List<String> existsCuc(String cuc) {
        Existscuc request = new Existscuc();
        request.setCuc(objectFactory.createExistscucCuc(cuc));

        log.info("Requesting to abc client existence by cuc ");

        ExistscucResponse response = (ExistscucResponse) getWebServiceTemplate()
            .marshalSendAndReceive(serviceUrl, request, new SoapActionCallback("http://tempuri.org/IService1/existscuc"));
        return response.getExistscucResult().getValue().getString();
    }

    /**
     * Registers a new client in the system
     * @param cuc client unique code, alphanumeric string of len 10
     * @param rfc alphanumeric code, in case rfc is not available use first 13 characters of CURP
     * @param name name of the owner of the client company
     * @param businessGroup name of the company
     * @return Array of strings
     */
    public List<String> nuevoCliente(String cuc, String rfc, String name, String businessGroup) {
        NuevoCliente request = new NuevoCliente();

        request.setNewIdcliente(objectFactory.createNuevoClienteNewIdcliente(cuc));
        request.setNewRFC(objectFactory.createNuevoClienteNewRFC(rfc));
        request.setNewname2(objectFactory.createNuevoClienteNewname2(name));
        request.setNewGrupoempresarial(objectFactory.createNuevoClienteNewGrupoempresarial(businessGroup));
        request.setIdLineaNegocio(businessLine);

        log.info("Requesting to abc client and business line registration for client by cuc");

        NuevoClienteResponse response = (NuevoClienteResponse) getWebServiceTemplate()
            .marshalSendAndReceive(serviceUrl, request, new SoapActionCallback("http://tempuri.org/IService1/nuevoCliente"));

        return response.getNuevoClienteResult().getValue().getString();
    }

    /**
     * Registers or asingsa business line to a client
     * @param user service user, alphanumeric code of len 15
     * @param cuc client unique code, alphanumeric string of len 10
     * @return Array of strings
     */
    public List<String> insertlineandcuc(String user, String cuc) {
        Insertlineandcuc request = new Insertlineandcuc();

        request.setUsuario(objectFactory.createInsertlineandcucUsuario(user));
        request.setCuc(objectFactory.createInsertlineandcucCuc(cuc));
        request.setLinea(businessLine);

        log.info("Requesting to abc client and business line insertion for client by cuc: ");

        InsertlineandcucResponse response = (InsertlineandcucResponse) getWebServiceTemplate()
            .marshalSendAndReceive(serviceUrl, request, new SoapActionCallback("http://tempuri.org/IService1/insertlineandcuc"));

        return response.getInsertlineandcucResult().getValue().getString();
    }

    /**
     * verifies the existence of an assignation between client code and business line
     * @param cuc client unique code, alphanumeric string of len 10
     * @return Array of strings
     */
    public List<String> existsLineAndCuc(String cuc) {
        Existlineandcuc request = new Existlineandcuc();

        request.setCuc(objectFactory.createExistlineandcucCuc(cuc));
        request.setLinea(businessLine);

        log.info("Requesting to abc client and business line relation verification for client by cuc");

        ExistlineandcucResponse response = (ExistlineandcucResponse) getWebServiceTemplate()
            .marshalSendAndReceive(serviceUrl, request, new SoapActionCallback("http://tempuri.org/IService1/existlineandcuc"));

        return response.getExistlineandcucResult().getValue().getString();
    }

    /**
     * Given a client and a business line, registers a document
     * @param cuc client unique code, alphanumeric string of len 10
     * @param docType type of document:
     *   252-491-Identificación Oficial (Cta Nivel 2)-: for INE
     *   253-491-Comprobante de Domicilio (Cta Nivel 2)-: for address proof document
     *   254-491-Prueba de Vida-: for proof of life image (specified at this moment as a selfie)
     * @param user service user, alphanumeric code of len 15
     * @return Array of strings
     */
    public List<String> subirregistrodedoc(String cuc, String docType, String user) {
        Subirregistrodedoc request = new Subirregistrodedoc();

        request.setIdcliente(objectFactory.createSubirregistrodedocIdcliente(cuc));
        request.setDsctipodoc2(objectFactory.createSubirregistrodedocDsctipodoc2(docType));
        request.setCveUsr(objectFactory.createSubirregistrodedocCveUsr(user));
        request.setIdLineaNegocio(businessLine);

        log.info("Requesting to abc document registration for client by cuc");

        SubirregistrodedocResponse response = (SubirregistrodedocResponse) getWebServiceTemplate()
            .marshalSendAndReceive(serviceUrl, request, new SoapActionCallback("http://tempuri.org/IService1/Subirregistrodedoc"));

        return response.getSubirregistrodedocResult().getValue().getString();
    }

    /**
     * registers metadatum for a previously registered document, resulting from subirregistrodedoc method
     * @param docId id of the previously registered document
     * @param desc one of the following descriptions:
     *      Docu-Archivo-: then value param is the name of the document file
     *      Creq-Tipo de Identificación (Cta Nivel 2)-: then value is either "INE (Frente)" or "INE (Tras)"
     *      Creq-Canal-: Then value is the defined by the provider
     *  a metadata for each description must be registered, 2 times for Creq-Tipo de Identificación (Cta Nivel 2)-
     *  (one for each side of the id card document INE)
     * @param value depending on the description value can be different things, those are explained in desc param
     * @return Array of strings
     */
    public List<String> subirdocumentometafield(Integer docId, String desc, String value) {
        Subirdocumentometafield request = new Subirdocumentometafield();

        request.setIddoc(docId);
        request.setDsctipodoc(objectFactory.createSubirdocumentometafieldDsctipodoc(desc));
        if (desc == "Creq-Canal-") {
            request.setValores(objectFactory.createSubirdocumentometafieldValores(businessLine + ""));
        }
        else {
            request.setValores(objectFactory.createSubirdocumentometafieldValores(value));
        }

        log.info("Requesting to abc document metadatum registration for document by doc Id");

        SubirdocumentometafieldResponse response = (SubirdocumentometafieldResponse) getWebServiceTemplate()
            .marshalSendAndReceive(serviceUrl, request, new SoapActionCallback("http://tempuri.org/IService1/Subirdocumentometafield"));

        return response.getSubirdocumentometafieldResult().getValue().getString();
    }

    /**
     * Loads a file and assings it to a previously registered document
     * @param byteFile byte array of the file to upload
     * @param docId id of the previously registered document
     * @param user service user, alphanumeric code of len 15
     * @param cuc client unique code, alphanumeric string of len 10
     * @return Array of strings
     */
    public List<String> recibeDoc(byte[] byteFile, String docId, String user, String cuc) {
        Recibedoc request = new Recibedoc();

        request.setArchivo(objectFactory.createRecibedocArchivo(byteFile));
        request.setCuc(objectFactory.createRecibedocCuc(cuc));
        request.setIddoc(objectFactory.createRecibedocIddoc(docId));
        request.setUsuario(objectFactory.createRecibedocUsuario(user));

        log.info("Requesting to abc document upload for document by docId");

        RecibedocResponse response = (RecibedocResponse) getWebServiceTemplate()
            .marshalSendAndReceive(serviceUrl, request, new SoapActionCallback("http://tempuri.org/IService1/recibedoc"));

        return response.getRecibedocResult().getValue().getString();
    }




}
