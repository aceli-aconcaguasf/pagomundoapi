package com.pagomundo.service.integrations.abc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import com.pagomundo.domain.integration.abc.fintech.ObjectFactory;
import com.pagomundo.domain.integration.abc.fintech.RegistroCliente;
import com.pagomundo.domain.integration.abc.fintech.RegistroClienteResponse;
import com.pagomundo.domain.integration.abc.fintech.AltaCuenta;
import com.pagomundo.domain.integration.abc.fintech.AltaCuentaResponse;

@Service
@Transactional
/**
 * This class connects to Fintech service on the abc bank side.
 * This services allow to create a client and sign up a bank account
 */
public class FintechService extends WebServiceGatewaySupport {
    private static final Logger log = LoggerFactory.getLogger(DCMService.class);
    private final ObjectFactory objectFactory = new ObjectFactory();
    private final String serviceUrl = "https://desavdigital.abccapital.com.mx/FintechSolution/FintechService.svc";

    /**
     * Registers a new client to initialize the process of account sign up
     * @param jsonin a json with all the client information (see abc documentation for more information)
     * @return a json with results (see abc documentation for more info)
     */
    public String registroCliente(String jsonin) {
        RegistroCliente request = new RegistroCliente();

        request.setJsonIn(objectFactory.createRegistroClienteJsonIn(jsonin));

        log.info("Requesting client registration.");
        
        RegistroClienteResponse response = (RegistroClienteResponse) getWebServiceTemplate()
            .marshalSendAndReceive(serviceUrl, request, new SoapActionCallback("http://tempuri.org/IFintechService/registroCliente"));
        return response.getRegistroClienteResult().getValue();
    }

    /**
     * Registers a new client to initialize the process of account sign up
     * @param jsonin a json with all the client information (see abc documentation for more information)
     * @return a json with results (see abc documentation for more info)
     */
    public String altaCuenta(String jsonin) {
        AltaCuenta request = new AltaCuenta();

        request.setJsonIn(objectFactory.createAltaCuentaJsonIn(jsonin));

        log.info("Requesting account registration.");
        
        AltaCuentaResponse response = (AltaCuentaResponse) getWebServiceTemplate()
            .marshalSendAndReceive(serviceUrl, request, new SoapActionCallback("http://tempuri.org/IFintechService/altaCuenta"));
        return response.getAltaCuentaResult().getValue();
    }
    
}
