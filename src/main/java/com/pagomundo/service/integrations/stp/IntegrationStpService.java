package com.pagomundo.service.integrations.stp;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.pagomundo.domain.IntegrationAudit;
import com.pagomundo.domain.Transaction;
import com.pagomundo.domain.enumeration.ErrorCode;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.domain.enumeration.stp.StpAccountType;
import com.pagomundo.domain.enumeration.stp.StpBankId;
import com.pagomundo.domain.enumeration.stp.StpPaymentType;
import com.pagomundo.service.NotificationReceiverService;
import com.pagomundo.service.TransactionService;
import com.pagomundo.service.TranslateService;
import com.pagomundo.service.dto.integrations.IntegrationAuditDTO;
import com.pagomundo.service.dto.integrations.IntegrationAuditExtraResponseDTO;
import com.pagomundo.service.dto.integrations.stp.IntegrationResponseStpDTO;
import com.pagomundo.service.dto.integrations.stp.IntegrationStpDTO;
import com.pagomundo.service.integrations.IntegrationAuditExtraResponseService;
import com.pagomundo.service.integrations.IntegrationAuditService;
import com.pagomundo.service.mapper.IntegrationAuditMapper;
import com.pagomundo.service.util.ParameterStringBuilder;
import com.pagomundo.service.util.StpCryptoHandler;
import com.pagomundo.service.util.StpPropertiesReader;
import com.pagomundo.web.rest.errors.BadRequestAlertException;
import com.pagomundo.web.rest.integrations.stp.IntegrationStpResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Service Implementation for managing {@link IntegrationStpResource}.
 */
@Service
public class IntegrationStpService {
    private static final int MAX_RETRIES = 3;
    private static final String PROVIDER = "STP";
    private static final String ENTITY = "transaction";
    private static final String CONNECTION_TIME_OUT = "connectionTimeout";
    private static final String RESPONSE_TIME_OUT = "responseTimeout";
    private static final String PUT = "PUT";
    private static final String GET = "GET";
    private static final String ENTITY_VALUE = "integrationStp";
    private static final String ACCEPTED = Status.getStringFromStatus(Status.ACCEPTED);
    private final Logger log = LoggerFactory.getLogger(IntegrationStpService.class);
    private final StpCryptoHandler stpCryptoHandler;
    private final StpPropertiesReader stpPropertiesReader;
    private final IntegrationAuditExtraResponseService integrationAuditExtraResponseService;
    private final TransactionService transactionService;
    private final IntegrationAuditMapper integrationAuditMapper;
    private final IntegrationAuditService integrationAuditService;
    private final NotificationReceiverService notificationReceiverService;
    private final TranslateService translateService;

    @Value("${environment}")
    private String environment;

    private String errorDetail;

    public IntegrationStpService(
        IntegrationAuditService integrationAuditService,
        IntegrationAuditExtraResponseService integrationAuditExtraResponseService,
        TransactionService transactionService, IntegrationAuditMapper integrationAuditMapper,
        NotificationReceiverService notificationReceiverService, TranslateService translateService) throws IOException {
        this.transactionService = transactionService;
        this.integrationAuditMapper = integrationAuditMapper;
        this.notificationReceiverService = notificationReceiverService;
        this.stpPropertiesReader = new StpPropertiesReader();
        stpPropertiesReader.initialize();
        this.stpCryptoHandler = new StpCryptoHandler(stpPropertiesReader);
        this.integrationAuditService = integrationAuditService;
        this.integrationAuditExtraResponseService = integrationAuditExtraResponseService;
        this.translateService = translateService;
    }

    private String getErrorDetail() {
        return errorDetail;
    }

    private void setErrorDetail(String errorDetail) {
        this.errorDetail = errorDetail;
    }

    private Boolean validateNullOrEmpty(String value) {
        if (value == null) return true;
        return value.equals("");
    }

    private Boolean validateAndSetIntegrationStp(IntegrationStpDTO integrationStpDTO) {
        String localErrorDetail = "";
        if (integrationStpDTO == null) {
            localErrorDetail += "Entity integracionStpDTO null,";
            setErrorDetail(localErrorDetail);
            return false;
        }

        localErrorDetail += validateNullOrEmpty(integrationStpDTO.getTrackingId()) ? "trackingId wrong, " : "";
        localErrorDetail += validateNullOrEmpty(integrationStpDTO.getDescription()) ? "description wrong, " : "";
        localErrorDetail += validateNullOrEmpty(integrationStpDTO.getAccountNumberDestiny()) ? "accountNumberDestiny wrong, " : "";
        localErrorDetail += validateNullOrEmpty(integrationStpDTO.getCompany()) ? "company wrong, " : "";
        localErrorDetail += validateNullOrEmpty(integrationStpDTO.getBankIdDestiny() != null ? integrationStpDTO.getBankIdDestiny().getValue() : null) ? "bankIdDestiny wrong, " : "";
        localErrorDetail += validateNullOrEmpty(integrationStpDTO.getAmount()) ? "amount wrong, " : "";
        localErrorDetail += validateNullOrEmpty(integrationStpDTO.getDestinyName()) ? "destinyName wrong, " : "";
        localErrorDetail += validateNullOrEmpty(integrationStpDTO.getReferenceNumber()) ? "referenceNumber wrong, " : "";
        localErrorDetail += validateNullOrEmpty(integrationStpDTO.getAccountTypeDestiny() != null ? integrationStpDTO.getAccountTypeDestiny().getValue() : null) ? "accountTypeDestiny wrong, " : "";

        if (localErrorDetail.length() > 0) {
            setErrorDetail(localErrorDetail);
            return false;
        }

        // Set default values for STP
        integrationStpDTO.setBankIdOrigin(StpBankId.STP);
        integrationStpDTO.setPaymentType(StpPaymentType.THIRD_PARTY);
        integrationStpDTO.setTopology("V");
        integrationStpDTO.setRfcCurpDestiny("ND");
        integrationStpDTO.setAccountTypeOrigin(StpAccountType.CLABE);
        integrationStpDTO.setElectronicSignature(generateSignature(integrationStpDTO));

        return true;
    }

    private String generateSignature(IntegrationStpDTO integrationStpDTO) {
        return stpCryptoHandler.signRequest(integrationStpDTO);
    }

    /**
     * Create a integrationStp.
     *
     * @param integrationStpDTO the entity to save.
     * @return the persisted entity.
     */
    public IntegrationStpDTO create(IntegrationStpDTO integrationStpDTO) {
        log.debug("Request to create one IntegrationStp call : {}", integrationStpDTO);
        if (!validateAndSetIntegrationStp(integrationStpDTO)) throw new BadRequestAlertException(getErrorDetail(),
            ENTITY_VALUE, ErrorCode.ERROR_VALIDATING_FIELDS.getValue().toString());
        IntegrationAuditDTO integrationAuditDTO = mapStpDtoToAuditDto(integrationStpDTO);

        IntegrationAudit integrationAudit = integrationAuditMapper.toEntity(integrationAuditDTO);
        integrationAudit.setStatus(Status.CREATED);
        integrationAudit.setStatusReason(null);
        integrationAudit.setEntityId(Long.valueOf(String.valueOf(integrationAuditDTO.getEntityId()).substring(6)));
        IntegrationAudit result = integrationAuditService.save(integrationAudit);
        log.info("--> Created IntegrationAudit - id: {} - status: '{}' - statusReason: '{}' - entityId: {}",
            result.getId(), result.getStatus(), result.getStatusReason(), result.getEntityId());

        integrationAuditDTO.setId(result.getId());
        integrationAuditDTO = doProcess(integrationAuditDTO);
        integrationAuditDTO.setExtraResponseEndpoint("/integration-stp/responses");
        integrationAuditDTO.setResponseDate(ZonedDateTime.now());
        integrationAuditDTO.setStatus(Status.IN_PROCESS);
        integrationAuditDTO.setStatusReason(null);
        integrationAuditDTO.setRetries(0);
        integrationAuditDTO.setMaxRetries(MAX_RETRIES);

        IntegrationAuditDTO iaUpdated = integrationAuditService.update(integrationAuditDTO);
        log.info("--> Updated IntegrationAudit - id: {} - status: '{}' - statusReason: '{}' - entityId: {}",
            iaUpdated.getId(), iaUpdated.getStatus(), iaUpdated.getStatusReason(), iaUpdated.getEntityId());

        return integrationStpDTO;
    }

    @Async
    public IntegrationAuditDTO doProcess(IntegrationAuditDTO integrationAuditDTO) {
        log.debug("---> doProcess - Request to STP: {}", integrationAuditDTO.getRequestMessage());

        doCall(integrationAuditDTO, this.stpPropertiesReader.getValue("methodRequest"));
        JsonObject json = (JsonObject) new JsonParser().parse(integrationAuditDTO.getResponseMessage());
        log.info("---> Raw response from STP: {}", json);

        JsonObject elements = json.get("resultado").getAsJsonObject();

        integrationAuditDTO.setResponseDate(ZonedDateTime.now());
        int id = elements.get("id").getAsInt();
        integrationAuditDTO.setProviderId(Integer.toString(id));
        if (id <= 0) {
            integrationAuditDTO.setStatus(Status.FAILED);
            integrationAuditDTO.setStatusReason(elements.get("descripcionError").getAsString());
        } else {
            integrationAuditDTO.setStatus(Status.IN_PROCESS);
            integrationAuditDTO.setStatusReason(null);
        }
        return integrationAuditDTO;

    }

    private void doCall(IntegrationAuditDTO integrationAuditDTO, String method) {
        log.info("---> Calling STP...");
        doCall(integrationAuditDTO, method, null);
    }

    private void doCall(IntegrationAuditDTO integrationAuditDTO, String method, String parameter) {
        try {
            URL url = new URL(integrationAuditDTO.getRequestEndpoint());
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            con.setRequestMethod(method);
            con.setRequestProperty("Content-Type", "application/json");
            con.setConnectTimeout(Integer.parseInt(stpPropertiesReader.getValue(CONNECTION_TIME_OUT)));
            con.setReadTimeout(Integer.parseInt(stpPropertiesReader.getValue(RESPONSE_TIME_OUT)));
            if (method.equals(PUT)) {
                con.setDoOutput(true);
                try (OutputStream os = con.getOutputStream()) {
                    byte[] input = integrationAuditDTO.getRequestMessage().getBytes(StandardCharsets.UTF_8);
                    os.write(input, 0, input.length);
                }
            }

            if (method.equals(GET)) {
                if (parameter == null || parameter.equals(""))
                    throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.parameter.empty"),
                        ENTITY_VALUE, ErrorCode.ERROR_CALLING_STP.getValue().toString());

                Map<String, String> parameters = new HashMap<>();
                parameters.put("param1", parameter);

                con.setDoOutput(true);
                DataOutputStream out = new DataOutputStream(con.getOutputStream());
                out.writeBytes(ParameterStringBuilder.getParamsString(parameters));
                out.flush();
                out.close();
            }

            try (BufferedReader br = new BufferedReader(
                new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8))) {
                StringBuilder response = new StringBuilder();
                String responseLine;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                integrationAuditDTO.setResponseMessage(response.toString());
            }
            con.disconnect();
        } catch (Exception e) {
            throw new BadRequestAlertException(e.getMessage(), ENTITY_VALUE, ErrorCode.ERROR_CALLING_STP.getValue().toString());
        }
    }

    private IntegrationAuditDTO mapStpDtoToAuditDto(IntegrationStpDTO integrationStpDTO) {
        IntegrationAuditDTO integrationAuditDTO = new IntegrationAuditDTO();
        try {
            integrationAuditDTO.setProvider(PROVIDER);
            integrationAuditDTO.setEntity(ENTITY);
            integrationAuditDTO.setEntityId(Long.valueOf(integrationStpDTO.getTrackingId()));
            integrationAuditDTO.setRequestDate(ZonedDateTime.now());
            integrationAuditDTO.setRequestEndpoint(stpPropertiesReader.getValue("urlRequest"));
            integrationAuditDTO.setRequestMessage(integrationStpDTO.toStpJsonString());
        } catch (Exception e) {
            log.error("mapStpDtoToAuditDto: ERROR ", e);
        }
        return integrationAuditDTO;
    }

    public String receiveResponse(IntegrationResponseStpDTO responseStp) {
        log.info("-->> STP incoming message: {}", responseStp);
        try {
            if (responseStp.getId() == null ||
                responseStp.getEmpresa() == null ||
                responseStp.getEstado() == null ||
                responseStp.getCausaDevolucion() == null) {
                throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.validation.error"), ENTITY_VALUE, ErrorCode.ERROR_VALIDATING_FIELDS.getValue().toString());
            }
        } catch (Exception e) {
            throw new BadRequestAlertException(e.getMessage(), ENTITY_VALUE, ErrorCode.ERROR_VALIDATING_FIELDS.getValue().toString());
        }

        // Find by provider_id
        Optional<IntegrationAuditDTO> rowIntegrationAuditOpt = integrationAuditService.findByProviderAndProviderId(PROVIDER, responseStp.getId());

        if (rowIntegrationAuditOpt.isPresent()) {
            IntegrationAuditDTO integrationAuditDTO = rowIntegrationAuditOpt.get();

            IntegrationAuditExtraResponseDTO integrationAuditExtraResponseDTO = new IntegrationAuditExtraResponseDTO();
            integrationAuditExtraResponseDTO.setIntegrationAuditId(integrationAuditDTO.getId());
            integrationAuditExtraResponseDTO.setMessage(responseStp.toString());
            integrationAuditExtraResponseDTO.setMessageDate(ZonedDateTime.now());
            IntegrationAuditExtraResponseDTO finalIntegrationAuditExtraResponseDTO = integrationAuditExtraResponseService.save(integrationAuditExtraResponseDTO);
            log.info("--> Created IntegrationAuditExtra - id: {} - status: '{}' - message: '{}' - integrationAuditId: {}",
                finalIntegrationAuditExtraResponseDTO.getId(), responseStp.getEstado(), finalIntegrationAuditExtraResponseDTO.getMessage(), finalIntegrationAuditExtraResponseDTO.getIntegrationAuditId());

            if (!responseStp.getEstado().equalsIgnoreCase(ACCEPTED)) {
                Transaction result = transactionService.updateTransaction(integrationAuditDTO.getEntityId(), Status.REJECTED, responseStp.getCausaDevolucion());
                notificationReceiverService.sendTransactionNotification(result, result.getAdmin());
                integrationAuditService.updateIntegrationAudit(integrationAuditDTO, Status.REJECTED, responseStp.getCausaDevolucion());
            }
        } else {
            log.error("-->> Not found IntegrationAudit with ID: {}", responseStp.getId());
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.row.not.found"), ENTITY_VALUE, ErrorCode.ERROR_IN_DATABASE.getValue().toString());
        }

        return responseStp.toString();
    }

    public Map<IntegrationAuditDTO, JsonObject> getLatestIntegrationAuditExtraResponses() {
        int minutes = environment.equalsIgnoreCase("prod") ? 3 : 6;
        int maxToProcess = 20;
        List<IntegrationAuditExtraResponseDTO> response = integrationAuditExtraResponseService.findAllByQuery(minutes, maxToProcess);
        Map<IntegrationAuditDTO, JsonObject> mapTransactionIdStatus = new HashMap<>();
        response.forEach(integrationAuditExtraResponseDTO -> {
            JsonObject json = (JsonObject) new JsonParser().parse(integrationAuditExtraResponseDTO.getMessage());
            integrationAuditService.findOne(integrationAuditExtraResponseDTO.getIntegrationAuditId()).ifPresent(
                element -> mapTransactionIdStatus.put(element, json)
            );
        });
        return mapTransactionIdStatus;
    }

    public Map<IntegrationAuditDTO, String> getLatestIntegrationAuditWithoutResponses() {
        int minutes = environment.equalsIgnoreCase("prod") ? 5 : 10;
        int maxToProcess = 20;
        List<IntegrationAuditDTO> response = integrationAuditService.findAllByQueryNoResponse(PROVIDER, minutes, maxToProcess);
        Map<IntegrationAuditDTO, String> mapTransactionIdStatus = new HashMap<>();
        response.forEach(
            integrationAuditDTO -> mapTransactionIdStatus.put(integrationAuditDTO,
                "PMI did not get a response from the provider " + PROVIDER + " to update the payment status after " + minutes + " minutes.")
        );
        return mapTransactionIdStatus;
    }
}
