package com.pagomundo.service;

import com.pagomundo.domain.DirectPaymentBank;
import com.pagomundo.repository.DirectPaymentBankRepository;
import com.pagomundo.repository.search.DirectPaymentBankSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link DirectPaymentBank}.
 */
@Service
@Transactional
public class DirectPaymentBankService {
    public static final String ENTITY_NAME = "DirectPaymentBank";
    private final Logger log = LoggerFactory.getLogger(DirectPaymentBankService.class);

    private final DirectPaymentBankRepository directPaymentBankRepository;

    private final DirectPaymentBankSearchRepository directPaymentBankSearchRepository;

    public DirectPaymentBankService(DirectPaymentBankRepository directPaymentBankRepository, DirectPaymentBankSearchRepository directPaymentBankSearchRepository) {
        this.directPaymentBankRepository = directPaymentBankRepository;
        this.directPaymentBankSearchRepository = directPaymentBankSearchRepository;
    }

    /**
     * Save a directPaymentBank.
     *
     * @param directPaymentBank the entity to save.
     * @return the persisted entity.
     */
    public DirectPaymentBank save(DirectPaymentBank directPaymentBank) {
        log.debug("Request to save DirectPaymentBank : {}", directPaymentBank);
        DirectPaymentBank result = directPaymentBankRepository.save(directPaymentBank);
        directPaymentBankSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the directPaymentBanks.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<DirectPaymentBank> findAll(Pageable pageable) {
        log.debug("Request to get all DirectPaymentBanks");
        return directPaymentBankRepository.findAll(pageable);
    }

    /**
     * Get all the directPaymentBanks.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Iterable<DirectPaymentBank> findAll() {
        log.debug("Request to get all DirectPaymentBanks");
        return directPaymentBankRepository.findAll();
    }


    /**
     * Get one directPaymentBank by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<DirectPaymentBank> findOne(Long id) {
        log.debug("Request to get DirectPaymentBank : {}", id);
        if (id == null) return Optional.empty();
        return directPaymentBankRepository.findById(id);
    }

    /**
     * Get one directPaymentBank by bankId.
     *
     * @param bankId the bankId of the bank.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public DirectPaymentBank findOneByBank(Long bankId) {
        
        log.info(">>>>>>>>>> Request to get findOneByBank - DirectPaymentBank by bank: {}", bankId);

        AtomicReference<DirectPaymentBank> dpb = new AtomicReference<>();

        log.info(">>>>>>>>>> findAllByDestinyBankId: {}", directPaymentBankRepository.findAllByDestinyBankId(bankId));

        directPaymentBankRepository.findAllByDestinyBankId(bankId).ifPresent(directPaymentBanks -> {
            
            log.info(">>>>>>>>>> iterator: {}", directPaymentBanks.iterator());

            Iterator<DirectPaymentBank> iterator = directPaymentBanks.iterator();
            
            if (iterator.hasNext()) dpb.set(iterator.next());
            
        });

        log.info(">>>>>>>>>> dpb: {}", dpb);

        return dpb.get();
    }

    /**
     * Get one directPaymentBank by idTypeId.
     *
     * @param idTypeId the idTypeId of the IdType.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public DirectPaymentBank findOneByIdType(Long idTypeId) {
        log.debug("Request to get DirectPaymentBank by idType: {}", idTypeId);

        AtomicReference<DirectPaymentBank> dpb = new AtomicReference<>();
        directPaymentBankRepository.findAllByIdTypeId(idTypeId).ifPresent(directPaymentBanks -> {
            Iterator<DirectPaymentBank> iterator = directPaymentBanks.iterator();
            if (iterator.hasNext()) dpb.set(iterator.next());
        });
        return dpb.get();
    }

    /**
     * Get one directPaymentBank by city.
     *
     * @param cityId the id of the City.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public DirectPaymentBank findOneByCity(Long cityId) {
        log.debug("Request to get DirectPaymentBank by City: {}", cityId);

        AtomicReference<DirectPaymentBank> dpb = new AtomicReference<>();
        directPaymentBankRepository.findAllByDestinyCityId(cityId).ifPresent(directPaymentBanks -> {
            Iterator<DirectPaymentBank> iterator = directPaymentBanks.iterator();
            if (iterator.hasNext()) dpb.set(iterator.next());
        });
        return dpb.get();
    }

    /**
     * Delete the directPaymentBank by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete DirectPaymentBank : {}", id);
        directPaymentBankRepository.deleteById(id);
        directPaymentBankSearchRepository.deleteById(id);
    }

    /**
     * Search for the directPaymentBank corresponding to the query.
     *
     * @param query    the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<DirectPaymentBank> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of DirectPaymentBanks for query {}", query);
        return directPaymentBankSearchRepository.search(queryStringQuery(query), pageable);
    }

    @Transactional
    public void saveAll(List<DirectPaymentBank> directPaymentBankList) {
        if (!directPaymentBankList.isEmpty())
            directPaymentBankSearchRepository.saveAll(directPaymentBankList);
    }

}
