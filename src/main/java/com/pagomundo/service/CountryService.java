package com.pagomundo.service;

import com.cronutils.descriptor.CronDescriptor;
import com.cronutils.model.definition.CronDefinitionBuilder;
import com.cronutils.parser.CronParser;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.pagomundo.domain.Bank;
import com.pagomundo.domain.Country;
import com.pagomundo.domain.DTO.ExchangeRateApiDTO;
import com.pagomundo.repository.CountryRepository;
import com.pagomundo.repository.search.CountrySearchRepository;
import com.pagomundo.repository.search.RefetchService;
import com.pagomundo.web.rest.errors.NotFoundAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import static com.cronutils.model.CronType.QUARTZ;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing {@link Country}.
 */
@Service
@Transactional
public class CountryService {
    public static final String ENTITY_NAME = "Country";
    public static final String NOT_FOUND = ENTITY_NAME + ".001";

    private static final String UPDATE_CURRENCY_CRON_EXPRESSION = "0 0 2 * * ?";
    private static final String UPDATE_CURRENCY_CRON_DESCRIPTION = CronDescriptor.instance(Locale.UK).describe(new CronParser(CronDefinitionBuilder.instanceDefinitionFor(QUARTZ)).parse(UPDATE_CURRENCY_CRON_EXPRESSION));

    private final Logger log = LoggerFactory.getLogger(CountryService.class);

    private final CountryRepository countryRepository;
    private final CountrySearchRepository countrySearchRepository;
    private final RefetchService refetchService;
    private final TranslateService translateService;

    public CountryService(CountryRepository countryRepository, CountrySearchRepository countrySearchRepository, RefetchService refetchService, TranslateService translateService) {
        this.countryRepository = countryRepository;
        this.countrySearchRepository = countrySearchRepository;
        this.refetchService = refetchService;
        this.translateService = translateService;
    }

    /**
     * Save a country.
     *
     * @param country the entity to save.
     * @return the persisted entity.
     */
    public Country save(Country country) {
        log.debug("Request to save Country : {}", country);
        Country result = countryRepository.save(country);
        countrySearchRepository.save(result);
        refetch(result.getId());
        return result;
    }

    /**
     * Get all the countries.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Country> findAll(Pageable pageable) {
        log.debug("Request to get all Countries");
        return countryRepository.findAll(pageable);
    }

    /**
     * Get all the countries.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Iterable<Country> findAll() {
        log.debug("Request to get all Countries");
        return countryRepository.findAll();
    }


    /**
     * Get one country by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Country> findOne(Long id) {
        log.debug("Request to get Country : {}", id);
        if (id == null) return Optional.empty();
        return countryRepository.findById(id);
    }

    /**
     * Get all countries for payees.
     *
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public List<Country> findAllForPayees() {
        log.debug("Request to find all countries for payees");
        return countryRepository.findAllByForPayeeIsTrue();
    }

    /**
     * Delete the country by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Country : {}", id);
        countryRepository.deleteById(id);
        countrySearchRepository.deleteById(id);
    }

    /**
     * Search for the country corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Country> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Countries for query {}", query);
        return countrySearchRepository.search(queryStringQuery(query), pageable);
    }

    @Transactional
    public void saveAll(List<Country> countryList) {
        if (!countryList.isEmpty())
            countrySearchRepository.saveAll(countryList);
    }

    @Async
    public void refetch(Long id) {
        refetchService.refetchCountry(id);
    }

    @Async
    public void refetchAll(Collection<Bank> elements) {
        elements.forEach(e -> refetch(e.getId()));
    }

    @Async
    @Scheduled(cron = UPDATE_CURRENCY_CRON_EXPRESSION)
    public void updateCurrencyJob() throws IOException {
        Date startDate = Calendar.getInstance().getTime();
        updateCurrency();
        Date endDate = Calendar.getInstance().getTime();
        log.info("updateCurrency {} - Elapsed time: {} ms", UPDATE_CURRENCY_CRON_DESCRIPTION, endDate.getTime() - startDate.getTime());
    }

    public void updateCurrency() throws IOException {
        String url = ExchangeRateApiDTO.URL;
        HttpURLConnection httpConnection = (HttpURLConnection) new URL(url).openConnection();
        httpConnection.setRequestProperty("Accept-Charset", StandardCharsets.UTF_8.name());
        httpConnection.setRequestMethod("GET");
        int responseCode = httpConnection.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) { // success
            JsonElement root = new JsonParser().parse(new InputStreamReader((InputStream) httpConnection.getContent()));
            saveCurrencies(new ExchangeRateApiDTO(root.getAsJsonObject()));
        } else {
            log.error("ERROR requesting currencies' values {} - responseCode {}", url, responseCode);
        }
    }

    private void saveCurrencies(ExchangeRateApiDTO era) {
        era.getRates().forEach(exchangeRate -> {
            Optional<Country> optionalCountry = countryRepository.findOneByCurrencyCode(exchangeRate.getCurrencyCode());
            if (optionalCountry.isEmpty()) return;
            Country country = optionalCountry.get();
            country.setCurrencyValue(exchangeRate.getValue());
            country.setCurrencyBase(era.getBase());
            Date d = new Date(Long.parseLong(era.getTimeLastUpdated()) * 1000);
            country.setCurrencyLastUpdated(ZonedDateTime.ofInstant(d.toInstant(), ZoneId.of("GMT")));
            save(country);
        });
    }

    public Country getCountryForPayee(Long countryId) {
        log.debug("Request to find the country for payee");
        Optional<Country> countryOptional = countryRepository.findOneByIdAndForPayeeTrue(countryId);
        if (countryOptional.isEmpty())
            throw new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.country.incorrect.id") + " " + countryId + " " + translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.this.action"),
                DirectPaymentBankService.ENTITY_NAME, CountryService.NOT_FOUND);
        return countryOptional.get();
    }
}
