package com.pagomundo.web.rest;

import com.pagomundo.domain.TransactionStatusChange;
import com.pagomundo.service.TransactionStatusChangeService;
import com.pagomundo.service.TranslateService;
import com.pagomundo.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.pagomundo.domain.TransactionStatusChange}.
 */
@RestController
@RequestMapping("/api")
public class TransactionStatusChangeResource {

    private static final String ENTITY_NAME = "transactionStatusChange";
    private final Logger log = LoggerFactory.getLogger(TransactionStatusChangeResource.class);
    private final TransactionStatusChangeService transactionStatusChangeService;
    @Value("${jhipster.clientApp.name}")
    private String applicationName;
    private final TranslateService translateService;

    public TransactionStatusChangeResource(TransactionStatusChangeService transactionStatusChangeService, TranslateService translateService) {
        this.transactionStatusChangeService = transactionStatusChangeService;
        this.translateService = translateService;
    }

    /**
     * {@code POST  /transaction-status-changes} : Create a new transactionStatusChange.
     *
     * @param transactionStatusChange the transactionStatusChange to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new transactionStatusChange, or with status {@code 400 (Bad Request)} if the transactionStatusChange has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/transaction-status-changes")
    public ResponseEntity<TransactionStatusChange> createTransactionStatusChange(@Valid @RequestBody TransactionStatusChange transactionStatusChange) throws URISyntaxException {
        log.debug("REST request to save TransactionStatusChange : {}", transactionStatusChange);
        if (transactionStatusChange.getId() != null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.transaction.status.change.exist"), ENTITY_NAME, "idexists");
        }
        TransactionStatusChange result = transactionStatusChangeService.save(transactionStatusChange);
        return ResponseEntity.created(new URI("/api/transaction-status-changes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /transaction-status-changes} : Updates an existing transactionStatusChange.
     *
     * @param transactionStatusChange the transactionStatusChange to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated transactionStatusChange,
     * or with status {@code 400 (Bad Request)} if the transactionStatusChange is not valid,
     * or with status {@code 500 (Internal Server Error)} if the transactionStatusChange couldn't be updated.
     */
    @PutMapping("/transaction-status-changes")
    public ResponseEntity<TransactionStatusChange> updateTransactionStatusChange(@Valid @RequestBody TransactionStatusChange transactionStatusChange) {
        log.debug("REST request to update TransactionStatusChange : {}", transactionStatusChange);
        if (transactionStatusChange.getId() == null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.invalid.id"), ENTITY_NAME, "idnull");
        }
        TransactionStatusChange result = transactionStatusChangeService.save(transactionStatusChange);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, transactionStatusChange.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /transaction-status-changes} : get all the transactionStatusChanges.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of transactionStatusChanges in body.
     */
    @GetMapping("/transaction-status-changes")
    public ResponseEntity<List<TransactionStatusChange>> getAllTransactionStatusChanges(Pageable pageable) {
        log.debug("REST request to get a page of TransactionStatusChanges");
        Page<TransactionStatusChange> page = transactionStatusChangeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /transaction-status-changes/:id} : get the "id" transactionStatusChange.
     *
     * @param id the id of the transactionStatusChange to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the transactionStatusChange, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/transaction-status-changes/{id}")
    public ResponseEntity<TransactionStatusChange> getTransactionStatusChange(@PathVariable Long id) {
        log.debug("REST request to get TransactionStatusChange : {}", id);
        Optional<TransactionStatusChange> transactionStatusChange = transactionStatusChangeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(transactionStatusChange);
    }

    /**
     * {@code DELETE  /transaction-status-changes/:id} : delete the "id" transactionStatusChange.
     *
     * @param id the id of the transactionStatusChange to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/transaction-status-changes/{id}")
    public ResponseEntity<Void> deleteTransactionStatusChange(@PathVariable Long id) {
        log.debug("REST request to delete TransactionStatusChange : {}", id);
        transactionStatusChangeService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/transaction-status-changes?query=:query} : search for the transactionStatusChange corresponding
     * to the query.
     *
     * @param query    the query of the transactionStatusChange search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/transaction-status-changes")
    public ResponseEntity<Page<TransactionStatusChange>> searchTransactionStatusChanges(@RequestParam String query, Pageable pageable) {
        Page<TransactionStatusChange> page = transactionStatusChangeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @GetMapping("/_refetch/transaction-status-changes")
    public ResponseEntity<List<TransactionStatusChange>> refetch() {
        Page<TransactionStatusChange> page = transactionStatusChangeService.findAll(Pageable.unpaged());
        transactionStatusChangeService.saveAll(page.getContent());
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
