package com.pagomundo.web.rest;

import com.pagomundo.domain.DTO.ExtendedUserGroupDTO;
import com.pagomundo.domain.ExtendedUserGroup;
import com.pagomundo.security.AuthoritiesConstants;
import com.pagomundo.service.ExtendedUserGroupService;
import com.pagomundo.service.ExtendedUserService;
import com.pagomundo.service.NotificationReceiverService;
import com.pagomundo.service.TranslateService;
import com.pagomundo.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.pagomundo.domain.ExtendedUserGroup}.
 */
@RestController
@RequestMapping("/api")
public class ExtendedUserGroupResource {

    private static final String ENTITY_NAME = "extendedUserGroup";
    private final Logger log = LoggerFactory.getLogger(ExtendedUserGroupResource.class);
    private final ExtendedUserGroupService extendedUserGroupService;
    private final ExtendedUserService extendedUserService;
    private final NotificationReceiverService notificationReceiverService;
    private final TranslateService translateService;
    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public ExtendedUserGroupResource(ExtendedUserGroupService extendedUserGroupService, ExtendedUserService extendedUserService, NotificationReceiverService notificationReceiverService, TranslateService translateService) {
        this.extendedUserGroupService = extendedUserGroupService;
        this.extendedUserService = extendedUserService;
        this.notificationReceiverService = notificationReceiverService;
        this.translateService = translateService;
    }

    /**
     * {@code POST  /extended-user-groups} : Create a new extendedUserGroup.
     *
     * @param extendedUserGroupDTO the extendedUserGroup to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new extendedUserGroup, or with status {@code 400 (Bad Request)} if the extendedUserGroup has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/extended-user-groups")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.SUPER_ADMIN + "\")" +
        " or hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<ExtendedUserGroupDTO> createExtendedUserGroup(@RequestBody ExtendedUserGroupDTO extendedUserGroupDTO) throws URISyntaxException, IOException {
        log.debug("REST request to save ExtendedUserGroup : {}", extendedUserGroupDTO);
        if (extendedUserGroupDTO.getId() != null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.extended.user.group.exist"), ENTITY_NAME, "idexists");
        }
        if (extendedUserGroupDTO.getBank() == null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.missing.bank"), ENTITY_NAME, "MissingBank");
        }
        if (extendedUserGroupDTO.getExtendedUsers().stream().anyMatch(extendedUser -> extendedUser.getId() == null)) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.missing.extended.user"), ENTITY_NAME, "extendedUserNull");
        }
        ExtendedUserGroupDTO result = extendedUserGroupService.create(extendedUserGroupDTO);
        extendedUserService.refetchAll(result.getExtendedUsers());
        extendedUserService.refetchAll(result.getExtendedUsersWithErrors());
        notificationReceiverService.sendExtendedUserNotification(result.getExtendedUsers(), extendedUserService.getCurrentExtendedUser());

        ExtendedUserGroup eug = extendedUserGroupService.findOne(result.getId()).orElse(null);
        if (eug != null) extendedUserGroupService.exportFile(eug);

        return ResponseEntity.created(new URI("/api/extended-user-groups/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /extended-user-groups} : Updates an existing extendedUserGroup.
     *
     * @param extendedUserGroup the extendedUserGroup to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated extendedUserGroup,
     * or with status {@code 400 (Bad Request)} if the extendedUserGroup is not valid,
     * or with status {@code 500 (Internal Server Error)} if the extendedUserGroup couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/extended-user-groups")
    public ResponseEntity<ExtendedUserGroup> updateExtendedUserGroup(@RequestBody ExtendedUserGroup extendedUserGroup) throws URISyntaxException {
        log.debug("REST request to update ExtendedUserGroup : {}", extendedUserGroup);
        if (extendedUserGroup.getId() == null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.invalid.id"), ENTITY_NAME, "idnull");
        }
        ExtendedUserGroup result = extendedUserGroupService.save(extendedUserGroup);
        extendedUserService.refetchAll(result.getExtendedUsers());
        notificationReceiverService.sendExtendedUserNotification(result.getExtendedUsers(), extendedUserService.getCurrentExtendedUser());

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, extendedUserGroup.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /extended-user-groups} : get all the extendedUserGroups.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of extendedUserGroups in body.
     */
    @GetMapping("/extended-user-groups")
    public ResponseEntity<List<ExtendedUserGroup>> getAllExtendedUserGroups(Pageable pageable) {
        log.debug("REST request to get a page of ExtendedUserGroups");
        Page<ExtendedUserGroup> page = extendedUserGroupService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /extended-user-groups/:id} : get the "id" extendedUserGroup.
     *
     * @param id the id of the extendedUserGroup to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the extendedUserGroup, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/extended-user-groups/{id}")
    public ResponseEntity<ExtendedUserGroup> getExtendedUserGroup(@PathVariable Long id) {
        log.debug("REST request to get ExtendedUserGroup : {}", id);
        Optional<ExtendedUserGroup> extendedUserGroup = extendedUserGroupService.findOne(id);
        return ResponseUtil.wrapOrNotFound(extendedUserGroup);
    }

    /**
     * {@code DELETE  /extended-user-groups/:id} : delete the "id" extendedUserGroup.
     *
     * @param id the id of the extendedUserGroup to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/extended-user-groups/{id}")
    public ResponseEntity<Void> deleteExtendedUserGroup(@PathVariable Long id) {
        log.debug("REST request to delete ExtendedUserGroup : {}", id);
        extendedUserGroupService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/extended-user-groups?query=:query} : search for the extendedUserGroup corresponding
     * to the query.
     *
     * @param query the query of the extendedUserGroup search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/extended-user-groups")
    public ResponseEntity<Page<ExtendedUserGroup>> searchExtendedUserGroups(@RequestParam String query, Pageable pageable) {
        Page<ExtendedUserGroup> page = extendedUserGroupService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @GetMapping("/_refetch/extended-user-groups")
    public ResponseEntity<List<ExtendedUserGroup>> refetch() {
        Page<ExtendedUserGroup> page = extendedUserGroupService.findAll(Pageable.unpaged());
        extendedUserGroupService.saveAll(page.getContent());
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
