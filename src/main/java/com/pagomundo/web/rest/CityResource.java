package com.pagomundo.web.rest;

import com.pagomundo.domain.City;
import com.pagomundo.domain.DTO.CityByStringDTO;
import com.pagomundo.domain.DTO.CityDTO;
import com.pagomundo.service.CityService;
import com.pagomundo.service.TranslateService;
import com.pagomundo.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.pagomundo.domain.City}.
 */
@RestController
@RequestMapping("/api")
public class CityResource {

    private static final String ENTITY_NAME = "city";
    private final Logger log = LoggerFactory.getLogger(CityResource.class);
    private final CityService cityService;
    @Value("${jhipster.clientApp.name}")
    private String applicationName;
    private final TranslateService translateService;

    public CityResource(CityService cityService, TranslateService translateService) {
        this.cityService = cityService;
        this.translateService = translateService;
    }

    /**
     * {@code POST  /cities} : Create a new city.
     *
     * @param city the city to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new city, or with status {@code 400 (Bad Request)} if the city has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/cities")
    public ResponseEntity<City> createCity(@Valid @RequestBody City city) throws URISyntaxException {
        log.debug("REST request to save City : {}", city);
        if (city.getId() != null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.city.exist"), ENTITY_NAME, "idexists");
        }
        City result = cityService.save(city);
        cityService.refetch(result.getId());

        return ResponseEntity.created(new URI("/api/cities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /cities} : Updates an existing city.
     *
     * @param city the city to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated city,
     * or with status {@code 400 (Bad Request)} if the city is not valid,
     * or with status {@code 500 (Internal Server Error)} if the city couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/cities")
    public ResponseEntity<City> updateCity(@Valid @RequestBody City city) throws URISyntaxException {
        log.debug("REST request to update City : {}", city);
        if (city.getId() == null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.invalid.id"), ENTITY_NAME, "idnull");
        }
        City result = cityService.save(city);
        cityService.refetch(result.getId());

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, city.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /cities} : get all the cities.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of cities in body.
     */
    @GetMapping("/cities")
    public ResponseEntity<List<City>> getAllCities(Pageable pageable) {
        log.debug("REST request to get a page of Cities");
        Page<City> page = cityService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /cities/:id} : get the "id" city.
     *
     * @param id the id of the city to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the city, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/cities/{id}")
    public ResponseEntity<City> getCity(@PathVariable Long id) {
        log.debug("REST request to get City : {}", id);
        Optional<City> city = cityService.findOne(id);
        return ResponseUtil.wrapOrNotFound(city);
    }

    /**
     * {@code DELETE  /cities/:id} : delete the "id" city.
     *
     * @param id the id of the city to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/cities/{id}")
    public ResponseEntity<Void> deleteCity(@PathVariable Long id) {
        log.debug("REST request to delete City : {}", id);
        cityService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/cities?query=:query} : search for the city corresponding
     * to the query.
     *
     * @param query    the query of the city search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/cities")
    public ResponseEntity<Page<City>> searchCities(@RequestParam String query, Pageable pageable) {
        Page<City> page = cityService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    /**
     * {@code SEARCH  /_search/cities?query=:query} : search for the city corresponding
     * to the query.
     *
     * @param query    the query of the city search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @PostMapping("/cities/citiesbycountry")
    public ResponseEntity<List<City>> citiesbycountry(@RequestBody CityByStringDTO citydto) throws URISyntaxException {
        
        log.debug("REST request to citiesbycountry : {}", citydto);

        if (citydto.getCountryId() == null ) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.invalid.id"), ENTITY_NAME, "idnull");
        }

        if (citydto.getSubStr() == null || citydto.getSubStr().equals("") || citydto.getSubStr().length() < 3 ) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.invalid.id"), ENTITY_NAME, "idnull");
        }

        List<City> result = cityService.findAllBySubString(citydto.getSubStr(), citydto.getCountryId());
        
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, ""))
            .body(result);


    }


    @GetMapping("/_refetch/cities")
    public ResponseEntity<List<City>> refetch() {
        Page<City> page = cityService.findAll(Pageable.unpaged());
        cityService.saveAll(page.getContent());
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * 
     * @param idContry
     * @return
     */
     @GetMapping("/cities/bycountry")
     public ResponseEntity<List<City>> getAllCitiesByCoutryId(@RequestParam Long idCountry) {
         log.info("REST request to get getAllCitiesByCoutryId");
         List<City> result = cityService.findAllByCountry(idCountry);
         return ResponseEntity.ok()
                 .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, ""))
                 .body(result);
     }
    
}
