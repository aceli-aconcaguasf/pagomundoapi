package com.pagomundo.web.rest.integrations.stp;

import com.pagomundo.domain.enumeration.ErrorCode;
import com.pagomundo.domain.enumeration.stp.StpAccountType;
import com.pagomundo.domain.enumeration.stp.StpBankId;
import com.pagomundo.service.dto.integrations.stp.IntegrationResponseStpDTO;
import com.pagomundo.service.dto.integrations.stp.IntegrationStpDTO;
import com.pagomundo.service.integrations.stp.IntegrationStpService;
import com.pagomundo.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * REST controller for managing STP integration
 */
@RestController
@RequestMapping("/api")
public class IntegrationStpResource {

    private static final String ENTITY_NAME = "integrationStp";
    private final Logger log = LoggerFactory.getLogger(IntegrationStpResource.class);
    private final IntegrationStpService integrationStpService;
    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public IntegrationStpResource(IntegrationStpService integrationStpService) {
        this.integrationStpService = integrationStpService;
    }

    /**
     * {@code POST  /integration-stp} : Create a new request to STP provider.
     *
     * @param integrationStpDTO the integrationStp request.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new integrationStpDTO, or with status {@code 400 (Bad Request)} if the integrationStpDTO has any error.
     */
    @PostMapping("/integration-stp")
    @ApiOperation(value = "Request a transfer by STP integration", notes = "Used for pay from a PMI account to another")
    public ResponseEntity<IntegrationStpDTO> createIntegrationStpRequest(@RequestBody IntegrationStpDTO integrationStpDTO) {
        log.debug("REST request to STP : {}", integrationStpDTO);
        IntegrationStpDTO result = null;
        try {
            result = integrationStpService.create(integrationStpDTO);
            return ResponseEntity.created(new URI("/api/integration-stp/" + result.getStpId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getStpId()))
                .body(integrationStpDTO);
        } catch (Exception e) {
            throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, ErrorCode.ERROR_UNDEFINED.getValue().toString());
        }

    }

    @GetMapping("/integration-stp-bank-id-list")
    @ApiOperation(value = "Request all STP BankId", notes = "Used for PMI transfers")
    public ResponseEntity<HashMap<String, List<String>>> getAllStpBankId() {
        log.debug("REST get all bank id");
        HashMap<String, List<String>> bankIdList = new HashMap<>();
        for (StpBankId bankId : StpBankId.values()) {
            List<String> details = new ArrayList<>();
            details.add(bankId.getValue());
            details.add(bankId.getDescription());
            bankIdList.put(bankId.name(), details);
        }

        return ResponseEntity.ok().headers(HeaderUtil.createAlert(applicationName, "Response ok", "")).body(bankIdList);
    }

    @GetMapping("/integration-stp-account-type-list")
    @ApiOperation(value = "Request all STP Account Type", notes = "Used for PMI transfers")
    public ResponseEntity<HashMap<String, List<String>>> getAllStpAccountType() {
        log.debug("REST get all account type");
        HashMap<String, List<String>> accountTypeList = new HashMap<>();
        for (StpAccountType accountType : StpAccountType.values()) {
            List<String> details = new ArrayList<>();
            details.add(accountType.getValue());
            details.add(accountType.getDescription());
            accountTypeList.put(accountType.name(), details);
        }

        return ResponseEntity.ok().headers(HeaderUtil.createAlert(applicationName, "Response ok", "")).body(accountTypeList);
    }

    /**
     * {@code POST  /integration-stp} : Receive responses from STP provider.
     *
     * @param genericResponse the stp response.
     * @return the {@link ResponseEntity} with status {@code 200 (Ok)}, or with status {@code 400 (Bad Request)} if the response has any error.
     */
    @PostMapping("/integration-stp/responses")
    @ApiOperation(value = "Response a transfer by STP integration", notes = "Used for receive from STP")
    public ResponseEntity<String> receiveResponse(@RequestBody IntegrationResponseStpDTO genericResponse) {
        log.debug("REST response from STP : {}", genericResponse);

        String response = integrationStpService.receiveResponse(genericResponse);
        return ResponseEntity.ok().body(response);
    }
}
