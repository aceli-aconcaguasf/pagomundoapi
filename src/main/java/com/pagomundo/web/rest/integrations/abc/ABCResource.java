package com.pagomundo.web.rest.integrations.abc;

import com.pagomundo.domain.DTO.abc.RequestDTO;
import com.pagomundo.service.integrations.abc.DCMService;
import com.pagomundo.service.integrations.abc.FintechService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.github.jhipster.web.util.HeaderUtil;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/api/bank")
public class ABCResource {
    private static final String ENTITY_NAME = "abc";
    private final Logger log = LoggerFactory.getLogger(ABCResource.class);
    
    private final DCMService dcmClient;
    private final FintechService fintechClient;
    
    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public ABCResource(DCMService dcmClient, FintechService fintechClient) {
        this.dcmClient = dcmClient;
        this.fintechClient = fintechClient;
    }

    @PostMapping("/dcm/verify-account")
    public ResponseEntity<List<String>> verifyCUC(@RequestBody RequestDTO request) throws URISyntaxException {
        log.debug("REST request to verify ABC bank cuc: {}", request.getCuc());
        List<String> response = dcmClient.existsCuc(request.getCuc());

        return ResponseEntity.created(new URI("/api/bank/dcm/verify-account"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, ENTITY_NAME))
            .body(response);
    }

    @PostMapping("/dcm/new-client")
    public ResponseEntity<List<String>> newClient(@RequestBody RequestDTO request) throws URISyntaxException {
        List<String> response = dcmClient.nuevoCliente(request.getCuc(), request.getRfc(), request.getName(), request.getBusinessGroup());

        return ResponseEntity.created(new URI("/api/bank/dcm/new-client"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, ENTITY_NAME))
            .body(response);
    }

    @PostMapping("/dcm/insert-line-cuc")
    public ResponseEntity<List<String>> insertLineAndCuc(@RequestBody RequestDTO request) throws URISyntaxException {
        List<String> response = dcmClient.insertlineandcuc(request.getUser(), request.getCuc());

        return ResponseEntity.created(new URI("/api/bank/dcm/insert-line-cuc"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, ENTITY_NAME))
            .body(response);
    }

    @PostMapping("/dcm/verify-line-cuc")
    public ResponseEntity<List<String>> verfifyLineAndCuc(@RequestBody RequestDTO request) throws URISyntaxException {
        List<String> response = dcmClient.existsLineAndCuc(request.getCuc());

        return ResponseEntity.created(new URI("/api/bank/dcm/verify-line-cuc"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, ENTITY_NAME))
            .body(response);
    }

    @PostMapping("/dcm/register-doc")
    public ResponseEntity<List<String>> registerDocument(@RequestBody RequestDTO request) throws URISyntaxException {
        List<String> response = dcmClient.subirregistrodedoc(request.getCuc(), request.getDocType(), request.getUser());

        return ResponseEntity.created(new URI("/api/bank/dcm/register-doc"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, ENTITY_NAME))
            .body(response);
    }

    @PostMapping("/dcm/register-doc-meta")
    public ResponseEntity<List<String>> registerDocumentMetaDatum(@RequestBody RequestDTO request) throws URISyntaxException {
        List<String> response = dcmClient.subirdocumentometafield(request.getDocId(), request.getDesc(), request.getValue());

        return ResponseEntity.created(new URI("/api/bank/dcm/register-doc-meta"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, ENTITY_NAME))
            .body(response);
    }

    @PostMapping("/dcm/upload-doc")
    public ResponseEntity<List<String>> uploadDocument(@RequestBody RequestDTO request) throws URISyntaxException {
        List<String> response = dcmClient.recibeDoc(request.getByteFile(), request.getDocId().toString(), request.getUser(), request.getCuc());

        return ResponseEntity.created(new URI("/api/bank/dcm/upload-doc"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, ENTITY_NAME))
            .body(response);
    }

    // FINTECH SERVICES

    @PostMapping("/fintech/register-client")
    public ResponseEntity<String> registerClient(@RequestBody String jsonin) throws URISyntaxException {
        String response = fintechClient.registroCliente(jsonin);

        return ResponseEntity.created(new URI("/api/bank/fintech/register-client"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, ENTITY_NAME))
            .body(response);
    }

    @PostMapping("/fintech/account-signup")
    public ResponseEntity<String> accountSignup(@RequestBody String jsonin) throws URISyntaxException {
        String response = fintechClient.altaCuenta(jsonin);

        return ResponseEntity.created(new URI("/api/bank/fintech/account-signup"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, ENTITY_NAME))
            .body(response);
    }


}