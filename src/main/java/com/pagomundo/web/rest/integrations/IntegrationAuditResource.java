package com.pagomundo.web.rest.integrations;

import com.pagomundo.service.TranslateService;
import com.pagomundo.service.dto.integrations.IntegrationAuditDTO;
import com.pagomundo.service.integrations.IntegrationAuditService;
import com.pagomundo.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.pagomundo.domain.IntegrationAudit}.
 */
@RestController
@RequestMapping("/api")
public class IntegrationAuditResource {

    private static final String ENTITY_NAME = "integrationAudit";
    private final Logger log = LoggerFactory.getLogger(IntegrationAuditResource.class);
    private final IntegrationAuditService integrationAuditService;
    private final TranslateService translateService;
    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public IntegrationAuditResource(IntegrationAuditService integrationAuditService, TranslateService translateService) {
        this.integrationAuditService = integrationAuditService;
        this.translateService = translateService;
    }

    /**
     * {@code POST  /integration-audits} : Create a new integrationAudit.
     *
     * @param integrationAuditDTO the integrationAuditDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new integrationAuditDTO, or with status {@code 400 (Bad Request)} if the integrationAudit has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/integration-audits")
    public ResponseEntity<IntegrationAuditDTO> createIntegrationAudit(@RequestBody IntegrationAuditDTO integrationAuditDTO) throws URISyntaxException {
        log.debug("REST request to save IntegrationAudit : {}", integrationAuditDTO);
        if (integrationAuditDTO.getId() != null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.new.integration.audit.can.not.already.id"), ENTITY_NAME, "idexists");
        }
        IntegrationAuditDTO result = integrationAuditService.save(integrationAuditDTO);
        return ResponseEntity.created(new URI("/api/integration-audits/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /integration-audits} : Updates an existing integrationAudit.
     *
     * @param integrationAuditDTO the integrationAuditDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated integrationAuditDTO,
     * or with status {@code 400 (Bad Request)} if the integrationAuditDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the integrationAuditDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/integration-audits")
    public ResponseEntity<IntegrationAuditDTO> updateIntegrationAudit(@RequestBody IntegrationAuditDTO integrationAuditDTO) throws URISyntaxException {
        log.debug("REST request to update IntegrationAudit : {}", integrationAuditDTO);
        if (integrationAuditDTO.getId() == null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.invalid.id"), ENTITY_NAME, "idnull");
        }
        IntegrationAuditDTO result = integrationAuditService.save(integrationAuditDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, integrationAuditDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /integration-audits} : get all the integrationAudits.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of integrationAudits in body.
     */
    @GetMapping("/integration-audits")
    public ResponseEntity<List<IntegrationAuditDTO>> getAllIntegrationAudits(Pageable pageable) {
        log.debug("REST request to get a page of IntegrationAudits");
        Page<IntegrationAuditDTO> page = integrationAuditService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /integration-audits/:id} : get the "id" integrationAudit.
     *
     * @param id the id of the integrationAuditDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the integrationAuditDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/integration-audits/{id}")
    public ResponseEntity<IntegrationAuditDTO> getIntegrationAudit(@PathVariable Long id) {
        log.debug("REST request to get IntegrationAudit : {}", id);
        Optional<IntegrationAuditDTO> integrationAuditDTO = integrationAuditService.findOne(id);
        return ResponseUtil.wrapOrNotFound(integrationAuditDTO);
    }

    /**
     * {@code DELETE  /integration-audits/:id} : delete the "id" integrationAudit.
     *
     * @param id the id of the integrationAuditDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/integration-audits/{id}")
    public ResponseEntity<Void> deleteIntegrationAudit(@PathVariable Long id) {
        log.debug("REST request to delete IntegrationAudit : {}", id);
        integrationAuditService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/integration-audits?query=:query} : search for the integrationAudit corresponding
     * to the query.
     *
     * @param query the query of the integrationAudit search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/integration-audits")
    public ResponseEntity<List<IntegrationAuditDTO>> searchIntegrationAudits(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of IntegrationAudits for query {}", query);
        Page<IntegrationAuditDTO> page = integrationAuditService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
