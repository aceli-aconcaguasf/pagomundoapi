package com.pagomundo.web.rest.integrations;

import com.pagomundo.service.TranslateService;
import com.pagomundo.service.dto.integrations.IntegrationAuditExtraResponseDTO;
import com.pagomundo.service.integrations.IntegrationAuditExtraResponseService;
import com.pagomundo.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.pagomundo.domain.IntegrationAuditExtraResponse}.
 */
@RestController
@RequestMapping("/api")
public class IntegrationAuditExtraResponseResource {

    private static final String ENTITY_NAME = "integrationAuditExtraResponse";
    private final Logger log = LoggerFactory.getLogger(IntegrationAuditExtraResponseResource.class);
    private final IntegrationAuditExtraResponseService integrationAuditExtraResponseService;
    private final TranslateService translateService;
    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public IntegrationAuditExtraResponseResource(IntegrationAuditExtraResponseService integrationAuditExtraResponseService, TranslateService translateService) {
        this.integrationAuditExtraResponseService = integrationAuditExtraResponseService;
        this.translateService = translateService;
    }

    /**
     * {@code POST  /integration-audit-extra-responses} : Create a new integrationAuditExtraResponse.
     *
     * @param integrationAuditExtraResponseDTO the integrationAuditExtraResponseDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new integrationAuditExtraResponseDTO, or with status {@code 400 (Bad Request)} if the integrationAuditExtraResponse has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/integration-audit-extra-responses")
    public ResponseEntity<IntegrationAuditExtraResponseDTO> createIntegrationAuditExtraResponse(@RequestBody IntegrationAuditExtraResponseDTO integrationAuditExtraResponseDTO) throws URISyntaxException {
        log.debug("REST request to save IntegrationAuditExtraResponse : {}", integrationAuditExtraResponseDTO);
        if (integrationAuditExtraResponseDTO.getId() != null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.new.integration.audit.extra.response.can.not.already.id"), ENTITY_NAME, "idexists");
        }
        IntegrationAuditExtraResponseDTO result = integrationAuditExtraResponseService.save(integrationAuditExtraResponseDTO);
        return ResponseEntity.created(new URI("/api/integration-audit-extra-responses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /integration-audit-extra-responses} : Updates an existing integrationAuditExtraResponse.
     *
     * @param integrationAuditExtraResponseDTO the integrationAuditExtraResponseDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated integrationAuditExtraResponseDTO,
     * or with status {@code 400 (Bad Request)} if the integrationAuditExtraResponseDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the integrationAuditExtraResponseDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/integration-audit-extra-responses")
    public ResponseEntity<IntegrationAuditExtraResponseDTO> updateIntegrationAuditExtraResponse(@RequestBody IntegrationAuditExtraResponseDTO integrationAuditExtraResponseDTO) throws URISyntaxException {
        log.debug("REST request to update IntegrationAuditExtraResponse : {}", integrationAuditExtraResponseDTO);
        if (integrationAuditExtraResponseDTO.getId() == null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.invalid.id"), ENTITY_NAME, "idnull");
        }
        IntegrationAuditExtraResponseDTO result = integrationAuditExtraResponseService.save(integrationAuditExtraResponseDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, integrationAuditExtraResponseDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /integration-audit-extra-responses} : get all the integrationAuditExtraResponses.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of integrationAuditExtraResponses in body.
     */
    @GetMapping("/integration-audit-extra-responses")
    public ResponseEntity<List<IntegrationAuditExtraResponseDTO>> getAllIntegrationAuditExtraResponses(Pageable pageable) {
        log.debug("REST request to get a page of IntegrationAuditExtraResponses");
        Page<IntegrationAuditExtraResponseDTO> page = integrationAuditExtraResponseService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /integration-audit-extra-responses/:id} : get the "id" integrationAuditExtraResponse.
     *
     * @param id the id of the integrationAuditExtraResponseDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the integrationAuditExtraResponseDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/integration-audit-extra-responses/{id}")
    public ResponseEntity<IntegrationAuditExtraResponseDTO> getIntegrationAuditExtraResponse(@PathVariable Long id) {
        log.debug("REST request to get IntegrationAuditExtraResponse : {}", id);
        Optional<IntegrationAuditExtraResponseDTO> integrationAuditExtraResponseDTO = integrationAuditExtraResponseService.findOne(id);
        return ResponseUtil.wrapOrNotFound(integrationAuditExtraResponseDTO);
    }

    /**
     * {@code DELETE  /integration-audit-extra-responses/:id} : delete the "id" integrationAuditExtraResponse.
     *
     * @param id the id of the integrationAuditExtraResponseDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/integration-audit-extra-responses/{id}")
    public ResponseEntity<Void> deleteIntegrationAuditExtraResponse(@PathVariable Long id) {
        log.debug("REST request to delete IntegrationAuditExtraResponse : {}", id);
        integrationAuditExtraResponseService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/integration-audit-extra-responses?query=:query} : search for the integrationAuditExtraResponse corresponding
     * to the query.
     *
     * @param query the query of the integrationAuditExtraResponse search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/integration-audit-extra-responses")
    public ResponseEntity<List<IntegrationAuditExtraResponseDTO>> searchIntegrationAuditExtraResponses(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of IntegrationAuditExtraResponses for query {}", query);
        Page<IntegrationAuditExtraResponseDTO> page = integrationAuditExtraResponseService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
