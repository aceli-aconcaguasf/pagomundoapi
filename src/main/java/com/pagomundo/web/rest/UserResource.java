package com.pagomundo.web.rest;

import com.pagomundo.config.Constants;
import com.pagomundo.domain.*;
import com.pagomundo.domain.DTO.*;
import com.pagomundo.domain.DTO.Kushki.KushkiResponseDTO;
import com.pagomundo.domain.enumeration.*;
import com.pagomundo.repository.UserRepository;
import com.pagomundo.repository.search.UserSearchRepository;
import com.pagomundo.security.AuthoritiesConstants;
import com.pagomundo.service.*;
import com.pagomundo.service.dto.UserDTO;
import com.pagomundo.web.rest.errors.BadRequestAlertException;
import com.pagomundo.web.rest.errors.EmailAlreadyUsedException;
import com.pagomundo.web.rest.errors.LoginAlreadyUsedException;
import com.pagomundo.web.rest.vm.KeyAndPasswordVM;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.*;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * REST controller for managing users.
 * <p>
 * This class accesses the {@link User} entity, and needs to fetch its collection of authorities.
 * <p>
 * For a normal use-case, it would be better to have an eager relationship between User and Authority,
 * and send everything to the client side: there would be no View Model and DTO, a lot less code, and an outer-join
 * which would be good for performance.
 * <p>
 * We use a View Model and a DTO for 3 reasons:
 * <ul>
 * <li>We want to keep a lazy association between the user and the authorities, because people will
 * quite often do relationships with the user, and we don't want them to get the authorities all
 * the time for nothing (for performance reasons). This is the #1 goal: we should not impact our users'
 * application because of this use-case.</li>
 * <li> Not having an outer join causes n+1 requests to the database. This is not a real issue as
 * we have by default a second-level cache. This means on the first HTTP call we do the n+1 requests,
 * but then all authorities come from the cache, so in fact it's much better than doing an outer join
 * (which will get lots of data from the database, for each HTTP call).</li>
 * <li> As this manages users, for security reasons, we'd rather have a DTO layer.</li>
 * </ul>
 * <p>
 * Another option would be to have a specific JPA entity graph to handle this case.
 */
@RestController
public class UserResource {
    private final Logger log = LoggerFactory.getLogger(UserResource.class);
    private final UserService userService;
    private final ExtendedUserService extendedUserService;
    private final CountryService countryService;
    private final BankService bankService;
    private final CityService cityService;
    private final IdTypeService idTypeService;
    private final UserRepository userRepository;
    private final UserSearchRepository userSearchRepository;
    private final NotificationReceiverService notificationReceiverService;
    private final ExtendedUserRelationService extendedUserRelationService;
    private final ExtendedUserFundsService extendedUserFundsService;
    private final TranslateService translateService;
    private final TransactionService transactionService;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    @Value("${kushki}")
    private String kushki;

    public UserResource(
        ExtendedUserFundsService extendedUserFundsService,
        UserService userService,
        ExtendedUserService extendedUserService,
        CountryService countryService,
        BankService bankService,
        CityService cityService,
        IdTypeService idTypeService,
        UserRepository userRepository,
        UserSearchRepository userSearchRepository,
        NotificationReceiverService notificationReceiverService,
        TranslateService translateService,
        ExtendedUserRelationService extendedUserRelationService,
        TransactionService transactionService) {
        this.extendedUserFundsService = extendedUserFundsService;
        this.userService = userService;
        this.extendedUserService = extendedUserService;
        this.countryService = countryService;
        this.bankService = bankService;
        this.cityService = cityService;
        this.idTypeService = idTypeService;
        this.userRepository = userRepository;
        this.userSearchRepository = userSearchRepository;
        this.notificationReceiverService = notificationReceiverService;
        this.translateService = translateService;
        this.extendedUserRelationService = extendedUserRelationService;
        this.transactionService = transactionService;
    }

    /**
     * {@code PUT /users} : Updates an existing User.
     *
     * @param userDTO the user to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated user.
     * @throws EmailAlreadyUsedException {@code 400 (Bad Request)} if the email is already in use.
     * @throws LoginAlreadyUsedException {@code 400 (Bad Request)} if the login is already in use.
     */
    @PutMapping("/api/users")
    @Secured({AuthoritiesConstants.SUPER_ADMIN, AuthoritiesConstants.ADMIN})
    public ResponseEntity<UserDTO> updateUser(@Valid @RequestBody UserDTO userDTO) {
        log.debug("REST request to update User : {}", userDTO);
        Optional<User> existingUser;

        existingUser = userRepository.findOneByEmailIgnoreCase(userDTO.getEmail());
        if (existingUser.isPresent() && (!existingUser.get().getId().equals(userDTO.getId()))) {
            throw new EmailAlreadyUsedException(userDTO.getEmail());
        }
        existingUser = userRepository.findOneByLogin(userDTO.getLogin().toLowerCase());
        if (existingUser.isPresent() && (!existingUser.get().getId().equals(userDTO.getId()))) {
            throw new LoginAlreadyUsedException(userDTO.getLogin());
        }
        Optional<UserDTO> updatedUser = userService.updateUser(userDTO);

        return ResponseUtil.wrapOrNotFound(updatedUser,
            HeaderUtil.createAlert(applicationName, "userManagement.updated", userDTO.getLogin()));
    }

    /**
     * {@code PUT /users/reset-roles} : Updates an existing User.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated user.
     */
    @PutMapping("/api/users/reset-roles")
    @Secured({AuthoritiesConstants.SUPER_ADMIN})
    public ResponseEntity<Void> resetRole() {
        userRepository.findAll().forEach(user -> {
            ExtendedUser eu = extendedUserService.getByUserId(user.getId());
            if (user.getId() <= 5 || eu == null) return;
            user.setRole(eu.getRole());
            userService.save(user);
        });
        return ResponseEntity.noContent().headers(HeaderUtil.createAlert(applicationName, "userManagement.updated", null)).build();
    }

    /**
     * {@code GET /users} : get all users.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body all users.
     */
    @GetMapping("/api/users")
    public ResponseEntity<List<UserDTO>> getAllUsers(Pageable pageable) {
        final Page<UserDTO> page = userService.getAllManagedUsers(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * Gets a list of all roles.
     *
     * @return a string list of all roles.
     */
    @GetMapping("/users/authorities")
    @Secured({AuthoritiesConstants.SUPER_ADMIN, AuthoritiesConstants.ADMIN})
    public List<String> getAuthorities() {
        return userService.getAuthorities();
    }

    /**
     * {@code GET /users/:login} : get the "login" user.
     *
     * @param login the login of the user to find.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the "login" user, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/users/{login:" + Constants.LOGIN_REGEX + "}")
    public ResponseEntity<UserDTO> getUser(@PathVariable String login) {
        log.debug("REST request to get User : {}", login);
        return ResponseUtil.wrapOrNotFound(
            userService.getUserWithAuthoritiesByLogin(login)
                .map(UserDTO::new));
    }

    /**
     * {@code DELETE /users/:login} : delete the "login" User.
     *
     * @param login the login of the user to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/api/users/{login:" + Constants.LOGIN_REGEX + "}")
    @Secured({AuthoritiesConstants.SUPER_ADMIN, AuthoritiesConstants.ADMIN})
    public ResponseEntity<Void> deleteUser(@PathVariable String login) {
        log.debug("REST request to delete User: {}", login);
        userService.deleteUser(login);
        return ResponseEntity.noContent().headers(HeaderUtil.createAlert(applicationName, "userManagement.deleted", login)).build();
    }

    /**
     * {@code SEARCH /_search/users/:query} : search for the User corresponding to the query.
     *
     * @param query the query to search.
     * @return the result of the search.
     */
    @GetMapping("/api/_search/users/{query}")
    public List<User> search(@PathVariable String query) {
        return StreamSupport
            .stream(userSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

    /**
     * {@code POST  /api/_multi/users} : Create users.
     *
     * @param userDTOList the users to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dose, or with status {@code 400 (Bad Request)} if the user has already an ID.
     */
    @PostMapping("/api/_multi/users")
    @Secured({AuthoritiesConstants.SUPER_ADMIN, AuthoritiesConstants.ADMIN, AuthoritiesConstants.MERCHANT, AuthoritiesConstants.RESELLER})
    public ResponseEntity<UserListDTO> createMultiUser(@RequestBody UserListDTO userDTOList) throws URISyntaxException {
        log.info(">>>>>>>>>>>>>>>>>>  /api/_multi/users");
        userDTOList.getUsers().forEach(userDTO ->{
            if(userDTO.getNickname().length()>255)
                throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.nickname.exceeds.length"),
                    UserService.ENTITY_NAME, UserService.BAD_REQUEST);
            if(userDTO.getEmail().length()>100)
                throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.email.exceeds.length"),
                    UserService.ENTITY_NAME, UserService.BAD_REQUEST);
        });

        UserListDTO result = createMultiUserProcess(userDTOList);

        log.info(">>>>>>>>>>>>>>>>>>  after createMultiUserProcess and sending response");

        return ResponseEntity.created(new URI("/api/_multi/users/" + ""))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, UserService.ENTITY_NAME, null))
            .body(result);
    }

    /**
     * {@code POST  /v1/payees/payee} : Create users.
     *
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dose, or with status {@code 400 (Bad Request)} if the user has already an ID.
     */
    @PostMapping("/v1/payees/payee")
    public ResponseEntity<PayeeResultDTO> createUnifiedPayee(@RequestBody PayeeForTransactionDTO payeeDto) throws URISyntaxException {
        log.info("Entra ENDPOINT PAYEE");
        //log.debug("URL Kushki"+url);
        log.info(" DTOPYEE"+payeeDto.toString());
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime myDateObj = LocalDateTime.now();
        log.info("DTOPYEE: "+payeeDto.toString());

        ExtendedUser extendedUserForTransaction;

        //modificacion del amount
        String merchantId=payeeDto.getMerchantId();
        BigDecimal fee=payeeDto.getFee();
        BigDecimal beforeFee= new BigDecimal(payeeDto.getAmount().getSubtotalIva0().toString());
        BigDecimal amountPlusFee= new BigDecimal(String.valueOf(beforeFee.add(payeeDto.getFee())));


        //verifica si la longitud de la cuenta clabe es correcta
        log.info("Bank account"+payeeDto.getPayee().getBankAccountNumber().length());
        if (payeeDto.getPayee().getBankAccountNumber().length() != 18)
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.request.exception.payee.incorrect.bankAccount.length") + " " + translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.this.action"),
                UserService.ENTITY_NAME, ExtendedUserService.INCORRECT_STATUS);


        //creamos pago por kushki
        String url=kushki;
        PayeeResultDTO payeeResult= KushkiService(url,merchantId,payeeDto,amountPlusFee);
        if(payeeResult.getTicketNumber() == null || payeeResult.getTransactionReference() == null){
            return ResponseEntity.badRequest()
                .headers(HeaderUtil.createAlert(applicationName,"BAD REQUEST",null))
                .body(payeeResult);
        }

        //extramos la informacion del payee
        log.info(payeeDto.toString());
        UserDTO user=payeeDto.getPayee();
        String name=user.getAlias();
        String account=user.getBankAccountNumber();

        String [] names=name.split(" ");
        user.setFirstName(names[0]);
        user.setLastName(names[1]);
        user.setNickname(user.getEmail());

        //valida si el usuario existe
        Optional<User> existingUser;
        existingUser = userRepository.findOneByEmailIgnoreCase(user.getEmail());
        if (!existingUser.isPresent()) {
            //creamos el usuario
            UserListDTO saveUser =new UserListDTO();
            List <UserDTO> usersList = new ArrayList<>();
            usersList.add(user);
            saveUser.setUsers(usersList);
            saveUser.setUsersRole(AuthoritiesConstants.PAYEE);
            log.info(saveUser.toString());
            UserListDTO result = createSinglePayeeProcess(saveUser);
            String resetKey=user.getResetKey(); //obtenemos resetKey para la creacion de la contraseña

            log.info("Resultado".concat(result.toString()));
            String resultado = "{ TransactionResult :"+ payeeDto.getTransactionResult()+"}";


            //creamos la contraseña del usuario
            KeyAndPasswordVM defaultPassword = new KeyAndPasswordVM();
            defaultPassword.setNewPassword("Qwerty123");
            defaultPassword.setKey(resetKey);
            Optional<User> userpassword = userService.completePasswordReset(defaultPassword.getNewPassword(), defaultPassword.getKey());

            //creamos la informacion adicional del usuario
            ExtendedUser extendedUser =new ExtendedUser();
            extendedUser.setId(user.getId());
            User completeUser = user.getUser();
            extendedUser.setUser(completeUser);
            extendedUser.setIdNumber("AAAA000000BBB");
            extendedUser.setPhoneNumber("1122334455");
            extendedUser.setPostalAddress("Default");
            extendedUser.setMaritalStatus(MaritalStatus.SINGLE);
            extendedUser.gender(Gender.MALE);
            extendedUser.setCardNumber("0");
            City residenceCity = new City();
            residenceCity.setId(1121L);
            Country postalCountry=new Country();
            postalCountry.setId(2L);
            extendedUser.setResidenceCity(residenceCity);
            extendedUser.setPostalCountry(postalCountry);
            ZonedDateTime dateTime = ZonedDateTime.now();
            extendedUser.setBirthDate(dateTime);
            extendedUser.setUseDirectPayment(true);
            extendedUser.setBankAccountNumber(account);
            extendedUser.setFullName(name);
            log.info("Creacion del Extended User"+extendedUser.toString());
            extendedUserForTransaction = extendedUserService.create(extendedUser);
        }else{
            extendedUserForTransaction=extendedUserService.getExtendedUser(existingUser.get().getId()).orElse(null);
        }

        //fondeo de la cuenta merchant
        ExtendedUserFunds euf=new ExtendedUserFunds();
        euf.setExtendedUser(extendedUserService.getCurrentExtendedUser());
        euf.setValue(payeeDto.getAmount().getSubtotalIva0());
        euf.setType(FundType.FUNDING);
        ExtendedUserFunds result = extendedUserFundsService.request(euf);

        Transaction transactionResult =createPayeeTransaction(extendedUserForTransaction,payeeDto,fee,payeeResult);

        return ResponseEntity.created(new URI("/v1/payees/payee" + ""))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, UserService.ENTITY_NAME, null))
            .body(payeeResult);
    }

    /**
     * {@code POST  /v2/payees/payee} : Create users.
     *
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dose, or with status {@code 400 (Bad Request)} if the user has already an ID.
     */
    @PostMapping("/v2/payees/payee")
    public ResponseEntity<PayeeResultDTO> createUnifiedPayeeV2(@RequestBody PayeeForTransactionDTOV2 payeeDto) throws URISyntaxException {
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime myDateObj = LocalDateTime.now();
        log.info("DTOPYEE V2: "+payeeDto.toString());

        ExtendedUser extendedUserForTransaction;

        //verifica si la descipcion del pago ya existe
        TransactionListDTO  transactionDescription=transactionService.findAllByDescription(payeeDto.getTransactionId());
        log.info("result: "+transactionDescription.toString());
        if (!transactionDescription.getTransactions().isEmpty())
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.request.exception.transaction.description.alreadyExists") + " " + translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.this.action"),
                UserService.ENTITY_NAME, ExtendedUserService.INCORRECT_STATUS);

        //verifica si la longitud de la cuenta clabe es correcta
        log.info("Bank account"+payeeDto.getPayee().getBankAccountNumber().length());
        if (payeeDto.getPayee().getBankAccountNumber().length() != 18)
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.request.exception.payee.incorrect.bankAccount.length") + " " + translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.this.action"),
                UserService.ENTITY_NAME, ExtendedUserService.INCORRECT_STATUS);

        //extramos la informacion del payee
        log.info(payeeDto.toString());
        UserDTO user=payeeDto.getPayee();
        String name=user.getAlias();
        String account=user.getBankAccountNumber();

        int index = name.indexOf("  ");
        while(index > -1){
            name = name.replaceFirst("  ", " ");
            index = name.indexOf("  ");
        }

        String [] names=name.split(" ");
        user.setFirstName(names[0]);

        String lastnameAccum = "";
        int lastnameLength = names.length;
        if(lastnameLength > 1){
            for(int i = 1; i < lastnameLength; i++){
                lastnameAccum = lastnameAccum + " " + names[i];
            }
        }

        user.setLastName(lastnameAccum);
        user.setNickname(user.getEmail());

        //valida si el usuario existe
        Optional<User> existingUser;
        existingUser = userRepository.findOneByEmailIgnoreCase(user.getEmail());
        if (!existingUser.isPresent()) {
            //creamos el usuario
            UserListDTO saveUser =new UserListDTO();
            List <UserDTO> usersList = new ArrayList<>();
            usersList.add(user);
            saveUser.setUsers(usersList);
            saveUser.setUsersRole(AuthoritiesConstants.PAYEE);
            log.info(saveUser.toString());
            UserListDTO result = createSinglePayeeProcess(saveUser);
            String resetKey=user.getResetKey(); //obtenemos resetKey para la creacion de la contraseña

            log.info("Resultado".concat(result.toString()));

            //creamos la contraseña del usuario
            KeyAndPasswordVM defaultPassword = new KeyAndPasswordVM();
            defaultPassword.setNewPassword("Qwerty123");
            defaultPassword.setKey(resetKey);
            Optional<User> userpassword = userService.completePasswordReset(defaultPassword.getNewPassword(), defaultPassword.getKey());

            //creamos la informacion adicional del usuario
            ExtendedUser extendedUser =new ExtendedUser();
            extendedUser.setId(user.getId());
            User completeUser = user.getUser();
            extendedUser.setUser(completeUser);
            extendedUser.setIdNumber("AAAA000000BBB");
            extendedUser.setPhoneNumber("1122334455");
            extendedUser.setPostalAddress("Default");
            extendedUser.setMaritalStatus(MaritalStatus.SINGLE);
            extendedUser.gender(Gender.MALE);
            extendedUser.setCardNumber("0");
            City residenceCity = new City();
            residenceCity.setId(1121L);
            Country postalCountry=new Country();
            postalCountry.setId(2L);
            extendedUser.setResidenceCity(residenceCity);
            extendedUser.setPostalCountry(postalCountry);
            ZonedDateTime dateTime = ZonedDateTime.now();
            extendedUser.setBirthDate(dateTime);
            extendedUser.setUseDirectPayment(true);
            extendedUser.setBankAccountNumber(account);
            extendedUser.setFullName(name);
            log.info("Creacion del Extended User"+extendedUser.toString());
            extendedUserForTransaction = extendedUserService.create(extendedUser);
        }else{
            extendedUserForTransaction=extendedUserService.getExtendedUser(existingUser.get().getId()).orElse(null);
        }

        //fondeo de la cuenta merchant
        /*ExtendedUserFunds euf=new ExtendedUserFunds();
        euf.setExtendedUser(extendedUserService.getCurrentExtendedUser());
        euf.setValue(payeeDto.getAmount());
        euf.setType(FundType.FUNDING);
        ExtendedUserFunds result = extendedUserFundsService.request(euf);*/

        PayeeResultDTO payeeResult=new PayeeResultDTO();
        payeeResult.setCurrency("MXN");
        payeeResult.setAmount(payeeDto.getAmount());
        payeeResult.setType("dispersion");
        payeeResult.setDate(myDateObj.format(myFormatObj));

        Transaction transactionResult =createPayeeTransactionNoFee(extendedUserForTransaction,payeeDto, payeeResult);

        return ResponseEntity.created(new URI("/v2/payees/payee" + ""))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, UserService.ENTITY_NAME, null))
            .body(payeeResult);
    }


    /**
     * {@code POST  /v1/payees} : Create payees.
     *
     * @param userDTOList the users to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dose, or with status {@code 400 (Bad Request)} if the user has already an ID.
     */
    @PostMapping("/v1/payees")
    @Secured({AuthoritiesConstants.MERCHANT, AuthoritiesConstants.RESELLER})
    public ResponseEntity<UserListForMerchantDTO> v1CreateMultiPayees(@RequestBody UserListDTO userDTOList) throws URISyntaxException {

        log.info(">>>>>>>>>>>>>>>>>>  /v1/payees");

        ExtendedUser eu = extendedUserService.getCurrentExtendedUser();
        log.info(">>>>>>>>>>>>>>>>>>  ExtendedUser email {}", eu.getEmail());

        log.info(">>>>>>>>>>>>>>>>>>  userDTOList lista {}", userDTOList.getUsers());


        if (!Status.ACCEPTED.equals(eu.getStatus()))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.incorrect.status") + " " + eu.getStatus() + " " + translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.this.action"),
                UserService.ENTITY_NAME, ExtendedUserService.INCORRECT_STATUS);
        log.info(">>>>>>>>>>>>>>>>>> after ACCEPTED estatus");

        log.info(">>>>>>>>>>>>>>>>>>  Status.ACCEPTED ");

        if (userDTOList.getCountry() != null && userDTOList.getCountry().getId() != null &&
            countryService.findAllForPayees().stream().noneMatch(country -> country.getId().equals(userDTOList.getCountry().getId())))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.missing.wrong.country"),
                UserService.ENTITY_NAME, CountryService.NOT_FOUND);


        log.info(">>>>>>>>>>>>>>>>>> after ACCEPTED Country");

        userDTOList.setUsersRole(AuthoritiesConstants.PAYEE);

        log.info(">>>>>>>>>>>>>>>>>> after set PAYEE role to the list of payees");

        if (AuthoritiesConstants.MERCHANT.equalsIgnoreCase(eu.getRole())) {

            log.info(">>>>>>>>>>>>>>>>>>  set MERCHANT to Payee");

            userDTOList.setToWhom(eu);

            log.info(">>>>>>>>>>>>>>>>>> after set mercahnt to user list");

        } else {

            log.info(">>>>>>>>>>>>>>>>>> else MERCHANT ");



            if (userDTOList.getToWhom() == null || userDTOList.getToWhom().getId() == null)
                throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.property.missing"),
                    UserService.ENTITY_NAME, UserService.MISSING_INFO);

            log.info(">>>>>>>>>>>>>>>>>> after ACCEPTED estatus");

            Optional<ExtendedUser> toWhomOptional = extendedUserService.findOne(userDTOList.getToWhom().getId());

            log.info(">>>>>>>>>>>>>>>>>>  after toWhomOptional");

            if (toWhomOptional.isEmpty())
                throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.id.not.valid"),
                    UserService.ENTITY_NAME, UserService.MISSING_INFO);


            log.info(">>>>>>>>>>>>>>>>>> after toWhomOptional.isEmpty()");

            if (!AuthoritiesConstants.MERCHANT.equalsIgnoreCase(toWhomOptional.get().getRole()))
                throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.role.not.valid"),
                    UserService.ENTITY_NAME, UserService.MISSING_INFO);


            log.info(">>>>>>>>>>>>>>>>>> after AuthoritiesConstants.MERCHANT");

        }
        UserListForMerchantDTO result;
        try {
             result = new UserListForMerchantDTO(createMultiUserProcess(userDTOList));
        }
        catch(Exception e) {
        	 log.error("Error al guardar list Payees v1/payees: {}",e.getMessage());
        	 log.info("Error al guardar list Payees v1/payees: {}" ,e.getLocalizedMessage());
        	 throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.account.user.email.regular.expression") + Constants.LOGIN_REGEX + "'", UserService.ENTITY_NAME, "Required");
        }

        log.info(">>>>>>>>>>>>>>>>>> after createMultiUserProcess");

        log.info(">>>>>>>>>>>>>>>>>> Send response with mail: {}", eu.getEmail());

        // return ResponseEntity.created(new URI("/v1/users" + ""))
        //     .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, UserService.ENTITY_NAME, null))
        //     .body(result);

        return ResponseEntity.created(new URI("/v1/payees" + ""))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, UserService.ENTITY_NAME, null))
            .body(result);
    }

    /**
     * {@code POST  /v1/merchants} : Create merchants.
     *
     * @param userDTOList the merchants to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dose, or with status {@code 400 (Bad Request)} if the user has already an ID.
     */
    @PostMapping("/v1/merchants")
    @Secured({AuthoritiesConstants.RESELLER})
    public ResponseEntity<UserListForMerchantDTO> v1CreateMultiMerchant(@RequestBody UserListDTO userDTOList) throws URISyntaxException {
        ExtendedUser eu = extendedUserService.getCurrentExtendedUser();
        if (!Status.ACCEPTED.equals(eu.getStatus()))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.incorrect.status") + " " + eu.getStatus() + " " + translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.this.action"),
                UserService.ENTITY_NAME, ExtendedUserService.INCORRECT_STATUS);

        userDTOList.setToWhom(eu);
        userDTOList.setUsersRole(AuthoritiesConstants.MERCHANT);

        UserListForMerchantDTO result = new UserListForMerchantDTO(createMultiUserProcess(userDTOList));

        return ResponseEntity.created(new URI("/v1/merchants" + ""))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, UserService.ENTITY_NAME, null))
            .body(result);
    }

    /**
     * {@code POST  /api/users/{userId}/langKey/{langKey} : Modify language by user
     *
     * @param userId id of the user to modify
     * @param langKey langKey update
     */
    @PostMapping(path = "/api/users/{userId}/langKey/{langKey}")
    public void changeUserLanguage(@PathVariable Long userId, @PathVariable String langKey) {
        log.debug("REST request to change language of given user : {}", userId);
        userService.updateLangKeyByUser(userId, langKey);
        log.debug("New language set to given user_id");
        log.debug("Refetching user");
        userService.refetch(userId);
        log.debug("User refetched");
    }

    private UserListDTO createPayeePreFiller(UserListDTO payee){
        log.debug("REST request to create basic payee info", payee);
        return null;
    }

    private UserListDTO createSinglePayeeProcess(UserListDTO user){
        log.debug("REST request to invite a group of users : {}", user);
        validateUserList(user.getUsers()); // valida la lista de usuarios ligados

        ExtendedUser toWhom = getToWhom(user.getUsersRole(), user.getToWhom());

        validateRoleToCreateAUser(user.getUsersRole(), toWhom);
        user.setToWhom(toWhom);

        log.debug("Country List userDTO: {}", user.getCountry());

        user.setCountry(user.getCountry() != null ?
            countryService.findOne(user.getCountry().getId()).orElse(null) : null);

        validateUserList(user);

        log.debug("===__====> createSinglePayeeProcess validated user {}", user);        

        UserListDTO result = userService.create(user);

        log.debug("===__====> createSinglePayeeProcess created user {}", result);

        return cleanResponse(result);
    }

    private UserListDTO createMultiUserProcess(UserListDTO userDTOList) {


        log.info(">>>>>>>>>>>>>>>>>> inside of createMultiUserProcess function");

        log.info(">>>>>>>>>>>>>>>>>>  REST request to invite a group of users : {}", userDTOList);

        validateUserList(userDTOList.getUsers());


        ExtendedUser toWhom = getToWhom(userDTOList.getUsersRole(), userDTOList.getToWhom());

        log.info(">>>>>>>>>>>>>>>>>>  toWhom : {}", toWhom);

        validateRoleToCreateAUser(userDTOList.getUsersRole(), toWhom);
        userDTOList.setToWhom(toWhom);

        log.debug("Country List userDTO: {}", userDTOList.getCountry());

        userDTOList.setCountry(userDTOList.getCountry() != null ?
            countryService.findOne(userDTOList.getCountry().getId()).orElse(null) : null);

        log.info(">>>>>>>>>>>>>>>>>> after setCountry");

        log.info(">>>>>>>>>>>>>>>>>>Country List userDTO: {}", userDTOList.getCountry());

        validateUserList(userDTOList);

        log.info(">>>>>>>>>>>>>>>>>> after validateUserList");

        UserListDTO result = userService.create(userDTOList);

        log.info(">>>>>>>>>>>>>>>>>> after userService.create");

        List<ExtendedUser> euCreated = extendedUserService.createExtendedUserList(result.getUsersCreatedAndAssociated());

        log.info(">>>>>>>>>>>>>>>>>> after createExtendedUserList");

        notificationReceiverService.sendMultiUserNotification(result.getUsersCreatedAndAssociated(), extendedUserService.getCurrentExtendedUser());

        log.info(">>>>>>>>>>>>>>>>>> after sendMultiUserNotification");

        notificationReceiverService.sendExtendedUserNotification(euCreated, extendedUserService.getCurrentExtendedUser());

        log.info(">>>>>>>>>>>>>>>>>> after sendExtendedUserNotification");

        for(UserDTO udto: result.getUsersCreatedAndAssociated()){
            log.debug("=====__======> createMultiUserProcess DTOUser ID: {}", udto.getId());
        }

        return cleanResponse(result);
    }

    private void validateUserList(UserListDTO userListDTO) {

        log.info(">>>>>>>>>>>>>>>>>> inside of validateUserList UserListDTO function");

        List<UserDTO> userListWithErrors = new ArrayList<>();
        userListDTO.getUsers().forEach(userDTO -> {
            userDTO.setRole(userListDTO.getUsersRole());
            userDTO.setToWhom(userListDTO.getToWhom());
            userDTO.setCountry(userListDTO.getCountry());
            validate(userDTO);
            if (Status.CANCELLED.equals(userDTO.getStatus())) userListWithErrors.add(userDTO);
        });
        List<UserDTO> userListWithOutErrors = userListDTO.getUsers();
        userListWithOutErrors.removeAll(userListWithErrors);
        userListDTO.setUsers(userListWithOutErrors);
        userListDTO.setUserNotCreated(userListWithErrors);
    }

    private void validate(UserDTO userDTO) {
        userDTO.setStatusReason(null);
        if (userDTO.getEmail() == null || "".equalsIgnoreCase(userDTO.getEmail()) || !userDTO.getEmail().matches(Constants.LOGIN_REGEX)) {
            userDTO.setStatusReason("email can't be null or void or should match to a valid email.");
            userDTO.setStatus(Status.CANCELLED);
            return;
        }

        if (userDTO.getFirstName() == null || "".equalsIgnoreCase(userDTO.getFirstName()) ||
            userDTO.getLastName() == null) {
            userDTO.setStatusReason("firstName or lastName can't be null or void.");
            userDTO.setStatus(Status.CANCELLED);
            return;
        }
        if (userDTO.isPayeeAsCompany() == null) {
            userDTO.setStatusReason("payeeAsCompany should be true or false.");
            userDTO.setStatus(Status.CANCELLED);
            return;
        }
        if (userDTO.getCountry() != null && userDTO.getCountry().getId() != null &&
            userDTO.getBankAccountNumber() != null && !"".equalsIgnoreCase(userDTO.getBankAccountNumber())) {
        	//consultar si la cuenta ya esta asociada a otro usuario
        	log.info("account number: {}", userDTO.getBankAccountNumber());
        	List<ExtendedUser> accountNumbers=extendedUserService.findAllByBankAccountNumber( userDTO.getBankAccountNumber());
        	if(!accountNumbers.isEmpty()) {
        		log.info("accounts numbers found: {} find:{}", accountNumbers.size(),accountNumbers.toArray());
        		userDTO.setStatusReason("the account number already exists by another user");
                userDTO.setStatus(Status.CANCELLED);
                return;
        	}
        	
            switch (userDTO.getCountry().getId().intValue()) {
                case 1: // COLOMBIA
                    if (userDTO.getBankAccountType() == null) {
                        userDTO.setStatusReason("bankAccountType should not be null.");
                        userDTO.setStatus(Status.CANCELLED);
                        return;
                    }
                    if (userDTO.getDirectPaymentBank() != null && userDTO.getDirectPaymentBank().getId() != null) {
                        bankService.findOne(userDTO.getDirectPaymentBank().getId()).ifPresentOrElse(bank -> {
                            if (!bank.getCountry().getId().equals(userDTO.getCountry().getId()))
                                userDTO.setStatusReason("directPaymentBank doesn't belong to countryId: " + userDTO.getCountry().getId() + ".");
                        }, () -> userDTO.setStatusReason("directPaymentBank is not valid."));
                    } else userDTO.setStatusReason("directPaymentBank should not be null or void.");
                    if (userDTO.getStatusReason() != null && !"".equalsIgnoreCase(userDTO.getStatusReason())) {
                        userDTO.setStatus(Status.CANCELLED);
                        return;
                    }
                    if (userDTO.getDirectPaymentCity() != null && userDTO.getDirectPaymentCity().getId() != null) {
                        cityService.findOne(userDTO.getDirectPaymentCity().getId()).ifPresentOrElse(city -> {
                            if (!city.getCountry().getId().equals(userDTO.getCountry().getId()))
                                userDTO.setStatusReason("directPaymentCity doesn't belong to countryId: " + userDTO.getCountry().getId() + ".");
                        }, () -> userDTO.setStatusReason("directPaymentCity is not valid."));
                    } else userDTO.setStatusReason("directPaymentCity should not be null or void.");
                    if (userDTO.getStatusReason() != null && !"".equalsIgnoreCase(userDTO.getStatusReason())) {
                        userDTO.setStatus(Status.CANCELLED);
                        return;
                    }
                    if (userDTO.isPayeeAsCompany()) {
                        if (userDTO.getTaxId() == null) {
                            userDTO.setStatusReason("taxId should not be null.");
                            userDTO.setStatus(Status.CANCELLED);
                            return;
                        }
                        validateIdTypeTaxId(userDTO);
                    }
                    if (userDTO.getStatusReason() != null && !"".equalsIgnoreCase(userDTO.getStatusReason())) {
                        userDTO.setStatus(Status.CANCELLED);
                        return;
                    }
                    if (!userDTO.isPayeeAsCompany()) {
                        if (userDTO.getIdNumber() == null) {
                            userDTO.setStatusReason("idNumber should not be null.");
                            userDTO.setStatus(Status.CANCELLED);
                            return;
                        }
                        validateIdType(userDTO);
                    }
                    if (userDTO.getStatusReason() != null && !"".equalsIgnoreCase(userDTO.getStatusReason())) {
                        userDTO.setStatus(Status.CANCELLED);
                        return;
                    }
                    break;
                case 2: // MEXICO
                    userDTO.setDirectPaymentBank(null);
                    userDTO.setDirectPaymentCity(null);
                    userDTO.setBankAccountType(null);
                    userDTO.setIdTypeTaxId(null);
                    userDTO.setTaxId(null);
                    userDTO.setIdType(null);
                    userDTO.setIdNumber(null);
                    if (!StringUtils.isNumeric(userDTO.getBankAccountNumber()) ||
                        userDTO.getBankAccountNumber().length() != 18) {
                        userDTO.setStatusReason("bankAccountNumber should have 18 digits.");
                        userDTO.setStatus(Status.CANCELLED);
                        return;
                    }
                    break;
                case 66: // ARGENTINA
                    userDTO.setDirectPaymentBank(null);
                    userDTO.setDirectPaymentCity(null);
                    userDTO.setBankAccountType(null);
                    if (!StringUtils.isNumeric(userDTO.getBankAccountNumber()) ||
                        userDTO.getBankAccountNumber().length() != 22) {
                        userDTO.setStatusReason("bankAccountNumber should have 22 digits.");
                        userDTO.setStatus(Status.CANCELLED);
                        return;
                    }
                    if (userDTO.isPayeeAsCompany()) {
                        if (userDTO.getTaxId() == null ||
                            !StringUtils.isNumeric(userDTO.getTaxId()) ||
                            userDTO.getTaxId().length() != 11 ) {

                            userDTO.setStatusReason("taxId should not be null or should have 11 digits..");
                            userDTO.setStatus(Status.CANCELLED);
                            return;
                        }
                        validateIdTypeTaxId(userDTO);
                    }
                    if (userDTO.getStatusReason() != null && !"".equalsIgnoreCase(userDTO.getStatusReason())) {
                        userDTO.setStatus(Status.CANCELLED);
                        return;
                    }
                    if (!userDTO.isPayeeAsCompany()) {
                        if (userDTO.getIdNumber() == null) {
                            userDTO.setStatusReason("idNumber should not be null.");
                            userDTO.setStatus(Status.CANCELLED);
                            return;
                        }
                        validateIdType(userDTO);
                    }
                    if (userDTO.getStatusReason() != null && !"".equalsIgnoreCase(userDTO.getStatusReason())) {
                        userDTO.setStatus(Status.CANCELLED);
                        return;
                    }
                    break;
                case 74:  // Brasil
                    if (userDTO.getBankBranch() == null || userDTO.getBankBranch().length() < 4 || userDTO.getBankBranch().length() > 7) {
                        userDTO.setStatus(Status.CANCELLED);
                        return;
                    }
                    if (userDTO.getDirectPaymentBank() != null && userDTO.getDirectPaymentBank().getId() != null) {
                        bankService.findOne(userDTO.getDirectPaymentBank().getId()).ifPresentOrElse(bank -> {
                            if (!bank.getCountry().getId().equals(userDTO.getCountry().getId()))
                                userDTO.setStatusReason("directPaymentBank doesn't belong to countryId: " + userDTO.getCountry().getId() + ".");
                        }, () -> userDTO.setStatusReason("directPaymentBank is not valid."));
                    } else userDTO.setStatusReason("directPaymentBank should not be null or void.");
                    if (userDTO.getBankAccountType() == null) {
                        userDTO.setStatusReason("bankAccountType should not be null .");
                        userDTO.setStatus(Status.CANCELLED);
                        return;
                    }


                    if (userDTO.getBankAccountNumber() == null || !StringUtils.isNumeric(userDTO.getBankAccountNumber())) {
                        userDTO.setStatusReason("bankAccountNumber should be a number.");
                        userDTO.setStatus(Status.CANCELLED);
                        return;
                    }


                    // Validaciones para length cuenta banco
                    if (userDTO.getBankAccountNumber().length() < 5 || userDTO.getBankAccountNumber().length() > 15) {
                        userDTO.setStatusReason("bankAccountNumber must have more than 4 digits and less than 15.");
                        userDTO.setStatus(Status.CANCELLED);
                        return;
                    }


                    if (userDTO.isPayeeAsCompany()) {

                        if (userDTO.getTaxId() == null ||
                            !StringUtils.isNumeric(userDTO.getTaxId()) ||
                            userDTO.getTaxId().length() != 14) {
                            userDTO.setStatusReason("taxId should not be null or should have 14 digits for companies..");
                            userDTO.setStatus(Status.CANCELLED);
                            return;
                        }
                        validateIdTypeTaxId(userDTO);

                    } else {


                        if (userDTO.getIdNumber() == null ||
                            !StringUtils.isNumeric(userDTO.getIdNumber()) ||
                            userDTO.getIdNumber().length() != 11) {
                            userDTO.setStatusReason("idNumber should not be null or should have 11 digits for persons..");
                            userDTO.setStatus(Status.CANCELLED);
                            return;
                        }
                        validateIdType(userDTO);

                    }
                    if (userDTO.getStatusReason() != null && !"".equalsIgnoreCase(userDTO.getStatusReason())) {
                        userDTO.setStatus(Status.CANCELLED);
                        return;
                    }
                    break;
                default:
                    break;
            }
        }
        userDTO.setStatus(userDTO.getStatusReason() != null ? Status.CANCELLED : null);
    }

    private void validateIdTypeTaxId(UserDTO userDTO) {
        if (userDTO.getIdTypeTaxId() != null && userDTO.getIdTypeTaxId().getId() != null) {
            idTypeService.findOne(userDTO.getIdTypeTaxId().getId()).ifPresentOrElse(idType -> {
                if (!idType.getCountry().getId().equals(userDTO.getCountry().getId()) ||
                    NaturalOrLegal.NATURAL.equals(idType.getNaturalOrLegal()) ||
                    NaturalOrLegal.BOTH.equals(idType.getNaturalOrLegal()))
                    userDTO.setStatusReason("idTypeTaxId: " + userDTO.getIdTypeTaxId().getId() + " is not valid " +
                        "for countryId: " + userDTO.getCountry().getId() + ".");
            }, () -> userDTO.setStatusReason("idTypeTaxId: " + userDTO.getIdTypeTaxId().getId() + " is not valid."));
        } else userDTO.setStatusReason("idTypeTaxId should not be null or void.");
    }

    private void validateIdType(UserDTO userDTO) {
        if (userDTO.getIdType() != null && userDTO.getIdType().getId() != null) {
            idTypeService.findOne(userDTO.getIdType().getId()).ifPresentOrElse(idType -> {
                if (!idType.getCountry().getId().equals(userDTO.getCountry().getId()) ||
                    NaturalOrLegal.LEGAL.equals(idType.getNaturalOrLegal()) ||
                    NaturalOrLegal.BOTH.equals(idType.getNaturalOrLegal()))
                    userDTO.setStatusReason("idType: " + userDTO.getIdTypeTaxId().getId() + " is not valid " +
                        "for countryId: " + userDTO.getCountry().getId() + ".");
            }, () -> userDTO.setStatusReason("idType: " + userDTO.getIdTypeTaxId().getId() + " is not valid."));
        } else userDTO.setStatusReason("idType should not be null or void.");
    }

    private UserDTO cleanResponseForAdmin(UserDTO userDTO) {
        userDTO.setBankAccountNumber(null);
        userDTO.setBankAccountType(null);
        userDTO.setDirectPaymentBank(null);
        userDTO.setDirectPaymentCity(null);
        userDTO.setPayeeAsCompany(null);
        userDTO.setTaxId(null);
        userDTO.setIdNumber(null);
        userDTO.setMobileNumber(null);
        userDTO.setBirthDate(null);
        userDTO.setCompany(null);
        userDTO.setPostalAddress(null);
        userDTO.setResidenceAddress(null);
        userDTO.setPostalCountry(null);
        userDTO.setResidenceCountry(null);

        return userDTO;
    }

    private UserListDTO cleanResponse(UserListDTO response) {
        response.setUsers(null);
        switch (response.getUsersRole()) {
            case AuthoritiesConstants.SUPER_ADMIN:
            case AuthoritiesConstants.ADMIN:
                response.setCountry(null);
                response.setToWhom(null);
                response.getUsersCreatedAndAssociated().forEach(userDTO -> userDTO = cleanResponseForAdmin(userDTO));
                response.getUserNotCreated().forEach(userDTO -> userDTO = cleanResponseForAdmin(userDTO));
                response.getUserExistingAndAssociated().forEach(userDTO -> userDTO = cleanResponseForAdmin(userDTO));
                response.getUserExistingAndNotAssociated().forEach(userDTO -> userDTO = cleanResponseForAdmin(userDTO));
                break;
            case AuthoritiesConstants.RESELLER:
            case AuthoritiesConstants.MERCHANT:
            case AuthoritiesConstants.PAYEE:
            default:
                break;
        }
        return response;
    }

    private ExtendedUser getToWhom(String role, ExtendedUser toWhom) {
        switch (role) {
            case AuthoritiesConstants.SUPER_ADMIN:
            case AuthoritiesConstants.ADMIN:
            case AuthoritiesConstants.RESELLER:
                return extendedUserService.getCurrentExtendedUser();
            case AuthoritiesConstants.MERCHANT:
            case AuthoritiesConstants.PAYEE:
            default:
                return toWhom != null ?
                    extendedUserService.findOne(toWhom.getId()).orElse(extendedUserService.getCurrentExtendedUser()) :
                    extendedUserService.getCurrentExtendedUser();
        }
    }

    private void validateUserList(List<UserDTO> userList) {
        if (userList == null || (userList.isEmpty()))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.not.create"), UserService.ENTITY_NAME, "empty");
        int upLimit = 100;
        if (userList.size() > upLimit)
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.limit.create"),
                UserService.ENTITY_NAME, UserService.CREATION_LIMIT_EXCEEDED);
        if (userList.stream().anyMatch(user -> user.getId() != null))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.exist"),
                UserService.ENTITY_NAME, UserService.SHOULD_BE_NULL_CODE);
    }

    private void validateRoleToCreateAUser(String role, ExtendedUser toWhom) {
        if (!toWhom.getStatus().equals(Status.ACCEPTED))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.whom.incorrect.status") + " " + toWhom.getStatus(),
                UserService.ENTITY_NAME, ExtendedUserService.NOT_FOUND);

        if (role == null)
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.role.missing"),
                UserService.ENTITY_NAME, UserService.WRONG_AUTHORITY);
        if (role.equals(AuthoritiesConstants.SUPER_ADMIN) &&
            !userService.isSuperAdmin(toWhom.getUser()))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.not.create.super.admin"),
                UserService.ENTITY_NAME, UserService.WRONG_AUTHORITY);
        if (role.equals(AuthoritiesConstants.ADMIN) &&
            !userService.isSuperAdmin(toWhom.getUser()))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.not.create.admin"),
                UserService.ENTITY_NAME, UserService.WRONG_AUTHORITY);
        if (role.equals(AuthoritiesConstants.RESELLER) &&
            !userService.isAdminOrSuperAdmin(toWhom.getUser()))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.not.create.super.merchant"),
                UserService.ENTITY_NAME, UserService.WRONG_AUTHORITY);
        if (role.equals(AuthoritiesConstants.MERCHANT) &&
            !(userService.isAdminOrSuperAdmin(toWhom.getUser()) || userService.isReseller(toWhom.getUser())))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.not.create.super.merchant"),
                UserService.ENTITY_NAME, UserService.WRONG_AUTHORITY);
        if (role.equals(AuthoritiesConstants.PAYEE) &&
            !(userService.isAdminOrSuperAdmin(toWhom.getUser()) || userService.isReseller(toWhom.getUser()) || userService.isMerchant(toWhom.getUser()))) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.not.create.super.payee"),
                UserService.ENTITY_NAME, UserService.WRONG_AUTHORITY);
        }
    }

    private PayeeResultDTO KushkiService (String url,String merchantId,PayeeForTransactionDTO payeeDto, BigDecimal amountPlusFee){
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime myDateObj = LocalDateTime.now();
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
        @Override
        public boolean hasError(HttpStatus statusCode) {
            return false;
        }});
        KushkiDTO kushkiRequest =new KushkiDTO();
        PayeeResultDTO payeeResult=new PayeeResultDTO();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Private-Merchant-Id", merchantId);
        KushkiResponseDTO kushkiResult=new KushkiResponseDTO();
        //String uri="https://api-uat.kushkipagos.com/card/v1/charges";
        try {
            URI endpoint =new URI(url.concat("/card/v1/charges"));
            kushkiRequest.setFullResponse(true);
            kushkiRequest.setToken(payeeDto.getCardToken());
            kushkiRequest.setAmount(payeeDto.getAmount());
            kushkiRequest.setContactDetails(payeeDto.getContactDetails());
            kushkiRequest.setProductDetails(payeeDto.getProductDetails());
            kushkiRequest.setOrderDetails(payeeDto.getOrderDetails());
            kushkiRequest.getAmount().setSubtotalIva0(amountPlusFee);
            log.info("Kushki Requst"+kushkiRequest.toString());
            HttpEntity<KushkiDTO> httpEntity = new HttpEntity<>(kushkiRequest, headers);
            log.info("REQUEST: "+httpEntity.toString());
            log.info("URL: "+endpoint.toString());
            ResponseEntity<KushkiResponseDTO> serviceResult =restTemplate.postForEntity(endpoint,httpEntity,KushkiResponseDTO.class);
            kushkiResult=serviceResult.getBody();
            log.info("Charge HTTP CODE: "+serviceResult.getStatusCode().toString());
            if(!serviceResult.getStatusCode().equals(HttpStatus.CREATED)) {
                throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.exist"), "USER", "id");
                //return null;
            }
        }catch(Exception e){
            log.info("ERROR: "+e.toString());
        }

        if (kushkiResult.getTicketNumber() == null || kushkiResult.getTransactionReference() == null){
            payeeResult.setMessage(kushkiResult.getMessage());
        }else{
            payeeResult.setCurrency(kushkiRequest.getAmount().getCurrency());
            payeeResult.setAmount(kushkiRequest.getAmount().getSubtotalIva0());
            payeeResult.setType("charge");
            payeeResult.setTransactionReference(kushkiResult.getTransactionReference());
            payeeResult.setTicketNumber(kushkiResult.getTicketNumber());
            payeeResult.setDate(myDateObj.format(myFormatObj));
        }

        return payeeResult;
    }

    private Transaction createPayeeTransaction (ExtendedUser extendedUser,PayeeForTransactionDTO payeeDto,BigDecimal fee, PayeeResultDTO payeeResult){
        TransactionListDTO transactionList = new TransactionListDTO();
        Set<Transaction> transactions=new HashSet<Transaction>();
        Transaction payeeTransaction=new Transaction();
        extendedUser.setStatus(Status.ACCEPTED);
        payeeTransaction.setPayee(extendedUser);
        payeeTransaction.setDescription(payeeResult.getTicketNumber()+"|"+payeeResult.getTransactionReference()+"|"+payeeDto.getPaymentIntentId()+"|"+payeeDto.getMerchantId());
        BigDecimal amountAfter=(payeeDto.getAmount().getSubtotalIva0().subtract(fee));
        payeeTransaction.setAmountBeforeCommission(payeeDto.getAmount().getSubtotalIva0());
        payeeTransaction.setAmountAfterCommission(amountAfter);
        log.info("Transaction:" + payeeTransaction.toString());
        transactions.add(payeeTransaction);
        log.info("SET Transaction "+payeeTransaction);
        transactionList.setTransactions(transactions);
        log.info("TransactionList "+transactionList.toString());
        Transaction transactionResult = transactionService.create(payeeTransaction);
        log.info("transactionResult "+transactionResult.toString());
        return transactionResult;
    }

    private Transaction createPayeeTransactionNoFee (ExtendedUser extendedUser, PayeeForTransactionDTOV2 payeeDto, PayeeResultDTO payeeResult){
        TransactionListDTO transactionList = new TransactionListDTO();
        Set<Transaction> transactions=new HashSet<Transaction>();
        Transaction payeeTransaction=new Transaction();
        extendedUser.setStatus(Status.ACCEPTED);
        payeeTransaction.setPayee(extendedUser);
        payeeTransaction.setDescription(payeeDto.getTransactionId());
        BigDecimal amountAfter=payeeDto.getAmount();
        payeeTransaction.setAmountBeforeCommission(payeeDto.getAmount());
        payeeTransaction.setAmountAfterCommission(amountAfter);
        log.info("Transaction:" + payeeTransaction.toString());
        transactions.add(payeeTransaction);
        log.info("SET Transaction "+payeeTransaction);
        transactionList.setTransactions(transactions);
        log.info("TransactionList "+transactionList.toString());
        Transaction transactionResult = transactionService.create(payeeTransaction);
        log.info("transactionResult "+transactionResult.toString());
        return transactionResult;
    }
}
