package com.pagomundo.web.rest;

import com.pagomundo.domain.DTO.NotificationReceiverDTO;
import com.pagomundo.domain.DTO.QueryDTO;
import com.pagomundo.domain.Notification;
import com.pagomundo.domain.NotificationReceiver;
import com.pagomundo.service.NotificationReceiverService;
import com.pagomundo.service.TranslateService;
import com.pagomundo.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.pagomundo.domain.NotificationReceiver}.
 */
@RestController
public class NotificationReceiverResource {

    private static final String ENTITY_NAME = "notificationReceiver";
    private final Logger log = LoggerFactory.getLogger(NotificationReceiverResource.class);
    private final NotificationReceiverService notificationReceiverService;
    @Value("${jhipster.clientApp.name}")
    private String applicationName;
    private final TranslateService translateService;

    public NotificationReceiverResource(NotificationReceiverService notificationReceiverService, TranslateService translateService) {
        this.notificationReceiverService = notificationReceiverService;
        this.translateService = translateService;
    }

    @GetMapping("/api/_refetch/notification-receivers/{id}")
    public ResponseEntity<List<NotificationReceiver>> refetchId(@PathVariable Long id) {
        notificationReceiverService.refetch(id);
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    /**
     * {@code POST  /notification-receivers} : Create a new notificationReceiver.
     *
     * @param notificationReceiverDTO the notificationReceiver to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new notificationReceiver, or with status {@code 400 (Bad Request)} if the notificationReceiver has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/api/notification-receivers")
    public ResponseEntity<Notification> createNotificationReceiver(@RequestBody NotificationReceiverDTO notificationReceiverDTO) throws URISyntaxException {
        log.debug("REST request to save NotificationReceiver : {}", notificationReceiverDTO);

        Notification result = notificationReceiverService.create(notificationReceiverDTO);
        if (result == null)
            return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, "")).build();

        return ResponseEntity.created(new URI("/api/notification-receivers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /notification-receivers} : Updates an existing notificationReceiver.
     *
     * @param notificationReceiver the notificationReceiver to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated notificationReceiver,
     * or with status {@code 400 (Bad Request)} if the notificationReceiver is not valid,
     * or with status {@code 500 (Internal Server Error)} if the notificationReceiver couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/api/notification-receivers")
    public ResponseEntity<NotificationReceiver> updateNotificationReceiver(@RequestBody NotificationReceiver notificationReceiver) throws URISyntaxException {
        log.debug("REST request to update NotificationReceiver : {}", notificationReceiver);
        if (notificationReceiver.getId() == null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.invalid.id"), ENTITY_NAME, "idnull");
        }
        NotificationReceiver result = notificationReceiverService.update(notificationReceiver);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, notificationReceiver.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /notification-receivers} : get all the notificationReceivers.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of notificationReceivers in body.
     */
    @GetMapping("/api/notification-receivers")
    public ResponseEntity<List<NotificationReceiver>> getAllNotificationReceivers(Pageable pageable) {
        log.debug("REST request to get a page of NotificationReceivers");
        Page<NotificationReceiver> page = notificationReceiverService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /notification-receivers/:id} : get the "id" notificationReceiver.
     *
     * @param id the id of the notificationReceiver to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the notificationReceiver, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/api/notification-receivers/{id}")
    public ResponseEntity<NotificationReceiver> getNotificationReceiver(@PathVariable Long id) {
        log.debug("REST request to get NotificationReceiver : {}", id);
        Optional<NotificationReceiver> notificationReceiver = notificationReceiverService.findOne(id);
        return ResponseUtil.wrapOrNotFound(notificationReceiver);
    }

    /**
     * {@code DELETE  /notification-receivers/:id} : delete the "id" notificationReceiver.
     *
     * @param id the id of the notificationReceiver to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/api/notification-receivers/{id}")
    public ResponseEntity<Void> deleteNotificationReceiver(@PathVariable Long id) {
        log.debug("REST request to delete NotificationReceiver : {}", id);
        notificationReceiverService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/notification-receivers?query=:query} : search for the notificationReceiver corresponding
     * to the query.
     *
     * @param query    the query of the notificationReceiver search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/api/_search/notification-receivers")
    public ResponseEntity<Page<NotificationReceiver>> searchNotificationReceivers(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of NotificationReceivers for query {}", query);
        Page<NotificationReceiver> page = notificationReceiverService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    /**
     * {@code FIND  /_find/notification-receivers} : search for the notificationReceiver corresponding to the query.
     *
     * @param queryDTO the query of the notificationReceiver search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @PostMapping("/api/_find/notification-receivers")
    public ResponseEntity<Page<NotificationReceiver>> findNotificationReceivers(@RequestBody QueryDTO queryDTO, Pageable pageable) {
        log.debug("REST request to find for a page of NotificationReceivers for query {}", queryDTO.getQuery());
        Page<NotificationReceiver> page = notificationReceiverService.search(queryDTO.getQuery(), pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @GetMapping("/api/_refetch/notification-receivers")
    public ResponseEntity<List<NotificationReceiver>> refetch() {
        Page<NotificationReceiver> page = notificationReceiverService.findAll(Pageable.unpaged());
        notificationReceiverService.saveAll(page.getContent());
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/api/_send/notification-receivers")
    public ResponseEntity<Void> sendNotificationsReceivers() {
        notificationReceiverService.sendNotificationsReceivers();
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, "")).build();
    }

    /**
     * {@code DELETE  /notification-receivers/cancelled} : delete cancelled sender notification receiver.
     *
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/api/notification-receivers/cancelled")
    public ResponseEntity<Void> deleteCancelledAndSenderNotNullNotificationsReceivers() {
        log.debug("REST request to delete cancelled and sender not null notifications.");
        notificationReceiverService.deleteCancelledAndSenderNotNullNotificationsReceivers();
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, null)).build();
    }
}
