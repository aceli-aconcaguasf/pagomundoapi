package com.pagomundo.web.rest;

import com.pagomundo.domain.DTO.IssueForMerchantDTO;
import com.pagomundo.domain.DTO.NotificationListDTO;
import com.pagomundo.domain.DTO.NotificationReceiverDTO;
import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.Notification;
import com.pagomundo.domain.enumeration.NotificationCategory;
import com.pagomundo.domain.enumeration.NotificationSubCategory;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.security.AuthoritiesConstants;
import com.pagomundo.service.ExtendedUserService;
import com.pagomundo.service.NotificationReceiverService;
import com.pagomundo.service.NotificationService;
import com.pagomundo.service.TranslateService;
import com.pagomundo.web.rest.errors.BadRequestAlertException;
import com.pagomundo.web.rest.errors.NotFoundAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.pagomundo.domain.Notification}.
 */
@RestController
public class NotificationResource {

    private static final String ENTITY_NAME = "notification";
    private final Logger log = LoggerFactory.getLogger(NotificationResource.class);
    private final NotificationService notificationService;
    private final NotificationReceiverService notificationReceiverService;
    private final ExtendedUserService extendedUserService;
    private final TranslateService translateService;
    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public NotificationResource(
        NotificationService notificationService,
        NotificationReceiverService notificationReceiverService,
        ExtendedUserService extendedUserService,
        TranslateService translateService) {
        this.notificationService = notificationService;
        this.notificationReceiverService = notificationReceiverService;
        this.extendedUserService = extendedUserService;
        this.translateService = translateService;
    }

    @GetMapping("/api/_refetch/notifications/{id}")
    public ResponseEntity<List<Notification>> refetchId(@PathVariable Long id) {
        notificationService.refetch(id);
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    /**
     * {@code POST  /notifications} : Create a new notification.
     *
     * @param notification the notification to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new notification, or with status {@code 400 (Bad Request)} if the notification has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/api/notifications")
    public ResponseEntity<Notification> createNotification(@Valid @RequestBody Notification notification) throws URISyntaxException {
        log.debug("REST request to save Notification : {}", notification);
        if (notification.getId() != null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.notification.exist"), ENTITY_NAME, "idexists");
        }
        Notification result = notificationService.create(notification);
        notificationService.refetch(result.getId());
        return ResponseEntity.created(new URI("/api/notifications/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /notifications} : Updates an existing notification.
     *
     * @param notification the notification to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated notification,
     * or with status {@code 400 (Bad Request)} if the notification is not valid,
     * or with status {@code 500 (Internal Server Error)} if the notification couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/api/notifications")
    public ResponseEntity<Notification> updateNotification(@Valid @RequestBody Notification notification) throws URISyntaxException {
        log.debug("REST request to update Notification : {}", notification);
        if (notification.getId() == null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.missing.id"), ENTITY_NAME, "idnull");
        }

        Notification result = notificationService.update(notification);
        notificationService.refetch(result.getId());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, notification.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /notifications} : get all the notifications.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of notifications in body.
     */
    @GetMapping("/api/notifications")
    public ResponseEntity<List<Notification>> getAllNotifications(Pageable pageable) {
        log.debug("REST request to get a page of Notifications");
        Page<Notification> page = notificationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /notifications/:id} : get the "id" notification.
     *
     * @param id the id of the notification to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the notification, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/api/notifications/{id}")
    public ResponseEntity<Notification> getNotification(@PathVariable Long id) {
        log.debug("REST request to get Notification : {}", id);
        Optional<Notification> notification = notificationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(notification);
    }

    /**
     * {@code DELETE  /notifications/:id} : delete the "id" notification.
     *
     * @param id the id of the notification to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/api/notifications/{id}")
    public ResponseEntity<Void> deleteNotification(@PathVariable Long id) {
        log.debug("REST request to delete Notification : {}", id);
        notificationService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/notifications?query=:query} : search for the notification corresponding
     * to the query.
     *
     * @param query the query of the notification search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/api/_search/notifications")
    public ResponseEntity<Page<Notification>> searchNotifications(@RequestParam String query, Pageable pageable) {
        Page<Notification> page = notificationService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @GetMapping("/api/_refetch/notifications")
    public ResponseEntity<List<Notification>> refetch() {
        Page<Notification> page = notificationService.findAll(Pageable.unpaged());
        notificationService.saveAll(page.getContent());
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code POST  /_multi/notifications} : Create notifications.
     *
     * @param notificationListDTO the notifications to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dose, or with status {@code 400 (Bad Request)} if the notification has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/api/_multi/notifications")
    public ResponseEntity<NotificationListDTO> createMultiNotification(@RequestBody NotificationListDTO notificationListDTO) throws URISyntaxException {
        log.debug("REST request to save a group of notifications : {}", notificationListDTO);

        if (notificationListDTO.getNotifications().isEmpty()) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.notification.create.empty"), ENTITY_NAME, "empty");
        }

        if (notificationListDTO.getNotifications().stream().anyMatch(notification -> notification.getId() != null)) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.notification.exist"), ENTITY_NAME, "idExists");
        }

        NotificationListDTO result = notificationService.create(notificationListDTO);
        notificationService.refetchAll(result.getNotifications());

        return ResponseEntity.created(new URI("/api/_multi/notification/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, null))
            .body(result);
    }

    /**
     * {@code PUT  /_multi/notifications} : Update notifications.
     *
     * @param notificationListDTO the notifications to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dose, or with status {@code 400 (Bad Request)} if the notification has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/api/_multi/notifications")
    public ResponseEntity<NotificationListDTO> updateMultiNotification(@RequestBody NotificationListDTO notificationListDTO) throws URISyntaxException {
        log.debug("REST request to update a group of notifications : {}", notificationListDTO);

        if (notificationListDTO.getNotifications().isEmpty()) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.notification.update.empty"), ENTITY_NAME, "empty");
        }

        if (notificationListDTO.getNotifications().stream().anyMatch(notification -> notification.getId() == null)) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.notification.not.exist"), ENTITY_NAME, "missingId");
        }

        NotificationListDTO result = notificationService.update(notificationListDTO);
        notificationService.refetchAll(result.getNotifications());

        return ResponseEntity.created(new URI("/api/_multi/notification/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, null))
            .body(result);
    }

    /**
     * {@code SEARCH  /v1/issues} : search for the issues
     * to the query.
     *
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/v1/issues")
    @Secured({AuthoritiesConstants.MERCHANT, AuthoritiesConstants.RESELLER})
    public ResponseEntity<Page<IssueForMerchantDTO>> v1GetIssues(Pageable pageable) {
        ExtendedUser eu = extendedUserService.getCurrentExtendedUser();
        if (Status.CANCELLED.equals(eu.getStatus()))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.incorrect.status") + " " + eu.getStatus() + " " + translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.this.action"),
                NotificationService.ENTITY_NAME, ExtendedUserService.INCORRECT_STATUS);

        Page<Notification> page = notificationService.search("category:ISSUE AND sender.id:" + eu.getId(), pageable);

        List<Notification> notificationList = page.getContent();
        List<IssueForMerchantDTO> issueForMerchantDTOList = new ArrayList<>();
        notificationList.forEach(notification -> issueForMerchantDTOList.add(new IssueForMerchantDTO(notification)));
        Page<IssueForMerchantDTO> issueForMerchantPage = new PageImpl<>(issueForMerchantDTOList, pageable, page.getTotalElements());

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(issueForMerchantPage);
    }

    /**
     * {@code GET  /v1/issues/:id} : get the "id" issue.
     *
     * @param id the id of the notification to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the notification, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/v1/issues/{id}")
    @Secured({AuthoritiesConstants.MERCHANT, AuthoritiesConstants.RESELLER})
    public ResponseEntity<IssueForMerchantDTO> v1Issue(@PathVariable Long id) {
        log.debug("REST request to get Issue : {}", id);
        if (id == null)
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.issue.exist"), "ISSUE", "missingId");

        ExtendedUser eu = extendedUserService.getCurrentExtendedUser();
        if (Status.CANCELLED.equals(eu.getStatus()))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.incorrect.status") + " " + eu.getStatus() + " " + translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.this.action"),
                NotificationService.ENTITY_NAME, ExtendedUserService.INCORRECT_STATUS);

        Optional<Notification> notification = notificationService.findOneByIdAndSenderId(id, eu.getId());
        if (notification.isEmpty())
            throw new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.issue.id") + " " + id, "ISSUE", NotificationService.NOT_FOUND);

        IssueForMerchantDTO issueForMerchantDTO = new IssueForMerchantDTO(notification.get());
        return ResponseUtil.wrapOrNotFound(Optional.of(issueForMerchantDTO));
    }

    /**
     * {@code POST  /v1/issues} : Create a new issue.
     *
     * @param issueForMerchantDTO the issue to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new notificationReceiver, or with status {@code 400 (Bad Request)} if the notificationReceiver has already an ID.
     */
    @PostMapping("/v1/issues")
    @Secured({AuthoritiesConstants.MERCHANT, AuthoritiesConstants.RESELLER})
    public ResponseEntity<IssueForMerchantDTO> v1CreateIssue(@RequestBody IssueForMerchantDTO issueForMerchantDTO) throws URISyntaxException {
        log.debug("REST request to create issue: {}", issueForMerchantDTO);

        ExtendedUser eu = extendedUserService.getCurrentExtendedUser();
        if (Status.CANCELLED.equals(eu.getStatus()))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.incorrect.status") + " " + eu.getStatus() + " " + translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.this.action"),
                NotificationService.ENTITY_NAME, ExtendedUserService.INCORRECT_STATUS);

        issueForMerchantDTO.setCategory(NotificationCategory.ISSUE);
        issueForMerchantDTO.setSubCategory(NotificationSubCategory.ISSUE_CREATED);
        issueForMerchantDTO.setSender(eu);

        NotificationReceiverDTO notificationReceiverDTO = new NotificationReceiverDTO();
        notificationReceiverDTO.setNotification(IssueForMerchantDTO.issueForMerchantToNotification(issueForMerchantDTO));
        Notification result = notificationReceiverService.create(notificationReceiverDTO);
        if (result == null)
            return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, "ISSUE", "")).build();
        issueForMerchantDTO = new IssueForMerchantDTO(result);

        return ResponseEntity.created(new URI("/v1/issues/" + issueForMerchantDTO.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, "ISSUE", issueForMerchantDTO.getId().toString()))
            .body(issueForMerchantDTO);
    }

}
