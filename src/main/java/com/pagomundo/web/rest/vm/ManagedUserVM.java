package com.pagomundo.web.rest.vm;

import com.pagomundo.service.dto.UserDTO;

import javax.validation.constraints.Size;

/**
 * View Model extending the UserDTO, which is meant to be used in the user management UI.
 */
public class ManagedUserVM extends UserDTO {

    public static final int PASSWORD_MIN_LENGTH = 8;

    public static final int PASSWORD_MAX_LENGTH = 40;

    private static final String AT_LEAST_ONE_DIGIT = "(?=.*[0-9])"; // a digit must occur at least once
    private static final String AT_LEAST_ONE_LOWER_CASE = "(?=.*[a-z])"; // a lower case letter must occur at least once
    private static final String AT_LEAST_ONE_UPPER_CASE = "(?=.*[A-Z])"; // a upper case letter must occur at least once
    private static final String BETWEEN_MIN_MAX_LENGTH = ".{" + PASSWORD_MIN_LENGTH + "," + PASSWORD_MAX_LENGTH + "}"; // between min and max length

    public static final String PASSWORD_PATTERN =
        AT_LEAST_ONE_DIGIT + AT_LEAST_ONE_LOWER_CASE + AT_LEAST_ONE_UPPER_CASE + BETWEEN_MIN_MAX_LENGTH;

    @Size(min = PASSWORD_MIN_LENGTH, max = PASSWORD_MAX_LENGTH)
    private String password;

    public ManagedUserVM() {
        // Empty constructor needed for Jackson.
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "ManagedUserVM{" + super.toString() + "} ";
    }
}
