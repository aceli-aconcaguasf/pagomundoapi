/**
 * View Models used by Spring MVC REST controllers.
 */
package com.pagomundo.web.rest.vm;
