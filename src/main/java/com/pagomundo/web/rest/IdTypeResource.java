package com.pagomundo.web.rest;

import com.google.common.collect.Streams;
import com.pagomundo.domain.Country;
import com.pagomundo.domain.DTO.IdTypeDTO;
import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.IdType;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.security.AuthoritiesConstants;
import com.pagomundo.service.CountryService;
import com.pagomundo.service.ExtendedUserService;
import com.pagomundo.service.IdTypeService;
import com.pagomundo.service.TranslateService;
import com.pagomundo.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing {@link com.pagomundo.domain.IdType}.
 */
@RestController
public class IdTypeResource {

    private static final String ENTITY_NAME = "idType";
    private final Logger log = LoggerFactory.getLogger(IdTypeResource.class);
    private final IdTypeService idTypeService;
    private final ExtendedUserService extendedUserService;
    private final CountryService countryService;
    private final TranslateService translateService;
    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public IdTypeResource(
        IdTypeService idTypeService,
        ExtendedUserService extendedUserService,
        CountryService countryService,
        TranslateService translateService) {
        this.idTypeService = idTypeService;
        this.extendedUserService = extendedUserService;
        this.countryService = countryService;
        this.translateService = translateService;
    }

    /**
     * {@code POST  /id-types} : Create a new idType.
     *
     * @param idType the idType to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new idType, or with status {@code 400 (Bad Request)} if the idType has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/api/id-types")
    public ResponseEntity<IdType> createIdType(@Valid @RequestBody IdType idType) throws URISyntaxException {
        log.debug("REST request to save IdType : {}", idType);
        if (idType.getId() != null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.id.type.exist"), ENTITY_NAME, "idexists");
        }
        IdType result = idTypeService.save(idType);
        idTypeService.refetch(result.getId());

        return ResponseEntity.created(new URI("/api/id-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /id-types} : Updates an existing idType.
     *
     * @param idType the idType to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated idType,
     * or with status {@code 400 (Bad Request)} if the idType is not valid,
     * or with status {@code 500 (Internal Server Error)} if the idType couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/api/id-types")
    public ResponseEntity<IdType> updateIdType(@Valid @RequestBody IdType idType) throws URISyntaxException {
        log.debug("REST request to update IdType : {}", idType);
        if (idType.getId() == null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.invalid.id"), ENTITY_NAME, "idnull");
        }
        IdType result = idTypeService.save(idType);
        idTypeService.refetch(result.getId());

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, idType.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /id-types} : get all the idTypes.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of idTypes in body.
     */
    @GetMapping("/api/id-types")
    public ResponseEntity<List<IdType>> getAllIdTypes(Pageable pageable) {
        log.debug("REST request to get a page of IdTypes");
        Page<IdType> page = idTypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /id-types/:id} : get the "id" idType.
     *
     * @param id the id of the idType to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the idType, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/api/id-types/{id}")
    public ResponseEntity<IdType> getIdType(@PathVariable Long id) {
        log.debug("REST request to get IdType : {}", id);
        Optional<IdType> idType = idTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(idType);
    }

    /**
     * {@code DELETE  /id-types/:id} : delete the "id" idType.
     *
     * @param id the id of the idType to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/api/id-types/{id}")
    public ResponseEntity<Void> deleteIdType(@PathVariable Long id) {
        log.debug("REST request to delete IdType : {}", id);
        idTypeService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/id-types?query=:query} : search for the idType corresponding
     * to the query.
     *
     * @param query the query of the idType search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/api/_search/id-types")
    public ResponseEntity<Page<IdType>> searchIdTypes(@RequestParam String query, Pageable pageable) {
        Page<IdType> page = idTypeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    /**
     * {@code SEARCH  /v1/id-types} : search for the directPaymentBank corresponding to the query.
     *
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/v1/{countryId}/id-types")
    @Secured({AuthoritiesConstants.MERCHANT, AuthoritiesConstants.RESELLER})
    public ResponseEntity<Page<IdTypeDTO>> v1SearchIdTypes(@PathVariable Long countryId, Pageable pageable) {
        log.debug("REST request to search for a page of IdTypes.");
        ExtendedUser eu = extendedUserService.getCurrentExtendedUser();
        if (Status.CANCELLED.equals(eu.getStatus()))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.incorrect.status") + " " + eu.getStatus() + " " + translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.this.action"),
                IdTypeService.ENTITY_NAME, ExtendedUserService.INCORRECT_STATUS);

        Country country = countryService.getCountryForPayee(countryId);

        List<IdTypeDTO> page = Streams.stream(idTypeService.findAll())
            .filter(idType -> country.equals(idType.getCountry()))
            .map(IdTypeDTO::new).collect(Collectors.toList());

        Page<IdTypeDTO> pageDTO = new PageImpl<>(page, pageable, page.size());
        return ResponseEntity.ok()
            .headers(PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), pageDTO))
            .body(pageDTO);
    }

    @GetMapping("/api/_refetch/id-types")
    public ResponseEntity<List<IdType>> refetch() {
        Page<IdType> page = idTypeService.findAll(Pageable.unpaged());
        idTypeService.saveAll(page.getContent());
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
