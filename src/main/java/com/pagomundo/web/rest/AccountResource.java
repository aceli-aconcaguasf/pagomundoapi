package com.pagomundo.web.rest;

import com.pagomundo.config.Constants;
import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.User;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.repository.UserRepository;
import com.pagomundo.security.SecurityUtils;
import com.pagomundo.service.ExtendedUserService;
import com.pagomundo.service.NotificationReceiverService;
import com.pagomundo.service.TranslateService;
import com.pagomundo.service.UserService;
import com.pagomundo.service.dto.EmailChangeDTO;
import com.pagomundo.service.dto.PasswordChangeDTO;
import com.pagomundo.service.dto.UserDTO;
import com.pagomundo.web.rest.errors.BadRequestAlertException;
import com.pagomundo.web.rest.errors.EmailAlreadyUsedException;
import com.pagomundo.web.rest.errors.EmailNotFoundException;
import com.pagomundo.web.rest.errors.InvalidPasswordException;
import com.pagomundo.web.rest.errors.LoginAlreadyUsedException;
import com.pagomundo.web.rest.errors.NotFoundAlertException;
import com.pagomundo.web.rest.errors.UserNotFoundException;
import com.pagomundo.web.rest.vm.KeyAndPasswordVM;
import com.pagomundo.web.rest.vm.ManagedUserVM;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;

/**
 * REST controller for managing the current user's account.
 */
@RestController
public class AccountResource {

    private final Logger log = LoggerFactory.getLogger(AccountResource.class);
    private final UserRepository userRepository;
    private final UserService userService;
    private final ExtendedUserService extendedUserService;
    private final NotificationReceiverService notificationReceiverService;
    private final TranslateService translateService;

    public AccountResource(
        UserRepository userRepository,
        UserService userService,
        ExtendedUserService extendedUserService,
        NotificationReceiverService notificationReceiverService,
        TranslateService translateService) {
        this.userRepository = userRepository;
        this.userService = userService;
        this.extendedUserService = extendedUserService;
        this.notificationReceiverService = notificationReceiverService;
        this.translateService = translateService;
    }

    private static boolean passwordIsNotValid(String password) {
        return StringUtils.isEmpty(password) ||
            !password.matches(ManagedUserVM.PASSWORD_PATTERN) ||
            password.length() < ManagedUserVM.PASSWORD_MIN_LENGTH || password.length() > ManagedUserVM.PASSWORD_MAX_LENGTH;
    }

    /**
     * {@code POST  /register} : register the user.
     *
     * @param managedUserVM the managed user View Model.
     * @throws InvalidPasswordException  {@code 400 (Bad Request)} if the password is incorrect.
     * @throws EmailAlreadyUsedException {@code 400 (Bad Request)} if the email is already used.
     * @throws LoginAlreadyUsedException {@code 400 (Bad Request)} if the login is already used.
     */
    @PostMapping("/api/register")
    @ResponseStatus(HttpStatus.CREATED)
    public void registerAccount(@Valid @RequestBody ManagedUserVM managedUserVM) {
        if (passwordIsNotValid(managedUserVM.getPassword())) throw new InvalidPasswordException();
        userService.registerUser(managedUserVM, managedUserVM.getPassword());
    }

    /**
     * {@code GET  /activate} : activate the registered user.
     *
     * @param key the activation key.
     * @throws RuntimeException {@code 500 (Internal Server Error)} if the user couldn't be activated.
     */
    @GetMapping("/api/activate")
    public void activateAccount(@RequestParam(value = "key") String key) {
        Optional<User> user = userService.activateRegistration(key);
        if (!user.isPresent())
            throw new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.account.user.not.exist.reset.key"), UserService.ENTITY_NAME, UserService.USER_KEY_NOT_FOUND_CODE);
        userService.refetch(user.get().getId());
    }

    /**
     * {@code GET  /authenticate} : check if the user is authenticated, and return its login.
     *
     * @param request the HTTP request.
     * @return the login if the user is authenticated.
     */
    @GetMapping("/api/authenticate")
    public String isAuthenticated(HttpServletRequest request) {
        log.debug("REST request to check if the current user is authenticated");
        return request.getRemoteUser();
    }

    /**
     * {@code GET  /account} : get the current user.
     *
     * @return the current user.
     * @throws RuntimeException {@code 500 (Internal Server Error)} if the user couldn't be returned.
     */
    @GetMapping("/api/account")
    public UserDTO getAccount() {
        return userService.getUserWithAuthorities()
            .map(UserDTO::new)
            .orElseThrow(() -> new UserNotFoundException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.account.user.not.found")));
    }

    /**
     * {@code POST  /account} : update the current user information.
     *
     * @param userDTO the current user information.
     * @throws EmailAlreadyUsedException {@code 400 (Bad Request)} if the email is already used.
     * @throws RuntimeException          {@code 500 (Internal Server Error)} if the user login wasn't found.
     */
    @PostMapping("/api/account")
    public void saveAccount(@Valid @RequestBody UserDTO userDTO) {
        String userLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new UserNotFoundException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.account.user.login.not.found")));
        Optional<User> existingUser = userRepository.findOneByEmailIgnoreCase(userDTO.getEmail());
        if (existingUser.isPresent() && (!existingUser.get().getLogin().equalsIgnoreCase(userLogin)))
            throw new EmailAlreadyUsedException(userDTO.getEmail());

        Optional<User> user = userRepository.findOneByLogin(userLogin);
        if (!user.isPresent()) throw new UserNotFoundException(userLogin);

        userService.updateUser(userDTO.getFirstName(), userDTO.getLastName(), userDTO.getEmail(), userDTO.getLangKey(), userDTO.getImageUrl());
        userService.refetch(userDTO.getId());
    }

    /**
     * {@code POST  /account/change-password} : changes the current user's password.
     *
     * @param passwordChangeDto current and new password.
     * @throws InvalidPasswordException {@code 400 (Bad Request)} if the new password is incorrect.
     */
    @PostMapping(path = "/api/account/change-password")
    public void changePassword(@RequestBody PasswordChangeDTO passwordChangeDto) {
        if (passwordChangeDto.getNewPassword().equalsIgnoreCase(passwordChangeDto.getCurrentPassword()))
            throw new InvalidPasswordException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.current.password.repeat"));
        if (passwordIsNotValid(passwordChangeDto.getNewPassword()))
            throw new InvalidPasswordException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.current.password.pattern"));

        userService.changePassword(passwordChangeDto.getCurrentPassword(), passwordChangeDto.getNewPassword());
    }

    /**
     * {@code POST   /account/reset-password/init} : Send an email to reset the password of the user.
     *
     * @param mail the mail of the user.
     * @throws EmailNotFoundException {@code 400 (Bad Request)} if the email address is not registered.
     */
    @PostMapping(path = "/api/account/reset-password/init")
    public void requestPasswordReset(@RequestBody String mail) {
    	log.info("mail forgot password: {}",mail);
        User user = userService.requestPasswordReset(mail).orElseThrow(() -> new EmailNotFoundException(mail));
        userService.refetch(user.getId());
        notificationReceiverService.sendForgotPasswordNotification(user);
        log.info("mail forgot password sent");
    }

    /**
     * {@code POST   /api/account/resend-creation-email} : ReSend an email of user creation.
     *
     * @param mail the mail of the user.
     * @throws EmailNotFoundException {@code 400 (Bad Request)} if the email address is not registered.
     */
    @PostMapping(path = "/api/account/resend-creation-email")
    public void resendCreationEmail(@RequestBody String mail) {
        User user = userService.requestPasswordReset(mail).orElseThrow(() -> new EmailNotFoundException(mail));
        userService.refetch(user.getId());
        user.setStatus(Status.CREATED);
        notificationReceiverService.sendCreateUserNotification(user, userService.getCurrentExtendedUser());
    }

    /**
     * {@code POST   /api/account/change-email} : ReSend an email of user creation.
     *
     * @param emailChangeDTO the mail of the user.
     * @throws EmailNotFoundException {@code 400 (Bad Request)} if the email address is not registered.
     */
    @PostMapping(path = "/api/account/change-email")
    public void changeEmail(@RequestBody EmailChangeDTO emailChangeDTO) {
        validateEmail(emailChangeDTO);

        Optional<User> user = userService.changeEmail(emailChangeDTO);
        if (!user.isPresent())
            throw new UserNotFoundException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.account.user.email.not.registered") + emailChangeDTO.getCurrentEmail());
        userService.refetch(user.get().getId());

        Optional<ExtendedUser> eu = extendedUserService.changeEmail(emailChangeDTO);
        eu.ifPresent(extendedUser -> extendedUserService.refetch(extendedUser.getId()));
    }

    /**
     * {@code POST   /account/reset-password/finish} : Finish to reset the password of the user.
     *
     * @param keyAndPassword the generated key and the new password.
     * @throws InvalidPasswordException {@code 400 (Bad Request)} if the password is incorrect.
     * @throws RuntimeException         {@code 500 (Internal Server Error)} if the password could not be reset.
     */
    @PostMapping(value = {"/api/account/reset-password/finish", "/v1/reset-password"})
    public void finishPasswordReset(@RequestBody KeyAndPasswordVM keyAndPassword) {
        if (passwordIsNotValid(keyAndPassword.getNewPassword())) throw new InvalidPasswordException();

        Optional<User> user = userService.completePasswordReset(keyAndPassword.getNewPassword(), keyAndPassword.getKey());
        if (!user.isPresent())
            throw new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.account.user.reset.key"),
                UserService.ENTITY_NAME, UserService.USER_KEY_NOT_FOUND_CODE);
        //if activation and reset key are equals, its a new user
        log.debug("User Data Activation {}", (user.get().getActivationKey()!= null && user.get().getActivationKey().equals(user.get().getResetKey()))||!user.get().getActivated());
        if((user.get().getActivationKey()!= null && user.get().getActivationKey().equals(user.get().getResetKey()))||!user.get().getActivated())userService.activateRegistration(user.get().getActivationKey());
        userService.refetch(user.get().getId());
    }

    /**
     * {@code GET   /account/verify-key} : Verify if the key exists.
     *
     * @param key the generated key.
     * @throws RuntimeException {@code 500 (Internal Server Error)} if the password could not be reset.
     */
    @GetMapping(path = "/api/account/verify-key")
    public User verifyKey(@RequestParam(value = "key") String key) {
        try{
            return userService.verifyKey(key);
        }catch(NotFoundAlertException ex){
            throw ex;
        }
        
        
           
    }

    /**
     * {@code POST   /account/change-email-notification} : Send notification for email change.
     *
     * @param emailChangeDTO object with currency email and new email
     */
    @PostMapping(path = "/api/account/change-email-notification")
    public void sendChangeEmailNotification(@RequestBody EmailChangeDTO emailChangeDTO) {
        validateEmail(emailChangeDTO);
        Optional<User> existingUser = userRepository.findOneByLogin(emailChangeDTO.getNewEmail());
        if (existingUser.isPresent())
            throw new EmailAlreadyUsedException(emailChangeDTO.getNewEmail());

        notificationReceiverService.sendChangeEmailNotification(emailChangeDTO);
    }

    /**
     * {@code POST   /account/change-email-confirmation} : Change email and send email notification
     *
     * @param activationKey the generated key by email change.
     */
    @PostMapping(path = "/api/account/change-email-confirmation")
    public void changeEmailAndSendNotification(@RequestBody String activationKey) {

        Optional<User> user = userService.changeEmailUserByActivationKey(activationKey);
        if (user.isPresent()) {
            userService.refetch(user.get().getId());
            Optional<ExtendedUser> extendedUser = extendedUserService.changeExtendedUserEmail(user.get(), user.get().getEmail());
            extendedUser.ifPresent(varExtendedUser -> extendedUserService.refetch(varExtendedUser.getId()));
            notificationReceiverService.sendChangeEmailConfirmationNotification(user.get());
        } else {
            throw new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.account.user.not.exist.activation.key"), UserService.ENTITY_NAME, UserService.USER_KEY_NOT_FOUND_CODE);
        }

    }

    private void validateEmail(EmailChangeDTO emailChangeDTO) {
        if (emailChangeDTO.getCurrentEmail() == null || emailChangeDTO.getCurrentEmail().equals(""))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.account.user.email.not.valid") + " '" + emailChangeDTO.getCurrentEmail() + "'",
                UserService.ENTITY_NAME, UserService.EMAIL_INVALID_CODE);

        if (emailChangeDTO.getNewEmail() == null || emailChangeDTO.getNewEmail().equals("") ||
            !emailChangeDTO.getNewEmail().matches(Constants.LOGIN_REGEX))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.account.user.new.email.regular.expression") + " '" + Constants.LOGIN_REGEX + "'",
                UserService.ENTITY_NAME, UserService.EMAIL_INVALID_CODE);
    }
}
