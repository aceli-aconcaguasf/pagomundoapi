package com.pagomundo.web.rest;

import com.google.common.collect.Streams;
import com.pagomundo.domain.Country;
import com.pagomundo.domain.DTO.DirectPaymentBankDTO;
import com.pagomundo.domain.DTO.DirectPaymentCityDTO;
import com.pagomundo.domain.DirectPaymentBank;
import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.security.AuthoritiesConstants;
import com.pagomundo.service.CountryService;
import com.pagomundo.service.DirectPaymentBankService;
import com.pagomundo.service.ExtendedUserService;
import com.pagomundo.service.TranslateService;
import com.pagomundo.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing {@link com.pagomundo.domain.DirectPaymentBank}.
 */
@RestController
public class DirectPaymentBankResource {

    private static final String ENTITY_NAME = "directPaymentBank";
    private final Logger log = LoggerFactory.getLogger(DirectPaymentBankResource.class);
    private final DirectPaymentBankService directPaymentBankService;
    private final ExtendedUserService extendedUserService;
    private final CountryService countryService;
    private final TranslateService translateService;
    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public DirectPaymentBankResource(
        DirectPaymentBankService directPaymentBankService,
        ExtendedUserService extendedUserService,
        CountryService countryService,
        TranslateService translateService) {
        this.directPaymentBankService = directPaymentBankService;
        this.extendedUserService = extendedUserService;
        this.countryService = countryService;
        this.translateService = translateService;
    }

    /**
     * {@code POST  /direct-payment-banks} : Create a new directPaymentBank.
     *
     * @param directPaymentBank the directPaymentBank to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new directPaymentBank, or with status {@code 400 (Bad Request)} if the directPaymentBank has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/api/direct-payment-banks")
    @Secured({AuthoritiesConstants.SUPER_ADMIN, AuthoritiesConstants.ADMIN})
    public ResponseEntity<DirectPaymentBank> createDirectPaymentBank(@RequestBody DirectPaymentBank directPaymentBank) throws URISyntaxException {
        log.debug("REST request to save DirectPaymentBank : {}", directPaymentBank);
        if (directPaymentBank.getId() != null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.direct.payment.exist"), ENTITY_NAME, "idExists");
        }
        DirectPaymentBank result = directPaymentBankService.save(directPaymentBank);
        return ResponseEntity.created(new URI("/api/direct-payment-banks/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /direct-payment-banks} : Updates an existing directPaymentBank.
     *
     * @param directPaymentBank the directPaymentBank to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated directPaymentBank,
     * or with status {@code 400 (Bad Request)} if the directPaymentBank is not valid,
     * or with status {@code 500 (Internal Server Error)} if the directPaymentBank couldn't be updated.
     */
    @PutMapping("/api/direct-payment-banks")
    @Secured({AuthoritiesConstants.SUPER_ADMIN, AuthoritiesConstants.ADMIN})
    public ResponseEntity<DirectPaymentBank> updateDirectPaymentBank(@RequestBody DirectPaymentBank directPaymentBank) {
        log.debug("REST request to update DirectPaymentBank : {}", directPaymentBank);
        if (directPaymentBank.getId() == null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.invalid.id"), ENTITY_NAME, "idNull");
        }
        DirectPaymentBank result = directPaymentBankService.save(directPaymentBank);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, directPaymentBank.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /direct-payment-banks} : get all the directPaymentBanks.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of directPaymentBanks in body.
     */
    @GetMapping("/api/direct-payment-banks")
    public ResponseEntity<List<DirectPaymentBank>> getAllDirectPaymentBanks(Pageable pageable) {
        log.debug("REST request to get a page of DirectPaymentBanks");
        Page<DirectPaymentBank> page = directPaymentBankService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /direct-payment-banks/:id} : get the "id" directPaymentBank.
     *
     * @param id the id of the directPaymentBank to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the directPaymentBank, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/api/direct-payment-banks/{id}")
    public ResponseEntity<DirectPaymentBank> getDirectPaymentBank(@PathVariable Long id) {
        log.debug("REST request to get DirectPaymentBank : {}", id);
        Optional<DirectPaymentBank> directPaymentBank = directPaymentBankService.findOne(id);
        return ResponseUtil.wrapOrNotFound(directPaymentBank);
    }

    /**
     * {@code DELETE  /direct-payment-banks/:id} : delete the "id" directPaymentBank.
     *
     * @param id the id of the directPaymentBank to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/api/direct-payment-banks/{id}")
    @Secured({AuthoritiesConstants.SUPER_ADMIN, AuthoritiesConstants.ADMIN})
    public ResponseEntity<Void> deleteDirectPaymentBank(@PathVariable Long id) {
        log.debug("REST request to delete DirectPaymentBank : {}", id);
        directPaymentBankService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/direct-payment-banks?query=:query} : search for the directPaymentBank corresponding
     * to the query.
     *
     * @param query    the query of the directPaymentBank search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/api/_search/direct-payment-banks")
    public ResponseEntity<Page<DirectPaymentBank>> searchDirectPaymentBanks(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of DirectPaymentBanks for query {}", query);
        Page<DirectPaymentBank> page = directPaymentBankService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    /**
     * {@code SEARCH  /v1/direct-payment-banks} : search for the directPaymentBank corresponding to the query.
     *
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/v1/{countryId}/direct-payment-banks")
    @Secured({AuthoritiesConstants.MERCHANT, AuthoritiesConstants.RESELLER})
    public ResponseEntity<Page<DirectPaymentBankDTO>> v1SearchDirectPaymentBanks(@PathVariable Long countryId, Pageable pageable) {
        log.debug("REST request to search for a page of DirectPaymentBanks.");
        ExtendedUser eu = extendedUserService.getCurrentExtendedUser();
        if (Status.CANCELLED.equals(eu.getStatus()))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.incorrect.status") + " " + eu.getStatus() + " " + translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.this.action"),
                DirectPaymentBankService.ENTITY_NAME, ExtendedUserService.INCORRECT_STATUS);

        Country country = countryService.getCountryForPayee(countryId);

        List<DirectPaymentBankDTO> page = Streams.stream(directPaymentBankService.findAll())
            .filter(dpb -> dpb.getDestinyBank() != null && country.equals(dpb.getDestinyBank().getCountry()))
            .map(DirectPaymentBankDTO::new).collect(Collectors.toList());

        Page<DirectPaymentBankDTO> pageDTO = new PageImpl<>(page, pageable, page.size());
        return ResponseEntity.ok()
            .headers(PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), pageDTO))
            .body(pageDTO);
    }

    /**
     * {@code SEARCH  /v1/direct-payment-cities} : search for the directPaymentBank corresponding to the query.
     *
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/v1/{countryId}/direct-payment-cities")
    @Secured({AuthoritiesConstants.MERCHANT, AuthoritiesConstants.RESELLER})
    public ResponseEntity<Page<DirectPaymentCityDTO>> v1SearchDirectPaymentCities(@PathVariable Long countryId, Pageable pageable) {
        log.debug("REST request to search for a page of DirectPaymentCities.");
        ExtendedUser eu = extendedUserService.getCurrentExtendedUser();
        if (Status.CANCELLED.equals(eu.getStatus()))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.incorrect.status") + " " + eu.getStatus() + " " + translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.this.action"),
                DirectPaymentBankService.ENTITY_NAME, ExtendedUserService.INCORRECT_STATUS);

        Country country = countryService.getCountryForPayee(countryId);

        List<DirectPaymentCityDTO> page = Streams.stream(directPaymentBankService.findAll())
            .filter(dpb -> dpb.getDestinyCity() != null && country.equals(dpb.getDestinyCity().getCountry()))
            .map(DirectPaymentCityDTO::new).collect(Collectors.toList());

        Page<DirectPaymentCityDTO> pageDTO = new PageImpl<>(page, pageable, page.size());
        return ResponseEntity.ok()
            .headers(PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), pageDTO))
            .body(pageDTO);
    }

    @GetMapping("/api/_refetch/direct-payment-banks")
    public ResponseEntity<List<DirectPaymentBank>> refetch() {
        Page<DirectPaymentBank> page = directPaymentBankService.findAll(Pageable.unpaged());
        directPaymentBankService.saveAll(page.getContent());
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
