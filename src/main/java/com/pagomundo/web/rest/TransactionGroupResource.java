package com.pagomundo.web.rest;

import com.pagomundo.domain.DTO.TransactionListDTO;
import com.pagomundo.domain.DTO.Ripple.*;
import com.pagomundo.domain.TransactionGroup;
import com.pagomundo.security.AuthoritiesConstants;
import com.pagomundo.service.ExtendedUserService;
import com.pagomundo.service.NotificationReceiverService;
import com.pagomundo.service.TransactionGroupService;
import com.pagomundo.service.TransactionService;
import com.pagomundo.service.TranslateService;
import com.pagomundo.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.pagomundo.domain.TransactionGroup}.
 */
@RestController
@RequestMapping("/api")
public class TransactionGroupResource {

    private static final String ENTITY_NAME = "transactionGroup";
    private static final String ENTITY_NAME_RIPPLE = "rippleTransactionGroup";
    private final Logger log = LoggerFactory.getLogger(TransactionGroupResource.class);
    private final TransactionGroupService transactionGroupService;
    private final TransactionService transactionService;
    private final NotificationReceiverService notificationReceiverService;
    private final ExtendedUserService extendedUserService;
    @Value("${jhipster.clientApp.name}")
    private String applicationName;
    private final TranslateService translateService;

    public TransactionGroupResource(TransactionGroupService transactionGroupService, TransactionService transactionService, NotificationReceiverService notificationReceiverService, ExtendedUserService extendedUserService, TranslateService translateService) {
        this.transactionGroupService = transactionGroupService;
        this.transactionService = transactionService;
        this.notificationReceiverService = notificationReceiverService;
        this.extendedUserService = extendedUserService;
        this.translateService = translateService;
    }

    /**
     * {@code POST  /transaction-groups-ripple} : Process a new group of transactions using Ripple Integration.
     *
     * @param RippleTransacctionDTO the transactionGroup to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new RippleTransacctionDTO, or with status {@code 400 (Bad Request)} if the transactionGroup has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    
    // @PreAuthorize("hasRole(\"" + AuthoritiesConstants.SUPER_ADMIN + "\")" + " or hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    @PostMapping("/transaction-groups-ripple")
    public ResponseEntity<RippleResultGroupDTO> createTransactionGroupRipple(@RequestBody RippleTransacctionDTO rippleTransactionGroupDTO) throws URISyntaxException, IOException {
        
        // log.info(">>>>>>>>>>>>>>>> REST request to save createTransactionGroupRipple : {}", transactionGroupDTO);
        log.info(">>>>>>>>>>>>>>>> rippleTransacctionDTO: {}", rippleTransactionGroupDTO);

        if (rippleTransactionGroupDTO.getBankAccount() == null || rippleTransactionGroupDTO.getBankAccount().getId() == null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.missing.bank.account"), ENTITY_NAME_RIPPLE, "MissingBankAccount");
        }
        log.info(">>>>>>>>>>>>>>>> Despues de vlaidacion de cuenta bancaria");

        // Puede que nos ea necesario, se procesara la moneda local del valor recibido amountLocalCurrency
        if (rippleTransactionGroupDTO.getExchangeRate() == null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.missing.exchange"), ENTITY_NAME_RIPPLE, "MissingExchangeRate");
        }
        log.info(">>>>>>>>>>>>>>>> after exception.missing.exchange validation");


        if (rippleTransactionGroupDTO.getTransactions().stream().anyMatch(transaction -> transaction.getId() == null)) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.missing.transaction"), ENTITY_NAME_RIPPLE, "transactionIdNull");
        }
        log.info(">>>>>>>>>>>>>>>> after exception.missing.transaction validation");

        // TODO: Revisar si se agregan mas validaciones

        
        RippleResultGroupDTO result = transactionGroupService.processAndUpdateRippleTrasaction(rippleTransactionGroupDTO);

        log.info(">>>>>>>>>>>>>>>> after create result group: {}", result);

        transactionService.refetchAll(rippleTransactionGroupDTO.getTransactions());

        log.info(">>>>>>>>>>>>>>>> after refetchAll transactions");

        // return ResponseEntity.created(new URI("/api/transaction-groups-ripple/" + result.getId()))
        return ResponseEntity.created(new URI("/api/transaction-groups-ripple/" + ""))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME_RIPPLE, null))
            .body(result);
    }

    

    /**
     * {@code POST  /transaction-groups} : Create a new transactionGroup.
     *
     * @param transactionGroupDTO the transactionGroup to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new transactionGroup, or with status {@code 400 (Bad Request)} if the transactionGroup has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/transaction-groups")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.SUPER_ADMIN + "\")" +
        " or hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<TransactionListDTO> createTransactionGroup(@RequestBody TransactionListDTO transactionGroupDTO) throws URISyntaxException, IOException {
        
        log.info(">>>>>>>>>>>>>>>> REST request to save TransactionGroup : {}", transactionGroupDTO);

        if (transactionGroupDTO.getId() != null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.invalid.id"), ENTITY_NAME, "idNull");
        }

        log.info(">>>>>>>>>>>>>>>> invalid.id validation");

        if (transactionGroupDTO.getBankAccount() == null || transactionGroupDTO.getBankAccount().getId() == null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.missing.bank.account"), ENTITY_NAME, "MissingBankAccount");
        }

        log.info(">>>>>>>>>>>>>>>> missing.bank.account validation");

        if (transactionGroupDTO.getExchangeRate() == null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.missing.exchange"), ENTITY_NAME, "MissingExchangeRate");
        }

        log.info(">>>>>>>>>>>>>>>> exception.missing.exchange validation");


        if (transactionGroupDTO.getTransactions().stream().anyMatch(transaction -> transaction.getId() == null)) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.missing.transaction"), ENTITY_NAME, "transactionIdNull");
        }

        log.info(">>>>>>>>>>>>>>>> exception.missing.transaction validation");

        TransactionListDTO result = transactionGroupService.create(transactionGroupDTO);

        log.info(">>>>>>>>>>>>>>>> after create transactions");

        transactionService.refetchAll(result.getTransactions());

        log.info(">>>>>>>>>>>>>>>> after refetchAll transactions");

        notificationReceiverService.sendTransactionNotification(result.getTransactions(), extendedUserService.getCurrentExtendedUser());

        log.info(">>>>>>>>>>>>>>>> after sendTransactionNotification");

        TransactionGroup tg = transactionGroupService.findOne(result.getId()).orElse(null);

        log.info(">>>>>>>>>>>>>>>> tg: {}", tg);

        if (tg != null) transactionGroupService.exportFile(tg);

        log.info(">>>>>>>>>>>>>>>> after exportFile");

        return ResponseEntity.created(new URI("/api/transaction-groups/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /transaction-groups} : Updates an existing transactionGroup.
     *
     * @param transactionGroup the transactionGroup to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated transactionGroup,
     * or with status {@code 400 (Bad Request)} if the transactionGroup is not valid,
     * or with status {@code 500 (Internal Server Error)} if the transactionGroup couldn't be updated.
     */
    @PutMapping("/transaction-groups")
    public ResponseEntity<TransactionGroup> updateTransactionGroup(@RequestBody TransactionGroup transactionGroup) {
        log.debug("REST request to update TransactionGroup : {}", transactionGroup);
        if (transactionGroup.getId() == null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.invalid.id"), ENTITY_NAME, "idnull");
        }
        TransactionGroup result = transactionGroupService.update(transactionGroup);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(transactionGroup);
    }

    /**
     * {@code GET  /transaction-groups} : get all the transactionGroups.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of transactionGroups in body.
     */
    @GetMapping("/transaction-groups")
    public ResponseEntity<List<TransactionGroup>> getAllTransactionGroups(Pageable pageable) {
        log.debug("REST request to get a page of TransactionGroups");
        Page<TransactionGroup> page = transactionGroupService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /transaction-groups/:id} : get the "id" transactionGroup.
     *
     * @param id the id of the transactionGroup to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the transactionGroup, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/transaction-groups/{id}")
    public ResponseEntity<TransactionGroup> getTransactionGroup(@PathVariable Long id) {
        log.debug("REST request to get TransactionGroup : {}", id);
        Optional<TransactionGroup> transactionGroup = transactionGroupService.findOne(id);
        return ResponseUtil.wrapOrNotFound(transactionGroup);
    }

    /**
     * {@code DELETE  /transaction-groups/:id} : delete the "id" transactionGroup.
     *
     * @param id the id of the transactionGroup to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/transaction-groups/{id}")
    public ResponseEntity<Void> deleteTransactionGroup(@PathVariable Long id) {
        log.debug("REST request to delete TransactionGroup : {}", id);
        transactionGroupService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/transaction-groups?query=:query} : search for the transactionGroup corresponding
     * to the query.
     *
     * @param query    the query of the transactionGroup search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/transaction-groups")
    public ResponseEntity<Page<TransactionGroup>> searchTransactionGroups(@RequestParam String query, Pageable pageable) {
        Page<TransactionGroup> page = transactionGroupService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @GetMapping("/_refetch/transaction-groups")
    public ResponseEntity<List<TransactionGroup>> refetch() {
        Page<TransactionGroup> page = transactionGroupService.findAll(Pageable.unpaged());
        transactionGroupService.saveAll(page.getContent());
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
