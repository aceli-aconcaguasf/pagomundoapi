package com.pagomundo.web.rest;

import com.pagomundo.domain.ExtendedUserStatusChange;
import com.pagomundo.service.ExtendedUserStatusChangeService;
import com.pagomundo.service.TranslateService;
import com.pagomundo.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.pagomundo.domain.ExtendedUserStatusChange}.
 */
@RestController
@RequestMapping("/api")
public class ExtendedUserStatusChangeResource {

    private static final String ENTITY_NAME = "extendedUserStatusChange";
    private final Logger log = LoggerFactory.getLogger(ExtendedUserStatusChangeResource.class);
    private final ExtendedUserStatusChangeService extendedUserStatusChangeService;
    @Value("${jhipster.clientApp.name}")
    private String applicationName;
    private final TranslateService translateService;

    public ExtendedUserStatusChangeResource(ExtendedUserStatusChangeService extendedUserStatusChangeService, TranslateService translateService) {
        this.extendedUserStatusChangeService = extendedUserStatusChangeService;
        this.translateService = translateService;
    }

    /**
     * {@code POST  /extended-user-status-changes} : Create a new extendedUserStatusChange.
     *
     * @param extendedUserStatusChange the extendedUserStatusChange to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new extendedUserStatusChange, or with status {@code 400 (Bad Request)} if the extendedUserStatusChange has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/extended-user-status-changes")
    public ResponseEntity<ExtendedUserStatusChange> createExtendedUserStatusChange(@RequestBody ExtendedUserStatusChange extendedUserStatusChange) throws URISyntaxException {
        log.debug("REST request to save ExtendedUserStatusChange : {}", extendedUserStatusChange);
        if (extendedUserStatusChange.getId() != null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.extended.user.status.change.exist"), ENTITY_NAME, "idexists");
        }
        ExtendedUserStatusChange result = extendedUserStatusChangeService.save(extendedUserStatusChange);
        return ResponseEntity.created(new URI("/api/extended-user-status-changes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /extended-user-status-changes} : Updates an existing extendedUserStatusChange.
     *
     * @param extendedUserStatusChange the extendedUserStatusChange to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated extendedUserStatusChange,
     * or with status {@code 400 (Bad Request)} if the extendedUserStatusChange is not valid,
     * or with status {@code 500 (Internal Server Error)} if the extendedUserStatusChange couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/extended-user-status-changes")
    public ResponseEntity<ExtendedUserStatusChange> updateExtendedUserStatusChange(@RequestBody ExtendedUserStatusChange extendedUserStatusChange) throws URISyntaxException {
        log.debug("REST request to update ExtendedUserStatusChange : {}", extendedUserStatusChange);
        if (extendedUserStatusChange.getId() == null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.invalid.id"), ENTITY_NAME, "idnull");
        }
        ExtendedUserStatusChange result = extendedUserStatusChangeService.save(extendedUserStatusChange);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, extendedUserStatusChange.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /extended-user-status-changes} : get all the extendedUserStatusChanges.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of extendedUserStatusChanges in body.
     */
    @GetMapping("/extended-user-status-changes")
    public ResponseEntity<List<ExtendedUserStatusChange>> getAllExtendedUserStatusChanges(Pageable pageable) {
        log.debug("REST request to get a page of ExtendedUserStatusChanges");
        Page<ExtendedUserStatusChange> page = extendedUserStatusChangeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /extended-user-status-changes/:id} : get the "id" extendedUserStatusChange.
     *
     * @param id the id of the extendedUserStatusChange to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the extendedUserStatusChange, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/extended-user-status-changes/{id}")
    public ResponseEntity<ExtendedUserStatusChange> getExtendedUserStatusChange(@PathVariable Long id) {
        log.debug("REST request to get ExtendedUserStatusChange : {}", id);
        Optional<ExtendedUserStatusChange> extendedUserStatusChange = extendedUserStatusChangeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(extendedUserStatusChange);
    }

    /**
     * {@code DELETE  /extended-user-status-changes/:id} : delete the "id" extendedUserStatusChange.
     *
     * @param id the id of the extendedUserStatusChange to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/extended-user-status-changes/{id}")
    public ResponseEntity<Void> deleteExtendedUserStatusChange(@PathVariable Long id) {
        log.debug("REST request to delete ExtendedUserStatusChange : {}", id);
        extendedUserStatusChangeService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/extended-user-status-changes?query=:query} : search for the extendedUserStatusChange corresponding
     * to the query.
     *
     * @param query    the query of the extendedUserStatusChange search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/extended-user-status-changes")
    public ResponseEntity<Page<ExtendedUserStatusChange>> searchExtendedUserStatusChanges(@RequestParam String query, Pageable pageable) {
        Page<ExtendedUserStatusChange> page = extendedUserStatusChangeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @GetMapping("/_refetch/extended-user-status-changes")
    public ResponseEntity<List<ExtendedUserStatusChange>> refetch() {
        Page<ExtendedUserStatusChange> page = extendedUserStatusChangeService.findAll(Pageable.unpaged());
        extendedUserStatusChangeService.saveAll(page.getContent());
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
