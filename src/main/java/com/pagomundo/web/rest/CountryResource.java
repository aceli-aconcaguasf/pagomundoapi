package com.pagomundo.web.rest;

import com.google.common.collect.Streams;
import com.pagomundo.domain.Country;
import com.pagomundo.domain.DTO.CountryDTO;
import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.security.AuthoritiesConstants;
import com.pagomundo.service.CountryService;
import com.pagomundo.service.ExtendedUserService;
import com.pagomundo.service.TranslateService;
import com.pagomundo.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing {@link com.pagomundo.domain.Country}.
 */
@RestController
public class CountryResource {

    private static final String ENTITY_NAME = "country";
    private final Logger log = LoggerFactory.getLogger(CountryResource.class);
    private final CountryService countryService;
    private final ExtendedUserService extendedUserService;
    @Value("${jhipster.clientApp.name}")
    private String applicationName;
    private final TranslateService translateService;

    public CountryResource(
        CountryService countryService,
        ExtendedUserService extendedUserService,
        TranslateService translateService
    ) {
        this.countryService = countryService;
        this.extendedUserService = extendedUserService;
        this.translateService = translateService;
    }

    /**
     * {@code POST  /api/countries} : Create a new country.
     *
     * @param country the country to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new country, or with status {@code 400 (Bad Request)} if the country has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/api/countries")
    @Secured({AuthoritiesConstants.SUPER_ADMIN, AuthoritiesConstants.ADMIN})
    public ResponseEntity<Country> createCountry(@Valid @RequestBody Country country) throws URISyntaxException {
        log.debug("REST request to save Country : {}", country);
        if (country.getId() != null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.country.exist"), ENTITY_NAME, "idexists");
        }
        Country result = countryService.save(country);
        countryService.refetch(result.getId());

        return ResponseEntity.created(new URI("/api/countries/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /api/countries} : Updates an existing country.
     *
     * @param country the country to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated country,
     * or with status {@code 400 (Bad Request)} if the country is not valid,
     * or with status {@code 500 (Internal Server Error)} if the country couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/api/countries")
    @Secured({AuthoritiesConstants.SUPER_ADMIN, AuthoritiesConstants.ADMIN})
    public ResponseEntity<Country> updateCountry(@Valid @RequestBody Country country) throws URISyntaxException {
        log.debug("REST request to update Country : {}", country);
        if (country.getId() == null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.invalid.id"), ENTITY_NAME, "idnull");
        }
        Country result = countryService.save(country);
        countryService.refetch(result.getId());

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, country.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET /api/countries} : get all the countries.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of countries in body.
     */
    @GetMapping("/api/countries")
    public ResponseEntity<List<Country>> getAllCountries(Pageable pageable) {
        log.debug("REST request to get a page of Countries");
        Page<Country> page = countryService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /api/countries/:id} : get the "id" country.
     *
     * @param id the id of the country to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the country, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/api/countries/{id}")
    public ResponseEntity<Country> getCountry(@PathVariable Long id) {
        log.debug("REST request to get Country : {}", id);
        Optional<Country> country = countryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(country);
    }

    /**
     * {@code DELETE  /api/countries/:id} : delete the "id" country.
     *
     * @param id the id of the country to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/api/countries/{id}")
    @Secured({AuthoritiesConstants.SUPER_ADMIN})
    public ResponseEntity<Void> deleteCountry(@PathVariable Long id) {
        log.debug("REST request to delete Country : {}", id);
        countryService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /api/_search/countries?query=:query} : search for the country corresponding
     * to the query.
     *
     * @param query    the query of the country search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/api/_search/countries")
    public ResponseEntity<Page<Country>> searchCountries(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to SEARCH Countries.");
        Page<Country> page = countryService.search(query, pageable);
        return ResponseEntity.ok()
            .headers(PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page))
            .body(page);
    }

    /**
     * {@code SEARCH  /v1/countries} : search for the country corresponding to the query.
     *
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/v1/countries")
    @Secured({AuthoritiesConstants.RESELLER, AuthoritiesConstants.MERCHANT})
    public ResponseEntity<Page<CountryDTO>> v1SearchCountries(Pageable pageable) {
        log.debug("REST request to SEARCH Countries.");
        ExtendedUser eu = extendedUserService.getCurrentExtendedUser();
        if (Status.CANCELLED.equals(eu.getStatus()))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.incorrect.status") + " " + eu.getStatus() + " " + translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.this.action"),
                CountryService.ENTITY_NAME, ExtendedUserService.INCORRECT_STATUS);

        List<CountryDTO> page = Streams.stream(countryService.findAll())
            .map(CountryDTO::new).collect(Collectors.toList());

        Page<CountryDTO> countryDTOPage = new PageImpl<>(page, pageable, page.size());
        return ResponseEntity.ok()
            .headers(PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), countryDTOPage))
            .body(countryDTOPage);
    }

    /**
     * {@code SEARCH  /v1/countries/forPayees} : search for the country corresponding to the query.
     *
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/v1/countries/forPayees")
    @Secured({AuthoritiesConstants.RESELLER, AuthoritiesConstants.MERCHANT})
    public ResponseEntity<Page<CountryDTO>> v1SearchCountriesForPayees(Pageable pageable) {
        log.debug("REST request to SEARCH Countries for Payees.");
        ExtendedUser eu = extendedUserService.getCurrentExtendedUser();
        if (Status.CANCELLED.equals(eu.getStatus()))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.incorrect.status") + " " + eu.getStatus() + " " + translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.this.action"),
                CountryService.ENTITY_NAME, ExtendedUserService.INCORRECT_STATUS);

        List<CountryDTO> page = Streams.stream(countryService.findAll())
            .filter(Country::isForPayee)
            .map(CountryDTO::new).collect(Collectors.toList());

        Page<CountryDTO> pageDTO = new PageImpl<>(page, pageable, page.size());
        return ResponseEntity.ok()
            .headers(PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), pageDTO))
            .body(pageDTO);
    }

    @GetMapping("/api/_refetch/countries")
    public ResponseEntity<List<Country>> refetch() {
        Page<Country> page = countryService.findAll(Pageable.unpaged());
        countryService.saveAll(page.getContent());
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code PUT  /api/countries/currencies} : update currencies of countries.
     *
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @PutMapping("/api/countries/currencies")
    @Secured({AuthoritiesConstants.SUPER_ADMIN})
    public ResponseEntity<Void> updateCurrencies() throws IOException {
        log.debug("REST request to update currencies of countries.");
        countryService.updateCurrency();
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, null)).build();
    }
}
