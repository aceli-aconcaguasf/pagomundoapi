package com.pagomundo.web.rest;

import com.pagomundo.domain.DTO.AttachmentDTO;
import com.pagomundo.domain.DTO.ExtendedUserListDTO;
import com.pagomundo.domain.DTO.MerchantDTO;
import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.User;
import com.pagomundo.security.AuthoritiesConstants;
import com.pagomundo.service.ExtendedUserService;
import com.pagomundo.service.FileService;
import com.pagomundo.service.NotificationReceiverService;
import com.pagomundo.service.TranslateService;
import com.pagomundo.service.UserService;
import com.pagomundo.web.rest.errors.BadRequestAlertException;
import com.pagomundo.web.rest.errors.NotFoundAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ExtendedUser}.
 */
@RestController
public class ExtendedUserResource {

    private static final long BYTES_TO_MB = 1024L * 1024L;
    private static final long MAX_ALLOWED_FILE_SIZE_IN_MB = 5; // 5 Mb logo file size maximum allowed
    private static final String ENTITY_NAME = "extendedUser";
    private final Logger log = LoggerFactory.getLogger(ExtendedUserResource.class);
    private final ExtendedUserService extendedUserService;
    private final NotificationReceiverService notificationReceiverService;
    private final FileService fileService;
    private final UserService userService;
    private final TranslateService translateService;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public ExtendedUserResource(ExtendedUserService extendedUserService, NotificationReceiverService notificationReceiverService, UserService userService, FileService fileService, TranslateService translateService) {
        this.extendedUserService = extendedUserService;
        this.notificationReceiverService = notificationReceiverService;
        this.userService = userService;
        this.fileService = fileService;
        this.translateService = translateService;
    }

    /**
     * {@code POST  /extended-users} : Create a new extendedUser.
     *
     * @param extendedUser the extendedUser to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new extendedUser, or with status {@code 400 (Bad Request)} if the extendedUser has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping(value={"/api/extended-users", "/v1/extended-users"})
    public ResponseEntity<ExtendedUser> createExtendedUser(@Valid @RequestBody ExtendedUser extendedUser) throws URISyntaxException {
        log.debug("REST request to save ExtendedUser : {}", extendedUser);
        if (extendedUser.getId() != null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.extended.user.exist"), ENTITY_NAME, "idexists");
        }

        User user = userService.findOne(extendedUser.getUser().getId()).orElseThrow(() ->
            new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.missing") + " " + extendedUser.getUser().getId(), ENTITY_NAME, "missingUser"));
        if (user.isPayeeAsCompany() == null) user.setPayeeAsCompany(false);

        user = userService.updateUserFirstNameLastNameAndLangKey(user, extendedUser.getFirstName1(), extendedUser.getLastName1(), extendedUser.getPostalCountry());

        extendedUser.setUser(user);
        ExtendedUser result = extendedUserService.create(extendedUser);
        extendedUserService.refetch(result.getId());
        notificationReceiverService.sendExtendedUserNotification(result, extendedUserService.getCurrentExtendedUser());

        return ResponseEntity.created(new URI("/api/extended-users/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /extended-users} : Updates an existing extendedUser.
     *
     * @param extendedUser the extendedUser to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated extendedUser,
     * or with status {@code 400 (Bad Request)} if the extendedUser is not valid,
     * or with status {@code 500 (Internal Server Error)} if the extendedUser couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/api/extended-users")
    public ResponseEntity<ExtendedUser> updateExtendedUser(@Valid @RequestBody ExtendedUser extendedUser) throws URISyntaxException {
        log.debug("REST request to update ExtendedUser : {}", extendedUser);
        if (extendedUser.getId() == null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.invalid.id"), ENTITY_NAME, "idnull");
        }
        ExtendedUser result = extendedUserService.update(extendedUser);
        extendedUserService.refetch(result.getId());
        notificationReceiverService.sendExtendedUserNotification(result, extendedUserService.getCurrentExtendedUser());

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, extendedUser.getId().toString()))
            .body(result);
    }

    /**
     * {@code POST  /api/_upload/reseller-logo} : Upload a reseller logo.
     *
     * @param attachmentDTO the attachment to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new attachment, or with status {@code 400 (Bad Request)} if the attachment has already an ID.
     */
    @PostMapping(path = "/api/_upload/reseller-logo", consumes = {"multipart/form-data"})
    @Secured({AuthoritiesConstants.SUPER_ADMIN, AuthoritiesConstants.ADMIN, AuthoritiesConstants.RESELLER})
    public ResponseEntity<ExtendedUser> createAttachmentWithFile(@ModelAttribute AttachmentDTO attachmentDTO) throws IOException {
        log.debug("MULTIPART request to save Reseller logo : '{}'", attachmentDTO.getName());
        if (attachmentDTO.getId() != null)
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.attachment.exist"), ENTITY_NAME, "idexists");

        validateFileFormat(attachmentDTO.getFile());

        ExtendedUser ceu = null;
        if (attachmentDTO.getExtendedUserId() != null)
            ceu = extendedUserService.findOne(attachmentDTO.getExtendedUserId()).orElse(extendedUserService.getCurrentExtendedUser());

        if (ceu == null)
            throw new NotFoundAlertException(ExtendedUserService.ENTITY_NAME + " id: " + attachmentDTO.getExtendedUserId(),
                ExtendedUserService.ENTITY_NAME, ExtendedUserService.NOT_FOUND);

        if (!ceu.getRole().equals(AuthoritiesConstants.RESELLER))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.extended.user.is.not") + " " + AuthoritiesConstants.RESELLER,
                ExtendedUserService.ENTITY_NAME, ExtendedUserService.BAD_REQUEST);

        ExtendedUser extendedUser = extendedUserService.uploadLogo(attachmentDTO.getFile(), ceu);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true,
                ExtendedUserService.ENTITY_NAME, extendedUser.getId().toString()))
            .body(extendedUser);
    }

    private void validateFileFormat(MultipartFile file) {
        if (file == null || file.isEmpty())
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.missing.file"),
                ExtendedUserService.ENTITY_NAME, ExtendedUserService.BAD_REQUEST);

        if (file.getSize() <= 0 || file.getSize() / BYTES_TO_MB > MAX_ALLOWED_FILE_SIZE_IN_MB)
            throw new BadRequestAlertException(
                translateService.getMessageByCurrentUserParamsNull("error.message.exception.file.size") + " " + file.getSize() / BYTES_TO_MB + " " + translateService.getMessageByCurrentUserParamsNull("error.message.exception.mb.not.valid") + " " +
                    translateService.getMessageByCurrentUserParamsNull("error.message.exception.max.allowed.size") + " " + MAX_ALLOWED_FILE_SIZE_IN_MB + " " + translateService.getMessageByCurrentUserParamsNull("error.message.exception.mb"),
                ExtendedUserService.ENTITY_NAME, ExtendedUserService.BAD_REQUEST);

        String extension = FilenameUtils.getExtension(file.getOriginalFilename());
        if (extension != null &&
            !(extension.equalsIgnoreCase("jpg") ||
                extension.equalsIgnoreCase("png") ||
                extension.equalsIgnoreCase("svg"))
        )
            throw new BadRequestAlertException(
                translateService.getMessageByCurrentUserParamsNull("error.message.exception.file.extension") + translateService.getMessageByCurrentUserParamsNull("extension + error.message.exception.file.extension.allowed"),
                ExtendedUserService.ENTITY_NAME, ExtendedUserService.BAD_REQUEST);
    }

    /**
     * {@code GET  /extended-users} : get all the extendedUsers.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of extendedUsers in body.
     */
    @GetMapping("/api/extended-users")
    public ResponseEntity<List<ExtendedUser>> getAllExtendedUsers(Pageable pageable) {
        log.debug("REST request to get a page of ExtendedUsers");
        Page<ExtendedUser> page = extendedUserService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /extended-users/:id} : get the "id" extendedUser.
     *
     * @param id the id of the extendedUser to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the extendedUser, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/api/extended-users/{id}")
    public ResponseEntity<ExtendedUser> getExtendedUser(@PathVariable Long id) {
        log.debug("REST request to get ExtendedUser : {}", id);
        Optional<ExtendedUser> extendedUser = extendedUserService.findOne(id);
        return ResponseUtil.wrapOrNotFound(extendedUser);
    }

    /**
     * {@code DELETE  /extended-users/:id} : delete the "id" extendedUser.
     *
     * @param id the id of the extendedUser to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/api/extended-users/{id}")
    public ResponseEntity<Void> deleteExtendedUser(@PathVariable Long id) {
        log.debug("REST request to delete ExtendedUser : {}", id);
        extendedUserService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/extended-users?query=:query} : search for the extendedUser corresponding
     * to the query.
     *
     * @param query the query of the extendedUser search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/api/_search/extended-users")
    public ResponseEntity<Page<ExtendedUser>> searchExtendedUsers(@RequestParam String query, Pageable pageable) {
    	log.info("query de filtrado elastic: {}",query);    	
        Page<ExtendedUser> page = extendedUserService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    /**
     * {@code GET  /v1/accounts : get the current user information.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the extendedUser, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/v1/accounts")
    @Secured({AuthoritiesConstants.MERCHANT, AuthoritiesConstants.RESELLER})
    public ResponseEntity<MerchantDTO> v1Users() {
        MerchantDTO merchant = new MerchantDTO(extendedUserService.getCurrentExtendedUser());
        return ResponseUtil.wrapOrNotFound(Optional.of(merchant));
    }

    /**
     * {@code GET  /api/accounts : get the current user information.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the extendedUser, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/api/basic-profile")
    public ResponseEntity<MerchantDTO> basicProfile() {
        return ResponseUtil.wrapOrNotFound(Optional.of(new MerchantDTO(extendedUserService.getCurrentExtendedUser())));
    }

    @GetMapping("/api/merchant")
    @Secured({AuthoritiesConstants.MERCHANT, AuthoritiesConstants.RESELLER})
    public ResponseEntity<MerchantDTO> getMerchant() {
        return v1Users();
    }

    @GetMapping("/api/_refetch/extended-users")
    public ResponseEntity<List<ExtendedUser>> refetch() {
        Page<ExtendedUser> page = extendedUserService.findAll(Pageable.unpaged());
        extendedUserService.saveAll(page.getContent());
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }


    @GetMapping("/api/_refetch/extended-users/{id}")
    public ResponseEntity<List<ExtendedUser>> refetchId(@PathVariable Long id) {
        extendedUserService.refetch(id);
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    /**
     * {@code PUT  /_multi/extended-users} : Update extendedUsers.
     *
     * @param extendedUserListDTO the extendedUsers to update.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dose, or with status {@code 400 (Bad Request)} if the transaction has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/api/_multi/extended-users")
    public ResponseEntity<ExtendedUserListDTO> updateMultiExtenderUser(@RequestBody ExtendedUserListDTO extendedUserListDTO) throws URISyntaxException {
        log.debug("REST request to update a group of extendedUsers : {}", extendedUserListDTO);

        if (extendedUserListDTO.getExtendedUsers().isEmpty())
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.extended.user.not.update"), ENTITY_NAME, "empty");

        if (extendedUserListDTO.getExtendedUsers().stream().anyMatch(extendedUser -> extendedUser.getId() == null))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.extended.user.not.exist"), ENTITY_NAME, "missingId");

        ExtendedUserListDTO result = extendedUserService.update(extendedUserListDTO);
        extendedUserService.refetchAll(result.getExtendedUsers());
        notificationReceiverService.sendExtendedUserNotification(result.getExtendedUsers(), extendedUserService.getCurrentExtendedUser());

        return ResponseEntity.created(new URI("/api/_multi/extended-users/" + ""))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, null))
            .body(result);
    }

    /**
     * {@code POST  /_multi/extended-users} : Update extendedUsers.
     *
     * @param extendedUserListDTO the extendedUsers to update.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dose, or with status {@code 400 (Bad Request)} if the transaction has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/api/_multi/extended-users")
    public ResponseEntity<ExtendedUserListDTO> createMultiExtenderUser(@RequestBody ExtendedUserListDTO extendedUserListDTO) throws URISyntaxException {
        log.debug("REST request to create a group of extendedUsers : {}", extendedUserListDTO);

        if (extendedUserListDTO.getExtendedUsers().isEmpty())
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.extended.user.not.create"), ENTITY_NAME, "empty");

        if (extendedUserListDTO.getExtendedUsers().stream().anyMatch(extendedUser -> extendedUser.getId() != null))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.extended.user.exist"), ENTITY_NAME, "wrongId");

        ExtendedUserListDTO result = extendedUserService.create(extendedUserListDTO);
        extendedUserService.refetchAll(result.getExtendedUsers());
        notificationReceiverService.sendExtendedUserNotification(result.getExtendedUsers(), extendedUserService.getCurrentExtendedUser());

        return ResponseEntity.created(new URI("/api/_multi/extended-users/" + ""))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, null))
            .body(result);
    }

    /**
     * generateExcelExtendedUser
     *
     * Generate excel for extended user
     *
     * @param query parameter for filter
     * @param chartTitle report title
     */
    @GetMapping("/api/_report/extended-users")
    public void generateExcelExtendedUser(@RequestParam String query, @RequestParam String chartTitle) {
        // A list by extended-users filtered by query
    	try {
        Iterable<ExtendedUser> iterableExtendedUser = extendedUserService.search(query);
        // User
        ExtendedUser user = extendedUserService.getCurrentExtendedUser();
        // Generate excel
        log.info("generateExcelExtendedUser query: {}",query);
        fileService.generateExcelExtendedUser(query, chartTitle, iterableExtendedUser, null, user);
    	} catch (Exception e) {
    		log.error("Error generateExcelExtendedUser: {}",e.getMessage());
    		throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, "wrongId");
    	}
    }
}
