package com.pagomundo.web.rest.errors;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.util.HashMap;

public class InvalidWithdrawalException extends AbstractThrowableProblem {
    public static final Status STATUS_CODE = Status.EXPECTATION_FAILED;
    private final String entityName;
    private final String errorCode;

    public InvalidWithdrawalException(String detail, String entityName, String errorCode) {
        super(null, STATUS_CODE.getReasonPhrase(), STATUS_CODE, detail, null, null, new HashMap<>());
        this.entityName = entityName;
        this.errorCode = errorCode;
    }

    public String getEntityName() {
        return entityName;
    }

    public String getErrorCode() {
        return errorCode;
    }

}
