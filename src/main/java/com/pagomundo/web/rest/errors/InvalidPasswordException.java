package com.pagomundo.web.rest.errors;

import com.pagomundo.service.UserService;

public class InvalidPasswordException extends BadRequestAlertException {

    public InvalidPasswordException(String message) {
        super(message, UserService.ENTITY_NAME, UserService.PASSWORD_INCORRECT_CODE);
    }

    public InvalidPasswordException() {
        this("Invalid password");
    }
}
