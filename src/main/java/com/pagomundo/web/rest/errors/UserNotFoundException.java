package com.pagomundo.web.rest.errors;

import com.pagomundo.service.UserService;

public class UserNotFoundException extends NotFoundAlertException {

    private static final long serialVersionUID = 1L;

    public UserNotFoundException(String defaultMessage) {
        super(defaultMessage, UserService.ENTITY_NAME, UserService.USER_NOT_FOUND_CODE);
    }
}
