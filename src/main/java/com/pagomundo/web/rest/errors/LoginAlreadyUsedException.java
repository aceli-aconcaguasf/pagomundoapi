package com.pagomundo.web.rest.errors;

import com.pagomundo.service.UserService;

public class LoginAlreadyUsedException extends BadRequestAlertException {

    private static final long serialVersionUID = 1L;

    public LoginAlreadyUsedException(String message) {
        super("Login name already used: " + message, UserService.ENTITY_NAME, UserService.LOGIN_NAME_ALREADY_USED_CODE);
    }
}
