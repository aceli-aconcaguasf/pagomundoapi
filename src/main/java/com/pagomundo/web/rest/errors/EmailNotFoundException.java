package com.pagomundo.web.rest.errors;

import com.pagomundo.service.UserService;

public class EmailNotFoundException extends NotFoundAlertException {

    public EmailNotFoundException(String emailAddress) {
        super("Email address not registered: " + emailAddress, UserService.ENTITY_NAME, UserService.EMAIL_NOT_REGISTERED_CODE);
    }
}
