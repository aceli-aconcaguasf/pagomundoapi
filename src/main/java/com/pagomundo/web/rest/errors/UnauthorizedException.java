package com.pagomundo.web.rest.errors;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.util.HashMap;

public class UnauthorizedException extends AbstractThrowableProblem {
    private static final Status STATUS_CODE = Status.UNAUTHORIZED;
    private final String entityName;
    private final String errorCode;

    public UnauthorizedException(String detail, String entityName, String errorCode) {
        super(null, STATUS_CODE.getReasonPhrase(), STATUS_CODE, detail, null, null, new HashMap<>());
        this.entityName = entityName;
        this.errorCode = errorCode;
    }

    public String getEntityName() {
        return entityName;
    }

    public String getErrorCode() {
        return errorCode;
    }

}
