package com.pagomundo.web.rest.errors;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.util.HashMap;

public class BadRequestAlertException extends AbstractThrowableProblem {
    private static final Status STATUS_CODE = Status.BAD_REQUEST;
    private final String entityName;
    private final String errorCode;

    public BadRequestAlertException(String detail, String entityName, String errorCode) {
        super(null, STATUS_CODE.getReasonPhrase(), STATUS_CODE, detail, null, null, new HashMap<>());
        this.entityName = entityName;
        this.errorCode = errorCode;
    }

    String getEntityName() {
        return entityName;
    }

    String getErrorCode() {
        return errorCode;
    }

}
