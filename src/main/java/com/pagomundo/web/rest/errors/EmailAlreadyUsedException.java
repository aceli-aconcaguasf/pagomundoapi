package com.pagomundo.web.rest.errors;

import com.pagomundo.service.UserService;

public class EmailAlreadyUsedException extends BadRequestAlertException {

    public EmailAlreadyUsedException(String emailAddress) {
        super("Email is already in use: " + emailAddress, UserService.ENTITY_NAME, UserService.EMAIL_ALREADY_EXISTS_CODE);
    }
}
