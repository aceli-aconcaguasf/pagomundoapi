package com.pagomundo.web.rest;

import com.pagomundo.domain.NotificationStatusChange;
import com.pagomundo.service.NotificationStatusChangeService;
import com.pagomundo.service.TranslateService;
import com.pagomundo.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.pagomundo.domain.NotificationStatusChange}.
 */
@RestController
@RequestMapping("/api")
public class NotificationStatusChangeResource {

    private static final String ENTITY_NAME = "notificationStatusChange";
    private final Logger log = LoggerFactory.getLogger(NotificationStatusChangeResource.class);
    private final NotificationStatusChangeService notificationStatusChangeService;
    private final TranslateService translateService;
    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public NotificationStatusChangeResource(NotificationStatusChangeService notificationStatusChangeService, TranslateService translateService) {
        this.notificationStatusChangeService = notificationStatusChangeService;
        this.translateService = translateService;
    }

    /**
     * {@code POST  /notification-status-changes} : Create a new notificationStatusChange.
     *
     * @param notificationStatusChange the notificationStatusChange to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new notificationStatusChange, or with status {@code 400 (Bad Request)} if the notificationStatusChange has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/notification-status-changes")
    public ResponseEntity<NotificationStatusChange> createNotificationStatusChange(@Valid @RequestBody NotificationStatusChange notificationStatusChange) throws URISyntaxException {
        log.debug("REST request to save NotificationStatusChange : {}", notificationStatusChange);
        if (notificationStatusChange.getId() != null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.notification.status.change"), ENTITY_NAME, "idexists");
        }
        NotificationStatusChange result = notificationStatusChangeService.save(notificationStatusChange);
        return ResponseEntity.created(new URI("/api/notification-status-changes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /notification-status-changes} : Updates an existing notificationStatusChange.
     *
     * @param notificationStatusChange the notificationStatusChange to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated notificationStatusChange,
     * or with status {@code 400 (Bad Request)} if the notificationStatusChange is not valid,
     * or with status {@code 500 (Internal Server Error)} if the notificationStatusChange couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/notification-status-changes")
    public ResponseEntity<NotificationStatusChange> updateNotificationStatusChange(@Valid @RequestBody NotificationStatusChange notificationStatusChange) throws URISyntaxException {
        log.debug("REST request to update NotificationStatusChange : {}", notificationStatusChange);
        if (notificationStatusChange.getId() == null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.invalid.id"), ENTITY_NAME, "idnull");
        }
        NotificationStatusChange result = notificationStatusChangeService.save(notificationStatusChange);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, notificationStatusChange.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /notification-status-changes} : get all the notificationStatusChanges.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of notificationStatusChanges in body.
     */
    @GetMapping("/notification-status-changes")
    public ResponseEntity<List<NotificationStatusChange>> getAllNotificationStatusChanges(Pageable pageable) {
        log.debug("REST request to get a page of NotificationStatusChanges");
        Page<NotificationStatusChange> page = notificationStatusChangeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /notification-status-changes/:id} : get the "id" notificationStatusChange.
     *
     * @param id the id of the notificationStatusChange to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the notificationStatusChange, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/notification-status-changes/{id}")
    public ResponseEntity<NotificationStatusChange> getNotificationStatusChange(@PathVariable Long id) {
        log.debug("REST request to get NotificationStatusChange : {}", id);
        Optional<NotificationStatusChange> notificationStatusChange = notificationStatusChangeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(notificationStatusChange);
    }

    /**
     * {@code DELETE  /notification-status-changes/:id} : delete the "id" notificationStatusChange.
     *
     * @param id the id of the notificationStatusChange to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/notification-status-changes/{id}")
    public ResponseEntity<Void> deleteNotificationStatusChange(@PathVariable Long id) {
        log.debug("REST request to delete NotificationStatusChange : {}", id);
        notificationStatusChangeService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/notification-status-changes?query=:query} : search for the notificationStatusChange corresponding
     * to the query.
     *
     * @param query    the query of the notificationStatusChange search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/notification-status-changes")
    public ResponseEntity<Page<NotificationStatusChange>> searchNotificationStatusChanges(@RequestParam String query, Pageable pageable) {
        Page<NotificationStatusChange> page = notificationStatusChangeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @GetMapping("/_refetch/notification-status-changes")
    public ResponseEntity<List<NotificationStatusChange>> refetch() {
        Page<NotificationStatusChange> page = notificationStatusChangeService.findAll(Pageable.unpaged());
        notificationStatusChangeService.saveAll(page.getContent());
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
