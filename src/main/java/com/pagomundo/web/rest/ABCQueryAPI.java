package com.pagomundo.web.rest;


import com.pagomundo.domain.DTO.abc.*;
import com.pagomundo.service.integrations.abc.ConverterMultiService;
import com.pagomundo.service.integrations.abc.ConverterMultiServiceAxis;
import com.pagomundo.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.JAXBException;
import java.net.URI;
import java.net.URISyntaxException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/bank")
public class ABCQueryAPI {
    private final Logger log = LoggerFactory.getLogger(ABCQueryAPI.class);
    private static final String ENTITY_NAME = "QueryABC";
    private final MessageSource messageSource;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public ABCQueryAPI(MessageSource messageSource) {
        this.messageSource = messageSource;
    }


    @GetMapping("/query-neighborhood-id")
    public ResponseEntity<NeighborhoodResponseDTO> getNeighborhoodId(@RequestBody NeighborhoodQueryDTO neighborhoodQueryDTO) throws URISyntaxException {
        log.info("REST request to query neighborhood id from ABC  : {}", neighborhoodQueryDTO);

        // Input Validation

        if ((neighborhoodQueryDTO==null)||
            ((neighborhoodQueryDTO.getZipCode()==null||neighborhoodQueryDTO.getZipCode().isEmpty()) &&
             (neighborhoodQueryDTO.getNeighborhoodName()==null||neighborhoodQueryDTO.getNeighborhoodName().isEmpty()))){
            throw new BadRequestAlertException("Error", ENTITY_NAME, "empty");
        }

        log.info("===> REST before to call ABC web service");

        ConverterMultiServiceAxis converterMultiServiceAxis = new ConverterMultiServiceAxis();
        NeighborhoodResponseDTO response;

        try {
            response = converterMultiServiceAxis.getNeighborhoodId(neighborhoodQueryDTO);
        } catch (RemoteException | JAXBException e) {
            log.info("===> Error : {}", e.getMessage());
            log.info("===> Error : {}", e);
            throw new BadRequestAlertException("Error", ENTITY_NAME, "empty");
        }

        log.info("===> REST response to neighborhood id frome ABC  : {}", response);

        //return result;
        if (response ==null){
            throw new BadRequestAlertException("Error", ENTITY_NAME, "empty");
        }

        return ResponseEntity.created(new URI("/api/query-neighborhood-id"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, ENTITY_NAME))
            .body(response);
    }


    @GetMapping("/query-account-balance")
    public ResponseEntity<BalanceResponseDTO> getAccountBalance(@RequestBody BalanceQueryDTO balanceQueryDTO) throws URISyntaxException {
        log.info("REST request to query Account Balance from ABC  : {}", balanceQueryDTO);

        // Input Validation

        if ((balanceQueryDTO==null)||
            ((balanceQueryDTO.getAccountNumber()==null||balanceQueryDTO.getAccountNumber().isEmpty()) &&
                (balanceQueryDTO.getChannel()==null||balanceQueryDTO.getChannel().isEmpty()))){
            throw new BadRequestAlertException("Error", ENTITY_NAME, "empty");
        }

        log.info("===> REST before to call ABC web service");

        ConverterMultiServiceAxis converterMultiServiceAxis = new ConverterMultiServiceAxis();
        BalanceResponseDTO response;

        try {
            response = converterMultiServiceAxis.getAccountBalance(balanceQueryDTO);
        } catch (RemoteException | JAXBException e) {
            throw new BadRequestAlertException("Error", ENTITY_NAME, "empty");
        }

        log.info("===> REST response to Account Balance from ABC  : {}", response);

        //return result;
        if (response ==null){
            throw new BadRequestAlertException("Error", ENTITY_NAME, "empty");
        }

        return ResponseEntity.created(new URI("/api/query-account-balance"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, ENTITY_NAME))
            .body(response);
    }


    @GetMapping("/query-account-movements")
    public ResponseEntity<AccountMovementsResponseDTO> getAccountMovements(@RequestBody AccountMovementsQueryDTO accountMovementsQueryDTO) throws URISyntaxException {
        log.info("REST request to queryAccount Movements from ABC  : {}", accountMovementsQueryDTO);

        // Input Validation

        if ((accountMovementsQueryDTO==null)||
            ((accountMovementsQueryDTO.getBookingDate()==null||accountMovementsQueryDTO.getBookingDate().isEmpty()) &&
             (accountMovementsQueryDTO.getAccountNumber()==null||accountMovementsQueryDTO.getAccountNumber().isEmpty()) &&
             (accountMovementsQueryDTO.getChannel()==null||accountMovementsQueryDTO.getChannel().isEmpty())
            )){
            throw new BadRequestAlertException("Error", ENTITY_NAME, "empty");
        }

        log.info("===> REST before to call ABC web service");

        ConverterMultiServiceAxis converterMultiServiceAxis = new ConverterMultiServiceAxis();
        AccountMovementsResponseDTO response;

        try {
            response = converterMultiServiceAxis.getAccountMovements(accountMovementsQueryDTO);
        } catch (RemoteException | JAXBException e) {
            throw new BadRequestAlertException("Error", ENTITY_NAME, "empty");
        }

        log.info("===> REST response to queryAccount Movements from ABC  : {}", response);

        //return result;
        if (response ==null){
            throw new BadRequestAlertException("Error", ENTITY_NAME, "empty");
        }

        return ResponseEntity.created(new URI("/api/query-account-movements"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, ENTITY_NAME))
            .body(response);
    }


    @GetMapping("/query-account-pending-movements")
    public ResponseEntity<AccountPendingMovementsResponseDTO> getAccountPendingMovements(@RequestBody AccountPendingMovementsQueryDTO accountPendingMovementsQueryDTO) throws URISyntaxException {
        log.info("REST request to queryAccount Pending Movements from ABC  : {}", accountPendingMovementsQueryDTO);

        // Input Validation

        if ((accountPendingMovementsQueryDTO==null)||
            ((accountPendingMovementsQueryDTO.getAccountNumber()==null||accountPendingMovementsQueryDTO.getAccountNumber().isEmpty()) &&
             (accountPendingMovementsQueryDTO.getChannel()==null||accountPendingMovementsQueryDTO.getChannel().isEmpty())
            )){
            throw new BadRequestAlertException("Error", ENTITY_NAME, "empty");
        }

        log.info("===> REST before to call ABC web service");

        ConverterMultiServiceAxis converterMultiServiceAxis = new ConverterMultiServiceAxis();


        AccountPendingMovementsResponseDTO response;

        try {
            response = converterMultiServiceAxis.getAccountPendingMovements(accountPendingMovementsQueryDTO);
        } catch (RemoteException | JAXBException e) {
            throw new BadRequestAlertException("Error", ENTITY_NAME, "empty");
        }

        log.info("===> REST response to queryAccount Pending Movements from ABC  : {}", response);

        //return result;
        if (response ==null){
            throw new BadRequestAlertException("Error", ENTITY_NAME, "empty");
        }

        return ResponseEntity.created(new URI("/api/query-account-pending-movements"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, ENTITY_NAME))
            .body(response);
    }
}
