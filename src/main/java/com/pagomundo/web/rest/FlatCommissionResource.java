package com.pagomundo.web.rest;

import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.FlatCommission;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.security.AuthoritiesConstants;
import com.pagomundo.service.ExtendedUserService;
import com.pagomundo.service.FlatCommissionService;
import com.pagomundo.web.rest.errors.BadRequestAlertException;
import com.pagomundo.web.rest.errors.NotFoundAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.pagomundo.domain.FlatCommission}.
 */
@RestController
@RequestMapping("/api")
public class FlatCommissionResource {

    private static final String ENTITY_NAME = "flatCommission";
    private final Logger log = LoggerFactory.getLogger(FlatCommissionResource.class);
    private final FlatCommissionService flatCommissionService;
    private final ExtendedUserService extendedUserService;
    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public FlatCommissionResource(FlatCommissionService flatCommissionService, ExtendedUserService extendedUserService) {
        this.flatCommissionService = flatCommissionService;
        this.extendedUserService = extendedUserService;
    }

    /**
     * {@code POST  /flat-commissions} : Create a new flatCommission.
     *
     * @param flatCommission the flatCommission to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new flatCommission, or with status {@code 400 (Bad Request)} if the flatCommission has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/flat-commissions")
    public ResponseEntity<FlatCommission> createFlatCommission(@RequestBody FlatCommission flatCommission) throws URISyntaxException {
        log.debug("REST request to save FlatCommission : {}", flatCommission);
        if (flatCommission.getId() != null) {
            throw new BadRequestAlertException("A new flatCommission cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FlatCommission result = flatCommissionService.save(flatCommission);
        return ResponseEntity.created(new URI("/api/flat-commissions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /flat-commissions} : Updates an existing flatCommission.
     *
     * @param flatCommission the flatCommission to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated flatCommission,
     * or with status {@code 400 (Bad Request)} if the flatCommission is not valid,
     * or with status {@code 500 (Internal Server Error)} if the flatCommission couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/flat-commissions")
    public ResponseEntity<FlatCommission> updateFlatCommission(@RequestBody FlatCommission flatCommission) throws URISyntaxException {
        log.debug("REST request to update FlatCommission : {}", flatCommission);
        if (flatCommission.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FlatCommission result = flatCommissionService.save(flatCommission);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, flatCommission.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /flat-commissions} : get all the flatCommissions.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of flatCommissions in body.
     */
    @GetMapping("/flat-commissions")
    public ResponseEntity<List<FlatCommission>> getAllFlatCommissions(Pageable pageable) {
        log.debug("REST request to get a page of FlatCommissions");
        Page<FlatCommission> page = flatCommissionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /flat-commissions/:id} : get the "id" flatCommission.
     *
     * @param id the id of the flatCommission to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the flatCommission, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/flat-commissions/{id}")
    public ResponseEntity<FlatCommission> getFlatCommission(@PathVariable Long id) {
        log.debug("REST request to get FlatCommission : {}", id);
        Optional<FlatCommission> flatCommission = flatCommissionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(flatCommission);
    }

    /**
     * {@code DELETE  /flat-commissions/:id} : delete the "id" flatCommission.
     *
     * @param id the id of the flatCommission to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/flat-commissions/{id}")
    public ResponseEntity<Void> deleteFlatCommission(@PathVariable Long id) {
        log.debug("REST request to delete FlatCommission : {}", id);
        flatCommissionService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/flat-commissions?query=:query} : search for the flatCommission corresponding
     * to the query.
     *
     * @param query    the query of the flatCommission search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/flat-commissions")
    public ResponseEntity<Page<FlatCommission>> searchFlatCommissions(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of FlatCommissions for query {}", query);
        Page<FlatCommission> page = flatCommissionService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @GetMapping("/api/_refetch/flat-copmmissions")
    public ResponseEntity<List<FlatCommission>> refetch() {
        Page<FlatCommission> page = flatCommissionService.findAll(Pageable.unpaged());
        flatCommissionService.saveAll(page.getContent());
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code POST  /extended-user/{merchantId}/flat-commissions} : Create a new flatCommission.
     *
     * @param flatCommissionList the flatCommissionList to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new flatCommission, or with status {@code 400 (Bad Request)} if the flatCommission has already an ID.
     */
    @PostMapping("/extended-user/{merchantId}/flat-commissions")
    @Secured({AuthoritiesConstants.SUPER_ADMIN})
    public ResponseEntity<List<FlatCommission>> createMultiFlatCommission(@PathVariable Long merchantId, @RequestBody List<FlatCommission> flatCommissionList) {
        log.debug("REST request to save multi FlatCommission : {}", flatCommissionList);
        ExtendedUser merchant = extendedUserService.findOne(merchantId).orElseThrow(() ->
            new NotFoundAlertException("Merchant id: " + merchantId, FlatCommissionService.ENTITY_NAME, ExtendedUserService.NOT_FOUND));

        if (!AuthoritiesConstants.MERCHANT.equals(merchant.getRole()))
            throw new NotFoundAlertException("Merchant id: " + merchantId, FlatCommissionService.ENTITY_NAME, ExtendedUserService.NOT_FOUND);

        if (Status.CANCELLED.equals(merchant.getStatus()))
            throw new BadRequestAlertException("Incorrect merchant's status: " + merchant.getStatus() + " for this action.",
                FlatCommissionService.ENTITY_NAME, ExtendedUserService.INCORRECT_STATUS);

        List<FlatCommission> result = flatCommissionService.create(flatCommissionList, merchant);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, FlatCommissionService.ENTITY_NAME, null))
            .body(result);

    }

    /**
     * {@code GET  /extended-user/{merchantId}/flat-commissions} : Create a new flatCommission.
     *
     * @return the result of the search.
     */
    @GetMapping("/extended-user/{merchantId}/flat-commissions")
    @Secured({AuthoritiesConstants.SUPER_ADMIN})
    public ResponseEntity<List<FlatCommission>> getMultiFlatCommission(@PathVariable Long merchantId) {
        log.debug("REST request to get multi FlatCommission for merchant: {}", merchantId);
        ExtendedUser merchant = extendedUserService.findOne(merchantId).orElseThrow(() ->
            new NotFoundAlertException("Merchant id: " + merchantId, FlatCommissionService.ENTITY_NAME, ExtendedUserService.NOT_FOUND));

        if (!AuthoritiesConstants.MERCHANT.equals(merchant.getRole()))
            throw new NotFoundAlertException("Merchant id: " + merchantId, FlatCommissionService.ENTITY_NAME, ExtendedUserService.NOT_FOUND);

        List<FlatCommission> result = flatCommissionService.findAllByMerchant(merchantId);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, FlatCommissionService.ENTITY_NAME, null))
            .body(result);

    }
}
