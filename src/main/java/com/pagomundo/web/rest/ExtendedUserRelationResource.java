package com.pagomundo.web.rest;

import com.google.common.collect.Streams;
import com.pagomundo.domain.DTO.ExtendedUserRelationListDTO;
import com.pagomundo.domain.DTO.InvitedUserDTO;
import com.pagomundo.domain.DTO.MerchantForResellerDTO;
import com.pagomundo.domain.DTO.PayeeForMerchantDTO;
import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.ExtendedUserRelation;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.repository.search.RefetchService;
import com.pagomundo.security.AuthoritiesConstants;
import com.pagomundo.service.ExtendedUserRelationService;
import com.pagomundo.service.ExtendedUserService;
import com.pagomundo.service.FileService;
import com.pagomundo.service.TranslateService;
import com.pagomundo.service.UserService;
import com.pagomundo.service.dto.UserDTO;
import com.pagomundo.web.rest.errors.BadRequestAlertException;
import com.pagomundo.web.rest.errors.NotFoundAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing {@link com.pagomundo.domain.ExtendedUserRelation}.
 */
@RestController
public class ExtendedUserRelationResource {

    private final Logger log = LoggerFactory.getLogger(ExtendedUserRelationResource.class);
    private final ExtendedUserRelationService extendedUserRelationService;
    private final ExtendedUserService extendedUserService;
    private final RefetchService refetchService;
    private final UserService userService;
    private final FileService fileService;
    private final TranslateService translateService;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public ExtendedUserRelationResource(
        ExtendedUserRelationService extendedUserRelationService,
        ExtendedUserService extendedUserService,
        RefetchService refetchService,
        UserService userService,
        FileService fileService, TranslateService translateService) {
        this.extendedUserRelationService = extendedUserRelationService;
        this.extendedUserService = extendedUserService;
        this.refetchService = refetchService;
        this.userService = userService;
        this.fileService = fileService;
        this.translateService = translateService;
    }

    @GetMapping("/api/_refetch/extended-user-relations/{id}")
    public ResponseEntity<List<ExtendedUserRelation>> refetchId(@PathVariable Long id) {
        extendedUserRelationService.refetchAllByExtendedUserId(id);
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    /**
     * {@code POST  /extended-user-relations} : Create a new extendedUserRelation.
     *
     * @param eur the extendedUserRelation to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new extendedUserRelation, or with status {@code 400 (Bad Request)} if the extendedUserRelation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/api/extended-user-relations")
    public ResponseEntity<ExtendedUserRelation> createExtendedUserRelation(@RequestBody ExtendedUserRelation eur) throws URISyntaxException {
        ExtendedUserRelation result = create(eur);
        return ResponseEntity.created(new URI("/api/extended-user-relations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true,
                ExtendedUserRelationService.ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code POST  /_multi/extended-user-relations} : Create a new extendedUserRelation.
     *
     * @param extendedUserRelationListDTO the extendedUserRelation to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new extendedUserRelation, or with status {@code 400 (Bad Request)} if the extendedUserRelation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/api/_multi/extended-user-relations")
    public ResponseEntity<ExtendedUserRelationListDTO> createMultiExtendedUserRelation(@RequestBody ExtendedUserRelationListDTO extendedUserRelationListDTO) throws URISyntaxException {
        ExtendedUserRelationListDTO result = create(extendedUserRelationListDTO);
        return ResponseEntity.created(new URI("/api/_multi/extended-user-relations/" + ""))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true,
                ExtendedUserRelationService.ENTITY_NAME, null))
            .body(result);
    }

    public ExtendedUserRelationListDTO create(ExtendedUserRelationListDTO eurListDTO) {
        ArrayList<ExtendedUserRelation> extendedUserRelationsWithoutErrors = new ArrayList<>();
        ArrayList<ExtendedUserRelation> extendedUserRelationsWithErrors = new ArrayList<>();
        eurListDTO.getExtendedUserRelations().forEach(eur -> {
            try {
                extendedUserRelationsWithoutErrors.add(create(eur));
            } catch (BadRequestAlertException | NotFoundAlertException bre) {
                eur.setStatus(Status.REJECTED);
                eur.setStatusReason(bre.getMessage());
                extendedUserRelationsWithErrors.add(eur);
            }
        });
        eurListDTO.setExtendedUserRelationsWithoutErrors(extendedUserRelationsWithoutErrors);
        eurListDTO.setExtendedUserRelationsWithErrors(extendedUserRelationsWithErrors);
        eurListDTO.setExtendedUserRelations(null);
        return eurListDTO;
    }

    public ExtendedUserRelation create(ExtendedUserRelation eur) {
        log.debug("REST request to save ExtendedUserRelation : {}", eur);
        if (eur.getId() != null)
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.extended.user.relation.exist"),
                ExtendedUserRelationService.ENTITY_NAME, ExtendedUserRelationService.SHOULD_BE_NULL_CODE);

        if (eur.getExtendedUser() == null || eur.getUserRelated() == null)
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.extended.user.related.not.null"),
                ExtendedUserRelationService.ENTITY_NAME, ExtendedUserRelationService.SHOULD_NOT_BE_NULL_CODE);

        eur.setExtendedUser(extendedUserService.findOne(eur.getExtendedUser().getId()).orElseThrow(() ->
            new NotFoundAlertException(ExtendedUserService.ENTITY_NAME + " id: " + eur.getExtendedUser().getId(),
                ExtendedUserService.ENTITY_NAME, ExtendedUserService.NOT_FOUND)));

        eur.setUserRelated(userService.findOne(eur.getUserRelated().getId()).orElseThrow(() ->
            new NotFoundAlertException(UserService.ENTITY_NAME + " id: " + eur.getUserRelated().getId(),
                UserService.ENTITY_NAME, UserService.USER_NOT_FOUND_CODE)));

        return extendedUserRelationService.update(eur, extendedUserService.getCurrentExtendedUser());
    }

    /**
     * {@code PUT  /extended-user-relations} : Updates an existing extendedUserRelation.
     *
     * @param eur the extendedUserRelation to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated extendedUserRelation,
     * or with status {@code 400 (Bad Request)} if the extendedUserRelation is not valid,
     * or with status {@code 500 (Internal Server Error)} if the extendedUserRelation couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/api/extended-user-relations")
    public ResponseEntity<ExtendedUserRelation> updateExtendedUserRelation(@RequestBody ExtendedUserRelation eur) throws URISyntaxException {
        ExtendedUserRelation result = update(eur);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true,
                ExtendedUserRelationService.ENTITY_NAME, eur.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /extended-user-relations} : Updates an existing extendedUserRelation.
     *
     * @param extendedUserRelationListDTO the extendedUserRelation to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated extendedUserRelation,
     * or with status {@code 400 (Bad Request)} if the extendedUserRelation is not valid,
     * or with status {@code 500 (Internal Server Error)} if the extendedUserRelation couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/api/_multi/extended-user-relations")
    public ResponseEntity<ExtendedUserRelationListDTO> updateMultiExtendedUserRelation(@RequestBody ExtendedUserRelationListDTO extendedUserRelationListDTO) throws URISyntaxException {
        ExtendedUserRelationListDTO result = update(extendedUserRelationListDTO);
        return ResponseEntity.created(new URI("/api/_multi/extended-user-relations/" + ""))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false,
                ExtendedUserRelationService.ENTITY_NAME, null))
            .body(result);
    }

    public ExtendedUserRelationListDTO update(ExtendedUserRelationListDTO eurListDTO) {
        ArrayList<ExtendedUserRelation> extendedUserRelationsWithoutErrors = new ArrayList<>();
        ArrayList<ExtendedUserRelation> extendedUserRelationsWithErrors = new ArrayList<>();
        eurListDTO.getExtendedUserRelations().forEach(eur -> {
            try {
                extendedUserRelationsWithoutErrors.add(update(eur));
            } catch (BadRequestAlertException bre) {
                eur.setStatus(Status.REJECTED);
                eur.setStatusReason(bre.getMessage());
                extendedUserRelationsWithErrors.add(eur);
            }
        });
        eurListDTO.setExtendedUserRelationsWithoutErrors(extendedUserRelationsWithoutErrors);
        eurListDTO.setExtendedUserRelationsWithErrors(extendedUserRelationsWithErrors);
        eurListDTO.setExtendedUserRelations(null);
        return eurListDTO;
    }

    public ExtendedUserRelation update(ExtendedUserRelation eur) {
        log.debug("REST request to update ExtendedUserRelation : {}", eur);
        if (eur.getId() == null)
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.invalid.id"),
                ExtendedUserRelationService.ENTITY_NAME, ExtendedUserRelationService.SHOULD_NOT_BE_NULL_CODE);

        if (eur.getStatus() == null)
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.status.not.null"),
                ExtendedUserRelationService.ENTITY_NAME, ExtendedUserRelationService.SHOULD_NOT_BE_NULL_CODE);

        return extendedUserRelationService.update(eur, extendedUserService.getCurrentExtendedUser());
    }

    /**
     * {@code GET  /extended-user-relations} : get all the extendedUserRelations.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of extendedUserRelations in body.
     */
    @GetMapping("/api/extended-user-relations")
    public ResponseEntity<List<ExtendedUserRelation>> getAllExtendedUserRelations(Pageable pageable) {
        log.debug("REST request to get a page of ExtendedUserRelations");
        Page<ExtendedUserRelation> page = extendedUserRelationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /extended-user-relations/:id} : get the "id" extendedUserRelation.
     *
     * @param id the id of the extendedUserRelation to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the extendedUserRelation, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/api/extended-user-relations/{id}")
    public ResponseEntity<ExtendedUserRelation> getExtendedUserRelation(@PathVariable Long id) {
        log.debug("REST request to get ExtendedUserRelation : {}", id);
        Optional<ExtendedUserRelation> extendedUserRelation = extendedUserRelationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(extendedUserRelation);
    }

    /**
     * {@code DELETE  /extended-user-relations/:id} : delete the "id" extendedUserRelation.
     *
     * @param id the id of the extendedUserRelation to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/api/extended-user-relations/{id}")
    public ResponseEntity<Void> deleteExtendedUserRelation(@PathVariable Long id) {
        log.debug("REST request to delete ExtendedUserRelation : {}", id);
        extendedUserRelationService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ExtendedUserRelationService.ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code DELETE  /extended-user-relations/:id} : delete the null extendedUserRelations.
     *
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/api/extended-user-relations/null-relations")
    @Secured({AuthoritiesConstants.SUPER_ADMIN})
    public ResponseEntity<Void> deleteNullRelations() {
        log.debug("REST request to delete Null ExtendedUserRelations.");
        int size = extendedUserRelationService.deleteNullExtendedUserRelations();
        log.info("deleteNullExtendedUserRelations - Processed: {}", size);

        return ResponseEntity.noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(
                applicationName, true, ExtendedUserRelationService.ENTITY_NAME, null)).build();
    }

    /**
     * {@code SEARCH  /_search/extended-user-relations?query=:query} : search for the extendedUserRelation corresponding
     * to the query.
     *
     * @param query the query of the extendedUserRelation search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/api/_search/extended-user-relations")
    public ResponseEntity<Page<ExtendedUserRelation>> searchExtendedUserRelations(@RequestParam String query, Pageable pageable) {
        Page<ExtendedUserRelation> page = extendedUserRelationService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    /**
     * {@code SEARCH  /_search/extended-user-relations?query=:query} : search for the extendedUserRelation corresponding
     * to the query.
     *
     * @return the result of the search.
     */
    @PutMapping("/api/_search/reset-resellers")
    @Secured({AuthoritiesConstants.SUPER_ADMIN})
    public ResponseEntity<Page<ExtendedUserRelation>> resetResellersOnMerchants() {
        String query = "extendedUser.role:ROLE_RESELLER";
        Page<ExtendedUserRelation> page = extendedUserRelationService.search(query, Pageable.unpaged());
        page.getContent().forEach(eur -> {
            ExtendedUser merchant = extendedUserService.getExtendedUserFromUser(eur.getUserRelated());
            if (merchant.getId() == null) return;
            merchant.setReseller(eur.getStatus().equals(Status.ACCEPTED) ? eur.getExtendedUser() : null);
            refetchService.refetchExtendedUser(extendedUserService.save(merchant).getId());
        });

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    /**
     * {@code SEARCH  /_search/merchants-not-related-to-resellers} : search for the merchants not related.
     *
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/api/_search/merchants-not-related-to-resellers")
    public ResponseEntity<Page<ExtendedUser>> searchMerchantsNotRelated(Pageable pageable) {
        String roleFrom = "ROLE_MERCHANT";
        String roleTo = "ROLE_RESELLER";

        List<ExtendedUser> euRelated = extendedUserRelationService.getExtendedUsersRelated(roleFrom, roleTo, Status.ACCEPTED);
        List<ExtendedUser> euNotRelated = extendedUserService.getAllExtendedUsers(roleFrom, Status.ACCEPTED);

        euNotRelated.removeAll(euRelated);
        Page<ExtendedUser> euNotRelatedPage = new PageImpl<>(euNotRelated, pageable, euNotRelated.size());

        return ResponseEntity.ok()
            .headers(PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), euNotRelatedPage))
            .body(euNotRelatedPage);
    }

    /**
     * {@code SEARCH  /_search/payees-related-to-reseller} : search for the payees related to one reseller.
     *
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/api/_search/payees-related-to-reseller")
    public ResponseEntity<Page<ExtendedUser>> searchPayeesRelatedToReseller(@RequestParam String query, Pageable pageable) {
        ExtendedUser eu = getResellerFromQuery(query);
        List<ExtendedUser> payeesRelated = extendedUserRelationService.getPayeesRelatedToReseller(eu.getUser());

        Page<ExtendedUser> payeesRelatedPage = new PageImpl<>(payeesRelated, pageable, payeesRelated.size());

        return ResponseEntity.ok()
            .headers(PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), payeesRelatedPage))
            .body(payeesRelatedPage);
    }

    private ExtendedUser getResellerFromQuery(String query) {
        String[] queryArray = query.split("extendedUser.id:");
        if (queryArray.length <= 0)
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.missing") + " " + ExtendedUserService.ENTITY_NAME + " id",
                ExtendedUserService.ENTITY_NAME, ExtendedUserService.NOT_FOUND);

        String[] queryArray2 = queryArray[1].split(":");
        Long euId = Long.parseLong(queryArray2[0]);

        ExtendedUser eu = extendedUserService.findOne(euId).orElseThrow(() ->
            new NotFoundAlertException(ExtendedUserService.ENTITY_NAME + " id: " + euId,
                ExtendedUserService.ENTITY_NAME, ExtendedUserService.NOT_FOUND));

        if (!eu.getRole().equals(AuthoritiesConstants.RESELLER))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.wrong.authority") + eu.getRole(),
                UserService.ENTITY_NAME, UserService.WRONG_AUTHORITY);

        return eu;
    }

    @GetMapping("/api/_refetch/extended-user-relations")
    public ResponseEntity<List<ExtendedUserRelation>> refetch() {
        Page<ExtendedUserRelation> page = extendedUserRelationService.findAll(Pageable.unpaged());
        extendedUserRelationService.saveAll(page.getContent());
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * generateExcelExtendedUser
     * <p>
     * Generate excel statistics for extended user relation
     *
     * @param query parameter for filter
     * @param chartTitle report title
     */
    @GetMapping("/api/_report/extended-user-relations")
    public void generateExcelExtendedUser(@RequestParam String query, @RequestParam String chartTitle) {
        // A list by extended-user-relation filtered by query
        Iterable<ExtendedUserRelation> iterableExtendedUserRelation = extendedUserRelationService.search(query);

        List<ExtendedUser> extendedUserList = new ArrayList<>();
        Map<Long, ZonedDateTime> dateRelatedMap = new HashMap<>();
        // A list of extended user by extended user relation and a relation date map by key userId
        for (ExtendedUserRelation extendedUserRelation : iterableExtendedUserRelation) {
            extendedUserList.add(extendedUserRelation.getExtendedUser());
            dateRelatedMap.put(extendedUserRelation.getExtendedUser().getId(), extendedUserRelation.getDateRelated());
        }
        // User
        ExtendedUser user = extendedUserService.getCurrentExtendedUser();
        // Generate excel
        fileService.generateExcelExtendedUser(query, chartTitle, extendedUserList, dateRelatedMap, user);
    }

    /**
     * {@code GET  /v1/payees/:id} : get the "id" extendedUser.
     *
     * @param id the id of the extendedUserRelation to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the extendedUserRelation, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/v1/payees/{id}")
    @Secured({AuthoritiesConstants.MERCHANT, AuthoritiesConstants.RESELLER})
    public ResponseEntity<PayeeForMerchantDTO> v1GetPayee(@PathVariable String id) {
        ExtendedUser eu = validateCurrentExtendedUser();

        log.debug("REST request to get Payee : {} by User {}", id, eu.getEmail());

        if (AuthoritiesConstants.MERCHANT.equalsIgnoreCase(eu.getRole())) {
            
            log.debug("{} - {} Is Merchant Role validation", id, eu.getEmail());

            Optional<ExtendedUserRelation> extendedUserRelation = StringUtils.isNumeric(id)
                ? v1GetExtendedUserRelation(eu.getUser().getId(), Long.parseLong(id))
                : v1GetExtendedUserRelation(id);
            
            log.debug("{} - {} extendedUserRelation: {} ", id, eu.getEmail(), extendedUserRelation);

            if (extendedUserRelation.isEmpty())
                throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.payee.not.found"), UserService.ENTITY_NAME, UserService.USER_NOT_FOUND_CODE);

            log.debug("{} - {} after extendedUserRelation EMPTY validation", id, eu.getEmail());

            ResponseEntity<PayeeForMerchantDTO> result = ResponseUtil.wrapOrNotFound(Optional.of(new PayeeForMerchantDTO(extendedUserRelation.get())));

            log.debug("{} - {} result: {}", id, eu.getEmail(), result);

            return result;

        } else {

            log.debug("{} - {} Is NOT a Merchant", id, eu.getEmail());

            Optional<ExtendedUser> extendedUser = StringUtils.isNumeric(id)
                ? extendedUserService.findPayeeForReseller(Long.parseLong(id), eu.getId())
                : extendedUserService.findPayeeForReseller(id, eu.getId());

            log.debug("{} - {} extendedUser findPayeeForReseller: {}", id, eu.getEmail(), extendedUser);

            if (extendedUser.isEmpty())
                throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.payee.not.found"), UserService.ENTITY_NAME, UserService.USER_NOT_FOUND_CODE);
            
            log.debug("{} - {} after findPayeeForReseller EMPTY validation: {}", id, eu.getEmail(), extendedUser);

            ResponseEntity<PayeeForMerchantDTO> result = ResponseUtil.wrapOrNotFound(Optional.of(new PayeeForMerchantDTO(extendedUser.get())));

            log.debug("{} - {} result: {}", id, eu.getEmail(), result);

            return result;
        }
    }

    /**
     * {@code GET  /v1/merchants/:id} : get the "id" extendedUser.
     *
     * @param id the id of the extendedUserRelation to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the extendedUserRelation, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/v1/merchants/{id}")
    @Secured({AuthoritiesConstants.RESELLER})
    public ResponseEntity<PayeeForMerchantDTO> v1GetMerchant(@PathVariable Long id) {
        log.debug("REST request to get Merchant : {}", id);
        ExtendedUser eu = validateCurrentExtendedUser();
        Optional<ExtendedUserRelation> extendedUserRelation = v1GetExtendedUserRelation(eu.getUser().getId(), id);
        if (extendedUserRelation.isEmpty())
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.merchant.not.found"), UserService.ENTITY_NAME, UserService.USER_NOT_FOUND_CODE);
        return ResponseUtil.wrapOrNotFound(Optional.of(new PayeeForMerchantDTO(extendedUserRelation.get().getExtendedUser())));
    }

    private Optional<ExtendedUserRelation> v1GetExtendedUserRelation(Long userId, Long id) {
        return extendedUserRelationService.findAcceptedByUserIdAndExtendedUserId(userId, id);
    }

    private Optional<ExtendedUserRelation> v1GetExtendedUserRelation(String email) {
        ExtendedUser eu = extendedUserService.getCurrentExtendedUser();
        return extendedUserRelationService.findAcceptedByUserIdAndExtendedUserEmail(eu.getUser().getId(), email);
    }

    /**
     * {@code SEARCH  /v1/payees} : search for the payees of the merchant
     *
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/v1/payees")
    @Secured({AuthoritiesConstants.MERCHANT})
    public ResponseEntity<Page<PayeeForMerchantDTO>> v1GetPayees(Pageable pageable, String role) {
        log.debug("REST request to get Payees");
        ExtendedUser eu = validateCurrentExtendedUser();
        role = !AuthoritiesConstants.MERCHANT.equalsIgnoreCase(role) ? AuthoritiesConstants.PAYEE : role;

        Page<ExtendedUserRelation> page = extendedUserRelationService.search(
            "status:ACCEPTED AND userRelated.id:" + eu.getUser().getId() + " AND extendedUser.role:" + role,
            pageable);
        Page<PayeeForMerchantDTO> payeesDTOPage = new PageImpl<>(
            page.getContent().stream().map(PayeeForMerchantDTO::new).collect(Collectors.toList()),
            pageable, page.getTotalElements());
        return ResponseEntity.ok()
            .headers(PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), payeesDTOPage))
            .body(payeesDTOPage);
    }

    /**
     * {@code SEARCH  /v1/merchants} : search for the payees of the merchant
     *
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/v1/merchants")
    @Secured({AuthoritiesConstants.RESELLER})
    public ResponseEntity<Page<PayeeForMerchantDTO>> v1GetMerchants(Pageable pageable) {
        log.debug("REST request to get Merchants");
        return v1GetPayees(pageable, AuthoritiesConstants.MERCHANT);
    }

    /**
     * {@code SEARCH  /v1/merchants/payees} : search for the payees of the merchant
     *
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/v1/merchants/payees")
    @Secured({AuthoritiesConstants.RESELLER})
    public ResponseEntity<Page<MerchantForResellerDTO>> v1GetMerchantsPayees(Pageable pageable) {
        log.debug("REST request to get Merchants & Payees");
        ExtendedUser eu = validateCurrentExtendedUser();

        List<MerchantForResellerDTO> merchantForResellerDTOList = new ArrayList<>();
        extendedUserRelationService.search(
            "status:ACCEPTED AND userRelated.id:" + eu.getUser().getId() + " AND extendedUser.role:" + AuthoritiesConstants.MERCHANT,
            pageable).getContent()
            .forEach(eur -> merchantForResellerDTOList.add(new MerchantForResellerDTO(
                eur.getExtendedUser(),
                extendedUserRelationService.search(
                    "status:ACCEPTED AND userRelated.id:" + eur.getExtendedUser().getUser().getId() + " AND extendedUser.role:" + AuthoritiesConstants.PAYEE,
                    Pageable.unpaged()).getContent()
                    .stream().map(PayeeForMerchantDTO::new).collect(Collectors.toList())
            )));

        Page<MerchantForResellerDTO> payeesDTOPage = new PageImpl<>(merchantForResellerDTOList, pageable, merchantForResellerDTOList.size());
        return ResponseEntity.ok()
            .headers(PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), payeesDTOPage))
            .body(payeesDTOPage);
    }

    /**
     * {@code SEARCH  /v1/users/invited} : search for the invited users corresponding to the query.
     *
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/v1/users/invited")
    @Secured({AuthoritiesConstants.MERCHANT, AuthoritiesConstants.RESELLER})
    public ResponseEntity<Page<InvitedUserDTO>> v1GetInvitedUsers(Pageable pageable) {
        ExtendedUser eu = validateCurrentExtendedUser();

        String query = "status:ACCEPTED AND relatedBy.id:" + eu.getId() + " ";
        query += AuthoritiesConstants.MERCHANT.equalsIgnoreCase(eu.getRole())
            ? "AND (userRelated.role:ROLE_PAYEE) "
            : "AND (userRelated.role:ROLE_MERCHANT OR userRelated.role:ROLE_PAYEE) ";

        List<InvitedUserDTO> users = Streams.stream(extendedUserRelationService.search(query)).distinct()
            .map(UserDTO::new)
            .map(extendedUserService::setInvitationStatus)
            .sorted(Comparator.comparing(UserDTO::getEmail))
            .map(InvitedUserDTO::new)
            .collect(Collectors.toList());

        Page<InvitedUserDTO> payeesDTOPage = new PageImpl<>(users, pageable, users.size());
        return ResponseEntity.ok()
            .headers(PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), payeesDTOPage))
            .body(payeesDTOPage);
    }

    /**
     * {@code SEARCH  /api/_search/invited-users} : search for the invited users corresponding to the query.
     *
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/api/_search/invited-users")
    @Secured({AuthoritiesConstants.MERCHANT, AuthoritiesConstants.RESELLER})
    public ResponseEntity<Page<InvitedUserDTO>> getInvitedUsers(Pageable pageable) {
        ExtendedUser eu = validateCurrentExtendedUser();

        String query = "status:ACCEPTED AND extendedUser.id:" + eu.getId() + " ";
        query += AuthoritiesConstants.MERCHANT.equalsIgnoreCase(eu.getRole())
            ? "AND (userRelated.role:ROLE_PAYEE) "
            : "AND (userRelated.role:ROLE_MERCHANT OR userRelated.role:ROLE_PAYEE) ";

        List<InvitedUserDTO> users = Streams.stream(extendedUserRelationService.search(query)).distinct()
            .map(UserDTO::new)
            .map(extendedUserService::setInvitationStatus)
            .filter(extendedUserService::isInvited)
            .sorted(Comparator.comparing(UserDTO::getEmail))
            .map(InvitedUserDTO::new)
            .collect(Collectors.toList());

        Page<InvitedUserDTO> payeesDTOPage = new PageImpl<>(users, pageable, users.size());
        return ResponseEntity.ok()
            .headers(PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), payeesDTOPage))
            .body(payeesDTOPage);
    }

    /**
     * {@code PATCH  /v1/payees} : patch the nickname for a payee
     *
     * @param payee the payee to change the nickname
     * @return the result of the search.
     */
    @PatchMapping("/v1/payees/{id}")
    @Secured({AuthoritiesConstants.MERCHANT})
    public ResponseEntity<PayeeForMerchantDTO> v1patchPayee(@Valid @RequestBody PayeeForMerchantDTO payee, @PathVariable String id) {
        ExtendedUser eu = validateCurrentExtendedUser();

        if (id == null)
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.payee.missing.id"),
                UserService.ENTITY_NAME, UserService.MISSING_INFO);

        Optional<ExtendedUserRelation> eurOptional = StringUtils.isNumeric(id)
            ? extendedUserRelationService.findAcceptedByUserIdAndExtendedUserId(eu.getUser().getId(), Long.parseLong(id))
            : extendedUserRelationService.findAcceptedByUserIdAndExtendedUserEmail(eu.getUser().getId(), id);
        if (eurOptional.isEmpty())
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.payee.id.not.found") + " " + id,
                UserService.ENTITY_NAME, UserService.USER_NOT_FOUND_CODE);

        ExtendedUserRelation eur = eurOptional.get();
        eur.setNickname(payee.getNickname());
        PayeeForMerchantDTO result = new PayeeForMerchantDTO(extendedUserRelationService.update(eur, eu));

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true,
                ExtendedUserRelationService.ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code SEARCH  /v1/users/invited/{email}} : search for the invited users corresponding to the query.
     *
     * @param email the email of the invited user.
     * @return the result of the search.
     */
    @GetMapping("/v1/users/invited/{email}")
    @Secured({AuthoritiesConstants.MERCHANT, AuthoritiesConstants.RESELLER})
    public ResponseEntity<InvitedUserDTO> v1InvitedUsers(@PathVariable String email) {
        ExtendedUser eu = validateCurrentExtendedUser();

        if (email == null)
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.payee.missing.email"),
                UserService.ENTITY_NAME, UserService.MISSING_INFO);

        Optional<ExtendedUserRelation> eurOptional =
            extendedUserRelationService.findAcceptedPayeeByRelatedByIdAndUserEmail(eu.getId(), email);
        if (eurOptional.isEmpty())
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.payee.not.found"),
                UserService.ENTITY_NAME, UserService.USER_NOT_FOUND_CODE);

        ExtendedUserRelation eur = eurOptional.get();

        UserDTO userDTO = new UserDTO(eur.getUserRelated(), eur.getNickname());

        userDTO = extendedUserService.setInvitationStatus(userDTO);

        InvitedUserDTO invitedUserDTO = new InvitedUserDTO(userDTO);

        return ResponseUtil.wrapOrNotFound(Optional.of(invitedUserDTO));
    }

    private ExtendedUser validateCurrentExtendedUser() {
        ExtendedUser eu = extendedUserService.getCurrentExtendedUser();
        if (Status.CANCELLED.equals(eu.getStatus()))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.incorrect.status") + " " + eu.getStatus() + " " + translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.this.action"),
                UserService.ENTITY_NAME, ExtendedUserService.INCORRECT_STATUS);
        return eu;
    }
}
