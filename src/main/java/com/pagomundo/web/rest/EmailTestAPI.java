package com.pagomundo.web.rest;


import com.pagomundo.domain.DTO.abc.*;
import com.pagomundo.domain.User;
import com.pagomundo.service.MailService;
import com.pagomundo.service.integrations.abc.ConverterMultiServiceAxis;
import com.pagomundo.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.config.JHipsterProperties;
import io.github.jhipster.web.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.rmi.RemoteException;

@RestController
@RequestMapping("/api/bank")
public class EmailTestAPI {
    private final Logger log = LoggerFactory.getLogger(EmailTestAPI.class);
    private static final String ENTITY_NAME = "QueryABC";

    private MessageSource messageSource;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private SpringTemplateEngine templateEngine;

    private JavaMailSenderImpl javaMailSender;

    private JHipsterProperties jHipsterProperties;
    private final MailService mailService;

    public EmailTestAPI(MessageSource messageSource, SpringTemplateEngine templateEngine, JavaMailSenderImpl javaMailSender, JHipsterProperties jHipsterProperties, MailService mailService) {
        this.messageSource = messageSource;
        this.templateEngine = templateEngine;
        this.javaMailSender = javaMailSender;
        this.jHipsterProperties = jHipsterProperties;
        this.mailService = mailService;
    }


    @GetMapping("/emailTemplateACtivation")
    public ResponseEntity<String> getNeighborhoodId(@RequestBody UserTestMailDTO userTestMailDTO) throws URISyntaxException, MessagingException, IOException {
        log.info("REST request to activationEmailToAdmin  : {}", userTestMailDTO);
        // Input Validation
        log.info("===> REST before to call ABC web service");

        User user = new User();
        user.setLogin(userTestMailDTO.getLogin());
        user.setEmail(userTestMailDTO.getEmail());
        user.setLangKey(userTestMailDTO.getLangKey());
        user.setExtraURLParameter(userTestMailDTO.getExtraURLParameter());
        user.setFirstName("Jorge");

        String response = mailService.sendEmailFromTemplateStr(user, null, userTestMailDTO.getTemplateEmail(), userTestMailDTO.getTitleKey());

        log.info("===> REST response to neighborhood id frome ABC  : {}", response);

        //return result;
        if (response ==null){
            throw new BadRequestAlertException("Error", ENTITY_NAME, "empty");
        }

        return ResponseEntity.created(new URI("/emailTemplateACtivation"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, ENTITY_NAME))
            .body(response);
    }

}
