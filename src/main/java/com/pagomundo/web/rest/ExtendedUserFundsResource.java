package com.pagomundo.web.rest;

import com.pagomundo.domain.DTO.ExtendedUserFundsDTO;
import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.ExtendedUserFunds;
import com.pagomundo.domain.enumeration.FundType;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.security.AuthoritiesConstants;
import com.pagomundo.service.ExtendedUserFundsService;
import com.pagomundo.service.ExtendedUserService;
import com.pagomundo.service.NotificationReceiverService;
import com.pagomundo.service.TranslateService;
import com.pagomundo.web.rest.errors.BadRequestAlertException;
import com.pagomundo.web.rest.errors.NotFoundAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.pagomundo.domain.ExtendedUserFunds}.
 */
@RestController
public class ExtendedUserFundsResource {

    private final Logger log = LoggerFactory.getLogger(ExtendedUserFundsResource.class);
    private final ExtendedUserFundsService extendedUserFundsService;
    private final ExtendedUserService extendedUserService;
    private final NotificationReceiverService notificationReceiverService;
    private final TranslateService translateService;
    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public ExtendedUserFundsResource(
        ExtendedUserFundsService extendedUserFundsService,
        ExtendedUserService extendedUserService,
        NotificationReceiverService notificationReceiverService, TranslateService translateService) {
        this.extendedUserFundsService = extendedUserFundsService;
        this.extendedUserService = extendedUserService;
        this.notificationReceiverService = notificationReceiverService;
        this.translateService = translateService;
    }

    @GetMapping("/api/_refetch/extended-user-funds/{id}")
    public ResponseEntity<List<ExtendedUserFunds>> refetchId(@PathVariable Long id) {
        extendedUserFundsService.refetch(id);
        return new ResponseEntity<>(null, HttpStatus.OK);
    }
    /**
     * {@code POST  /extended-user-funds} : Create a new euf.
     *
     * @param euf the euf to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new euf,
     * or with status {@code 400 (Bad Request)} if the euf has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/api/extended-user-funds")
    @Secured({AuthoritiesConstants.SUPER_ADMIN, AuthoritiesConstants.ADMIN,
        AuthoritiesConstants.MERCHANT, AuthoritiesConstants.RESELLER})
    public ResponseEntity<ExtendedUserFunds> requestFund(@RequestBody ExtendedUserFunds euf) throws URISyntaxException {
        log.debug("REST request to create Fund : {}", euf);
        if (euf.getId() != null)
            throw new BadRequestAlertException(
                ExtendedUserFundsService.ENTITY_NAME + translateService.getMessageByCurrentUserParamsNull("error.message.exception.provided.null") + " " + euf.getId(),
                ExtendedUserFundsService.ENTITY_NAME, ExtendedUserFundsService.SHOULD_BE_NULL_CODE);

        if (euf.getValue() == null)
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.incorrect.value"),
                ExtendedUserFundsService.ENTITY_NAME, ExtendedUserFundsService.INCORRECT_VALUE);

        if (euf.getValue().compareTo(new BigDecimal(500)) < 0 && extendedUserService.isReseller())
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.incorrect.value.withdraw"),
                ExtendedUserFundsService.ENTITY_NAME, ExtendedUserFundsService.INCORRECT_VALUE);

        if (euf.getValue().compareTo(BigDecimal.ZERO) <= 0 && extendedUserService.isMerchant())
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.incorrect.value.fund"),
                ExtendedUserFundsService.ENTITY_NAME, ExtendedUserFundsService.INCORRECT_VALUE);

        ExtendedUser eu = validateCurrentExtendedUser();

        euf.setExtendedUser(
            extendedUserService.isMerchant() || extendedUserService.isReseller() ?
                extendedUserService.getCurrentExtendedUser() :
                extendedUserService.findOne(euf.getExtendedUser().getId()).orElseThrow(() ->
                    new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.extended.user.id") + " " + euf.getExtendedUser().getId(),
                        ExtendedUserFundsService.ENTITY_NAME, ExtendedUserService.NOT_FOUND))
        );

        if (euf.getValue().compareTo(eu.getBalance()) >= 0 && extendedUserService.isReseller())
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.found.insufficient.balance"),
                ExtendedUserFundsService.ENTITY_NAME, ExtendedUserFundsService.INCORRECT_VALUE);

        euf.setType(euf.getExtendedUser().getRole().equals(AuthoritiesConstants.MERCHANT) ?
            FundType.FUNDING : FundType.WITHDRAWAL);

        ExtendedUserFunds result = extendedUserFundsService.request(euf);

        notificationReceiverService.sendFundNotification(result, eu);

        return ResponseEntity.created(new URI("/api/extended-user-funds/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true,
                ExtendedUserFundsService.ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code POST  /v1/funds} : Create a new euf.
     *
     * @param euf the euf to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new euf,
     * or with status {@code 400 (Bad Request)} if the euf has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/v1/funds")
    @Secured({AuthoritiesConstants.MERCHANT})
    public ResponseEntity<ExtendedUserFundsDTO> v1RequestFund(@RequestBody ExtendedUserFunds euf) throws URISyntaxException {
        ExtendedUserFundsDTO result = new ExtendedUserFundsDTO(requestFund(euf).getBody());
        return ResponseEntity.created(new URI("/v1/funds" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true,
                ExtendedUserFundsService.ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code POST  /v1/withdrawals} : Create a new euf.
     *
     * @param euf the euf to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new euf,
     * or with status {@code 400 (Bad Request)} if the euf has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/v1/withdrawals")
    @Secured({AuthoritiesConstants.RESELLER})
    public ResponseEntity<ExtendedUserFundsDTO> v1RequestWithdrawals(@RequestBody ExtendedUserFunds euf) throws URISyntaxException {
        ExtendedUserFundsDTO result = new ExtendedUserFundsDTO(requestFund(euf).getBody());
        return ResponseEntity.created(new URI("/v1/withdrawals" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true,
                ExtendedUserFundsService.ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    private ExtendedUser validateCurrentExtendedUser() {
        ExtendedUser eu = extendedUserService.getCurrentExtendedUser();
        if (!Status.ACCEPTED.equals(eu.getStatus()))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.incorrect.status") + " " + eu.getStatus() + " " + translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.this.action"),
                ExtendedUserFundsService.ENTITY_NAME, ExtendedUserService.INCORRECT_STATUS);
        return eu;
    }

    /**
     * {@code PUT  /extended-user-funds} : Updates an existing extendedUserFunds.
     *
     * @param euf the extendedUserFunds to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated extendedUserFunds,
     * or with status {@code 400 (Bad Request)} if the extendedUserFunds is not valid,
     * or with status {@code 500 (Internal Server Error)} if the extendedUserFunds couldn't be updated.
     */
    @PutMapping("/api/extended-user-funds")
    @Secured({AuthoritiesConstants.SUPER_ADMIN, AuthoritiesConstants.ADMIN,
        AuthoritiesConstants.MERCHANT, AuthoritiesConstants.RESELLER})
    public ResponseEntity<ExtendedUserFunds> updateFund(@RequestBody ExtendedUserFunds euf) {
        log.debug("REST request to update ExtendedUserFunds : {}", euf);
        if (euf.getId() == null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.is.property.not.null"),
                ExtendedUserFundsService.ENTITY_NAME, ExtendedUserFundsService.SHOULD_NOT_BE_NULL_CODE);
        }

        ExtendedUserFunds result = extendedUserFundsService.update(euf);
        extendedUserService.refetch(result.getExtendedUser().getId());

        notificationReceiverService.sendFundNotification(result, extendedUserService.getCurrentExtendedUser());

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true,
                ExtendedUserFundsService.ENTITY_NAME, euf.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /v1/funds} : Updates an existing fund.
     *
     * @param euf the extendedUserFunds to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated extendedUserFunds,
     * or with status {@code 400 (Bad Request)} if the extendedUserFunds is not valid,
     * or with status {@code 500 (Internal Server Error)} if the extendedUserFunds couldn't be updated.
     */
    @PutMapping("/v1/funds")
    @Secured({AuthoritiesConstants.MERCHANT})
    public ResponseEntity<ExtendedUserFundsDTO> v1UpdateFund(@RequestBody ExtendedUserFunds euf) {
        ExtendedUserFundsDTO result = new ExtendedUserFundsDTO(updateFund(euf).getBody());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true,
                ExtendedUserFundsService.ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /v1/withdrawals} : Updates an existing fund.
     *
     * @param euf the extendedUserFunds to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated extendedUserFunds,
     * or with status {@code 400 (Bad Request)} if the extendedUserFunds is not valid,
     * or with status {@code 500 (Internal Server Error)} if the extendedUserFunds couldn't be updated.
     */
    @PutMapping("/v1/withdrawals")
    @Secured({AuthoritiesConstants.RESELLER})
    public ResponseEntity<ExtendedUserFundsDTO> v1UpdateWithdrawals(@RequestBody ExtendedUserFunds euf) {
        ExtendedUserFundsDTO result = new ExtendedUserFundsDTO(updateFund(euf).getBody());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true,
                ExtendedUserFundsService.ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /extended-user-funds} : get all the extendedUserFunds.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of extendedUserFunds in body.
     */
    @GetMapping("/api/extended-user-funds")
    public ResponseEntity<List<ExtendedUserFunds>> getAllExtendedUserFunds(Pageable pageable) {
        log.debug("REST request to get a page of ExtendedUserFunds");
        Page<ExtendedUserFunds> page = extendedUserFundsService.findAll(pageable);
        return ResponseEntity.ok()
            .headers(PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page))
            .body(page.getContent());
    }

    /**
     * {@code GET  /extended-user-funds/:id} : get the "id" extendedUserFunds.
     *
     * @param id the id of the extendedUserFunds to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the extendedUserFunds,
     * or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/api/extended-user-funds/{id}")
    public ResponseEntity<ExtendedUserFunds> getExtendedUserFunds(@PathVariable Long id) {
        log.debug("REST request to get ExtendedUserFunds : {}", id);
        Optional<ExtendedUserFunds> extendedUserFunds = extendedUserFundsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(extendedUserFunds);
    }

    /**
     * {@code DELETE  /extended-user-funds/:id} : delete the "id" extendedUserFunds.
     *
     * @param id the id of the extendedUserFunds to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/api/extended-user-funds/{id}")
    public ResponseEntity<Void> deleteExtendedUserFunds(@PathVariable Long id) {
        log.debug("REST request to delete ExtendedUserFunds : {}", id);
        extendedUserFundsService.delete(id);
        return ResponseEntity.noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true,
                ExtendedUserFundsService.ENTITY_NAME, id.toString()))
            .build();
    }

    /**
     * {@code SEARCH  /_search/extended-user-funds?query=:query} : search for the extendedUserFunds corresponding
     * to the query.
     *
     * @param query    the query of the extendedUserFunds search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/api/_search/extended-user-funds")
    public ResponseEntity<Page<ExtendedUserFunds>> searchExtendedUserFunds(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of ExtendedUserFunds for query {}", query);
        Page<ExtendedUserFunds> page = extendedUserFundsService.search(query, pageable);
        return ResponseEntity.ok()
            .headers(PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page))
            .body(page);
    }

    private Page<ExtendedUserFundsDTO> getFundOrWithdrawal(@RequestParam String query, Pageable pageable) {
        Long id = extendedUserService.getCurrentExtendedUser().getId();
        if (query.contains("extendedUser.id:"))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.query.malformed"),
                ExtendedUserFundsService.ENTITY_NAME, ExtendedUserFundsService.BAD_REQUEST);
        if (query.contains("category:"))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.query.malformed"),
                ExtendedUserFundsService.ENTITY_NAME, ExtendedUserFundsService.BAD_REQUEST);
        Page<ExtendedUserFunds> page = extendedUserFundsService.search(
            query + " AND extendedUser.id: " + id + " AND category:REQUESTED", pageable);
        List<ExtendedUserFunds> list = page.getContent();
        List<ExtendedUserFundsDTO> listDTO = new ArrayList<>();
        list.forEach(euf -> listDTO.add(new ExtendedUserFundsDTO(euf)));
        return new PageImpl<>(listDTO, pageable, page.getTotalElements());
    }

    /**
     * {@code SEARCH  /v1/funds} : search for the fund for the merchant.
     *
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/v1/funds")
    @Secured({AuthoritiesConstants.MERCHANT})
    public ResponseEntity<Page<ExtendedUserFundsDTO>> v1GetFunds(@RequestParam String query, Pageable pageable) {
        Page<ExtendedUserFundsDTO> listDTOPage = getFundOrWithdrawal(query, pageable);
        return ResponseEntity.ok()
            .headers(PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), listDTOPage))
            .body(listDTOPage);
    }

    /**
     * {@code SEARCH  /v1/withdrawals} : search for the fund for the merchant.
     *
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/v1/withdrawals")
    @Secured({AuthoritiesConstants.RESELLER})
    public ResponseEntity<Page<ExtendedUserFundsDTO>> v1GetWithdrawals(@RequestParam String query, Pageable pageable) {
        Page<ExtendedUserFundsDTO> listDTOPage = getFundOrWithdrawal(query, pageable);
        return ResponseEntity.ok()
            .headers(PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), listDTOPage))
            .body(listDTOPage);
    }

    @GetMapping("/api/_refetch/extended-user-funds")
    public ResponseEntity<List<ExtendedUserFunds>> refetch() {
        Page<ExtendedUserFunds> page = extendedUserFundsService.findAll(Pageable.unpaged());
        extendedUserFundsService.saveAll(page.getContent());
        return ResponseEntity.ok()
            .headers(PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page))
            .body(page.getContent());
    }

}
