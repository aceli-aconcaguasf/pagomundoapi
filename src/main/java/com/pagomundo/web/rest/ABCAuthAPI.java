package com.pagomundo.web.rest;

import com.pagomundo.domain.DTO.abc.GenerateCodeDTO;
import com.pagomundo.domain.DTO.abc.SignatureDTO;
import com.pagomundo.domain.DTO.abc.SignatureResponseDTO;
import com.pagomundo.domain.DTO.abc.AuthenticateCodeDTO;
import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.User;
import com.pagomundo.security.AuthoritiesConstants;
import com.pagomundo.service.ExtendedUserService;
import com.pagomundo.service.util.*;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.pagomundo.web.rest.errors.BadRequestAlertException;
import com.pagomundo.service.TranslateService;

import javax.validation.Valid;
import java.io.IOException;
import java.io.Reader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.FileReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.ArrayList;

import org.bouncycastle.crypto.signers.RSADigestSigner;
// import org.bouncycastle.util.io.pem.PemReader;
import org.bouncycastle.util.io.pem.PemObject;

import java.security.KeyPair;
import org.bouncycastle.openssl.*;
import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.openssl.PasswordFinder;
import org.bouncycastle.crypto.*;
import org.bouncycastle.crypto.params.*;
import org.bouncycastle.crypto.util.*;

import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Signature;
import java.security.SignatureException;
import java.security.interfaces.RSAPrivateKey;
import java.util.Base64;
import java.nio.charset.StandardCharsets;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;


import java.security.*;
import java.io.*;
import org.apache.commons.ssl.*;
import java.util.Base64;




@RestController
@RequestMapping("/api/bank")
public class ABCAuthAPI {

    private final Logger log = LoggerFactory.getLogger(ABCAuthAPI.class);
    private static final String ENTITY_NAME = "AuthABC";
    private final TranslateService translateService;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    @Value("${path-certificate}")
    private String path;

    @Value("${name-certificate}")
    private String name;

    @Value("${pass-certificate}")
    private String password;


    public ABCAuthAPI(TranslateService translateService) {
        this.translateService = translateService;
    }


    @PostMapping("/signature")
    public ResponseEntity<SignatureResponseDTO> generatpagoeSignature(@RequestBody SignatureDTO signatureDTO) throws URISyntaxException {

        log.info("REST request generatpagoeSignature: {}", signatureDTO);

        if (signatureDTO.getMonto().equals(null))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.missing"), ENTITY_NAME, "empty");

        String cadena = signatureDTO.getCuentacargo() + "|" + signatureDTO.getMonto() + "|" + signatureDTO.getConcepto() + "|" + signatureDTO.getCuentabeneficiario() + "|" + signatureDTO.getCanal();

        log.info("cadena original: {}", cadena);

        SignatureResponseDTO newSignature = new SignatureResponseDTO();
        String signature = "";

        try {

            String ContrasenaClavePrivada = password;
            byte[] CadenaOriginalUTF8= cadena.getBytes("UTF-8");

            String fileName =  path + name;
            log.info("fileName certabc: {}", fileName);

            //File file = new File(getClass().getResource(fileName).getFile());
            File file = new File(fileName);
            FileInputStream fileInputStream = new FileInputStream(file);
            
            PKCS8Key pkcs8 = new PKCS8Key(fileInputStream,ContrasenaClavePrivada.toCharArray());
            java.security.PrivateKey privatekey = pkcs8.getPrivateKey();
            
            Signature firma;					
            firma = Signature.getInstance("SHA512withRSA");
            firma.initSign(privatekey);
            firma.update(CadenaOriginalUTF8);

            byte [] cadenafirmada = firma.sign();

            signature = new String(Base64.getEncoder().encode(cadenafirmada));

            log.info("signature: {}", signature);

            newSignature.setSignature(signature);

            log.info("newSignature: {}", newSignature);

        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return ResponseEntity.created(new URI("/api/signature"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, ENTITY_NAME))
            .body(newSignature);
    }


    public String sign(String chain) throws Exception {

        log.info("Inside of sign: {}", chain);
        String retVal = "OK";
        /*
        try {
            // Signature signature = Signature.getInstance("SHA256withRSA");

            log.info("fileName Cert: {}", fileName);
            log.info("alias Cert: {}", alias);

            // String privateKey = getCertified(fileName, password, alias);
            
            signature.initSign(privateKey);
            byte[] bytes = chain.getBytes(StandardCharsets.UTF_8);
            signature.update(bytes, 0, bytes.length);

            retVal = Base64.getEncoder().encodeToString(signature.sign());
            log.debug("Signature: {}", retVal);
            

        } catch (NoSuchAlgorithmException e) {
            throw new NoSuchAlgorithmException("NoSuchAlgorithmException", e);
        } catch (InvalidKeyException e) {
            throw new InvalidKeyException("InvalidKeyException: ", e);
        } catch (SignatureException e) {
            throw new SignatureException("SignatureException", e);
        } catch (NoSuchProviderException e) {
            throw new NoSuchProviderException(e.getLocalizedMessage());
        }
        */
        return retVal;
    }

    private String getCertified(String keystoreFilename, String password, String alias) throws Exception {

        log.debug("Inside of getCertified: {}", keystoreFilename);   

        try{

            InputStream inputStream = new FileInputStream(keystoreFilename);

            log.debug("inputStream: {}", inputStream);

            Reader stReader = new InputStreamReader(inputStream);

            log.debug("stReader: {}", stReader);

            PEMReader pemReader = new PEMReader(stReader, new PasswordFinder() {
                public char[] getPassword() {
                  return password != null ? password.toCharArray() : new char[0];
                }
              });

            log.debug("PemReader: {}", pemReader);

            AsymmetricCipherKeyPair keyPair = (AsymmetricCipherKeyPair) pemReader.readObject();

            log.debug("keyPair: {}", keyPair);

            RSAPrivateCrtKeyParameters rsaP = (RSAPrivateCrtKeyParameters) keyPair.getPrivate();

            log.debug("rsaP: {}", rsaP);


        } catch (FileNotFoundException ex) {
            log.error("ex: {}", ex);
            throw new FileNotFoundException(keystoreFilename);
        } catch (IOException ex) {
            log.error("ex: {}", ex);
            throw new IOException("IOException", ex);
        } catch (Exception ex) {
            log.error("ex: {}", ex);
            throw new Exception("Exception", ex);
        }

        return "Ok";
    }


    @PostMapping("/generate-code")
    public ResponseEntity<String> generateCode(@RequestBody GenerateCodeDTO generateCodeDTO) throws URISyntaxException {
        log.info("REST request to generate-code for ABC  : {}", generateCodeDTO);

        if (generateCodeDTO.getNotifyTo().equals(null))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.missing"), ENTITY_NAME, "empty");

        if (generateCodeDTO.getDeviceId().equals(null))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.missing"), ENTITY_NAME, "empty");

        String notifyTo = generateCodeDTO.getNotifyTo(); 
        String deviceId = generateCodeDTO.getDeviceId();
        int transactionType = generateCodeDTO.getTransactionType();
        // int transactionType = 332000;
        
        String customerNumber = null;
        String idNotificationType = "SMS";
        String tokenNumber = null;
        int idAplicacion = 1;
        String idCompany = "4AD8A2A7-57A4-4731-B0ED-37EE5B17D1EF";

        log.info("===> REST before to generate-code for ABC ");

        ABCTokenAuth abctauth = new ABCTokenAuth();
        String result = abctauth.generateCode(transactionType, customerNumber, deviceId,
                                     idNotificationType, notifyTo, tokenNumber,
                                    idAplicacion, idCompany); 

        log.info("===> REST response to generate-code for ABC  : {}", result);

        //return result;
        List<String> response = new ArrayList<String>();

        return ResponseEntity.created(new URI("/api/generate-code"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, ENTITY_NAME))
            .body(result);
    }

    @PostMapping("/authenticate-code")
    public ResponseEntity<String> authenticateCode(@RequestBody AuthenticateCodeDTO authenticateCodeDTO) throws URISyntaxException {
        log.info("REST request to authenticate-code for ABC  : {}", authenticateCodeDTO);

        if (authenticateCodeDTO.getNotifyId().equals(null))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.missing"), ENTITY_NAME, "empty");

        if (authenticateCodeDTO.getDeviceId().equals(null))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.missing"), ENTITY_NAME, "empty");

        if (authenticateCodeDTO.getNotifyTo().equals(null))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.missing"), ENTITY_NAME, "empty");

        if (authenticateCodeDTO.getKeygen().equals(null))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.missing"), ENTITY_NAME, "empty");

        String idNotify = authenticateCodeDTO.getNotifyId(); 
        String deviceId = authenticateCodeDTO.getDeviceId();       
        String idNotificationType = "SMS";
        String notifyTo = authenticateCodeDTO.getNotifyTo();
        String keygen = authenticateCodeDTO.getKeygen();

        ABCTokenAuth abctauth = new ABCTokenAuth();
        String result = abctauth.authenticate(idNotify, deviceId, idNotificationType, notifyTo, keygen); 

        log.info("REST response to authenticate-code for ABC  : {}", result);

        //return result;

        List<String> response = new ArrayList<String>();

        return ResponseEntity.created(new URI("/api/authenticate-code"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, ENTITY_NAME))
            .body(result);
    }

}
