package com.pagomundo.web.rest;

import com.pagomundo.domain.DTO.PaymentListDTO;
import com.pagomundo.domain.DTO.Ripple.RippleResultGroupDTO;
import com.pagomundo.domain.DTO.TransactionByDescriptionDTO;
import com.pagomundo.domain.DTO.TransactionForMerchantDTO;
import com.pagomundo.domain.DTO.TransactionListDTO;
import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.Transaction;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.repository.search.RefetchService;
import com.pagomundo.security.AuthoritiesConstants;
import com.pagomundo.service.*;
import com.pagomundo.web.rest.errors.BadRequestAlertException;
import com.pagomundo.web.rest.errors.NotFoundAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * REST controller for managing {@link com.pagomundo.domain.Transaction}.
 */
@RestController
public class TransactionResource {

    private static final String ENTITY_NAME = "transaction";
    private final Logger log = LoggerFactory.getLogger(TransactionResource.class);
    private final RefetchService refetchService;
    private final TransactionService transactionService;
    private final ExtendedUserService extendedUserService;
    private final NotificationReceiverService notificationReceiverService;
    private final FileService fileService;
    private final TranslateService translateService;


    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public TransactionResource(
        RefetchService refetchService,
        TransactionService transactionService,
        ExtendedUserService extendedUserService,
        NotificationReceiverService notificationReceiverService,
        FileService fileService, TranslateService translateService) {
        this.refetchService = refetchService;
        this.extendedUserService = extendedUserService;
        this.transactionService = transactionService;
        this.notificationReceiverService = notificationReceiverService;
        this.fileService = fileService;
        this.translateService = translateService;
    }

    @GetMapping("/api/_refetch/all")
    public ResponseEntity<Void> refetchAll() {
        refetchService.refetchAll();
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, "")).build();
    }

    @GetMapping("/api/_refetch/transactions/{id}")
    public ResponseEntity<List<Transaction>> refetchId(@PathVariable Long id) {
        transactionService.refetch(id);
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    /**
     * {@code POST  /transactions} : Create a new transaction.
     *
     * @param transaction the transaction to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new transaction, or with status {@code 400 (Bad Request)} if the transaction has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/api/transactions")
    public ResponseEntity<Transaction> createTransaction(@Valid @RequestBody Transaction transaction) throws URISyntaxException {
        log.debug("REST request to save Transaction : {}", transaction);
        if (transaction.getId() != null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.transaction.exist"), ENTITY_NAME, "idexists");
        }
            Transaction result = transactionService.create(transaction);
        transactionService.refetch(result.getId());
        extendedUserService.refetch(result.getMerchant().getId());
        if (result.getReseller() != null) extendedUserService.refetch(result.getReseller().getId());
        notificationReceiverService.sendTransactionNotification(result, extendedUserService.getCurrentExtendedUser());

        return ResponseEntity.created(new URI("/api/transactions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /transactions} : Updates an existing transaction.
     *
     * @param transaction the transaction to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated transaction,
     * or with status {@code 400 (Bad Request)} if the transaction is not valid,
     * or with status {@code 500 (Internal Server Error)} if the transaction couldn't be updated.
     */
    @PutMapping("/api/transactions")
    public ResponseEntity<Transaction> updateTransaction(@Valid @RequestBody Transaction transaction) {
        log.debug("REST request to update Transaction : {}", transaction);
        if (transaction.getId() == null) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.transaction.not.exist"), ENTITY_NAME, "missingId");
        }
        if (transaction.getStatus() == Status.CREATED) {  // if PUT, status should not be CREATED
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.status.invalid"), ENTITY_NAME, "status");
        }

        Transaction result = transactionService.update(transaction);
        transactionService.refetch(result.getId());
        extendedUserService.refetch(result.getMerchant().getId());
        if (result.getReseller() != null) extendedUserService.refetch(result.getReseller().getId());
        if (transaction.getStatus() == Status.SETTLEMENT_DECLINED)result.setStatus(Status.SETTLEMENT_DECLINED);
        notificationReceiverService.sendTransactionNotification(result, extendedUserService.getCurrentExtendedUser());
        //if(transaction.getStatus() == Status.REVERTED)transactionGroupService.refetch(transaction.getTransactionGroup().getId());
            
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, transaction.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /transactions} : get all the transactions.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of transactions in body.
     */
    @GetMapping("/api/transactions")
    public ResponseEntity<List<Transaction>> getAllTransactions(Pageable pageable) {
        log.debug("REST request to get a page of Transactions");
        Page<Transaction> page = transactionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /transactions/:id} : get the "id" transaction.
     *
     * @param id the id of the transaction to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the transaction, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/api/transactions/{id}")
    public ResponseEntity<Transaction> getTransaction(@PathVariable Long id) {
        log.debug("REST request to get Transaction : {}", id);
        Optional<Transaction> transaction = transactionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(transaction);
    }

    /**
     * {@code DELETE  /transactions/:id} : delete the "id" transaction.
     *
     * @param id the id of the transaction to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    // @DeleteMapping("/api/transactions/{id}")
    // public ResponseEntity<Void> deleteTransaction(@PathVariable Long id) {
    //     log.debug("REST request to delete Transaction : {}", id);
    //     transactionService.delete(id);
    //     return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    // }

    /**
     * {@code SEARCH  /_search/transactions?query=:query} : search for the transaction corresponding
     * to the query.
     *
     * @param query the query of the transaction search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/api/_search/transactions")
    public ResponseEntity<Page<Transaction>> searchTransactions(@RequestParam String query, Pageable pageable) {
        Page<Transaction> page = transactionService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page);
    }

    @GetMapping("/api/_refetch/transactions")
    public ResponseEntity<List<Transaction>> refetch() {
        Page<Transaction> page = transactionService.findAll(Pageable.unpaged());
        transactionService.saveAll(page.getContent());
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code POST  /_multi/transactions} : Create transactions.
     *
     * @param transactionListDTO the transactions to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dose, or with status {@code 400 (Bad Request)} if the transaction has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/api/_multi/transactions")
    public ResponseEntity<TransactionListDTO> createMultiTransaction(@RequestBody TransactionListDTO transactionListDTO) throws URISyntaxException {

        log.debug(">>>>>>>> REST request to save a group of transactions : {}", transactionListDTO);

        ExtendedUser merchant = extendedUserService.getCurrentExtendedUser();
        if (!merchant.getStatus().equals(Status.ACCEPTED)) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.merchant.incorrect.status") + " " + merchant.getStatus(), TransactionService.ENTITY_NAME, ExtendedUserService.INCORRECT_STATUS);
        }

        if (transactionListDTO.getTransactions().isEmpty()) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.transaction.not.create"), TransactionService.ENTITY_NAME, "empty");
        }

        if (transactionListDTO.getTransactions().stream().anyMatch(transaction -> transaction.getId() != null)) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.transaction.exist"), TransactionService.ENTITY_NAME, "idExists");
        }

        TransactionListDTO result = transactionService.create(transactionListDTO);
        transactionService.refetchAll(result.getTransactions());
        notificationReceiverService.sendMultiPostTransactionNotification(result.getTransactions(), extendedUserService.getCurrentExtendedUser());

        return ResponseEntity.created(new URI("/api/_multi/transaction/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, null))
            .body(result);


    }

    /**
     * {@code PUT  /_multi/transactions} : Update transactions.
     *
     * @param transactionListDTO the transactions to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dose, or with status {@code 400 (Bad Request)} if the transaction has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/api/_multi/transactions")
    public ResponseEntity<TransactionListDTO> updateMultiTransaction(@RequestBody TransactionListDTO transactionListDTO) throws URISyntaxException {
        log.debug("REST request to update a group of transactions : {}", transactionListDTO);

        if (transactionListDTO.getTransactions().isEmpty()) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.transaction.not.update"), ENTITY_NAME, "empty");
        }

        if (transactionListDTO.getTransactions().stream().anyMatch(transaction -> transaction.getId() == null)) {
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.transaction"), ENTITY_NAME, "missingId");
        }

        TransactionListDTO result = transactionService.update(transactionListDTO);
        transactionService.refetchAll(result.getTransactions());
        notificationReceiverService.sendTransactionNotification(result.getTransactions(), extendedUserService.getCurrentExtendedUser());

        return ResponseEntity.created(new URI("/api/_multi/transaction/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, null))
            .body(result);
    }

    /**
     * generateExcelTransactions
     * <p>
     * Generate excel statistics for transactions
     *
     * @param query parameter for filter
     * @param chartTitle report title
     */
    @GetMapping("/api/_report/transactions")
    public void generateExcelTransactions(@RequestParam String query, @RequestParam String chartTitle) {
        // A list by extended-user-relation filtered by query
        Iterable<Transaction> iterableTransactions = transactionService.search(query);
        // User
        ExtendedUser user = extendedUserService.getCurrentExtendedUser();
        // Generate excel
        fileService.generateExcelTransaction(query, chartTitle, iterableTransactions, user);
    }

    /**
     * {@code SEARCH  /v1/payments} : search for the payment for the merchant.
     *
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/v1/payments")
    @Secured({AuthoritiesConstants.MERCHANT, AuthoritiesConstants.RESELLER})
    public ResponseEntity<Page<TransactionForMerchantDTO>> v1GetPayments(String query, Pageable pageable) {
        ExtendedUser eu = extendedUserService.getCurrentExtendedUser();
        if (Status.CANCELLED.equals(eu.getStatus()))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.incorrect.status") + " " + eu.getStatus() + " " + translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.this.action"),
                TransactionService.ENTITY_NAME, ExtendedUserService.INCORRECT_STATUS);

        if (extendedUserService.isMerchant() && query.toLowerCase().contains("merchant.id:") ||
            extendedUserService.isReseller() && query.toLowerCase().contains("reseller.id:"))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.query.malformed"), TransactionService.ENTITY_NAME, TransactionService.BAD_REQUEST);

        Page<Transaction> transactionPage = transactionService.search(
            query + (extendedUserService.isMerchant() ? " AND merchant.id: " : " AND reseller.id: ") + eu.getId(), pageable);
        List<Transaction> transactionList = transactionPage.getContent();
        List<TransactionForMerchantDTO> transactionForMerchantDTOList = new ArrayList<>();
        transactionList.forEach(transaction -> transactionForMerchantDTOList.add(new TransactionForMerchantDTO(transaction)));
        Page<TransactionForMerchantDTO> transactionDTOPage = new PageImpl<>(transactionForMerchantDTOList, pageable, transactionPage.getTotalElements());

        return ResponseEntity.ok()
            .headers(PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), transactionDTOPage))
            .body(transactionDTOPage);
    }

    /**
     * {@code GET  /v1/payments/:id} : get the "id" payment.
     *
     * @param id the id of the payment to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the transaction, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/v1/payments/{id}")
    @Secured({AuthoritiesConstants.MERCHANT, AuthoritiesConstants.RESELLER})
    public ResponseEntity<TransactionForMerchantDTO> v1GetPayment(@PathVariable Long id) {
        log.debug("REST request to get payment : {}", id);
        if (id == null)
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.payment.exist"),
                TransactionService.ENTITY_NAME, TransactionService.BAD_REQUEST);

        ExtendedUser eu = extendedUserService.getCurrentExtendedUser();
        if (Status.CANCELLED.equals(eu.getStatus()))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.incorrect.status") + " " + eu.getStatus() + " " + translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.this.action"),
                TransactionService.ENTITY_NAME, ExtendedUserService.INCORRECT_STATUS);

        Optional<Transaction> transactionOptional = AuthoritiesConstants.MERCHANT.equals(eu.getRole()) ?
            transactionService.findOneByIdAndMerchantId(id, eu.getId()) :
            transactionService.findOneByIdAndResellerId(id, eu.getId());
        if (transactionOptional.isEmpty())
            throw new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.payment.id") + " " + id,
                TransactionService.ENTITY_NAME, TransactionService.NOT_FOUND);

        return ResponseUtil.wrapOrNotFound(Optional.of(new TransactionForMerchantDTO(transactionOptional.get())));
    }

    /**
     * {@code GET  /v1/payments/description} : Find payments.
     *
     * @param body <String,Object> the payments to create.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the list of transactions, or with status {@code 404 (Not Found)} if the transaction doesn't exists.
     */
    @GetMapping("/v1/payments/description")
    @Secured({AuthoritiesConstants.MERCHANT})
    public ResponseEntity <List<TransactionByDescriptionDTO>> FindPaymentsByDescription(@RequestBody Map<String,Object> body) {

        log.info("FindPaymentsByDescription - body: " + body.get("description").toString());

        TransactionListDTO  tl=transactionService.findAllByDescription(body.get("description").toString());
        if (tl == null){
            return ResponseEntity.notFound()
                .headers(HeaderUtil.createAlert(applicationName,"BAD REQUEST",null))
                .build();
        }
        List <TransactionByDescriptionDTO> result =new ArrayList<TransactionByDescriptionDTO>();
        tl.getTransactions().forEach(transaction -> {
            TransactionByDescriptionDTO t=new TransactionByDescriptionDTO();
            t.setId(transaction.getId());
            t.setStatus(transaction.getStatus());
            t.setStatusReason(transaction.getStatusReason());
            result.add(t);
        });

        log.info("description: {} - result: {}", body.get("description").toString(), result);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, null))
            .body(result);
    }

    /**
     * {@code POST  /v1/payments} : Create payments.
     *
     * @param paymentListDTO the payments to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dose, or with status {@code 400 (Bad Request)} if the transaction has already an ID.
     */
    @PostMapping("/v1/payments")
    @Secured({AuthoritiesConstants.MERCHANT})
    public ResponseEntity<PaymentListDTO> v1CreatePayments(@RequestBody PaymentListDTO paymentListDTO) {

        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        log.info(">>>>>> timestamp: {}", timestamp.getTime());

        log.info(">>>>>> {}: REST request to save a group of payments {}", timestamp.getTime(), paymentListDTO);
        
        ExtendedUser eu = extendedUserService.getCurrentExtendedUser();

        log.info(">>>>>> {}: After extendedUserService.getCurrentExtendedUser function", timestamp.getTime());

        if (!Status.ACCEPTED.equals(eu.getStatus()))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.incorrect.status") + " " + eu.getStatus() + " " + translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.incorrect.status"),
                TransactionService.ENTITY_NAME, ExtendedUserService.INCORRECT_STATUS);

        log.info(">>>>>> {}: after INCORRECT_STATUS validation.", timestamp.getTime());

        if (paymentListDTO.getPayments() == null || paymentListDTO.getPayments().isEmpty())
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.payment.not.create"),
                TransactionService.ENTITY_NAME, TransactionService.BAD_REQUEST);

        log.info(">>>>>> {}: after getPayments empty validation.", timestamp.getTime());

        int upLimit = 100;
        if (paymentListDTO.getPayments().size() > upLimit)
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.limits.payments"),
                TransactionService.ENTITY_NAME, TransactionService.BAD_REQUEST);

        log.info(">>>>>> {}: after getPayments size upLimit validation.", timestamp.getTime());

        if (paymentListDTO.getPayments().stream().anyMatch(transaction -> transaction.getId() != null))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.payment.already.exist"),
                TransactionService.ENTITY_NAME, TransactionService.BAD_REQUEST);

        if (paymentListDTO.getPayments().stream().anyMatch(transaction -> transaction.getPayee().getId() == null))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.payee.not.defined"),
                TransactionService.ENTITY_NAME, TransactionService.BAD_REQUEST);

        if (paymentListDTO.getPayments().stream().anyMatch(transaction -> transaction.getAmount() == null))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.missing.amount"),
                TransactionService.ENTITY_NAME, TransactionService.BAD_REQUEST);

        log.info(">>>>>> {}: after initial validations", timestamp.getTime());

        PaymentListDTO result = transactionService.create(paymentListDTO);

        log.info(">>>>>> {}: after transactionService create: {}", timestamp.getTime(), result);

        transactionService.refetchAll(result.getPayments());

        log.info(">>>>>> {}: after save values on ESS for result: {}", timestamp.getTime(), result);

        notificationReceiverService.sendMultiPostTransactionNotification(result.getPayments(), extendedUserService.getCurrentExtendedUser());

        log.info(">>>>>> {}: result sent as response {}", timestamp.getTime(), result);
        
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, null))
            .body(result);
    }

    /**
     * {@code PUT  /v1/payments} : Updates an existing payment.
     *
     * @param id of the payment to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated transaction,
     * or with status {@code 400 (Bad Request)} if the transaction is not valid,
     * or with status {@code 500 (Internal Server Error)} if the transaction couldn't be updated.
     */
    @PatchMapping("/v1/payments/{id}")
    @Secured({AuthoritiesConstants.MERCHANT})
    public ResponseEntity<TransactionForMerchantDTO> v1CancelPayment(@Valid @RequestBody Transaction payment, @PathVariable Long id) {
        log.debug("REST request to cancel payment with id : {}", id);
        if (id == null)
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.payment.exist"), TransactionService.ENTITY_NAME, "missingId");
        if (payment.getStatus() != Status.CANCELLED)
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.payment.invalid.status") + " " + payment.getStatus(), TransactionService.ENTITY_NAME, "status");

        ExtendedUser eu = extendedUserService.getCurrentExtendedUser();
        if (!Status.ACCEPTED.equals(eu.getStatus()))
            throw new BadRequestAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.incorrect.status") + " " + eu.getStatus() + " " + translateService.getMessageByCurrentUserParamsNull("error.message.exception.user.this.action"),
                TransactionService.ENTITY_NAME, ExtendedUserService.INCORRECT_STATUS);

        Optional<Transaction> transactionOptional = transactionService.findOneByIdAndMerchantId(id, eu.getId());
        if (transactionOptional.isEmpty())
            throw new NotFoundAlertException(translateService.getMessageByCurrentUserParamsNull("error.message.exception.payment.id") + " " + id, TransactionService.ENTITY_NAME, TransactionService.NOT_FOUND);

        Transaction paymentToUpdate = new Transaction();
        paymentToUpdate.setId(id);
        paymentToUpdate.setStatus(Status.CANCELLED);
        paymentToUpdate.setStatusReason(payment.getStatusReason());
        Transaction result = transactionService.update(paymentToUpdate);
        transactionService.refetch(result.getId());

        TransactionForMerchantDTO paymentForMerchant = new TransactionForMerchantDTO(result);

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, TransactionService.ENTITY_NAME, paymentForMerchant.getId().toString()))
            .body(paymentForMerchant);
    }
    @PostMapping("/api/transactionQuote/{idBankAccount}")
    public ResponseEntity<RippleResultGroupDTO> getTransactionQuote(@RequestBody List<Transaction> transactions, @PathVariable Long idBankAccount) {
        log.debug("REST request to get the quote of Transactions");
        RippleResultGroupDTO transactionsQuote = transactionService.getTranstactionQuotes(transactions,idBankAccount);
        return ResponseEntity.ok().body(transactionsQuote);
    }

}
