package com.pagomundo.web.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pagomundo.domain.User;
import com.pagomundo.security.jwt.JWTFilter;
import com.pagomundo.security.jwt.TokenProvider;
import com.pagomundo.service.UserService;
import com.pagomundo.web.rest.errors.UnauthorizedException;
import com.pagomundo.web.rest.vm.LoginVM;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Date;

import javax.validation.Valid;

/**
 * Controller to authenticate users.
 */
@RestController
public class UserJWTController {

    private final TokenProvider tokenProvider;

    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final Logger log = LoggerFactory.getLogger(UserJWTController.class);

    @Autowired
    private UserService userService;

    public UserJWTController(TokenProvider tokenProvider, AuthenticationManagerBuilder authenticationManagerBuilder) {
        this.tokenProvider = tokenProvider;
        this.authenticationManagerBuilder = authenticationManagerBuilder;
    }
    
    @PostMapping("/v1/authenticate")
    public ResponseEntity<JWTToken> v1Authorize(@Valid @RequestBody LoginVM loginVM) {
        return authorize(loginVM);
    }

    @PostMapping("/api/authenticate")
    public ResponseEntity<JWTToken> authorize(@Valid @RequestBody LoginVM loginVM) {
    	
    	String token=existeTokenSesion(loginVM.getUsername());
    	if(token.isEmpty()) {
    		log.info("No hay token, se genera");	
            UsernamePasswordAuthenticationToken authenticationToken =
               new UsernamePasswordAuthenticationToken(loginVM.getUsername(), loginVM.getPassword());

            Authentication authentication;
            try {
                authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
            } catch (AuthenticationException ae) {
                throw new UnauthorizedException(loginVM.getUsername(), UserService.ENTITY_NAME, UserService.UNAUTHORIZED_USER_CODE);
            }
            SecurityContextHolder.getContext().setAuthentication(authentication);
            boolean rememberMe = (loginVM.isRememberMe() == null) ? false : loginVM.isRememberMe();
            String jwt = tokenProvider.createToken(authentication, rememberMe);
            //se divide por si existe token
            saveNewToken(loginVM.getUsername(),jwt);
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);
            return new ResponseEntity<>(new JWTToken(jwt), httpHeaders, HttpStatus.OK);
        }else {
        	log.info("hay token vigente, se regresa");
        	HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + token);
            return new ResponseEntity<>(new JWTToken(token), httpHeaders, HttpStatus.OK);
        }
    	
    }

    /**
     * Object to return as body in JWT Authentication.
     */
    static class JWTToken {

        private String idToken;

        JWTToken(String idToken) {
            this.idToken = idToken;
        }

        @JsonProperty("id_token")
        String getIdToken() {
            return idToken;
        }

        void setIdToken(String idToken) {
            this.idToken = idToken;
        }
    }
    private String existeTokenSesion(String login) {
    	User user=userService.findUserByLogin(login);
    	String respuesta="";
    	if(null!=user) {
    		Date fechaVigencia=user.getTokenValidity();
    		Date fechaActual=new Date();
    		if(null!=fechaVigencia && fechaActual.before(fechaVigencia)) {
    			log.info("vigencia del token encontrado: {}",fechaVigencia);
    			respuesta=user.getCurrentToken();
    		}
    	}
    	return respuesta;
    }
    private void saveNewToken(String login, String token) {
    	log.info("saveNewToken");
    	userService.changeToken(login, token, new Date());
    }
}
