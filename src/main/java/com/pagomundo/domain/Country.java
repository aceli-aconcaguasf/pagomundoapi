package com.pagomundo.domain;

import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * A Country.
 */
@Entity
@Table(name = "country")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "country")
public class Country implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "spanish_name")
    private String spanishName;

    @Column(name = "sub_region_name")
    private String subRegionName;

    @Column(name = "region_name")
    private String regionName;

    @Column(name = "code")
    private String code;

    @Column(name = "for_payee")
    private Boolean forPayee;

    @Column(name = "currency_name")
    private String currencyName;

    @Column(name = "currency_code")
    private String currencyCode;

    @Column(name = "currency_value")
    private Float currencyValue;

    @Column(name = "currency_base")
    private String currencyBase;

    @Column(name = "currency_last_updated")
    private ZonedDateTime currencyLastUpdated;
    
    @Column(name = "lang_key")
    private String langKey;
    
    @Column(name = "phone_code")
    private String phoneCode;

    @OneToMany(mappedBy = "country")
    private Set<Bank> banks = new HashSet<>();

    @OneToMany(mappedBy = "country")
    private Set<City> cities = new HashSet<>();

    @OneToMany(mappedBy = "country")
    private Set<Currency> currencies = new HashSet<>();

    @OneToMany(mappedBy = "country")
    private Set<IdType> idTypes = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country name(String name) {
        this.name = name;
        return this;
    }

    public String getSpanishName() {
        return spanishName;
    }

    public void setSpanishName(String spanishName) {
        this.spanishName = spanishName;
    }

    public Country spanishName(String spanishName) {
        this.spanishName = spanishName;
        return this;
    }

    public String getSubRegionName() {
        return subRegionName;
    }

    public void setSubRegionName(String subRegionName) {
        this.subRegionName = subRegionName;
    }

    public Country subRegionName(String subRegionName) {
        this.subRegionName = subRegionName;
        return this;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public Country regionName(String regionName) {
        this.regionName = regionName;
        return this;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Country code(String code) {
        this.code = code;
        return this;
    }

    public Boolean isForPayee() {
        return forPayee;
    }

    public Country forPayee(Boolean forPayee) {
        this.forPayee = forPayee;
        return this;
    }

    public void setForPayee(Boolean forPayee) {
        this.forPayee = forPayee;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public Country currencyName(String currencyName) {
        this.currencyName = currencyName;
        return this;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Country currencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
        return this;
    }

    public Float getCurrencyValue() {
        return currencyValue;
    }

    public void setCurrencyValue(Float currencyValue) {
        this.currencyValue = currencyValue;
    }

    public Country currencyValue(Float currencyValue) {
        this.currencyValue = currencyValue;
        return this;
    }

    public String getCurrencyBase() {
        return currencyBase;
    }

    public void setCurrencyBase(String currencyBase) {
        this.currencyBase = currencyBase;
    }

    public Country currencyBase(String currencyBase) {
        this.currencyBase = currencyBase;
        return this;
    }

    public ZonedDateTime getCurrencyLastUpdated() {
        return currencyLastUpdated;
    }

    public void setCurrencyLastUpdated(ZonedDateTime currencyLastUpdated) {
        this.currencyLastUpdated = currencyLastUpdated;
    }

    public Country currencyLastUpdated(ZonedDateTime currencyLastUpdated) {
        this.currencyLastUpdated = currencyLastUpdated;
        return this;
    }

    public String getLangKey() {
        return langKey;
    }

    public void setLangKey(String langKey) {
        this.langKey = langKey;
    }
    
     public Country langKey(String langKey) {
        this.langKey = langKey;
        return this;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }
    
    public Country phoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
        return this;
    }

    public Set<Bank> getBanks() {
        return banks;
    }

    public void setBanks(Set<Bank> banks) {
        this.banks = banks;
    }

    public Country banks(Set<Bank> banks) {
        this.banks = banks;
        return this;
    }

    public Country addBank(Bank bank) {
        this.banks.add(bank);
        bank.setCountry(this);
        return this;
    }

    public Country removeBank(Bank bank) {
        this.banks.remove(bank);
        bank.setCountry(null);
        return this;
    }

    public Set<City> getCities() {
        return cities;
    }

    public void setCities(Set<City> cities) {
        this.cities = cities;
    }

    public Country cities(Set<City> cities) {
        this.cities = cities;
        return this;
    }

    public Country addCity(City city) {
        this.cities.add(city);
        city.setCountry(this);
        return this;
    }

    public Country removeCity(City city) {
        this.cities.remove(city);
        city.setCountry(null);
        return this;
    }

    public Set<Currency> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(Set<Currency> currencies) {
        this.currencies = currencies;
    }

    public Country currencies(Set<Currency> currencies) {
        this.currencies = currencies;
        return this;
    }

    public Country addCurrency(Currency currency) {
        this.currencies.add(currency);
        currency.setCountry(this);
        return this;
    }

    public Country removeCurrency(Currency currency) {
        this.currencies.remove(currency);
        currency.setCountry(null);
        return this;
    }

    public Set<IdType> getIdTypes() {
        return idTypes;
    }

    public void setIdTypes(Set<IdType> idTypes) {
        this.idTypes = idTypes;
    }

    public Country idTypes(Set<IdType> idTypes) {
        this.idTypes = idTypes;
        return this;
    }

    public Country addIdType(IdType idType) {
        this.idTypes.add(idType);
        idType.setCountry(this);
        return this;
    }

    public Country removeIdType(IdType idType) {
        this.idTypes.remove(idType);
        idType.setCountry(null);
        return this;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Country)) {
            return false;
        }
        return id != null && id.equals(((Country) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Country{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", spanishName='" + getSpanishName() + "'" +
            ", subRegionName='" + getSubRegionName() + "'" +
            ", regionName='" + getRegionName() + "'" +
            ", code='" + getCode() + "'" +
            ", forPayee='" + isForPayee() + "'" +
            ", currencyName='" + getCurrencyName() + "'" +
            ", currencyCode='" + getCurrencyCode() + "'" +
            ", currencyValue=" + getCurrencyValue() +
            ", currencyBase='" + getCurrencyBase() + "'" +
            ", currencyLastUpdated='" + getCurrencyLastUpdated() + "'" +
            "}";
    }
}
