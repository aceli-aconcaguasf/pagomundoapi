package com.pagomundo.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * A DirectPaymentBank.
 */
@Entity
@Table(name = "direct_payment_bank")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "directpaymentbank")
public class DirectPaymentBank implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "destiny_bank_code")
    private String destinyBankCode;

    @Column(name = "id_type_name")
    private String idTypeName;

    @Column(name = "id_type_code")
    private String idTypeCode;

    @Column(name = "city_code")
    private String cityCode;

    @Column(name = "department_code")
    private String departmentCode;
    
    @Column(name = "destiny_bank_bicfi")
    private String destinyBankBicfi;

    @ManyToOne
    @JsonIgnoreProperties(value = "directPaymentBanks", allowSetters = true)
    private Bank originBank;

    @ManyToOne
    @JsonIgnoreProperties(value = "directPaymentBanks", allowSetters = true)
    private Bank destinyBank;

    @ManyToOne
    @JsonIgnoreProperties(value = "directPaymentBanks", allowSetters = true)
    private City destinyCity;

    @ManyToOne
    @JsonIgnoreProperties(value = "directPaymentBanks", allowSetters = true)
    private IdType idType;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDestinyBankCode() {
        return destinyBankCode;
    }

    public void setDestinyBankCode(String destinyBankCode) {
        this.destinyBankCode = destinyBankCode;
    }

    public DirectPaymentBank destinyBankCode(String destinyBankCode) {
        this.destinyBankCode = destinyBankCode;
        return this;
    }

    public String getIdTypeName() {
        return idTypeName;
    }

    public void setIdTypeName(String idTypeName) {
        this.idTypeName = idTypeName;
    }

    public DirectPaymentBank idTypeName(String idTypeName) {
        this.idTypeName = idTypeName;
        return this;
    }

    public String getIdTypeCode() {
        return idTypeCode;
    }

    public void setIdTypeCode(String idTypeCode) {
        this.idTypeCode = idTypeCode;
    }

    public DirectPaymentBank idTypeCode(String idTypeCode) {
        this.idTypeCode = idTypeCode;
        return this;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public DirectPaymentBank cityCode(String cityCode) {
        this.cityCode = cityCode;
        return this;
    }

    public String getDepartmentCode() {
        return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }

    public DirectPaymentBank departmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
        return this;
    }

    public Bank getOriginBank() {
        return originBank;
    }

    public void setOriginBank(Bank bank) {
        this.originBank = bank;
    }

    public DirectPaymentBank originBank(Bank bank) {
        this.originBank = bank;
        return this;
    }

    public Bank getDestinyBank() {
        return destinyBank;
    }

    public void setDestinyBank(Bank bank) {
        this.destinyBank = bank;
    }

    public DirectPaymentBank destinyBank(Bank bank) {
        this.destinyBank = bank;
        return this;
    }

    public City getDestinyCity() {
        return destinyCity;
    }

    public void setDestinyCity(City city) {
        this.destinyCity = city;
    }

    public DirectPaymentBank destinyCity(City city) {
        this.destinyCity = city;
        return this;
    }

    public IdType getIdType() {
        return idType;
    }

    public void setIdType(IdType idType) {
        this.idType = idType;
    }

    public DirectPaymentBank idType(IdType idType) {
        this.idType = idType;
        return this;
    }

    public String getDestinyBankBicfi() {
        return destinyBankBicfi;
    }

    public void setDestinyBankBicfi(String destinyBankBicfi) {
        this.destinyBankBicfi = destinyBankBicfi;
    }
    
    public DirectPaymentBank destinyBankBicfi(String destinyBankBicfi) {
        this.destinyBankBicfi = destinyBankBicfi;
        return this;
    }
    
    
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DirectPaymentBank)) {
            return false;
        }
        return id != null && id.equals(((DirectPaymentBank) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "DirectPaymentBank{" +
            "id=" + getId() +
            ", destinyBankCode='" + getDestinyBankCode() + "'" +
            ", idTypeName='" + getIdTypeName() + "'" +
            ", idTypeCode='" + getIdTypeCode() + "'" +
            ", cityCode='" + getCityCode() + "'" +
            ", departmentCode='" + getDepartmentCode() + "'" +
            "}";
    }
}
