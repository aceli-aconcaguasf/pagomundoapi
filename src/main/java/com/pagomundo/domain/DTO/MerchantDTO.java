package com.pagomundo.domain.DTO;

import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.enumeration.Status;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

/**
 * A DTO representing a merchant
 */
public class MerchantDTO {

    private Long id;

    @Enumerated(EnumType.STRING)
    private Status status;
    private String fullName;
    private String email;
    private BigDecimal balance;
    private ZonedDateTime birthDate;
    private String mobileNumber;
    private String idNumber;
    private String address;
    private CountryDTO country;
    private String companyName;
    private String companyTaxId;
    private String companyAddress;
    private CountryDTO companyCountry;

    public MerchantDTO() {
    }

    public MerchantDTO(ExtendedUser extendedUser) {
        if (extendedUser == null) return;
        this.id = extendedUser.getId();
        this.status = extendedUser.getStatus();
        this.fullName = extendedUser.getFullName();
        this.email = extendedUser.getEmail();
        this.birthDate = extendedUser.getBirthDate();
        this.mobileNumber = extendedUser.getMobileNumber();
        this.idNumber = extendedUser.getIdNumber();
        this.address = extendedUser.getPostalAddress();
        this.country = new CountryDTO(extendedUser.getPostalCountry());
        this.companyName = extendedUser.getCompany();
        this.companyTaxId = extendedUser.getTaxId();
        this.companyAddress = extendedUser.getResidenceAddress();
        this.companyCountry = new CountryDTO(extendedUser.getResidenceCountry());
        this.balance = extendedUser.getBalance();
    }

    public CountryDTO getCountry() {
        return country;
    }

    public void setCountry(CountryDTO country) {
        this.country = country;
    }

    public CountryDTO getCompanyCountry() {
        return companyCountry;
    }

    public void setCompanyCountry(CountryDTO companyCountry) {
        this.companyCountry = companyCountry;
    }

    public String getCompanyTaxId() {
        return companyTaxId;
    }

    public void setCompanyTaxId(String companyTaxId) {
        this.companyTaxId = companyTaxId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public ZonedDateTime getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(ZonedDateTime birthDate) {
        this.birthDate = birthDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

}
