package com.pagomundo.domain.DTO.Kushki;

import com.fasterxml.jackson.annotation.JsonInclude;

public class ContactDetailsDTO{
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String documentType;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String documentNumber;

    private String email;

    private String firstName;

    private String lastName;

    private String phoneNumber;

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "ContactDetails{" +
            "documentType='" + documentType + '\'' +
            ", documentNumber='" + documentNumber + '\'' +
            ", email='" + email + '\'' +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", phoneNumber='" + phoneNumber + '\'' +
            '}';
    }
}
