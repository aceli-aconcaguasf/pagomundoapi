package com.pagomundo.domain.DTO.Kushki;

import java.util.Set;

public class ProductDetailsDTO {

    private Set<Product> product;

    public Set<Product> getProduct() {
        return product;
    }

    public void setProduct(Set<Product> product) {
        this.product = product;
    }

    @Override
    public String toString() {
        return "ProductDetailsDTO{" +
            "product=" + product +
            '}';
    }
}
