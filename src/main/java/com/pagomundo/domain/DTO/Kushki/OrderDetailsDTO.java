package com.pagomundo.domain.DTO.Kushki;

import com.fasterxml.jackson.annotation.JsonInclude;

public class OrderDetailsDTO {
    private String siteDomain;

    private BillingDetailsDTO billingDetails;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private BillingDetailsDTO shippingDetails;

    public String getSiteDomain() {
        return siteDomain;
    }

    public void setSiteDomain(String diteDomain) {
        this.siteDomain = diteDomain;
    }

    public BillingDetailsDTO getBillingDetails() {
        return billingDetails;
    }

    public void setBillingDetails(BillingDetailsDTO billingDetails) {
        this.billingDetails = billingDetails;
    }

    public BillingDetailsDTO getShippingDetails() {
        return shippingDetails;
    }

    public void setShippingDetails(BillingDetailsDTO shippingDetails) {
        this.shippingDetails = shippingDetails;
    }

    @Override
    public String toString() {
        return "OrderDetailsDTO{" +
            "siteDomain='" + siteDomain + '\'' +
            ", billingDetails=" + billingDetails +
            ", shippingDetails=" + shippingDetails +
            '}';
    }
}
