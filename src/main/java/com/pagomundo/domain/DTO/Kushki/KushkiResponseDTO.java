package com.pagomundo.domain.DTO.Kushki;

import com.fasterxml.jackson.annotation.JsonInclude;

public class KushkiResponseDTO {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String ticketNumber;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String transactionReference;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String message;

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getTransactionReference() {
        return transactionReference;
    }

    public void setTransactionReference(String transactionReference) {
        this.transactionReference = transactionReference;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "KushkiResponseDTO{" +
            "ticketNumber='" + ticketNumber + '\'' +
            ", transactionReference='" + transactionReference + '\'' +
            ", message='" + message + '\'' +
            '}';
    }
}
