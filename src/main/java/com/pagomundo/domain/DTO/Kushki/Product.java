package com.pagomundo.domain.DTO.Kushki;

import java.math.BigDecimal;

public class Product {
    private String id;

    private String title;

    private BigDecimal price;

    private String sku;

    private int quantity;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "product{" +
            "id='" + id + '\'' +
            ", title='" + title + '\'' +
            ", price='" + price + '\'' +
            ", sku='" + sku + '\'' +
            ", quantity=" + quantity +
            '}';
    }
}
