package com.pagomundo.domain.DTO.Kushki;

import java.math.BigDecimal;

public class AmountDto {

    private BigDecimal subtotalIva;

    private BigDecimal subtotalIva0;

    private BigDecimal ice;

    private BigDecimal iva;

    private String currency;

    public BigDecimal getSubtotalIva() {
        return subtotalIva;
    }

    public void setSubtotalIva(BigDecimal subtotalIva) {
        this.subtotalIva = subtotalIva;
    }

    public BigDecimal getSubtotalIva0() {
        return subtotalIva0;
    }

    public void setSubtotalIva0(BigDecimal subtotalIva0) {
        this.subtotalIva0 = subtotalIva0;
    }

    public BigDecimal getIce() {
        return ice;
    }

    public void setIce(BigDecimal ice) {
        this.ice = ice;
    }

    public BigDecimal getIva() {
        return iva;
    }

    public void setIva(BigDecimal iva) {
        this.iva = iva;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return "AmountDto{" +
            "subtotalIva=" + subtotalIva +
            ", subtotalIva0=" + subtotalIva0 +
            ", ice=" + ice +
            ", iva=" + iva +
            ", currency='" + currency + '\'' +
            '}';
    }
}
