package com.pagomundo.domain.DTO;

import com.pagomundo.domain.DirectPaymentBank;

/**
 * A DTO representing a DirectPaymentBankDTO
 */
public class DirectPaymentBankDTO {

    private Long id;    // DirectPaymentBank
    private String name; // DestinyBank.name

    public DirectPaymentBankDTO() {
    }

    public DirectPaymentBankDTO(DirectPaymentBank dpb) {
        if (dpb == null) return;
        this.id = dpb.getId();
        if (dpb.getDestinyBank() != null)
            this.name = dpb.getDestinyBank().getName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DirectPaymentBankDTO)) {
            return false;
        }
        return id != null && id.equals(((DirectPaymentBankDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }
}
