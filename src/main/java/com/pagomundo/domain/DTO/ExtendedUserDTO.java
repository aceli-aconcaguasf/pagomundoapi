package com.pagomundo.domain.DTO;

import com.pagomundo.domain.Bank;
import com.pagomundo.domain.City;
import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.IdType;
import com.pagomundo.domain.User;
import com.pagomundo.domain.enumeration.Gender;
import com.pagomundo.domain.enumeration.MaritalStatus;
import com.pagomundo.domain.enumeration.Status;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

/**
 * A DTO representing an extendedUser, with his authorities.
 */
public class ExtendedUserDTO {

    private Long id;

    @Enumerated(EnumType.STRING)
    private Status status;
    private String lastName1;
    private String lastName2;
    private String firstName1;
    private String firstName2;
    private String fullName;
    private String idNumber;
    private String taxId;
    private Gender gender;
    private MaritalStatus maritalStatus;
    private String residenceAddress;
    private String postalAddress;
    private String phoneNumber;
    private String mobileNumber;
    private ZonedDateTime birthDate;
    private ZonedDateTime dateCreated;
    private ZonedDateTime lastUpdated;
    private String email;
    private String company;
    private String imageIdUrl;
    private String imageAddressUrl;
    private BigDecimal balance;
    private User user;
    private City residenceCity;
    private City postalCity;
    private Bank bank;
    private IdType idType;

    public ExtendedUserDTO() {
    }

    public ExtendedUserDTO(ExtendedUser eu) {
        this.status = eu.getStatus();
        this.lastName1 = eu.getLastName1();
        this.lastName2 = eu.getLastName2();
        this.firstName1 = eu.getFirstName1();
        this.firstName2 = eu.getFirstName2();
        this.fullName = eu.getFullName();
        this.idNumber = eu.getIdNumber();
        this.taxId = eu.getTaxId();
        this.gender = eu.getGender();
        this.maritalStatus = eu.getMaritalStatus();
        this.residenceAddress = eu.getResidenceAddress();
        this.postalAddress = eu.getPostalAddress();
        this.phoneNumber = eu.getPhoneNumber();
        this.mobileNumber = eu.getMobileNumber();
        this.birthDate = eu.getBirthDate();
        this.dateCreated = eu.getDateCreated();
        this.lastUpdated = eu.getLastUpdated();
        this.email = eu.getEmail();
        this.company = eu.getCompany();
        this.imageIdUrl = eu.getImageIdUrl();
        this.imageAddressUrl = eu.getImageAddressUrl();
        this.balance = eu.getBalance();
        this.user = eu.getUser();
        this.residenceCity = eu.getResidenceCity();
        this.postalCity = eu.getPostalCity();
        this.bank = eu.getBank();
        this.idType = eu.getIdType();
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getLastName1() {
        return lastName1;
    }

    public void setLastName1(String lastName1) {
        this.lastName1 = lastName1;
    }

    public String getLastName2() {
        return lastName2;
    }

    public void setLastName2(String lastName2) {
        this.lastName2 = lastName2;
    }

    public String getFirstName1() {
        return firstName1;
    }

    public void setFirstName1(String firstName1) {
        this.firstName1 = firstName1;
    }

    public String getFirstName2() {
        return firstName2;
    }

    public void setFirstName2(String firstName2) {
        this.firstName2 = firstName2;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public MaritalStatus getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(MaritalStatus maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getResidenceAddress() {
        return residenceAddress;
    }

    public void setResidenceAddress(String residenceAddress) {
        this.residenceAddress = residenceAddress;
    }

    public String getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(String postalAddress) {
        this.postalAddress = postalAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public ZonedDateTime getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(ZonedDateTime birthDate) {
        this.birthDate = birthDate;
    }

    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public ZonedDateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(ZonedDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getImageIdUrl() {
        return imageIdUrl;
    }

    public void setImageIdUrl(String imageIdUrl) {
        this.imageIdUrl = imageIdUrl;
    }

    public String getImageAddressUrl() {
        return imageAddressUrl;
    }

    public void setImageAddressUrl(String imageAddressUrl) {
        this.imageAddressUrl = imageAddressUrl;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public City getResidenceCity() {
        return residenceCity;
    }

    public void setResidenceCity(City city) {
        this.residenceCity = city;
    }

    public City getPostalCity() {
        return postalCity;
    }

    public void setPostalCity(City city) {
        this.postalCity = city;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public IdType getIdType() {
        return idType;
    }

    public void setIdType(IdType idType) {
        this.idType = idType;
    }

    @Override
    public String toString() {
        return "ExtendedUserDTO{" +
            "id=" + getId() +
            ", status='" + getStatus() + "'" +
            ", lastName1='" + getLastName1() + "'" +
            ", lastName2='" + getLastName2() + "'" +
            ", firstName1='" + getFirstName1() + "'" +
            ", firstName2='" + getFirstName2() + "'" +
            ", fullName='" + getFullName() + "'" +
            ", idNumber='" + getIdNumber() + "'" +
            ", taxId='" + getTaxId() + "'" +
            ", gender='" + getGender() + "'" +
            ", maritalStatus='" + getMaritalStatus() + "'" +
            ", residenceAddress='" + getResidenceAddress() + "'" +
            ", postalAddress='" + getPostalAddress() + "'" +
            ", phoneNumber='" + getPhoneNumber() + "'" +
            ", mobileNumber='" + getMobileNumber() + "'" +
            ", birthDate='" + getBirthDate() + "'" +
            ", dateCreated='" + getDateCreated() + "'" +
            ", lastUpdated='" + getLastUpdated() + "'" +
            ", email='" + getEmail() + "'" +
            ", company='" + getCompany() + "'" +
            ", imageIdUrl='" + getImageIdUrl() + "'" +
            ", imageAddressUrl='" + getImageAddressUrl() + "'" +
            ", balance=" + getBalance() +
            "}";
    }
}
