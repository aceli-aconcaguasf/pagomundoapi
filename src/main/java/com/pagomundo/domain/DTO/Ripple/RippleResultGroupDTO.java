package com.pagomundo.domain.DTO.Ripple;

import com.pagomundo.domain.BankAccount;
import com.pagomundo.domain.Currency;
import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.Transaction;

import java.util.List;
import java.util.Set;

public class RippleResultGroupDTO {
    
    private Long id;

    private Set<RippleResultDTO> rippleResults;
    private Set<RippleResultDTO> rippleResultsWithErrors;


    public RippleResultGroupDTO() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<RippleResultDTO> getRippleResults() {
        return this.rippleResults;
    }

    public void setRippleResults(Set<RippleResultDTO> rippleResults) {
        this.rippleResults = rippleResults;
    }

    public Set<RippleResultDTO> getRippleResultsWithErrors() {
        return this.rippleResultsWithErrors;
    }

    public void setRippleResultsWithErrors(Set<RippleResultDTO> rippleResultsWithErrors) {
        this.rippleResultsWithErrors = rippleResultsWithErrors;
    }


   
}
