package com.pagomundo.domain.DTO.Ripple;

import com.pagomundo.domain.BankAccount;
import com.pagomundo.domain.Currency;
import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.Transaction;

import java.util.List;
import java.util.Set;

public class RippleTransacctionDTO {
    
    private Long id;
    private BankAccount bankAccount;
    private ExtendedUser admin;
    private ExtendedUser merchant;
    private ExtendedUser payee;
    private Boolean directPayment;
    private Float exchangeRate;
    private Set<Transaction> transactions;
    private Set<Transaction> transactionsWithErrors;
    private boolean remitee;

    public RippleTransacctionDTO() {
    }


    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BankAccount getBankAccount() {
        return this.bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public ExtendedUser getAdmin() {
        return this.admin;
    }

    public void setAdmin(ExtendedUser admin) {
        this.admin = admin;
    }

    public ExtendedUser getMerchant() {
        return this.merchant;
    }

    public void setMerchant(ExtendedUser merchant) {
        this.merchant = merchant;
    }

    public ExtendedUser getPayee() {
        return this.payee;
    }

    public void setPayee(ExtendedUser payee) {
        this.payee = payee;
    }

    public Boolean isDirectPayment() {
        return this.directPayment;
    }

    public Boolean getDirectPayment() {
        return this.directPayment;
    }

    public void setDirectPayment(Boolean directPayment) {
        this.directPayment = directPayment;
    }

    public Float getExchangeRate() {
        return this.exchangeRate;
    }

    public void setExchangeRate(Float exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public Set<Transaction> getTransactions() {
        return this.transactions;
    }

    public void setTransactions(Set<Transaction> transactions) {
        this.transactions = transactions;
    }

    public Set<Transaction> getTransactionsWithErrors() {
        return this.transactionsWithErrors;
    }

    public void setTransactionsWithErrors(Set<Transaction> transactionsWithErrors) {
        this.transactionsWithErrors = transactionsWithErrors;
    }

    public boolean isRemitee() {
        return this.remitee;
    }

    public void setRemitee(boolean remitee) {
        this.remitee = remitee;
    }

    
    
    


    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", bankAccount='" + getBankAccount() + "'" +
            ", admin='" + getAdmin() + "'" +
            ", merchant='" + getMerchant() + "'" +
            ", payee='" + getPayee() + "'" +
            ", directPayment='" + isDirectPayment() + "'" +
            ", exchangeRate='" + getExchangeRate() + "'" +
            ", transactions='" + getTransactions() + "'" +
            ", transactionsWithErrors='" + getTransactionsWithErrors() + "'" +
            "}";
    }
    
   
}
