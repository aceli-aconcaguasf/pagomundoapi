package com.pagomundo.domain.DTO.Ripple;

import java.util.*;  
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude;



public class RippleResultDTO {
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String status;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String message;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String transaction_id;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String rate;


    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTransaction_id() {
        return this.transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    
    

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("data")
    private void unpackNestedEstatus(Map<String,Object> data) {
        this.status = (String)data.get("status");
        this.message = (String)data.get("message");
        this.transaction_id = (String)data.get("transaction_id");
    }  

    public RippleResultDTO() {
    }


    @Override
    public String toString() {
        return "{" +
            " status='" + getStatus() + "'" +
            ", message='" + getMessage() + "'" +
            ", transaction_id='" + getTransaction_id() + "'" +
             ", rate='" + getRate() + "'" +   
            "}";
    }

}
