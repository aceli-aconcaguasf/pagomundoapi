package com.pagomundo.domain.DTO;

/**
 * A DTO representing a query for ES search.
 */
public class QueryDTO {
    private String query;

    public QueryDTO() {
        // Empty constructor needed for Jackson.
    }

    public QueryDTO(String query) {
        this.query = query;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

}
