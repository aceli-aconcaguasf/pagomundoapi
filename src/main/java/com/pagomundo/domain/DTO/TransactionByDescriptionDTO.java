package com.pagomundo.domain.DTO;

import com.pagomundo.domain.enumeration.Status;

public class TransactionByDescriptionDTO {

    private Long id;

    private Status status;

    private String statusReason;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getStatusReason() {
        return statusReason;
    }

    public void setStatusReason(String statusReason) {
        this.statusReason = statusReason;
    }

}
