package com.pagomundo.domain.DTO;

import com.pagomundo.domain.IdType;
import com.pagomundo.domain.enumeration.NaturalOrLegal;

/**
 * A DTO representing a country
 */
public class IdTypeDTO {

    private Long id;
    private String name;
    private NaturalOrLegal naturalOrLegal;

    public IdTypeDTO() {
    }

    public IdTypeDTO(IdType idType) {
        if (idType == null) return;
        this.id = idType.getId();
        this.name = idType.getName();
        this.naturalOrLegal = idType.getNaturalOrLegal();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public NaturalOrLegal getNaturalOrLegal() {
        return naturalOrLegal;
    }

    public void setNaturalOrLegal(NaturalOrLegal naturalOrLegal) {
        this.naturalOrLegal = naturalOrLegal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof IdTypeDTO)) {
            return false;
        }
        return id != null && id.equals(((IdTypeDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

}
