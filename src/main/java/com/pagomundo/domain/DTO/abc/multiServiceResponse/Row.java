package com.pagomundo.domain.DTO.abc.multiServiceResponse;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Row {
	@XmlElement (name="field")
	public List<Field> fields;
}
