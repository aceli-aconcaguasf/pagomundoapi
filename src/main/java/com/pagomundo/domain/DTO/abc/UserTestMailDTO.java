package com.pagomundo.domain.DTO.abc;

public class UserTestMailDTO {
    private String login;
    private String email;
    private String langKey;
    private String extraURLParameter;
    private String templateEmail;
    private String titleKey;


    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLangKey() {
        return langKey;
    }

    public void setLangKey(String langKey) {
        this.langKey = langKey;
    }

    public String getExtraURLParameter() {
        return extraURLParameter;
    }

    public void setExtraURLParameter(String extraURLParameter) {
        this.extraURLParameter = extraURLParameter;
    }

    public String getTemplateEmail() {
        return templateEmail;
    }

    public void setTemplateEmail(String templateEmail) {
        this.templateEmail = templateEmail;
    }

    public String getTitleKey() {
        return titleKey;
    }

    public void setTitleKey(String titleKey) {
        this.titleKey = titleKey;
    }
}
