package com.pagomundo.domain.DTO.abc;

public class NeighborhoodQueryDTO {
    private String zipCode;
    private String neighborhoodName;

    public NeighborhoodQueryDTO() {
    }


    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getNeighborhoodName() {
        return neighborhoodName;
    }

    public void setNeighborhoodName(String neighborhoodName) {
        this.neighborhoodName = neighborhoodName;
    }
}
