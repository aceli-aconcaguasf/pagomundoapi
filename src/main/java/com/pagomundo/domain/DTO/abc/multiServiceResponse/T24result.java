package com.pagomundo.domain.DTO.abc.multiServiceResponse;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name="T24result")
public class T24result {
	@XmlElement (required = false)
	public Head head;
	@XmlElement (required = false)
	public Body body;
}
