package com.pagomundo.domain.DTO.abc.multiServiceResponse;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement
public class Field {
	@XmlValue
	public String value;
	@XmlAttribute (required = false)
	public String name;

}
