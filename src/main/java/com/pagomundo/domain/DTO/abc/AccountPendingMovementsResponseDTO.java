package com.pagomundo.domain.DTO.abc;

import java.util.List;

public class AccountPendingMovementsResponseDTO {
    private String messageId;
    private String resultId;
    private String error;
    private List<AccountPendingMovementDTO> accountPendingMovements;

    public AccountPendingMovementsResponseDTO() {
    }


    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getResultId() {
        return resultId;
    }

    public void setResultId(String resultId) {
        this.resultId = resultId;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<AccountPendingMovementDTO> getAccountPendingMovements() {
        return accountPendingMovements;
    }

    public void setAccountPendingMovements(List<AccountPendingMovementDTO> accountPendingMovements) {
        this.accountPendingMovements = accountPendingMovements;
    }
}
