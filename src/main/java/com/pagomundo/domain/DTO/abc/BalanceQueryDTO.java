package com.pagomundo.domain.DTO.abc;

public class BalanceQueryDTO {
    private String accountNumber;
    private String channel;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
}
