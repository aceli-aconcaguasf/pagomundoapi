package com.pagomundo.domain.DTO.abc;

public class SignatureDTO {

    private String cuentacargo;
    private String monto;
    private String concepto;
    private String cuentabeneficiario;
    private String canal;
   
    public SignatureDTO() {
    }


    public SignatureDTO(String cuentacargo, String monto, String concepto, String cuentabeneficiario, String canal) {
        this.cuentacargo = cuentacargo;
        this.monto = monto;
        this.concepto = concepto;
        this.cuentabeneficiario = cuentabeneficiario;
        this.canal = canal;
    }


    public String getCuentacargo() {
        return this.cuentacargo;
    }

    public void setCuentacargo(String cuentacargo) {
        this.cuentacargo = cuentacargo;
    }

    public String getMonto() {
        return this.monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public String getConcepto() {
        return this.concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public String getCuentabeneficiario() {
        return this.cuentabeneficiario;
    }

    public void setCuentabeneficiario(String cuentabeneficiario) {
        this.cuentabeneficiario = cuentabeneficiario;
    }

    public String getCanal() {
        return this.canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }


    @Override
    public String toString() {
        return "{" +
            " cuentacargo='" + getCuentacargo() + "'" +
            ", monto='" + getMonto() + "'" +
            ", concepto='" + getConcepto() + "'" +
            ", cuentabeneficiario='" + getCuentabeneficiario() + "'" +
            ", canal='" + getCanal() + "'" +
            "}";
    }
    
    
}
