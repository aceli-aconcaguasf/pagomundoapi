package com.pagomundo.domain.DTO.abc.multiServiceResponse;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Table {
	@XmlAttribute (required = false)
	public int columnsCount;
	@XmlAttribute (required = false)
	public String id;
	@XmlAttribute (required = false)
	public int rowsCount;
	@XmlElement(name="row", required = false) 
	public List<Row> rows;

}
