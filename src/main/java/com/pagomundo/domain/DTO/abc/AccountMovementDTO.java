package com.pagomundo.domain.DTO.abc;

public class AccountMovementDTO {
    private String operationDate;
    private String movementDescription;
    private String reference;
    private String accountingDate;
    private String operationTimeStamp;
    private String movementNarrative;
    private String debitAmount;
    private String creditAmount;
    private String runningBalance;

    public AccountMovementDTO() {
    }

    public String getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(String operationDate) {
        this.operationDate = operationDate;
    }

    public String getMovementDescription() {
        return movementDescription;
    }

    public void setMovementDescription(String movementDescription) {
        this.movementDescription = movementDescription;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getOperationTimeStamp() {
        return operationTimeStamp;
    }

    public void setOperationTimeStamp(String operationTimeStamp) {
        this.operationTimeStamp = operationTimeStamp;
    }

    public String getMovementNarrative() {
        return movementNarrative;
    }

    public void setMovementNarrative(String movementNarrative) {
        this.movementNarrative = movementNarrative;
    }

    public String getDebitAmount() {
        return debitAmount;
    }

    public void setDebitAmount(String debitAmount) {
        this.debitAmount = debitAmount;
    }

    public String getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(String creditAmount) {
        this.creditAmount = creditAmount;
    }


    public String getAccountingDate() {
        return accountingDate;
    }

    public void setAccountingDate(String accountingDate) {
        this.accountingDate = accountingDate;
    }

    public String getRunningBalance() {
        return runningBalance;
    }

    public void setRunningBalance(String runningBalance) {
        this.runningBalance = runningBalance;
    }
}
