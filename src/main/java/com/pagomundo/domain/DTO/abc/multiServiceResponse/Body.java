package com.pagomundo.domain.DTO.abc.multiServiceResponse;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Body {
	@XmlElement (required = false)
	public Table table;
}
