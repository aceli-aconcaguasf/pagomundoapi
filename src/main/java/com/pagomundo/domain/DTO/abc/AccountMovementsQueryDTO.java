package com.pagomundo.domain.DTO.abc;

public class AccountMovementsQueryDTO {
    private String bookingDate;
    private String accountNumber;
    private String channel;

    public AccountMovementsQueryDTO() {
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
}
