package com.pagomundo.domain.DTO.abc.multiServiceResponse;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Head {
	@XmlElement (required = false)
	public String resultId;
	@XmlElement (required = false)
	public String messageId;
	@XmlElement (required = false)
	public String error;
}
