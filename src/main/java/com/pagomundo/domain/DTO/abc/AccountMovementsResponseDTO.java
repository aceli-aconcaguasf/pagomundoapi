package com.pagomundo.domain.DTO.abc;

import java.util.List;

public class AccountMovementsResponseDTO {
    private String messageId;
    private String resultId;
    private String error;
    private List<AccountMovementDTO> accountMovements;

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getResultId() {
        return resultId;
    }

    public void setResultId(String resultId) {
        this.resultId = resultId;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<AccountMovementDTO> getAccountMovements() {
        return accountMovements;
    }

    public void setAccountMovements(List<AccountMovementDTO> accountMovements) {
        this.accountMovements = accountMovements;
    }
}
