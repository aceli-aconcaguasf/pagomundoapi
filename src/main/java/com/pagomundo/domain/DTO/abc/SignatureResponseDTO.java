package com.pagomundo.domain.DTO.abc;

public class SignatureResponseDTO {

    private String signature;


    public SignatureResponseDTO() {
    }


    public SignatureResponseDTO(String signature) {
        this.signature = signature;
    }



    public String getSignature() {
        return this.signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }



    @Override
    public String toString() {
        return "{" +
            " signature='" + getSignature() + "'" +
            "}";
    }
    

    
}
