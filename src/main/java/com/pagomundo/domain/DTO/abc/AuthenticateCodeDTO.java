package com.pagomundo.domain.DTO.abc;

public class AuthenticateCodeDTO {

    private String notifyId;
    private String deviceId;
    private String keygen;
    private String notifyTo;

    public AuthenticateCodeDTO() {
        // Constructor
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getNotifyId() {
        return notifyId;
    }

    public void setNotifyId(String notifyId) {
        this.notifyId = notifyId;
    }

    public String getKeygen() {
        return keygen;
    }

    public void setKeygen(String keygen) {
        this.keygen = keygen;
    }

    public String getNotifyTo() {
        return notifyTo;
    }

    public void setNotifyTo(String notifyTo) {
        this.notifyTo = notifyTo;
    }

}
