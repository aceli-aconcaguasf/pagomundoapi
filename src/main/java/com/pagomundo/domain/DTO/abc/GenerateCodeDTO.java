package com.pagomundo.domain.DTO.abc;

public class GenerateCodeDTO {

    private String notifyTo;
    private String deviceId;
    private int transactionType;

    public GenerateCodeDTO() {
        // Constructor
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getNotifyTo() {
        return notifyTo;
    }

    public void setNotifyTo(String notifyTo) {
        this.notifyTo = notifyTo;
    }

	public int getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(int transactionType) {
		this.transactionType = transactionType;
	}

}
