package com.pagomundo.domain.DTO.abc;

import java.util.List;

public class NeighborhoodResponseDTO {
    private String messageId;
    private String resultId;
    private String error;
    private List<NeighborhoodDTO> neighborhoods;

    public NeighborhoodResponseDTO() {
    }


    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getResultId() {
        return resultId;
    }

    public void setResultId(String resultId) {
        this.resultId = resultId;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<NeighborhoodDTO> getNeighborhoods() {
        return neighborhoods;
    }

    public void setNeighborhoods(List<NeighborhoodDTO> neighborhoods) {
        this.neighborhoods = neighborhoods;
    }
}
