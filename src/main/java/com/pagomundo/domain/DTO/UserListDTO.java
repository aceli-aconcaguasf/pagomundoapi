package com.pagomundo.domain.DTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.pagomundo.domain.Country;
import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.service.dto.UserDTO;

import java.util.List;

public class UserListDTO {
    @JsonIgnoreProperties(allowSetters = true,
        value = {
            "spanishName",
            "subRegionName",
            "regionName",
            "code",
            "forPayee",
            "currencyName",
            "currencyCode",
            "currencyValue",
            "currencyBase",
            "currencyLastUpdated",
            "banks",
            "cities",
            "currencies",
            "idTypes"
        })
    private Country country;

    @JsonIgnoreProperties(allowSetters = true,
        value = {
            "lastName1",
            "lastName2",
            "firstName1",
            "firstName2",
            "idNumber",
            "taxId",
            "gender",
            "maritalStatus",
            "residenceAddress",
            "postalAddress",
            "phoneNumber",
            "birthDate",
            "dateCreated",
            "lastUpdated",
            "imageIdUrl",
            "imageAddressUrl",
            "balance",
            "statusReason",
            "cardNumber",
            "bankCommission",
            "fxCommission",
            "rechargeCost",
            "useMerchantCommission",
            "bankAccountNumber",
            "useDirectPayment",
            "bankAccountType",
            "canChangePaymentMethod",
            "mustNotify",
            "profileInfoChanged",
            "user",
            "userRelations",
            "extendedUserFunds",
            "notificationSenders",
            "notificationReceivers",
            "notificationResponsibles",
            "notificationStatusChanges",
            "transactionMerchants",
            "transactionPayees",
            "transactionAdmins",
            "transactionStatusChanges",
            "residenceCity",
            "idType",
            "extendedUserGroup",
            "bank",
            "residenceCountry",
            "directPaymentBank",
            "directPaymentCity",
            "postalCity",
            "postalCountry",
            "mobileNumber",
            "idTypeTaxId"
        })
    private ExtendedUser toWhom;

    private String usersRole;

    private List<UserDTO> users;

    private List<UserDTO> usersCreatedAndAssociated;
    private List<UserDTO> userExistingAndAssociated;
    private List<UserDTO> userExistingAndNotAssociated;
    private List<UserDTO> userNotCreated;

    public UserListDTO() {
    }

    public UserListDTO(List<UserDTO> userDTOList) {
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public ExtendedUser getToWhom() {
        return toWhom;
    }

    public void setToWhom(ExtendedUser toWhom) {
        this.toWhom = toWhom;
    }

    public String getUsersRole() {
        return usersRole;
    }

    public void setUsersRole(String usersRole) {
        this.usersRole = usersRole;
    }

    public List<UserDTO> getUserExistingAndNotAssociated() {
        return userExistingAndNotAssociated;
    }

    public void setUserExistingAndNotAssociated(List<UserDTO> userExistingAndNotAssociated) {
        this.userExistingAndNotAssociated = userExistingAndNotAssociated;
    }

    public List<UserDTO> getUserExistingAndAssociated() {
        return userExistingAndAssociated;
    }

    public void setUserExistingAndAssociated(List<UserDTO> userExistingAndAssociated) {
        this.userExistingAndAssociated = userExistingAndAssociated;
    }

    public List<UserDTO> getUsersCreatedAndAssociated() {
        return usersCreatedAndAssociated;
    }

    public void setUsersCreatedAndAssociated(List<UserDTO> usersCreatedAndAssociated) {
        this.usersCreatedAndAssociated = usersCreatedAndAssociated;
    }

    public List<UserDTO> getUsers() {
        return users;
    }

    public void setUsers(List<UserDTO> users) {
        this.users = users;
    }

    public List<UserDTO> getUserNotCreated() {
        return userNotCreated;
    }

    public void setUserNotCreated(List<UserDTO> userNotCreated) {
        this.userNotCreated = userNotCreated;
    }

    @Override
    public String toString() {
        return "UserListDTO{" +
            ", toWhom=" + toWhom +
            ", usersRole=" + usersRole +
            ", users=" + users +
            ", userList=" + usersCreatedAndAssociated +
            '}';
    }
}
