package com.pagomundo.domain.DTO;

import com.pagomundo.domain.Country;
import com.pagomundo.domain.DTO.Kushki.*;
import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.service.dto.UserDTO;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

public class PayeeForTransactionDTO {

    private String cardToken;

    private String merchantId;

    private String paymentIntentId;

    private BigDecimal fee;

    private AmountDto amount;

    private UserDTO payee;

    private ContactDetailsDTO contactDetails;

    private OrderDetailsDTO orderDetails;

    private ProductDetailsDTO productDetails;

    private ExtendedUser toWhom;

    private Country country;

    private String usersRole;

    private String transactionResult;



    public String getCardToken() {
        return cardToken;
    }

    public void setCardToken(String cardToken) {
        this.cardToken = cardToken;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getPaymentIntentId() {
        return paymentIntentId;
    }

    public void setPaymentIntentId(String paymentIntentId) {
        this.paymentIntentId = paymentIntentId;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public AmountDto getAmount() {
        return amount;
    }

    public void setAmount(AmountDto amount) {
        this.amount = amount;
    }

    public UserDTO getPayee() {
        return payee;
    }

    public void setPayee(UserDTO payee) {
        this.payee = payee;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public ExtendedUser getToWhom() {
        return toWhom;
    }

    public void setToWhom(ExtendedUser toWhom) {
        this.toWhom = toWhom;
    }

    public String getUsersRole() {
        return usersRole;
    }

    public void setUsersRole(String usersRole) {
        this.usersRole = usersRole;
    }

    @Override
    public String toString() {
        return "PayeeForTransactionDTO{" +
            "cardToken='" + cardToken + '\'' +
            ", merchantId='" + merchantId + '\'' +
            ", paymentIntentId='" + paymentIntentId + '\'' +
            ", fee=" + fee +
            ", amount=" + amount +
            ", payee=" + payee +
            ", contactDetails=" + contactDetails +
            ", orderDetails=" + orderDetails +
            ", productDetails=" + productDetails +
            ", toWhom=" + toWhom +
            ", country=" + country +
            ", usersRole='" + usersRole + '\'' +
            ", transactionResult='" + transactionResult + '\'' +
            '}';
    }

    public String getTransactionResult() {
        return transactionResult;
    }

    public void setTransactionResult(String transactionResult) {
        this.transactionResult = transactionResult;
    }

    public ContactDetailsDTO getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(ContactDetailsDTO contactDetails) {
        this.contactDetails = contactDetails;
    }

    public OrderDetailsDTO getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(OrderDetailsDTO orderDetails) {
        this.orderDetails = orderDetails;
    }

    public ProductDetailsDTO getProductDetails() {
        return productDetails;
    }

    public void setProductDetails(ProductDetailsDTO productDetails) {
        this.productDetails = productDetails;
    }

}
