package com.pagomundo.domain.DTO;

import com.pagomundo.domain.BankAccount;
import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.Transaction;

import java.util.List;
import java.util.Set;

public class TransactionListDTO {
    private Long id;
    private BankAccount bankAccount;
    private ExtendedUser admin;
    private Float exchangeRate;
    private Set<Transaction> transactions;
    private Set<Transaction> transactionsWithErrors;
    private Boolean directPayment;

    public TransactionListDTO() {
    }

    public TransactionListDTO(List<Transaction> transactions) {
    }

    public Boolean getDirectPayment() {
        return directPayment;
    }

    public void setDirectPayment(Boolean directPayment) {
        this.directPayment = directPayment;
    }

    public ExtendedUser getAdmin() {
        return admin;
    }

    public void setAdmin(ExtendedUser admin) {
        this.admin = admin;
    }

    public Set<Transaction> getTransactionsWithErrors() {
        return transactionsWithErrors;
    }

    public void setTransactionsWithErrors(Set<Transaction> transactionsWithErrors) {
        this.transactionsWithErrors = transactionsWithErrors;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public Float getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(Float exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(Set<Transaction> transactions) {
        this.transactions = transactions;
    }

    @Override
    public String toString() {
        return "TransactionListDTO{" +
            "transactionGroupId=" + id +
            ", transactions=" + transactions +
            ", transactionsWithErrors=" + transactionsWithErrors +
            '}';
    }
}
