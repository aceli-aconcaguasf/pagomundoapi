package com.pagomundo.domain.DTO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pagomundo.domain.ExtendedUserRelation;
import com.pagomundo.domain.User;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.service.dto.UserDTO;

/**
 * A DTO representing an invited user
 */
public class InvitedUserDTO {
    @JsonIgnore
    private String login;
    //@JsonIgnore
    private Long id;

    private String email;

    private String firstName;
    private String lastName;
    private String nickname = "";
    private Status status = null;
    private String statusReason = "";
    private String role = "";
    private String resetKey = "";

    public InvitedUserDTO() {
    }

    public InvitedUserDTO(ExtendedUserRelation eur) {
        if (eur == null) return;
        User user = eur.getUserRelated();
        if (user == null) return;
        this.id = user.getId();
        this.login = user.getLogin();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.email = user.getEmail() != null ? user.getEmail() : user.getLogin();
        this.status = user.getStatus() != null ? user.getStatus() : Status.CREATED;
        this.statusReason = user.getStatusReason() != null ? user.getStatusReason() : "";
        this.role = user.getRole();
        this.nickname = eur.getNickname() != null ? eur.getNickname() : "";
    }

    public InvitedUserDTO(UserDTO user) {
        if (user == null) return;
        this.id = user.getId();
        this.login = user.getLogin();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.email = user.getEmail() != null ? user.getEmail() : user.getLogin();
        this.status = user.getStatus();
        this.statusReason = user.getStatusReason() != null ? user.getStatusReason() : "";
        this.role = user.getRole();
        this.nickname = user.getNickname();
        this.resetKey = user.getResetKey();
    }

    static UserDTO invitedUserDTOToUserDTO(InvitedUserDTO invitedUserDTO) {
        if (invitedUserDTO == null) return null;
        UserDTO user = new UserDTO();
        user.setId(invitedUserDTO.getId());
        user.setLogin(invitedUserDTO.getLogin());
        user.setEmail(invitedUserDTO.getEmail());
        user.setFirstName(invitedUserDTO.getFirstName());
        user.setLastName(invitedUserDTO.getLastName());
        user.setStatus(invitedUserDTO.getStatus());
        user.setStatusReason(invitedUserDTO.getStatusReason());
        user.setResetKey(invitedUserDTO.getResetKey());
        return user;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getStatusReason() {
        return statusReason;
    }

    public void setStatusReason(String statusReason) {
        this.statusReason = statusReason;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getResetKey() {
        return resetKey;
    }

    public void setResetKey(String resetKey) {
        this.resetKey = resetKey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof InvitedUserDTO)) {
            return false;
        }
        return id != null && id.equals(((InvitedUserDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }
}
