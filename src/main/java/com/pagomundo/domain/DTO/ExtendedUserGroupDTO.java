package com.pagomundo.domain.DTO;

import com.pagomundo.domain.Bank;
import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.ExtendedUserGroup;
import com.pagomundo.domain.enumeration.Status;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * A ExtendedUserGroupDTO.
 */
public class ExtendedUserGroupDTO implements Serializable {

    private Long id;
    private ZonedDateTime dateCreated;
    private Status status;
    private String fileName;
    private String fileNameFromBank;
    private Set<ExtendedUser> extendedUsers = new HashSet<>();
    private Set<ExtendedUser> extendedUsersWithErrors = new HashSet<>();
    private ExtendedUser admin;
    private Bank bank;

    public ExtendedUserGroupDTO() {
    }

    public ExtendedUserGroupDTO(ExtendedUserGroup eug) {
        this.id = eug.getId();
        this.dateCreated = eug.getDateCreated();
        this.status = eug.getStatus();
        this.fileName = eug.getFileName();
        this.fileNameFromBank = eug.getFileNameFromBank();
        this.extendedUsers = eug.getExtendedUsers();
        this.admin = eug.getAdmin();
        this.bank = eug.getBank();
    }

    public Set<ExtendedUser> getExtendedUsers() {
        return extendedUsers;
    }

    public void setExtendedUsers(Set<ExtendedUser> extendedUsers) {
        this.extendedUsers = extendedUsers;
    }

    public Set<ExtendedUser> getExtendedUsersWithErrors() {
        return extendedUsersWithErrors;
    }

    public void setExtendedUsersWithErrors(Set<ExtendedUser> extendedUsersWithErrors) {
        this.extendedUsersWithErrors = extendedUsersWithErrors;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileNameFromBank() {
        return fileNameFromBank;
    }

    public void setFileNameFromBank(String fileNameFromBank) {
        this.fileNameFromBank = fileNameFromBank;
    }

    public ExtendedUser getAdmin() {
        return admin;
    }

    public void setAdmin(ExtendedUser extendedUser) {
        this.admin = extendedUser;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    @Override
    public String toString() {
        return "ExtendedUserGroup{" +
            "id=" + getId() +
            ", dateCreated='" + getDateCreated() + "'" +
            ", status='" + getStatus() + "'" +
            ", fileName='" + getFileName() + "'" +
            ", fileNameFromBank='" + getFileNameFromBank() + "'" +
            "}";
    }
}
