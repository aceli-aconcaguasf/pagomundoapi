package com.pagomundo.domain.DTO;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * A DTO representing the exchange rates from exchangerate-api.com
 */
public class ExchangeRateApiDTO {

    public static final String URL = "https://api.exchangerate-api.com/v4/latest/USD";
    private String base;
    private String date;
    private String timeLastUpdated;
    private List<ExchangeRateValueDTO> rates = new ArrayList<>();

    public ExchangeRateApiDTO() {
    }

    public ExchangeRateApiDTO(JsonObject jsonObj) {
        Set<String> keys = jsonObj.keySet();
        keys.forEach(k -> {
            if ("base".equalsIgnoreCase(k)) base = jsonObj.get(k).getAsString();
            if ("date".equalsIgnoreCase(k)) date = jsonObj.get(k).getAsString();
            if ("time_last_updated".equalsIgnoreCase(k)) timeLastUpdated = jsonObj.get(k).getAsString();
            if ("rates".equalsIgnoreCase(k)) {
                JsonObject exchangeRatesAsJsonObject = jsonObj.get(k).getAsJsonObject();
                exchangeRatesAsJsonObject.keySet().forEach(kRates ->
                    rates.add(new ExchangeRateValueDTO(kRates, Float.parseFloat(exchangeRatesAsJsonObject.get(kRates).getAsString())))
                );
            }
        });
    }

    public List<ExchangeRateValueDTO> getRates() {
        return rates;
    }

    public void setRates(List<ExchangeRateValueDTO> rates) {
        this.rates = rates;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTimeLastUpdated() {
        return timeLastUpdated;
    }

    public void setTimeLastUpdated(String timeLastUpdated) {
        this.timeLastUpdated = timeLastUpdated;
    }

}
