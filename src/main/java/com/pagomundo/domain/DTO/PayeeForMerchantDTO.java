package com.pagomundo.domain.DTO;

import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.ExtendedUserRelation;
import com.pagomundo.domain.enumeration.Status;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 * A DTO representing a merchant, with his authorities.
 */
public class PayeeForMerchantDTO {
    private Long id;

    private String email;

    @Enumerated(EnumType.STRING)
    private Status status;

    private String statusReason = "";

    private String fullName;

    private String nickname = "";

    private String company = "";

    private Boolean useDirectPayment = false;

    public PayeeForMerchantDTO() {
    }

    public PayeeForMerchantDTO(ExtendedUserRelation eur) {
        this(eur.getExtendedUser());
        this.nickname = eur.getNickname() != null ? eur.getNickname() : "";
    }

    public PayeeForMerchantDTO(ExtendedUser extendedUser) {
        if (extendedUser == null) return;
        this.id = extendedUser.getId();
        this.status = extendedUser.getStatus();
        this.statusReason = extendedUser.getStatusReason();
        this.fullName = extendedUser.getFullName();
        this.email = extendedUser.getEmail();
        this.company = extendedUser.getCompany() != null ? extendedUser.getCompany() : "";
        this.useDirectPayment = extendedUser.isUseDirectPayment();
    }

    PayeeForMerchantDTO(ExtendedUser extendedUser, String nickname) {
        this(extendedUser);
        this.nickname = nickname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatusReason() {
        return statusReason;
    }

    public void setStatusReason(String statusReason) {
        this.statusReason = statusReason;
    }

    public Boolean getUseDirectPayment() {
        return this.useDirectPayment;
    }

    public void setUseDirectPayment(Boolean useDirectPayment) {
        this.useDirectPayment = useDirectPayment;
    }


    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", email='" + getEmail() + "'" +
            ", status='" + getStatus() + "'" +
            ", statusReason='" + getStatusReason() + "'" +
            ", fullName='" + getFullName() + "'" +
            ", nickname='" + getNickname() + "'" +
            ", company='" + getCompany() + "'" +
            "}";
    }

}
