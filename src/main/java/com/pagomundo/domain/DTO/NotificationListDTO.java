package com.pagomundo.domain.DTO;

import com.pagomundo.domain.Notification;

import java.util.List;
import java.util.Set;

public class NotificationListDTO {
    private Long id;
    private Set<Notification> notifications;
    private Set<Notification> notificationsWithErrors;

    public NotificationListDTO() {
    }

    public NotificationListDTO(List<Notification> notifications) {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Notification> getNotificationsWithErrors() {
        return notificationsWithErrors;
    }

    public void setNotificationsWithErrors(Set<Notification> notificationsWithErrors) {
        this.notificationsWithErrors = notificationsWithErrors;
    }

    public Set<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(Set<Notification> notifications) {
        this.notifications = notifications;
    }

    @Override
    public String toString() {
        return "NotificationListDTO{" +
            "id=" + id +
            ", notifications=" + notifications +
            ", notificationsWithErrors=" + notificationsWithErrors +
            '}';
    }
}
