package com.pagomundo.domain.DTO;

import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.Notification;
import com.pagomundo.domain.User;

import java.util.List;

public class NotificationReceiverDTO {
    private Notification notification;
    private List<ExtendedUser> receivers;
    private List<User> userReceivers;

    public NotificationReceiverDTO() {
    }

    public NotificationReceiverDTO(Notification notification) {
    }

    public List<User> getUserReceivers() {
        return userReceivers;
    }

    public void setUserReceivers(List<User> userReceivers) {
        this.userReceivers = userReceivers;
    }

    public List<ExtendedUser> getReceivers() {
        return receivers;
    }

    public void setReceivers(List<ExtendedUser> receivers) {
        this.receivers = receivers;
    }

    public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }

    @Override
    public String toString() {
        return "NotificationListDTO{" +
            ", notification=" + notification +
            ", receivers=" + receivers +
            '}';
    }
}
