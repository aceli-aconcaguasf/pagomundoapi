package com.pagomundo.domain.DTO;

import com.pagomundo.domain.Country;
import com.pagomundo.domain.DTO.Kushki.AmountDto;
import com.pagomundo.domain.DTO.Kushki.ContactDetailsDTO;
import com.pagomundo.domain.DTO.Kushki.OrderDetailsDTO;
import com.pagomundo.domain.DTO.Kushki.ProductDetailsDTO;
import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.service.dto.UserDTO;

import java.math.BigDecimal;

public class PayeeForTransactionDTOV2 {

    private String transactionId;

    private BigDecimal amount;

    private UserDTO payee;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public UserDTO getPayee() {
        return payee;
    }

    public void setPayee(UserDTO payee) {
        this.payee = payee;
    }

    @Override
    public String toString() {
        return "PayeeForTransactionDTOV2{" +
            "transactionId='" + transactionId + '\'' +
            ", amount=" + amount +
            ", payee=" + payee +
            '}';
    }
}
