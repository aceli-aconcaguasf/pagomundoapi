package com.pagomundo.domain.DTO;

import java.util.List;

public class PaymentListDTO {
    private List<TransactionForMerchantDTO> payments;
    private List<TransactionForMerchantDTO> paymentsWithErrors;

    public PaymentListDTO() {
    }

    public PaymentListDTO(List<TransactionForMerchantDTO> payments) {
    }

    public List<TransactionForMerchantDTO> getPaymentsWithErrors() {
        return paymentsWithErrors;
    }

    public void setPaymentsWithErrors(List<TransactionForMerchantDTO> paymentsWithErrors) {
        this.paymentsWithErrors = paymentsWithErrors;
    }

    public List<TransactionForMerchantDTO> getPayments() {
        return payments;
    }

    public void setPayments(List<TransactionForMerchantDTO> payments) {
        this.payments = payments;
    }


    @Override
    public String toString() {
        return "{" +
            " payments='" + getPayments() + "'" +
            ", paymentsWithErrors='" + getPaymentsWithErrors() + "'" +
            "}";
    }


}
