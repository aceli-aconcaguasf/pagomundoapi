package com.pagomundo.domain.DTO;

import com.pagomundo.domain.City;

/**
 * A DTO representing a city
 */
public class CityDTO {
    private Long id;
    private String name;
    private CountryDTO country;

    public CityDTO() {
    }

    public CityDTO(City city) {
        if (city == null) return;
        this.id = city.getId();
        this.name = city.getName();
        this.country = new CountryDTO(city.getCountry());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CountryDTO getCountry() {
        return country;
    }

    public void setCountry(CountryDTO country) {
        this.country = country;
    }

}
