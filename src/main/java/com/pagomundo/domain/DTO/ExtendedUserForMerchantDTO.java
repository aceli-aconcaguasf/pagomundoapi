package com.pagomundo.domain.DTO;

import com.pagomundo.domain.ExtendedUser;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 * A DTO representing a merchant, with his authorities.
 */
public class ExtendedUserForMerchantDTO {

    private Long id;

    private String email;

    @Enumerated(EnumType.STRING)
    private String fullName;

    public ExtendedUserForMerchantDTO() {
    }

    ExtendedUserForMerchantDTO(ExtendedUser extendedUser) {
        this.id = extendedUser.getId();
        this.fullName = extendedUser.getFullName();
        this.email = extendedUser.getEmail();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", email='" + getEmail() + "'" +
            ", fullName='" + getFullName() + "'" +
            "}";
    }


}
