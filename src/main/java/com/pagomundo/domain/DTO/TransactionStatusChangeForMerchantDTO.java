package com.pagomundo.domain.DTO;

import com.pagomundo.domain.TransactionStatusChange;
import com.pagomundo.domain.enumeration.Status;

import java.time.ZonedDateTime;

/**
 * A TransactionStatusChange for a merchant.
 */
public class TransactionStatusChangeForMerchantDTO {
    private Long id;
    private ZonedDateTime dateCreated;
    private Status status;
    private String reason;
    private ExtendedUserForMerchantDTO user;

    TransactionStatusChangeForMerchantDTO(TransactionStatusChange tsc) {
        this.id = tsc.getId();
        this.dateCreated = tsc.getDateCreated();
        this.status = tsc.getStatus();
        this.reason = tsc.getReason();
        this.user = tsc.getUser() != null ? new ExtendedUserForMerchantDTO(tsc.getUser()) : null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public ExtendedUserForMerchantDTO getUser() {
        return user;
    }

    public void setUser(ExtendedUserForMerchantDTO user) {
        this.user = user;
    }


    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", dateCreated='" + getDateCreated() + "'" +
            ", status='" + getStatus() + "'" +
            ", reason='" + getReason() + "'" +
            ", user='" + getUser() + "'" +
            "}";
    }


}
