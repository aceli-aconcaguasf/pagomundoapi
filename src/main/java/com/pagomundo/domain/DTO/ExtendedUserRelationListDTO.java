package com.pagomundo.domain.DTO;

import com.pagomundo.domain.ExtendedUserRelation;

import java.util.List;

public class ExtendedUserRelationListDTO {
    private List<ExtendedUserRelation> extendedUserRelations;
    private List<ExtendedUserRelation> extendedUserRelationsWithoutErrors;
    private List<ExtendedUserRelation> extendedUserRelationsWithErrors;

    public ExtendedUserRelationListDTO() {
    }

    public ExtendedUserRelationListDTO(List<ExtendedUserRelation> extendedUserRelationListList) {
    }

    public List<ExtendedUserRelation> getExtendedUserRelationsWithoutErrors() {
        return extendedUserRelationsWithoutErrors;
    }

    public void setExtendedUserRelationsWithoutErrors(List<ExtendedUserRelation> extendedUserRelationsWithoutErrors) {
        this.extendedUserRelationsWithoutErrors = extendedUserRelationsWithoutErrors;
    }

    public List<ExtendedUserRelation> getExtendedUserRelationsWithErrors() {
        return extendedUserRelationsWithErrors;
    }

    public void setExtendedUserRelationsWithErrors(List<ExtendedUserRelation> extendedUserRelationsWithErrors) {
        this.extendedUserRelationsWithErrors = extendedUserRelationsWithErrors;
    }

    public List<ExtendedUserRelation> getExtendedUserRelations() {
        return extendedUserRelations;
    }

    public void setExtendedUserRelations(List<ExtendedUserRelation> extendedUserRelations) {
        this.extendedUserRelations = extendedUserRelations;
    }

    @Override
    public String toString() {
        return "ExtendedUserRelationListDTO{" +
            ", extendedUserRelations=" + extendedUserRelations +
            ", extendedUserRelationsWithoutErrors=" + extendedUserRelationsWithoutErrors +
            ", extendedUserRelationsWithErrors=" + extendedUserRelationsWithErrors +
            '}';
    }
}
