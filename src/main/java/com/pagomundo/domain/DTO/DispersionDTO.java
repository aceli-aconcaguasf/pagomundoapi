package com.pagomundo.domain.DTO;

import java.math.BigDecimal;

public class DispersionDTO {

    private String transactionId;

    private BigDecimal amount;

    private String currency;

    private String type;

    private String status;

    private String date;

    private Long internalTransactionId;

    private String message;

    public String getMessage() { return message; }

    public void setMessage(String message) { this.message = message; }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Long getInternalTransactionId() {
        return internalTransactionId;
    }

    public void setInternalTransactionId(Long internalTransactionId) {
        this.internalTransactionId = internalTransactionId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "DispersionDTO{" +
            "transactionId='" + transactionId + '\'' +
            ", amount=" + amount +
            ", currency='" + currency + '\'' +
            ", type='" + type + '\'' +
            ", status='" + status + '\'' +
            ", date='" + date + '\'' +
            ", internalTransactionId=" + internalTransactionId +
            ", message='" + message + '\'' +
            '}';
    }
}
