package com.pagomundo.domain.DTO;

/**
 * A DTO representing one exchange rate
 */
public class ExchangeRateValueDTO {
    private String currencyCode;
    private Float value;

    public ExchangeRateValueDTO() {
    }

    public ExchangeRateValueDTO(String cur, Float val) {
        currencyCode = cur;
        value = val;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }


}
