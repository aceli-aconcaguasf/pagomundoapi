package com.pagomundo.domain.DTO;

public class CityByStringDTO {

    private String subStr;
    private Long countryId;


    public CityByStringDTO() {
    }


    public CityByStringDTO(String subStr, Long countryId) {
        this.subStr = subStr;
        this.countryId = countryId;
    }

    public String getSubStr() {
        return this.subStr;
    }

    public void setSubStr(String subStr) {
        this.subStr = subStr;
    }

    public Long getCountryId() {
        return this.countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }


    @Override
    public String toString() {
        return "{" +
            " subStr='" + getSubStr() + "'" +
            ", countryId='" + getCountryId() + "'" +
            "}";
    }




    

    
}
