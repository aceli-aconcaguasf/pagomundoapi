package com.pagomundo.domain.DTO;

import com.pagomundo.domain.enumeration.Status;

import java.util.List;
import java.util.stream.Collectors;

public class UserListForMerchantDTO {
    private List<InvitedUserDTO> usersCreatedAndAssociated;
    private List<InvitedUserDTO> userExistingAndAssociated;
    private List<InvitedUserDTO> userExistingAndNotAssociated;
    private List<InvitedUserDTO> userNotCreated;

    public UserListForMerchantDTO() {
    }

    public UserListForMerchantDTO(UserListDTO userListDTO) {
        this.usersCreatedAndAssociated = userListDTO.getUsersCreatedAndAssociated().stream()
            .map(InvitedUserDTO::new).collect(Collectors.toList());
        this.userExistingAndAssociated = userListDTO.getUserExistingAndAssociated().stream()
            .map(InvitedUserDTO::new).collect(Collectors.toList());
        this.userExistingAndAssociated.forEach(invitedUserDTO -> invitedUserDTO.setStatus(Status.ALREADY_REPORTED));

        this.userExistingAndNotAssociated = userListDTO.getUserExistingAndNotAssociated().stream()
            .map(InvitedUserDTO::new).collect(Collectors.toList());
        this.userNotCreated = userListDTO.getUserNotCreated().stream()
            .map(InvitedUserDTO::new).collect(Collectors.toList());
    }

    public List<InvitedUserDTO> getUserExistingAndNotAssociated() {
        return userExistingAndNotAssociated;
    }

    public void setUserExistingAndNotAssociated(List<InvitedUserDTO> userExistingAndNotAssociated) {
        this.userExistingAndNotAssociated = userExistingAndNotAssociated;
    }

    public List<InvitedUserDTO> getUserExistingAndAssociated() {
        return userExistingAndAssociated;
    }

    public void setUserExistingAndAssociated(List<InvitedUserDTO> userExistingAndAssociated) {
        this.userExistingAndAssociated = userExistingAndAssociated;
    }

    public List<InvitedUserDTO> getUsersCreatedAndAssociated() {
        return usersCreatedAndAssociated;
    }

    public void setUsersCreatedAndAssociated(List<InvitedUserDTO> usersCreatedAndAssociated) {
        this.usersCreatedAndAssociated = usersCreatedAndAssociated;
    }

    public List<InvitedUserDTO> getUserNotCreated() {
        return userNotCreated;
    }

    public void setUserNotCreated(List<InvitedUserDTO> userNotCreated) {
        this.userNotCreated = userNotCreated;
    }

    @Override
    public String toString() {
        return "UserListDTO{" +
            ", userList=" + usersCreatedAndAssociated +
            '}';
    }
}
