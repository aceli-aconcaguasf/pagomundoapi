package com.pagomundo.domain.DTO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.Notification;
import com.pagomundo.domain.enumeration.NotificationCategory;
import com.pagomundo.domain.enumeration.NotificationSubCategory;
import com.pagomundo.domain.enumeration.ReceiverGroup;
import com.pagomundo.domain.enumeration.Status;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.io.Serializable;
import java.time.ZonedDateTime;

public class IssueForMerchantDTO implements Serializable {

    private Long id;
    private String subject;
    private String description;
    @JsonIgnore
    private ZonedDateTime dateCreated;
    private ZonedDateTime lastUpdated;
    @Enumerated(EnumType.STRING)
    private Status status;
    private String statusReason;
    @Enumerated(EnumType.STRING)
    @JsonIgnore
    private NotificationCategory category;
    private String notes;
    @Enumerated(EnumType.STRING)
    @JsonIgnore
    private NotificationSubCategory subCategory;
    @Enumerated(EnumType.STRING)
    @JsonIgnore
    private ReceiverGroup receiverGroup;
    @JsonIgnore
    private ExtendedUser sender;
    @JsonIgnore
    private ExtendedUser responsible;

    public IssueForMerchantDTO() {
    }

    public IssueForMerchantDTO(Notification n) {
        if (n == null) return;
        this.id = n.getId();
        this.subject = n.getSubject();
        this.description = n.getDescription();
        this.dateCreated = n.getDateCreated();
        this.lastUpdated = n.getLastUpdated();
        this.status = n.getStatus();
        this.statusReason = n.getStatusReason();
        this.category = n.getCategory();
        this.subCategory = n.getSubCategory();
        this.notes = n.getNotes();
        this.receiverGroup = n.getReceiverGroup();
        this.sender = n.getSender();
        this.responsible = n.getResponsible();
    }

    public static Notification issueForMerchantToNotification(IssueForMerchantDTO ifm) {
        if (ifm == null) return null;
        Notification n = new Notification();

        n.setId(ifm.getId());
        n.setSubject(ifm.getSubject());
        n.setDescription(ifm.getDescription());
        n.setDateCreated(ifm.getDateCreated());
        n.setLastUpdated(ifm.getLastUpdated());
        n.setStatus(ifm.getStatus());
        n.setStatusReason(ifm.getStatusReason());
        n.setCategory(ifm.getCategory());
        n.setSubCategory(ifm.getSubCategory());
        n.setNotes(ifm.getNotes());
        n.setReceiverGroup(ifm.getReceiverGroup());
        n.setSender(ifm.getSender());
        n.setResponsible(ifm.getResponsible());
        return n;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public ZonedDateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(ZonedDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getStatusReason() {
        return statusReason;
    }

    public void setStatusReason(String statusReason) {
        this.statusReason = statusReason;
    }

    public NotificationCategory getCategory() {
        return category;
    }

    public void setCategory(NotificationCategory category) {
        this.category = category;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public NotificationSubCategory getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(NotificationSubCategory subCategory) {
        this.subCategory = subCategory;
    }

    public ReceiverGroup getReceiverGroup() {
        return receiverGroup;
    }

    public void setReceiverGroup(ReceiverGroup receiverGroup) {
        this.receiverGroup = receiverGroup;
    }

    public ExtendedUser getSender() {
        return sender;
    }

    public void setSender(ExtendedUser extendedUser) {
        this.sender = extendedUser;
    }

    public ExtendedUser getResponsible() {
        return responsible;
    }

    public void setResponsible(ExtendedUser extendedUser) {
        this.responsible = extendedUser;
    }

}
