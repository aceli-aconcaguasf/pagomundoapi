package com.pagomundo.domain.DTO;

import com.pagomundo.domain.ExtendedUser;

import java.util.List;

public class ExtendedUserListDTO {
    private List<ExtendedUser> extendedUsers;

    private List<ExtendedUser> extendedUsersWithErrors;

    public ExtendedUserListDTO() {
    }

    public ExtendedUserListDTO(List<ExtendedUser> userDTOList) {
    }

    public List<ExtendedUser> getExtendedUsersWithErrors() {
        return extendedUsersWithErrors;
    }

    public void setExtendedUsersWithErrors(List<ExtendedUser> extendedUsersWithErrors) {
        this.extendedUsersWithErrors = extendedUsersWithErrors;
    }

    public List<ExtendedUser> getExtendedUsers() {
        return extendedUsers;
    }

    public void setExtendedUsers(List<ExtendedUser> extendedUsers) {
        this.extendedUsers = extendedUsers;
    }

    @Override
    public String toString() {
        return "ExtendedUserListDTO{" +
            ", extendedUsers=" + extendedUsers +
            ", extendedUsersWithErrors=" + extendedUsersWithErrors +
            '}';
    }
}
