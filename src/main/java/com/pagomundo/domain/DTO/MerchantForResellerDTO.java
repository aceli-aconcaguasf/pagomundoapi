package com.pagomundo.domain.DTO;

import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.enumeration.Status;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.List;

/**
 * A DTO representing a merchant, with his authorities.
 */
public class MerchantForResellerDTO {

    private Long id;

    @Enumerated(EnumType.STRING)
    private Status status;

    private String statusReason;

    private String fullName;

    private String email;

    private String company;

    private List<PayeeForMerchantDTO> payees;

    public MerchantForResellerDTO() {
    }

    public MerchantForResellerDTO(ExtendedUser eu, List<PayeeForMerchantDTO> payeesForMerchant) {
        this.id = eu.getId();
        this.status = eu.getStatus();
        this.statusReason = eu.getStatusReason();
        this.fullName = eu.getFullName();
        this.email = eu.getEmail();
        this.company = eu.getCompany();
        this.payees = payeesForMerchant;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public List<PayeeForMerchantDTO> getPayees() {
        return payees;
    }

    public void setPayees(List<PayeeForMerchantDTO> payees) {
        this.payees = payees;
    }

    public String getStatusReason() {
        return statusReason;
    }

    public void setStatusReason(String statusReason) {
        this.statusReason = statusReason;
    }
}
