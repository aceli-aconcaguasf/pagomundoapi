package com.pagomundo.domain.DTO;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

public class PayeeResultDTO {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String transactionReference;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private BigDecimal amount;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String currency;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private PayeeForTransactionDTO metadata;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String ticketNumber;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String type;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String date;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String message;

    public String getTransactionReference() {
        return transactionReference;
    }

    public void setTransactionReference(String transactionReference) {
        this.transactionReference = transactionReference;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public PayeeForTransactionDTO getMetadata() {
        return metadata;
    }

    public void setMetadata(PayeeForTransactionDTO metadata) {
        this.metadata = metadata;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMessage() { return message; }

    public void setMessage(String message) { this.message = message; }
}
