package com.pagomundo.domain.DTO;

import com.pagomundo.domain.ExtendedUserFunds;
import com.pagomundo.domain.enumeration.Status;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

/**
 * A DTO representing a fund
 */

public class ExtendedUserFundsDTO implements Serializable {

    private Long id;
    private ZonedDateTime dateCreated;
    private BigDecimal value;
    private String reason;
    private Status status;
    private ZonedDateTime lastUpdated;

    public ExtendedUserFundsDTO() {
    }

    public ExtendedUserFundsDTO(ExtendedUserFunds euf) {
        if (euf == null) return;
        this.id = euf.getId();
        this.dateCreated = euf.getDateCreated();
        this.value = euf.getValue();
        this.reason = euf.getReason();
        this.status = euf.getStatus();
        this.lastUpdated = euf.getLastUpdated();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public ZonedDateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(ZonedDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ExtendedUserFundsDTO)) {
            return false;
        }
        return id != null && id.equals(((ExtendedUserFundsDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ExtendedUserFunds{" +
            "id=" + getId() +
            ", dateCreated='" + getDateCreated() + "'" +
            ", value=" + getValue() +
            ", reason='" + getReason() + "'" +
            ", status='" + getStatus() + "'" +
            ", lastUpdated='" + getLastUpdated() + "'" +
            "}";
    }
}
