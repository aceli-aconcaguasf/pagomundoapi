package com.pagomundo.domain.DTO;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.pagomundo.domain.DTO.Kushki.*;

import java.util.List;
import java.util.Set;
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KushkiDTO {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String token;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private AmountDto amount;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private ContactDetailsDTO contactDetails;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private OrderDetailsDTO orderDetails;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private ProductDetailsDTO productDetails;

    private boolean fullResponse;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public AmountDto getAmount() { return amount; }

    public void setAmount(AmountDto amount) {
        this.amount = amount;
    }

    public ContactDetailsDTO getContactDetails() {return contactDetails; }

    public void setContactDetails(ContactDetailsDTO contactDetails) {this.contactDetails = contactDetails; }

    public OrderDetailsDTO getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(OrderDetailsDTO orderDetails) {
        this.orderDetails = orderDetails;
    }

    public ProductDetailsDTO getProductDetails() {
        return productDetails;
    }

    public void setProductDetails(ProductDetailsDTO productDetails) {
        this.productDetails = productDetails;
    }

    public boolean isFullResponse() {
        return fullResponse;
    }

    public void setFullResponse(boolean fullResponse) {
        this.fullResponse = fullResponse;
    }

    @Override
    public String toString() {
        return "KushkiDTO{" +
            "token='" + token + '\'' +
            ", amount=" + amount +
            ", contactDetails=" + contactDetails +
            ", orderDetails=" + orderDetails +
            ", productDetails=" + productDetails +
            '}';
    }
}
