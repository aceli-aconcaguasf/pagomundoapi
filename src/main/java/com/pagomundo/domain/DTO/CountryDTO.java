package com.pagomundo.domain.DTO;

import com.pagomundo.domain.Country;

/**
 * A DTO representing a country
 */
public class CountryDTO {

    private Long id;
    private String name;
    private String code;

    public CountryDTO() {
    }

    public CountryDTO(Country country) {
        if (country == null) return;
        this.id = country.getId();
        this.name = country.getName();
        this.code = country.getCode();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CountryDTO)) {
            return false;
        }
        return id != null && id.equals(((CountryDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }
}
