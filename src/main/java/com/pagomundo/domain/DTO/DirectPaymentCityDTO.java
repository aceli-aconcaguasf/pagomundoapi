package com.pagomundo.domain.DTO;

import com.pagomundo.domain.DirectPaymentBank;

/**
 * A DTO representing a DirectPaymentBankDTO
 */
public class DirectPaymentCityDTO {

    private Long id;    // DirectPaymentBank
    private String name; // DestinyCity.name

    public DirectPaymentCityDTO() {
    }

    public DirectPaymentCityDTO(DirectPaymentBank dpb) {
        if (dpb == null) return;
        this.id = dpb.getId();
        if (dpb.getDestinyCity() != null)
            this.name = dpb.getDestinyCity().getName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DirectPaymentCityDTO)) {
            return false;
        }
        return id != null && id.equals(((DirectPaymentCityDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }
}
