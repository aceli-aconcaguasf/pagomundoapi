package com.pagomundo.domain.DTO;

import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.Transaction;
import com.pagomundo.domain.enumeration.Status;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * A DTO representing a payment for a merchant
 */
public class TransactionForMerchantDTO {

    private Long id;
    private ZonedDateTime dateCreated;
    private ZonedDateTime lastUpdated;
    @Enumerated(EnumType.STRING)
    private Status status;
    private String statusReason;
    private PayeeForMerchantDTO payee;
    private BigDecimal amount;
    private String notes;
    private String description;
    private List<TransactionStatusChangeForMerchantDTO> transactionStatusChanges = new ArrayList<>();

    public TransactionForMerchantDTO() {
    }

    public TransactionForMerchantDTO(Transaction t) {
        if (t == null) return;
        this.id = t.getId();
        this.dateCreated = t.getDateCreated();
        this.lastUpdated = t.getLastUpdated();
        this.status = t.getStatus();
        this.statusReason = t.getStatusReason();
        this.amount = t.getAmountBeforeCommission();
        this.notes = t.getNotes();
        this.payee = new PayeeForMerchantDTO(t.getPayee());
        this.description = t.getDescription();
        t.getTransactionStatusChanges().forEach(
            tsc -> this.transactionStatusChanges.add(new TransactionStatusChangeForMerchantDTO(tsc))
        );
    }

    public TransactionForMerchantDTO(Transaction t, String nickname) {
        this(t);
        this.payee = new PayeeForMerchantDTO(t.getPayee(), nickname);
    }

    public static Transaction transactionForMerchantToTransaction(TransactionForMerchantDTO tForM) {
        if (tForM == null) return null;
        Transaction t = new Transaction();
        t.setAmountBeforeCommission(tForM.getAmount());
        t.setDescription(tForM.getDescription());
        t.setPayee(new ExtendedUser(tForM.getPayee()));
        return t;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public ZonedDateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(ZonedDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getStatusReason() {
        return statusReason;
    }

    public void setStatusReason(String statusReason) {
        this.statusReason = statusReason;
    }

    public PayeeForMerchantDTO getPayee() {
        return payee;
    }

    public void setPayee(PayeeForMerchantDTO payee) {
        this.payee = payee;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public List<TransactionStatusChangeForMerchantDTO> getTransactionStatusChanges() {
        return transactionStatusChanges;
    }

    public void setTransactionStatusChanges(List<TransactionStatusChangeForMerchantDTO> transactionStatusChanges) {
        this.transactionStatusChanges = transactionStatusChanges;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", dateCreated='" + getDateCreated() + "'" +
            ", lastUpdated='" + getLastUpdated() + "'" +
            ", status='" + getStatus() + "'" +
            ", statusReason='" + getStatusReason() + "'" +
            ", payee='" + getPayee() + "'" +
            ", amount='" + getAmount() + "'" +
            ", notes='" + getNotes() + "'" +
            ", description='" + getDescription() + "'" +
            ", transactionStatusChanges='" + getTransactionStatusChanges() + "'" +
            "}";
    }


}
