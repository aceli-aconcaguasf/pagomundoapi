package com.pagomundo.domain.DTO;

import java.util.ArrayList;
import java.util.List;

public class PayeeListDTO {
    private List<InvitedUserDTO> payees = new ArrayList<>();
    private List<InvitedUserDTO> payeesWithErrors = new ArrayList<>();
    private List<InvitedUserDTO> payeesAlreadyExist = new ArrayList<>();

    public PayeeListDTO() {
    }

    public PayeeListDTO(UserListDTO userListDTO) {
        if (userListDTO == null) return;

        if (userListDTO.getUsersCreatedAndAssociated() != null && !userListDTO.getUsersCreatedAndAssociated().isEmpty()) {
            this.setPayees(new ArrayList<>());
            userListDTO.getUsersCreatedAndAssociated().forEach(user -> this.getPayees().add(new InvitedUserDTO(user)));
        }
        if (userListDTO.getUserNotCreated() != null && !userListDTO.getUserNotCreated().isEmpty()) {
            this.setPayeesWithErrors(new ArrayList<>());
            userListDTO.getUserNotCreated().forEach(user -> this.getPayeesWithErrors().add(new InvitedUserDTO(user)));
        }
        if (userListDTO.getUserExistingAndNotAssociated() != null && !userListDTO.getUserExistingAndNotAssociated().isEmpty()) {
            userListDTO.getUserExistingAndNotAssociated().forEach(user -> this.getPayeesWithErrors().add(new InvitedUserDTO(user)));
        }
        if (userListDTO.getUserExistingAndAssociated() != null && !userListDTO.getUserExistingAndAssociated().isEmpty()) {
            this.setPayeesAlreadyExist(new ArrayList<>());
            userListDTO.getUserExistingAndAssociated().forEach(user -> this.getPayeesAlreadyExist().add(new InvitedUserDTO(user)));
        }
    }

    public static UserListDTO payeeListDTOToUserListDTO(PayeeListDTO payeeListDTO) {
        if (payeeListDTO == null) return null;
        UserListDTO userListDTO = new UserListDTO();

        if (payeeListDTO.getPayees() != null && !payeeListDTO.getPayees().isEmpty()) {
            userListDTO.setUsersCreatedAndAssociated(new ArrayList<>());
            userListDTO.setUsers(new ArrayList<>());
            payeeListDTO.getPayees().forEach(payee -> {
                userListDTO.getUsersCreatedAndAssociated().add(InvitedUserDTO.invitedUserDTOToUserDTO(payee));
                userListDTO.getUsers().add(InvitedUserDTO.invitedUserDTOToUserDTO(payee));
            });
        }
        if (payeeListDTO.getPayeesWithErrors() != null && !payeeListDTO.getPayeesWithErrors().isEmpty()) {
            userListDTO.setUserNotCreated(new ArrayList<>());
            payeeListDTO.getPayeesWithErrors().forEach(payee -> userListDTO.getUserNotCreated().add(InvitedUserDTO.invitedUserDTOToUserDTO(payee)));
        }
        if (payeeListDTO.getPayeesAlreadyExist() != null && !payeeListDTO.getPayeesAlreadyExist().isEmpty()) {
            userListDTO.setUserExistingAndAssociated(new ArrayList<>());
            payeeListDTO.getPayeesAlreadyExist().forEach(payee -> userListDTO.getUserExistingAndAssociated().add(InvitedUserDTO.invitedUserDTOToUserDTO(payee)));
        }

        return userListDTO;

    }

    public List<InvitedUserDTO> getPayees() {
        return payees;
    }

    public void setPayees(List<InvitedUserDTO> payees) {
        this.payees = payees;
    }

    public List<InvitedUserDTO> getPayeesWithErrors() {
        return payeesWithErrors;
    }

    public void setPayeesWithErrors(List<InvitedUserDTO> payeesWithErrors) {
        this.payeesWithErrors = payeesWithErrors;
    }

    public List<InvitedUserDTO> getPayeesAlreadyExist() {
        return payeesAlreadyExist;
    }

    public void setPayeesAlreadyExist(List<InvitedUserDTO> payeesAlreadyExist) {
        this.payeesAlreadyExist = payeesAlreadyExist;
    }
}
