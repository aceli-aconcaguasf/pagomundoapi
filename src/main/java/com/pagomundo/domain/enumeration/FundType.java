package com.pagomundo.domain.enumeration;

/**
 * The FundType enumeration.
 */
public enum FundType {
    FUNDING, WITHDRAWAL
}
