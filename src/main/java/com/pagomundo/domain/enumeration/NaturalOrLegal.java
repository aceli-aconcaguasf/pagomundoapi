package com.pagomundo.domain.enumeration;

/**
 * The NaturalOrLegal enumeration.
 */
public enum NaturalOrLegal {
    NATURAL, LEGAL, BOTH
}
