package com.pagomundo.domain.enumeration.stp;

import java.util.Arrays;

public enum StpBankId {
    ABC_CAPITAL("40138", "138"),
    ACCIVAL("90614", "614"),
    ACTINBER("90621", "621"),
    ACTINVER("40133", "133"),
    AFIRME("40062", "062"),
    AKALA("90638", "638"),
    AMERICAN_EXPRESS("40103", "103"),
    ASEA("90652", "652"),
    AUTOFIN("40128", "128"),
    AXA("90674", "674"),
    AZTECA("40127", "127"),
    B_B("90610", "610"),
    BAJIO("40030", "030"),
    BAMSA("40106", "106"),
    BANAMEX("40002", "002"),
    BANCO_FINTERRA("40154", "154"),
    BANCOMEXT("37006", "006"),
    BANCOPPEL("40137", "137"),
    BANCREA("40152", "152"),
    BANJERCITO("37019", "019"),
    BANKAOOL("40147", "147"),
    BANOBRAS("37009", "009"),
    BANORTE("40072", "072"),
    BANREGIO("40058", "058"),
    BANSEFI("37166", "166"),
    BANSI("40060", "060"),
    BARCLAYS("40129", "129"),
    BBASE("40145", "145"),
    BBVA_BANCOMER("40012", "012"),
    BIM("40150", "150"),
    BMONEX("40112", "112"),
    BMULTIVA("40132", "132"),
    BOLSA("90631", "631"),
    BULLTICK("90632", "632"),
    CAJA_POP_MEXICA("90677", "677"),
    CAJA_TELEFONIST("90683", "683"),
    CBDEUTSCHE("90626", "626"),
    CIBANCO("40143", "143"),
    CLS("90901", "901"),
    COMPARTAMOS("40130", "130"),
    CONSUBANCO("40140", "140"),
    CONSULTORIA("90624", "624"),
    CREDIT_SUISSE("40126", "126"),
    CRISTOBAL_COLON("90680", "680"),
    DEL_NOROESTE("90659", "659"),
    DEUTSCHE("40124", "124"),
    DONDE("40151", "151"),
    ESTRUCTURADORES("90606", "606"),
    EVERCORE("90648", "648"),
    FAMSA("40131", "131"),
    FINAMEX("90616", "616"),
    FINCOMUN("90634", "634"),
    FND("90679", "679"),
    FOMPED("90689", "689"),
    FONDO_FIR("90685", "685"),
    GBM("90601", "601"),
    GE_MONEY("40022", "022"),
    HDI_SEGUROS("90636", "636"),
    HIPOTECARIA_FEDERAL("37168", "168"),
    HSBC("40021", "021"),
    HUASTECAS("90671", "671"),
    ICBC("40155", "155"),
    INBURSA("40036", "036"),
    CB_INBURSA("90604", "604"),
    INDEVAL("90902", "902"),
    INFONAVIT("90687", "687"),
    ING("40116", "116"),
    INTERACCIONES("40037", "037"),
    INTERBANCO("40136", "136"),
    INTERCAM("90611", "611"),
    CB_INTERCAM("90630", "630"),
    INVERCAP("90686", "686"),
    INVEX("40059", "059"),
    IXE("40032", "032"),
    JP_MORGAN("40110", "110"),
    CB_JPMORGAN("90640", "640"),
    KUSPIT("90653", "653"),
    LIBERTAD("90670", "670"),
    MAPFRE("90619", "619"),
    MASARI_CB("90602", "602"),
    MERRILL_LYNCH("90615", "615"),
    MIFEL("40042", "042"),
    MIZUHO_BANK("40158", "158"),
    MONEXCB("90600", "600"),
    MULTIVA("90613", "613"),
    NAFIN("37135", "135"),
    OACTIN("90622", "622"),
    ORDER("90637", "637"),
    PAGATODO("40148", "148"),
    PRINCIPAL("90681", "681"),
    PROFUTURO("90620", "620"),
    REFORMA("90642", "642"),
    SABADELL("40156", "156"),
    SANTANDER("40014", "014"),
    SCOTIABANK("40044", "044"),
    SEGMTY("90651", "651"),
    SKANDIA("90623", "623"),
    OSKNDIA("90649", "649"),
    SOFIEXPRESS("90655", "655"),
    STERLING("90633", "633"),
    STP("90646", "646"),
    STPDEMO("846", "846"),
    SU_CASITA("90629", "629"),
    SURA("90678", "678"),
    TELECOMM("90647", "647"),
    THE_ROYAL_BANK("40102", "102"),
    TIBER("90607", "607"),
    TOKYO("40108", "108"),
    TRANSFER("90684", "684"),
    UBS_BANK("40139", "139"),
    UNAGRA("90656", "656"),
    UNICA("90618", "618"),
    VALMEX("90617", "617"),
    VALUE("90605", "605"),
    VE_POR_MAS("40113", "113"),
    VECTOR("90608", "608"),
    VOLKSWAGEN("40141", "141"),
    WAL_MART("40134", "134"),
    ZURICH("90627", "627"),
    ZURICHVI("90628", "628"),
    ABC("12345678910","123")
    ;

    private final String value;
    private final String description;

    StpBankId(String value, String description) {
        this.value = value;
        this.description = description;
    }

    public static StpBankId findByDescription(final String description) {
        return Arrays.stream(values()).filter(v -> v.getDescription().equalsIgnoreCase(description)).findFirst().orElse(null);
    }

    public static StpBankId findByValue(final String value) {
        return Arrays.stream(values()).filter(v -> v.getValue().equalsIgnoreCase(value)).findFirst().orElse(null);
    }

    public String getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }
}
