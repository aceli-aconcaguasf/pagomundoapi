package com.pagomundo.domain.enumeration.stp;

public enum StpPaymentType {
    THIRD_PARTY("1");

    private final String value;

    StpPaymentType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
