package com.pagomundo.domain.enumeration.stp;

public enum StpConversionFields {
    trackingId("claveRastreo"),
    description("conceptoPago"),
    accountNumberDestiny("cuentaBeneficiario"),
    accountNumberOrigin("cuentaOrdenante"),
    emailDestiny("emailBeneficiario"),
    company("empresa"),
    operationDate("fechaOperacion"),
    electronicSignature("firma"),
    bankIdDestiny("institucionContraparte"),
    bankIdOrigin("institucionOperante"),
    amount("monto"),
    destinyName("nombreBeneficiario"),
    originName("nombreOrdenante"),
    referenceNumber("referenciaNumerica"),
    rfcCurpDestiny("rfcCurpBeneficiario"),
    rfcCurpOrigin("rfcCurpOrdenante"),
    accountTypeDestiny("tipoCuentaBeneficiario"),
    accountTypeOrigin("tipoCuentaOrdenante"),
    paymentType("tipoPago"),
    topology("topologia"),
    ;

    private final String value;

    StpConversionFields(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
