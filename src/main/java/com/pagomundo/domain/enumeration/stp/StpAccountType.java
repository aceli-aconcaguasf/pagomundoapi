package com.pagomundo.domain.enumeration.stp;

public enum StpAccountType {
    DEBIT_CARD("3", "Tarjeta de débito"),
    CELLPHONE("10", "Teléfono celular"),
    CLABE("40", "CLABE"),
    ACCOUNT_NUMBER_FOR_DEMO("846180000400000001", "Just for STP demo environment");

    private final String value;
    private final String description;

    StpAccountType(String value, String description) {
        this.value = value;
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }
}
