package com.pagomundo.domain.enumeration;

import com.google.common.collect.ImmutableMap;
import com.pagomundo.domain.Country;
import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.Transaction;
import com.pagomundo.security.AuthoritiesConstants;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

/**
 * The StatisticsTransactionColumnsReport enumeration.
 */
public enum StatisticsTransactionColumnsReport {
    ID("statistics.column.report.transaction.id"),
    ADMIN("statistics.column.report.transaction.admin"),
    MERCHANT_COMPANY("statistics.column.report.transaction.merchant.company"),
    PAYEE_BUSINESS("statistics.column.report.transaction.payee.business"),
    COMMISSION_PAID("statistics.column.report.transaction.commission.paid"),
    COUNTRY("statistics.column.report.transaction.country"),
    DATE("statistics.column.report.transaction.date"),
    CREATION_DATE("statistics.column.report.transaction.creation.date"),
    STATUS("statistics.column.report.transaction.status"),
    STATUS_REASON("statistics.column.report.transaction.status.reason"),
    DESCRIPTION("statistics.column.report.transaction.description"),
    AMOUNT("statistics.column.report.transaction.amount"),
    BANK_COMMISSION("statistics.column.report.transaction.bank.commission"),
    FX_COMMISSION("statistics.column.report.transaction.fx.commission"),
    RECHARGE_COST("statistics.column.report.transaction.recharge.cost"),
    AFTER_COMMISSION("statistics.column.report.transaction.after.commission"),
    EXCHANGE_RATE("statistics.column.report.transaction.exchange.rate"),
    LOCAL_CURRENCY("statistics.column.report.transaction.local.currency"),
    NOTES("statistics.column.report.transaction.notes");

    private static List<StatisticsTransactionColumnsReport> adminSuperAdminColumns = List.of(
        ID,
        ADMIN,
        MERCHANT_COMPANY,
        PAYEE_BUSINESS,
        COUNTRY,
        DATE,
        CREATION_DATE,
        STATUS,
        STATUS_REASON,
        DESCRIPTION,
        AMOUNT,
        BANK_COMMISSION,
        FX_COMMISSION,
        RECHARGE_COST,
        AFTER_COMMISSION,
        EXCHANGE_RATE,
        LOCAL_CURRENCY,
        NOTES);

    private static List<StatisticsTransactionColumnsReport> resellerColumns = List.of(
        ID,
        ADMIN,
        MERCHANT_COMPANY,
        PAYEE_BUSINESS,
        COMMISSION_PAID,
        COUNTRY,
        DATE,
        CREATION_DATE,
        STATUS,
        STATUS_REASON,
        AMOUNT,
        NOTES);

    private static List<StatisticsTransactionColumnsReport> merchantColumns = List.of(
        ID,
        PAYEE_BUSINESS,
        COUNTRY,
        DATE,
        CREATION_DATE,
        STATUS,
        STATUS_REASON,
        DESCRIPTION,
        AMOUNT,
        NOTES);

    private static List<StatisticsTransactionColumnsReport> payeeColumns = List.of(
        ID,
        MERCHANT_COMPANY,
        DATE,
        CREATION_DATE,
        STATUS,
        STATUS_REASON,
        DESCRIPTION,
        LOCAL_CURRENCY,
        NOTES);

    private static Map<String, List<StatisticsTransactionColumnsReport>> roleColumnsPermission = ImmutableMap.of(
        AuthoritiesConstants.SUPER_ADMIN, adminSuperAdminColumns,
        AuthoritiesConstants.ADMIN, adminSuperAdminColumns,
        AuthoritiesConstants.RESELLER, resellerColumns,
        AuthoritiesConstants.MERCHANT, merchantColumns,
        AuthoritiesConstants.PAYEE, payeeColumns);

    private String columnName;

    StatisticsTransactionColumnsReport(String columnName) {
        this.columnName = columnName;
    }

    /**
     * getColumnsValuesByRole
     * <p>
     * return an array with column value by role
     *
     * @param role        user role
     * @param transaction transaction where return values by column
     * @return a array with values
     */
    public static Object[] getColumnsValuesByRole(String role, Transaction transaction) {
        Object[] arrayValues = new Object[roleColumnsPermission.get(role).size()];
        int positionArray = 0;

        for (StatisticsTransactionColumnsReport statisticsTransactionColumnsReport : roleColumnsPermission.get(role)) {
            arrayValues[positionArray] = getColumnValue(statisticsTransactionColumnsReport, transaction);
            positionArray++;
        }

        return arrayValues;
    }

    /**
     * getColumnsHeadersByRole
     * <p>
     * Return an array with headers
     *
     * @param role role for filter header
     * @return an array with header
     */
    public static String[] getColumnsHeadersByRole(String role) {
        String[] columnsHeaders = new String[roleColumnsPermission.get(role).size()];
        int positionArray = 0;

        for (StatisticsTransactionColumnsReport statisticsTransactionColumnsReport : roleColumnsPermission.get(role)) {
            columnsHeaders[positionArray] = statisticsTransactionColumnsReport.getColumnName();
            positionArray++;
        }

        return columnsHeaders;
    }

    /**
     * getColumnValue
     * <p>
     * generate a value by column for report
     *
     * @param statisticsTransactionColumnsReport column
     * @param transaction                        traansactio for return value
     * @return a value by report
     */
    private static Object getColumnValue(StatisticsTransactionColumnsReport statisticsTransactionColumnsReport, Transaction transaction) {

        switch (statisticsTransactionColumnsReport) {
            case ID:
                return String.valueOf(transaction.getId());
            case ADMIN:
                return transaction.getAdmin() != null ? transaction.getAdmin().getFullName() : null;
            case MERCHANT_COMPANY:
                return transaction.getMerchant() != null ? transaction.getMerchant().getCompany() : null;
            case PAYEE_BUSINESS:
                return getValueByObject(transaction.getPayee());
            case COMMISSION_PAID:
                return getValueByObject(transaction.getResellerCommission());
            case COUNTRY:
                String countryName = null;
                if (transaction.getPayee() != null) {
                    countryName = (String) getValueByObject(transaction.getPayee().getPostalCountry());
                }
                return countryName;
            case DATE:
                return getValueByObject(transaction.getLastUpdated());
            case CREATION_DATE:
                return getValueByObject(transaction.getDateCreated());
            case STATUS:
                return Status.getStringFromTransactionStatus(transaction.getStatus());
            case STATUS_REASON:
                return transaction.getStatusReason();
            case DESCRIPTION:
                return transaction.getDescription();
            case AMOUNT:
                return getValueByObject(transaction.getAmountBeforeCommission());
            case BANK_COMMISSION:
                return getValueByObject(transaction.getBankCommission());
            case FX_COMMISSION:
                return getValueByObject(transaction.getFxCommission());
            case RECHARGE_COST:
                return getValueByObject(transaction.getRechargeCost());
            case AFTER_COMMISSION:
                return getValueByObject(transaction.getAmountAfterCommission());
            case EXCHANGE_RATE:
                return getValueByObject(transaction.getExchangeRate());
            case LOCAL_CURRENCY:
                return getValueByObject(transaction.getAmountLocalCurrency());
            case NOTES:
                return transaction.getNotes();
            default:
                return null;
        }
    }

    /**
     * getValueByObject
     * <p>
     * Validate field and return an object byreport
     *
     * @param field field to validate
     * @return an Object by report
     */
    private static Object getValueByObject(Object field) {
        Object objectReturn = null;
        if (field != null) {
            if (field instanceof BigDecimal) {
                objectReturn = ((BigDecimal) field).doubleValue();
            } else if (field instanceof Country) {
                objectReturn = ((Country) field).getName();
            } else if (field instanceof ZonedDateTime) {
                objectReturn = ((ZonedDateTime) field).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            } else if (field instanceof Float) {
                objectReturn = ((Float) field).doubleValue();
            } else if (field instanceof ExtendedUser) {
                ExtendedUser extendedUser = (ExtendedUser) field;
                if (extendedUser.getUser() != null &&
                    extendedUser.getUser().isPayeeAsCompany() != null &&
                    extendedUser.getUser().isPayeeAsCompany()) {

                    objectReturn = extendedUser.getCompany();
                } else {
                    objectReturn = extendedUser.getFullName();
                }
            }
        }
        return objectReturn;
    }

    public String getColumnName() {
        return columnName;
    }
}
