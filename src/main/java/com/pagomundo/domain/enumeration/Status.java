package com.pagomundo.domain.enumeration;

/**
 * The Status enumeration.
 */
public enum Status {
    ALREADY_REPORTED, INVITED, PENDING_PROFILE,
    CREATED, IN_PROCESS, ACCEPTED, REJECTED, CANCELLED, FAILED, REVERTED, SETTLEMENT_DECLINED;
    
    public static Status getStatusFromString(String stringStatus) {
        switch (stringStatus.toUpperCase()) {
            case "ALREADY_REPORTED":
                return ALREADY_REPORTED;
            case "CREATED":
                return CREATED;
            case "IN_PROCESS":
                return IN_PROCESS;
            case "ACCEPTED":
                return ACCEPTED;
            case "REJECTED":
                return REJECTED;
            case "CANCELLED":
                return CANCELLED;
            case "FAILED":
                return FAILED;
            case "REVERTED":
                return REVERTED;
            case "SETTLEMENT_DECLINED":
                return SETTLEMENT_DECLINED;
            default:
                return null;
        }
    }
    
    public static String getStringFromTransactionStatus(Status status) {
        switch (status) {
            case ALREADY_REPORTED:
                return "Already reported";
            case CREATED:
                return "Pending";
            case IN_PROCESS:
                return "In process";
            case ACCEPTED:
                return "Completed";
            case REJECTED:
                return "Rejected";
            case CANCELLED:
                return "Cancelled";
            case FAILED:
                return "Failed";
            case REVERTED:
                return "Reverted";
            case SETTLEMENT_DECLINED:
                return "Settlement declined";
            default:
                return null;
        }
        
    }
    
    public static String getStringFromStatus(Status status) {
        switch (status) {
            case CREATED:
                return "CREATED";
            case IN_PROCESS:
                return "IN_PROCESS";
            case ACCEPTED:
                return "ACCEPTED";
            case REJECTED:
                return "REJECTED";
            case CANCELLED:
                return "CANCELLED";
            case FAILED:
                return "FAILED";
            case REVERTED:
                return "REVERTED";
            case SETTLEMENT_DECLINED:
                return "SETTLEMENT_DECLINED";
            default:
                return null;
        }
        
    }
    
    public static String getStringFromUserStatus(Status status) {
        switch (status) {
            case ALREADY_REPORTED:
                return "Already reported";
            case CREATED:
                return "Pending";
            case IN_PROCESS:
                return "Processing";
            case ACCEPTED:
                return "Active";
            case REJECTED:
                return "Disabled";
            case CANCELLED:
                return "Deleted";
            case FAILED:
                return "Failed";
            default:
                return null;
        }
    }
}
