package com.pagomundo.domain.enumeration;

/**
 * The BankAccountType enumeration.
 */
public enum BankAccountType {
    AHO, CTE;

    public static String getStringFromBankAccountType(BankAccountType bankAccountType) {
        switch (bankAccountType) {
            case AHO:
                return "CUENTA DE AHORROS";
            case CTE:
                return "CUENTA CORRIENTE";
            default:
                return null;
        }
    }

    public static String getStringFromBankAccountTypeBrazil(BankAccountType bankAccountType) {
        switch (bankAccountType) {
            case AHO:
                return "A";
            case CTE:
                return "C";
            default:
                return null;
        }
    }
}
