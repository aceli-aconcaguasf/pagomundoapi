package com.pagomundo.domain.enumeration;

/**
 * The NotificationSubCategory enumeration.
 */
public enum NotificationSubCategory {
    MESSAGE,
    ISSUE_CREATED, ISSUE_IN_PROCESS, ISSUE_ACCEPTED, ISSUE_REJECTED, ISSUE_CANCELLED,
    TRANSACTION_CREATED, TRANSACTION_IN_PROCESS, TRANSACTION_ACCEPTED, TRANSACTION_REJECTED, TRANSACTION_CANCELLED,
    FUND_CREATED, FUND_ACCEPTED, FUND_REJECTED, FUND_CANCELLED,
    WITHDRAWAL_CREATED, WITHDRAWAL_ACCEPTED, WITHDRAWAL_REJECTED, WITHDRAWAL_CANCELLED,
    USER_CREATED, USER_FORGOT_PASSWORD, USER_FORGOT_PASSWORD_NEW_ACCOUNT, USER_EMAIL_CHANGE, USER_EMAIL_CHANGE_CONFIRMATION,
    EXTENDED_USER_CREATED, EXTENDED_USER_IN_PROCESS, EXTENDED_USER_ACCEPTED, EXTENDED_USER_REJECTED, EXTENDED_USER_CANCELLED, EXTENDED_USER_CHANGE_PROFILE,
    RELATION_ACCEPTED, RELATION_CANCELLED,
    DOWNLOADED_FILE, TRANSACTION_SETTLEMENT_DECLINED;
    
    public static NotificationCategory getNotificationCategory(NotificationSubCategory notificationSubCategory) {
        switch (notificationSubCategory) {
            case MESSAGE:
                return NotificationCategory.MESSAGE;
            case DOWNLOADED_FILE:
                return NotificationCategory.DOWNLOADED_FILE;
            case ISSUE_CREATED:
            case ISSUE_IN_PROCESS:
            case ISSUE_ACCEPTED:
            case ISSUE_REJECTED:
            case ISSUE_CANCELLED:
                return NotificationCategory.ISSUE;
            case TRANSACTION_CREATED:
            case TRANSACTION_IN_PROCESS:
            case TRANSACTION_ACCEPTED:
            case TRANSACTION_REJECTED:
            case TRANSACTION_CANCELLED:
            case TRANSACTION_SETTLEMENT_DECLINED:
                return NotificationCategory.TRANSACTION;
            case FUND_CREATED:
            case FUND_ACCEPTED:
            case FUND_REJECTED:
            case FUND_CANCELLED:
                return NotificationCategory.FUND;
            case WITHDRAWAL_CREATED:
            case WITHDRAWAL_ACCEPTED:
            case WITHDRAWAL_REJECTED:
            case WITHDRAWAL_CANCELLED:
                return NotificationCategory.WITHDRAWAL;
            case EXTENDED_USER_CREATED:
            case EXTENDED_USER_IN_PROCESS:
            case EXTENDED_USER_ACCEPTED:
            case EXTENDED_USER_REJECTED:
            case EXTENDED_USER_CANCELLED:
            case EXTENDED_USER_CHANGE_PROFILE:
                return NotificationCategory.EXTENDED_USER;
            case USER_CREATED:
            case USER_FORGOT_PASSWORD:
            case USER_FORGOT_PASSWORD_NEW_ACCOUNT:
            case USER_EMAIL_CHANGE:
            case USER_EMAIL_CHANGE_CONFIRMATION:
                return NotificationCategory.USER_ACCOUNT;
            case RELATION_ACCEPTED:
            case RELATION_CANCELLED:
                return NotificationCategory.RELATION;
            default:
                return null;
        }
    }
}
