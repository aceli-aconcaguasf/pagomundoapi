package com.pagomundo.domain.enumeration;

/**
 * The NotificationCategory enumeration.
 */
public enum NotificationCategory {
    ISSUE, TRANSACTION, FUND, WITHDRAWAL, EXTENDED_USER, USER_ACCOUNT, MESSAGE, RELATION, DOWNLOADED_FILE
}
