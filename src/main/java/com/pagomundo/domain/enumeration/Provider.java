package com.pagomundo.domain.enumeration;

public enum Provider {
    STP("STP"),
    OTHER("OTHER");

    private final String value;

    Provider(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
