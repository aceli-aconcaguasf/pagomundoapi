package com.pagomundo.domain.enumeration;

/**
 * The ProcessType enumeration.
 */
public enum ProcessType {
    FILE_EXPORT, API_CALL;

    public static ProcessType getProcessTypeFromString(String stringProcessType) {
        switch (stringProcessType.toUpperCase()) {
            case "FILE_EXPORT":
                return FILE_EXPORT;
            case "API_CALL":
                return API_CALL;
            default:
                return null;
        }
    }

    public static String getStringFromTransactionProcessType(ProcessType processType) {
        switch (processType) {
            case FILE_EXPORT:
                return "Exported to file";
            case API_CALL:
                return "API call";
            default:
                return null;
        }
    }

    public static String getStringFromUserProcessType(ProcessType processType) {
        return ProcessType.getStringFromTransactionProcessType(processType);
    }
}
