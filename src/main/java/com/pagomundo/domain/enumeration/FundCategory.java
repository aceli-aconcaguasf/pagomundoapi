package com.pagomundo.domain.enumeration;

/**
 * The FundCategory enumeration.
 */
public enum FundCategory {
    REQUESTED, AUTOMATIC
}
