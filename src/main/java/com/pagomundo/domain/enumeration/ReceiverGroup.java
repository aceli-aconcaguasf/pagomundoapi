package com.pagomundo.domain.enumeration;

/**
 * The ReceiverGroup enumeration.
 */
public enum ReceiverGroup {
    ALL_ADMINS, ALL_RESELLERS, ALL_MERCHANTS, ALL_PAYEES,
    ALL_MERCHANTS_FROM_RESELLER, ALL_PAYEES_FROM_MERCHANT, ALL_MERCHANTS_FROM_PAYEE,
    RECEIVERS;

    public static ReceiverGroup getReceiverGroup(NotificationCategory notificationCategory) {
        switch (notificationCategory) {
            case TRANSACTION:
            case EXTENDED_USER:
            case USER_ACCOUNT:
                return RECEIVERS;
            case ISSUE:
            case MESSAGE:
            default:
                return null;
        }
    }
}
