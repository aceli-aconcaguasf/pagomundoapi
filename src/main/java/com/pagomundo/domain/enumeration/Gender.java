package com.pagomundo.domain.enumeration;

/**
 * The Gender enumeration.
 */
public enum Gender {
    MALE, FEMALE;

    public static Gender getGenderFromString(String stringGender) {
        switch (stringGender.toUpperCase()) {
            case "MALE":
                return MALE;
            case "FEMALE":
                return FEMALE;
            default:
                return null;
        }
    }

    public static String getGenderCode(Gender gender) {
        switch (gender) {
            case MALE:
                return "M";
            case FEMALE:
                return "F";
            default:
                return null;
        }
    }

}
