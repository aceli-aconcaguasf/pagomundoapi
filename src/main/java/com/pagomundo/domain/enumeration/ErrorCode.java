package com.pagomundo.domain.enumeration;

public enum ErrorCode {
    ERROR_VALIDATING_FIELDS(10000),
    ERROR_CALLING_STP(10001),
    ERROR_IN_DATABASE(10002),
    ERROR_UNDEFINED(20000);

    private final Integer value;

    ErrorCode(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }
}
