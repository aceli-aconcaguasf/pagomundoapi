package com.pagomundo.domain.enumeration;

/**
 * The MaritalStatus enumeration.
 */
public enum MaritalStatus {
    SINGLE, MARRIED, DIVORCED, FREE_UNION, WIDOWER
}
