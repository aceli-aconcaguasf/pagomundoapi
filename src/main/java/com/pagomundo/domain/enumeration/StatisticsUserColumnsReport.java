package com.pagomundo.domain.enumeration;

import com.google.common.collect.ImmutableMap;
import com.pagomundo.domain.City;
import com.pagomundo.domain.Country;
import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.IdType;
import com.pagomundo.security.AuthoritiesConstants;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * The StatisticsUserColumnsReport enumeration.
 */
public enum StatisticsUserColumnsReport {
    STATUS("statistics.column.report.user.status"),
    ROLE("statistics.column.report.user.role"),
    NAME("statistics.column.report.user.name"),
    EMAIL("statistics.column.report.user.email"),
    ID_NUMBER("statistics.column.report.user.id.number"),
    ID_TYPE("statistics.column.report.user.id.type"),
    BIRTH_DATE("statistics.column.report.user.birth.date"),
    GENDER("statistics.column.report.user.gender"),
    MARITAL_STATUS("statistics.column.report.user.marital.status"),
    COMPANY("statistics.column.report.user.company"),
    TAX_ID("statistics.column.report.user.tax.id"),
    BUSINESS_ADDRESS("statistics.column.report.user.business.address"),
    BUSINESS_ADDRESS_CITY("statistics.column.report.user.business.address.city"),
    BUSINESS_ADDRESS_COUNTRY("statistics.column.report.user.business.address.country"),
    PHONE_NUMBER("statistics.column.report.user.phone.number"),
    MOBILE_NUMBER("statistics.column.report.user.mobile.number"),
    ADDRESS("statistics.column.report.user.address"),
    ADDRESS_CITY("statistics.column.report.user.address.city"),
    ADDRESS_COUNTRY("statistics.column.report.user.address.country"),
    DATE_CREATED("statistics.column.report.user.date.created"),
    DATE_RELATED("statistics.column.report.user.date.related");

    private static final String PAYEE_ADDED_REPORT = "PAYEES ADDED";
    private static final String MERCHANT_ADDED_REPORT = "MERCHANTS ADDED";
    private static final String USERS_ADDED_STATUS_REPORT = "USERS ADDED STATUS";
    private static final String PAYEE_ADDED_STATUS_REPORT = "PAYEES ADDED STATUS";
    private static final String PAYEE_ADDED_COUNTRY_REPORT = "PAYEES ADDED COUNTRY";
    private static final String TITLE_CHART_PAYEES_ADDED_RECIEVED = "STATISTICS.PAYEES_ADDED_COUNTRY";
    private static final String TITLE_CHART_PAYEE_ADDED_STATUS_REPORT = "STATISTICS.PAYEES_ADDED_STATUS";
    
    private static final String BENEFICIARIES_ADDED_REPORT = "BENEFICIARIES ADDED";
    private static final String USER_STATUS_ADDED = "USER STATUS ADDED";
    private static final String COUNTRY_ADDED_BENEFICIARY="COUNTRY ADDED BENEFICIARY";
    private static final String PAYMENT_STATUS_ADDED="PAYMENT STATUS ADDED";
    

    private static List<StatisticsUserColumnsReport> payeeAddedColumns = List.of(
        STATUS,
        NAME,
        EMAIL,
        ID_TYPE,
        ID_NUMBER,
        BIRTH_DATE,
        GENDER,
        MARITAL_STATUS,
        ADDRESS,
        ADDRESS_CITY,
        ADDRESS_COUNTRY,
        MOBILE_NUMBER,
        COMPANY,
        TAX_ID,
        BUSINESS_ADDRESS,
        BUSINESS_ADDRESS_CITY,
        BUSINESS_ADDRESS_COUNTRY,
        PHONE_NUMBER);

    private static List<StatisticsUserColumnsReport> merchantAddedColumns = List.of(
        STATUS,
        NAME,
        EMAIL,
        ID_TYPE,
        ID_NUMBER,
        BIRTH_DATE,
        ADDRESS,
        ADDRESS_COUNTRY,
        MOBILE_NUMBER,
        COMPANY,
        TAX_ID,
        BUSINESS_ADDRESS,
        BUSINESS_ADDRESS_COUNTRY);

    private static List<StatisticsUserColumnsReport> userAddedStatusColumns = List.of(
        ROLE,
        NAME,
        EMAIL,
        STATUS);

    private static List<StatisticsUserColumnsReport> payeeAddedStatusColumns = List.of(
        NAME,
        EMAIL,
        STATUS);

    private static List<StatisticsUserColumnsReport> payeeAddedCountryColumns = List.of(
        NAME,
        EMAIL,
        ADDRESS_COUNTRY,
        ADDRESS_CITY);

    private static Map<String, List<StatisticsUserColumnsReport>> reportColumnsPermission = ImmutableMap.of(
        PAYEE_ADDED_REPORT, payeeAddedColumns,
        MERCHANT_ADDED_REPORT, merchantAddedColumns,
        USERS_ADDED_STATUS_REPORT, userAddedStatusColumns,
        PAYEE_ADDED_STATUS_REPORT, payeeAddedStatusColumns,
        PAYEE_ADDED_COUNTRY_REPORT, payeeAddedCountryColumns);

    private String columnName;

    StatisticsUserColumnsReport(String columnName) {
        this.columnName = columnName;
    }

    /**
     * getColumnsValuesByRoleList
     *
     * return an array with column value by role list
     *
     * @param user user by return date
     * @param userRole user role running the report
     * @param chartTitle report title
     * @param dateRelatedMap related date map
     * @return an array with column
     */
    public static Object[] getColumnsValuesByChartTitle(ExtendedUser user, String userRole, String chartTitle, Map<Long, ZonedDateTime> dateRelatedMap) {
        List<StatisticsUserColumnsReport> userColumnReportList = addDateColumnToReport(userRole, chartTitle);
        Object[] arrayValues = new Object[userColumnReportList.size()];
        int positionArray = 0;

        for (StatisticsUserColumnsReport statisticsTransactionColumnsReport : userColumnReportList) {
            arrayValues[positionArray] = getColumnValue(statisticsTransactionColumnsReport, user, dateRelatedMap);
            positionArray++;
        }

        return arrayValues;
    }

    /**
     * getColumnsHeadersByRoleList
     *
     * Return an array with column header from an a role
     *
     * @param userRole user running the report
     * @param chartTitle chart title
     * @return a array with headers
     */
    public static String[] getColumnsHeadersByChartTitle(String userRole, String chartTitle) {
        List<StatisticsUserColumnsReport> userColumnReportList = addDateColumnToReport(userRole, chartTitle);
        String[] columnsHeaders = new String[userColumnReportList.size()];
        int positionArray = 0;
        for (StatisticsUserColumnsReport statisticsUserColumnsReport : userColumnReportList) {
            columnsHeaders[positionArray] = statisticsUserColumnsReport.getColumnName();
            positionArray++;
        }

        return columnsHeaders;
    }

    /**
     * getColumnValue
     *
     * Return a value from a column
     *
     * @param statisticsUserColumnsReport column
     * @param user user from return value
     * @param dateRelatedMap relation date map
     * @return an Object
     */
    private static Object getColumnValue(StatisticsUserColumnsReport statisticsUserColumnsReport, ExtendedUser user, Map<Long, ZonedDateTime> dateRelatedMap) {

        switch (statisticsUserColumnsReport) {
            case STATUS:
                return Status.getStringFromUserStatus(user.getStatus());
            case ROLE:
                return user.getRole();
            case NAME:
                return user.getFullName();
            case EMAIL:
                return user.getEmail();
            case ID_NUMBER:
                return user.getIdNumber();
            case ID_TYPE:
                return getStringValueByObject(user.getIdType());
            case BIRTH_DATE:
                return getStringValueByObject(user.getBirthDate());
            case GENDER:
                return getStringValueByObject(user.getGender());
            case MARITAL_STATUS:
                return getStringValueByObject(user.getMaritalStatus());
            case COMPANY:
                return user.getCompany();
            case TAX_ID:
                return user.getTaxId();
            case BUSINESS_ADDRESS:
                return user.getPostalAddress();
            case BUSINESS_ADDRESS_CITY:
                return getStringValueByObject(user.getPostalCity());
            case BUSINESS_ADDRESS_COUNTRY:
                return getStringValueByObject(user.getPostalCountry());
            case PHONE_NUMBER:
                return user.getPhoneNumber();
            case MOBILE_NUMBER:
                return user.getMobileNumber();
            case ADDRESS:
                return user.getResidenceAddress();
            case ADDRESS_CITY:
                return getStringValueByObject(user.getResidenceCity());
            case ADDRESS_COUNTRY:
                return getStringValueByObject(user.getResidenceCountry());
            case DATE_CREATED:
                return getStringValueByObject(user.getDateCreated());
            case DATE_RELATED:
                return getStringValueByObject(dateRelatedMap.get(user.getId()));
            default:
                return null;
        }
    }

    /**
     * getStringValueByObject
     *
     * Return a String from an Object
     *
     * @param field object to parse
     * @return a String from an Object
     */
    private static String getStringValueByObject(Object field) {
        String stringValue = null;
        if (field != null) {
            if (field instanceof IdType) {
                stringValue = ((IdType) field).getName();
            } else if (field instanceof ZonedDateTime) {
                stringValue = ((ZonedDateTime) field).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            } else if (field instanceof Gender) {
                stringValue = Gender.getGenderCode(((Gender) field));
            } else if (field instanceof City) {
                stringValue = ((City) field).getName();
            } else if (field instanceof Country) {
                stringValue = ((Country) field).getName();
            } else if (field instanceof MaritalStatus) {
                stringValue = field.toString();
            }
        }
        return stringValue;
    }

    /**
     * addDateColumnToReport
     *
     * Add date to report by report and role
     *
     * @param userRole user role
     * @param chartTitle report title
     * @return List of columns to report
     */
    private static List<StatisticsUserColumnsReport> addDateColumnToReport(String userRole, String chartTitle) {
        if(TITLE_CHART_PAYEES_ADDED_RECIEVED.equals(chartTitle))chartTitle =  PAYEE_ADDED_COUNTRY_REPORT;
        if(TITLE_CHART_PAYEE_ADDED_STATUS_REPORT.equals(chartTitle))chartTitle = PAYEE_ADDED_STATUS_REPORT;
        // se agregan cambios para hacer match titulo
        if(BENEFICIARIES_ADDED_REPORT.equalsIgnoreCase(chartTitle))chartTitle = PAYEE_ADDED_REPORT;
        if(USER_STATUS_ADDED.equalsIgnoreCase(chartTitle))chartTitle = USERS_ADDED_STATUS_REPORT;
        if(COUNTRY_ADDED_BENEFICIARY.equalsIgnoreCase(chartTitle))chartTitle = PAYEE_ADDED_COUNTRY_REPORT;
        if(PAYMENT_STATUS_ADDED.equalsIgnoreCase(chartTitle))chartTitle = PAYEE_ADDED_STATUS_REPORT;
        
        List<StatisticsUserColumnsReport> userColumnReportList = new ArrayList<>(reportColumnsPermission.get(chartTitle));

        if ((AuthoritiesConstants.MERCHANT.equals(userRole) && (PAYEE_ADDED_COUNTRY_REPORT.equals(chartTitle) || PAYEE_ADDED_STATUS_REPORT.equals(chartTitle) || PAYEE_ADDED_REPORT.equals(chartTitle))) ||
            (AuthoritiesConstants.RESELLER.equals(userRole) && MERCHANT_ADDED_REPORT.equals(chartTitle))) {
            userColumnReportList.add(DATE_RELATED);
        } else {
            userColumnReportList.add(DATE_CREATED);
        }

        return userColumnReportList;
    }

    public String getColumnName() {
        return columnName;
    }
}
