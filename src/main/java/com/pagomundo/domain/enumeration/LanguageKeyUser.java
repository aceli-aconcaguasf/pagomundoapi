package com.pagomundo.domain.enumeration;

import java.util.List;

/**
 * The LanguageKeyUser enumeration.
 */
public enum LanguageKeyUser {
    LANGUAGE_ENGLISH("en"),
    LANGUAGE_SPANISH("es"),
    LANGUAGE_PORTUGUESE("pt");

    private static List<LanguageKeyUser> languageKeyList = List.of(LANGUAGE_ENGLISH, LANGUAGE_PORTUGUESE, LANGUAGE_SPANISH);
    private String langKey;

    LanguageKeyUser(String langKey) {
        this.langKey = langKey;
    }

    public static boolean verifyLangKey(String langKey) {
        boolean langKeyValid = false;
        if (langKey != null) {
            for (LanguageKeyUser langKeyInList : languageKeyList) {
                if (langKeyInList.getLangKey().equals(langKey)) {
                    langKeyValid = true;
                    break;
                }
            }
        }
        return langKeyValid;
    }

    public String getLangKey() {
        return langKey;
    }
}
