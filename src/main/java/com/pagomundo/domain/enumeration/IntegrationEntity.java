package com.pagomundo.domain.enumeration;

public enum IntegrationEntity {
    TRANSACTION("Transaction"),
    OTHERS("OTHERS");

    private final String value;

    IntegrationEntity(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
