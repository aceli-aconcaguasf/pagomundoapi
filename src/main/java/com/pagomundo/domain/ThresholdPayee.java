package com.pagomundo.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.pagomundo.domain.enumeration.ProcessType;
import com.pagomundo.domain.enumeration.Status;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * A ThresholdPayee.
 */
@Entity
@Table(name = "threshold_payee")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "threshold_payee")
public class ThresholdPayee implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "country_id")
    private Long countryId;

    @Column(name = "threshold")
    private Float threshold;

    @Column(name = "period")
    private String period;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getThreshold() {
        return this.threshold;
    }

    public void setThreshold(Float newThreshold) {
        this.threshold = newThreshold;
    }

    public Long getCountryId() {
        return this.countryId;
    }

    public void setCountryId(Long newCountryId) {
        this.countryId = newCountryId;
    }

    public String getPeriod() {
        return this.period;
    }

    public void setPeriod(String newPeriod) {
        this.period = newPeriod;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ThresholdPayee)) {
            return false;
        }
        return id != null && id.equals(((ThresholdPayee) o).id);
    }

    @Override
    public String toString() {
        return "ThresholdPayee{" +
            "id=" + getId() +
            "threshold=" + getThreshold() +
            "countryId=" + getCountryId() + 
            "period=" + getPeriod() +
            "}";
    }
}
