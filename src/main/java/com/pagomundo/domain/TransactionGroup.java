package com.pagomundo.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.pagomundo.domain.enumeration.Status;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * A TransactionGroup.
 */
@Entity
@Table(name = "transaction_group")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "transactiongroup")
public class TransactionGroup implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "date_created")
    private ZonedDateTime dateCreated;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "file_name_bex")
    private String fileNameBex;

    @Column(name= "file_bank_account")
    private String fileBankAccount;

    @Column(name = "exchange_rate")
    private Float exchangeRate;

    @Column(name = "file_name_from_bank")
    private String fileNameFromBank;

    @Column(name = "direct_payment")
    private Boolean directPayment;

    @OneToMany(mappedBy = "transactionGroup")
    private Set<Transaction> transactions = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "transactionGroups", allowSetters = true)
    private BankAccount bankAccount;

    @ManyToOne
    @JsonIgnoreProperties(allowSetters = true,
        value = {
            "status",
            "lastName1",
            "lastName2",
            "firstName1",
            "firstName2",
            "idNumber",
            "taxId",
            "gender",
            "maritalStatus",
            "residenceAddress",
            "postalAddress",
            "phoneNumber",
            "mobileNumber",
            "birthDate",
            "dateCreated",
            "lastUpdated",
            "email",
            "company",
            "imageIdUrl",
            "imageAddressUrl",
            "balance",
            "statusReason",
            "role",
            "cardNumber",
            "bankCommission",
            "fxCommission",
            "rechargeCost",
            "useMerchantCommission",
            "bankAccountNumber",
            "useDirectPayment",
            "bankAccountType",
            "canChangePaymentMethod",
            "mustNotify",
            "profileInfoChanged",
            "user",
            "userRelations",
            "notificationSenders",
            "notificationReceivers",
            "notificationResponsibles",
            "notificationStatusChanges",
            "transactionMerchants",
            "transactionPayees",
            "transactionAdmins",
            "transactionStatusChanges",
            "residenceCity",
            "postalCity",
            "idType",
            "extendedUserGroup",
            "bank",
            "residenceCountry",
            "postalCountry",
            "directPaymentBank",
            "directPaymentCity"
        })
    private ExtendedUser admin;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public TransactionGroup dateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
        return this;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public TransactionGroup status(Status status) {
        this.status = status;
        return this;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileNameBex() {
        return fileNameBex;
    }

    public void setFileNameBex(String fileNameBex) {
        this.fileNameBex = fileNameBex;
    }

    public String getFileBankAccount() { return fileBankAccount; }

    public void setFileBankAccount(String fileBankAccount) { this.fileBankAccount = fileBankAccount; }

    public TransactionGroup fileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public Float getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(Float exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public TransactionGroup exchangeRate(Float exchangeRate) {
        this.exchangeRate = exchangeRate;
        return this;
    }

    public String getFileNameFromBank() {
        return fileNameFromBank;
    }

    public void setFileNameFromBank(String fileNameFromBank) {
        this.fileNameFromBank = fileNameFromBank;
    }

    public TransactionGroup fileNameFromBank(String fileNameFromBank) {
        this.fileNameFromBank = fileNameFromBank;
        return this;
    }

    public Boolean isDirectPayment() {
        return directPayment;
    }

    public TransactionGroup directPayment(Boolean directPayment) {
        this.directPayment = directPayment;
        return this;
    }

    public void setDirectPayment(Boolean directPayment) {
        this.directPayment = directPayment;
    }

    public Set<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(Set<Transaction> transactions) {
        this.transactions = transactions;
    }

    public TransactionGroup transactions(Set<Transaction> transactions) {
        this.transactions = transactions;
        return this;
    }

    public TransactionGroup addTransaction(Transaction transaction) {
        this.transactions.add(transaction);
        transaction.setTransactionGroup(this);
        return this;
    }

    public TransactionGroup removeTransaction(Transaction transaction) {
        this.transactions.remove(transaction);
        transaction.setTransactionGroup(null);
        return this;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public TransactionGroup bankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
        return this;
    }

    public ExtendedUser getAdmin() {
        return admin;
    }

    public void setAdmin(ExtendedUser extendedUser) {
        this.admin = extendedUser;
    }

    public TransactionGroup admin(ExtendedUser extendedUser) {
        this.admin = extendedUser;
        return this;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TransactionGroup)) {
            return false;
        }
        return id != null && id.equals(((TransactionGroup) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "TransactionGroup{" +
            "id=" + getId() +
            ", dateCreated='" + getDateCreated() + "'" +
            ", status='" + getStatus() + "'" +
            ", fileName='" + getFileName() + "'" +
            ", fileNameBex='" + getFileNameBex() + "'" +
            ", fileBankAccount='" + getFileBankAccount() + "'" +
            ", exchangeRate=" + getExchangeRate() +
            ", fileNameFromBank='" + getFileNameFromBank() + "'" +
            ", directPayment='" + isDirectPayment() + "'" +
            "}";
    }
}
