package com.pagomundo.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pagomundo.domain.DTO.PayeeForMerchantDTO;
import com.pagomundo.domain.enumeration.BankAccountType;
import com.pagomundo.domain.enumeration.Gender;
import com.pagomundo.domain.enumeration.MaritalStatus;
import com.pagomundo.domain.enumeration.Status;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * A ExtendedUser.
 */
@Entity
@Table(name = "extended_user")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "extendeduser")
public class ExtendedUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    @Column(name = "last_name_1")
    private String lastName1;

    @Column(name = "last_name_2")
    private String lastName2;

    @Column(name = "first_name_1")
    private String firstName1;

    @Column(name = "first_name_2")
    private String firstName2;

    @Column(name = "full_name")
    private String fullName;

    @Transient
    @JsonProperty
    private String nickname;

    @Column(name = "id_number")
    private String idNumber;

    @Column(name = "tax_id")
    private String taxId;

    @Enumerated(EnumType.STRING)
    @Column(name = "gender")
    private Gender gender;

    @Enumerated(EnumType.STRING)
    @Column(name = "marital_status")
    private MaritalStatus maritalStatus;

    @Column(name = "residence_address")
    private String residenceAddress;

    @Column(name = "postal_address")
    private String postalAddress;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "mobile_number")
    private String mobileNumber;

    @Column(name = "birth_date")
    private ZonedDateTime birthDate;

    @Column(name = "date_created")
    private ZonedDateTime dateCreated;

    @Column(name = "last_updated")
    private ZonedDateTime lastUpdated;

    @Column(name = "email")
    private String email;

    @Column(name = "company")
    private String company;

    @Column(name = "image_id_url")
    private String imageIdUrl;

    @Column(name = "image_address_url")
    private String imageAddressUrl;

    @Column(name = "balance", precision = 21, scale = 2)
    private BigDecimal balance;

    @Lob
    @Column(name = "status_reason")
    private String statusReason;

    @Column(name = "role")
    private String role;

    @Column(name = "card_number")
    private String cardNumber;

    @Column(name = "bank_commission")
    private Float bankCommission;

    @Column(name = "fx_commission")
    private Float fxCommission;

    @Column(name = "recharge_cost")
    private Float rechargeCost;

    @Column(name = "use_merchant_commission")
    private Boolean useMerchantCommission;

    @Column(name = "bank_account_number")
    private String bankAccountNumber;

    @Column(name = "use_direct_payment")
    private Boolean useDirectPayment;

    @Enumerated(EnumType.STRING)
    @Column(name = "bank_account_type")
    private BankAccountType bankAccountType;

    @Column(name = "can_change_payment_method")
    private Boolean canChangePaymentMethod;

    @Column(name = "must_notify")
    private Boolean mustNotify;

    @Column(name = "sent_abc")
    private Boolean sentAbc;

    @Lob
    @Column(name = "profile_info_changed")
    private String profileInfoChanged;

    @Column(name = "confirmed_profile")
    private Boolean confirmedProfile;

    @Column(name = "reseller_fixed_commission")
    private Float resellerFixedCommission;

    @Column(name = "reseller_percentage_commission")
    private Float resellerPercentageCommission;

    @Column(name = "use_flat_commission")
    private Boolean useFlatCommission;
    @Column(name = "bank_branch")
    private String bankBranch;

    @Column(name = "alias")
    private String alias;

    @OneToOne
    @JoinColumn(unique = true)
    private User user;

    @OneToMany(mappedBy = "extendedUser")
    private Set<ExtendedUserRelation> userRelations = new HashSet<>();

    @OneToMany(mappedBy = "sender")
    private Set<Notification> notificationSenders = new HashSet<>();

    @OneToMany(mappedBy = "responsible")
    private Set<Notification> notificationResponsibles = new HashSet<>();

    @OneToMany(mappedBy = "user")
    private Set<NotificationStatusChange> notificationStatusChanges = new HashSet<>();

    @OneToMany(mappedBy = "merchant")
    private Set<Transaction> transactionMerchants = new HashSet<>();

    @OneToMany(mappedBy = "payee")
    private Set<Transaction> transactionPayees = new HashSet<>();

    @OneToMany(mappedBy = "admin")
    private Set<Transaction> transactionAdmins = new HashSet<>();

    @OneToMany(mappedBy = "user")
    private Set<TransactionStatusChange> transactionStatusChanges = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "extendedUsersForResidenceCities", allowSetters = true)
    private City residenceCity;

    @ManyToOne
    @JsonIgnoreProperties(value = "extendedUsersForPostalCities", allowSetters = true)
    private City postalCity;

    @ManyToOne
    @JsonIgnoreProperties(value = "extendedUsers", allowSetters = true)
    private IdType idType;

    @ManyToOne
    @JsonIgnoreProperties(value = "extendedUsers", allowSetters = true)
    private ExtendedUserGroup extendedUserGroup;

    @ManyToOne
    @JsonIgnoreProperties(value = "extendedUsers", allowSetters = true)
    private Bank bank;

    @ManyToOne
    @JsonIgnoreProperties(value = "extendedUsers", allowSetters = true)
    private Country residenceCountry;

    @ManyToOne
    @JsonIgnoreProperties(value = "extendedUsers", allowSetters = true)
    private Country postalCountry;

    @ManyToOne
    @JsonIgnoreProperties(value = "extendedUsers", allowSetters = true)
    private Bank directPaymentBank;

    @ManyToOne
    @JsonIgnoreProperties(value = "extendedUsers", allowSetters = true)
    private City directPaymentCity;

    @ManyToOne
    @JsonIgnoreProperties(value = "extendedUsers", allowSetters = true)
    private IdType idTypeTaxId;

    @ManyToOne
    @JsonIgnoreProperties(allowSetters = true,
        value = {
            "extendedUsers",
            "lastName1",
            "lastName2",
            "firstName1",
            "firstName2",
            "gender",
            "maritalStatus",
            "dateCreated",
            "imageIdUrl",
            "imageAddressUrl",
            "balance",
            "statusReason",
            "cardNumber",
            "bankCommission",
            "fxCommission",
            "rechargeCost",
            "useMerchantCommission",
            "bankAccountNumber",
            "useDirectPayment",
            "bankAccountType",
            "canChangePaymentMethod",
            "mustNotify",
            "profileInfoChanged",
            "userRelations",
            "extendedUserFunds",
            "notificationSenders",
            "notificationReceivers",
            "notificationResponsibles",
            "notificationStatusChanges",
            "transactionMerchants",
            "transactionPayees",
            "transactionAdmins",
            "transactionStatusChanges",
            "idType",
            "extendedUserGroup",
            "bank",
            "directPaymentBank",
            "directPaymentCity",
            "reseller"
        })
    private ExtendedUser reseller;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove

    public ExtendedUser() {
    }

    public ExtendedUser(PayeeForMerchantDTO payeeForMerchantDTO) {
        if (payeeForMerchantDTO == null) return;
        this.id = payeeForMerchantDTO.getId();
        this.nickname = payeeForMerchantDTO.getNickname();
        this.email = payeeForMerchantDTO.getEmail();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public ExtendedUser status(Status status) {
        this.status = status;
        return this;
    }

    public String getLastName1() {
        return lastName1;
    }

    public void setLastName1(String lastName1) {
        this.lastName1 = lastName1;
    }

    public ExtendedUser lastName1(String lastName1) {
        this.lastName1 = lastName1;
        return this;
    }

    public String getLastName2() {
        return lastName2;
    }

    public void setLastName2(String lastName2) {
        this.lastName2 = lastName2;
    }

    public ExtendedUser lastName2(String lastName2) {
        this.lastName2 = lastName2;
        return this;
    }

    public String getFirstName1() {
        return firstName1;
    }

    public void setFirstName1(String firstName1) {
        this.firstName1 = firstName1;
    }

    public ExtendedUser firstName1(String firstName1) {
        this.firstName1 = firstName1;
        return this;
    }

    public String getFirstName2() {
        return firstName2;
    }

    public void setFirstName2(String firstName2) {
        this.firstName2 = firstName2;
    }

    public ExtendedUser firstName2(String firstName2) {
        this.firstName2 = firstName2;
        return this;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public ExtendedUser fullName(String fullName) {
        this.fullName = fullName;
        return this;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public ExtendedUser idNumber(String idNumber) {
        this.idNumber = idNumber;
        return this;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

    public ExtendedUser taxId(String taxId) {
        this.taxId = taxId;
        return this;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public ExtendedUser gender(Gender gender) {
        this.gender = gender;
        return this;
    }

    public MaritalStatus getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(MaritalStatus maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public ExtendedUser maritalStatus(MaritalStatus maritalStatus) {
        this.maritalStatus = maritalStatus;
        return this;
    }

    public String getResidenceAddress() {
        return residenceAddress;
    }

    public void setResidenceAddress(String residenceAddress) {
        this.residenceAddress = residenceAddress;
    }

    public ExtendedUser residenceAddress(String residenceAddress) {
        this.residenceAddress = residenceAddress;
        return this;
    }

    public String getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(String postalAddress) {
        this.postalAddress = postalAddress;
    }

    public ExtendedUser postalAddress(String postalAddress) {
        this.postalAddress = postalAddress;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public ExtendedUser phoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public ExtendedUser mobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
        return this;
    }

    public ZonedDateTime getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(ZonedDateTime birthDate) {
        this.birthDate = birthDate;
    }

    public ExtendedUser birthDate(ZonedDateTime birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public ExtendedUser dateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
        return this;
    }

    public ZonedDateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(ZonedDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public ExtendedUser lastUpdated(ZonedDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ExtendedUser email(String email) {
        this.email = email;
        return this;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public ExtendedUser company(String company) {
        this.company = company;
        return this;
    }

    public String getImageIdUrl() {
        return imageIdUrl;
    }

    public void setImageIdUrl(String imageIdUrl) {
        this.imageIdUrl = imageIdUrl;
    }

    public ExtendedUser imageIdUrl(String imageIdUrl) {
        this.imageIdUrl = imageIdUrl;
        return this;
    }

    public String getImageAddressUrl() {
        return imageAddressUrl;
    }

    public void setImageAddressUrl(String imageAddressUrl) {
        this.imageAddressUrl = imageAddressUrl;
    }

    public ExtendedUser imageAddressUrl(String imageAddressUrl) {
        this.imageAddressUrl = imageAddressUrl;
        return this;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public ExtendedUser balance(BigDecimal balance) {
        this.balance = balance;
        return this;
    }

    public String getStatusReason() {
        return statusReason;
    }

    public void setStatusReason(String statusReason) {
        this.statusReason = statusReason;
    }

    public ExtendedUser statusReason(String statusReason) {
        this.statusReason = statusReason;
        return this;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public ExtendedUser role(String role) {
        this.role = role;
        return this;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public ExtendedUser cardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
        return this;
    }

    public Float getBankCommission() {
        return bankCommission;
    }

    public void setBankCommission(Float bankCommission) {
        this.bankCommission = bankCommission;
    }

    public ExtendedUser bankCommission(Float bankCommission) {
        this.bankCommission = bankCommission;
        return this;
    }

    public Float getFxCommission() {
        return fxCommission;
    }

    public void setFxCommission(Float fxCommission) {
        this.fxCommission = fxCommission;
    }

    public ExtendedUser fxCommission(Float fxCommission) {
        this.fxCommission = fxCommission;
        return this;
    }

    public Float getRechargeCost() {
        return rechargeCost;
    }

    public void setRechargeCost(Float rechargeCost) {
        this.rechargeCost = rechargeCost;
    }

    public ExtendedUser rechargeCost(Float rechargeCost) {
        this.rechargeCost = rechargeCost;
        return this;
    }

    public Boolean isUseMerchantCommission() {
        return useMerchantCommission;
    }

    public ExtendedUser useMerchantCommission(Boolean useMerchantCommission) {
        this.useMerchantCommission = useMerchantCommission;
        return this;
    }

    public void setUseMerchantCommission(Boolean useMerchantCommission) {
        this.useMerchantCommission = useMerchantCommission;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public ExtendedUser bankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
        return this;
    }

    public Boolean isUseDirectPayment() {
        return useDirectPayment;
    }

    public ExtendedUser useDirectPayment(Boolean useDirectPayment) {
        this.useDirectPayment = useDirectPayment;
        return this;
    }

    public void setUseDirectPayment(Boolean useDirectPayment) {
        this.useDirectPayment = useDirectPayment;
    }

    public BankAccountType getBankAccountType() {
        return bankAccountType;
    }

    public void setBankAccountType(BankAccountType bankAccountType) {
        this.bankAccountType = bankAccountType;
    }

    public ExtendedUser bankAccountType(BankAccountType bankAccountType) {
        this.bankAccountType = bankAccountType;
        return this;
    }

    public Boolean isCanChangePaymentMethod() {
        return canChangePaymentMethod;
    }

    public ExtendedUser canChangePaymentMethod(Boolean canChangePaymentMethod) {
        this.canChangePaymentMethod = canChangePaymentMethod;
        return this;
    }

    public void setCanChangePaymentMethod(Boolean canChangePaymentMethod) {
        this.canChangePaymentMethod = canChangePaymentMethod;
    }

    public Boolean isMustNotify() {
        return mustNotify;
    }

    public ExtendedUser mustNotify(Boolean mustNotify) {
        this.mustNotify = mustNotify;
        return this;
    }

    public void setMustNotify(Boolean mustNotify) {
        this.mustNotify = mustNotify;
    }

    public String getProfileInfoChanged() {
        return profileInfoChanged;
    }

    public void setProfileInfoChanged(String profileInfoChanged) {
        this.profileInfoChanged = profileInfoChanged;
    }

    public ExtendedUser profileInfoChanged(String profileInfoChanged) {
        this.profileInfoChanged = profileInfoChanged;
        return this;
    }

    public Boolean isConfirmedProfile() {
        return confirmedProfile;
    }

    public ExtendedUser confirmedProfile(Boolean confirmedProfile) {
        this.confirmedProfile = confirmedProfile;
        return this;
    }

    public void setConfirmedProfile(Boolean confirmedProfile) {
        this.confirmedProfile = confirmedProfile;
    }

    public Float getResellerFixedCommission() {
        return resellerFixedCommission;
    }

    public void setResellerFixedCommission(Float resellerFixedCommission) {
        this.resellerFixedCommission = resellerFixedCommission;
    }

    public ExtendedUser resellerFixedCommission(Float resellerFixedCommission) {
        this.resellerFixedCommission = resellerFixedCommission;
        return this;
    }

    public Float getResellerPercentageCommission() {
        return resellerPercentageCommission;
    }

    public void setResellerPercentageCommission(Float resellerPercentageCommission) {
        this.resellerPercentageCommission = resellerPercentageCommission;
    }

    public ExtendedUser resellerPercentageCommission(Float resellerPercentageCommission) {
        this.resellerPercentageCommission = resellerPercentageCommission;
        return this;
    }

    public Boolean isUseFlatCommission() {
        return useFlatCommission;
    }

    public ExtendedUser useFlatCommission(Boolean useFlatCommission) {
        this.useFlatCommission = useFlatCommission;
        return this;
    }

    public void setUseFlatCommission(Boolean useFlatCommission) {
        this.useFlatCommission = useFlatCommission;
    }

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }

    public ExtendedUser bankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
        return this;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public ExtendedUser alias(String alias) {
        this.alias = alias;
        return this;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ExtendedUser user(User user) {
        this.user = user;
        return this;
    }

    public Set<ExtendedUserRelation> getUserRelations() {
        return userRelations;
    }

    public void setUserRelations(Set<ExtendedUserRelation> extendedUserRelations) {
        this.userRelations = extendedUserRelations;
    }

    public ExtendedUser userRelations(Set<ExtendedUserRelation> extendedUserRelations) {
        this.userRelations = extendedUserRelations;
        return this;
    }

    public ExtendedUser addUserRelations(ExtendedUserRelation extendedUserRelation) {
        this.userRelations.add(extendedUserRelation);
        extendedUserRelation.setExtendedUser(this);
        return this;
    }

    public ExtendedUser removeUserRelations(ExtendedUserRelation extendedUserRelation) {
        this.userRelations.remove(extendedUserRelation);
        extendedUserRelation.setExtendedUser(null);
        return this;
    }

    public Set<Notification> getNotificationSenders() {
        return notificationSenders;
    }

    public void setNotificationSenders(Set<Notification> notifications) {
        this.notificationSenders = notifications;
    }

    public ExtendedUser notificationSenders(Set<Notification> notifications) {
        this.notificationSenders = notifications;
        return this;
    }

    public ExtendedUser addNotificationSenders(Notification notification) {
        this.notificationSenders.add(notification);
        notification.setSender(this);
        return this;
    }

    public ExtendedUser removeNotificationSenders(Notification notification) {
        this.notificationSenders.remove(notification);
        notification.setSender(null);
        return this;
    }

    public Set<Notification> getNotificationResponsibles() {
        return notificationResponsibles;
    }

    public void setNotificationResponsibles(Set<Notification> notifications) {
        this.notificationResponsibles = notifications;
    }

    public ExtendedUser notificationResponsibles(Set<Notification> notifications) {
        this.notificationResponsibles = notifications;
        return this;
    }

    public ExtendedUser addNotificationResponsibles(Notification notification) {
        this.notificationResponsibles.add(notification);
        notification.setResponsible(this);
        return this;
    }

    public ExtendedUser removeNotificationResponsibles(Notification notification) {
        this.notificationResponsibles.remove(notification);
        notification.setResponsible(null);
        return this;
    }

    public Set<NotificationStatusChange> getNotificationStatusChanges() {
        return notificationStatusChanges;
    }

    public void setNotificationStatusChanges(Set<NotificationStatusChange> notificationStatusChanges) {
        this.notificationStatusChanges = notificationStatusChanges;
    }

    public ExtendedUser notificationStatusChanges(Set<NotificationStatusChange> notificationStatusChanges) {
        this.notificationStatusChanges = notificationStatusChanges;
        return this;
    }

    public ExtendedUser addNotificationStatusChange(NotificationStatusChange notificationStatusChange) {
        this.notificationStatusChanges.add(notificationStatusChange);
        notificationStatusChange.setUser(this);
        return this;
    }

    public ExtendedUser removeNotificationStatusChange(NotificationStatusChange notificationStatusChange) {
        this.notificationStatusChanges.remove(notificationStatusChange);
        notificationStatusChange.setUser(null);
        return this;
    }

    public Set<Transaction> getTransactionMerchants() {
        return transactionMerchants;
    }

    public void setTransactionMerchants(Set<Transaction> transactions) {
        this.transactionMerchants = transactions;
    }

    public ExtendedUser transactionMerchants(Set<Transaction> transactions) {
        this.transactionMerchants = transactions;
        return this;
    }

    public ExtendedUser addTransactionMerchants(Transaction transaction) {
        this.transactionMerchants.add(transaction);
        transaction.setMerchant(this);
        return this;
    }

    public ExtendedUser removeTransactionMerchants(Transaction transaction) {
        this.transactionMerchants.remove(transaction);
        transaction.setMerchant(null);
        return this;
    }

    public Set<Transaction> getTransactionPayees() {
        return transactionPayees;
    }

    public void setTransactionPayees(Set<Transaction> transactions) {
        this.transactionPayees = transactions;
    }

    public ExtendedUser transactionPayees(Set<Transaction> transactions) {
        this.transactionPayees = transactions;
        return this;
    }

    public ExtendedUser addTransactionPayees(Transaction transaction) {
        this.transactionPayees.add(transaction);
        transaction.setPayee(this);
        return this;
    }

    public ExtendedUser removeTransactionPayees(Transaction transaction) {
        this.transactionPayees.remove(transaction);
        transaction.setPayee(null);
        return this;
    }

    public Set<Transaction> getTransactionAdmins() {
        return transactionAdmins;
    }

    public void setTransactionAdmins(Set<Transaction> transactions) {
        this.transactionAdmins = transactions;
    }

    public ExtendedUser transactionAdmins(Set<Transaction> transactions) {
        this.transactionAdmins = transactions;
        return this;
    }

    public ExtendedUser addTransactionAdmins(Transaction transaction) {
        this.transactionAdmins.add(transaction);
        transaction.setAdmin(this);
        return this;
    }

    public ExtendedUser removeTransactionAdmins(Transaction transaction) {
        this.transactionAdmins.remove(transaction);
        transaction.setAdmin(null);
        return this;
    }

    public Set<TransactionStatusChange> getTransactionStatusChanges() {
        return transactionStatusChanges;
    }

    public void setTransactionStatusChanges(Set<TransactionStatusChange> transactionStatusChanges) {
        this.transactionStatusChanges = transactionStatusChanges;
    }

    public ExtendedUser transactionStatusChanges(Set<TransactionStatusChange> transactionStatusChanges) {
        this.transactionStatusChanges = transactionStatusChanges;
        return this;
    }

    public ExtendedUser addTransactionStatusChange(TransactionStatusChange transactionStatusChange) {
        this.transactionStatusChanges.add(transactionStatusChange);
        transactionStatusChange.setUser(this);
        return this;
    }

    public ExtendedUser removeTransactionStatusChange(TransactionStatusChange transactionStatusChange) {
        this.transactionStatusChanges.remove(transactionStatusChange);
        transactionStatusChange.setUser(null);
        return this;
    }

    public City getResidenceCity() {
        return residenceCity;
    }

    public void setResidenceCity(City city) {
        this.residenceCity = city;
    }

    public ExtendedUser residenceCity(City city) {
        this.residenceCity = city;
        return this;
    }

    public City getPostalCity() {
        return postalCity;
    }

    public void setPostalCity(City city) {
        this.postalCity = city;
    }

    public ExtendedUser postalCity(City city) {
        this.postalCity = city;
        return this;
    }

    public IdType getIdType() {
        return idType;
    }

    public void setIdType(IdType idType) {
        this.idType = idType;
    }

    public ExtendedUser idType(IdType idType) {
        this.idType = idType;
        return this;
    }

    public ExtendedUserGroup getExtendedUserGroup() {
        return extendedUserGroup;
    }

    public void setExtendedUserGroup(ExtendedUserGroup extendedUserGroup) {
        this.extendedUserGroup = extendedUserGroup;
    }

    public ExtendedUser extendedUserGroup(ExtendedUserGroup extendedUserGroup) {
        this.extendedUserGroup = extendedUserGroup;
        return this;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public ExtendedUser bank(Bank bank) {
        this.bank = bank;
        return this;
    }

    public Country getResidenceCountry() {
        return residenceCountry;
    }

    public void setResidenceCountry(Country country) {
        this.residenceCountry = country;
    }

    public ExtendedUser residenceCountry(Country country) {
        this.residenceCountry = country;
        return this;
    }

    public Country getPostalCountry() {
        return postalCountry;
    }

    public void setPostalCountry(Country country) {
        this.postalCountry = country;
    }

    public ExtendedUser postalCountry(Country country) {
        this.postalCountry = country;
        return this;
    }

    public Bank getDirectPaymentBank() {
        return directPaymentBank;
    }

    public void setDirectPaymentBank(Bank bank) {
        this.directPaymentBank = bank;
    }

    public ExtendedUser directPaymentBank(Bank bank) {
        this.directPaymentBank = bank;
        return this;
    }

    public City getDirectPaymentCity() {
        return directPaymentCity;
    }

    public void setDirectPaymentCity(City city) {
        this.directPaymentCity = city;
    }

    public ExtendedUser directPaymentCity(City city) {
        this.directPaymentCity = city;
        return this;
    }

    public IdType getIdTypeTaxId() {
        return idTypeTaxId;
    }

    public void setIdTypeTaxId(IdType idType) {
        this.idTypeTaxId = idType;
    }

    public ExtendedUser idTypeTaxId(IdType idType) {
        this.idTypeTaxId = idType;
        return this;
    }

    public ExtendedUser getReseller() {
        return reseller;
    }

    public void setReseller(ExtendedUser extendedUser) {
        this.reseller = extendedUser;
    }

    public ExtendedUser reseller(ExtendedUser extendedUser) {
        this.reseller = extendedUser;
        return this;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Boolean getSentAbc() {
        return sentAbc;
    }

    public void setSentAbc(Boolean sentAbc){
        this.sentAbc = sentAbc;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ExtendedUser)) {
            return false;
        }
        return id != null && id.equals(((ExtendedUser) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ExtendedUser{" +
            "id=" + getId() +
            ", status='" + getStatus() + "'" +
            ", lastName1='" + getLastName1() + "'" +
            ", lastName2='" + getLastName2() + "'" +
            ", firstName1='" + getFirstName1() + "'" +
            ", firstName2='" + getFirstName2() + "'" +
            ", fullName='" + getFullName() + "'" +
            ", idNumber='" + getIdNumber() + "'" +
            ", taxId='" + getTaxId() + "'" +
            ", gender='" + getGender() + "'" +
            ", maritalStatus='" + getMaritalStatus() + "'" +
            ", residenceAddress='" + getResidenceAddress() + "'" +
            ", postalAddress='" + getPostalAddress() + "'" +
            ", phoneNumber='" + getPhoneNumber() + "'" +
            ", mobileNumber='" + getMobileNumber() + "'" +
            ", birthDate='" + getBirthDate() + "'" +
            ", dateCreated='" + getDateCreated() + "'" +
            ", lastUpdated='" + getLastUpdated() + "'" +
            ", email='" + getEmail() + "'" +
            ", company='" + getCompany() + "'" +
            ", imageIdUrl='" + getImageIdUrl() + "'" +
            ", imageAddressUrl='" + getImageAddressUrl() + "'" +
            ", balance=" + getBalance() +
            ", statusReason='" + getStatusReason() + "'" +
            ", role='" + getRole() + "'" +
            ", cardNumber='" + getCardNumber() + "'" +
            ", bankCommission=" + getBankCommission() +
            ", fxCommission=" + getFxCommission() +
            ", rechargeCost=" + getRechargeCost() +
            ", useMerchantCommission='" + isUseMerchantCommission() + "'" +
            ", bankAccountNumber='" + getBankAccountNumber() + "'" +
            ", useDirectPayment='" + isUseDirectPayment() + "'" +
            ", bankAccountType='" + getBankAccountType() + "'" +
            ", canChangePaymentMethod='" + isCanChangePaymentMethod() + "'" +
            ", mustNotify='" + isMustNotify() + "'" +
            ", profileInfoChanged='" + getProfileInfoChanged() + "'" +
            ", confirmedProfile='" + isConfirmedProfile() + "'" +
            ", resellerFixedCommission=" + getResellerFixedCommission() +
            ", resellerPercentageCommission=" + getResellerPercentageCommission() +
            ", useFlatCommission='" + isUseFlatCommission() + "'" +
            ", bankBranch='" + getBankBranch() + "'" +
            ", alias='" + getAlias() + "'" +
            "}";
    }
}
