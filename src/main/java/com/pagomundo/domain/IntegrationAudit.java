package com.pagomundo.domain;

import com.pagomundo.domain.enumeration.Status;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A IntegrationAudit.
 */
@Entity
@Table(name = "integration_audit")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "integrationaudit")
public class IntegrationAudit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "request_date")
    private ZonedDateTime requestDate;

    @Lob
    @Column(name = "request_message")
    private String requestMessage;

    @Column(name = "request_endpoint")
    private String requestEndpoint;

    @Column(name = "response_date")
    private ZonedDateTime responseDate;

    @Lob
    @Column(name = "response_message")
    private String responseMessage;

    @Column(name = "entity_id")
    private Long entityId;

    @Column(name = "entity")
    private String entity;

    @Column(name = "provider_id")
    private String providerId;

    @Column(name = "provider")
    private String provider;

    @Column(name = "extra_response_endpoint")
    private String extraResponseEndpoint;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    @Lob
    @Column(name = "status_reason")
    private String statusReason;

    @Column(name = "retries")
    private Integer retries;

    @Column(name = "max_retries")
    private Integer maxRetries;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(ZonedDateTime requestDate) {
        this.requestDate = requestDate;
    }

    public IntegrationAudit requestDate(ZonedDateTime requestDate) {
        this.requestDate = requestDate;
        return this;
    }

    public String getRequestMessage() {
        return requestMessage;
    }

    public void setRequestMessage(String requestMessage) {
        this.requestMessage = requestMessage;
    }

    public IntegrationAudit requestMessage(String requestMessage) {
        this.requestMessage = requestMessage;
        return this;
    }

    public String getRequestEndpoint() {
        return requestEndpoint;
    }

    public void setRequestEndpoint(String requestEndpoint) {
        this.requestEndpoint = requestEndpoint;
    }

    public IntegrationAudit requestEndpoint(String requestEndpoint) {
        this.requestEndpoint = requestEndpoint;
        return this;
    }

    public ZonedDateTime getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(ZonedDateTime responseDate) {
        this.responseDate = responseDate;
    }

    public IntegrationAudit responseDate(ZonedDateTime responseDate) {
        this.responseDate = responseDate;
        return this;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public IntegrationAudit responseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
        return this;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public IntegrationAudit entityId(Long entityId) {
        this.entityId = entityId;
        return this;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public IntegrationAudit entity(String entity) {
        this.entity = entity;
        return this;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public IntegrationAudit providerId(String providerId) {
        this.providerId = providerId;
        return this;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public IntegrationAudit provider(String provider) {
        this.provider = provider;
        return this;
    }

    public String getExtraResponseEndpoint() {
        return extraResponseEndpoint;
    }

    public void setExtraResponseEndpoint(String extraResponseEndpoint) {
        this.extraResponseEndpoint = extraResponseEndpoint;
    }

    public IntegrationAudit extraResponseEndpoint(String extraResponseEndpoint) {
        this.extraResponseEndpoint = extraResponseEndpoint;
        return this;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public IntegrationAudit status(Status status) {
        this.status = status;
        return this;
    }

    public String getStatusReason() {
        return statusReason;
    }

    public void setStatusReason(String statusReason) {
        this.statusReason = statusReason;
    }

    public IntegrationAudit statusReason(String statusReason) {
        this.statusReason = statusReason;
        return this;
    }

    public Integer getRetries() {
        return retries;
    }

    public void setRetries(Integer retries) {
        this.retries = retries;
    }

    public IntegrationAudit retries(Integer retries) {
        this.retries = retries;
        return this;
    }

    public Integer getMaxRetries() {
        return maxRetries;
    }

    public void setMaxRetries(Integer maxRetries) {
        this.maxRetries = maxRetries;
    }

    public IntegrationAudit maxRetries(Integer maxRetries) {
        this.maxRetries = maxRetries;
        return this;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof IntegrationAudit)) {
            return false;
        }
        return id != null && id.equals(((IntegrationAudit) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "IntegrationAudit{" +
            "id=" + getId() +
            ", requestDate='" + getRequestDate() + "'" +
            ", requestMessage='" + getRequestMessage() + "'" +
            ", requestEndpoint='" + getRequestEndpoint() + "'" +
            ", responseDate='" + getResponseDate() + "'" +
            ", responseMessage='" + getResponseMessage() + "'" +
            ", entityId=" + getEntityId() +
            ", entity='" + getEntity() + "'" +
            ", providerId='" + getProviderId() + "'" +
            ", provider='" + getProvider() + "'" +
            ", extraResponseEndpoint='" + getExtraResponseEndpoint() + "'" +
            ", status='" + getStatus() + "'" +
            ", statusReason='" + getStatusReason() + "'" +
            ", retries=" + getRetries() +
            ", maxRetries=" + getMaxRetries() +
            "}";
    }
}
