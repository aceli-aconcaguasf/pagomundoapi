package com.pagomundo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

/**
 * A FlatCommission.
 */
@Entity
@Table(name = "flat_commission")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "flatcommission")
public class FlatCommission implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "up_to", precision = 21, scale = 2)
    private BigDecimal upTo;

    @Column(name = "commission")
    private Float commission;

    @Column(name = "description")
    private String description;

    @Column(name = "date_created")
    private ZonedDateTime dateCreated;

    @JsonIgnore
    @Column(name = "last_updated")
    private ZonedDateTime lastUpdated;

    @ManyToOne
    @JsonIgnoreProperties(allowSetters = true,
        value = {
            "flatCommissions",
            "lastName1",
            "lastName2",
            "firstName1",
            "firstName2",
            "idNumber",
            "taxId",
            "gender",
            "maritalStatus",
            "residenceAddress",
            "postalAddress",
            "phoneNumber",
            "mobileNumber",
            "birthDate",
            "dateCreated",
            "email",
            "company",
            "imageIdUrl",
            "imageAddressUrl",
            "balance",
            "cardNumber",
            "bankCommission",
            "fxCommission",
            "rechargeCost",
            "bankAccountNumber",
            "useDirectPayment",
            "bankAccountType",
            "canChangePaymentMethod",
            "mustNotify",
            "profileInfoChanged",
            "userRelations",
            "extendedUserFunds",
            "notificationSenders",
            "notificationReceivers",
            "notificationResponsibles",
            "notificationStatusChanges",
            "transactionMerchants",
            "transactionPayees",
            "transactionAdmins",
            "transactionStatusChanges",
            "residenceCity",
            "postalCity",
            "idType",
            "extendedUserGroup",
            "bank",
            "residenceCountry",
            "postalCountry",
            "directPaymentBank",
            "directPaymentCity",
            "idTypeTaxId",
            "user",
            "lastUpdated",
            "confirmedProfile",
            "resellerCommission",
            "fixedCommission",
            "resellerFixedCommission",
            "resellerPercentageCommission",
            "statusReason",
            "status",
            "nickname",
            "role",
            "reseller"
        })
    private ExtendedUser extendedUser;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getUpTo() {
        return upTo;
    }

    public void setUpTo(BigDecimal upTo) {
        this.upTo = upTo;
    }

    public FlatCommission upTo(BigDecimal upTo) {
        this.upTo = upTo;
        return this;
    }

    public Float getCommission() {
        return commission;
    }

    public void setCommission(Float commission) {
        this.commission = commission;
    }

    public FlatCommission commission(Float commission) {
        this.commission = commission;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public FlatCommission description(String description) {
        this.description = description;
        return this;
    }

    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public FlatCommission dateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
        return this;
    }

    public ZonedDateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(ZonedDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public FlatCommission lastUpdated(ZonedDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
        return this;
    }

    public ExtendedUser getExtendedUser() {
        return extendedUser;
    }

    public void setExtendedUser(ExtendedUser extendedUser) {
        this.extendedUser = extendedUser;
    }

    public FlatCommission extendedUser(ExtendedUser extendedUser) {
        this.extendedUser = extendedUser;
        return this;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FlatCommission)) {
            return false;
        }
        return id != null && id.equals(((FlatCommission) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "FlatCommission{" +
            "id=" + getId() +
            ", upTo=" + getUpTo() +
            ", commission=" + getCommission() +
            ", description='" + getDescription() + "'" +
            ", dateCreated='" + getDateCreated() + "'" +
            ", lastUpdated='" + getLastUpdated() + "'" +
            "}";
    }
}
