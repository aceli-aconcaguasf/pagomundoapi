package com.pagomundo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.pagomundo.domain.enumeration.Status;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A NotificationReceiver.
 */
@Entity
@Table(name = "notification_receiver")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "notificationreceiver")
public class NotificationReceiver implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "date_created")
    private ZonedDateTime dateCreated;

    @Column(name = "last_updated")
    private ZonedDateTime lastUpdated;

    @JsonIgnore
    @Column(name = "retries")
    private Integer retries;

    @JsonIgnore
    @Column(name = "max_retries")
    private Integer maxRetries;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    @Lob
    @Column(name = "reason")
    private String reason;

    @JsonIgnore
    @Column(name = "must_send_email")
    private Boolean mustSendEmail;

    @JsonIgnore
    @Column(name = "email_template")
    private String emailTemplate;

    @JsonIgnore
    @Column(name = "email_sent")
    private Boolean emailSent;

    @JsonIgnore
    @Column(name = "debug_reason")
    private String debugReason;

    @JsonIgnore
    @Column(name = "email_title_key")
    private String emailTitleKey;

    @Column(name = "subject")
    private String subject;

    @Lob
    @Column(name = "description")
    private String description;

    @ManyToOne
    @JsonIgnoreProperties(allowSetters = true,
        value = {"notificationReceivers"})
    private Notification notification;

    @ManyToOne
    @JsonIgnoreProperties(allowSetters = true,
        value = {
            "status",
            "lastName1",
            "lastName2",
            "firstName1",
            "firstName2",
            "idNumber",
            "taxId",
            "gender",
            "maritalStatus",
            "residenceAddress",
            "postalAddress",
            "phoneNumber",
            "mobileNumber",
            "birthDate",
            "dateCreated",
            "lastUpdated",
            "email",
            "company",
            "imageIdUrl",
            "imageAddressUrl",
            "balance",
            "statusReason",
            "role",
            "cardNumber",
            "bankCommission",
            "fxCommission",
            "rechargeCost",
            "useMerchantCommission",
            "bankAccountNumber",
            "useDirectPayment",
            "bankAccountType",
            "canChangePaymentMethod",
            "mustNotify",
            "profileInfoChanged",
            "user",
            "userRelations",
            "notificationSenders",
            "notificationReceivers",
            "notificationResponsibles",
            "notificationStatusChanges",
            "transactionMerchants",
            "transactionPayees",
            "transactionAdmins",
            "transactionStatusChanges",
            "residenceCity",
            "postalCity",
            "idType",
            "extendedUserGroup",
            "bank",
            "residenceCountry",
            "postalCountry",
            "directPaymentBank",
            "directPaymentCity"
        })
    private ExtendedUser receiver;

    @ManyToOne
    @JsonIgnoreProperties(allowSetters = true,
        value = {
            "status",
            "lastName1",
            "lastName2",
            "firstName1",
            "firstName2",
            "idNumber",
            "taxId",
            "gender",
            "maritalStatus",
            "residenceAddress",
            "postalAddress",
            "phoneNumber",
            "mobileNumber",
            "birthDate",
            "dateCreated",
            "lastUpdated",
            "email",
            "company",
            "imageIdUrl",
            "imageAddressUrl",
            "balance",
            "statusReason",
            "role",
            "cardNumber",
            "bankCommission",
            "fxCommission",
            "rechargeCost",
            "useMerchantCommission",
            "bankAccountNumber",
            "useDirectPayment",
            "bankAccountType",
            "canChangePaymentMethod",
            "mustNotify",
            "profileInfoChanged",
            "user",
            "userRelations",
            "notificationSenders",
            "notificationReceivers",
            "notificationResponsibles",
            "notificationStatusChanges",
            "transactionMerchants",
            "transactionPayees",
            "transactionAdmins",
            "transactionStatusChanges",
            "residenceCity",
            "postalCity",
            "idType",
            "extendedUserGroup",
            "bank",
            "residenceCountry",
            "postalCountry",
            "directPaymentBank",
            "directPaymentCity"
        })
    private ExtendedUser sender;

    @ManyToOne
    @JsonIgnoreProperties(allowSetters = true,
        value = {
            "password",
            "firstName",
            "lastName",
            "email",
            "activated",
            "langKey",
            "imageUrl",
            "activationKey",
            "resetKey",
            "resetDate",
            "authorities",
            "status",
            "statusReason"
        })
    private User userReceiver;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public NotificationReceiver dateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
        return this;
    }

    public ZonedDateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(ZonedDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public NotificationReceiver lastUpdated(ZonedDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
        return this;
    }

    public Integer getRetries() {
        return retries;
    }

    public void setRetries(Integer retries) {
        this.retries = retries;
    }

    public NotificationReceiver retries(Integer retries) {
        this.retries = retries;
        return this;
    }

    public Integer getMaxRetries() {
        return maxRetries;
    }

    public void setMaxRetries(Integer maxRetries) {
        this.maxRetries = maxRetries;
    }

    public NotificationReceiver maxRetries(Integer maxRetries) {
        this.maxRetries = maxRetries;
        return this;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public NotificationReceiver status(Status status) {
        this.status = status;
        return this;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public NotificationReceiver reason(String reason) {
        this.reason = reason;
        return this;
    }

    public Boolean isMustSendEmail() {
        return mustSendEmail;
    }

    public NotificationReceiver mustSendEmail(Boolean mustSendEmail) {
        this.mustSendEmail = mustSendEmail;
        return this;
    }

    public void setMustSendEmail(Boolean mustSendEmail) {
        this.mustSendEmail = mustSendEmail;
    }

    public String getEmailTemplate() {
        return emailTemplate;
    }

    public void setEmailTemplate(String emailTemplate) {
        this.emailTemplate = emailTemplate;
    }

    public NotificationReceiver emailTemplate(String emailTemplate) {
        this.emailTemplate = emailTemplate;
        return this;
    }

    public Boolean isEmailSent() {
        return emailSent;
    }

    public NotificationReceiver emailSent(Boolean emailSent) {
        this.emailSent = emailSent;
        return this;
    }

    public void setEmailSent(Boolean emailSent) {
        this.emailSent = emailSent;
    }

    public String getDebugReason() {
        return debugReason;
    }

    public void setDebugReason(String debugReason) {
        this.debugReason = debugReason;
    }

    public NotificationReceiver debugReason(String debugReason) {
        this.debugReason = debugReason;
        return this;
    }

    public String getEmailTitleKey() {
        return emailTitleKey;
    }

    public void setEmailTitleKey(String emailTitleKey) {
        this.emailTitleKey = emailTitleKey;
    }

    public NotificationReceiver emailTitleKey(String emailTitleKey) {
        this.emailTitleKey = emailTitleKey;
        return this;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public NotificationReceiver subject(String subject) {
        this.subject = subject;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public NotificationReceiver description(String description) {
        this.description = description;
        return this;
    }

    public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }

    public NotificationReceiver notification(Notification notification) {
        this.notification = notification;
        return this;
    }

    public ExtendedUser getReceiver() {
        return receiver;
    }

    public void setReceiver(ExtendedUser extendedUser) {
        this.receiver = extendedUser;
    }

    public NotificationReceiver receiver(ExtendedUser extendedUser) {
        this.receiver = extendedUser;
        return this;
    }

    public ExtendedUser getSender() {
        return sender;
    }

    public void setSender(ExtendedUser extendedUser) {
        this.sender = extendedUser;
    }

    public NotificationReceiver sender(ExtendedUser extendedUser) {
        this.sender = extendedUser;
        return this;
    }

    public User getUserReceiver() {
        return userReceiver;
    }

    public void setUserReceiver(User user) {
        this.userReceiver = user;
    }

    public NotificationReceiver userReceiver(User user) {
        this.userReceiver = user;
        return this;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NotificationReceiver)) {
            return false;
        }
        return id != null && id.equals(((NotificationReceiver) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "NotificationReceiver{" +
            "id=" + getId() +
            ", dateCreated='" + getDateCreated() + "'" +
            ", lastUpdated='" + getLastUpdated() + "'" +
            ", retries=" + getRetries() +
            ", maxRetries=" + getMaxRetries() +
            ", status='" + getStatus() + "'" +
            ", reason='" + getReason() + "'" +
            ", mustSendEmail='" + isMustSendEmail() + "'" +
            ", emailTemplate='" + getEmailTemplate() + "'" +
            ", emailSent='" + isEmailSent() + "'" +
            ", debugReason='" + getDebugReason() + "'" +
            ", emailTitleKey='" + getEmailTitleKey() + "'" +
            ", subject='" + getSubject() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
