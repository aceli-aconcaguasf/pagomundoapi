package com.pagomundo.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.pagomundo.domain.enumeration.NaturalOrLegal;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A IdType.
 */
@Entity
@Table(name = "id_type")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "idtype")
public class IdType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "code")
    private String code;

    @Enumerated(EnumType.STRING)
    @Column(name = "natural_or_legal")
    private NaturalOrLegal naturalOrLegal;

    @OneToMany(mappedBy = "idType")
    private Set<ExtendedUser> extendedUsers = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("idTypes")
    private Country country;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public IdType name(String name) {
        this.name = name;
        return this;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public IdType code(String code) {
        this.code = code;
        return this;
    }

    public NaturalOrLegal getNaturalOrLegal() {
        return naturalOrLegal;
    }

    public void setNaturalOrLegal(NaturalOrLegal naturalOrLegal) {
        this.naturalOrLegal = naturalOrLegal;
    }

    public IdType naturalOrLegal(NaturalOrLegal naturalOrLegal) {
        this.naturalOrLegal = naturalOrLegal;
        return this;
    }

    public Set<ExtendedUser> getExtendedUsers() {
        return extendedUsers;
    }

    public void setExtendedUsers(Set<ExtendedUser> extendedUsers) {
        this.extendedUsers = extendedUsers;
    }

    public IdType extendedUsers(Set<ExtendedUser> extendedUsers) {
        this.extendedUsers = extendedUsers;
        return this;
    }

    public IdType addExtendedUser(ExtendedUser extendedUser) {
        this.extendedUsers.add(extendedUser);
        extendedUser.setIdType(this);
        return this;
    }

    public IdType removeExtendedUser(ExtendedUser extendedUser) {
        this.extendedUsers.remove(extendedUser);
        extendedUser.setIdType(null);
        return this;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public IdType country(Country country) {
        this.country = country;
        return this;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof IdType)) {
            return false;
        }
        return id != null && id.equals(((IdType) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "IdType{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", code='" + getCode() + "'" +
            ", naturalOrLegal='" + getNaturalOrLegal() + "'" +
            "}";
    }
}
