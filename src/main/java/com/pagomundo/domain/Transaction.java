package com.pagomundo.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.pagomundo.domain.enumeration.ProcessType;
import com.pagomundo.domain.enumeration.Status;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * A Transaction.
 */
@Entity
@Table(name = "transaction")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "transaction")
public class Transaction implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "date_created")
    private ZonedDateTime dateCreated;

    @Column(name = "last_updated")
    private ZonedDateTime lastUpdated;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    @Lob
    @Column(name = "status_reason")
    private String statusReason;

    @Column(name = "amount_before_commission", precision = 21, scale = 2)
    private BigDecimal amountBeforeCommission;

    @Column(name = "bank_commission")
    private Float bankCommission;

    @Column(name = "fx_commission")
    private Float fxCommission;

    @Column(name = "recharge_cost")
    private Float rechargeCost;

    @Column(name = "amount_after_commission", precision = 21, scale = 2)
    private BigDecimal amountAfterCommission;

    @Column(name = "exchange_rate")
    private Float exchangeRate;

    @Column(name = "amount_local_currency", precision = 21, scale = 2)
    private BigDecimal amountLocalCurrency;

    @Column(name = "bank_reference")
    private String bankReference;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "description")
    private String description;

    @Lob
    @Column(name = "notes")
    private String notes;

    @Column(name = "must_notify")
    private Boolean mustNotify;

    @Lob
    @Column(name = "info_changed")
    private String infoChanged;

    @Column(name = "reseller_commission", precision = 21, scale = 2)
    private BigDecimal resellerCommission;

    @Column(name = "reseller_fixed_commission")
    private Float resellerFixedCommission;

    @Column(name = "reseller_percentage_commission")
    private Float resellerPercentageCommission;

    @Enumerated(EnumType.STRING)
    @Column(name = "process_type")
    private ProcessType processType;
    
    @Column(name = "must_notify_payee")
    private Boolean mustNotifyPayee;

    @OneToMany(mappedBy = "transaction", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<TransactionStatusChange> transactionStatusChanges = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "transactions", allowSetters = true)
    private Currency currency;

    @ManyToOne
    @JsonIgnoreProperties(allowSetters = true,
        value = {
            "transactionMerchants",
            "lastName1",
            "lastName2",
            "firstName1",
            "firstName2",
            "gender",
            "maritalStatus",
            "dateCreated",
            "imageIdUrl",
            "imageAddressUrl",
            "balance",
            "statusReason",
            "cardNumber",
            "bankAccountNumber",
            "useDirectPayment",
            "bankAccountType",
            "canChangePaymentMethod",
            "mustNotify",
            "profileInfoChanged",
            "userRelations",
            "extendedUserFunds",
            "notificationSenders",
            "notificationReceivers",
            "notificationResponsibles",
            "notificationStatusChanges",
            "transactionMerchants",
            "transactionPayees",
            "transactionAdmins",
            "idType",
            "extendedUserGroup",
            "bank",
            "directPaymentBank",
            "directPaymentCity",
            "idTypeTaxId",
            "mustNotifyPayee",
        })
    private ExtendedUser merchant;

    @ManyToOne
    @JsonIgnoreProperties(allowSetters = true,
        value = {
            "transactionPayees",
            "lastName1",
            "lastName2",
            "firstName1",
            "firstName2",
            "gender",
            "maritalStatus",
            "dateCreated",
            "imageIdUrl",
            "imageAddressUrl",
            "balance",
            "statusReason",
            "cardNumber",
            "bankCommission",
            "fxCommission",
            "rechargeCost",
            "useMerchantCommission",
            "bankAccountNumber",
            "bankAccountType",
            "canChangePaymentMethod",
            "mustNotify",
            "profileInfoChanged",
            "userRelations",
            "extendedUserFunds",
            "notificationSenders",
            "notificationReceivers",
            "notificationResponsibles",
            "notificationStatusChanges",
            "transactionMerchants",
            "transactionPayees",
            "transactionAdmins",
            "transactionStatusChanges",
            "idType",
            "extendedUserGroup",
            "bank",
            "directPaymentBank",
            "directPaymentCity",
            "reseller",
            "mustNotifyPayee"
        })
    private ExtendedUser payee;

    @ManyToOne
    @JsonIgnoreProperties(allowSetters = true,
        value = {
            "transactionResellers",
            "lastName1",
            "lastName2",
            "firstName1",
            "firstName2",
            "gender",
            "maritalStatus",
            "dateCreated",
            "imageIdUrl",
            "imageAddressUrl",
            "balance",
            "statusReason",
            "cardNumber",
            "bankCommission",
            "fxCommission",
            "rechargeCost",
            "useMerchantCommission",
            "bankAccountNumber",
            "useDirectPayment",
            "bankAccountType",
            "canChangePaymentMethod",
            "mustNotify",
            "profileInfoChanged",
            "userRelations",
            "extendedUserFunds",
            "notificationSenders",
            "notificationReceivers",
            "notificationResponsibles",
            "notificationStatusChanges",
            "transactionMerchants",
            "transactionPayees",
            "transactionAdmins",
            "transactionStatusChanges",
            "idType",
            "extendedUserGroup",
            "bank",
            "directPaymentBank",
            "directPaymentCity",
            "idTypeTaxId",
            "reseller",
            "mustNotifyPayee"
        })
    private ExtendedUser reseller;

    @ManyToOne
    @JsonIgnoreProperties(allowSetters = true,
        value = {
            "transactionAdmins",
            "lastName1",
            "lastName2",
            "firstName1",
            "firstName2",
            "gender",
            "maritalStatus",
            "dateCreated",
            "imageIdUrl",
            "imageAddressUrl",
            "balance",
            "statusReason",
            "cardNumber",
            "bankCommission",
            "fxCommission",
            "rechargeCost",
            "useMerchantCommission",
            "bankAccountNumber",
            "useDirectPayment",
            "bankAccountType",
            "canChangePaymentMethod",
            "mustNotify",
            "profileInfoChanged",
            "userRelations",
            "extendedUserFunds",
            "notificationSenders",
            "notificationReceivers",
            "notificationResponsibles",
            "notificationStatusChanges",
            "transactionMerchants",
            "transactionPayees",
            "transactionAdmins",
            "transactionStatusChanges",
            "idType",
            "extendedUserGroup",
            "bank",
            "directPaymentBank",
            "directPaymentCity",
            "idTypeTaxId",
            "reseller",
            "mustNotifyPayee"
        })
    private ExtendedUser admin;

    @ManyToOne
    @JsonIgnoreProperties(value = "transactions", allowSetters = true)
    private BankAccount bankAccount;

    @ManyToOne
    @JsonIgnoreProperties(value = "transactions", allowSetters = true)
    private TransactionGroup transactionGroup;

    @ManyToOne
    @JsonIgnoreProperties(allowSetters = true,
        value = {
            "transactions",
            "status",
            "statusReason",
            "dateRelated",
            "dateUnrelated",
            "userRelated",
            "extendedUser",
            "relatedBy",
            "unrelatedBy"
        })

    private ExtendedUserRelation extendedUserRelation;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Transaction dateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
        return this;
    }

    public ZonedDateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(ZonedDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Transaction lastUpdated(ZonedDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
        return this;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Transaction status(Status status) {
        this.status = status;
        return this;
    }

    public String getStatusReason() {
        return statusReason;
    }

    public void setStatusReason(String statusReason) {
        this.statusReason = statusReason;
    }

    public Transaction statusReason(String statusReason) {
        this.statusReason = statusReason;
        return this;
    }

    public BigDecimal getAmountBeforeCommission() {
        return amountBeforeCommission;
    }

    public void setAmountBeforeCommission(BigDecimal amountBeforeCommission) {
        this.amountBeforeCommission = amountBeforeCommission;
    }

    public Transaction amountBeforeCommission(BigDecimal amountBeforeCommission) {
        this.amountBeforeCommission = amountBeforeCommission;
        return this;
    }

    public Float getBankCommission() {
        return bankCommission;
    }

    public void setBankCommission(Float bankCommission) {
        this.bankCommission = bankCommission;
    }

    public Transaction bankCommission(Float bankCommission) {
        this.bankCommission = bankCommission;
        return this;
    }

    public Float getFxCommission() {
        return fxCommission;
    }

    public void setFxCommission(Float fxCommission) {
        this.fxCommission = fxCommission;
    }

    public Transaction fxCommission(Float fxCommission) {
        this.fxCommission = fxCommission;
        return this;
    }

    public Float getRechargeCost() {
        return rechargeCost;
    }

    public void setRechargeCost(Float rechargeCost) {
        this.rechargeCost = rechargeCost;
    }

    public Transaction rechargeCost(Float rechargeCost) {
        this.rechargeCost = rechargeCost;
        return this;
    }

    public BigDecimal getAmountAfterCommission() {
        return amountAfterCommission;
    }

    public void setAmountAfterCommission(BigDecimal amountAfterCommission) {
        this.amountAfterCommission = amountAfterCommission;
    }

    public Transaction amountAfterCommission(BigDecimal amountAfterCommission) {
        this.amountAfterCommission = amountAfterCommission;
        return this;
    }

    public Float getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(Float exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public Transaction exchangeRate(Float exchangeRate) {
        this.exchangeRate = exchangeRate;
        return this;
    }

    public BigDecimal getAmountLocalCurrency() {
        return amountLocalCurrency;
    }

    public void setAmountLocalCurrency(BigDecimal amountLocalCurrency) {
        this.amountLocalCurrency = amountLocalCurrency;
    }

    public Transaction amountLocalCurrency(BigDecimal amountLocalCurrency) {
        this.amountLocalCurrency = amountLocalCurrency;
        return this;
    }

    public String getBankReference() {
        return bankReference;
    }

    public void setBankReference(String bankReference) {
        this.bankReference = bankReference;
    }

    public Transaction bankReference(String bankReference) {
        this.bankReference = bankReference;
        return this;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Transaction fileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Transaction description(String description) {
        this.description = description;
        return this;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Transaction notes(String notes) {
        this.notes = notes;
        return this;
    }

    public Boolean isMustNotify() {
        return mustNotify;
    }

    public Transaction mustNotify(Boolean mustNotify) {
        this.mustNotify = mustNotify;
        return this;
    }

    public void setMustNotify(Boolean mustNotify) {
        this.mustNotify = mustNotify;
    }

    public String getInfoChanged() {
        return infoChanged;
    }

    public void setInfoChanged(String infoChanged) {
        this.infoChanged = infoChanged;
    }

    public Transaction infoChanged(String infoChanged) {
        this.infoChanged = infoChanged;
        return this;
    }

    public BigDecimal getResellerCommission() {
        return resellerCommission;
    }

    public void setResellerCommission(BigDecimal resellerCommission) {
        this.resellerCommission = resellerCommission;
    }

    public Transaction resellerCommission(BigDecimal resellerCommission) {
        this.resellerCommission = resellerCommission;
        return this;
    }

    public Float getResellerFixedCommission() {
        return resellerFixedCommission;
    }

    public void setResellerFixedCommission(Float resellerFixedCommission) {
        this.resellerFixedCommission = resellerFixedCommission;
    }

    public Transaction resellerFixedCommission(Float resellerFixedCommission) {
        this.resellerFixedCommission = resellerFixedCommission;
        return this;
    }

    public Float getResellerPercentageCommission() {
        return resellerPercentageCommission;
    }

    public void setResellerPercentageCommission(Float resellerPercentageCommission) {
        this.resellerPercentageCommission = resellerPercentageCommission;
    }

    public Transaction resellerPercentageCommission(Float resellerPercentageCommission) {
        this.resellerPercentageCommission = resellerPercentageCommission;
        return this;
    }

    public ProcessType getProcessType() {
        return processType;
    }

    public void setProcessType(ProcessType processType) {
        this.processType = processType;
    }

    public Transaction processType(ProcessType processType) {
        this.processType = processType;
        return this;
    }

    public Set<TransactionStatusChange> getTransactionStatusChanges() {
        return transactionStatusChanges;
    }

    public void setTransactionStatusChanges(Set<TransactionStatusChange> transactionStatusChanges) {
        this.transactionStatusChanges = transactionStatusChanges;
    }

    public Transaction transactionStatusChanges(Set<TransactionStatusChange> transactionStatusChanges) {
        this.transactionStatusChanges = transactionStatusChanges;
        return this;
    }

    public Transaction addTransactionStatusChange(TransactionStatusChange transactionStatusChange) {
        this.transactionStatusChanges.add(transactionStatusChange);
        transactionStatusChange.setTransaction(this);
        return this;
    }

    public Transaction removeTransactionStatusChange(TransactionStatusChange transactionStatusChange) {
        this.transactionStatusChanges.remove(transactionStatusChange);
        transactionStatusChange.setTransaction(null);
        return this;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Transaction currency(Currency currency) {
        this.currency = currency;
        return this;
    }

    public ExtendedUser getMerchant() {
        return merchant;
    }

    public void setMerchant(ExtendedUser extendedUser) {
        this.merchant = extendedUser;
    }

    public Transaction merchant(ExtendedUser extendedUser) {
        this.merchant = extendedUser;
        return this;
    }

    public ExtendedUser getPayee() {
        return payee;
    }

    public void setPayee(ExtendedUser extendedUser) {
        this.payee = extendedUser;
    }

    public Transaction payee(ExtendedUser extendedUser) {
        this.payee = extendedUser;
        return this;
    }

    public ExtendedUser getAdmin() {
        return admin;
    }

    public void setAdmin(ExtendedUser extendedUser) {
        this.admin = extendedUser;
    }

    public Transaction admin(ExtendedUser extendedUser) {
        this.admin = extendedUser;
        return this;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public Transaction bankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
        return this;
    }

    public TransactionGroup getTransactionGroup() {
        return transactionGroup;
    }

    public void setTransactionGroup(TransactionGroup transactionGroup) {
        this.transactionGroup = transactionGroup;
    }

    public Transaction transactionGroup(TransactionGroup transactionGroup) {
        this.transactionGroup = transactionGroup;
        return this;
    }

    public ExtendedUser getReseller() {
        return reseller;
    }

    public void setReseller(ExtendedUser extendedUser) {
        this.reseller = extendedUser;
    }

    public Transaction reseller(ExtendedUser extendedUser) {
        this.reseller = extendedUser;
        return this;
    }

    public ExtendedUserRelation getExtendedUserRelation() {
        return extendedUserRelation;
    }

    public void setExtendedUserRelation(ExtendedUserRelation extendedUserRelation) {
        this.extendedUserRelation = extendedUserRelation;
    }

    public Transaction extendedUserRelation(ExtendedUserRelation extendedUserRelation) {
        this.extendedUserRelation = extendedUserRelation;
        return this;
    }

    public Boolean isMustNotifyPayee() {
        return mustNotifyPayee;
    }

    public void setMustNotifyPayee(Boolean mustNotifyPayee) {
        this.mustNotifyPayee = mustNotifyPayee;
    }
    
    public Transaction mustNotifyPayee(Boolean mustNotifyPayee) {
        this.mustNotifyPayee = mustNotifyPayee;
        return this;
    }
    
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Transaction)) {
            return false;
        }
        return id != null && id.equals(((Transaction) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Transaction{" +
            "id=" + getId() +
            ", dateCreated='" + getDateCreated() + "'" +
            ", lastUpdated='" + getLastUpdated() + "'" +
            ", status='" + getStatus() + "'" +
            ", statusReason='" + getStatusReason() + "'" +
            ", amountBeforeCommission=" + getAmountBeforeCommission() +
            ", bankCommission=" + getBankCommission() +
            ", fxCommission=" + getFxCommission() +
            ", rechargeCost=" + getRechargeCost() +
            ", amountAfterCommission=" + getAmountAfterCommission() +
            ", exchangeRate=" + getExchangeRate() +
            ", amountLocalCurrency=" + getAmountLocalCurrency() +
            ", bankReference='" + getBankReference() + "'" +
            ", fileName='" + getFileName() + "'" +
            ", description='" + getDescription() + "'" +
            ", notes='" + getNotes() + "'" +
            ", mustNotify='" + isMustNotify() + "'" +
            ", infoChanged='" + getInfoChanged() + "'" +
            ", resellerCommission=" + getResellerCommission() +
            ", resellerFixedCommission=" + getResellerFixedCommission() +
            ", resellerPercentageCommission=" + getResellerPercentageCommission() +
            ", processType='" + getProcessType() + "'" +
            ", mustNotifyPayee='" + isMustNotifyPayee() + "'" +
            "}";
    }
}
