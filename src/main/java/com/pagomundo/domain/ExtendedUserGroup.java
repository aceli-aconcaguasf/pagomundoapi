package com.pagomundo.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.pagomundo.domain.enumeration.Status;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * A ExtendedUserGroup.
 */
@Entity
@Table(name = "extended_user_group")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "extendedusergroup")
public class ExtendedUserGroup implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "date_created")
    private ZonedDateTime dateCreated;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "file_name_from_bank")
    private String fileNameFromBank;

    @OneToMany(mappedBy = "extendedUserGroup", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JsonIgnoreProperties(value = "extendedUserGroup", allowSetters = true)
    private Set<ExtendedUser> extendedUsers = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(allowSetters = true,
        value = {
            "status",
            "lastName1",
            "lastName2",
            "firstName1",
            "firstName2",
            "idNumber",
            "taxId",
            "gender",
            "maritalStatus",
            "residenceAddress",
            "postalAddress",
            "phoneNumber",
            "mobileNumber",
            "birthDate",
            "dateCreated",
            "lastUpdated",
            "email",
            "company",
            "imageIdUrl",
            "imageAddressUrl",
            "balance",
            "statusReason",
            "role",
            "cardNumber",
            "bankCommission",
            "fxCommission",
            "rechargeCost",
            "useMerchantCommission",
            "bankAccountNumber",
            "useDirectPayment",
            "bankAccountType",
            "canChangePaymentMethod",
            "mustNotify",
            "profileInfoChanged",
            "user",
            "userRelations",
            "extendedUserFunds",
            "notificationSenders",
            "notificationReceivers",
            "notificationResponsibles",
            "notificationStatusChanges",
            "transactionMerchants",
            "transactionPayees",
            "transactionAdmins",
            "transactionStatusChanges",
            "residenceCity",
            "postalCity",
            "idType",
            "extendedUserGroup",
            "bank",
            "residenceCountry",
            "postalCountry",
            "directPaymentBank",
            "directPaymentCity"
        })
    private ExtendedUser admin;

    @ManyToOne
    @JsonIgnoreProperties(value = "extendedUserGroups", allowSetters = true)
    private Bank bank;

    public Set<ExtendedUser> getExtendedUsers() {
        return extendedUsers;
    }

    public void setExtendedUsers(Set<ExtendedUser> extendedUsers) {
        this.extendedUsers = extendedUsers;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public ExtendedUserGroup dateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
        return this;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public ExtendedUserGroup status(Status status) {
        this.status = status;
        return this;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public ExtendedUserGroup fileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public String getFileNameFromBank() {
        return fileNameFromBank;
    }

    public void setFileNameFromBank(String fileNameFromBank) {
        this.fileNameFromBank = fileNameFromBank;
    }

    public ExtendedUserGroup fileNameFromBank(String fileNameFromBank) {
        this.fileNameFromBank = fileNameFromBank;
        return this;
    }

    public ExtendedUser getAdmin() {
        return admin;
    }

    public void setAdmin(ExtendedUser extendedUser) {
        this.admin = extendedUser;
    }

    public ExtendedUserGroup admin(ExtendedUser extendedUser) {
        this.admin = extendedUser;
        return this;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public ExtendedUserGroup bank(Bank bank) {
        this.bank = bank;
        return this;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ExtendedUserGroup)) {
            return false;
        }
        return id != null && id.equals(((ExtendedUserGroup) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ExtendedUserGroup{" +
            "id=" + getId() +
            ", dateCreated='" + getDateCreated() + "'" +
            ", status='" + getStatus() + "'" +
            ", fileName='" + getFileName() + "'" +
            ", fileNameFromBank='" + getFileNameFromBank() + "'" +
            "}";
    }
}
