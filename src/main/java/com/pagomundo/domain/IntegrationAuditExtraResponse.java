package com.pagomundo.domain;

import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A IntegrationAuditExtraResponse.
 */
@Entity
@Table(name = "integration_audit_extra")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "integrationauditextraresponse")
public class IntegrationAuditExtraResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "message_date")
    private ZonedDateTime messageDate;

    @Lob
    @Column(name = "message")
    private String message;

    @ManyToOne
    private IntegrationAudit integrationAudit;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getMessageDate() {
        return messageDate;
    }

    public void setMessageDate(ZonedDateTime messageDate) {
        this.messageDate = messageDate;
    }

    public IntegrationAuditExtraResponse messageDate(ZonedDateTime messageDate) {
        this.messageDate = messageDate;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public IntegrationAuditExtraResponse message(String message) {
        this.message = message;
        return this;
    }

    public IntegrationAudit getIntegrationAudit() {
        return integrationAudit;
    }

    public void setIntegrationAudit(IntegrationAudit integrationAudit) {
        this.integrationAudit = integrationAudit;
    }

    public IntegrationAuditExtraResponse integrationAudit(IntegrationAudit integrationAudit) {
        this.integrationAudit = integrationAudit;
        return this;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof IntegrationAuditExtraResponse)) {
            return false;
        }
        return id != null && id.equals(((IntegrationAuditExtraResponse) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "IntegrationAuditExtraResponse{" +
            "id=" + getId() +
            ", messageDate='" + getMessageDate() + "'" +
            ", message='" + getMessage() + "'" +
            "}";
    }
}
