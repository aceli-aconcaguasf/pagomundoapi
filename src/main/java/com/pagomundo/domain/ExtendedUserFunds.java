package com.pagomundo.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.pagomundo.domain.enumeration.FundCategory;
import com.pagomundo.domain.enumeration.FundType;
import com.pagomundo.domain.enumeration.Status;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

/**
 * A ExtendedUserFunds.
 */
@Entity
@Table(name = "extended_user_funds")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "extendeduserfunds")
public class ExtendedUserFunds implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "date_created")
    private ZonedDateTime dateCreated;

    @Column(name = "value", precision = 21, scale = 2)
    private BigDecimal value;

    @Column(name = "balance_before", precision = 21, scale = 2)
    private BigDecimal balanceBefore;

    @Column(name = "balance_after", precision = 21, scale = 2)
    private BigDecimal balanceAfter;

    @Column(name = "reason")
    private String reason;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    @Enumerated(EnumType.STRING)
    @Column(name = "category")
    private FundCategory category;

    @Column(name = "last_updated")
    private ZonedDateTime lastUpdated;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private FundType type;

    @ManyToOne
    @JsonIgnoreProperties(value = "extendedUserFunds", allowSetters = true)
    private ExtendedUser extendedUser;

    @ManyToOne
    @JsonIgnoreProperties(value = "extendedUserFunds", allowSetters = true)
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public ExtendedUserFunds dateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
        return this;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public ExtendedUserFunds value(BigDecimal value) {
        this.value = value;
        return this;
    }

    public BigDecimal getBalanceBefore() {
        return balanceBefore;
    }

    public void setBalanceBefore(BigDecimal balanceBefore) {
        this.balanceBefore = balanceBefore;
    }

    public ExtendedUserFunds balanceBefore(BigDecimal balanceBefore) {
        this.balanceBefore = balanceBefore;
        return this;
    }

    public BigDecimal getBalanceAfter() {
        return balanceAfter;
    }

    public void setBalanceAfter(BigDecimal balanceAfter) {
        this.balanceAfter = balanceAfter;
    }

    public ExtendedUserFunds balanceAfter(BigDecimal balanceAfter) {
        this.balanceAfter = balanceAfter;
        return this;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public ExtendedUserFunds reason(String reason) {
        this.reason = reason;
        return this;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public ExtendedUserFunds status(Status status) {
        this.status = status;
        return this;
    }

    public FundCategory getCategory() {
        return category;
    }

    public void setCategory(FundCategory category) {
        this.category = category;
    }

    public ExtendedUserFunds category(FundCategory category) {
        this.category = category;
        return this;
    }

    public ZonedDateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(ZonedDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public ExtendedUserFunds lastUpdated(ZonedDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
        return this;
    }

    public FundType getType() {
        return type;
    }

    public void setType(FundType type) {
        this.type = type;
    }

    public ExtendedUserFunds type(FundType type) {
        this.type = type;
        return this;
    }

    public ExtendedUser getExtendedUser() {
        return extendedUser;
    }

    public void setExtendedUser(ExtendedUser extendedUser) {
        this.extendedUser = extendedUser;
    }

    public ExtendedUserFunds extendedUser(ExtendedUser extendedUser) {
        this.extendedUser = extendedUser;
        return this;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ExtendedUserFunds user(User user) {
        this.user = user;
        return this;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ExtendedUserFunds)) {
            return false;
        }
        return id != null && id.equals(((ExtendedUserFunds) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ExtendedUserFunds{" +
            "id=" + getId() +
            ", dateCreated='" + getDateCreated() + "'" +
            ", value=" + getValue() +
            ", balanceBefore=" + getBalanceBefore() +
            ", balanceAfter=" + getBalanceAfter() +
            ", reason='" + getReason() + "'" +
            ", status='" + getStatus() + "'" +
            ", category='" + getCategory() + "'" +
            ", lastUpdated='" + getLastUpdated() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
