package com.pagomundo.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A City.
 */
@Entity
@Table(name = "city")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "city")
public class City implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "code")
    private String code;

    @OneToMany(mappedBy = "residenceCity")
    private Set<ExtendedUser> extendedUsersForResidenceCities = new HashSet<>();

    @OneToMany(mappedBy = "postalCity")
    private Set<ExtendedUser> extendedUsersForPostalCities = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "cities", allowSetters = true)
    private Country country;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public City name(String name) {
        this.name = name;
        return this;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public City code(String code) {
        this.code = code;
        return this;
    }

    public Set<ExtendedUser> getExtendedUsersForResidenceCities() {
        return extendedUsersForResidenceCities;
    }

    public void setExtendedUsersForResidenceCities(Set<ExtendedUser> extendedUsers) {
        this.extendedUsersForResidenceCities = extendedUsers;
    }

    public City extendedUsersForResidenceCities(Set<ExtendedUser> extendedUsers) {
        this.extendedUsersForResidenceCities = extendedUsers;
        return this;
    }

    public City addExtendedUsersForResidenceCities(ExtendedUser extendedUser) {
        this.extendedUsersForResidenceCities.add(extendedUser);
        extendedUser.setResidenceCity(this);
        return this;
    }

    public City removeExtendedUsersForResidenceCities(ExtendedUser extendedUser) {
        this.extendedUsersForResidenceCities.remove(extendedUser);
        extendedUser.setResidenceCity(null);
        return this;
    }

    public Set<ExtendedUser> getExtendedUsersForPostalCities() {
        return extendedUsersForPostalCities;
    }

    public void setExtendedUsersForPostalCities(Set<ExtendedUser> extendedUsers) {
        this.extendedUsersForPostalCities = extendedUsers;
    }

    public City extendedUsersForPostalCities(Set<ExtendedUser> extendedUsers) {
        this.extendedUsersForPostalCities = extendedUsers;
        return this;
    }

    public City addExtendedUsersForPostalCities(ExtendedUser extendedUser) {
        this.extendedUsersForPostalCities.add(extendedUser);
        extendedUser.setPostalCity(this);
        return this;
    }

    public City removeExtendedUsersForPostalCities(ExtendedUser extendedUser) {
        this.extendedUsersForPostalCities.remove(extendedUser);
        extendedUser.setPostalCity(null);
        return this;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public City country(Country country) {
        this.country = country;
        return this;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof City)) {
            return false;
        }
        return id != null && id.equals(((City) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "City{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", code='" + getCode() + "'" +
            "}";
    }
}
