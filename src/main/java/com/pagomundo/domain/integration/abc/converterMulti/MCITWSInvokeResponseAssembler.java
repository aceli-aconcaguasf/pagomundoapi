package com.pagomundo.domain.integration.abc.converterMulti;

import com.pagomundo.domain.DTO.abc.*;
import com.pagomundo.domain.DTO.abc.multiServiceResponse.Field;
import com.pagomundo.domain.DTO.abc.multiServiceResponse.Row;
import com.pagomundo.domain.DTO.abc.multiServiceResponse.T24result;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public class MCITWSInvokeResponseAssembler {
    public static NeighborhoodResponseDTO mCITWSInvokeResponse2NeighborhoodResponseDTO(String mCITWSInvokeResponse) throws JAXBException {
        NeighborhoodResponseDTO result = new NeighborhoodResponseDTO();
        StringReader stringReader = new StringReader(mCITWSInvokeResponse);
        JAXBContext contextObj = JAXBContext.newInstance(T24result.class);
        Unmarshaller unmarshaller = contextObj.createUnmarshaller();
        T24result t24result = (T24result) unmarshaller.unmarshal(stringReader);

        if (t24result.head!=null) {
            result.setResultId(t24result.head.resultId);
            result.setMessageId(t24result.head.messageId);
            result.setMessageId(t24result.head.error);
        }

        List<NeighborhoodDTO> neighborhoodDTOs = new ArrayList<NeighborhoodDTO>();

        if (t24result!=null && t24result.body!=null && t24result.body.table != null && t24result.body.table.rows !=null) {
	        for (Row row: t24result.body.table.rows){
                NeighborhoodDTO element = new NeighborhoodDTO();
	            for(Field field : row.fields){
	                if (field.name.equalsIgnoreCase("RESPUESTA")){
	                    element.setResponse(field.value);
	                } else if (field.name.equalsIgnoreCase("ID.COLONIA")){
	                    element.setNeighborhoodId(field.value);
	                } else if (field.name.equalsIgnoreCase("ID.ESTADO")){
	                    element.setStateId(field.value);
	                } else if (field.name.equalsIgnoreCase("ID.CIUDAD")){
	                    element.setCityId(field.value);
	                } else if (field.name.equalsIgnoreCase("ID.MUNICIPIO")){
	                    element.setMunicipalityId(field.value);
	                } else if (field.name.equalsIgnoreCase("ID.PAIS")){
	                    element.setCountryId(field.value);
	                } else if (field.name.equalsIgnoreCase("ERROR")){
	                    element.setError(field.value);
	                }
	            }
                neighborhoodDTOs.add(element);
	        }
            result.setNeighborhoods(neighborhoodDTOs);
        }
        return result;
    }

    public static NeighborhoodResponseDTO mCITWSInvokeResponse2NeighborhoodResponseDTO(MCITWSInvokeResponse mCITWSInvokeResponse) throws JAXBException {
        NeighborhoodResponseDTO result = new NeighborhoodResponseDTO();
        StringReader stringReader = new StringReader(mCITWSInvokeResponse.getMCITWSInvokeResult().getValue());
        JAXBContext contextObj = JAXBContext.newInstance(T24result.class);
        Unmarshaller unmarshaller = contextObj.createUnmarshaller();
        T24result t24result = (T24result) unmarshaller.unmarshal(stringReader);

        if (t24result.head!=null) {
            result.setResultId(t24result.head.resultId);
            result.setMessageId(t24result.head.messageId);
            result.setMessageId(t24result.head.error);
        }

        List<NeighborhoodDTO> neighborhoodDTOs = new ArrayList<NeighborhoodDTO>();

        if (t24result!=null && t24result.body!=null && t24result.body.table != null && t24result.body.table.rows !=null) {
            for (Row row: t24result.body.table.rows){
                NeighborhoodDTO element = new NeighborhoodDTO();
                for(Field field : row.fields){
                    if (field.name.equalsIgnoreCase("RESPUESTA")){
                        element.setResponse(field.value);
                    } else if (field.name.equalsIgnoreCase("ID.COLONIA")){
                        element.setNeighborhoodId(field.value);
                    } else if (field.name.equalsIgnoreCase("ID.ESTADO")){
                        element.setStateId(field.value);
                    } else if (field.name.equalsIgnoreCase("ID.CIUDAD")){
                        element.setCityId(field.value);
                    } else if (field.name.equalsIgnoreCase("ID.MUNICIPIO")){
                        element.setMunicipalityId(field.value);
                    } else if (field.name.equalsIgnoreCase("ID.PAIS")){
                        element.setCountryId(field.value);
                    } else if (field.name.equalsIgnoreCase("ERROR")){
                        element.setError(field.value);
                    }
                }
                neighborhoodDTOs.add(element);
            }
            result.setNeighborhoods(neighborhoodDTOs);
        }
        return result;
    }

    public static BalanceResponseDTO mCITWSInvokeResponse2BalanceResponseDTO(String mCITWSInvokeResponse) throws JAXBException {
        BalanceResponseDTO result = new BalanceResponseDTO();
        StringReader stringReader = new StringReader(mCITWSInvokeResponse);
        JAXBContext contextObj = JAXBContext.newInstance(T24result.class);
        Unmarshaller unmarshaller = contextObj.createUnmarshaller();
        T24result t24result = (T24result) unmarshaller.unmarshal(stringReader);

        if (t24result.head!=null) {
            result.setResultId(t24result.head.resultId);
            result.setMessageId(t24result.head.messageId);
            result.setMessageId(t24result.head.error);
        }



        if (t24result!=null && t24result.body!=null && t24result.body.table != null && t24result.body.table.rows !=null) {

            for (Row row: t24result.body.table.rows){

                for(Field field : row.fields){
                    if (field.name.equalsIgnoreCase("ACCOUNT.NO")){
                        result.setAccountNumber(field.value);
                    } else if (field.name.equalsIgnoreCase("CAT.DES")){
                        result.setAccountType(field.value);
                    } else if (field.name.equalsIgnoreCase("SAL.DIS")){
                        result.setAvailableBalance(field.value);
                    } else if (field.name.equalsIgnoreCase("SAL.BLO")){
                        result.setPendingBalance(field.value);
                    } else if (field.name.equalsIgnoreCase("SAL.CUENTA")){
                        result.setBalance(field.value);
                    } else if (field.name.equalsIgnoreCase("CTA.CLABE")){
                        result.setClabe(field.value);
                    }
                }
            }


        }

        return result;
    }


    public static BalanceResponseDTO mCITWSInvokeResponse2BalanceResponseDTO(MCITWSInvokeResponse mCITWSInvokeResponse) throws JAXBException {
        BalanceResponseDTO result = new BalanceResponseDTO();
        StringReader stringReader = new StringReader(mCITWSInvokeResponse.getMCITWSInvokeResult().getValue());
        JAXBContext contextObj = JAXBContext.newInstance(T24result.class);
        Unmarshaller unmarshaller = contextObj.createUnmarshaller();
        T24result t24result = (T24result) unmarshaller.unmarshal(stringReader);

        if (t24result.head!=null) {
            result.setResultId(t24result.head.resultId);
            result.setMessageId(t24result.head.messageId);
            result.setMessageId(t24result.head.error);
        }



        if (t24result!=null && t24result.body!=null && t24result.body.table != null && t24result.body.table.rows !=null) {

            for (Row row: t24result.body.table.rows){

                for(Field field : row.fields){
                    if (field.name.equalsIgnoreCase("ACCOUNT.NO")){
                        result.setAccountNumber(field.value);
                    } else if (field.name.equalsIgnoreCase("CAT.DES")){
                        result.setAccountType(field.value);
                    } else if (field.name.equalsIgnoreCase("SAL.DIS")){
                        result.setAvailableBalance(field.value);
                    } else if (field.name.equalsIgnoreCase("SAL.BLO")){
                        result.setPendingBalance(field.value);
                    } else if (field.name.equalsIgnoreCase("SAL.CUENTA")){
                        result.setBalance(field.value);
                    } else if (field.name.equalsIgnoreCase("CTA.CLABE")){
                        result.setClabe(field.value);
                    }
                }
            }


        }

        return result;
    }

    public static AccountMovementsResponseDTO mCITWSInvokeResponse2AccountMovementsResponseDTO(String mCITWSInvokeResponse) throws JAXBException {
        AccountMovementsResponseDTO result = new AccountMovementsResponseDTO();
        StringReader stringReader = new StringReader(mCITWSInvokeResponse);
        JAXBContext contextObj = JAXBContext.newInstance(T24result.class);
        Unmarshaller unmarshaller = contextObj.createUnmarshaller();
        T24result t24result = (T24result) unmarshaller.unmarshal(stringReader);

        if (t24result.head!=null) {
            result.setResultId(t24result.head.resultId);
            result.setMessageId(t24result.head.messageId);
            result.setMessageId(t24result.head.error);
        }

        List<AccountMovementDTO> accountMovementDTOs = new ArrayList<AccountMovementDTO>();

        if (t24result!=null && t24result.body!=null && t24result.body.table != null && t24result.body.table.rows !=null) {

	        for (Row row: t24result.body.table.rows){
                AccountMovementDTO element = new AccountMovementDTO();
	            for(Field field : row.fields){
	                if (field.name.equalsIgnoreCase("VALDESC")){
	                    element.setOperationDate(field.value);
	                } else if (field.name.equalsIgnoreCase("PDESC")){
	                    element.setMovementDescription(field.value);
	                } else if (field.name.equalsIgnoreCase("REFNO")){
	                    element.setReference(field.value);
	                } else if (field.name.equalsIgnoreCase("POST")){
	                    element.setAccountingDate(field.value);
	                } else if (field.name.equalsIgnoreCase("DESC.ID")){
	                    element.setMovementNarrative(field.value);
	                } else if (field.name.equalsIgnoreCase("NARRATIVE")){
	                    element.setOperationTimeStamp(field.value);
	                } else if (field.name.equalsIgnoreCase("PAMTDEBIT")){
	                    element.setDebitAmount(field.value);
	                } else if (field.name.equalsIgnoreCase("PAMTCREDIT")){
	                    element.setCreditAmount(field.value);
	                } else if (field.name.equalsIgnoreCase("RUNNING.BALANCE")){
	                    element.setRunningBalance(field.value);
	                }
	            }
                accountMovementDTOs.add(element);
	        }
	        result.setAccountMovements(accountMovementDTOs);
        }
        return result;
    }

    public static AccountMovementsResponseDTO mCITWSInvokeResponse2AccountMovementsResponseDTO(MCITWSInvokeResponse mCITWSInvokeResponse) throws JAXBException {
        AccountMovementsResponseDTO result = new AccountMovementsResponseDTO();
        StringReader stringReader = new StringReader(mCITWSInvokeResponse.getMCITWSInvokeResult().getValue());
        JAXBContext contextObj = JAXBContext.newInstance(T24result.class);
        Unmarshaller unmarshaller = contextObj.createUnmarshaller();
        T24result t24result = (T24result) unmarshaller.unmarshal(stringReader);

        if (t24result.head!=null) {
            result.setResultId(t24result.head.resultId);
            result.setMessageId(t24result.head.messageId);
            result.setMessageId(t24result.head.error);
        }

        List<AccountMovementDTO> accountMovementDTOs = new ArrayList<AccountMovementDTO>();

        if (t24result!=null && t24result.body!=null && t24result.body.table != null && t24result.body.table.rows !=null) {

            for (Row row: t24result.body.table.rows){
                AccountMovementDTO element = new AccountMovementDTO();
                for(Field field : row.fields){
                    if (field.name.equalsIgnoreCase("VALDESC")){
                        element.setOperationDate(field.value);
                    } else if (field.name.equalsIgnoreCase("PDESC")){
                        element.setMovementDescription(field.value);
                    } else if (field.name.equalsIgnoreCase("REFNO")){
                        element.setReference(field.value);
                    } else if (field.name.equalsIgnoreCase("POST")){
                        element.setAccountingDate(field.value);
                    } else if (field.name.equalsIgnoreCase("DESC.ID")){
                        element.setMovementNarrative(field.value);
                    } else if (field.name.equalsIgnoreCase("NARRATIVE")){
                        element.setOperationTimeStamp(field.value);
                    } else if (field.name.equalsIgnoreCase("PAMTDEBIT")){
                        element.setDebitAmount(field.value);
                    } else if (field.name.equalsIgnoreCase("PAMTCREDIT")){
                        element.setCreditAmount(field.value);
                    } else if (field.name.equalsIgnoreCase("RUNNING.BALANCE")){
                        element.setRunningBalance(field.value);
                    }
                }
                accountMovementDTOs.add(element);
            }
            result.setAccountMovements(accountMovementDTOs);
        }
        return result;
    }

    public static AccountPendingMovementsResponseDTO mCITWSInvokeResponse2AccountPendingMovementsResponseDTO (String mCITWSInvokeResponse) throws JAXBException {
        AccountPendingMovementsResponseDTO result = new AccountPendingMovementsResponseDTO();
        StringReader stringReader = new StringReader(mCITWSInvokeResponse);
        JAXBContext contextObj = JAXBContext.newInstance(T24result.class);
        Unmarshaller unmarshaller = contextObj.createUnmarshaller();
        T24result t24result = (T24result) unmarshaller.unmarshal(stringReader);

        if (t24result.head!=null) {
        	System.out.println("<"+t24result.head.resultId+"><"+t24result.head.messageId+"><"+t24result.head.error+">");
        }

        List<AccountPendingMovementDTO> accountPendingMovementDTOs = new ArrayList<AccountPendingMovementDTO>();

        if (t24result!=null && t24result.body!=null && t24result.body.table != null && t24result.body.table.rows !=null) {

	        for (Row row: t24result.body.table.rows){
                AccountPendingMovementDTO element = new AccountPendingMovementDTO();
	            for(Field field : row.fields){
	                if (field.name.equalsIgnoreCase("CUENTA.CARGO")){
	                    element.setDebitAccount(field.value);
	                } else if (field.name.equalsIgnoreCase("REFERENCIA")){
	                    element.setReference(field.value);
	                } else if (field.name.equalsIgnoreCase("DESCRIPCION")){
	                    element.setDescription(field.value);
	                } else if (field.name.equalsIgnoreCase("FECHA.BLOQUEO")){
	                    element.setLockDate(field.value);
	                } else if (field.name.equalsIgnoreCase("MONTO.BLOQUEADO")){
	                    element.setLockAmount(field.value);
	                }
	            }
                accountPendingMovementDTOs.add(element);
	        }
	        result.setAccountPendingMovements(accountPendingMovementDTOs);
        }
        return result;
    }


    public static AccountPendingMovementsResponseDTO mCITWSInvokeResponse2AccountPendingMovementsResponseDTO (MCITWSInvokeResponse mCITWSInvokeResponse) throws JAXBException {
        AccountPendingMovementsResponseDTO result = new AccountPendingMovementsResponseDTO();
        StringReader stringReader = new StringReader(mCITWSInvokeResponse.getMCITWSInvokeResult().getValue());
        JAXBContext contextObj = JAXBContext.newInstance(T24result.class);
        Unmarshaller unmarshaller = contextObj.createUnmarshaller();
        T24result t24result = (T24result) unmarshaller.unmarshal(stringReader);

        if (t24result.head!=null) {
            System.out.println("<"+t24result.head.resultId+"><"+t24result.head.messageId+"><"+t24result.head.error+">");
        }

        List<AccountPendingMovementDTO> accountPendingMovementDTOs = new ArrayList<AccountPendingMovementDTO>();

        if (t24result!=null && t24result.body!=null && t24result.body.table != null && t24result.body.table.rows !=null) {

            for (Row row: t24result.body.table.rows){
                AccountPendingMovementDTO element = new AccountPendingMovementDTO();
                for(Field field : row.fields){
                    if (field.name.equalsIgnoreCase("CUENTA.CARGO")){
                        element.setDebitAccount(field.value);
                    } else if (field.name.equalsIgnoreCase("REFERENCIA")){
                        element.setReference(field.value);
                    } else if (field.name.equalsIgnoreCase("DESCRIPCION")){
                        element.setDescription(field.value);
                    } else if (field.name.equalsIgnoreCase("FECHA.BLOQUEO")){
                        element.setLockDate(field.value);
                    } else if (field.name.equalsIgnoreCase("MONTO.BLOQUEADO")){
                        element.setLockAmount(field.value);
                    }
                }
                accountPendingMovementDTOs.add(element);
            }
            result.setAccountPendingMovements(accountPendingMovementDTOs);
        }
        return result;
    }
}
