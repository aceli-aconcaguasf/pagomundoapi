package com.pagomundo.domain.integration.abc.converterMulti;

import com.pagomundo.domain.DTO.abc.AccountMovementsQueryDTO;
import com.pagomundo.domain.DTO.abc.AccountPendingMovementsQueryDTO;
import com.pagomundo.domain.DTO.abc.BalanceQueryDTO;
import com.pagomundo.domain.DTO.abc.NeighborhoodQueryDTO;

public class MCITWSInvokeAssembler {
    public static String neighborhoodQueryDTO2XMLrequest(NeighborhoodQueryDTO neighborhoodQueryDTO){
        String xml = "<T24query>" +
                     "<head><query>ENQ.ABC.CODI.DIR</query></head>" +
                     "<body>" +
                            (neighborhoodQueryDTO.getZipCode().isEmpty() ?"":
                                "<selectionData><field name=\"CODIGO.POSTAL\" oper=\"EQ\">"+neighborhoodQueryDTO.getZipCode()+"</field></selectionData>") +
                            (neighborhoodQueryDTO.getNeighborhoodName().isEmpty() ?"":
                                "<selectionData><field name=\"COLONIA\" oper=\"EQ\">"+neighborhoodQueryDTO.getNeighborhoodName()+"</field></selectionData>") +
                                "</body>" +
                            "</T24query>";
        return xml;
    }

    public static String balanceQueryDTO2XMLrequest(BalanceQueryDTO balanceQueryDTO){
        String xml = "<T24query>" +
                     "<head><query>ABC.SALDO.CTA.DIG</query></head>" +
                     "<body>" +
                     (balanceQueryDTO.getAccountNumber().isEmpty() ?"":
                         "<selectionData><field name=\"ACCOUNT.NO\" oper=\"EQ\">"+balanceQueryDTO.getAccountNumber()+"</field></selectionData>") +
                     (balanceQueryDTO.getChannel().isEmpty() ?"":
                         "<selectionData><field name=\"CANAL\" oper=\"EQ\">"+balanceQueryDTO.getChannel()+"</field></selectionData>") +
                     "</body>" +
                     "</T24query>";
        return xml;
    }

    public static String accountMovementsQueryDTO2XMLrequest(AccountMovementsQueryDTO accountMovementsQueryDTO){
        String xml = "<T24query>" +
                     "<head><query>ABC.MOV.CTAS</query></head>" +
                     "<body>" +
                     (accountMovementsQueryDTO.getBookingDate().isEmpty() ?"":
                        "<selectionData><field name=\"BOOKING.DATE\" oper=\"RG\">"+accountMovementsQueryDTO.getBookingDate()+"</field></selectionData>") +
                     (accountMovementsQueryDTO.getAccountNumber().isEmpty() ?"":
                        "<selectionData><field name=\"ACCOUNT\" oper=\"EQ\">"+accountMovementsQueryDTO.getAccountNumber()+"</field></selectionData>") +
                     (accountMovementsQueryDTO.getChannel().isEmpty() ?"":
                        "<selectionData><field name=\"CANAL\" oper=\"EQ\">"+accountMovementsQueryDTO.getChannel()+"</field></selectionData>") +
            "</body>" +
            "</T24query>";
        return xml;
    }

    public static String accountPendingMovementsQueryDTO2XMLrequest(AccountPendingMovementsQueryDTO accountPendingMovementsQueryDTO){
        String xml = "<T24query>" +
                     "<head><query>ABC.MOV.PEND.CTAS</query></head>" +
                     "<body>" +
                     (accountPendingMovementsQueryDTO.getAccountNumber().isEmpty() ?"":
                        "<selectionData><field name=\"ACCOUNT.NUMBER\" oper=\"EQ\">"+accountPendingMovementsQueryDTO.getAccountNumber()+"</field></selectionData>") +
                     (accountPendingMovementsQueryDTO.getChannel().isEmpty() ?"":
                        "<selectionData><field name=\"CANAL\" oper=\"EQ\">"+accountPendingMovementsQueryDTO.getChannel()+"</field></selectionData>") +
                     "</body>" +
                     "</T24query>";
        return xml;
    }
}
