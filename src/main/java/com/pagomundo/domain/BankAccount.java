package com.pagomundo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A BankAccount.
 */
@Entity
@Table(name = "bank_account")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "bankaccount")
public class BankAccount implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "account_number")
    private String accountNumber;

    @Column(name = "bank_commission")
    private Float bankCommission;

    @Column(name = "fx_commission")
    private Float fxCommission;

    @Column(name = "recharge_cost")
    private Float rechargeCost;
    
    @Column(name = "is_remitee")
    private Boolean isRemitee;

    @OneToMany(mappedBy = "bankAccount")
    @JsonIgnore
    private Set<Transaction> transactions = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "bankAccounts", allowSetters = true)
    private Bank bank;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public BankAccount accountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
        return this;
    }

    public Float getBankCommission() {
        return bankCommission;
    }

    public void setBankCommission(Float bankCommission) {
        this.bankCommission = bankCommission;
    }

    public BankAccount bankCommission(Float bankCommission) {
        this.bankCommission = bankCommission;
        return this;
    }

    public Float getFxCommission() {
        return fxCommission;
    }

    public void setFxCommission(Float fxCommission) {
        this.fxCommission = fxCommission;
    }

    public BankAccount fxCommission(Float fxCommission) {
        this.fxCommission = fxCommission;
        return this;
    }

    public Float getRechargeCost() {
        return rechargeCost;
    }

    public void setRechargeCost(Float rechargeCost) {
        this.rechargeCost = rechargeCost;
    }

    public BankAccount rechargeCost(Float rechargeCost) {
        this.rechargeCost = rechargeCost;
        return this;
    }

    public Set<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(Set<Transaction> transactions) {
        this.transactions = transactions;
    }

    public BankAccount transactions(Set<Transaction> transactions) {
        this.transactions = transactions;
        return this;
    }

    public BankAccount addTransaction(Transaction transaction) {
        this.transactions.add(transaction);
        transaction.setBankAccount(this);
        return this;
    }

    public BankAccount removeTransaction(Transaction transaction) {
        this.transactions.remove(transaction);
        transaction.setBankAccount(null);
        return this;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public BankAccount bank(Bank bank) {
        this.bank = bank;
        return this;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BankAccount)) {
            return false;
        }
        return id != null && id.equals(((BankAccount) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
            "id=" + getId() +
            ", accountNumber='" + getAccountNumber() + "'" +
            ", bankCommission=" + getBankCommission() +
            ", fxCommission=" + getFxCommission() +
            ", rechargeCost=" + getRechargeCost() +
            "}";
    }

    public Boolean getIsRemitee() {
        return isRemitee;
    }

    public void setIsRemitee(Boolean isRemitee) {
        this.isRemitee = isRemitee;
    }
    
    public BankAccount isRemitee(boolean isRemitee) {
        this.isRemitee = isRemitee;
        return this;
    }
    
    
}
