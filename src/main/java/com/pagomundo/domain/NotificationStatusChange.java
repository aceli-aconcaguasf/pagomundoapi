package com.pagomundo.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.pagomundo.domain.enumeration.Status;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A NotificationStatusChange.
 */
@Entity
@Table(name = "notification_status_change")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "notificationstatuschange")
public class NotificationStatusChange implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Column(name = "date_created", nullable = false)
    private ZonedDateTime dateCreated;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private Status status;

    @Lob
    @Column(name = "reason")
    private String reason;

    @ManyToOne
    @JsonIgnoreProperties(allowSetters = true,
        value = {
            "subject",
            "description",
            "dateCreated",
            "lastUpdated",
            "status",
            "statusReason",
            "category",
            "notes",
            "subCategory",
            "receiverGroup",
            "entityId",
            "sender",
            "responsible",
            "extendedUser",
            "notificationStatusChanges"
        }
    )
    private Notification notification;

    @ManyToOne
    @JsonIgnoreProperties(allowSetters = true,
        value = {
            "status",
            "lastName1",
            "lastName2",
            "firstName1",
            "firstName2",
            "idNumber",
            "taxId",
            "gender",
            "maritalStatus",
            "residenceAddress",
            "postalAddress",
            "phoneNumber",
            "mobileNumber",
            "birthDate",
            "dateCreated",
            "lastUpdated",
            "company",
            "imageIdUrl",
            "imageAddressUrl",
            "balance",
            "statusReason",
            "cardNumber",
            "bankCommission",
            "fxCommission",
            "rechargeCost",
            "useMerchantCommission",
            "bankAccountNumber",
            "useDirectPayment",
            "bankAccountType",
            "canChangePaymentMethod",
            "mustNotify",
            "profileInfoChanged",
            "confirmedProfile",
            "fixedCommission",
            "resellerCommission",
            "user",
            "userRelations",
            "notificationSenders",
            "notificationReceivers",
            "notificationResponsibles",
            "notificationStatusChanges",
            "transactionMerchants",
            "transactionPayees",
            "transactionAdmins",
            "transactionStatusChanges",
            "residenceCity",
            "postalCity",
            "idType",
            "extendedUserGroup",
            "bank",
            "residenceCountry",
            "postalCountry",
            "directPaymentBank",
            "directPaymentCity",
            "idTypeTaxId",
            "reseller",
        })
    private ExtendedUser user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public NotificationStatusChange dateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
        return this;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public NotificationStatusChange status(Status status) {
        this.status = status;
        return this;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public NotificationStatusChange reason(String reason) {
        this.reason = reason;
        return this;
    }

    public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }

    public NotificationStatusChange notification(Notification notification) {
        this.notification = notification;
        return this;
    }

    public ExtendedUser getUser() {
        return user;
    }

    public void setUser(ExtendedUser extendedUser) {
        this.user = extendedUser;
    }

    public NotificationStatusChange user(ExtendedUser extendedUser) {
        this.user = extendedUser;
        return this;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NotificationStatusChange)) {
            return false;
        }
        return id != null && id.equals(((NotificationStatusChange) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "NotificationStatusChange{" +
            "id=" + getId() +
            ", dateCreated='" + getDateCreated() + "'" +
            ", status='" + getStatus() + "'" +
            ", reason='" + getReason() + "'" +
            "}";
    }
}
