package com.pagomundo.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.pagomundo.domain.enumeration.Status;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A ExtendedUserRelation.
 */
@Entity
@Table(name = "extended_user_relation")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "extendeduserrelation")
public class ExtendedUserRelation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    @Column(name = "status_reason")
    private String statusReason;

    @Column(name = "date_related")
    private ZonedDateTime dateRelated;

    @Column(name = "date_unrelated")
    private ZonedDateTime dateUnrelated;

    @Column(name = "nickname")
    private String nickname = "";

    @OneToOne
    @JoinColumn(unique = true)
    @JsonIgnoreProperties(allowSetters = true,
        value = {
            "extendedUserRelations",
            "password",
            "email",
            "activated",
            "langKey",
            "imageUrl",
            "activationKey",
            "resetKey",
            "status",
            "statusReason"
        })
    private User userRelated;

    @ManyToOne
    @JsonIgnoreProperties(allowSetters = true,
        value = {
            "extendedUserRelations",
            "dateCreated",
            "imageIdUrl",
            "imageAddressUrl",
            "balance",
            "bankCommission",
            "fxCommission",
            "rechargeCost",
            "useMerchantCommission",
            "mustNotify",
            "profileInfoChanged",
            "userRelations",
            "extendedUserFunds",
            "notificationSenders",
            "notificationReceivers",
            "notificationResponsibles",
            "notificationStatusChanges",
            "transactionMerchants",
            "transactionPayees",
            "transactionAdmins",
            "transactionStatusChanges",
            "extendedUserGroup",
            "bank",
            "reseller"
        })
    private ExtendedUser extendedUser;

    @ManyToOne
    @JsonIgnoreProperties(allowSetters = true,
        value = {
            "extendedUserRelations",
            "lastName1",
            "lastName2",
            "firstName1",
            "firstName2",
            "idNumber",
            "taxId",
            "gender",
            "maritalStatus",
            "residenceAddress",
            "postalAddress",
            "phoneNumber",
            "mobileNumber",
            "birthDate",
            "dateCreated",
            "email",
            "company",
            "imageIdUrl",
            "imageAddressUrl",
            "balance",
            "cardNumber",
            "bankCommission",
            "fxCommission",
            "rechargeCost",
            "useMerchantCommission",
            "bankAccountNumber",
            "useDirectPayment",
            "bankAccountType",
            "canChangePaymentMethod",
            "mustNotify",
            "profileInfoChanged",
            "userRelations",
            "extendedUserFunds",
            "notificationSenders",
            "notificationReceivers",
            "notificationResponsibles",
            "notificationStatusChanges",
            "transactionMerchants",
            "transactionPayees",
            "transactionAdmins",
            "transactionStatusChanges",
            "residenceCity",
            "postalCity",
            "idType",
            "extendedUserGroup",
            "bank",
            "residenceCountry",
            "postalCountry",
            "directPaymentBank",
            "directPaymentCity",
            "idTypeTaxId",
            "user",
            "lastUpdated",
            "confirmedProfile",
            "resellerCommission",
            "fixedCommission",
            "reseller"
        })
    private ExtendedUser relatedBy;

    @ManyToOne
    @JsonIgnoreProperties(allowSetters = true,
        value = {
            "extendedUserRelations",
            "lastName1",
            "lastName2",
            "firstName1",
            "firstName2",
            "idNumber",
            "taxId",
            "gender",
            "maritalStatus",
            "residenceAddress",
            "postalAddress",
            "phoneNumber",
            "mobileNumber",
            "birthDate",
            "dateCreated",
            "email",
            "company",
            "imageIdUrl",
            "imageAddressUrl",
            "balance",
            "cardNumber",
            "bankCommission",
            "fxCommission",
            "rechargeCost",
            "useMerchantCommission",
            "bankAccountNumber",
            "useDirectPayment",
            "bankAccountType",
            "canChangePaymentMethod",
            "mustNotify",
            "profileInfoChanged",
            "userRelations",
            "extendedUserFunds",
            "notificationSenders",
            "notificationReceivers",
            "notificationResponsibles",
            "notificationStatusChanges",
            "transactionMerchants",
            "transactionPayees",
            "transactionAdmins",
            "transactionStatusChanges",
            "residenceCity",
            "postalCity",
            "idType",
            "extendedUserGroup",
            "bank",
            "residenceCountry",
            "postalCountry",
            "directPaymentBank",
            "directPaymentCity",
            "idTypeTaxId",
            "user",
            "lastUpdated",
            "confirmedProfile",
            "resellerCommission",
            "fixedCommission",
            "reseller"
        })
    private ExtendedUser unrelatedBy;

    public ExtendedUserRelation() {
    }

    public ExtendedUserRelation(User user, ExtendedUser extendedUser, String nickName) {
        this.setExtendedUser(extendedUser);
        this.setUserRelated(user);
        this.setNickname(nickName);
    }

    public ExtendedUserRelation(User user, ExtendedUser extendedUser, Status status, String nickname) {
        this(user, extendedUser, nickname);
        this.setStatus(status);
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public ExtendedUserRelation status(Status status) {
        this.status = status;
        return this;
    }

    public String getStatusReason() {
        return statusReason;
    }

    public void setStatusReason(String statusReason) {
        this.statusReason = statusReason;
    }

    public ExtendedUserRelation statusReason(String statusReason) {
        this.statusReason = statusReason;
        return this;
    }

    public ZonedDateTime getDateRelated() {
        return dateRelated;
    }

    public void setDateRelated(ZonedDateTime dateRelated) {
        this.dateRelated = dateRelated;
    }

    public ExtendedUserRelation dateRelated(ZonedDateTime dateRelated) {
        this.dateRelated = dateRelated;
        return this;
    }

    public ZonedDateTime getDateUnrelated() {
        return dateUnrelated;
    }

    public void setDateUnrelated(ZonedDateTime dateUnrelated) {
        this.dateUnrelated = dateUnrelated;
    }

    public ExtendedUserRelation dateUnrelated(ZonedDateTime dateUnrelated) {
        this.dateUnrelated = dateUnrelated;
        return this;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public ExtendedUserRelation nickname(String nickname) {
        this.nickname = nickname;
        return this;
    }

    public User getUserRelated() {
        return userRelated;
    }

    public void setUserRelated(User user) {
        this.userRelated = user;
    }

    public ExtendedUserRelation userRelated(User user) {
        this.userRelated = user;
        return this;
    }

    public ExtendedUser getExtendedUser() {
        return extendedUser;
    }

    public void setExtendedUser(ExtendedUser extendedUser) {
        this.extendedUser = extendedUser;
    }

    public ExtendedUserRelation extendedUser(ExtendedUser extendedUser) {
        this.extendedUser = extendedUser;
        return this;
    }

    public ExtendedUser getRelatedBy() {
        return relatedBy;
    }

    public void setRelatedBy(ExtendedUser extendedUser) {
        this.relatedBy = extendedUser;
    }

    public ExtendedUserRelation relatedBy(ExtendedUser extendedUser) {
        this.relatedBy = extendedUser;
        return this;
    }

    public ExtendedUser getUnrelatedBy() {
        return unrelatedBy;
    }

    public void setUnrelatedBy(ExtendedUser extendedUser) {
        this.unrelatedBy = extendedUser;
    }

    public ExtendedUserRelation unrelatedBy(ExtendedUser extendedUser) {
        this.unrelatedBy = extendedUser;
        return this;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ExtendedUserRelation)) {
            return false;
        }
        return id != null && id.equals(((ExtendedUserRelation) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ExtendedUserRelation{" +
            "id=" + getId() +
            ", status='" + getStatus() + "'" +
            ", statusReason='" + getStatusReason() + "'" +
            ", dateRelated='" + getDateRelated() + "'" +
            ", dateUnrelated='" + getDateUnrelated() + "'" +
            ", nickname='" + getNickname() + "'" +
            "}";
    }
}
