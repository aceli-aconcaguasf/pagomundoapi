package com.pagomundo.config;

import com.pagomundo.service.integrations.abc.FintechService;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class SoapFintechAbcConfig {
  private final String serviceUrl = "https://desavdigital.abccapital.com.mx/FintechSolution/FintechService.svc";
  @Bean
  public Jaxb2Marshaller fintechmarshaller() {
    Jaxb2Marshaller fintechmarshaller = new Jaxb2Marshaller();
    // this package must match the package in the <generatePackage> specified in
    // pom.xml
    fintechmarshaller.setContextPath("com.pagomundo.domain.integration.abc.fintech");
    return fintechmarshaller;
  }

  @Bean
  public FintechService fintechClient(Jaxb2Marshaller fintechmarshaller) {
    FintechService client = new FintechService();
    client.setDefaultUri(serviceUrl);
    client.setMarshaller(fintechmarshaller);
    client.setUnmarshaller(fintechmarshaller);
    return client;
  }

}