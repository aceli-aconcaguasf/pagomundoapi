package com.pagomundo.config;

import com.pagomundo.service.integrations.abc.DCMService;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class SoapDCMAbcConfig {
  private final String dcmServiceUrl = "https://desavataico.abccapital.com.mx/Service1.svc";
  @Bean
  public Jaxb2Marshaller marshaller() {
    Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
    // this package must match the package in the <generatePackage> specified in
    // pom.xml
    marshaller.setContextPath("com.pagomundo.domain.integration.abc.service1");
    return marshaller;
  }

  @Bean
  public DCMService dcmClient(Jaxb2Marshaller marshaller) {
    DCMService abcClient = new DCMService();
    abcClient.setDefaultUri(dcmServiceUrl);
    abcClient.setMarshaller(marshaller);
    abcClient.setUnmarshaller(marshaller);
    return abcClient;
  }

}