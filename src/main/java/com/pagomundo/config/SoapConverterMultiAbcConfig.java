package com.pagomundo.config;

import com.pagomundo.service.integrations.abc.ConverterMultiService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class SoapConverterMultiAbcConfig {
  private final String serviceUrl = "https://desavdigital.abccapital.com.mx:9098/";
  @Bean
  public Jaxb2Marshaller converterMultiMarshaller() {
    Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
    // this package must match the package in the <generatePackage> specified in
    // pom.xml
      marshaller.setContextPath("com.pagomundo.domain.integration.abc.converterMulti");
    return marshaller;
  }

  @Bean
  public ConverterMultiService converterMultiClient(Jaxb2Marshaller marshaller) {
      ConverterMultiService client = new ConverterMultiService();
      client.setDefaultUri(serviceUrl);
      client.setMarshaller(marshaller);
      client.setUnmarshaller(marshaller);
    return client;
  }

}
