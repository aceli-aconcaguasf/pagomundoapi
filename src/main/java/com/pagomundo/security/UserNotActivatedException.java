package com.pagomundo.security;

import com.pagomundo.service.UserService;
import com.pagomundo.web.rest.errors.BadRequestAlertException;

public class UserNotActivatedException extends BadRequestAlertException {

    private static final long serialVersionUID = 1L;

    public UserNotActivatedException(String defaultMessage) {
        super(defaultMessage, UserService.ENTITY_NAME, UserService.USER_NOT_ACTIVATED_CODE);
    }
}
