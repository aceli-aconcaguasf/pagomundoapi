import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PagoMundoApiSharedModule } from 'app/shared/shared.module';
import { TransactionGroupComponent } from './transaction-group.component';
import { TransactionGroupDetailComponent } from './transaction-group-detail.component';
import { TransactionGroupUpdateComponent } from './transaction-group-update.component';
import { TransactionGroupDeleteDialogComponent, TransactionGroupDeletePopupComponent } from './transaction-group-delete-dialog.component';
import { transactionGroupPopupRoute, transactionGroupRoute } from './transaction-group.route';

const ENTITY_STATES = [...transactionGroupRoute, ...transactionGroupPopupRoute];

@NgModule({
  imports: [PagoMundoApiSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    TransactionGroupComponent,
    TransactionGroupDetailComponent,
    TransactionGroupUpdateComponent,
    TransactionGroupDeleteDialogComponent,
    TransactionGroupDeletePopupComponent
  ],
  entryComponents: [TransactionGroupDeleteDialogComponent]
})
export class PagoMundoApiTransactionGroupModule {}
