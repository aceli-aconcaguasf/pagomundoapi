import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ITransactionGroup } from 'app/shared/model/transaction-group.model';

type EntityResponseType = HttpResponse<ITransactionGroup>;
type EntityArrayResponseType = HttpResponse<ITransactionGroup[]>;

@Injectable({ providedIn: 'root' })
export class TransactionGroupService {
  public resourceUrl = SERVER_API_URL + 'api/transaction-groups';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/transaction-groups';

  constructor(protected http: HttpClient) {}

  create(transactionGroup: ITransactionGroup): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(transactionGroup);
    return this.http
      .post<ITransactionGroup>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(transactionGroup: ITransactionGroup): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(transactionGroup);
    return this.http
      .put<ITransactionGroup>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ITransactionGroup>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ITransactionGroup[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ITransactionGroup[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(transactionGroup: ITransactionGroup): ITransactionGroup {
    const copy: ITransactionGroup = Object.assign({}, transactionGroup, {
      dateCreated:
        transactionGroup.dateCreated != null && transactionGroup.dateCreated.isValid() ? transactionGroup.dateCreated.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateCreated = res.body.dateCreated != null ? moment(res.body.dateCreated) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((transactionGroup: ITransactionGroup) => {
        transactionGroup.dateCreated = transactionGroup.dateCreated != null ? moment(transactionGroup.dateCreated) : null;
      });
    }
    return res;
  }
}
