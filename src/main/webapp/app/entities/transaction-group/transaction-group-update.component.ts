import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { ITransactionGroup, TransactionGroup } from 'app/shared/model/transaction-group.model';
import { TransactionGroupService } from './transaction-group.service';
import { IBankAccount } from 'app/shared/model/bank-account.model';
import { BankAccountService } from 'app/entities/bank-account/bank-account.service';
import { IExtendedUser } from 'app/shared/model/extended-user.model';
import { ExtendedUserService } from 'app/entities/extended-user/extended-user.service';

@Component({
  selector: 'jhi-transaction-group-update',
  templateUrl: './transaction-group-update.component.html'
})
export class TransactionGroupUpdateComponent implements OnInit {
  isSaving: boolean;

  bankaccounts: IBankAccount[];

  extendedusers: IExtendedUser[];

  editForm = this.fb.group({
    id: [],
    dateCreated: [],
    status: [],
    fileName: [],
    exchangeRate: [],
    fileNameFromBank: [],
    directPayment: [],
    bankAccount: [],
    admin: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected transactionGroupService: TransactionGroupService,
    protected bankAccountService: BankAccountService,
    protected extendedUserService: ExtendedUserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ transactionGroup }) => {
      this.updateForm(transactionGroup);
    });
    this.bankAccountService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IBankAccount[]>) => mayBeOk.ok),
        map((response: HttpResponse<IBankAccount[]>) => response.body)
      )
      .subscribe((res: IBankAccount[]) => (this.bankaccounts = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.extendedUserService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IExtendedUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IExtendedUser[]>) => response.body)
      )
      .subscribe((res: IExtendedUser[]) => (this.extendedusers = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(transactionGroup: ITransactionGroup) {
    this.editForm.patchValue({
      id: transactionGroup.id,
      dateCreated: transactionGroup.dateCreated != null ? transactionGroup.dateCreated.format(DATE_TIME_FORMAT) : null,
      status: transactionGroup.status,
      fileName: transactionGroup.fileName,
      exchangeRate: transactionGroup.exchangeRate,
      fileNameFromBank: transactionGroup.fileNameFromBank,
      directPayment: transactionGroup.directPayment,
      bankAccount: transactionGroup.bankAccount,
      admin: transactionGroup.admin
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const transactionGroup = this.createFromForm();
    if (transactionGroup.id !== undefined) {
      this.subscribeToSaveResponse(this.transactionGroupService.update(transactionGroup));
    } else {
      this.subscribeToSaveResponse(this.transactionGroupService.create(transactionGroup));
    }
  }

  private createFromForm(): ITransactionGroup {
    return {
      ...new TransactionGroup(),
      id: this.editForm.get(['id']).value,
      dateCreated:
        this.editForm.get(['dateCreated']).value != null ? moment(this.editForm.get(['dateCreated']).value, DATE_TIME_FORMAT) : undefined,
      status: this.editForm.get(['status']).value,
      fileName: this.editForm.get(['fileName']).value,
      exchangeRate: this.editForm.get(['exchangeRate']).value,
      fileNameFromBank: this.editForm.get(['fileNameFromBank']).value,
      directPayment: this.editForm.get(['directPayment']).value,
      bankAccount: this.editForm.get(['bankAccount']).value,
      admin: this.editForm.get(['admin']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITransactionGroup>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackBankAccountById(index: number, item: IBankAccount) {
    return item.id;
  }

  trackExtendedUserById(index: number, item: IExtendedUser) {
    return item.id;
  }
}
