import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { BankAccount, IBankAccount } from 'app/shared/model/bank-account.model';
import { BankAccountService } from './bank-account.service';
import { IBank } from 'app/shared/model/bank.model';
import { BankService } from 'app/entities/bank/bank.service';

@Component({
  selector: 'jhi-bank-account-update',
  templateUrl: './bank-account-update.component.html'
})
export class BankAccountUpdateComponent implements OnInit {
  isSaving: boolean;

  banks: IBank[];

  editForm = this.fb.group({
    id: [],
    accountNumber: [],
    bankCommission: [],
    fxCommission: [],
    rechargeCost: [],
    bank: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected bankAccountService: BankAccountService,
    protected bankService: BankService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ bankAccount }) => {
      this.updateForm(bankAccount);
    });
    this.bankService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IBank[]>) => mayBeOk.ok),
        map((response: HttpResponse<IBank[]>) => response.body)
      )
      .subscribe((res: IBank[]) => (this.banks = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(bankAccount: IBankAccount) {
    this.editForm.patchValue({
      id: bankAccount.id,
      accountNumber: bankAccount.accountNumber,
      bankCommission: bankAccount.bankCommission,
      fxCommission: bankAccount.fxCommission,
      rechargeCost: bankAccount.rechargeCost,
      bank: bankAccount.bank
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const bankAccount = this.createFromForm();
    if (bankAccount.id !== undefined) {
      this.subscribeToSaveResponse(this.bankAccountService.update(bankAccount));
    } else {
      this.subscribeToSaveResponse(this.bankAccountService.create(bankAccount));
    }
  }

  trackBankById(index: number, item: IBank) {
    return item.id;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBankAccount>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  private createFromForm(): IBankAccount {
    return {
      ...new BankAccount(),
      id: this.editForm.get(['id']).value,
      accountNumber: this.editForm.get(['accountNumber']).value,
      bankCommission: this.editForm.get(['bankCommission']).value,
      fxCommission: this.editForm.get(['fxCommission']).value,
      rechargeCost: this.editForm.get(['rechargeCost']).value,
      bank: this.editForm.get(['bank']).value
    };
  }
}
