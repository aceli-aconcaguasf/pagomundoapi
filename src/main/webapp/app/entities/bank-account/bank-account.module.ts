import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PagoMundoApiSharedModule } from 'app/shared/shared.module';
import { BankAccountComponent } from './bank-account.component';
import { BankAccountDetailComponent } from './bank-account-detail.component';
import { BankAccountUpdateComponent } from './bank-account-update.component';
import { BankAccountDeleteDialogComponent, BankAccountDeletePopupComponent } from './bank-account-delete-dialog.component';
import { bankAccountPopupRoute, bankAccountRoute } from './bank-account.route';

const ENTITY_STATES = [...bankAccountRoute, ...bankAccountPopupRoute];

@NgModule({
  imports: [PagoMundoApiSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    BankAccountComponent,
    BankAccountDetailComponent,
    BankAccountUpdateComponent,
    BankAccountDeleteDialogComponent,
    BankAccountDeletePopupComponent
  ],
  entryComponents: [BankAccountDeleteDialogComponent]
})
export class PagoMundoApiBankAccountModule {}
