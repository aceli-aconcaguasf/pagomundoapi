import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PagoMundoApiSharedModule } from 'app/shared/shared.module';
import { IdTypeComponent } from './id-type.component';
import { IdTypeDetailComponent } from './id-type-detail.component';
import { IdTypeUpdateComponent } from './id-type-update.component';
import { IdTypeDeleteDialogComponent, IdTypeDeletePopupComponent } from './id-type-delete-dialog.component';
import { idTypePopupRoute, idTypeRoute } from './id-type.route';

const ENTITY_STATES = [...idTypeRoute, ...idTypePopupRoute];

@NgModule({
  imports: [PagoMundoApiSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [IdTypeComponent, IdTypeDetailComponent, IdTypeUpdateComponent, IdTypeDeleteDialogComponent, IdTypeDeletePopupComponent],
  entryComponents: [IdTypeDeleteDialogComponent]
})
export class PagoMundoApiIdTypeModule {}
