import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { ITransaction, Transaction } from 'app/shared/model/transaction.model';
import { TransactionService } from './transaction.service';
import { ICurrency } from 'app/shared/model/currency.model';
import { CurrencyService } from 'app/entities/currency/currency.service';
import { IExtendedUser } from 'app/shared/model/extended-user.model';
import { ExtendedUserService } from 'app/entities/extended-user/extended-user.service';
import { IBankAccount } from 'app/shared/model/bank-account.model';
import { BankAccountService } from 'app/entities/bank-account/bank-account.service';
import { ITransactionGroup } from 'app/shared/model/transaction-group.model';
import { TransactionGroupService } from 'app/entities/transaction-group/transaction-group.service';
import { IExtendedUserRelation } from 'app/shared/model/extended-user-relation.model';
import { ExtendedUserRelationService } from 'app/entities/extended-user-relation/extended-user-relation.service';

@Component({
  selector: 'jhi-transaction-update',
  templateUrl: './transaction-update.component.html'
})
export class TransactionUpdateComponent implements OnInit {
  isSaving: boolean;

  currencies: ICurrency[];

  extendedusers: IExtendedUser[];

  bankaccounts: IBankAccount[];

  transactiongroups: ITransactionGroup[];

  extendeduserrelations: IExtendedUserRelation[];

  editForm = this.fb.group({
    id: [],
    dateCreated: [],
    lastUpdated: [],
    status: [],
    statusReason: [],
    amountBeforeCommission: [],
    bankCommission: [],
    fxCommission: [],
    rechargeCost: [],
    amountAfterCommission: [],
    exchangeRate: [],
    amountLocalCurrency: [],
    bankReference: [],
    fileName: [],
    description: [],
    notes: [],
    mustNotify: [],
    infoChanged: [],
    resellerCommission: [],
    resellerFixedCommission: [],
    resellerPercentageCommission: [],
    processType: [],
    currency: [],
    merchant: [],
    payee: [],
    admin: [],
    bankAccount: [],
    transactionGroup: [],
    reseller: [],
    extendedUserRelation: []
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected jhiAlertService: JhiAlertService,
    protected transactionService: TransactionService,
    protected currencyService: CurrencyService,
    protected extendedUserService: ExtendedUserService,
    protected bankAccountService: BankAccountService,
    protected transactionGroupService: TransactionGroupService,
    protected extendedUserRelationService: ExtendedUserRelationService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ transaction }) => {
      this.updateForm(transaction);
    });
    this.currencyService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ICurrency[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICurrency[]>) => response.body)
      )
      .subscribe((res: ICurrency[]) => (this.currencies = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.extendedUserService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IExtendedUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IExtendedUser[]>) => response.body)
      )
      .subscribe((res: IExtendedUser[]) => (this.extendedusers = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.bankAccountService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IBankAccount[]>) => mayBeOk.ok),
        map((response: HttpResponse<IBankAccount[]>) => response.body)
      )
      .subscribe((res: IBankAccount[]) => (this.bankaccounts = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.transactionGroupService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ITransactionGroup[]>) => mayBeOk.ok),
        map((response: HttpResponse<ITransactionGroup[]>) => response.body)
      )
      .subscribe((res: ITransactionGroup[]) => (this.transactiongroups = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.extendedUserRelationService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IExtendedUserRelation[]>) => mayBeOk.ok),
        map((response: HttpResponse<IExtendedUserRelation[]>) => response.body)
      )
      .subscribe(
        (res: IExtendedUserRelation[]) => (this.extendeduserrelations = res),
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  updateForm(transaction: ITransaction) {
    this.editForm.patchValue({
      id: transaction.id,
      dateCreated: transaction.dateCreated != null ? transaction.dateCreated.format(DATE_TIME_FORMAT) : null,
      lastUpdated: transaction.lastUpdated != null ? transaction.lastUpdated.format(DATE_TIME_FORMAT) : null,
      status: transaction.status,
      statusReason: transaction.statusReason,
      amountBeforeCommission: transaction.amountBeforeCommission,
      bankCommission: transaction.bankCommission,
      fxCommission: transaction.fxCommission,
      rechargeCost: transaction.rechargeCost,
      amountAfterCommission: transaction.amountAfterCommission,
      exchangeRate: transaction.exchangeRate,
      amountLocalCurrency: transaction.amountLocalCurrency,
      bankReference: transaction.bankReference,
      fileName: transaction.fileName,
      description: transaction.description,
      notes: transaction.notes,
      mustNotify: transaction.mustNotify,
      infoChanged: transaction.infoChanged,
      resellerCommission: transaction.resellerCommission,
      resellerFixedCommission: transaction.resellerFixedCommission,
      resellerPercentageCommission: transaction.resellerPercentageCommission,
      processType: transaction.processType,
      currency: transaction.currency,
      merchant: transaction.merchant,
      payee: transaction.payee,
      admin: transaction.admin,
      bankAccount: transaction.bankAccount,
      transactionGroup: transaction.transactionGroup,
      reseller: transaction.reseller,
      extendedUserRelation: transaction.extendedUserRelation
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }

  setFileData(event, field: string, isImage) {
    return new Promise((resolve, reject) => {
      if (event && event.target && event.target.files && event.target.files[0]) {
        const file: File = event.target.files[0];
        if (isImage && !file.type.startsWith('image/')) {
          reject(`File was expected to be an image but was found to be ${file.type}`);
        } else {
          const filedContentType: string = field + 'ContentType';
          this.dataUtils.toBase64(file, base64Data => {
            this.editForm.patchValue({
              [field]: base64Data,
              [filedContentType]: file.type
            });
          });
        }
      } else {
        reject(`Base64 data was not set as file could not be extracted from passed parameter: ${event}`);
      }
    }).then(
      // eslint-disable-next-line no-console
      () => console.log('blob added'), // success
      this.onError
    );
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const transaction = this.createFromForm();
    if (transaction.id !== undefined) {
      this.subscribeToSaveResponse(this.transactionService.update(transaction));
    } else {
      this.subscribeToSaveResponse(this.transactionService.create(transaction));
    }
  }

  trackExtendedUserRelationById(index: number, item: IExtendedUserRelation) {
    return item.id;
  }

  trackCurrencyById(index: number, item: ICurrency) {
    return item.id;
  }

  trackExtendedUserById(index: number, item: IExtendedUser) {
    return item.id;
  }

  trackBankAccountById(index: number, item: IBankAccount) {
    return item.id;
  }

  trackTransactionGroupById(index: number, item: ITransactionGroup) {
    return item.id;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITransaction>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  private createFromForm(): ITransaction {
    return {
      ...new Transaction(),
      id: this.editForm.get(['id']).value,
      dateCreated:
        this.editForm.get(['dateCreated']).value != null ? moment(this.editForm.get(['dateCreated']).value, DATE_TIME_FORMAT) : undefined,
      lastUpdated:
        this.editForm.get(['lastUpdated']).value != null ? moment(this.editForm.get(['lastUpdated']).value, DATE_TIME_FORMAT) : undefined,
      status: this.editForm.get(['status']).value,
      statusReason: this.editForm.get(['statusReason']).value,
      amountBeforeCommission: this.editForm.get(['amountBeforeCommission']).value,
      bankCommission: this.editForm.get(['bankCommission']).value,
      fxCommission: this.editForm.get(['fxCommission']).value,
      rechargeCost: this.editForm.get(['rechargeCost']).value,
      amountAfterCommission: this.editForm.get(['amountAfterCommission']).value,
      exchangeRate: this.editForm.get(['exchangeRate']).value,
      amountLocalCurrency: this.editForm.get(['amountLocalCurrency']).value,
      bankReference: this.editForm.get(['bankReference']).value,
      fileName: this.editForm.get(['fileName']).value,
      description: this.editForm.get(['description']).value,
      notes: this.editForm.get(['notes']).value,
      mustNotify: this.editForm.get(['mustNotify']).value,
      infoChanged: this.editForm.get(['infoChanged']).value,
      resellerCommission: this.editForm.get(['resellerCommission']).value,
      resellerFixedCommission: this.editForm.get(['resellerFixedCommission']).value,
      resellerPercentageCommission: this.editForm.get(['resellerPercentageCommission']).value,
      processType: this.editForm.get(['processType']).value,
      currency: this.editForm.get(['currency']).value,
      merchant: this.editForm.get(['merchant']).value,
      payee: this.editForm.get(['payee']).value,
      admin: this.editForm.get(['admin']).value,
      bankAccount: this.editForm.get(['bankAccount']).value,
      transactionGroup: this.editForm.get(['transactionGroup']).value,
      reseller: this.editForm.get(['reseller']).value,
      extendedUserRelation: this.editForm.get(['extendedUserRelation']).value
    };
  }
}
