import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { INotification, Notification } from 'app/shared/model/notification.model';
import { NotificationService } from './notification.service';
import { IExtendedUser } from 'app/shared/model/extended-user.model';
import { ExtendedUserService } from 'app/entities/extended-user/extended-user.service';

@Component({
  selector: 'jhi-notification-update',
  templateUrl: './notification-update.component.html'
})
export class NotificationUpdateComponent implements OnInit {
  isSaving: boolean;

  extendedusers: IExtendedUser[];

  editForm = this.fb.group({
    id: [],
    subject: [],
    description: [],
    dateCreated: [],
    lastUpdated: [],
    status: [],
    statusReason: [],
    category: [],
    notes: [],
    subCategory: [],
    receiverGroup: [],
    entityId: [],
    sender: [],
    responsible: [],
    extendedUser: []
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected jhiAlertService: JhiAlertService,
    protected notificationService: NotificationService,
    protected extendedUserService: ExtendedUserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ notification }) => {
      this.updateForm(notification);
    });
    this.extendedUserService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IExtendedUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IExtendedUser[]>) => response.body)
      )
      .subscribe((res: IExtendedUser[]) => (this.extendedusers = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(notification: INotification) {
    this.editForm.patchValue({
      id: notification.id,
      subject: notification.subject,
      description: notification.description,
      dateCreated: notification.dateCreated != null ? notification.dateCreated.format(DATE_TIME_FORMAT) : null,
      lastUpdated: notification.lastUpdated != null ? notification.lastUpdated.format(DATE_TIME_FORMAT) : null,
      status: notification.status,
      statusReason: notification.statusReason,
      category: notification.category,
      notes: notification.notes,
      subCategory: notification.subCategory,
      receiverGroup: notification.receiverGroup,
      entityId: notification.entityId,
      sender: notification.sender,
      responsible: notification.responsible,
      extendedUser: notification.extendedUser
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }

  setFileData(event, field: string, isImage) {
    return new Promise((resolve, reject) => {
      if (event && event.target && event.target.files && event.target.files[0]) {
        const file: File = event.target.files[0];
        if (isImage && !file.type.startsWith('image/')) {
          reject(`File was expected to be an image but was found to be ${file.type}`);
        } else {
          const filedContentType: string = field + 'ContentType';
          this.dataUtils.toBase64(file, base64Data => {
            this.editForm.patchValue({
              [field]: base64Data,
              [filedContentType]: file.type
            });
          });
        }
      } else {
        reject(`Base64 data was not set as file could not be extracted from passed parameter: ${event}`);
      }
    }).then(
      // eslint-disable-next-line no-console
      () => console.log('blob added'), // success
      this.onError
    );
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const notification = this.createFromForm();
    if (notification.id !== undefined) {
      this.subscribeToSaveResponse(this.notificationService.update(notification));
    } else {
      this.subscribeToSaveResponse(this.notificationService.create(notification));
    }
  }

  private createFromForm(): INotification {
    return {
      ...new Notification(),
      id: this.editForm.get(['id']).value,
      subject: this.editForm.get(['subject']).value,
      description: this.editForm.get(['description']).value,
      dateCreated:
        this.editForm.get(['dateCreated']).value != null ? moment(this.editForm.get(['dateCreated']).value, DATE_TIME_FORMAT) : undefined,
      lastUpdated:
        this.editForm.get(['lastUpdated']).value != null ? moment(this.editForm.get(['lastUpdated']).value, DATE_TIME_FORMAT) : undefined,
      status: this.editForm.get(['status']).value,
      statusReason: this.editForm.get(['statusReason']).value,
      category: this.editForm.get(['category']).value,
      notes: this.editForm.get(['notes']).value,
      subCategory: this.editForm.get(['subCategory']).value,
      receiverGroup: this.editForm.get(['receiverGroup']).value,
      entityId: this.editForm.get(['entityId']).value,
      sender: this.editForm.get(['sender']).value,
      responsible: this.editForm.get(['responsible']).value,
      extendedUser: this.editForm.get(['extendedUser']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<INotification>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackExtendedUserById(index: number, item: IExtendedUser) {
    return item.id;
  }
}
