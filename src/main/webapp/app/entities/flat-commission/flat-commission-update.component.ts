import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IFlatCommission, FlatCommission } from 'app/shared/model/flat-commission.model';
import { FlatCommissionService } from './flat-commission.service';
import { IExtendedUser } from 'app/shared/model/extended-user.model';
import { ExtendedUserService } from 'app/entities/extended-user/extended-user.service';

@Component({
  selector: 'jhi-flat-commission-update',
  templateUrl: './flat-commission-update.component.html'
})
export class FlatCommissionUpdateComponent implements OnInit {
  isSaving: boolean;

  extendedusers: IExtendedUser[];

  editForm = this.fb.group({
    id: [],
    upTo: [],
    commission: [],
    description: [],
    dateCreated: [],
    lastUpdated: [],
    extendedUser: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected flatCommissionService: FlatCommissionService,
    protected extendedUserService: ExtendedUserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ flatCommission }) => {
      this.updateForm(flatCommission);
    });
    this.extendedUserService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IExtendedUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IExtendedUser[]>) => response.body)
      )
      .subscribe((res: IExtendedUser[]) => (this.extendedusers = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(flatCommission: IFlatCommission) {
    this.editForm.patchValue({
      id: flatCommission.id,
      upTo: flatCommission.upTo,
      commission: flatCommission.commission,
      description: flatCommission.description,
      dateCreated: flatCommission.dateCreated != null ? flatCommission.dateCreated.format(DATE_TIME_FORMAT) : null,
      lastUpdated: flatCommission.lastUpdated != null ? flatCommission.lastUpdated.format(DATE_TIME_FORMAT) : null,
      extendedUser: flatCommission.extendedUser
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const flatCommission = this.createFromForm();
    if (flatCommission.id !== undefined) {
      this.subscribeToSaveResponse(this.flatCommissionService.update(flatCommission));
    } else {
      this.subscribeToSaveResponse(this.flatCommissionService.create(flatCommission));
    }
  }

  private createFromForm(): IFlatCommission {
    return {
      ...new FlatCommission(),
      id: this.editForm.get(['id']).value,
      upTo: this.editForm.get(['upTo']).value,
      commission: this.editForm.get(['commission']).value,
      description: this.editForm.get(['description']).value,
      dateCreated:
        this.editForm.get(['dateCreated']).value != null ? moment(this.editForm.get(['dateCreated']).value, DATE_TIME_FORMAT) : undefined,
      lastUpdated:
        this.editForm.get(['lastUpdated']).value != null ? moment(this.editForm.get(['lastUpdated']).value, DATE_TIME_FORMAT) : undefined,
      extendedUser: this.editForm.get(['extendedUser']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFlatCommission>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackExtendedUserById(index: number, item: IExtendedUser) {
    return item.id;
  }
}
