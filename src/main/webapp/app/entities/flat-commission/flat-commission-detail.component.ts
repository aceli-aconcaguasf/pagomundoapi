import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IFlatCommission } from 'app/shared/model/flat-commission.model';

@Component({
  selector: 'jhi-flat-commission-detail',
  templateUrl: './flat-commission-detail.component.html'
})
export class FlatCommissionDetailComponent implements OnInit {
  flatCommission: IFlatCommission;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ flatCommission }) => {
      this.flatCommission = flatCommission;
    });
  }

  previousState() {
    window.history.back();
  }
}
