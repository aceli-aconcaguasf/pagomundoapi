import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IFlatCommission } from 'app/shared/model/flat-commission.model';
import { FlatCommissionService } from './flat-commission.service';

@Component({
  selector: 'jhi-flat-commission-delete-dialog',
  templateUrl: './flat-commission-delete-dialog.component.html'
})
export class FlatCommissionDeleteDialogComponent {
  flatCommission: IFlatCommission;

  constructor(
    protected flatCommissionService: FlatCommissionService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.flatCommissionService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'flatCommissionListModification',
        content: 'Deleted an flatCommission'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-flat-commission-delete-popup',
  template: ''
})
export class FlatCommissionDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ flatCommission }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(FlatCommissionDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.flatCommission = flatCommission;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/flat-commission', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/flat-commission', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
