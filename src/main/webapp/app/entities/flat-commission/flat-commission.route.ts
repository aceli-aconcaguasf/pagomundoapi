import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { FlatCommission } from 'app/shared/model/flat-commission.model';
import { FlatCommissionService } from './flat-commission.service';
import { FlatCommissionComponent } from './flat-commission.component';
import { FlatCommissionDetailComponent } from './flat-commission-detail.component';
import { FlatCommissionUpdateComponent } from './flat-commission-update.component';
import { FlatCommissionDeletePopupComponent } from './flat-commission-delete-dialog.component';
import { IFlatCommission } from 'app/shared/model/flat-commission.model';

@Injectable({ providedIn: 'root' })
export class FlatCommissionResolve implements Resolve<IFlatCommission> {
  constructor(private service: FlatCommissionService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IFlatCommission> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<FlatCommission>) => response.ok),
        map((flatCommission: HttpResponse<FlatCommission>) => flatCommission.body)
      );
    }
    return of(new FlatCommission());
  }
}

export const flatCommissionRoute: Routes = [
  {
    path: '',
    component: FlatCommissionComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'pagoMundoApiApp.flatCommission.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: FlatCommissionDetailComponent,
    resolve: {
      flatCommission: FlatCommissionResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.flatCommission.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: FlatCommissionUpdateComponent,
    resolve: {
      flatCommission: FlatCommissionResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.flatCommission.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: FlatCommissionUpdateComponent,
    resolve: {
      flatCommission: FlatCommissionResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.flatCommission.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const flatCommissionPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: FlatCommissionDeletePopupComponent,
    resolve: {
      flatCommission: FlatCommissionResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.flatCommission.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
