import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PagoMundoApiSharedModule } from 'app/shared/shared.module';
import { FlatCommissionComponent } from './flat-commission.component';
import { FlatCommissionDetailComponent } from './flat-commission-detail.component';
import { FlatCommissionUpdateComponent } from './flat-commission-update.component';
import { FlatCommissionDeletePopupComponent, FlatCommissionDeleteDialogComponent } from './flat-commission-delete-dialog.component';
import { flatCommissionRoute, flatCommissionPopupRoute } from './flat-commission.route';

const ENTITY_STATES = [...flatCommissionRoute, ...flatCommissionPopupRoute];

@NgModule({
  imports: [PagoMundoApiSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    FlatCommissionComponent,
    FlatCommissionDetailComponent,
    FlatCommissionUpdateComponent,
    FlatCommissionDeleteDialogComponent,
    FlatCommissionDeletePopupComponent
  ],
  entryComponents: [FlatCommissionDeleteDialogComponent]
})
export class PagoMundoApiFlatCommissionModule {}
