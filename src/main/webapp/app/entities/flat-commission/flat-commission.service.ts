import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IFlatCommission } from 'app/shared/model/flat-commission.model';

type EntityResponseType = HttpResponse<IFlatCommission>;
type EntityArrayResponseType = HttpResponse<IFlatCommission[]>;

@Injectable({ providedIn: 'root' })
export class FlatCommissionService {
  public resourceUrl = SERVER_API_URL + 'api/flat-commissions';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/flat-commissions';

  constructor(protected http: HttpClient) {}

  create(flatCommission: IFlatCommission): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(flatCommission);
    return this.http
      .post<IFlatCommission>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(flatCommission: IFlatCommission): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(flatCommission);
    return this.http
      .put<IFlatCommission>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IFlatCommission>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IFlatCommission[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IFlatCommission[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(flatCommission: IFlatCommission): IFlatCommission {
    const copy: IFlatCommission = Object.assign({}, flatCommission, {
      dateCreated: flatCommission.dateCreated != null && flatCommission.dateCreated.isValid() ? flatCommission.dateCreated.toJSON() : null,
      lastUpdated: flatCommission.lastUpdated != null && flatCommission.lastUpdated.isValid() ? flatCommission.lastUpdated.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateCreated = res.body.dateCreated != null ? moment(res.body.dateCreated) : null;
      res.body.lastUpdated = res.body.lastUpdated != null ? moment(res.body.lastUpdated) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((flatCommission: IFlatCommission) => {
        flatCommission.dateCreated = flatCommission.dateCreated != null ? moment(flatCommission.dateCreated) : null;
        flatCommission.lastUpdated = flatCommission.lastUpdated != null ? moment(flatCommission.lastUpdated) : null;
      });
    }
    return res;
  }
}
