import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PagoMundoApiSharedModule } from 'app/shared/shared.module';
import { ExtendedUserRelationComponent } from './extended-user-relation.component';
import { ExtendedUserRelationDetailComponent } from './extended-user-relation-detail.component';
import { ExtendedUserRelationUpdateComponent } from './extended-user-relation-update.component';
import {
  ExtendedUserRelationDeleteDialogComponent,
  ExtendedUserRelationDeletePopupComponent
} from './extended-user-relation-delete-dialog.component';
import { extendedUserRelationPopupRoute, extendedUserRelationRoute } from './extended-user-relation.route';

const ENTITY_STATES = [...extendedUserRelationRoute, ...extendedUserRelationPopupRoute];

@NgModule({
  imports: [PagoMundoApiSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    ExtendedUserRelationComponent,
    ExtendedUserRelationDetailComponent,
    ExtendedUserRelationUpdateComponent,
    ExtendedUserRelationDeleteDialogComponent,
    ExtendedUserRelationDeletePopupComponent
  ],
  entryComponents: [ExtendedUserRelationDeleteDialogComponent]
})
export class PagoMundoApiExtendedUserRelationModule {}
