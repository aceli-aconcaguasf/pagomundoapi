import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IExtendedUserRelation } from 'app/shared/model/extended-user-relation.model';

@Component({
  selector: 'jhi-extended-user-relation-detail',
  templateUrl: './extended-user-relation-detail.component.html'
})
export class ExtendedUserRelationDetailComponent implements OnInit {
  extendedUserRelation: IExtendedUserRelation;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ extendedUserRelation }) => {
      this.extendedUserRelation = extendedUserRelation;
    });
  }

  previousState() {
    window.history.back();
  }
}
