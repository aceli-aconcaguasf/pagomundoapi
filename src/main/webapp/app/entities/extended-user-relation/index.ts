export * from './extended-user-relation.service';
export * from './extended-user-relation-update.component';
export * from './extended-user-relation-delete-dialog.component';
export * from './extended-user-relation-detail.component';
export * from './extended-user-relation.component';
export * from './extended-user-relation.route';
