import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { ExtendedUserRelation, IExtendedUserRelation } from 'app/shared/model/extended-user-relation.model';
import { ExtendedUserRelationService } from './extended-user-relation.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { IExtendedUser } from 'app/shared/model/extended-user.model';
import { ExtendedUserService } from 'app/entities/extended-user/extended-user.service';

@Component({
  selector: 'jhi-extended-user-relation-update',
  templateUrl: './extended-user-relation-update.component.html'
})
export class ExtendedUserRelationUpdateComponent implements OnInit {
  isSaving: boolean;

  users: IUser[];

  extendedusers: IExtendedUser[];

  editForm = this.fb.group({
    id: [],
    status: [],
    statusReason: [],
    dateRelated: [],
    dateUnrelated: [],
    nickname: [],
    userRelated: [],
    extendedUser: [],
    relatedBy: [],
    unrelatedBy: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected extendedUserRelationService: ExtendedUserRelationService,
    protected userService: UserService,
    protected extendedUserService: ExtendedUserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ extendedUserRelation }) => {
      this.updateForm(extendedUserRelation);
    });
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.extendedUserService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IExtendedUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IExtendedUser[]>) => response.body)
      )
      .subscribe((res: IExtendedUser[]) => (this.extendedusers = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(extendedUserRelation: IExtendedUserRelation) {
    this.editForm.patchValue({
      id: extendedUserRelation.id,
      status: extendedUserRelation.status,
      statusReason: extendedUserRelation.statusReason,
      dateRelated: extendedUserRelation.dateRelated != null ? extendedUserRelation.dateRelated.format(DATE_TIME_FORMAT) : null,
      dateUnrelated: extendedUserRelation.dateUnrelated != null ? extendedUserRelation.dateUnrelated.format(DATE_TIME_FORMAT) : null,
      nickname: extendedUserRelation.nickname,
      userRelated: extendedUserRelation.userRelated,
      extendedUser: extendedUserRelation.extendedUser,
      relatedBy: extendedUserRelation.relatedBy,
      unrelatedBy: extendedUserRelation.unrelatedBy
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const extendedUserRelation = this.createFromForm();
    if (extendedUserRelation.id !== undefined) {
      this.subscribeToSaveResponse(this.extendedUserRelationService.update(extendedUserRelation));
    } else {
      this.subscribeToSaveResponse(this.extendedUserRelationService.create(extendedUserRelation));
    }
  }

  private createFromForm(): IExtendedUserRelation {
    return {
      ...new ExtendedUserRelation(),
      id: this.editForm.get(['id']).value,
      status: this.editForm.get(['status']).value,
      statusReason: this.editForm.get(['statusReason']).value,
      dateRelated:
        this.editForm.get(['dateRelated']).value != null ? moment(this.editForm.get(['dateRelated']).value, DATE_TIME_FORMAT) : undefined,
      dateUnrelated:
        this.editForm.get(['dateUnrelated']).value != null
          ? moment(this.editForm.get(['dateUnrelated']).value, DATE_TIME_FORMAT)
          : undefined,
      nickname: this.editForm.get(['nickname']).value,
      userRelated: this.editForm.get(['userRelated']).value,
      extendedUser: this.editForm.get(['extendedUser']).value,
      relatedBy: this.editForm.get(['relatedBy']).value,
      unrelatedBy: this.editForm.get(['unrelatedBy']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IExtendedUserRelation>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }

  trackExtendedUserById(index: number, item: IExtendedUser) {
    return item.id;
  }
}
