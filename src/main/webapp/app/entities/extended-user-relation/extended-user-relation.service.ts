import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IExtendedUserRelation } from 'app/shared/model/extended-user-relation.model';

type EntityResponseType = HttpResponse<IExtendedUserRelation>;
type EntityArrayResponseType = HttpResponse<IExtendedUserRelation[]>;

@Injectable({ providedIn: 'root' })
export class ExtendedUserRelationService {
  public resourceUrl = SERVER_API_URL + 'api/extended-user-relations';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/extended-user-relations';

  constructor(protected http: HttpClient) {}

  create(extendedUserRelation: IExtendedUserRelation): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(extendedUserRelation);
    return this.http
      .post<IExtendedUserRelation>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(extendedUserRelation: IExtendedUserRelation): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(extendedUserRelation);
    return this.http
      .put<IExtendedUserRelation>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IExtendedUserRelation>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IExtendedUserRelation[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IExtendedUserRelation[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(extendedUserRelation: IExtendedUserRelation): IExtendedUserRelation {
    const copy: IExtendedUserRelation = Object.assign({}, extendedUserRelation, {
      dateRelated:
        extendedUserRelation.dateRelated != null && extendedUserRelation.dateRelated.isValid()
          ? extendedUserRelation.dateRelated.toJSON()
          : null,
      dateUnrelated:
        extendedUserRelation.dateUnrelated != null && extendedUserRelation.dateUnrelated.isValid()
          ? extendedUserRelation.dateUnrelated.toJSON()
          : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateRelated = res.body.dateRelated != null ? moment(res.body.dateRelated) : null;
      res.body.dateUnrelated = res.body.dateUnrelated != null ? moment(res.body.dateUnrelated) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((extendedUserRelation: IExtendedUserRelation) => {
        extendedUserRelation.dateRelated = extendedUserRelation.dateRelated != null ? moment(extendedUserRelation.dateRelated) : null;
        extendedUserRelation.dateUnrelated = extendedUserRelation.dateUnrelated != null ? moment(extendedUserRelation.dateUnrelated) : null;
      });
    }
    return res;
  }
}
