import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IExtendedUserRelation } from 'app/shared/model/extended-user-relation.model';
import { ExtendedUserRelationService } from './extended-user-relation.service';

@Component({
  selector: 'jhi-extended-user-relation-delete-dialog',
  templateUrl: './extended-user-relation-delete-dialog.component.html'
})
export class ExtendedUserRelationDeleteDialogComponent {
  extendedUserRelation: IExtendedUserRelation;

  constructor(
    protected extendedUserRelationService: ExtendedUserRelationService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.extendedUserRelationService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'extendedUserRelationListModification',
        content: 'Deleted an extendedUserRelation'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-extended-user-relation-delete-popup',
  template: ''
})
export class ExtendedUserRelationDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ extendedUserRelation }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(ExtendedUserRelationDeleteDialogComponent as Component, {
          size: 'lg',
          backdrop: 'static'
        });
        this.ngbModalRef.componentInstance.extendedUserRelation = extendedUserRelation;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/extended-user-relation', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/extended-user-relation', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
