import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ExtendedUserRelation, IExtendedUserRelation } from 'app/shared/model/extended-user-relation.model';
import { ExtendedUserRelationService } from './extended-user-relation.service';
import { ExtendedUserRelationComponent } from './extended-user-relation.component';
import { ExtendedUserRelationDetailComponent } from './extended-user-relation-detail.component';
import { ExtendedUserRelationUpdateComponent } from './extended-user-relation-update.component';
import { ExtendedUserRelationDeletePopupComponent } from './extended-user-relation-delete-dialog.component';

@Injectable({ providedIn: 'root' })
export class ExtendedUserRelationResolve implements Resolve<IExtendedUserRelation> {
  constructor(private service: ExtendedUserRelationService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IExtendedUserRelation> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<ExtendedUserRelation>) => response.ok),
        map((extendedUserRelation: HttpResponse<ExtendedUserRelation>) => extendedUserRelation.body)
      );
    }
    return of(new ExtendedUserRelation());
  }
}

export const extendedUserRelationRoute: Routes = [
  {
    path: '',
    component: ExtendedUserRelationComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'pagoMundoApiApp.extendedUserRelation.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ExtendedUserRelationDetailComponent,
    resolve: {
      extendedUserRelation: ExtendedUserRelationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.extendedUserRelation.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ExtendedUserRelationUpdateComponent,
    resolve: {
      extendedUserRelation: ExtendedUserRelationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.extendedUserRelation.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ExtendedUserRelationUpdateComponent,
    resolve: {
      extendedUserRelation: ExtendedUserRelationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.extendedUserRelation.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const extendedUserRelationPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ExtendedUserRelationDeletePopupComponent,
    resolve: {
      extendedUserRelation: ExtendedUserRelationResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.extendedUserRelation.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
