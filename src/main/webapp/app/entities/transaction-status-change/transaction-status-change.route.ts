import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { TransactionStatusChange } from 'app/shared/model/transaction-status-change.model';
import { TransactionStatusChangeService } from './transaction-status-change.service';
import { TransactionStatusChangeComponent } from './transaction-status-change.component';
import { TransactionStatusChangeDetailComponent } from './transaction-status-change-detail.component';
import { TransactionStatusChangeUpdateComponent } from './transaction-status-change-update.component';
import { TransactionStatusChangeDeletePopupComponent } from './transaction-status-change-delete-dialog.component';
import { ITransactionStatusChange } from 'app/shared/model/transaction-status-change.model';

@Injectable({ providedIn: 'root' })
export class TransactionStatusChangeResolve implements Resolve<ITransactionStatusChange> {
  constructor(private service: TransactionStatusChangeService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ITransactionStatusChange> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<TransactionStatusChange>) => response.ok),
        map((transactionStatusChange: HttpResponse<TransactionStatusChange>) => transactionStatusChange.body)
      );
    }
    return of(new TransactionStatusChange());
  }
}

export const transactionStatusChangeRoute: Routes = [
  {
    path: '',
    component: TransactionStatusChangeComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'pagoMundoApiApp.transactionStatusChange.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: TransactionStatusChangeDetailComponent,
    resolve: {
      transactionStatusChange: TransactionStatusChangeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.transactionStatusChange.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: TransactionStatusChangeUpdateComponent,
    resolve: {
      transactionStatusChange: TransactionStatusChangeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.transactionStatusChange.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: TransactionStatusChangeUpdateComponent,
    resolve: {
      transactionStatusChange: TransactionStatusChangeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.transactionStatusChange.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const transactionStatusChangePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: TransactionStatusChangeDeletePopupComponent,
    resolve: {
      transactionStatusChange: TransactionStatusChangeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.transactionStatusChange.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
