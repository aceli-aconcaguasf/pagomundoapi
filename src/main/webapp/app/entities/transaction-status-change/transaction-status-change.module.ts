import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PagoMundoApiSharedModule } from 'app/shared/shared.module';
import { TransactionStatusChangeComponent } from './transaction-status-change.component';
import { TransactionStatusChangeDetailComponent } from './transaction-status-change-detail.component';
import { TransactionStatusChangeUpdateComponent } from './transaction-status-change-update.component';
import {
  TransactionStatusChangeDeletePopupComponent,
  TransactionStatusChangeDeleteDialogComponent
} from './transaction-status-change-delete-dialog.component';
import { transactionStatusChangeRoute, transactionStatusChangePopupRoute } from './transaction-status-change.route';

const ENTITY_STATES = [...transactionStatusChangeRoute, ...transactionStatusChangePopupRoute];

@NgModule({
  imports: [PagoMundoApiSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    TransactionStatusChangeComponent,
    TransactionStatusChangeDetailComponent,
    TransactionStatusChangeUpdateComponent,
    TransactionStatusChangeDeleteDialogComponent,
    TransactionStatusChangeDeletePopupComponent
  ],
  entryComponents: [TransactionStatusChangeDeleteDialogComponent]
})
export class PagoMundoApiTransactionStatusChangeModule {}
