import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITransactionStatusChange } from 'app/shared/model/transaction-status-change.model';
import { TransactionStatusChangeService } from './transaction-status-change.service';

@Component({
  selector: 'jhi-transaction-status-change-delete-dialog',
  templateUrl: './transaction-status-change-delete-dialog.component.html'
})
export class TransactionStatusChangeDeleteDialogComponent {
  transactionStatusChange: ITransactionStatusChange;

  constructor(
    protected transactionStatusChangeService: TransactionStatusChangeService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.transactionStatusChangeService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'transactionStatusChangeListModification',
        content: 'Deleted an transactionStatusChange'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-transaction-status-change-delete-popup',
  template: ''
})
export class TransactionStatusChangeDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ transactionStatusChange }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(TransactionStatusChangeDeleteDialogComponent as Component, {
          size: 'lg',
          backdrop: 'static'
        });
        this.ngbModalRef.componentInstance.transactionStatusChange = transactionStatusChange;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/transaction-status-change', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/transaction-status-change', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
