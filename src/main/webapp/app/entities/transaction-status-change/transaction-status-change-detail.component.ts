import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { ITransactionStatusChange } from 'app/shared/model/transaction-status-change.model';

@Component({
  selector: 'jhi-transaction-status-change-detail',
  templateUrl: './transaction-status-change-detail.component.html'
})
export class TransactionStatusChangeDetailComponent implements OnInit {
  transactionStatusChange: ITransactionStatusChange;

  constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ transactionStatusChange }) => {
      this.transactionStatusChange = transactionStatusChange;
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }
  previousState() {
    window.history.back();
  }
}
