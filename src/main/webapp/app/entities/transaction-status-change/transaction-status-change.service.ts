import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ITransactionStatusChange } from 'app/shared/model/transaction-status-change.model';

type EntityResponseType = HttpResponse<ITransactionStatusChange>;
type EntityArrayResponseType = HttpResponse<ITransactionStatusChange[]>;

@Injectable({ providedIn: 'root' })
export class TransactionStatusChangeService {
  public resourceUrl = SERVER_API_URL + 'api/transaction-status-changes';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/transaction-status-changes';

  constructor(protected http: HttpClient) {}

  create(transactionStatusChange: ITransactionStatusChange): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(transactionStatusChange);
    return this.http
      .post<ITransactionStatusChange>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(transactionStatusChange: ITransactionStatusChange): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(transactionStatusChange);
    return this.http
      .put<ITransactionStatusChange>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ITransactionStatusChange>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ITransactionStatusChange[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ITransactionStatusChange[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(transactionStatusChange: ITransactionStatusChange): ITransactionStatusChange {
    const copy: ITransactionStatusChange = Object.assign({}, transactionStatusChange, {
      dateCreated:
        transactionStatusChange.dateCreated != null && transactionStatusChange.dateCreated.isValid()
          ? transactionStatusChange.dateCreated.toJSON()
          : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateCreated = res.body.dateCreated != null ? moment(res.body.dateCreated) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((transactionStatusChange: ITransactionStatusChange) => {
        transactionStatusChange.dateCreated =
          transactionStatusChange.dateCreated != null ? moment(transactionStatusChange.dateCreated) : null;
      });
    }
    return res;
  }
}
