import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PagoMundoApiSharedModule } from 'app/shared/shared.module';
import { ExtendedUserFundsComponent } from './extended-user-funds.component';
import { ExtendedUserFundsDetailComponent } from './extended-user-funds-detail.component';
import { ExtendedUserFundsUpdateComponent } from './extended-user-funds-update.component';
import {
  ExtendedUserFundsDeleteDialogComponent,
  ExtendedUserFundsDeletePopupComponent
} from './extended-user-funds-delete-dialog.component';
import { extendedUserFundsPopupRoute, extendedUserFundsRoute } from './extended-user-funds.route';

const ENTITY_STATES = [...extendedUserFundsRoute, ...extendedUserFundsPopupRoute];

@NgModule({
  imports: [PagoMundoApiSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    ExtendedUserFundsComponent,
    ExtendedUserFundsDetailComponent,
    ExtendedUserFundsUpdateComponent,
    ExtendedUserFundsDeleteDialogComponent,
    ExtendedUserFundsDeletePopupComponent
  ],
  entryComponents: [ExtendedUserFundsDeleteDialogComponent]
})
export class PagoMundoApiExtendedUserFundsModule {}
