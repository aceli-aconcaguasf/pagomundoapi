import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IExtendedUserFunds } from 'app/shared/model/extended-user-funds.model';

@Component({
  selector: 'jhi-extended-user-funds-detail',
  templateUrl: './extended-user-funds-detail.component.html'
})
export class ExtendedUserFundsDetailComponent implements OnInit {
  extendedUserFunds: IExtendedUserFunds;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ extendedUserFunds }) => {
      this.extendedUserFunds = extendedUserFunds;
    });
  }

  previousState() {
    window.history.back();
  }
}
