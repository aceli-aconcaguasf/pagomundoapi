import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IExtendedUserFunds } from 'app/shared/model/extended-user-funds.model';
import { ExtendedUserFundsService } from './extended-user-funds.service';

@Component({
  selector: 'jhi-extended-user-funds-delete-dialog',
  templateUrl: './extended-user-funds-delete-dialog.component.html'
})
export class ExtendedUserFundsDeleteDialogComponent {
  extendedUserFunds: IExtendedUserFunds;

  constructor(
    protected extendedUserFundsService: ExtendedUserFundsService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.extendedUserFundsService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'extendedUserFundsListModification',
        content: 'Deleted an extendedUserFunds'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-extended-user-funds-delete-popup',
  template: ''
})
export class ExtendedUserFundsDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ extendedUserFunds }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(ExtendedUserFundsDeleteDialogComponent as Component, {
          size: 'lg',
          backdrop: 'static'
        });
        this.ngbModalRef.componentInstance.extendedUserFunds = extendedUserFunds;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/extended-user-funds', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/extended-user-funds', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
