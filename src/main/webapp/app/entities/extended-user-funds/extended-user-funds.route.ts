import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ExtendedUserFunds, IExtendedUserFunds } from 'app/shared/model/extended-user-funds.model';
import { ExtendedUserFundsService } from './extended-user-funds.service';
import { ExtendedUserFundsComponent } from './extended-user-funds.component';
import { ExtendedUserFundsDetailComponent } from './extended-user-funds-detail.component';
import { ExtendedUserFundsUpdateComponent } from './extended-user-funds-update.component';
import { ExtendedUserFundsDeletePopupComponent } from './extended-user-funds-delete-dialog.component';

@Injectable({ providedIn: 'root' })
export class ExtendedUserFundsResolve implements Resolve<IExtendedUserFunds> {
  constructor(private service: ExtendedUserFundsService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IExtendedUserFunds> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<ExtendedUserFunds>) => response.ok),
        map((extendedUserFunds: HttpResponse<ExtendedUserFunds>) => extendedUserFunds.body)
      );
    }
    return of(new ExtendedUserFunds());
  }
}

export const extendedUserFundsRoute: Routes = [
  {
    path: '',
    component: ExtendedUserFundsComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'pagoMundoApiApp.extendedUserFunds.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ExtendedUserFundsDetailComponent,
    resolve: {
      extendedUserFunds: ExtendedUserFundsResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.extendedUserFunds.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ExtendedUserFundsUpdateComponent,
    resolve: {
      extendedUserFunds: ExtendedUserFundsResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.extendedUserFunds.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ExtendedUserFundsUpdateComponent,
    resolve: {
      extendedUserFunds: ExtendedUserFundsResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.extendedUserFunds.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const extendedUserFundsPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ExtendedUserFundsDeletePopupComponent,
    resolve: {
      extendedUserFunds: ExtendedUserFundsResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.extendedUserFunds.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
