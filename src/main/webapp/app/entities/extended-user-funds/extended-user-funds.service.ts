import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IExtendedUserFunds } from 'app/shared/model/extended-user-funds.model';

type EntityResponseType = HttpResponse<IExtendedUserFunds>;
type EntityArrayResponseType = HttpResponse<IExtendedUserFunds[]>;

@Injectable({ providedIn: 'root' })
export class ExtendedUserFundsService {
  public resourceUrl = SERVER_API_URL + 'api/extended-user-funds';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/extended-user-funds';

  constructor(protected http: HttpClient) {}

  create(extendedUserFunds: IExtendedUserFunds): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(extendedUserFunds);
    return this.http
      .post<IExtendedUserFunds>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(extendedUserFunds: IExtendedUserFunds): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(extendedUserFunds);
    return this.http
      .put<IExtendedUserFunds>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IExtendedUserFunds>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IExtendedUserFunds[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IExtendedUserFunds[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(extendedUserFunds: IExtendedUserFunds): IExtendedUserFunds {
    const copy: IExtendedUserFunds = Object.assign({}, extendedUserFunds, {
      dateCreated:
        extendedUserFunds.dateCreated != null && extendedUserFunds.dateCreated.isValid() ? extendedUserFunds.dateCreated.toJSON() : null,
      lastUpdated:
        extendedUserFunds.lastUpdated != null && extendedUserFunds.lastUpdated.isValid() ? extendedUserFunds.lastUpdated.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateCreated = res.body.dateCreated != null ? moment(res.body.dateCreated) : null;
      res.body.lastUpdated = res.body.lastUpdated != null ? moment(res.body.lastUpdated) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((extendedUserFunds: IExtendedUserFunds) => {
        extendedUserFunds.dateCreated = extendedUserFunds.dateCreated != null ? moment(extendedUserFunds.dateCreated) : null;
        extendedUserFunds.lastUpdated = extendedUserFunds.lastUpdated != null ? moment(extendedUserFunds.lastUpdated) : null;
      });
    }
    return res;
  }
}
