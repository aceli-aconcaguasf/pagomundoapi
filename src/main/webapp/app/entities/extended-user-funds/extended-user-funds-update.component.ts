import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { ExtendedUserFunds, IExtendedUserFunds } from 'app/shared/model/extended-user-funds.model';
import { ExtendedUserFundsService } from './extended-user-funds.service';
import { IExtendedUser } from 'app/shared/model/extended-user.model';
import { ExtendedUserService } from 'app/entities/extended-user/extended-user.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';

@Component({
  selector: 'jhi-extended-user-funds-update',
  templateUrl: './extended-user-funds-update.component.html'
})
export class ExtendedUserFundsUpdateComponent implements OnInit {
  isSaving: boolean;

  extendedusers: IExtendedUser[];

  users: IUser[];

  editForm = this.fb.group({
    id: [],
    dateCreated: [],
    value: [],
    balanceBefore: [],
    balanceAfter: [],
    reason: [],
    status: [],
    category: [],
    lastUpdated: [],
    type: [],
    extendedUser: [],
    user: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected extendedUserFundsService: ExtendedUserFundsService,
    protected extendedUserService: ExtendedUserService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ extendedUserFunds }) => {
      this.updateForm(extendedUserFunds);
    });
    this.extendedUserService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IExtendedUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IExtendedUser[]>) => response.body)
      )
      .subscribe((res: IExtendedUser[]) => (this.extendedusers = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(extendedUserFunds: IExtendedUserFunds) {
    this.editForm.patchValue({
      id: extendedUserFunds.id,
      dateCreated: extendedUserFunds.dateCreated != null ? extendedUserFunds.dateCreated.format(DATE_TIME_FORMAT) : null,
      value: extendedUserFunds.value,
      balanceBefore: extendedUserFunds.balanceBefore,
      balanceAfter: extendedUserFunds.balanceAfter,
      reason: extendedUserFunds.reason,
      status: extendedUserFunds.status,
      category: extendedUserFunds.category,
      lastUpdated: extendedUserFunds.lastUpdated != null ? extendedUserFunds.lastUpdated.format(DATE_TIME_FORMAT) : null,
      type: extendedUserFunds.type,
      extendedUser: extendedUserFunds.extendedUser,
      user: extendedUserFunds.user
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const extendedUserFunds = this.createFromForm();
    if (extendedUserFunds.id !== undefined) {
      this.subscribeToSaveResponse(this.extendedUserFundsService.update(extendedUserFunds));
    } else {
      this.subscribeToSaveResponse(this.extendedUserFundsService.create(extendedUserFunds));
    }
  }

  trackExtendedUserById(index: number, item: IExtendedUser) {
    return item.id;
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IExtendedUserFunds>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  private createFromForm(): IExtendedUserFunds {
    return {
      ...new ExtendedUserFunds(),
      id: this.editForm.get(['id']).value,
      dateCreated:
        this.editForm.get(['dateCreated']).value != null ? moment(this.editForm.get(['dateCreated']).value, DATE_TIME_FORMAT) : undefined,
      value: this.editForm.get(['value']).value,
      balanceBefore: this.editForm.get(['balanceBefore']).value,
      balanceAfter: this.editForm.get(['balanceAfter']).value,
      reason: this.editForm.get(['reason']).value,
      status: this.editForm.get(['status']).value,
      category: this.editForm.get(['category']).value,
      lastUpdated:
        this.editForm.get(['lastUpdated']).value != null ? moment(this.editForm.get(['lastUpdated']).value, DATE_TIME_FORMAT) : undefined,
      type: this.editForm.get(['type']).value,
      extendedUser: this.editForm.get(['extendedUser']).value,
      user: this.editForm.get(['user']).value
    };
  }
}
