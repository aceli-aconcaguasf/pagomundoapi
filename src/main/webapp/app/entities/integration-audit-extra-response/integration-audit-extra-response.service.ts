import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IIntegrationAuditExtraResponse } from 'app/shared/model/integration-audit-extra-response.model';

type EntityResponseType = HttpResponse<IIntegrationAuditExtraResponse>;
type EntityArrayResponseType = HttpResponse<IIntegrationAuditExtraResponse[]>;

@Injectable({ providedIn: 'root' })
export class IntegrationAuditExtraResponseService {
  public resourceUrl = SERVER_API_URL + 'api/integration-audit-extra-responses';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/integration-audit-extra-responses';

  constructor(protected http: HttpClient) {}

  create(integrationAuditExtraResponse: IIntegrationAuditExtraResponse): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(integrationAuditExtraResponse);
    return this.http
      .post<IIntegrationAuditExtraResponse>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(integrationAuditExtraResponse: IIntegrationAuditExtraResponse): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(integrationAuditExtraResponse);
    return this.http
      .put<IIntegrationAuditExtraResponse>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IIntegrationAuditExtraResponse>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IIntegrationAuditExtraResponse[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IIntegrationAuditExtraResponse[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(integrationAuditExtraResponse: IIntegrationAuditExtraResponse): IIntegrationAuditExtraResponse {
    const copy: IIntegrationAuditExtraResponse = Object.assign({}, integrationAuditExtraResponse, {
      messageDate:
        integrationAuditExtraResponse.messageDate != null && integrationAuditExtraResponse.messageDate.isValid()
          ? integrationAuditExtraResponse.messageDate.toJSON()
          : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.messageDate = res.body.messageDate != null ? moment(res.body.messageDate) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((integrationAuditExtraResponse: IIntegrationAuditExtraResponse) => {
        integrationAuditExtraResponse.messageDate =
          integrationAuditExtraResponse.messageDate != null ? moment(integrationAuditExtraResponse.messageDate) : null;
      });
    }
    return res;
  }
}
