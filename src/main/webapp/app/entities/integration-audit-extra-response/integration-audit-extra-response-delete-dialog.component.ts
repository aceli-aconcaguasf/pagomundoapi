import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IIntegrationAuditExtraResponse } from 'app/shared/model/integration-audit-extra-response.model';
import { IntegrationAuditExtraResponseService } from './integration-audit-extra-response.service';

@Component({
  selector: 'jhi-integration-audit-extra-response-delete-dialog',
  templateUrl: './integration-audit-extra-response-delete-dialog.component.html'
})
export class IntegrationAuditExtraResponseDeleteDialogComponent {
  integrationAuditExtraResponse: IIntegrationAuditExtraResponse;

  constructor(
    protected integrationAuditExtraResponseService: IntegrationAuditExtraResponseService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.integrationAuditExtraResponseService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'integrationAuditExtraResponseListModification',
        content: 'Deleted an integrationAuditExtraResponse'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-integration-audit-extra-response-delete-popup',
  template: ''
})
export class IntegrationAuditExtraResponseDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ integrationAuditExtraResponse }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(IntegrationAuditExtraResponseDeleteDialogComponent as Component, {
          size: 'lg',
          backdrop: 'static'
        });
        this.ngbModalRef.componentInstance.integrationAuditExtraResponse = integrationAuditExtraResponse;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/integration-audit-extra-response', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/integration-audit-extra-response', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
