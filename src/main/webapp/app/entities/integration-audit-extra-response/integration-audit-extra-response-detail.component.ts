import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IIntegrationAuditExtraResponse } from 'app/shared/model/integration-audit-extra-response.model';

@Component({
  selector: 'jhi-integration-audit-extra-response-detail',
  templateUrl: './integration-audit-extra-response-detail.component.html'
})
export class IntegrationAuditExtraResponseDetailComponent implements OnInit {
  integrationAuditExtraResponse: IIntegrationAuditExtraResponse;

  constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ integrationAuditExtraResponse }) => {
      this.integrationAuditExtraResponse = integrationAuditExtraResponse;
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }

  previousState() {
    window.history.back();
  }
}
