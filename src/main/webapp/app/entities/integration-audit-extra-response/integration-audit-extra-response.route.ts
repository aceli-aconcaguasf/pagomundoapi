import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IIntegrationAuditExtraResponse, IntegrationAuditExtraResponse } from 'app/shared/model/integration-audit-extra-response.model';
import { IntegrationAuditExtraResponseService } from './integration-audit-extra-response.service';
import { IntegrationAuditExtraResponseComponent } from './integration-audit-extra-response.component';
import { IntegrationAuditExtraResponseDetailComponent } from './integration-audit-extra-response-detail.component';
import { IntegrationAuditExtraResponseUpdateComponent } from './integration-audit-extra-response-update.component';
import { IntegrationAuditExtraResponseDeletePopupComponent } from './integration-audit-extra-response-delete-dialog.component';

@Injectable({ providedIn: 'root' })
export class IntegrationAuditExtraResponseResolve implements Resolve<IIntegrationAuditExtraResponse> {
  constructor(private service: IntegrationAuditExtraResponseService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IIntegrationAuditExtraResponse> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<IntegrationAuditExtraResponse>) => response.ok),
        map((integrationAuditExtraResponse: HttpResponse<IntegrationAuditExtraResponse>) => integrationAuditExtraResponse.body)
      );
    }
    return of(new IntegrationAuditExtraResponse());
  }
}

export const integrationAuditExtraResponseRoute: Routes = [
  {
    path: '',
    component: IntegrationAuditExtraResponseComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'pagoMundoApiApp.integrationAuditExtraResponse.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: IntegrationAuditExtraResponseDetailComponent,
    resolve: {
      integrationAuditExtraResponse: IntegrationAuditExtraResponseResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.integrationAuditExtraResponse.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: IntegrationAuditExtraResponseUpdateComponent,
    resolve: {
      integrationAuditExtraResponse: IntegrationAuditExtraResponseResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.integrationAuditExtraResponse.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: IntegrationAuditExtraResponseUpdateComponent,
    resolve: {
      integrationAuditExtraResponse: IntegrationAuditExtraResponseResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.integrationAuditExtraResponse.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const integrationAuditExtraResponsePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: IntegrationAuditExtraResponseDeletePopupComponent,
    resolve: {
      integrationAuditExtraResponse: IntegrationAuditExtraResponseResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.integrationAuditExtraResponse.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
