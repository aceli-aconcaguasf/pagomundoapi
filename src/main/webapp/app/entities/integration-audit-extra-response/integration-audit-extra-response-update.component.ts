import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { IIntegrationAuditExtraResponse, IntegrationAuditExtraResponse } from 'app/shared/model/integration-audit-extra-response.model';
import { IntegrationAuditExtraResponseService } from './integration-audit-extra-response.service';
import { IIntegrationAudit } from 'app/shared/model/integration-audit.model';
import { IntegrationAuditService } from 'app/entities/integration-audit/integration-audit.service';

@Component({
  selector: 'jhi-integration-audit-extra-response-update',
  templateUrl: './integration-audit-extra-response-update.component.html'
})
export class IntegrationAuditExtraResponseUpdateComponent implements OnInit {
  isSaving: boolean;

  integrationaudits: IIntegrationAudit[];

  editForm = this.fb.group({
    id: [],
    messageDate: [],
    message: [],
    integrationAuditId: []
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected jhiAlertService: JhiAlertService,
    protected integrationAuditExtraResponseService: IntegrationAuditExtraResponseService,
    protected integrationAuditService: IntegrationAuditService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ integrationAuditExtraResponse }) => {
      this.updateForm(integrationAuditExtraResponse);
    });
    this.integrationAuditService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IIntegrationAudit[]>) => mayBeOk.ok),
        map((response: HttpResponse<IIntegrationAudit[]>) => response.body)
      )
      .subscribe((res: IIntegrationAudit[]) => (this.integrationaudits = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(integrationAuditExtraResponse: IIntegrationAuditExtraResponse) {
    this.editForm.patchValue({
      id: integrationAuditExtraResponse.id,
      messageDate:
        integrationAuditExtraResponse.messageDate != null ? integrationAuditExtraResponse.messageDate.format(DATE_TIME_FORMAT) : null,
      message: integrationAuditExtraResponse.message,
      integrationAuditId: integrationAuditExtraResponse.integrationAuditId
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }

  setFileData(event, field: string, isImage) {
    return new Promise((resolve, reject) => {
      if (event && event.target && event.target.files && event.target.files[0]) {
        const file: File = event.target.files[0];
        if (isImage && !file.type.startsWith('image/')) {
          reject(`File was expected to be an image but was found to be ${file.type}`);
        } else {
          const filedContentType: string = field + 'ContentType';
          this.dataUtils.toBase64(file, base64Data => {
            this.editForm.patchValue({
              [field]: base64Data,
              [filedContentType]: file.type
            });
          });
        }
      } else {
        reject(`Base64 data was not set as file could not be extracted from passed parameter: ${event}`);
      }
    }).then(
      // eslint-disable-next-line no-console
      () => console.log('blob added'), // success
      this.onError
    );
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const integrationAuditExtraResponse = this.createFromForm();
    if (integrationAuditExtraResponse.id !== undefined) {
      this.subscribeToSaveResponse(this.integrationAuditExtraResponseService.update(integrationAuditExtraResponse));
    } else {
      this.subscribeToSaveResponse(this.integrationAuditExtraResponseService.create(integrationAuditExtraResponse));
    }
  }

  trackIntegrationAuditById(index: number, item: IIntegrationAudit) {
    return item.id;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IIntegrationAuditExtraResponse>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  private createFromForm(): IIntegrationAuditExtraResponse {
    return {
      ...new IntegrationAuditExtraResponse(),
      id: this.editForm.get(['id']).value,
      messageDate:
        this.editForm.get(['messageDate']).value != null ? moment(this.editForm.get(['messageDate']).value, DATE_TIME_FORMAT) : undefined,
      message: this.editForm.get(['message']).value,
      integrationAuditId: this.editForm.get(['integrationAuditId']).value
    };
  }
}
