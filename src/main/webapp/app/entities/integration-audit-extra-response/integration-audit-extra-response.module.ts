import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PagoMundoApiSharedModule } from 'app/shared/shared.module';
import { IntegrationAuditExtraResponseComponent } from './integration-audit-extra-response.component';
import { IntegrationAuditExtraResponseDetailComponent } from './integration-audit-extra-response-detail.component';
import { IntegrationAuditExtraResponseUpdateComponent } from './integration-audit-extra-response-update.component';
import {
  IntegrationAuditExtraResponseDeleteDialogComponent,
  IntegrationAuditExtraResponseDeletePopupComponent
} from './integration-audit-extra-response-delete-dialog.component';
import { integrationAuditExtraResponsePopupRoute, integrationAuditExtraResponseRoute } from './integration-audit-extra-response.route';

const ENTITY_STATES = [...integrationAuditExtraResponseRoute, ...integrationAuditExtraResponsePopupRoute];

@NgModule({
  imports: [PagoMundoApiSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    IntegrationAuditExtraResponseComponent,
    IntegrationAuditExtraResponseDetailComponent,
    IntegrationAuditExtraResponseUpdateComponent,
    IntegrationAuditExtraResponseDeleteDialogComponent,
    IntegrationAuditExtraResponseDeletePopupComponent
  ],
  entryComponents: [IntegrationAuditExtraResponseDeleteDialogComponent]
})
export class PagoMundoApiIntegrationAuditExtraResponseModule {}
