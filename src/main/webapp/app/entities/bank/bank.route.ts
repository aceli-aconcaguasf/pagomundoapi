import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Bank, IBank } from 'app/shared/model/bank.model';
import { BankService } from './bank.service';
import { BankComponent } from './bank.component';
import { BankDetailComponent } from './bank-detail.component';
import { BankUpdateComponent } from './bank-update.component';
import { BankDeletePopupComponent } from './bank-delete-dialog.component';

@Injectable({ providedIn: 'root' })
export class BankResolve implements Resolve<IBank> {
  constructor(private service: BankService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IBank> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Bank>) => response.ok),
        map((bank: HttpResponse<Bank>) => bank.body)
      );
    }
    return of(new Bank());
  }
}

export const bankRoute: Routes = [
  {
    path: '',
    component: BankComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'pagoMundoApiApp.bank.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: BankDetailComponent,
    resolve: {
      bank: BankResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.bank.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: BankUpdateComponent,
    resolve: {
      bank: BankResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.bank.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: BankUpdateComponent,
    resolve: {
      bank: BankResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.bank.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const bankPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: BankDeletePopupComponent,
    resolve: {
      bank: BankResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.bank.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
