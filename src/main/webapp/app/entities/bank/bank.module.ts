import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PagoMundoApiSharedModule } from 'app/shared/shared.module';
import { BankComponent } from './bank.component';
import { BankDetailComponent } from './bank-detail.component';
import { BankUpdateComponent } from './bank-update.component';
import { BankDeleteDialogComponent, BankDeletePopupComponent } from './bank-delete-dialog.component';
import { bankPopupRoute, bankRoute } from './bank.route';

const ENTITY_STATES = [...bankRoute, ...bankPopupRoute];

@NgModule({
  imports: [PagoMundoApiSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [BankComponent, BankDetailComponent, BankUpdateComponent, BankDeleteDialogComponent, BankDeletePopupComponent],
  entryComponents: [BankDeleteDialogComponent]
})
export class PagoMundoApiBankModule {}
