import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { IExtendedUser, ExtendedUser } from 'app/shared/model/extended-user.model';
import { ExtendedUserService } from './extended-user.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { ICity } from 'app/shared/model/city.model';
import { CityService } from 'app/entities/city/city.service';
import { IIdType } from 'app/shared/model/id-type.model';
import { IdTypeService } from 'app/entities/id-type/id-type.service';
import { IExtendedUserGroup } from 'app/shared/model/extended-user-group.model';
import { ExtendedUserGroupService } from 'app/entities/extended-user-group/extended-user-group.service';
import { IBank } from 'app/shared/model/bank.model';
import { BankService } from 'app/entities/bank/bank.service';
import { ICountry } from 'app/shared/model/country.model';
import { CountryService } from 'app/entities/country/country.service';

@Component({
  selector: 'jhi-extended-user-update',
  templateUrl: './extended-user-update.component.html'
})
export class ExtendedUserUpdateComponent implements OnInit {
  isSaving: boolean;

  users: IUser[];

  cities: ICity[];

  idtypes: IIdType[];

  extendedusergroups: IExtendedUserGroup[];

  banks: IBank[];

  countries: ICountry[];

  extendedusers: IExtendedUser[];

  editForm = this.fb.group({
    id: [],
    status: [],
    lastName1: [],
    lastName2: [],
    firstName1: [],
    firstName2: [],
    fullName: [],
    idNumber: [],
    taxId: [],
    gender: [],
    maritalStatus: [],
    residenceAddress: [],
    postalAddress: [],
    phoneNumber: [],
    mobileNumber: [],
    birthDate: [],
    dateCreated: [],
    lastUpdated: [],
    email: [],
    company: [],
    imageIdUrl: [],
    imageAddressUrl: [],
    balance: [],
    statusReason: [],
    role: [],
    cardNumber: [],
    bankCommission: [],
    fxCommission: [],
    rechargeCost: [],
    useMerchantCommission: [],
    bankAccountNumber: [],
    useDirectPayment: [],
    bankAccountType: [],
    canChangePaymentMethod: [],
    mustNotify: [],
    profileInfoChanged: [],
    confirmedProfile: [],
    resellerFixedCommission: [],
    resellerPercentageCommission: [],
    useFlatCommission: [],
    bankBranch: [],
    alias: [],
    user: [],
    residenceCity: [],
    postalCity: [],
    idType: [],
    extendedUserGroup: [],
    bank: [],
    residenceCountry: [],
    postalCountry: [],
    directPaymentBank: [],
    directPaymentCity: [],
    idTypeTaxId: [],
    reseller: []
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected jhiAlertService: JhiAlertService,
    protected extendedUserService: ExtendedUserService,
    protected userService: UserService,
    protected cityService: CityService,
    protected idTypeService: IdTypeService,
    protected extendedUserGroupService: ExtendedUserGroupService,
    protected bankService: BankService,
    protected countryService: CountryService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ extendedUser }) => {
      this.updateForm(extendedUser);
    });
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.cityService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ICity[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICity[]>) => response.body)
      )
      .subscribe((res: ICity[]) => (this.cities = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.idTypeService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IIdType[]>) => mayBeOk.ok),
        map((response: HttpResponse<IIdType[]>) => response.body)
      )
      .subscribe((res: IIdType[]) => (this.idtypes = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.extendedUserGroupService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IExtendedUserGroup[]>) => mayBeOk.ok),
        map((response: HttpResponse<IExtendedUserGroup[]>) => response.body)
      )
      .subscribe((res: IExtendedUserGroup[]) => (this.extendedusergroups = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.bankService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IBank[]>) => mayBeOk.ok),
        map((response: HttpResponse<IBank[]>) => response.body)
      )
      .subscribe((res: IBank[]) => (this.banks = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.countryService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ICountry[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICountry[]>) => response.body)
      )
      .subscribe((res: ICountry[]) => (this.countries = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.extendedUserService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IExtendedUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IExtendedUser[]>) => response.body)
      )
      .subscribe((res: IExtendedUser[]) => (this.extendedusers = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(extendedUser: IExtendedUser) {
    this.editForm.patchValue({
      id: extendedUser.id,
      status: extendedUser.status,
      lastName1: extendedUser.lastName1,
      lastName2: extendedUser.lastName2,
      firstName1: extendedUser.firstName1,
      firstName2: extendedUser.firstName2,
      fullName: extendedUser.fullName,
      idNumber: extendedUser.idNumber,
      taxId: extendedUser.taxId,
      gender: extendedUser.gender,
      maritalStatus: extendedUser.maritalStatus,
      residenceAddress: extendedUser.residenceAddress,
      postalAddress: extendedUser.postalAddress,
      phoneNumber: extendedUser.phoneNumber,
      mobileNumber: extendedUser.mobileNumber,
      birthDate: extendedUser.birthDate != null ? extendedUser.birthDate.format(DATE_TIME_FORMAT) : null,
      dateCreated: extendedUser.dateCreated != null ? extendedUser.dateCreated.format(DATE_TIME_FORMAT) : null,
      lastUpdated: extendedUser.lastUpdated != null ? extendedUser.lastUpdated.format(DATE_TIME_FORMAT) : null,
      email: extendedUser.email,
      company: extendedUser.company,
      imageIdUrl: extendedUser.imageIdUrl,
      imageAddressUrl: extendedUser.imageAddressUrl,
      balance: extendedUser.balance,
      statusReason: extendedUser.statusReason,
      role: extendedUser.role,
      cardNumber: extendedUser.cardNumber,
      bankCommission: extendedUser.bankCommission,
      fxCommission: extendedUser.fxCommission,
      rechargeCost: extendedUser.rechargeCost,
      useMerchantCommission: extendedUser.useMerchantCommission,
      bankAccountNumber: extendedUser.bankAccountNumber,
      useDirectPayment: extendedUser.useDirectPayment,
      bankAccountType: extendedUser.bankAccountType,
      canChangePaymentMethod: extendedUser.canChangePaymentMethod,
      mustNotify: extendedUser.mustNotify,
      profileInfoChanged: extendedUser.profileInfoChanged,
      confirmedProfile: extendedUser.confirmedProfile,
      resellerFixedCommission: extendedUser.resellerFixedCommission,
      resellerPercentageCommission: extendedUser.resellerPercentageCommission,
      useFlatCommission: extendedUser.useFlatCommission,
      bankBranch: extendedUser.bankBranch,
      alias: extendedUser.alias,
      user: extendedUser.user,
      residenceCity: extendedUser.residenceCity,
      postalCity: extendedUser.postalCity,
      idType: extendedUser.idType,
      extendedUserGroup: extendedUser.extendedUserGroup,
      bank: extendedUser.bank,
      residenceCountry: extendedUser.residenceCountry,
      postalCountry: extendedUser.postalCountry,
      directPaymentBank: extendedUser.directPaymentBank,
      directPaymentCity: extendedUser.directPaymentCity,
      idTypeTaxId: extendedUser.idTypeTaxId,
      reseller: extendedUser.reseller
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }

  setFileData(event, field: string, isImage) {
    return new Promise((resolve, reject) => {
      if (event && event.target && event.target.files && event.target.files[0]) {
        const file: File = event.target.files[0];
        if (isImage && !file.type.startsWith('image/')) {
          reject(`File was expected to be an image but was found to be ${file.type}`);
        } else {
          const filedContentType: string = field + 'ContentType';
          this.dataUtils.toBase64(file, base64Data => {
            this.editForm.patchValue({
              [field]: base64Data,
              [filedContentType]: file.type
            });
          });
        }
      } else {
        reject(`Base64 data was not set as file could not be extracted from passed parameter: ${event}`);
      }
    }).then(
      // eslint-disable-next-line no-console
      () => console.log('blob added'), // success
      this.onError
    );
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const extendedUser = this.createFromForm();
    if (extendedUser.id !== undefined) {
      this.subscribeToSaveResponse(this.extendedUserService.update(extendedUser));
    } else {
      this.subscribeToSaveResponse(this.extendedUserService.create(extendedUser));
    }
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }

  trackCityById(index: number, item: ICity) {
    return item.id;
  }

  trackIdTypeById(index: number, item: IIdType) {
    return item.id;
  }

  trackExtendedUserGroupById(index: number, item: IExtendedUserGroup) {
    return item.id;
  }

  trackBankById(index: number, item: IBank) {
    return item.id;
  }

  trackCountryById(index: number, item: ICountry) {
    return item.id;
  }

  trackExtendedUserById(index: number, item: IExtendedUser) {
    return item.id;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IExtendedUser>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  private createFromForm(): IExtendedUser {
    return {
      ...new ExtendedUser(),
      id: this.editForm.get(['id']).value,
      status: this.editForm.get(['status']).value,
      lastName1: this.editForm.get(['lastName1']).value,
      lastName2: this.editForm.get(['lastName2']).value,
      firstName1: this.editForm.get(['firstName1']).value,
      firstName2: this.editForm.get(['firstName2']).value,
      fullName: this.editForm.get(['fullName']).value,
      idNumber: this.editForm.get(['idNumber']).value,
      taxId: this.editForm.get(['taxId']).value,
      gender: this.editForm.get(['gender']).value,
      maritalStatus: this.editForm.get(['maritalStatus']).value,
      residenceAddress: this.editForm.get(['residenceAddress']).value,
      postalAddress: this.editForm.get(['postalAddress']).value,
      phoneNumber: this.editForm.get(['phoneNumber']).value,
      mobileNumber: this.editForm.get(['mobileNumber']).value,
      birthDate:
        this.editForm.get(['birthDate']).value != null ? moment(this.editForm.get(['birthDate']).value, DATE_TIME_FORMAT) : undefined,
      dateCreated:
        this.editForm.get(['dateCreated']).value != null ? moment(this.editForm.get(['dateCreated']).value, DATE_TIME_FORMAT) : undefined,
      lastUpdated:
        this.editForm.get(['lastUpdated']).value != null ? moment(this.editForm.get(['lastUpdated']).value, DATE_TIME_FORMAT) : undefined,
      email: this.editForm.get(['email']).value,
      company: this.editForm.get(['company']).value,
      imageIdUrl: this.editForm.get(['imageIdUrl']).value,
      imageAddressUrl: this.editForm.get(['imageAddressUrl']).value,
      balance: this.editForm.get(['balance']).value,
      statusReason: this.editForm.get(['statusReason']).value,
      role: this.editForm.get(['role']).value,
      cardNumber: this.editForm.get(['cardNumber']).value,
      bankCommission: this.editForm.get(['bankCommission']).value,
      fxCommission: this.editForm.get(['fxCommission']).value,
      rechargeCost: this.editForm.get(['rechargeCost']).value,
      useMerchantCommission: this.editForm.get(['useMerchantCommission']).value,
      bankAccountNumber: this.editForm.get(['bankAccountNumber']).value,
      useDirectPayment: this.editForm.get(['useDirectPayment']).value,
      bankAccountType: this.editForm.get(['bankAccountType']).value,
      canChangePaymentMethod: this.editForm.get(['canChangePaymentMethod']).value,
      mustNotify: this.editForm.get(['mustNotify']).value,
      profileInfoChanged: this.editForm.get(['profileInfoChanged']).value,
      confirmedProfile: this.editForm.get(['confirmedProfile']).value,
      resellerFixedCommission: this.editForm.get(['resellerFixedCommission']).value,
      resellerPercentageCommission: this.editForm.get(['resellerPercentageCommission']).value,
      useFlatCommission: this.editForm.get(['useFlatCommission']).value,
      bankBranch: this.editForm.get(['bankBranch']).value,
      alias: this.editForm.get(['alias']).value,
      user: this.editForm.get(['user']).value,
      residenceCity: this.editForm.get(['residenceCity']).value,
      postalCity: this.editForm.get(['postalCity']).value,
      idType: this.editForm.get(['idType']).value,
      extendedUserGroup: this.editForm.get(['extendedUserGroup']).value,
      bank: this.editForm.get(['bank']).value,
      residenceCountry: this.editForm.get(['residenceCountry']).value,
      postalCountry: this.editForm.get(['postalCountry']).value,
      directPaymentBank: this.editForm.get(['directPaymentBank']).value,
      directPaymentCity: this.editForm.get(['directPaymentCity']).value,
      idTypeTaxId: this.editForm.get(['idTypeTaxId']).value,
      reseller: this.editForm.get(['reseller']).value
    };
  }
}
