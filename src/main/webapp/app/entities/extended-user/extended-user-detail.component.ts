import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IExtendedUser } from 'app/shared/model/extended-user.model';

@Component({
  selector: 'jhi-extended-user-detail',
  templateUrl: './extended-user-detail.component.html'
})
export class ExtendedUserDetailComponent implements OnInit {
  extendedUser: IExtendedUser;

  constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ extendedUser }) => {
      this.extendedUser = extendedUser;
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }
  previousState() {
    window.history.back();
  }
}
