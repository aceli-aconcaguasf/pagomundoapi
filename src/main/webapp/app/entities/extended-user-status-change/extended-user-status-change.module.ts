import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PagoMundoApiSharedModule } from 'app/shared/shared.module';
import { ExtendedUserStatusChangeComponent } from './extended-user-status-change.component';
import { ExtendedUserStatusChangeDetailComponent } from './extended-user-status-change-detail.component';
import { ExtendedUserStatusChangeUpdateComponent } from './extended-user-status-change-update.component';
import {
  ExtendedUserStatusChangeDeleteDialogComponent,
  ExtendedUserStatusChangeDeletePopupComponent
} from './extended-user-status-change-delete-dialog.component';
import { extendedUserStatusChangePopupRoute, extendedUserStatusChangeRoute } from './extended-user-status-change.route';

const ENTITY_STATES = [...extendedUserStatusChangeRoute, ...extendedUserStatusChangePopupRoute];

@NgModule({
  imports: [PagoMundoApiSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    ExtendedUserStatusChangeComponent,
    ExtendedUserStatusChangeDetailComponent,
    ExtendedUserStatusChangeUpdateComponent,
    ExtendedUserStatusChangeDeleteDialogComponent,
    ExtendedUserStatusChangeDeletePopupComponent
  ],
  entryComponents: [ExtendedUserStatusChangeDeleteDialogComponent]
})
export class PagoMundoApiExtendedUserStatusChangeModule {}
