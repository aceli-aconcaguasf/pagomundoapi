import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ExtendedUserStatusChange, IExtendedUserStatusChange } from 'app/shared/model/extended-user-status-change.model';
import { ExtendedUserStatusChangeService } from './extended-user-status-change.service';
import { ExtendedUserStatusChangeComponent } from './extended-user-status-change.component';
import { ExtendedUserStatusChangeDetailComponent } from './extended-user-status-change-detail.component';
import { ExtendedUserStatusChangeUpdateComponent } from './extended-user-status-change-update.component';
import { ExtendedUserStatusChangeDeletePopupComponent } from './extended-user-status-change-delete-dialog.component';

@Injectable({ providedIn: 'root' })
export class ExtendedUserStatusChangeResolve implements Resolve<IExtendedUserStatusChange> {
  constructor(private service: ExtendedUserStatusChangeService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IExtendedUserStatusChange> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<ExtendedUserStatusChange>) => response.ok),
        map((extendedUserStatusChange: HttpResponse<ExtendedUserStatusChange>) => extendedUserStatusChange.body)
      );
    }
    return of(new ExtendedUserStatusChange());
  }
}

export const extendedUserStatusChangeRoute: Routes = [
  {
    path: '',
    component: ExtendedUserStatusChangeComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'pagoMundoApiApp.extendedUserStatusChange.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ExtendedUserStatusChangeDetailComponent,
    resolve: {
      extendedUserStatusChange: ExtendedUserStatusChangeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.extendedUserStatusChange.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ExtendedUserStatusChangeUpdateComponent,
    resolve: {
      extendedUserStatusChange: ExtendedUserStatusChangeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.extendedUserStatusChange.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ExtendedUserStatusChangeUpdateComponent,
    resolve: {
      extendedUserStatusChange: ExtendedUserStatusChangeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.extendedUserStatusChange.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const extendedUserStatusChangePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ExtendedUserStatusChangeDeletePopupComponent,
    resolve: {
      extendedUserStatusChange: ExtendedUserStatusChangeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.extendedUserStatusChange.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
