import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IExtendedUserStatusChange } from 'app/shared/model/extended-user-status-change.model';
import { ExtendedUserStatusChangeService } from './extended-user-status-change.service';

@Component({
  selector: 'jhi-extended-user-status-change-delete-dialog',
  templateUrl: './extended-user-status-change-delete-dialog.component.html'
})
export class ExtendedUserStatusChangeDeleteDialogComponent {
  extendedUserStatusChange: IExtendedUserStatusChange;

  constructor(
    protected extendedUserStatusChangeService: ExtendedUserStatusChangeService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.extendedUserStatusChangeService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'extendedUserStatusChangeListModification',
        content: 'Deleted an extendedUserStatusChange'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-extended-user-status-change-delete-popup',
  template: ''
})
export class ExtendedUserStatusChangeDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ extendedUserStatusChange }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(ExtendedUserStatusChangeDeleteDialogComponent as Component, {
          size: 'lg',
          backdrop: 'static'
        });
        this.ngbModalRef.componentInstance.extendedUserStatusChange = extendedUserStatusChange;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/extended-user-status-change', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/extended-user-status-change', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
