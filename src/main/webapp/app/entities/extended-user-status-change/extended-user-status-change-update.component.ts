import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { ExtendedUserStatusChange, IExtendedUserStatusChange } from 'app/shared/model/extended-user-status-change.model';
import { ExtendedUserStatusChangeService } from './extended-user-status-change.service';
import { IExtendedUser } from 'app/shared/model/extended-user.model';
import { ExtendedUserService } from 'app/entities/extended-user/extended-user.service';

@Component({
  selector: 'jhi-extended-user-status-change-update',
  templateUrl: './extended-user-status-change-update.component.html'
})
export class ExtendedUserStatusChangeUpdateComponent implements OnInit {
  isSaving: boolean;

  extendedusers: IExtendedUser[];

  editForm = this.fb.group({
    id: [],
    dateCreated: [],
    status: [],
    statusReason: [],
    extendedUser: [],
    userId: []
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected jhiAlertService: JhiAlertService,
    protected extendedUserStatusChangeService: ExtendedUserStatusChangeService,
    protected extendedUserService: ExtendedUserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ extendedUserStatusChange }) => {
      this.updateForm(extendedUserStatusChange);
    });
    this.extendedUserService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IExtendedUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IExtendedUser[]>) => response.body)
      )
      .subscribe((res: IExtendedUser[]) => (this.extendedusers = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(extendedUserStatusChange: IExtendedUserStatusChange) {
    this.editForm.patchValue({
      id: extendedUserStatusChange.id,
      dateCreated: extendedUserStatusChange.dateCreated != null ? extendedUserStatusChange.dateCreated.format(DATE_TIME_FORMAT) : null,
      status: extendedUserStatusChange.status,
      statusReason: extendedUserStatusChange.statusReason,
      extendedUser: extendedUserStatusChange.extendedUser,
      userId: extendedUserStatusChange.userId
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }

  setFileData(event, field: string, isImage) {
    return new Promise((resolve, reject) => {
      if (event && event.target && event.target.files && event.target.files[0]) {
        const file: File = event.target.files[0];
        if (isImage && !file.type.startsWith('image/')) {
          reject(`File was expected to be an image but was found to be ${file.type}`);
        } else {
          const filedContentType: string = field + 'ContentType';
          this.dataUtils.toBase64(file, base64Data => {
            this.editForm.patchValue({
              [field]: base64Data,
              [filedContentType]: file.type
            });
          });
        }
      } else {
        reject(`Base64 data was not set as file could not be extracted from passed parameter: ${event}`);
      }
    }).then(
      // eslint-disable-next-line no-console
      () => console.log('blob added'), // success
      this.onError
    );
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const extendedUserStatusChange = this.createFromForm();
    if (extendedUserStatusChange.id !== undefined) {
      this.subscribeToSaveResponse(this.extendedUserStatusChangeService.update(extendedUserStatusChange));
    } else {
      this.subscribeToSaveResponse(this.extendedUserStatusChangeService.create(extendedUserStatusChange));
    }
  }

  trackExtendedUserById(index: number, item: IExtendedUser) {
    return item.id;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IExtendedUserStatusChange>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  private createFromForm(): IExtendedUserStatusChange {
    return {
      ...new ExtendedUserStatusChange(),
      id: this.editForm.get(['id']).value,
      dateCreated:
        this.editForm.get(['dateCreated']).value != null ? moment(this.editForm.get(['dateCreated']).value, DATE_TIME_FORMAT) : undefined,
      status: this.editForm.get(['status']).value,
      statusReason: this.editForm.get(['statusReason']).value,
      extendedUser: this.editForm.get(['extendedUser']).value,
      userId: this.editForm.get(['userId']).value
    };
  }
}
