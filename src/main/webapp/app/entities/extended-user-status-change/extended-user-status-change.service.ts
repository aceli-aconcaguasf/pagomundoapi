import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IExtendedUserStatusChange } from 'app/shared/model/extended-user-status-change.model';

type EntityResponseType = HttpResponse<IExtendedUserStatusChange>;
type EntityArrayResponseType = HttpResponse<IExtendedUserStatusChange[]>;

@Injectable({ providedIn: 'root' })
export class ExtendedUserStatusChangeService {
  public resourceUrl = SERVER_API_URL + 'api/extended-user-status-changes';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/extended-user-status-changes';

  constructor(protected http: HttpClient) {}

  create(extendedUserStatusChange: IExtendedUserStatusChange): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(extendedUserStatusChange);
    return this.http
      .post<IExtendedUserStatusChange>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(extendedUserStatusChange: IExtendedUserStatusChange): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(extendedUserStatusChange);
    return this.http
      .put<IExtendedUserStatusChange>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IExtendedUserStatusChange>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IExtendedUserStatusChange[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IExtendedUserStatusChange[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(extendedUserStatusChange: IExtendedUserStatusChange): IExtendedUserStatusChange {
    const copy: IExtendedUserStatusChange = Object.assign({}, extendedUserStatusChange, {
      dateCreated:
        extendedUserStatusChange.dateCreated != null && extendedUserStatusChange.dateCreated.isValid()
          ? extendedUserStatusChange.dateCreated.toJSON()
          : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateCreated = res.body.dateCreated != null ? moment(res.body.dateCreated) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((extendedUserStatusChange: IExtendedUserStatusChange) => {
        extendedUserStatusChange.dateCreated =
          extendedUserStatusChange.dateCreated != null ? moment(extendedUserStatusChange.dateCreated) : null;
      });
    }
    return res;
  }
}
