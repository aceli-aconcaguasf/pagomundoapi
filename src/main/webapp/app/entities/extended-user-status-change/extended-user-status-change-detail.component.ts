import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IExtendedUserStatusChange } from 'app/shared/model/extended-user-status-change.model';

@Component({
  selector: 'jhi-extended-user-status-change-detail',
  templateUrl: './extended-user-status-change-detail.component.html'
})
export class ExtendedUserStatusChangeDetailComponent implements OnInit {
  extendedUserStatusChange: IExtendedUserStatusChange;

  constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ extendedUserStatusChange }) => {
      this.extendedUserStatusChange = extendedUserStatusChange;
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }

  previousState() {
    window.history.back();
  }
}
