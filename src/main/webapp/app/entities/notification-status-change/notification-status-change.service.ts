import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { INotificationStatusChange } from 'app/shared/model/notification-status-change.model';

type EntityResponseType = HttpResponse<INotificationStatusChange>;
type EntityArrayResponseType = HttpResponse<INotificationStatusChange[]>;

@Injectable({ providedIn: 'root' })
export class NotificationStatusChangeService {
  public resourceUrl = SERVER_API_URL + 'api/notification-status-changes';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/notification-status-changes';

  constructor(protected http: HttpClient) {}

  create(notificationStatusChange: INotificationStatusChange): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(notificationStatusChange);
    return this.http
      .post<INotificationStatusChange>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(notificationStatusChange: INotificationStatusChange): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(notificationStatusChange);
    return this.http
      .put<INotificationStatusChange>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<INotificationStatusChange>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<INotificationStatusChange[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<INotificationStatusChange[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(notificationStatusChange: INotificationStatusChange): INotificationStatusChange {
    const copy: INotificationStatusChange = Object.assign({}, notificationStatusChange, {
      dateCreated:
        notificationStatusChange.dateCreated != null && notificationStatusChange.dateCreated.isValid()
          ? notificationStatusChange.dateCreated.toJSON()
          : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateCreated = res.body.dateCreated != null ? moment(res.body.dateCreated) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((notificationStatusChange: INotificationStatusChange) => {
        notificationStatusChange.dateCreated =
          notificationStatusChange.dateCreated != null ? moment(notificationStatusChange.dateCreated) : null;
      });
    }
    return res;
  }
}
