import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { INotificationStatusChange } from 'app/shared/model/notification-status-change.model';

@Component({
  selector: 'jhi-notification-status-change-detail',
  templateUrl: './notification-status-change-detail.component.html'
})
export class NotificationStatusChangeDetailComponent implements OnInit {
  notificationStatusChange: INotificationStatusChange;

  constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ notificationStatusChange }) => {
      this.notificationStatusChange = notificationStatusChange;
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }
  previousState() {
    window.history.back();
  }
}
