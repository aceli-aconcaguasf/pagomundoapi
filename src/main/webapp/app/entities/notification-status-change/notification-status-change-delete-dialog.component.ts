import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { INotificationStatusChange } from 'app/shared/model/notification-status-change.model';
import { NotificationStatusChangeService } from './notification-status-change.service';

@Component({
  selector: 'jhi-notification-status-change-delete-dialog',
  templateUrl: './notification-status-change-delete-dialog.component.html'
})
export class NotificationStatusChangeDeleteDialogComponent {
  notificationStatusChange: INotificationStatusChange;

  constructor(
    protected notificationStatusChangeService: NotificationStatusChangeService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.notificationStatusChangeService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'notificationStatusChangeListModification',
        content: 'Deleted an notificationStatusChange'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-notification-status-change-delete-popup',
  template: ''
})
export class NotificationStatusChangeDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ notificationStatusChange }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(NotificationStatusChangeDeleteDialogComponent as Component, {
          size: 'lg',
          backdrop: 'static'
        });
        this.ngbModalRef.componentInstance.notificationStatusChange = notificationStatusChange;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/notification-status-change', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/notification-status-change', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
