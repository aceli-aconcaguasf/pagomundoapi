import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PagoMundoApiSharedModule } from 'app/shared/shared.module';
import { NotificationStatusChangeComponent } from './notification-status-change.component';
import { NotificationStatusChangeDetailComponent } from './notification-status-change-detail.component';
import { NotificationStatusChangeUpdateComponent } from './notification-status-change-update.component';
import {
  NotificationStatusChangeDeletePopupComponent,
  NotificationStatusChangeDeleteDialogComponent
} from './notification-status-change-delete-dialog.component';
import { notificationStatusChangeRoute, notificationStatusChangePopupRoute } from './notification-status-change.route';

const ENTITY_STATES = [...notificationStatusChangeRoute, ...notificationStatusChangePopupRoute];

@NgModule({
  imports: [PagoMundoApiSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    NotificationStatusChangeComponent,
    NotificationStatusChangeDetailComponent,
    NotificationStatusChangeUpdateComponent,
    NotificationStatusChangeDeleteDialogComponent,
    NotificationStatusChangeDeletePopupComponent
  ],
  entryComponents: [NotificationStatusChangeDeleteDialogComponent]
})
export class PagoMundoApiNotificationStatusChangeModule {}
