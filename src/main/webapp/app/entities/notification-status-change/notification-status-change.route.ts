import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { NotificationStatusChange } from 'app/shared/model/notification-status-change.model';
import { NotificationStatusChangeService } from './notification-status-change.service';
import { NotificationStatusChangeComponent } from './notification-status-change.component';
import { NotificationStatusChangeDetailComponent } from './notification-status-change-detail.component';
import { NotificationStatusChangeUpdateComponent } from './notification-status-change-update.component';
import { NotificationStatusChangeDeletePopupComponent } from './notification-status-change-delete-dialog.component';
import { INotificationStatusChange } from 'app/shared/model/notification-status-change.model';

@Injectable({ providedIn: 'root' })
export class NotificationStatusChangeResolve implements Resolve<INotificationStatusChange> {
  constructor(private service: NotificationStatusChangeService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<INotificationStatusChange> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<NotificationStatusChange>) => response.ok),
        map((notificationStatusChange: HttpResponse<NotificationStatusChange>) => notificationStatusChange.body)
      );
    }
    return of(new NotificationStatusChange());
  }
}

export const notificationStatusChangeRoute: Routes = [
  {
    path: '',
    component: NotificationStatusChangeComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'pagoMundoApiApp.notificationStatusChange.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: NotificationStatusChangeDetailComponent,
    resolve: {
      notificationStatusChange: NotificationStatusChangeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.notificationStatusChange.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: NotificationStatusChangeUpdateComponent,
    resolve: {
      notificationStatusChange: NotificationStatusChangeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.notificationStatusChange.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: NotificationStatusChangeUpdateComponent,
    resolve: {
      notificationStatusChange: NotificationStatusChangeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.notificationStatusChange.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const notificationStatusChangePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: NotificationStatusChangeDeletePopupComponent,
    resolve: {
      notificationStatusChange: NotificationStatusChangeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.notificationStatusChange.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
