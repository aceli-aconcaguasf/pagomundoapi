import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { INotificationStatusChange, NotificationStatusChange } from 'app/shared/model/notification-status-change.model';
import { NotificationStatusChangeService } from './notification-status-change.service';
import { INotification } from 'app/shared/model/notification.model';
import { NotificationService } from 'app/entities/notification/notification.service';
import { IExtendedUser } from 'app/shared/model/extended-user.model';
import { ExtendedUserService } from 'app/entities/extended-user/extended-user.service';

@Component({
  selector: 'jhi-notification-status-change-update',
  templateUrl: './notification-status-change-update.component.html'
})
export class NotificationStatusChangeUpdateComponent implements OnInit {
  isSaving: boolean;

  notifications: INotification[];

  extendedusers: IExtendedUser[];

  editForm = this.fb.group({
    id: [],
    dateCreated: [null, [Validators.required]],
    status: [null, [Validators.required]],
    reason: [],
    notification: [],
    user: []
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected jhiAlertService: JhiAlertService,
    protected notificationStatusChangeService: NotificationStatusChangeService,
    protected notificationService: NotificationService,
    protected extendedUserService: ExtendedUserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ notificationStatusChange }) => {
      this.updateForm(notificationStatusChange);
    });
    this.notificationService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<INotification[]>) => mayBeOk.ok),
        map((response: HttpResponse<INotification[]>) => response.body)
      )
      .subscribe((res: INotification[]) => (this.notifications = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.extendedUserService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IExtendedUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IExtendedUser[]>) => response.body)
      )
      .subscribe((res: IExtendedUser[]) => (this.extendedusers = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(notificationStatusChange: INotificationStatusChange) {
    this.editForm.patchValue({
      id: notificationStatusChange.id,
      dateCreated: notificationStatusChange.dateCreated != null ? notificationStatusChange.dateCreated.format(DATE_TIME_FORMAT) : null,
      status: notificationStatusChange.status,
      reason: notificationStatusChange.reason,
      notification: notificationStatusChange.notification,
      user: notificationStatusChange.user
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }

  setFileData(event, field: string, isImage) {
    return new Promise((resolve, reject) => {
      if (event && event.target && event.target.files && event.target.files[0]) {
        const file: File = event.target.files[0];
        if (isImage && !file.type.startsWith('image/')) {
          reject(`File was expected to be an image but was found to be ${file.type}`);
        } else {
          const filedContentType: string = field + 'ContentType';
          this.dataUtils.toBase64(file, base64Data => {
            this.editForm.patchValue({
              [field]: base64Data,
              [filedContentType]: file.type
            });
          });
        }
      } else {
        reject(`Base64 data was not set as file could not be extracted from passed parameter: ${event}`);
      }
    }).then(
      // eslint-disable-next-line no-console
      () => console.log('blob added'), // success
      this.onError
    );
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const notificationStatusChange = this.createFromForm();
    if (notificationStatusChange.id !== undefined) {
      this.subscribeToSaveResponse(this.notificationStatusChangeService.update(notificationStatusChange));
    } else {
      this.subscribeToSaveResponse(this.notificationStatusChangeService.create(notificationStatusChange));
    }
  }

  private createFromForm(): INotificationStatusChange {
    return {
      ...new NotificationStatusChange(),
      id: this.editForm.get(['id']).value,
      dateCreated:
        this.editForm.get(['dateCreated']).value != null ? moment(this.editForm.get(['dateCreated']).value, DATE_TIME_FORMAT) : undefined,
      status: this.editForm.get(['status']).value,
      reason: this.editForm.get(['reason']).value,
      notification: this.editForm.get(['notification']).value,
      user: this.editForm.get(['user']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<INotificationStatusChange>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackNotificationById(index: number, item: INotification) {
    return item.id;
  }

  trackExtendedUserById(index: number, item: IExtendedUser) {
    return item.id;
  }
}
