import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PagoMundoApiSharedModule } from 'app/shared/shared.module';
import { BranchComponent } from './branch.component';
import { BranchDetailComponent } from './branch-detail.component';
import { BranchUpdateComponent } from './branch-update.component';
import { BranchDeleteDialogComponent, BranchDeletePopupComponent } from './branch-delete-dialog.component';
import { branchPopupRoute, branchRoute } from './branch.route';

const ENTITY_STATES = [...branchRoute, ...branchPopupRoute];

@NgModule({
  imports: [PagoMundoApiSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [BranchComponent, BranchDetailComponent, BranchUpdateComponent, BranchDeleteDialogComponent, BranchDeletePopupComponent],
  entryComponents: [BranchDeleteDialogComponent]
})
export class PagoMundoApiBranchModule {}
