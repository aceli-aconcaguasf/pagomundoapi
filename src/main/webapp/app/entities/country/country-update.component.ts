import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { Country, ICountry } from 'app/shared/model/country.model';
import { CountryService } from './country.service';

@Component({
  selector: 'jhi-country-update',
  templateUrl: './country-update.component.html'
})
export class CountryUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    name: [],
    spanishName: [],
    subRegionName: [],
    regionName: [],
    code: [],
    forPayee: [],
    currencyName: [],
    currencyCode: [],
    currencyValue: [],
    currencyBase: [],
    currencyLastUpdated: []
  });

  constructor(protected countryService: CountryService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ country }) => {
      this.updateForm(country);
    });
  }

  updateForm(country: ICountry) {
    this.editForm.patchValue({
      id: country.id,
      name: country.name,
      spanishName: country.spanishName,
      subRegionName: country.subRegionName,
      regionName: country.regionName,
      code: country.code,
      forPayee: country.forPayee,
      currencyName: country.currencyName,
      currencyCode: country.currencyCode,
      currencyValue: country.currencyValue,
      currencyBase: country.currencyBase,
      currencyLastUpdated: country.currencyLastUpdated != null ? country.currencyLastUpdated.format(DATE_TIME_FORMAT) : null
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const country = this.createFromForm();
    if (country.id !== undefined) {
      this.subscribeToSaveResponse(this.countryService.update(country));
    } else {
      this.subscribeToSaveResponse(this.countryService.create(country));
    }
  }

  private createFromForm(): ICountry {
    return {
      ...new Country(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      spanishName: this.editForm.get(['spanishName']).value,
      subRegionName: this.editForm.get(['subRegionName']).value,
      regionName: this.editForm.get(['regionName']).value,
      code: this.editForm.get(['code']).value,
      forPayee: this.editForm.get(['forPayee']).value,
      currencyName: this.editForm.get(['currencyName']).value,
      currencyCode: this.editForm.get(['currencyCode']).value,
      currencyValue: this.editForm.get(['currencyValue']).value,
      currencyBase: this.editForm.get(['currencyBase']).value,
      currencyLastUpdated:
        this.editForm.get(['currencyLastUpdated']).value != null
          ? moment(this.editForm.get(['currencyLastUpdated']).value, DATE_TIME_FORMAT)
          : undefined
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICountry>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
