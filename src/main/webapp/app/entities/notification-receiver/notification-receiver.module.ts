import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PagoMundoApiSharedModule } from 'app/shared/shared.module';
import { NotificationReceiverComponent } from './notification-receiver.component';
import { NotificationReceiverDetailComponent } from './notification-receiver-detail.component';
import { NotificationReceiverUpdateComponent } from './notification-receiver-update.component';
import {
  NotificationReceiverDeleteDialogComponent,
  NotificationReceiverDeletePopupComponent
} from './notification-receiver-delete-dialog.component';
import { notificationReceiverPopupRoute, notificationReceiverRoute } from './notification-receiver.route';

const ENTITY_STATES = [...notificationReceiverRoute, ...notificationReceiverPopupRoute];

@NgModule({
  imports: [PagoMundoApiSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    NotificationReceiverComponent,
    NotificationReceiverDetailComponent,
    NotificationReceiverUpdateComponent,
    NotificationReceiverDeleteDialogComponent,
    NotificationReceiverDeletePopupComponent
  ],
  entryComponents: [NotificationReceiverDeleteDialogComponent]
})
export class PagoMundoApiNotificationReceiverModule {}
