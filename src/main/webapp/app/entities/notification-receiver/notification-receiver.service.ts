import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { INotificationReceiver } from 'app/shared/model/notification-receiver.model';

type EntityResponseType = HttpResponse<INotificationReceiver>;
type EntityArrayResponseType = HttpResponse<INotificationReceiver[]>;

@Injectable({ providedIn: 'root' })
export class NotificationReceiverService {
  public resourceUrl = SERVER_API_URL + 'api/notification-receivers';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/notification-receivers';

  constructor(protected http: HttpClient) {}

  create(notificationReceiver: INotificationReceiver): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(notificationReceiver);
    return this.http
      .post<INotificationReceiver>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(notificationReceiver: INotificationReceiver): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(notificationReceiver);
    return this.http
      .put<INotificationReceiver>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<INotificationReceiver>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<INotificationReceiver[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<INotificationReceiver[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(notificationReceiver: INotificationReceiver): INotificationReceiver {
    const copy: INotificationReceiver = Object.assign({}, notificationReceiver, {
      dateCreated:
        notificationReceiver.dateCreated != null && notificationReceiver.dateCreated.isValid()
          ? notificationReceiver.dateCreated.toJSON()
          : null,
      lastUpdated:
        notificationReceiver.lastUpdated != null && notificationReceiver.lastUpdated.isValid()
          ? notificationReceiver.lastUpdated.toJSON()
          : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateCreated = res.body.dateCreated != null ? moment(res.body.dateCreated) : null;
      res.body.lastUpdated = res.body.lastUpdated != null ? moment(res.body.lastUpdated) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((notificationReceiver: INotificationReceiver) => {
        notificationReceiver.dateCreated = notificationReceiver.dateCreated != null ? moment(notificationReceiver.dateCreated) : null;
        notificationReceiver.lastUpdated = notificationReceiver.lastUpdated != null ? moment(notificationReceiver.lastUpdated) : null;
      });
    }
    return res;
  }
}
