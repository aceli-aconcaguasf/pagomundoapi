import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { INotificationReceiver } from 'app/shared/model/notification-receiver.model';
import { NotificationReceiverService } from './notification-receiver.service';

@Component({
  selector: 'jhi-notification-receiver-delete-dialog',
  templateUrl: './notification-receiver-delete-dialog.component.html'
})
export class NotificationReceiverDeleteDialogComponent {
  notificationReceiver: INotificationReceiver;

  constructor(
    protected notificationReceiverService: NotificationReceiverService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.notificationReceiverService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'notificationReceiverListModification',
        content: 'Deleted an notificationReceiver'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-notification-receiver-delete-popup',
  template: ''
})
export class NotificationReceiverDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ notificationReceiver }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(NotificationReceiverDeleteDialogComponent as Component, {
          size: 'lg',
          backdrop: 'static'
        });
        this.ngbModalRef.componentInstance.notificationReceiver = notificationReceiver;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/notification-receiver', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/notification-receiver', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
