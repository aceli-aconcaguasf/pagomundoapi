import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { INotificationReceiver, NotificationReceiver } from 'app/shared/model/notification-receiver.model';
import { NotificationReceiverService } from './notification-receiver.service';
import { INotification } from 'app/shared/model/notification.model';
import { NotificationService } from 'app/entities/notification/notification.service';
import { IExtendedUser } from 'app/shared/model/extended-user.model';
import { ExtendedUserService } from 'app/entities/extended-user/extended-user.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';

@Component({
  selector: 'jhi-notification-receiver-update',
  templateUrl: './notification-receiver-update.component.html'
})
export class NotificationReceiverUpdateComponent implements OnInit {
  isSaving: boolean;

  notifications: INotification[];

  extendedusers: IExtendedUser[];

  users: IUser[];

  editForm = this.fb.group({
    id: [],
    dateCreated: [],
    lastUpdated: [],
    retries: [],
    maxRetries: [],
    status: [],
    reason: [],
    mustSendEmail: [],
    emailTemplate: [],
    emailSent: [],
    debugReason: [],
    emailTitleKey: [],
    subject: [],
    description: [],
    notification: [],
    receiver: [],
    sender: [],
    userReceiver: []
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected jhiAlertService: JhiAlertService,
    protected notificationReceiverService: NotificationReceiverService,
    protected notificationService: NotificationService,
    protected extendedUserService: ExtendedUserService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ notificationReceiver }) => {
      this.updateForm(notificationReceiver);
    });
    this.notificationService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<INotification[]>) => mayBeOk.ok),
        map((response: HttpResponse<INotification[]>) => response.body)
      )
      .subscribe((res: INotification[]) => (this.notifications = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.extendedUserService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IExtendedUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IExtendedUser[]>) => response.body)
      )
      .subscribe((res: IExtendedUser[]) => (this.extendedusers = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(notificationReceiver: INotificationReceiver) {
    this.editForm.patchValue({
      id: notificationReceiver.id,
      dateCreated: notificationReceiver.dateCreated != null ? notificationReceiver.dateCreated.format(DATE_TIME_FORMAT) : null,
      lastUpdated: notificationReceiver.lastUpdated != null ? notificationReceiver.lastUpdated.format(DATE_TIME_FORMAT) : null,
      retries: notificationReceiver.retries,
      maxRetries: notificationReceiver.maxRetries,
      status: notificationReceiver.status,
      reason: notificationReceiver.reason,
      mustSendEmail: notificationReceiver.mustSendEmail,
      emailTemplate: notificationReceiver.emailTemplate,
      emailSent: notificationReceiver.emailSent,
      debugReason: notificationReceiver.debugReason,
      emailTitleKey: notificationReceiver.emailTitleKey,
      subject: notificationReceiver.subject,
      description: notificationReceiver.description,
      notification: notificationReceiver.notification,
      receiver: notificationReceiver.receiver,
      sender: notificationReceiver.sender,
      userReceiver: notificationReceiver.userReceiver
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }

  setFileData(event, field: string, isImage) {
    return new Promise((resolve, reject) => {
      if (event && event.target && event.target.files && event.target.files[0]) {
        const file: File = event.target.files[0];
        if (isImage && !file.type.startsWith('image/')) {
          reject(`File was expected to be an image but was found to be ${file.type}`);
        } else {
          const filedContentType: string = field + 'ContentType';
          this.dataUtils.toBase64(file, base64Data => {
            this.editForm.patchValue({
              [field]: base64Data,
              [filedContentType]: file.type
            });
          });
        }
      } else {
        reject(`Base64 data was not set as file could not be extracted from passed parameter: ${event}`);
      }
    }).then(
      // eslint-disable-next-line no-console
      () => console.log('blob added'), // success
      this.onError
    );
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const notificationReceiver = this.createFromForm();
    if (notificationReceiver.id !== undefined) {
      this.subscribeToSaveResponse(this.notificationReceiverService.update(notificationReceiver));
    } else {
      this.subscribeToSaveResponse(this.notificationReceiverService.create(notificationReceiver));
    }
  }

  trackNotificationById(index: number, item: INotification) {
    return item.id;
  }

  trackExtendedUserById(index: number, item: IExtendedUser) {
    return item.id;
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<INotificationReceiver>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  private createFromForm(): INotificationReceiver {
    return {
      ...new NotificationReceiver(),
      id: this.editForm.get(['id']).value,
      dateCreated:
        this.editForm.get(['dateCreated']).value != null ? moment(this.editForm.get(['dateCreated']).value, DATE_TIME_FORMAT) : undefined,
      lastUpdated:
        this.editForm.get(['lastUpdated']).value != null ? moment(this.editForm.get(['lastUpdated']).value, DATE_TIME_FORMAT) : undefined,
      retries: this.editForm.get(['retries']).value,
      maxRetries: this.editForm.get(['maxRetries']).value,
      status: this.editForm.get(['status']).value,
      reason: this.editForm.get(['reason']).value,
      mustSendEmail: this.editForm.get(['mustSendEmail']).value,
      emailTemplate: this.editForm.get(['emailTemplate']).value,
      emailSent: this.editForm.get(['emailSent']).value,
      debugReason: this.editForm.get(['debugReason']).value,
      emailTitleKey: this.editForm.get(['emailTitleKey']).value,
      subject: this.editForm.get(['subject']).value,
      description: this.editForm.get(['description']).value,
      notification: this.editForm.get(['notification']).value,
      receiver: this.editForm.get(['receiver']).value,
      sender: this.editForm.get(['sender']).value,
      userReceiver: this.editForm.get(['userReceiver']).value
    };
  }
}
