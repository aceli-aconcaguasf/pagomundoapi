import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { INotificationReceiver, NotificationReceiver } from 'app/shared/model/notification-receiver.model';
import { NotificationReceiverService } from './notification-receiver.service';
import { NotificationReceiverComponent } from './notification-receiver.component';
import { NotificationReceiverDetailComponent } from './notification-receiver-detail.component';
import { NotificationReceiverUpdateComponent } from './notification-receiver-update.component';
import { NotificationReceiverDeletePopupComponent } from './notification-receiver-delete-dialog.component';

@Injectable({ providedIn: 'root' })
export class NotificationReceiverResolve implements Resolve<INotificationReceiver> {
  constructor(private service: NotificationReceiverService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<INotificationReceiver> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<NotificationReceiver>) => response.ok),
        map((notificationReceiver: HttpResponse<NotificationReceiver>) => notificationReceiver.body)
      );
    }
    return of(new NotificationReceiver());
  }
}

export const notificationReceiverRoute: Routes = [
  {
    path: '',
    component: NotificationReceiverComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'pagoMundoApiApp.notificationReceiver.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: NotificationReceiverDetailComponent,
    resolve: {
      notificationReceiver: NotificationReceiverResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.notificationReceiver.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: NotificationReceiverUpdateComponent,
    resolve: {
      notificationReceiver: NotificationReceiverResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.notificationReceiver.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: NotificationReceiverUpdateComponent,
    resolve: {
      notificationReceiver: NotificationReceiverResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.notificationReceiver.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const notificationReceiverPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: NotificationReceiverDeletePopupComponent,
    resolve: {
      notificationReceiver: NotificationReceiverResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.notificationReceiver.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
