import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { INotificationReceiver } from 'app/shared/model/notification-receiver.model';

@Component({
  selector: 'jhi-notification-receiver-detail',
  templateUrl: './notification-receiver-detail.component.html'
})
export class NotificationReceiverDetailComponent implements OnInit {
  notificationReceiver: INotificationReceiver;

  constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ notificationReceiver }) => {
      this.notificationReceiver = notificationReceiver;
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }
  previousState() {
    window.history.back();
  }
}
