import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IExtendedUserGroup } from 'app/shared/model/extended-user-group.model';

@Component({
  selector: 'jhi-extended-user-group-detail',
  templateUrl: './extended-user-group-detail.component.html'
})
export class ExtendedUserGroupDetailComponent implements OnInit {
  extendedUserGroup: IExtendedUserGroup;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ extendedUserGroup }) => {
      this.extendedUserGroup = extendedUserGroup;
    });
  }

  previousState() {
    window.history.back();
  }
}
