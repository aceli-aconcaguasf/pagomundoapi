import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { ExtendedUserGroup, IExtendedUserGroup } from 'app/shared/model/extended-user-group.model';
import { ExtendedUserGroupService } from './extended-user-group.service';
import { IExtendedUser } from 'app/shared/model/extended-user.model';
import { ExtendedUserService } from 'app/entities/extended-user/extended-user.service';
import { IBank } from 'app/shared/model/bank.model';
import { BankService } from 'app/entities/bank/bank.service';

@Component({
  selector: 'jhi-extended-user-group-update',
  templateUrl: './extended-user-group-update.component.html'
})
export class ExtendedUserGroupUpdateComponent implements OnInit {
  isSaving: boolean;

  extendedusers: IExtendedUser[];

  banks: IBank[];

  editForm = this.fb.group({
    id: [],
    dateCreated: [],
    status: [],
    fileName: [],
    fileNameFromBank: [],
    admin: [],
    bank: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected extendedUserGroupService: ExtendedUserGroupService,
    protected extendedUserService: ExtendedUserService,
    protected bankService: BankService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ extendedUserGroup }) => {
      this.updateForm(extendedUserGroup);
    });
    this.extendedUserService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IExtendedUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IExtendedUser[]>) => response.body)
      )
      .subscribe((res: IExtendedUser[]) => (this.extendedusers = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.bankService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IBank[]>) => mayBeOk.ok),
        map((response: HttpResponse<IBank[]>) => response.body)
      )
      .subscribe((res: IBank[]) => (this.banks = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(extendedUserGroup: IExtendedUserGroup) {
    this.editForm.patchValue({
      id: extendedUserGroup.id,
      dateCreated: extendedUserGroup.dateCreated != null ? extendedUserGroup.dateCreated.format(DATE_TIME_FORMAT) : null,
      status: extendedUserGroup.status,
      fileName: extendedUserGroup.fileName,
      fileNameFromBank: extendedUserGroup.fileNameFromBank,
      admin: extendedUserGroup.admin,
      bank: extendedUserGroup.bank
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const extendedUserGroup = this.createFromForm();
    if (extendedUserGroup.id !== undefined) {
      this.subscribeToSaveResponse(this.extendedUserGroupService.update(extendedUserGroup));
    } else {
      this.subscribeToSaveResponse(this.extendedUserGroupService.create(extendedUserGroup));
    }
  }

  trackExtendedUserById(index: number, item: IExtendedUser) {
    return item.id;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IExtendedUserGroup>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackBankById(index: number, item: IBank) {
    return item.id;
  }

  private createFromForm(): IExtendedUserGroup {
    return {
      ...new ExtendedUserGroup(),
      id: this.editForm.get(['id']).value,
      dateCreated:
        this.editForm.get(['dateCreated']).value != null ? moment(this.editForm.get(['dateCreated']).value, DATE_TIME_FORMAT) : undefined,
      status: this.editForm.get(['status']).value,
      fileName: this.editForm.get(['fileName']).value,
      fileNameFromBank: this.editForm.get(['fileNameFromBank']).value,
      admin: this.editForm.get(['admin']).value,
      bank: this.editForm.get(['bank']).value
    };
  }
}
