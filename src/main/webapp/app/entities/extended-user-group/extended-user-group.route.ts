import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ExtendedUserGroup, IExtendedUserGroup } from 'app/shared/model/extended-user-group.model';
import { ExtendedUserGroupService } from './extended-user-group.service';
import { ExtendedUserGroupComponent } from './extended-user-group.component';
import { ExtendedUserGroupDetailComponent } from './extended-user-group-detail.component';
import { ExtendedUserGroupUpdateComponent } from './extended-user-group-update.component';
import { ExtendedUserGroupDeletePopupComponent } from './extended-user-group-delete-dialog.component';

@Injectable({ providedIn: 'root' })
export class ExtendedUserGroupResolve implements Resolve<IExtendedUserGroup> {
  constructor(private service: ExtendedUserGroupService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IExtendedUserGroup> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<ExtendedUserGroup>) => response.ok),
        map((extendedUserGroup: HttpResponse<ExtendedUserGroup>) => extendedUserGroup.body)
      );
    }
    return of(new ExtendedUserGroup());
  }
}

export const extendedUserGroupRoute: Routes = [
  {
    path: '',
    component: ExtendedUserGroupComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'pagoMundoApiApp.extendedUserGroup.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ExtendedUserGroupDetailComponent,
    resolve: {
      extendedUserGroup: ExtendedUserGroupResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.extendedUserGroup.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ExtendedUserGroupUpdateComponent,
    resolve: {
      extendedUserGroup: ExtendedUserGroupResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.extendedUserGroup.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ExtendedUserGroupUpdateComponent,
    resolve: {
      extendedUserGroup: ExtendedUserGroupResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.extendedUserGroup.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const extendedUserGroupPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ExtendedUserGroupDeletePopupComponent,
    resolve: {
      extendedUserGroup: ExtendedUserGroupResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.extendedUserGroup.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
