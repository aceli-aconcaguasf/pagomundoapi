import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IExtendedUserGroup } from 'app/shared/model/extended-user-group.model';

type EntityResponseType = HttpResponse<IExtendedUserGroup>;
type EntityArrayResponseType = HttpResponse<IExtendedUserGroup[]>;

@Injectable({ providedIn: 'root' })
export class ExtendedUserGroupService {
  public resourceUrl = SERVER_API_URL + 'api/extended-user-groups';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/extended-user-groups';

  constructor(protected http: HttpClient) {}

  create(extendedUserGroup: IExtendedUserGroup): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(extendedUserGroup);
    return this.http
      .post<IExtendedUserGroup>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(extendedUserGroup: IExtendedUserGroup): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(extendedUserGroup);
    return this.http
      .put<IExtendedUserGroup>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IExtendedUserGroup>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IExtendedUserGroup[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IExtendedUserGroup[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(extendedUserGroup: IExtendedUserGroup): IExtendedUserGroup {
    const copy: IExtendedUserGroup = Object.assign({}, extendedUserGroup, {
      dateCreated:
        extendedUserGroup.dateCreated != null && extendedUserGroup.dateCreated.isValid() ? extendedUserGroup.dateCreated.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateCreated = res.body.dateCreated != null ? moment(res.body.dateCreated) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((extendedUserGroup: IExtendedUserGroup) => {
        extendedUserGroup.dateCreated = extendedUserGroup.dateCreated != null ? moment(extendedUserGroup.dateCreated) : null;
      });
    }
    return res;
  }
}
