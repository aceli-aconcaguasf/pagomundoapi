import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IExtendedUserGroup } from 'app/shared/model/extended-user-group.model';
import { ExtendedUserGroupService } from './extended-user-group.service';

@Component({
  selector: 'jhi-extended-user-group-delete-dialog',
  templateUrl: './extended-user-group-delete-dialog.component.html'
})
export class ExtendedUserGroupDeleteDialogComponent {
  extendedUserGroup: IExtendedUserGroup;

  constructor(
    protected extendedUserGroupService: ExtendedUserGroupService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.extendedUserGroupService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'extendedUserGroupListModification',
        content: 'Deleted an extendedUserGroup'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-extended-user-group-delete-popup',
  template: ''
})
export class ExtendedUserGroupDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ extendedUserGroup }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(ExtendedUserGroupDeleteDialogComponent as Component, {
          size: 'lg',
          backdrop: 'static'
        });
        this.ngbModalRef.componentInstance.extendedUserGroup = extendedUserGroup;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/extended-user-group', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/extended-user-group', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
