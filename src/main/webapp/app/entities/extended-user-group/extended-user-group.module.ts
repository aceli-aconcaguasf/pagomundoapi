import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PagoMundoApiSharedModule } from 'app/shared/shared.module';
import { ExtendedUserGroupComponent } from './extended-user-group.component';
import { ExtendedUserGroupDetailComponent } from './extended-user-group-detail.component';
import { ExtendedUserGroupUpdateComponent } from './extended-user-group-update.component';
import {
  ExtendedUserGroupDeleteDialogComponent,
  ExtendedUserGroupDeletePopupComponent
} from './extended-user-group-delete-dialog.component';
import { extendedUserGroupPopupRoute, extendedUserGroupRoute } from './extended-user-group.route';

const ENTITY_STATES = [...extendedUserGroupRoute, ...extendedUserGroupPopupRoute];

@NgModule({
  imports: [PagoMundoApiSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    ExtendedUserGroupComponent,
    ExtendedUserGroupDetailComponent,
    ExtendedUserGroupUpdateComponent,
    ExtendedUserGroupDeleteDialogComponent,
    ExtendedUserGroupDeletePopupComponent
  ],
  entryComponents: [ExtendedUserGroupDeleteDialogComponent]
})
export class PagoMundoApiExtendedUserGroupModule {}
