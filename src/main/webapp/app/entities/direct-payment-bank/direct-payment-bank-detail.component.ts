import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDirectPaymentBank } from 'app/shared/model/direct-payment-bank.model';

@Component({
  selector: 'jhi-direct-payment-bank-detail',
  templateUrl: './direct-payment-bank-detail.component.html'
})
export class DirectPaymentBankDetailComponent implements OnInit {
  directPaymentBank: IDirectPaymentBank;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ directPaymentBank }) => {
      this.directPaymentBank = directPaymentBank;
    });
  }

  previousState() {
    window.history.back();
  }
}
