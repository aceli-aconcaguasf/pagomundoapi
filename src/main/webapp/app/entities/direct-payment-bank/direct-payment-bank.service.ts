import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IDirectPaymentBank } from 'app/shared/model/direct-payment-bank.model';

type EntityResponseType = HttpResponse<IDirectPaymentBank>;
type EntityArrayResponseType = HttpResponse<IDirectPaymentBank[]>;

@Injectable({ providedIn: 'root' })
export class DirectPaymentBankService {
  public resourceUrl = SERVER_API_URL + 'api/direct-payment-banks';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/direct-payment-banks';

  constructor(protected http: HttpClient) {}

  create(directPaymentBank: IDirectPaymentBank): Observable<EntityResponseType> {
    return this.http.post<IDirectPaymentBank>(this.resourceUrl, directPaymentBank, { observe: 'response' });
  }

  update(directPaymentBank: IDirectPaymentBank): Observable<EntityResponseType> {
    return this.http.put<IDirectPaymentBank>(this.resourceUrl, directPaymentBank, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IDirectPaymentBank>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDirectPaymentBank[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IDirectPaymentBank[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
