import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { DirectPaymentBank, IDirectPaymentBank } from 'app/shared/model/direct-payment-bank.model';
import { DirectPaymentBankService } from './direct-payment-bank.service';
import { DirectPaymentBankComponent } from './direct-payment-bank.component';
import { DirectPaymentBankDetailComponent } from './direct-payment-bank-detail.component';
import { DirectPaymentBankUpdateComponent } from './direct-payment-bank-update.component';
import { DirectPaymentBankDeletePopupComponent } from './direct-payment-bank-delete-dialog.component';

@Injectable({ providedIn: 'root' })
export class DirectPaymentBankResolve implements Resolve<IDirectPaymentBank> {
  constructor(private service: DirectPaymentBankService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IDirectPaymentBank> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<DirectPaymentBank>) => response.ok),
        map((directPaymentBank: HttpResponse<DirectPaymentBank>) => directPaymentBank.body)
      );
    }
    return of(new DirectPaymentBank());
  }
}

export const directPaymentBankRoute: Routes = [
  {
    path: '',
    component: DirectPaymentBankComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'pagoMundoApiApp.directPaymentBank.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: DirectPaymentBankDetailComponent,
    resolve: {
      directPaymentBank: DirectPaymentBankResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.directPaymentBank.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: DirectPaymentBankUpdateComponent,
    resolve: {
      directPaymentBank: DirectPaymentBankResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.directPaymentBank.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: DirectPaymentBankUpdateComponent,
    resolve: {
      directPaymentBank: DirectPaymentBankResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.directPaymentBank.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const directPaymentBankPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: DirectPaymentBankDeletePopupComponent,
    resolve: {
      directPaymentBank: DirectPaymentBankResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.directPaymentBank.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
