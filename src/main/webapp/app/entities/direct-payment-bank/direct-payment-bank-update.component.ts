import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { DirectPaymentBank, IDirectPaymentBank } from 'app/shared/model/direct-payment-bank.model';
import { DirectPaymentBankService } from './direct-payment-bank.service';
import { IBank } from 'app/shared/model/bank.model';
import { BankService } from 'app/entities/bank/bank.service';
import { ICity } from 'app/shared/model/city.model';
import { CityService } from 'app/entities/city/city.service';
import { IIdType } from 'app/shared/model/id-type.model';
import { IdTypeService } from 'app/entities/id-type/id-type.service';

@Component({
  selector: 'jhi-direct-payment-bank-update',
  templateUrl: './direct-payment-bank-update.component.html'
})
export class DirectPaymentBankUpdateComponent implements OnInit {
  isSaving: boolean;

  banks: IBank[];

  cities: ICity[];

  idtypes: IIdType[];

  editForm = this.fb.group({
    id: [],
    destinyBankCode: [],
    idTypeName: [],
    idTypeCode: [],
    cityCode: [],
    departmentCode: [],
    originBank: [],
    destinyBank: [],
    destinyCity: [],
    idType: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected directPaymentBankService: DirectPaymentBankService,
    protected bankService: BankService,
    protected cityService: CityService,
    protected idTypeService: IdTypeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ directPaymentBank }) => {
      this.updateForm(directPaymentBank);
    });
    this.bankService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IBank[]>) => mayBeOk.ok),
        map((response: HttpResponse<IBank[]>) => response.body)
      )
      .subscribe((res: IBank[]) => (this.banks = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.cityService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ICity[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICity[]>) => response.body)
      )
      .subscribe((res: ICity[]) => (this.cities = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.idTypeService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IIdType[]>) => mayBeOk.ok),
        map((response: HttpResponse<IIdType[]>) => response.body)
      )
      .subscribe((res: IIdType[]) => (this.idtypes = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(directPaymentBank: IDirectPaymentBank) {
    this.editForm.patchValue({
      id: directPaymentBank.id,
      destinyBankCode: directPaymentBank.destinyBankCode,
      idTypeName: directPaymentBank.idTypeName,
      idTypeCode: directPaymentBank.idTypeCode,
      cityCode: directPaymentBank.cityCode,
      departmentCode: directPaymentBank.departmentCode,
      originBank: directPaymentBank.originBank,
      destinyBank: directPaymentBank.destinyBank,
      destinyCity: directPaymentBank.destinyCity,
      idType: directPaymentBank.idType
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const directPaymentBank = this.createFromForm();
    if (directPaymentBank.id !== undefined) {
      this.subscribeToSaveResponse(this.directPaymentBankService.update(directPaymentBank));
    } else {
      this.subscribeToSaveResponse(this.directPaymentBankService.create(directPaymentBank));
    }
  }

  trackBankById(index: number, item: IBank) {
    return item.id;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDirectPaymentBank>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackIdTypeById(index: number, item: IIdType) {
    return item.id;
  }

  trackCityById(index: number, item: ICity) {
    return item.id;
  }

  private createFromForm(): IDirectPaymentBank {
    return {
      ...new DirectPaymentBank(),
      id: this.editForm.get(['id']).value,
      destinyBankCode: this.editForm.get(['destinyBankCode']).value,
      idTypeName: this.editForm.get(['idTypeName']).value,
      idTypeCode: this.editForm.get(['idTypeCode']).value,
      cityCode: this.editForm.get(['cityCode']).value,
      departmentCode: this.editForm.get(['departmentCode']).value,
      originBank: this.editForm.get(['originBank']).value,
      destinyBank: this.editForm.get(['destinyBank']).value,
      destinyCity: this.editForm.get(['destinyCity']).value,
      idType: this.editForm.get(['idType']).value
    };
  }
}
