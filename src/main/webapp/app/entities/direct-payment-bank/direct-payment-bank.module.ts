import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PagoMundoApiSharedModule } from 'app/shared/shared.module';
import { DirectPaymentBankComponent } from './direct-payment-bank.component';
import { DirectPaymentBankDetailComponent } from './direct-payment-bank-detail.component';
import { DirectPaymentBankUpdateComponent } from './direct-payment-bank-update.component';
import {
  DirectPaymentBankDeleteDialogComponent,
  DirectPaymentBankDeletePopupComponent
} from './direct-payment-bank-delete-dialog.component';
import { directPaymentBankPopupRoute, directPaymentBankRoute } from './direct-payment-bank.route';

const ENTITY_STATES = [...directPaymentBankRoute, ...directPaymentBankPopupRoute];

@NgModule({
  imports: [PagoMundoApiSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    DirectPaymentBankComponent,
    DirectPaymentBankDetailComponent,
    DirectPaymentBankUpdateComponent,
    DirectPaymentBankDeleteDialogComponent,
    DirectPaymentBankDeletePopupComponent
  ],
  entryComponents: [DirectPaymentBankDeleteDialogComponent]
})
export class PagoMundoApiDirectPaymentBankModule {}
