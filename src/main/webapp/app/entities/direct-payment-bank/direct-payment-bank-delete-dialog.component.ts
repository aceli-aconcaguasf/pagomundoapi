import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IDirectPaymentBank } from 'app/shared/model/direct-payment-bank.model';
import { DirectPaymentBankService } from './direct-payment-bank.service';

@Component({
  selector: 'jhi-direct-payment-bank-delete-dialog',
  templateUrl: './direct-payment-bank-delete-dialog.component.html'
})
export class DirectPaymentBankDeleteDialogComponent {
  directPaymentBank: IDirectPaymentBank;

  constructor(
    protected directPaymentBankService: DirectPaymentBankService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.directPaymentBankService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'directPaymentBankListModification',
        content: 'Deleted an directPaymentBank'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-direct-payment-bank-delete-popup',
  template: ''
})
export class DirectPaymentBankDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ directPaymentBank }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(DirectPaymentBankDeleteDialogComponent as Component, {
          size: 'lg',
          backdrop: 'static'
        });
        this.ngbModalRef.componentInstance.directPaymentBank = directPaymentBank;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/direct-payment-bank', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/direct-payment-bank', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
