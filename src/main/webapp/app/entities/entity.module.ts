import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'country',
        loadChildren: () => import('./country/country.module').then(m => m.PagoMundoApiCountryModule)
      },
      {
        path: 'bank',
        loadChildren: () => import('./bank/bank.module').then(m => m.PagoMundoApiBankModule)
      },
      {
        path: 'branch',
        loadChildren: () => import('./branch/branch.module').then(m => m.PagoMundoApiBranchModule)
      },
      {
        path: 'currency',
        loadChildren: () => import('./currency/currency.module').then(m => m.PagoMundoApiCurrencyModule)
      },
      {
        path: 'city',
        loadChildren: () => import('./city/city.module').then(m => m.PagoMundoApiCityModule)
      },
      {
        path: 'id-type',
        loadChildren: () => import('./id-type/id-type.module').then(m => m.PagoMundoApiIdTypeModule)
      },
      {
        path: 'extended-user',
        loadChildren: () => import('./extended-user/extended-user.module').then(m => m.PagoMundoApiExtendedUserModule)
      },
      {
        path: 'transaction',
        loadChildren: () => import('./transaction/transaction.module').then(m => m.PagoMundoApiTransactionModule)
      },
      {
        path: 'transaction-status-change',
        loadChildren: () =>
          import('./transaction-status-change/transaction-status-change.module').then(m => m.PagoMundoApiTransactionStatusChangeModule)
      },
      {
        path: 'notification',
        loadChildren: () => import('./notification/notification.module').then(m => m.PagoMundoApiNotificationModule)
      },
      {
        path: 'notification-status-change',
        loadChildren: () =>
          import('./notification-status-change/notification-status-change.module').then(m => m.PagoMundoApiNotificationStatusChangeModule)
      },
      {
        path: 'bank-account',
        loadChildren: () => import('./bank-account/bank-account.module').then(m => m.PagoMundoApiBankAccountModule)
      },
      {
        path: 'extended-user-relation',
        loadChildren: () =>
          import('./extended-user-relation/extended-user-relation.module').then(m => m.PagoMundoApiExtendedUserRelationModule)
      },
      {
        path: 'transaction-group',
        loadChildren: () => import('./transaction-group/transaction-group.module').then(m => m.PagoMundoApiTransactionGroupModule)
      },
      {
        path: 'extended-user-group',
        loadChildren: () => import('./extended-user-group/extended-user-group.module').then(m => m.PagoMundoApiExtendedUserGroupModule)
      },
      {
        path: 'extended-user-status-change',
        loadChildren: () =>
          import('./extended-user-status-change/extended-user-status-change.module').then(m => m.PagoMundoApiExtendedUserStatusChangeModule)
      },
      {
        path: 'extended-user-funds',
        loadChildren: () => import('./extended-user-funds/extended-user-funds.module').then(m => m.PagoMundoApiExtendedUserFundsModule)
      },
      {
        path: 'notification-receiver',
        loadChildren: () =>
          import('./notification-receiver/notification-receiver.module').then(m => m.PagoMundoApiNotificationReceiverModule)
      },
      {
        path: 'direct-payment-bank',
        loadChildren: () => import('./direct-payment-bank/direct-payment-bank.module').then(m => m.PagoMundoApiDirectPaymentBankModule)
      },
      {
        path: 'integration-audit',
        loadChildren: () => import('./integration-audit/integration-audit.module').then(m => m.PagoMundoApiIntegrationAuditModule)
      },
      {
        path: 'integration-audit-extra-response',
        loadChildren: () =>
          import('./integration-audit-extra-response/integration-audit-extra-response.module').then(
            m => m.PagoMundoApiIntegrationAuditExtraResponseModule
          )
      },
      {
        path: 'flat-commission',
        loadChildren: () => import('./flat-commission/flat-commission.module').then(m => m.PagoMundoApiFlatCommissionModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class PagoMundoApiEntityModule {}
