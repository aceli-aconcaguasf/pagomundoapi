import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { IIntegrationAudit, IntegrationAudit } from 'app/shared/model/integration-audit.model';
import { IntegrationAuditService } from './integration-audit.service';

@Component({
  selector: 'jhi-integration-audit-update',
  templateUrl: './integration-audit-update.component.html'
})
export class IntegrationAuditUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    requestDate: [],
    requestMessage: [],
    requestEndpoint: [],
    responseDate: [],
    responseMessage: [],
    entityId: [],
    entity: [],
    providerId: [],
    provider: [],
    extraResponseEndpoint: [],
    status: [],
    statusReason: [],
    retries: [],
    maxRetries: []
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected jhiAlertService: JhiAlertService,
    protected integrationAuditService: IntegrationAuditService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ integrationAudit }) => {
      this.updateForm(integrationAudit);
    });
  }

  updateForm(integrationAudit: IIntegrationAudit) {
    this.editForm.patchValue({
      id: integrationAudit.id,
      requestDate: integrationAudit.requestDate != null ? integrationAudit.requestDate.format(DATE_TIME_FORMAT) : null,
      requestMessage: integrationAudit.requestMessage,
      requestEndpoint: integrationAudit.requestEndpoint,
      responseDate: integrationAudit.responseDate != null ? integrationAudit.responseDate.format(DATE_TIME_FORMAT) : null,
      responseMessage: integrationAudit.responseMessage,
      entityId: integrationAudit.entityId,
      entity: integrationAudit.entity,
      providerId: integrationAudit.providerId,
      provider: integrationAudit.provider,
      extraResponseEndpoint: integrationAudit.extraResponseEndpoint,
      status: integrationAudit.status,
      statusReason: integrationAudit.statusReason,
      retries: integrationAudit.retries,
      maxRetries: integrationAudit.maxRetries
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }

  setFileData(event, field: string, isImage) {
    return new Promise((resolve, reject) => {
      if (event && event.target && event.target.files && event.target.files[0]) {
        const file: File = event.target.files[0];
        if (isImage && !file.type.startsWith('image/')) {
          reject(`File was expected to be an image but was found to be ${file.type}`);
        } else {
          const filedContentType: string = field + 'ContentType';
          this.dataUtils.toBase64(file, base64Data => {
            this.editForm.patchValue({
              [field]: base64Data,
              [filedContentType]: file.type
            });
          });
        }
      } else {
        reject(`Base64 data was not set as file could not be extracted from passed parameter: ${event}`);
      }
    }).then(
      // eslint-disable-next-line no-console
      () => console.log('blob added'), // success
      this.onError
    );
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const integrationAudit = this.createFromForm();
    if (integrationAudit.id !== undefined) {
      this.subscribeToSaveResponse(this.integrationAuditService.update(integrationAudit));
    } else {
      this.subscribeToSaveResponse(this.integrationAuditService.create(integrationAudit));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IIntegrationAudit>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  private createFromForm(): IIntegrationAudit {
    return {
      ...new IntegrationAudit(),
      id: this.editForm.get(['id']).value,
      requestDate:
        this.editForm.get(['requestDate']).value != null ? moment(this.editForm.get(['requestDate']).value, DATE_TIME_FORMAT) : undefined,
      requestMessage: this.editForm.get(['requestMessage']).value,
      requestEndpoint: this.editForm.get(['requestEndpoint']).value,
      responseDate:
        this.editForm.get(['responseDate']).value != null ? moment(this.editForm.get(['responseDate']).value, DATE_TIME_FORMAT) : undefined,
      responseMessage: this.editForm.get(['responseMessage']).value,
      entityId: this.editForm.get(['entityId']).value,
      entity: this.editForm.get(['entity']).value,
      providerId: this.editForm.get(['providerId']).value,
      provider: this.editForm.get(['provider']).value,
      extraResponseEndpoint: this.editForm.get(['extraResponseEndpoint']).value,
      status: this.editForm.get(['status']).value,
      statusReason: this.editForm.get(['statusReason']).value,
      retries: this.editForm.get(['retries']).value,
      maxRetries: this.editForm.get(['maxRetries']).value
    };
  }
}
