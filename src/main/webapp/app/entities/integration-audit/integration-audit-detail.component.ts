import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IIntegrationAudit } from 'app/shared/model/integration-audit.model';

@Component({
  selector: 'jhi-integration-audit-detail',
  templateUrl: './integration-audit-detail.component.html'
})
export class IntegrationAuditDetailComponent implements OnInit {
  integrationAudit: IIntegrationAudit;

  constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ integrationAudit }) => {
      this.integrationAudit = integrationAudit;
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }
  previousState() {
    window.history.back();
  }
}
