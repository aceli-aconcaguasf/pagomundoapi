import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PagoMundoApiSharedModule } from 'app/shared/shared.module';
import { IntegrationAuditComponent } from './integration-audit.component';
import { IntegrationAuditDetailComponent } from './integration-audit-detail.component';
import { IntegrationAuditUpdateComponent } from './integration-audit-update.component';
import { IntegrationAuditDeleteDialogComponent, IntegrationAuditDeletePopupComponent } from './integration-audit-delete-dialog.component';
import { integrationAuditPopupRoute, integrationAuditRoute } from './integration-audit.route';

const ENTITY_STATES = [...integrationAuditRoute, ...integrationAuditPopupRoute];

@NgModule({
  imports: [PagoMundoApiSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    IntegrationAuditComponent,
    IntegrationAuditDetailComponent,
    IntegrationAuditUpdateComponent,
    IntegrationAuditDeleteDialogComponent,
    IntegrationAuditDeletePopupComponent
  ],
  entryComponents: [IntegrationAuditDeleteDialogComponent]
})
export class PagoMundoApiIntegrationAuditModule {}
