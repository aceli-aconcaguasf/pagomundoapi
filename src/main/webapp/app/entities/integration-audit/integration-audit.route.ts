import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IIntegrationAudit, IntegrationAudit } from 'app/shared/model/integration-audit.model';
import { IntegrationAuditService } from './integration-audit.service';
import { IntegrationAuditComponent } from './integration-audit.component';
import { IntegrationAuditDetailComponent } from './integration-audit-detail.component';
import { IntegrationAuditUpdateComponent } from './integration-audit-update.component';
import { IntegrationAuditDeletePopupComponent } from './integration-audit-delete-dialog.component';

@Injectable({ providedIn: 'root' })
export class IntegrationAuditResolve implements Resolve<IIntegrationAudit> {
  constructor(private service: IntegrationAuditService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IIntegrationAudit> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<IntegrationAudit>) => response.ok),
        map((integrationAudit: HttpResponse<IntegrationAudit>) => integrationAudit.body)
      );
    }
    return of(new IntegrationAudit());
  }
}

export const integrationAuditRoute: Routes = [
  {
    path: '',
    component: IntegrationAuditComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'pagoMundoApiApp.integrationAudit.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: IntegrationAuditDetailComponent,
    resolve: {
      integrationAudit: IntegrationAuditResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.integrationAudit.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: IntegrationAuditUpdateComponent,
    resolve: {
      integrationAudit: IntegrationAuditResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.integrationAudit.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: IntegrationAuditUpdateComponent,
    resolve: {
      integrationAudit: IntegrationAuditResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.integrationAudit.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const integrationAuditPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: IntegrationAuditDeletePopupComponent,
    resolve: {
      integrationAudit: IntegrationAuditResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'pagoMundoApiApp.integrationAudit.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
