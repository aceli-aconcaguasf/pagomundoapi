import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IIntegrationAudit } from 'app/shared/model/integration-audit.model';

type EntityResponseType = HttpResponse<IIntegrationAudit>;
type EntityArrayResponseType = HttpResponse<IIntegrationAudit[]>;

@Injectable({ providedIn: 'root' })
export class IntegrationAuditService {
  public resourceUrl = SERVER_API_URL + 'api/integration-audits';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/integration-audits';

  constructor(protected http: HttpClient) {}

  create(integrationAudit: IIntegrationAudit): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(integrationAudit);
    return this.http
      .post<IIntegrationAudit>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(integrationAudit: IIntegrationAudit): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(integrationAudit);
    return this.http
      .put<IIntegrationAudit>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IIntegrationAudit>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IIntegrationAudit[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IIntegrationAudit[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(integrationAudit: IIntegrationAudit): IIntegrationAudit {
    const copy: IIntegrationAudit = Object.assign({}, integrationAudit, {
      requestDate:
        integrationAudit.requestDate != null && integrationAudit.requestDate.isValid() ? integrationAudit.requestDate.toJSON() : null,
      responseDate:
        integrationAudit.responseDate != null && integrationAudit.responseDate.isValid() ? integrationAudit.responseDate.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.requestDate = res.body.requestDate != null ? moment(res.body.requestDate) : null;
      res.body.responseDate = res.body.responseDate != null ? moment(res.body.responseDate) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((integrationAudit: IIntegrationAudit) => {
        integrationAudit.requestDate = integrationAudit.requestDate != null ? moment(integrationAudit.requestDate) : null;
        integrationAudit.responseDate = integrationAudit.responseDate != null ? moment(integrationAudit.responseDate) : null;
      });
    }
    return res;
  }
}
