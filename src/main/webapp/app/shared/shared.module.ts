import { NgModule } from '@angular/core';
import { PagoMundoApiSharedLibsModule } from './shared-libs.module';
import { JhiLoginModalComponent } from './login/login.component';
import { HasAnyAuthorityDirective } from './auth/has-any-authority.directive';
import { JhiAlertErrorComponent } from 'app/shared/alert/alert-error.component';
import { JhiAlertComponent } from 'app/shared/alert/alert.component';
import { FindLanguageFromKeyPipe } from 'app/shared/language/find-language-from-key.pipe';

@NgModule({
  imports: [PagoMundoApiSharedLibsModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective, JhiAlertComponent, JhiAlertErrorComponent, FindLanguageFromKeyPipe],
  entryComponents: [JhiLoginModalComponent],
  exports: [
    PagoMundoApiSharedLibsModule,
    JhiLoginModalComponent,
    HasAnyAuthorityDirective,
    JhiAlertComponent,
    JhiAlertErrorComponent,
    FindLanguageFromKeyPipe
  ]
})
export class PagoMundoApiSharedModule {}
