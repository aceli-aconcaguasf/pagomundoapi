import { Moment } from 'moment';
import { IExtendedUser } from 'app/shared/model/extended-user.model';
import { IBank } from 'app/shared/model/bank.model';
import { Status } from 'app/shared/model/enumerations/status.model';

export interface IExtendedUserGroup {
  id?: number;
  dateCreated?: Moment;
  status?: Status;
  fileName?: string;
  fileNameFromBank?: string;
  admin?: IExtendedUser;
  bank?: IBank;
}

export class ExtendedUserGroup implements IExtendedUserGroup {
  constructor(
    public id?: number,
    public dateCreated?: Moment,
    public status?: Status,
    public fileName?: string,
    public fileNameFromBank?: string,
    public admin?: IExtendedUser,
    public bank?: IBank
  ) {}
}
