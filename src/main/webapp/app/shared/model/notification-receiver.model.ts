import { Moment } from 'moment';
import { INotification } from 'app/shared/model/notification.model';
import { IExtendedUser } from 'app/shared/model/extended-user.model';
import { IUser } from 'app/core/user/user.model';
import { Status } from 'app/shared/model/enumerations/status.model';

export interface INotificationReceiver {
  id?: number;
  dateCreated?: Moment;
  lastUpdated?: Moment;
  retries?: number;
  maxRetries?: number;
  status?: Status;
  reason?: any;
  mustSendEmail?: boolean;
  emailTemplate?: string;
  emailSent?: boolean;
  debugReason?: string;
  emailTitleKey?: string;
  subject?: string;
  description?: any;
  notification?: INotification;
  receiver?: IExtendedUser;
  sender?: IExtendedUser;
  userReceiver?: IUser;
}

export class NotificationReceiver implements INotificationReceiver {
  constructor(
    public id?: number,
    public dateCreated?: Moment,
    public lastUpdated?: Moment,
    public retries?: number,
    public maxRetries?: number,
    public status?: Status,
    public reason?: any,
    public mustSendEmail?: boolean,
    public emailTemplate?: string,
    public emailSent?: boolean,
    public debugReason?: string,
    public emailTitleKey?: string,
    public subject?: string,
    public description?: any,
    public notification?: INotification,
    public receiver?: IExtendedUser,
    public sender?: IExtendedUser,
    public userReceiver?: IUser
  ) {
    this.mustSendEmail = this.mustSendEmail || false;
    this.emailSent = this.emailSent || false;
  }
}
