import { ITransaction } from 'app/shared/model/transaction.model';
import { ICountry } from 'app/shared/model/country.model';

export interface ICurrency {
  id?: number;
  name?: string;
  code?: string;
  transactions?: ITransaction[];
  country?: ICountry;
}

export class Currency implements ICurrency {
  constructor(
    public id?: number,
    public name?: string,
    public code?: string,
    public transactions?: ITransaction[],
    public country?: ICountry
  ) {}
}
