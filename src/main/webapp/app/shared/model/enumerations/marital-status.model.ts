export const enum MaritalStatus {
  SINGLE = 'SINGLE',
  MARRIED = 'MARRIED',
  DIVORCED = 'DIVORCED',
  FREE_UNION = 'FREE_UNION',
  WIDOWER = 'WIDOWER'
}
