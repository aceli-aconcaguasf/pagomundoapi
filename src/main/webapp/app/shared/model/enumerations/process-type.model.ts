export const enum ProcessType {
  FILE_EXPORT = 'FILE_EXPORT',
  API_CALL = 'API_CALL'
}
