export const enum BankAccountType {
  AHO = 'AHO',
  CTE = 'CTE',
  CRT = 'CRT'
}
