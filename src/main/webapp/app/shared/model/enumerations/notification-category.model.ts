export const enum NotificationCategory {
  ISSUE = 'ISSUE',
  TRANSACTION = 'TRANSACTION',
  FUND = 'FUND',
  WITHDRAWAL = 'WITHDRAWAL',
  EXTENDED_USER = 'EXTENDED_USER',
  USER_ACCOUNT = 'USER_ACCOUNT',
  MESSAGE = 'MESSAGE',
  RELATION = 'RELATION'
}
