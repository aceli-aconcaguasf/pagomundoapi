export const enum NaturalOrLegal {
  NATURAL = 'NATURAL',
  LEGAL = 'LEGAL',
  BOTH = 'BOTH'
}
