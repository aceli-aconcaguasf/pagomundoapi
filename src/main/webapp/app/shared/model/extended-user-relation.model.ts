import { Moment } from 'moment';
import { IUser } from 'app/core/user/user.model';
import { IExtendedUser } from 'app/shared/model/extended-user.model';
import { Status } from 'app/shared/model/enumerations/status.model';

export interface IExtendedUserRelation {
  id?: number;
  status?: Status;
  statusReason?: string;
  dateRelated?: Moment;
  dateUnrelated?: Moment;
  nickname?: string;
  userRelated?: IUser;
  extendedUser?: IExtendedUser;
  relatedBy?: IExtendedUser;
  unrelatedBy?: IExtendedUser;
}

export class ExtendedUserRelation implements IExtendedUserRelation {
  constructor(
    public id?: number,
    public status?: Status,
    public statusReason?: string,
    public dateRelated?: Moment,
    public dateUnrelated?: Moment,
    public nickname?: string,
    public userRelated?: IUser,
    public extendedUser?: IExtendedUser,
    public relatedBy?: IExtendedUser,
    public unrelatedBy?: IExtendedUser
  ) {}
}
