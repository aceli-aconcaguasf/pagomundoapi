import { Moment } from 'moment';
import { IExtendedUser } from 'app/shared/model/extended-user.model';
import { IUser } from 'app/core/user/user.model';
import { Status } from 'app/shared/model/enumerations/status.model';
import { FundCategory } from 'app/shared/model/enumerations/fund-category.model';
import { FundType } from 'app/shared/model/enumerations/fund-type.model';

export interface IExtendedUserFunds {
  id?: number;
  dateCreated?: Moment;
  value?: number;
  balanceBefore?: number;
  balanceAfter?: number;
  reason?: string;
  status?: Status;
  category?: FundCategory;
  lastUpdated?: Moment;
  type?: FundType;
  extendedUser?: IExtendedUser;
  user?: IUser;
}

export class ExtendedUserFunds implements IExtendedUserFunds {
  constructor(
    public id?: number,
    public dateCreated?: Moment,
    public value?: number,
    public balanceBefore?: number,
    public balanceAfter?: number,
    public reason?: string,
    public status?: Status,
    public category?: FundCategory,
    public lastUpdated?: Moment,
    public type?: FundType,
    public extendedUser?: IExtendedUser,
    public user?: IUser
  ) {}
}
