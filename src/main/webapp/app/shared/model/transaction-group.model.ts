import { Moment } from 'moment';
import { ITransaction } from 'app/shared/model/transaction.model';
import { IBankAccount } from 'app/shared/model/bank-account.model';
import { IExtendedUser } from 'app/shared/model/extended-user.model';
import { Status } from 'app/shared/model/enumerations/status.model';

export interface ITransactionGroup {
  id?: number;
  dateCreated?: Moment;
  status?: Status;
  fileName?: string;
  exchangeRate?: number;
  fileNameFromBank?: string;
  directPayment?: boolean;
  transactions?: ITransaction[];
  bankAccount?: IBankAccount;
  admin?: IExtendedUser;
}

export class TransactionGroup implements ITransactionGroup {
  constructor(
    public id?: number,
    public dateCreated?: Moment,
    public status?: Status,
    public fileName?: string,
    public exchangeRate?: number,
    public fileNameFromBank?: string,
    public directPayment?: boolean,
    public transactions?: ITransaction[],
    public bankAccount?: IBankAccount,
    public admin?: IExtendedUser
  ) {
    this.directPayment = this.directPayment || false;
  }
}
