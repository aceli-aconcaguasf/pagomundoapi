import { Moment } from 'moment';
import { IBank } from 'app/shared/model/bank.model';
import { ICity } from 'app/shared/model/city.model';
import { ICurrency } from 'app/shared/model/currency.model';
import { IIdType } from 'app/shared/model/id-type.model';

export interface ICountry {
  id?: number;
  name?: string;
  spanishName?: string;
  subRegionName?: string;
  regionName?: string;
  code?: string;
  forPayee?: boolean;
  currencyName?: string;
  currencyCode?: string;
  currencyValue?: number;
  currencyBase?: string;
  currencyLastUpdated?: Moment;
  banks?: IBank[];
  cities?: ICity[];
  currencies?: ICurrency[];
  idTypes?: IIdType[];
}

export class Country implements ICountry {
  constructor(
    public id?: number,
    public name?: string,
    public spanishName?: string,
    public subRegionName?: string,
    public regionName?: string,
    public code?: string,
    public forPayee?: boolean,
    public currencyName?: string,
    public currencyCode?: string,
    public currencyValue?: number,
    public currencyBase?: string,
    public currencyLastUpdated?: Moment,
    public banks?: IBank[],
    public cities?: ICity[],
    public currencies?: ICurrency[],
    public idTypes?: IIdType[]
  ) {
    this.forPayee = this.forPayee || false;
  }
}
