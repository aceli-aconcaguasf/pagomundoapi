import { Moment } from 'moment';

export interface IIntegrationAuditExtraResponse {
  id?: number;
  messageDate?: Moment;
  message?: any;
  integrationAuditId?: number;
}

export class IntegrationAuditExtraResponse implements IIntegrationAuditExtraResponse {
  constructor(public id?: number, public messageDate?: Moment, public message?: any, public integrationAuditId?: number) {}
}
