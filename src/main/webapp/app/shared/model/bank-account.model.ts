import { ITransaction } from 'app/shared/model/transaction.model';
import { IBank } from 'app/shared/model/bank.model';

export interface IBankAccount {
  id?: number;
  accountNumber?: string;
  bankCommission?: number;
  fxCommission?: number;
  rechargeCost?: number;
  transactions?: ITransaction[];
  bank?: IBank;
}

export class BankAccount implements IBankAccount {
  constructor(
    public id?: number,
    public accountNumber?: string,
    public bankCommission?: number,
    public fxCommission?: number,
    public rechargeCost?: number,
    public transactions?: ITransaction[],
    public bank?: IBank
  ) {}
}
