import { Moment } from 'moment';
import { INotificationStatusChange } from 'app/shared/model/notification-status-change.model';
import { IExtendedUser } from 'app/shared/model/extended-user.model';
import { Status } from 'app/shared/model/enumerations/status.model';
import { NotificationCategory } from 'app/shared/model/enumerations/notification-category.model';
import { NotificationSubCategory } from 'app/shared/model/enumerations/notification-sub-category.model';
import { ReceiverGroup } from 'app/shared/model/enumerations/receiver-group.model';

export interface INotification {
  id?: number;
  subject?: string;
  description?: any;
  dateCreated?: Moment;
  lastUpdated?: Moment;
  status?: Status;
  statusReason?: any;
  category?: NotificationCategory;
  notes?: any;
  subCategory?: NotificationSubCategory;
  receiverGroup?: ReceiverGroup;
  entityId?: number;
  notificationStatusChanges?: INotificationStatusChange[];
  sender?: IExtendedUser;
  responsible?: IExtendedUser;
  extendedUser?: IExtendedUser;
}

export class Notification implements INotification {
  constructor(
    public id?: number,
    public subject?: string,
    public description?: any,
    public dateCreated?: Moment,
    public lastUpdated?: Moment,
    public status?: Status,
    public statusReason?: any,
    public category?: NotificationCategory,
    public notes?: any,
    public subCategory?: NotificationSubCategory,
    public receiverGroup?: ReceiverGroup,
    public entityId?: number,
    public notificationStatusChanges?: INotificationStatusChange[],
    public sender?: IExtendedUser,
    public responsible?: IExtendedUser,
    public extendedUser?: IExtendedUser
  ) {}
}
