import { ICountry } from 'app/shared/model/country.model';

export interface IBank {
  id?: number;
  name?: string;
  country?: ICountry;
}

export class Bank implements IBank {
  constructor(public id?: number, public name?: string, public country?: ICountry) {}
}
