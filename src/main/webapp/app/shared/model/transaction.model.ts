import { Moment } from 'moment';
import { ITransactionStatusChange } from 'app/shared/model/transaction-status-change.model';
import { ICurrency } from 'app/shared/model/currency.model';
import { IExtendedUser } from 'app/shared/model/extended-user.model';
import { IBankAccount } from 'app/shared/model/bank-account.model';
import { ITransactionGroup } from 'app/shared/model/transaction-group.model';
import { IExtendedUserRelation } from 'app/shared/model/extended-user-relation.model';
import { Status } from 'app/shared/model/enumerations/status.model';
import { ProcessType } from 'app/shared/model/enumerations/process-type.model';

export interface ITransaction {
  id?: number;
  dateCreated?: Moment;
  lastUpdated?: Moment;
  status?: Status;
  statusReason?: any;
  amountBeforeCommission?: number;
  bankCommission?: number;
  fxCommission?: number;
  rechargeCost?: number;
  amountAfterCommission?: number;
  exchangeRate?: number;
  amountLocalCurrency?: number;
  bankReference?: string;
  fileName?: string;
  description?: string;
  notes?: any;
  mustNotify?: boolean;
  infoChanged?: any;
  resellerCommission?: number;
  resellerFixedCommission?: number;
  resellerPercentageCommission?: number;
  processType?: ProcessType;
  transactionStatusChanges?: ITransactionStatusChange[];
  currency?: ICurrency;
  merchant?: IExtendedUser;
  payee?: IExtendedUser;
  admin?: IExtendedUser;
  bankAccount?: IBankAccount;
  transactionGroup?: ITransactionGroup;
  reseller?: IExtendedUser;
  extendedUserRelation?: IExtendedUserRelation;
}

export class Transaction implements ITransaction {
  constructor(
    public id?: number,
    public dateCreated?: Moment,
    public lastUpdated?: Moment,
    public status?: Status,
    public statusReason?: any,
    public amountBeforeCommission?: number,
    public bankCommission?: number,
    public fxCommission?: number,
    public rechargeCost?: number,
    public amountAfterCommission?: number,
    public exchangeRate?: number,
    public amountLocalCurrency?: number,
    public bankReference?: string,
    public fileName?: string,
    public description?: string,
    public notes?: any,
    public mustNotify?: boolean,
    public infoChanged?: any,
    public resellerCommission?: number,
    public resellerFixedCommission?: number,
    public resellerPercentageCommission?: number,
    public processType?: ProcessType,
    public transactionStatusChanges?: ITransactionStatusChange[],
    public currency?: ICurrency,
    public merchant?: IExtendedUser,
    public payee?: IExtendedUser,
    public admin?: IExtendedUser,
    public bankAccount?: IBankAccount,
    public transactionGroup?: ITransactionGroup,
    public reseller?: IExtendedUser,
    public extendedUserRelation?: IExtendedUserRelation
  ) {
    this.mustNotify = this.mustNotify || false;
  }
}
