import { Moment } from 'moment';
import { IExtendedUser } from 'app/shared/model/extended-user.model';

export interface IFlatCommission {
  id?: number;
  upTo?: number;
  commission?: number;
  description?: string;
  dateCreated?: Moment;
  lastUpdated?: Moment;
  extendedUser?: IExtendedUser;
}

export class FlatCommission implements IFlatCommission {
  constructor(
    public id?: number,
    public upTo?: number,
    public commission?: number,
    public description?: string,
    public dateCreated?: Moment,
    public lastUpdated?: Moment,
    public extendedUser?: IExtendedUser
  ) {}
}
