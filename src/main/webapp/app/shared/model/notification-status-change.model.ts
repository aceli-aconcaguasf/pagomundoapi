import { Moment } from 'moment';
import { INotification } from 'app/shared/model/notification.model';
import { IExtendedUser } from 'app/shared/model/extended-user.model';
import { Status } from 'app/shared/model/enumerations/status.model';

export interface INotificationStatusChange {
  id?: number;
  dateCreated?: Moment;
  status?: Status;
  reason?: any;
  notification?: INotification;
  user?: IExtendedUser;
}

export class NotificationStatusChange implements INotificationStatusChange {
  constructor(
    public id?: number,
    public dateCreated?: Moment,
    public status?: Status,
    public reason?: any,
    public notification?: INotification,
    public user?: IExtendedUser
  ) {}
}
