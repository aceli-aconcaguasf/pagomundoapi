import { Moment } from 'moment';
import { IUser } from 'app/core/user/user.model';
import { IExtendedUserRelation } from 'app/shared/model/extended-user-relation.model';
import { INotification } from 'app/shared/model/notification.model';
import { INotificationStatusChange } from 'app/shared/model/notification-status-change.model';
import { ITransaction } from 'app/shared/model/transaction.model';
import { ITransactionStatusChange } from 'app/shared/model/transaction-status-change.model';
import { ICity } from 'app/shared/model/city.model';
import { IIdType } from 'app/shared/model/id-type.model';
import { IExtendedUserGroup } from 'app/shared/model/extended-user-group.model';
import { IBank } from 'app/shared/model/bank.model';
import { ICountry } from 'app/shared/model/country.model';
import { IExtendedUser } from 'app/shared/model/extended-user.model';
import { Status } from 'app/shared/model/enumerations/status.model';
import { Gender } from 'app/shared/model/enumerations/gender.model';
import { MaritalStatus } from 'app/shared/model/enumerations/marital-status.model';
import { BankAccountType } from 'app/shared/model/enumerations/bank-account-type.model';

export interface IExtendedUser {
  id?: number;
  status?: Status;
  lastName1?: string;
  lastName2?: string;
  firstName1?: string;
  firstName2?: string;
  fullName?: string;
  idNumber?: string;
  taxId?: string;
  gender?: Gender;
  maritalStatus?: MaritalStatus;
  residenceAddress?: string;
  postalAddress?: string;
  phoneNumber?: string;
  mobileNumber?: string;
  birthDate?: Moment;
  dateCreated?: Moment;
  lastUpdated?: Moment;
  email?: string;
  company?: string;
  imageIdUrl?: string;
  imageAddressUrl?: string;
  balance?: number;
  statusReason?: any;
  role?: string;
  cardNumber?: string;
  bankCommission?: number;
  fxCommission?: number;
  rechargeCost?: number;
  useMerchantCommission?: boolean;
  bankAccountNumber?: string;
  useDirectPayment?: boolean;
  bankAccountType?: BankAccountType;
  canChangePaymentMethod?: boolean;
  mustNotify?: boolean;
  profileInfoChanged?: any;
  confirmedProfile?: boolean;
  resellerFixedCommission?: number;
  resellerPercentageCommission?: number;
  useFlatCommission?: boolean;
  bankBranch?: string;
  alias?: string;
  user?: IUser;
  userRelations?: IExtendedUserRelation[];
  notificationSenders?: INotification[];
  notificationReceivers?: INotification[];
  notificationResponsibles?: INotification[];
  notificationStatusChanges?: INotificationStatusChange[];
  transactionMerchants?: ITransaction[];
  transactionPayees?: ITransaction[];
  transactionAdmins?: ITransaction[];
  transactionStatusChanges?: ITransactionStatusChange[];
  residenceCity?: ICity;
  postalCity?: ICity;
  idType?: IIdType;
  extendedUserGroup?: IExtendedUserGroup;
  bank?: IBank;
  residenceCountry?: ICountry;
  postalCountry?: ICountry;
  directPaymentBank?: IBank;
  directPaymentCity?: ICity;
  idTypeTaxId?: IIdType;
  reseller?: IExtendedUser;
}

export class ExtendedUser implements IExtendedUser {
  constructor(
    public id?: number,
    public status?: Status,
    public lastName1?: string,
    public lastName2?: string,
    public firstName1?: string,
    public firstName2?: string,
    public fullName?: string,
    public idNumber?: string,
    public taxId?: string,
    public gender?: Gender,
    public maritalStatus?: MaritalStatus,
    public residenceAddress?: string,
    public postalAddress?: string,
    public phoneNumber?: string,
    public mobileNumber?: string,
    public birthDate?: Moment,
    public dateCreated?: Moment,
    public lastUpdated?: Moment,
    public email?: string,
    public company?: string,
    public imageIdUrl?: string,
    public imageAddressUrl?: string,
    public balance?: number,
    public statusReason?: any,
    public role?: string,
    public cardNumber?: string,
    public bankCommission?: number,
    public fxCommission?: number,
    public rechargeCost?: number,
    public useMerchantCommission?: boolean,
    public bankAccountNumber?: string,
    public useDirectPayment?: boolean,
    public bankAccountType?: BankAccountType,
    public canChangePaymentMethod?: boolean,
    public mustNotify?: boolean,
    public profileInfoChanged?: any,
    public confirmedProfile?: boolean,
    public resellerFixedCommission?: number,
    public resellerPercentageCommission?: number,
    public useFlatCommission?: boolean,
    public bankBranch?: string,
    public alias?: string,
    public user?: IUser,
    public userRelations?: IExtendedUserRelation[],
    public notificationSenders?: INotification[],
    public notificationReceivers?: INotification[],
    public notificationResponsibles?: INotification[],
    public notificationStatusChanges?: INotificationStatusChange[],
    public transactionMerchants?: ITransaction[],
    public transactionPayees?: ITransaction[],
    public transactionAdmins?: ITransaction[],
    public transactionStatusChanges?: ITransactionStatusChange[],
    public residenceCity?: ICity,
    public postalCity?: ICity,
    public idType?: IIdType,
    public extendedUserGroup?: IExtendedUserGroup,
    public bank?: IBank,
    public residenceCountry?: ICountry,
    public postalCountry?: ICountry,
    public directPaymentBank?: IBank,
    public directPaymentCity?: ICity,
    public idTypeTaxId?: IIdType,
    public reseller?: IExtendedUser
  ) {
    this.useMerchantCommission = this.useMerchantCommission || false;
    this.useDirectPayment = this.useDirectPayment || false;
    this.canChangePaymentMethod = this.canChangePaymentMethod || false;
    this.mustNotify = this.mustNotify || false;
    this.confirmedProfile = this.confirmedProfile || false;
    this.useFlatCommission = this.useFlatCommission || false;
  }
}
