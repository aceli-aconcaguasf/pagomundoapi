import { Moment } from 'moment';
import { Status } from 'app/shared/model/enumerations/status.model';

export interface IIntegrationAudit {
  id?: number;
  requestDate?: Moment;
  requestMessage?: any;
  requestEndpoint?: string;
  responseDate?: Moment;
  responseMessage?: any;
  entityId?: number;
  entity?: string;
  providerId?: string;
  provider?: string;
  extraResponseEndpoint?: string;
  status?: Status;
  statusReason?: any;
  retries?: number;
  maxRetries?: number;
}

export class IntegrationAudit implements IIntegrationAudit {
  constructor(
    public id?: number,
    public requestDate?: Moment,
    public requestMessage?: any,
    public requestEndpoint?: string,
    public responseDate?: Moment,
    public responseMessage?: any,
    public entityId?: number,
    public entity?: string,
    public providerId?: string,
    public provider?: string,
    public extraResponseEndpoint?: string,
    public status?: Status,
    public statusReason?: any,
    public retries?: number,
    public maxRetries?: number
  ) {}
}
