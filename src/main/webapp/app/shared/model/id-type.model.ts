import { IExtendedUser } from 'app/shared/model/extended-user.model';
import { ICountry } from 'app/shared/model/country.model';
import { NaturalOrLegal } from 'app/shared/model/enumerations/natural-or-legal.model';

export interface IIdType {
  id?: number;
  name?: string;
  code?: string;
  naturalOrLegal?: NaturalOrLegal;
  extendedUsers?: IExtendedUser[];
  country?: ICountry;
}

export class IdType implements IIdType {
  constructor(
    public id?: number,
    public name?: string,
    public code?: string,
    public naturalOrLegal?: NaturalOrLegal,
    public extendedUsers?: IExtendedUser[],
    public country?: ICountry
  ) {}
}
