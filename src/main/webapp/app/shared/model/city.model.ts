import { IExtendedUser } from 'app/shared/model/extended-user.model';
import { ICountry } from 'app/shared/model/country.model';

export interface ICity {
  id?: number;
  name?: string;
  code?: string;
  extendedUsersForResidenceCities?: IExtendedUser[];
  extendedUsersForPostalCities?: IExtendedUser[];
  country?: ICountry;
}

export class City implements ICity {
  constructor(
    public id?: number,
    public name?: string,
    public code?: string,
    public extendedUsersForResidenceCities?: IExtendedUser[],
    public extendedUsersForPostalCities?: IExtendedUser[],
    public country?: ICountry
  ) {}
}
