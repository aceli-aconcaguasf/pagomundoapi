export interface IBranch {
  id?: number;
}

export class Branch implements IBranch {
  constructor(public id?: number) {}
}
