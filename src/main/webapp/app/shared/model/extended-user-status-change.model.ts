import { Moment } from 'moment';
import { IExtendedUser } from 'app/shared/model/extended-user.model';
import { Status } from 'app/shared/model/enumerations/status.model';

export interface IExtendedUserStatusChange {
  id?: number;
  dateCreated?: Moment;
  status?: Status;
  statusReason?: any;
  extendedUser?: IExtendedUser;
  userId?: IExtendedUser;
}

export class ExtendedUserStatusChange implements IExtendedUserStatusChange {
  constructor(
    public id?: number,
    public dateCreated?: Moment,
    public status?: Status,
    public statusReason?: any,
    public extendedUser?: IExtendedUser,
    public userId?: IExtendedUser
  ) {}
}
