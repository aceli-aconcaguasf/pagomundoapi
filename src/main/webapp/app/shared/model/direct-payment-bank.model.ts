import { IBank } from 'app/shared/model/bank.model';
import { ICity } from 'app/shared/model/city.model';
import { IIdType } from 'app/shared/model/id-type.model';

export interface IDirectPaymentBank {
  id?: number;
  destinyBankCode?: string;
  idTypeName?: string;
  idTypeCode?: string;
  cityCode?: string;
  departmentCode?: string;
  originBank?: IBank;
  destinyBank?: IBank;
  destinyCity?: ICity;
  idType?: IIdType;
}

export class DirectPaymentBank implements IDirectPaymentBank {
  constructor(
    public id?: number,
    public destinyBankCode?: string,
    public idTypeName?: string,
    public idTypeCode?: string,
    public cityCode?: string,
    public departmentCode?: string,
    public originBank?: IBank,
    public destinyBank?: IBank,
    public destinyCity?: ICity,
    public idType?: IIdType
  ) {}
}
