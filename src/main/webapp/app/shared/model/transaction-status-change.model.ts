import { Moment } from 'moment';
import { ITransaction } from 'app/shared/model/transaction.model';
import { IExtendedUser } from 'app/shared/model/extended-user.model';
import { Status } from 'app/shared/model/enumerations/status.model';

export interface ITransactionStatusChange {
  id?: number;
  dateCreated?: Moment;
  status?: Status;
  reason?: any;
  transaction?: ITransaction;
  user?: IExtendedUser;
}

export class TransactionStatusChange implements ITransactionStatusChange {
  constructor(
    public id?: number,
    public dateCreated?: Moment,
    public status?: Status,
    public reason?: any,
    public transaction?: ITransaction,
    public user?: IExtendedUser
  ) {}
}
