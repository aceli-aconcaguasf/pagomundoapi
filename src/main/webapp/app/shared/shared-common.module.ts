import { NgModule } from '@angular/core';

import { PagoMundoApiSharedLibsModule } from './';

@NgModule({
  imports: [PagoMundoApiSharedLibsModule],
  declarations: [],
  exports: [PagoMundoApiSharedLibsModule]
})
export class PagoMundoApiSharedCommonModule {}
