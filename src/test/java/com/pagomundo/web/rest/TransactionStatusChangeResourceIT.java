package com.pagomundo.web.rest;

import com.pagomundo.PagoMundoApiApp;
import com.pagomundo.domain.TransactionStatusChange;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.repository.TransactionStatusChangeRepository;
import com.pagomundo.repository.search.TransactionStatusChangeSearchRepository;
import com.pagomundo.service.TransactionStatusChangeService;
import com.pagomundo.service.TranslateService;
import com.pagomundo.web.rest.errors.ExceptionTranslator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;

import static com.pagomundo.web.rest.TestUtil.createFormattingConversionService;
import static com.pagomundo.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
/**
 * Integration tests for the {@link TransactionStatusChangeResource} REST controller.
 */
@SpringBootTest(classes = PagoMundoApiApp.class)
public class TransactionStatusChangeResourceIT {

    private static final ZonedDateTime DEFAULT_DATE_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_CREATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_DATE_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final Status DEFAULT_STATUS = Status.CREATED;
    private static final Status UPDATED_STATUS = Status.IN_PROCESS;

    private static final String DEFAULT_REASON = "AAAAAAAAAA";
    private static final String UPDATED_REASON = "BBBBBBBBBB";

    @Autowired
    private TransactionStatusChangeRepository transactionStatusChangeRepository;

    @Autowired
    private TransactionStatusChangeService transactionStatusChangeService;

    /**
     * This repository is mocked in the com.pagomundo.repository.search test package.
     *
     * @see com.pagomundo.repository.search.TransactionStatusChangeSearchRepositoryMockConfiguration
     */
    @Autowired
    private TransactionStatusChangeSearchRepository mockTransactionStatusChangeSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTransactionStatusChangeMockMvc;

    private TransactionStatusChange transactionStatusChange;

    @Autowired
    private TranslateService translateService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TransactionStatusChangeResource transactionStatusChangeResource = new TransactionStatusChangeResource(transactionStatusChangeService, translateService);
        this.restTransactionStatusChangeMockMvc = MockMvcBuilders.standaloneSetup(transactionStatusChangeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TransactionStatusChange createEntity(EntityManager em) {
        TransactionStatusChange transactionStatusChange = new TransactionStatusChange()
            .dateCreated(DEFAULT_DATE_CREATED)
            .status(DEFAULT_STATUS)
            .reason(DEFAULT_REASON);
        return transactionStatusChange;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TransactionStatusChange createUpdatedEntity(EntityManager em) {
        TransactionStatusChange transactionStatusChange = new TransactionStatusChange()
            .dateCreated(UPDATED_DATE_CREATED)
            .status(UPDATED_STATUS)
            .reason(UPDATED_REASON);
        return transactionStatusChange;
    }

    @BeforeEach
    public void initTest() {
        transactionStatusChange = createEntity(em);
    }

    @Test
    @Transactional
    public void createTransactionStatusChange() throws Exception {
        int databaseSizeBeforeCreate = transactionStatusChangeRepository.findAll().size();

        // Create the TransactionStatusChange
        restTransactionStatusChangeMockMvc.perform(post("/api/transaction-status-changes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transactionStatusChange)))
            .andExpect(status().isCreated());

        // Validate the TransactionStatusChange in the database
        List<TransactionStatusChange> transactionStatusChangeList = transactionStatusChangeRepository.findAll();
        assertThat(transactionStatusChangeList).hasSize(databaseSizeBeforeCreate + 1);
        TransactionStatusChange testTransactionStatusChange = transactionStatusChangeList.get(transactionStatusChangeList.size() - 1);
        assertThat(testTransactionStatusChange.getDateCreated()).isEqualTo(DEFAULT_DATE_CREATED);
        assertThat(testTransactionStatusChange.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testTransactionStatusChange.getReason()).isEqualTo(DEFAULT_REASON);

        // Validate the TransactionStatusChange in Elasticsearch
        verify(mockTransactionStatusChangeSearchRepository, times(1)).save(testTransactionStatusChange);
    }

    @Test
    @Transactional
    public void createTransactionStatusChangeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = transactionStatusChangeRepository.findAll().size();

        // Create the TransactionStatusChange with an existing ID
        transactionStatusChange.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTransactionStatusChangeMockMvc.perform(post("/api/transaction-status-changes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transactionStatusChange)))
            .andExpect(status().isBadRequest());

        // Validate the TransactionStatusChange in the database
        List<TransactionStatusChange> transactionStatusChangeList = transactionStatusChangeRepository.findAll();
        assertThat(transactionStatusChangeList).hasSize(databaseSizeBeforeCreate);

        // Validate the TransactionStatusChange in Elasticsearch
        verify(mockTransactionStatusChangeSearchRepository, times(0)).save(transactionStatusChange);
    }


    @Test
    @Transactional
    public void checkDateCreatedIsRequired() throws Exception {
        int databaseSizeBeforeTest = transactionStatusChangeRepository.findAll().size();
        // set the field null
        transactionStatusChange.setDateCreated(null);

        // Create the TransactionStatusChange, which fails.

        restTransactionStatusChangeMockMvc.perform(post("/api/transaction-status-changes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transactionStatusChange)))
            .andExpect(status().isBadRequest());

        List<TransactionStatusChange> transactionStatusChangeList = transactionStatusChangeRepository.findAll();
        assertThat(transactionStatusChangeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = transactionStatusChangeRepository.findAll().size();
        // set the field null
        transactionStatusChange.setStatus(null);

        // Create the TransactionStatusChange, which fails.

        restTransactionStatusChangeMockMvc.perform(post("/api/transaction-status-changes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transactionStatusChange)))
            .andExpect(status().isBadRequest());

        List<TransactionStatusChange> transactionStatusChangeList = transactionStatusChangeRepository.findAll();
        assertThat(transactionStatusChangeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTransactionStatusChanges() throws Exception {
        // Initialize the database
        transactionStatusChangeRepository.saveAndFlush(transactionStatusChange);

        // Get all the transactionStatusChangeList
        restTransactionStatusChangeMockMvc.perform(get("/api/transaction-status-changes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(transactionStatusChange.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateCreated").value(hasItem(sameInstant(DEFAULT_DATE_CREATED))))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].reason").value(hasItem(DEFAULT_REASON.toString())));
    }

    @Test
    @Transactional
    public void getTransactionStatusChange() throws Exception {
        // Initialize the database
        transactionStatusChangeRepository.saveAndFlush(transactionStatusChange);

        // Get the transactionStatusChange
        restTransactionStatusChangeMockMvc.perform(get("/api/transaction-status-changes/{id}", transactionStatusChange.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(transactionStatusChange.getId().intValue()))
            .andExpect(jsonPath("$.dateCreated").value(sameInstant(DEFAULT_DATE_CREATED)))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.reason").value(DEFAULT_REASON.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTransactionStatusChange() throws Exception {
        // Get the transactionStatusChange
        restTransactionStatusChangeMockMvc.perform(get("/api/transaction-status-changes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTransactionStatusChange() throws Exception {
        // Initialize the database
        transactionStatusChangeService.save(transactionStatusChange);
        // As the test used the service layer, reset the Elasticsearch mock repository
        reset(mockTransactionStatusChangeSearchRepository);

        int databaseSizeBeforeUpdate = transactionStatusChangeRepository.findAll().size();

        // Update the transactionStatusChange
        TransactionStatusChange updatedTransactionStatusChange = transactionStatusChangeRepository.findById(transactionStatusChange.getId()).get();
        // Disconnect from session so that the updates on updatedTransactionStatusChange are not directly saved in db
        em.detach(updatedTransactionStatusChange);
        updatedTransactionStatusChange
            .dateCreated(UPDATED_DATE_CREATED)
            .status(UPDATED_STATUS)
            .reason(UPDATED_REASON);

        restTransactionStatusChangeMockMvc.perform(put("/api/transaction-status-changes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTransactionStatusChange)))
            .andExpect(status().isOk());

        // Validate the TransactionStatusChange in the database
        List<TransactionStatusChange> transactionStatusChangeList = transactionStatusChangeRepository.findAll();
        assertThat(transactionStatusChangeList).hasSize(databaseSizeBeforeUpdate);
        TransactionStatusChange testTransactionStatusChange = transactionStatusChangeList.get(transactionStatusChangeList.size() - 1);
        assertThat(testTransactionStatusChange.getDateCreated()).isEqualTo(UPDATED_DATE_CREATED);
        assertThat(testTransactionStatusChange.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testTransactionStatusChange.getReason()).isEqualTo(UPDATED_REASON);

        // Validate the TransactionStatusChange in Elasticsearch
        verify(mockTransactionStatusChangeSearchRepository, times(1)).save(testTransactionStatusChange);
    }

    @Test
    @Transactional
    public void updateNonExistingTransactionStatusChange() throws Exception {
        int databaseSizeBeforeUpdate = transactionStatusChangeRepository.findAll().size();

        // Create the TransactionStatusChange

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTransactionStatusChangeMockMvc.perform(put("/api/transaction-status-changes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transactionStatusChange)))
            .andExpect(status().isBadRequest());

        // Validate the TransactionStatusChange in the database
        List<TransactionStatusChange> transactionStatusChangeList = transactionStatusChangeRepository.findAll();
        assertThat(transactionStatusChangeList).hasSize(databaseSizeBeforeUpdate);

        // Validate the TransactionStatusChange in Elasticsearch
        verify(mockTransactionStatusChangeSearchRepository, times(0)).save(transactionStatusChange);
    }

    @Test
    @Transactional
    public void deleteTransactionStatusChange() throws Exception {
        // Initialize the database
        transactionStatusChangeService.save(transactionStatusChange);

        int databaseSizeBeforeDelete = transactionStatusChangeRepository.findAll().size();

        // Delete the transactionStatusChange
        restTransactionStatusChangeMockMvc.perform(delete("/api/transaction-status-changes/{id}", transactionStatusChange.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TransactionStatusChange> transactionStatusChangeList = transactionStatusChangeRepository.findAll();
        assertThat(transactionStatusChangeList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the TransactionStatusChange in Elasticsearch
        verify(mockTransactionStatusChangeSearchRepository, times(1)).deleteById(transactionStatusChange.getId());
    }

    @Test
    @Transactional
    public void searchTransactionStatusChange() throws Exception {
        // Initialize the database
        transactionStatusChangeService.save(transactionStatusChange);
        when(mockTransactionStatusChangeSearchRepository.search(queryStringQuery("id:" + transactionStatusChange.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(transactionStatusChange), PageRequest.of(0, 1), 1));
        // Search the transactionStatusChange
        restTransactionStatusChangeMockMvc.perform(get("/api/_search/transaction-status-changes?query=id:" + transactionStatusChange.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(transactionStatusChange.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateCreated").value(hasItem(sameInstant(DEFAULT_DATE_CREATED))))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].reason").value(hasItem(DEFAULT_REASON.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TransactionStatusChange.class);
        TransactionStatusChange transactionStatusChange1 = new TransactionStatusChange();
        transactionStatusChange1.setId(1L);
        TransactionStatusChange transactionStatusChange2 = new TransactionStatusChange();
        transactionStatusChange2.setId(transactionStatusChange1.getId());
        assertThat(transactionStatusChange1).isEqualTo(transactionStatusChange2);
        transactionStatusChange2.setId(2L);
        assertThat(transactionStatusChange1).isNotEqualTo(transactionStatusChange2);
        transactionStatusChange1.setId(null);
        assertThat(transactionStatusChange1).isNotEqualTo(transactionStatusChange2);
    }
}
