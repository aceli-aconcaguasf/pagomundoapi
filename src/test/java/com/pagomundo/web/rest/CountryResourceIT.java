package com.pagomundo.web.rest;

import com.pagomundo.PagoMundoApiApp;
import com.pagomundo.domain.Country;
import com.pagomundo.repository.CountryRepository;
import com.pagomundo.repository.search.CountrySearchRepository;
import com.pagomundo.service.CountryService;
import com.pagomundo.service.ExtendedUserService;
import com.pagomundo.service.TranslateService;
import com.pagomundo.web.rest.errors.ExceptionTranslator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;

import static com.pagomundo.web.rest.TestUtil.createFormattingConversionService;
import static com.pagomundo.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration tests for the {@link CountryResource} REST controller.
 */
@SpringBootTest(classes = PagoMundoApiApp.class)
class CountryResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_SPANISH_NAME = "AAAAAAAAAA";
    private static final String UPDATED_SPANISH_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_SUB_REGION_NAME = "AAAAAAAAAA";
    private static final String UPDATED_SUB_REGION_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_REGION_NAME = "AAAAAAAAAA";
    private static final String UPDATED_REGION_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_FOR_PAYEE = false;
    private static final Boolean UPDATED_FOR_PAYEE = true;

    private static final String DEFAULT_CURRENCY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CURRENCY_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CURRENCY_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CURRENCY_CODE = "BBBBBBBBBB";

    private static final Float DEFAULT_CURRENCY_VALUE = 1F;
    private static final Float UPDATED_CURRENCY_VALUE = 2F;
    private static final Float SMALLER_CURRENCY_VALUE = 1F - 1F;

    private static final String DEFAULT_CURRENCY_BASE = "AAAAAAAAAA";
    private static final String UPDATED_CURRENCY_BASE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CURRENCY_LAST_UPDATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CURRENCY_LAST_UPDATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_CURRENCY_LAST_UPDATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private CountryService countryService;

    @Autowired
    private ExtendedUserService extendedUserService;

    /**
     * This repository is mocked in the com.pagomundo.repository.search test package.
     *
     * @see com.pagomundo.repository.search.CountrySearchRepositoryMockConfiguration
     */
    @Autowired
    private CountrySearchRepository mockCountrySearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restCountryMockMvc;

    private Country country;

    @Autowired
    private TranslateService translateService;

    @BeforeEach
    void setup() {
        MockitoAnnotations.initMocks(this);
        final CountryResource countryResource = new CountryResource(countryService, extendedUserService, translateService);
        this.restCountryMockMvc = MockMvcBuilders.standaloneSetup(countryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Country createEntity(EntityManager em) {
        Country country = new Country()
            .name(DEFAULT_NAME)
            .spanishName(DEFAULT_SPANISH_NAME)
            .subRegionName(DEFAULT_SUB_REGION_NAME)
            .regionName(DEFAULT_REGION_NAME)
            .code(DEFAULT_CODE)
            .forPayee(DEFAULT_FOR_PAYEE)
            .currencyName(DEFAULT_CURRENCY_NAME)
            .currencyCode(DEFAULT_CURRENCY_CODE)
            .currencyValue(DEFAULT_CURRENCY_VALUE)
            .currencyBase(DEFAULT_CURRENCY_BASE)
            .currencyLastUpdated(DEFAULT_CURRENCY_LAST_UPDATED);
        return country;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Country createUpdatedEntity(EntityManager em) {
        Country country = new Country()
            .name(UPDATED_NAME)
            .spanishName(UPDATED_SPANISH_NAME)
            .subRegionName(UPDATED_SUB_REGION_NAME)
            .regionName(UPDATED_REGION_NAME)
            .code(UPDATED_CODE)
            .forPayee(UPDATED_FOR_PAYEE)
            .currencyName(UPDATED_CURRENCY_NAME)
            .currencyCode(UPDATED_CURRENCY_CODE)
            .currencyValue(UPDATED_CURRENCY_VALUE)
            .currencyBase(UPDATED_CURRENCY_BASE)
            .currencyLastUpdated(UPDATED_CURRENCY_LAST_UPDATED);
        return country;
    }

    @BeforeEach
    void initTest() {
        country = createEntity(em);
    }

    @Test
    @Transactional
    void createCountry() throws Exception {
        int databaseSizeBeforeCreate = countryRepository.findAll().size();

        // Create the Country
        restCountryMockMvc.perform(post("/api/countries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(country)))
            .andExpect(status().isCreated());

        // Validate the Country in the database
        List<Country> countryList = countryRepository.findAll();
        assertThat(countryList).hasSize(databaseSizeBeforeCreate + 1);
        Country testCountry = countryList.get(countryList.size() - 1);
        assertThat(testCountry.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCountry.getSpanishName()).isEqualTo(DEFAULT_SPANISH_NAME);
        assertThat(testCountry.getSubRegionName()).isEqualTo(DEFAULT_SUB_REGION_NAME);
        assertThat(testCountry.getRegionName()).isEqualTo(DEFAULT_REGION_NAME);
        assertThat(testCountry.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testCountry.isForPayee()).isEqualTo(DEFAULT_FOR_PAYEE);
        assertThat(testCountry.getCurrencyName()).isEqualTo(DEFAULT_CURRENCY_NAME);
        assertThat(testCountry.getCurrencyCode()).isEqualTo(DEFAULT_CURRENCY_CODE);
        assertThat(testCountry.getCurrencyValue()).isEqualTo(DEFAULT_CURRENCY_VALUE);
        assertThat(testCountry.getCurrencyBase()).isEqualTo(DEFAULT_CURRENCY_BASE);
        assertThat(testCountry.getCurrencyLastUpdated()).isEqualTo(DEFAULT_CURRENCY_LAST_UPDATED);

        // Validate the Country in Elasticsearch
        verify(mockCountrySearchRepository, times(1)).save(testCountry);
    }

    @Test
    @Transactional
    void createCountryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = countryRepository.findAll().size();

        // Create the Country with an existing ID
        country.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCountryMockMvc.perform(post("/api/countries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(country)))
            .andExpect(status().isBadRequest());

        // Validate the Country in the database
        List<Country> countryList = countryRepository.findAll();
        assertThat(countryList).hasSize(databaseSizeBeforeCreate);

        // Validate the Country in Elasticsearch
        verify(mockCountrySearchRepository, times(0)).save(country);
    }


    @Test
    @Transactional
    void getAllCountries() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get all the countryList
        restCountryMockMvc.perform(get("/api/countries?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(country.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].spanishName").value(hasItem(DEFAULT_SPANISH_NAME)))
            .andExpect(jsonPath("$.[*].subRegionName").value(hasItem(DEFAULT_SUB_REGION_NAME)))
            .andExpect(jsonPath("$.[*].regionName").value(hasItem(DEFAULT_REGION_NAME)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].forPayee").value(hasItem(DEFAULT_FOR_PAYEE.booleanValue())))
            .andExpect(jsonPath("$.[*].currencyName").value(hasItem(DEFAULT_CURRENCY_NAME)))
            .andExpect(jsonPath("$.[*].currencyCode").value(hasItem(DEFAULT_CURRENCY_CODE)))
            .andExpect(jsonPath("$.[*].currencyValue").value(hasItem(DEFAULT_CURRENCY_VALUE.doubleValue())))
            .andExpect(jsonPath("$.[*].currencyBase").value(hasItem(DEFAULT_CURRENCY_BASE)))
            .andExpect(jsonPath("$.[*].currencyLastUpdated").value(hasItem(sameInstant(DEFAULT_CURRENCY_LAST_UPDATED))));
    }

    @Test
    @Transactional
    void getCountry() throws Exception {
        // Initialize the database
        countryRepository.saveAndFlush(country);

        // Get the country
        restCountryMockMvc.perform(get("/api/countries/{id}", country.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(country.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.spanishName").value(DEFAULT_SPANISH_NAME))
            .andExpect(jsonPath("$.subRegionName").value(DEFAULT_SUB_REGION_NAME))
            .andExpect(jsonPath("$.regionName").value(DEFAULT_REGION_NAME))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.forPayee").value(DEFAULT_FOR_PAYEE.booleanValue()))
            .andExpect(jsonPath("$.currencyName").value(DEFAULT_CURRENCY_NAME))
            .andExpect(jsonPath("$.currencyCode").value(DEFAULT_CURRENCY_CODE))
            .andExpect(jsonPath("$.currencyValue").value(DEFAULT_CURRENCY_VALUE.doubleValue()))
            .andExpect(jsonPath("$.currencyBase").value(DEFAULT_CURRENCY_BASE))
            .andExpect(jsonPath("$.currencyLastUpdated").value(sameInstant(DEFAULT_CURRENCY_LAST_UPDATED)));
    }

    @Test
    @Transactional
    void getNonExistingCountry() throws Exception {
        // Get the country
        restCountryMockMvc.perform(get("/api/countries/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void updateCountry() throws Exception {
        // Initialize the database
        countryService.save(country);
        // As the test used the service layer, reset the Elasticsearch mock repository
        reset(mockCountrySearchRepository);

        int databaseSizeBeforeUpdate = countryRepository.findAll().size();

        // Update the country
        Country updatedCountry = countryRepository.findById(country.getId()).get();
        // Disconnect from session so that the updates on updatedCountry are not directly saved in db
        em.detach(updatedCountry);
        updatedCountry
            .name(UPDATED_NAME)
            .spanishName(UPDATED_SPANISH_NAME)
            .subRegionName(UPDATED_SUB_REGION_NAME)
            .regionName(UPDATED_REGION_NAME)
            .code(UPDATED_CODE)
            .forPayee(UPDATED_FOR_PAYEE)
            .currencyName(UPDATED_CURRENCY_NAME)
            .currencyCode(UPDATED_CURRENCY_CODE)
            .currencyValue(UPDATED_CURRENCY_VALUE)
            .currencyBase(UPDATED_CURRENCY_BASE)
            .currencyLastUpdated(UPDATED_CURRENCY_LAST_UPDATED);

        restCountryMockMvc.perform(put("/api/countries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCountry)))
            .andExpect(status().isOk());

        // Validate the Country in the database
        List<Country> countryList = countryRepository.findAll();
        assertThat(countryList).hasSize(databaseSizeBeforeUpdate);
        Country testCountry = countryList.get(countryList.size() - 1);
        assertThat(testCountry.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCountry.getSpanishName()).isEqualTo(UPDATED_SPANISH_NAME);
        assertThat(testCountry.getSubRegionName()).isEqualTo(UPDATED_SUB_REGION_NAME);
        assertThat(testCountry.getRegionName()).isEqualTo(UPDATED_REGION_NAME);
        assertThat(testCountry.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testCountry.isForPayee()).isEqualTo(UPDATED_FOR_PAYEE);
        assertThat(testCountry.getCurrencyName()).isEqualTo(UPDATED_CURRENCY_NAME);
        assertThat(testCountry.getCurrencyCode()).isEqualTo(UPDATED_CURRENCY_CODE);
        assertThat(testCountry.getCurrencyValue()).isEqualTo(UPDATED_CURRENCY_VALUE);
        assertThat(testCountry.getCurrencyBase()).isEqualTo(UPDATED_CURRENCY_BASE);
        assertThat(testCountry.getCurrencyLastUpdated()).isEqualTo(UPDATED_CURRENCY_LAST_UPDATED);

        // Validate the Country in Elasticsearch
        verify(mockCountrySearchRepository, times(1)).save(testCountry);
    }

    @Test
    @Transactional
    void updateNonExistingCountry() throws Exception {
        int databaseSizeBeforeUpdate = countryRepository.findAll().size();

        // Create the Country

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCountryMockMvc.perform(put("/api/countries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(country)))
            .andExpect(status().isBadRequest());

        // Validate the Country in the database
        List<Country> countryList = countryRepository.findAll();
        assertThat(countryList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Country in Elasticsearch
        verify(mockCountrySearchRepository, times(0)).save(country);
    }

    @Test
    @Transactional
    void deleteCountry() throws Exception {
        // Initialize the database
        countryService.save(country);

        int databaseSizeBeforeDelete = countryRepository.findAll().size();

        // Delete the country
        restCountryMockMvc.perform(delete("/api/countries/{id}", country.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Country> countryList = countryRepository.findAll();
        assertThat(countryList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Country in Elasticsearch
        verify(mockCountrySearchRepository, times(1)).deleteById(country.getId());
    }

    @Test
    @Transactional
    void searchCountry() throws Exception {
        // Initialize the database
        countryService.save(country);
        when(mockCountrySearchRepository.search(queryStringQuery("id:" + country.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(country), PageRequest.of(0, 1), 1));
        // Search the country
        restCountryMockMvc.perform(get("/api/_search/countries?query=id:" + country.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(country.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].spanishName").value(hasItem(DEFAULT_SPANISH_NAME)))
            .andExpect(jsonPath("$.[*].subRegionName").value(hasItem(DEFAULT_SUB_REGION_NAME)))
            .andExpect(jsonPath("$.[*].regionName").value(hasItem(DEFAULT_REGION_NAME)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].forPayee").value(hasItem(DEFAULT_FOR_PAYEE.booleanValue())))
            .andExpect(jsonPath("$.[*].currencyName").value(hasItem(DEFAULT_CURRENCY_NAME)))
            .andExpect(jsonPath("$.[*].currencyCode").value(hasItem(DEFAULT_CURRENCY_CODE)))
            .andExpect(jsonPath("$.[*].currencyValue").value(hasItem(DEFAULT_CURRENCY_VALUE.doubleValue())))
            .andExpect(jsonPath("$.[*].currencyBase").value(hasItem(DEFAULT_CURRENCY_BASE)))
            .andExpect(jsonPath("$.[*].currencyLastUpdated").value(hasItem(sameInstant(DEFAULT_CURRENCY_LAST_UPDATED))));
    }

    @Test
    @Transactional
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Country.class);
        Country country1 = new Country();
        country1.setId(1L);
        Country country2 = new Country();
        country2.setId(country1.getId());
        assertThat(country1).isEqualTo(country2);
        country2.setId(2L);
        assertThat(country1).isNotEqualTo(country2);
        country1.setId(null);
        assertThat(country1).isNotEqualTo(country2);
    }
}
