package com.pagomundo.web.rest;

import com.pagomundo.PagoMundoApiApp;
import com.pagomundo.domain.ExtendedUserStatusChange;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.repository.ExtendedUserStatusChangeRepository;
import com.pagomundo.repository.search.ExtendedUserStatusChangeSearchRepository;
import com.pagomundo.service.ExtendedUserStatusChangeService;
import com.pagomundo.service.TranslateService;
import com.pagomundo.web.rest.errors.ExceptionTranslator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;

import static com.pagomundo.web.rest.TestUtil.createFormattingConversionService;
import static com.pagomundo.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration tests for the {@link ExtendedUserStatusChangeResource} REST controller.
 */
@SpringBootTest(classes = PagoMundoApiApp.class)
public class ExtendedUserStatusChangeResourceIT {

    private static final ZonedDateTime DEFAULT_DATE_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_CREATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_DATE_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final Status DEFAULT_STATUS = Status.CREATED;
    private static final Status UPDATED_STATUS = Status.IN_PROCESS;

    private static final String DEFAULT_STATUS_REASON = "AAAAAAAAAA";
    private static final String UPDATED_STATUS_REASON = "BBBBBBBBBB";

    @Autowired
    private ExtendedUserStatusChangeRepository extendedUserStatusChangeRepository;

    @Autowired
    private ExtendedUserStatusChangeService extendedUserStatusChangeService;

    /**
     * This repository is mocked in the com.pagomundo.repository.search test package.
     *
     * @see com.pagomundo.repository.search.ExtendedUserStatusChangeSearchRepositoryMockConfiguration
     */
    @Autowired
    private ExtendedUserStatusChangeSearchRepository mockExtendedUserStatusChangeSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restExtendedUserStatusChangeMockMvc;

    private ExtendedUserStatusChange extendedUserStatusChange;

    @Autowired
    private TranslateService translateService;

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExtendedUserStatusChange createEntity(EntityManager em) {
        ExtendedUserStatusChange extendedUserStatusChange = new ExtendedUserStatusChange()
            .dateCreated(DEFAULT_DATE_CREATED)
            .status(DEFAULT_STATUS)
            .statusReason(DEFAULT_STATUS_REASON);
        return extendedUserStatusChange;
    }

    /**
     * Create an updated entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExtendedUserStatusChange createUpdatedEntity(EntityManager em) {
        ExtendedUserStatusChange extendedUserStatusChange = new ExtendedUserStatusChange()
            .dateCreated(UPDATED_DATE_CREATED)
            .status(UPDATED_STATUS)
            .statusReason(UPDATED_STATUS_REASON);
        return extendedUserStatusChange;
    }

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ExtendedUserStatusChangeResource extendedUserStatusChangeResource = new ExtendedUserStatusChangeResource(extendedUserStatusChangeService, translateService);
        this.restExtendedUserStatusChangeMockMvc = MockMvcBuilders.standaloneSetup(extendedUserStatusChangeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    @BeforeEach
    public void initTest() {
        extendedUserStatusChange = createEntity(em);
    }

    @Test
    @Transactional
    public void createExtendedUserStatusChange() throws Exception {
        int databaseSizeBeforeCreate = extendedUserStatusChangeRepository.findAll().size();

        // Create the ExtendedUserStatusChange
        restExtendedUserStatusChangeMockMvc.perform(post("/api/extended-user-status-changes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(extendedUserStatusChange)))
            .andExpect(status().isCreated());

        // Validate the ExtendedUserStatusChange in the database
        List<ExtendedUserStatusChange> extendedUserStatusChangeList = extendedUserStatusChangeRepository.findAll();
        assertThat(extendedUserStatusChangeList).hasSize(databaseSizeBeforeCreate + 1);
        ExtendedUserStatusChange testExtendedUserStatusChange = extendedUserStatusChangeList.get(extendedUserStatusChangeList.size() - 1);
        assertThat(testExtendedUserStatusChange.getDateCreated()).isEqualTo(DEFAULT_DATE_CREATED);
        assertThat(testExtendedUserStatusChange.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testExtendedUserStatusChange.getReason()).isEqualTo(DEFAULT_STATUS_REASON);

        // Validate the ExtendedUserStatusChange in Elasticsearch
        verify(mockExtendedUserStatusChangeSearchRepository, times(1)).save(testExtendedUserStatusChange);
    }

    @Test
    @Transactional
    public void createExtendedUserStatusChangeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = extendedUserStatusChangeRepository.findAll().size();

        // Create the ExtendedUserStatusChange with an existing ID
        extendedUserStatusChange.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restExtendedUserStatusChangeMockMvc.perform(post("/api/extended-user-status-changes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(extendedUserStatusChange)))
            .andExpect(status().isBadRequest());

        // Validate the ExtendedUserStatusChange in the database
        List<ExtendedUserStatusChange> extendedUserStatusChangeList = extendedUserStatusChangeRepository.findAll();
        assertThat(extendedUserStatusChangeList).hasSize(databaseSizeBeforeCreate);

        // Validate the ExtendedUserStatusChange in Elasticsearch
        verify(mockExtendedUserStatusChangeSearchRepository, times(0)).save(extendedUserStatusChange);
    }


    @Test
    @Transactional
    public void getAllExtendedUserStatusChanges() throws Exception {
        // Initialize the database
        extendedUserStatusChangeRepository.saveAndFlush(extendedUserStatusChange);

        // Get all the extendedUserStatusChangeList
        restExtendedUserStatusChangeMockMvc.perform(get("/api/extended-user-status-changes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(extendedUserStatusChange.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateCreated").value(hasItem(sameInstant(DEFAULT_DATE_CREATED))))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].statusReason").value(hasItem(DEFAULT_STATUS_REASON.toString())));
    }

    @Test
    @Transactional
    public void getExtendedUserStatusChange() throws Exception {
        // Initialize the database
        extendedUserStatusChangeRepository.saveAndFlush(extendedUserStatusChange);

        // Get the extendedUserStatusChange
        restExtendedUserStatusChangeMockMvc.perform(get("/api/extended-user-status-changes/{id}", extendedUserStatusChange.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(extendedUserStatusChange.getId().intValue()))
            .andExpect(jsonPath("$.dateCreated").value(sameInstant(DEFAULT_DATE_CREATED)))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.statusReason").value(DEFAULT_STATUS_REASON.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingExtendedUserStatusChange() throws Exception {
        // Get the extendedUserStatusChange
        restExtendedUserStatusChangeMockMvc.perform(get("/api/extended-user-status-changes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateExtendedUserStatusChange() throws Exception {
        // Initialize the database
        extendedUserStatusChangeService.save(extendedUserStatusChange);
        // As the test used the service layer, reset the Elasticsearch mock repository
        reset(mockExtendedUserStatusChangeSearchRepository);

        int databaseSizeBeforeUpdate = extendedUserStatusChangeRepository.findAll().size();

        // Update the extendedUserStatusChange
        ExtendedUserStatusChange updatedExtendedUserStatusChange = extendedUserStatusChangeRepository.findById(extendedUserStatusChange.getId()).get();
        // Disconnect from session so that the updates on updatedExtendedUserStatusChange are not directly saved in db
        em.detach(updatedExtendedUserStatusChange);
        updatedExtendedUserStatusChange
            .dateCreated(UPDATED_DATE_CREATED)
            .status(UPDATED_STATUS)
            .statusReason(UPDATED_STATUS_REASON);

        restExtendedUserStatusChangeMockMvc.perform(put("/api/extended-user-status-changes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedExtendedUserStatusChange)))
            .andExpect(status().isOk());

        // Validate the ExtendedUserStatusChange in the database
        List<ExtendedUserStatusChange> extendedUserStatusChangeList = extendedUserStatusChangeRepository.findAll();
        assertThat(extendedUserStatusChangeList).hasSize(databaseSizeBeforeUpdate);
        ExtendedUserStatusChange testExtendedUserStatusChange = extendedUserStatusChangeList.get(extendedUserStatusChangeList.size() - 1);
        assertThat(testExtendedUserStatusChange.getDateCreated()).isEqualTo(UPDATED_DATE_CREATED);
        assertThat(testExtendedUserStatusChange.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testExtendedUserStatusChange.getReason()).isEqualTo(UPDATED_STATUS_REASON);

        // Validate the ExtendedUserStatusChange in Elasticsearch
        verify(mockExtendedUserStatusChangeSearchRepository, times(1)).save(testExtendedUserStatusChange);
    }

    @Test
    @Transactional
    public void updateNonExistingExtendedUserStatusChange() throws Exception {
        int databaseSizeBeforeUpdate = extendedUserStatusChangeRepository.findAll().size();

        // Create the ExtendedUserStatusChange

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExtendedUserStatusChangeMockMvc.perform(put("/api/extended-user-status-changes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(extendedUserStatusChange)))
            .andExpect(status().isBadRequest());

        // Validate the ExtendedUserStatusChange in the database
        List<ExtendedUserStatusChange> extendedUserStatusChangeList = extendedUserStatusChangeRepository.findAll();
        assertThat(extendedUserStatusChangeList).hasSize(databaseSizeBeforeUpdate);

        // Validate the ExtendedUserStatusChange in Elasticsearch
        verify(mockExtendedUserStatusChangeSearchRepository, times(0)).save(extendedUserStatusChange);
    }

    @Test
    @Transactional
    public void deleteExtendedUserStatusChange() throws Exception {
        // Initialize the database
        extendedUserStatusChangeService.save(extendedUserStatusChange);

        int databaseSizeBeforeDelete = extendedUserStatusChangeRepository.findAll().size();

        // Delete the extendedUserStatusChange
        restExtendedUserStatusChangeMockMvc.perform(delete("/api/extended-user-status-changes/{id}", extendedUserStatusChange.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ExtendedUserStatusChange> extendedUserStatusChangeList = extendedUserStatusChangeRepository.findAll();
        assertThat(extendedUserStatusChangeList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the ExtendedUserStatusChange in Elasticsearch
        verify(mockExtendedUserStatusChangeSearchRepository, times(1)).deleteById(extendedUserStatusChange.getId());
    }

    @Test
    @Transactional
    public void searchExtendedUserStatusChange() throws Exception {
        // Initialize the database
        extendedUserStatusChangeService.save(extendedUserStatusChange);
        when(mockExtendedUserStatusChangeSearchRepository.search(queryStringQuery("id:" + extendedUserStatusChange.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(extendedUserStatusChange), PageRequest.of(0, 1), 1));
        // Search the extendedUserStatusChange
        restExtendedUserStatusChangeMockMvc.perform(get("/api/_search/extended-user-status-changes?query=id:" + extendedUserStatusChange.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(extendedUserStatusChange.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateCreated").value(hasItem(sameInstant(DEFAULT_DATE_CREATED))))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].statusReason").value(hasItem(DEFAULT_STATUS_REASON.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExtendedUserStatusChange.class);
        ExtendedUserStatusChange extendedUserStatusChange1 = new ExtendedUserStatusChange();
        extendedUserStatusChange1.setId(1L);
        ExtendedUserStatusChange extendedUserStatusChange2 = new ExtendedUserStatusChange();
        extendedUserStatusChange2.setId(extendedUserStatusChange1.getId());
        assertThat(extendedUserStatusChange1).isEqualTo(extendedUserStatusChange2);
        extendedUserStatusChange2.setId(2L);
        assertThat(extendedUserStatusChange1).isNotEqualTo(extendedUserStatusChange2);
        extendedUserStatusChange1.setId(null);
        assertThat(extendedUserStatusChange1).isNotEqualTo(extendedUserStatusChange2);
    }
}
