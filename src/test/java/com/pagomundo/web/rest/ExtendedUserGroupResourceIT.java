package com.pagomundo.web.rest;

import com.pagomundo.PagoMundoApiApp;
import com.pagomundo.domain.ExtendedUserGroup;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.repository.ExtendedUserGroupRepository;
import com.pagomundo.repository.search.ExtendedUserGroupSearchRepository;
import com.pagomundo.service.ExtendedUserGroupService;
import com.pagomundo.service.ExtendedUserService;
import com.pagomundo.service.NotificationReceiverService;
import com.pagomundo.service.TranslateService;
import com.pagomundo.web.rest.errors.ExceptionTranslator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;

import static com.pagomundo.web.rest.TestUtil.createFormattingConversionService;
import static com.pagomundo.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration tests for the {@link ExtendedUserGroupResource} REST controller.
 */
@SpringBootTest(classes = PagoMundoApiApp.class)
public class ExtendedUserGroupResourceIT {

    private static final ZonedDateTime DEFAULT_DATE_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_CREATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_DATE_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final Status DEFAULT_STATUS = Status.CREATED;
    private static final Status UPDATED_STATUS = Status.IN_PROCESS;

    private static final String DEFAULT_FILE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FILE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_FILE_NAME_FROM_BANK = "AAAAAAAAAA";
    private static final String UPDATED_FILE_NAME_FROM_BANK = "BBBBBBBBBB";

    @Autowired
    private ExtendedUserGroupRepository extendedUserGroupRepository;

    @Autowired
    private ExtendedUserGroupService extendedUserGroupService;

    @Autowired
    private ExtendedUserService extendedUserService;

    @Autowired
    private NotificationReceiverService notificationReceiverService;

    /**
     * This repository is mocked in the com.pagomundo.repository.search test package.
     *
     * @see com.pagomundo.repository.search.ExtendedUserGroupSearchRepositoryMockConfiguration
     */
    @Autowired
    private ExtendedUserGroupSearchRepository mockExtendedUserGroupSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restExtendedUserGroupMockMvc;

    private ExtendedUserGroup extendedUserGroup;

    @Autowired
    private TranslateService translateService;

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExtendedUserGroup createEntity(EntityManager em) {
        ExtendedUserGroup extendedUserGroup = new ExtendedUserGroup()
            .dateCreated(DEFAULT_DATE_CREATED)
            .status(DEFAULT_STATUS)
            .fileName(DEFAULT_FILE_NAME)
            .fileNameFromBank(DEFAULT_FILE_NAME_FROM_BANK);
        return extendedUserGroup;
    }

    /**
     * Create an updated entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExtendedUserGroup createUpdatedEntity(EntityManager em) {
        ExtendedUserGroup extendedUserGroup = new ExtendedUserGroup()
            .dateCreated(UPDATED_DATE_CREATED)
            .status(UPDATED_STATUS)
            .fileName(UPDATED_FILE_NAME)
            .fileNameFromBank(UPDATED_FILE_NAME_FROM_BANK);
        return extendedUserGroup;
    }

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ExtendedUserGroupResource extendedUserGroupResource = new ExtendedUserGroupResource(extendedUserGroupService, extendedUserService, notificationReceiverService, translateService);
        this.restExtendedUserGroupMockMvc = MockMvcBuilders.standaloneSetup(extendedUserGroupResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    @BeforeEach
    public void initTest() {
        extendedUserGroup = createEntity(em);
    }

    @Test
    @Transactional
    public void createExtendedUserGroup() throws Exception {
        int databaseSizeBeforeCreate = extendedUserGroupRepository.findAll().size();

        // Create the ExtendedUserGroup
        restExtendedUserGroupMockMvc.perform(post("/api/extended-user-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(extendedUserGroup)))
            .andExpect(status().isCreated());

        // Validate the ExtendedUserGroup in the database
        List<ExtendedUserGroup> extendedUserGroupList = extendedUserGroupRepository.findAll();
        assertThat(extendedUserGroupList).hasSize(databaseSizeBeforeCreate + 1);
        ExtendedUserGroup testExtendedUserGroup = extendedUserGroupList.get(extendedUserGroupList.size() - 1);
        assertThat(testExtendedUserGroup.getDateCreated()).isEqualTo(DEFAULT_DATE_CREATED);
        assertThat(testExtendedUserGroup.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testExtendedUserGroup.getFileName()).isEqualTo(DEFAULT_FILE_NAME);
        assertThat(testExtendedUserGroup.getFileNameFromBank()).isEqualTo(DEFAULT_FILE_NAME_FROM_BANK);

        // Validate the ExtendedUserGroup in Elasticsearch
        verify(mockExtendedUserGroupSearchRepository, times(1)).save(testExtendedUserGroup);
    }

    @Test
    @Transactional
    public void createExtendedUserGroupWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = extendedUserGroupRepository.findAll().size();

        // Create the ExtendedUserGroup with an existing ID
        extendedUserGroup.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restExtendedUserGroupMockMvc.perform(post("/api/extended-user-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(extendedUserGroup)))
            .andExpect(status().isBadRequest());

        // Validate the ExtendedUserGroup in the database
        List<ExtendedUserGroup> extendedUserGroupList = extendedUserGroupRepository.findAll();
        assertThat(extendedUserGroupList).hasSize(databaseSizeBeforeCreate);

        // Validate the ExtendedUserGroup in Elasticsearch
        verify(mockExtendedUserGroupSearchRepository, times(0)).save(extendedUserGroup);
    }


    @Test
    @Transactional
    public void getAllExtendedUserGroups() throws Exception {
        // Initialize the database
        extendedUserGroupRepository.saveAndFlush(extendedUserGroup);

        // Get all the extendedUserGroupList
        restExtendedUserGroupMockMvc.perform(get("/api/extended-user-groups?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(extendedUserGroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateCreated").value(hasItem(sameInstant(DEFAULT_DATE_CREATED))))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].fileName").value(hasItem(DEFAULT_FILE_NAME.toString())))
            .andExpect(jsonPath("$.[*].fileNameFromBank").value(hasItem(DEFAULT_FILE_NAME_FROM_BANK.toString())));
    }

    @Test
    @Transactional
    public void getExtendedUserGroup() throws Exception {
        // Initialize the database
        extendedUserGroupRepository.saveAndFlush(extendedUserGroup);

        // Get the extendedUserGroup
        restExtendedUserGroupMockMvc.perform(get("/api/extended-user-groups/{id}", extendedUserGroup.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(extendedUserGroup.getId().intValue()))
            .andExpect(jsonPath("$.dateCreated").value(sameInstant(DEFAULT_DATE_CREATED)))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.fileName").value(DEFAULT_FILE_NAME.toString()))
            .andExpect(jsonPath("$.fileNameFromBank").value(DEFAULT_FILE_NAME_FROM_BANK.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingExtendedUserGroup() throws Exception {
        // Get the extendedUserGroup
        restExtendedUserGroupMockMvc.perform(get("/api/extended-user-groups/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateExtendedUserGroup() throws Exception {
        // Initialize the database
        extendedUserGroupService.save(extendedUserGroup);
        // As the test used the service layer, reset the Elasticsearch mock repository
        reset(mockExtendedUserGroupSearchRepository);

        int databaseSizeBeforeUpdate = extendedUserGroupRepository.findAll().size();

        // Update the extendedUserGroup
        ExtendedUserGroup updatedExtendedUserGroup = extendedUserGroupRepository.findById(extendedUserGroup.getId()).get();
        // Disconnect from session so that the updates on updatedExtendedUserGroup are not directly saved in db
        em.detach(updatedExtendedUserGroup);
        updatedExtendedUserGroup
            .dateCreated(UPDATED_DATE_CREATED)
            .status(UPDATED_STATUS)
            .fileName(UPDATED_FILE_NAME)
            .fileNameFromBank(UPDATED_FILE_NAME_FROM_BANK);

        restExtendedUserGroupMockMvc.perform(put("/api/extended-user-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedExtendedUserGroup)))
            .andExpect(status().isOk());

        // Validate the ExtendedUserGroup in the database
        List<ExtendedUserGroup> extendedUserGroupList = extendedUserGroupRepository.findAll();
        assertThat(extendedUserGroupList).hasSize(databaseSizeBeforeUpdate);
        ExtendedUserGroup testExtendedUserGroup = extendedUserGroupList.get(extendedUserGroupList.size() - 1);
        assertThat(testExtendedUserGroup.getDateCreated()).isEqualTo(UPDATED_DATE_CREATED);
        assertThat(testExtendedUserGroup.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testExtendedUserGroup.getFileName()).isEqualTo(UPDATED_FILE_NAME);
        assertThat(testExtendedUserGroup.getFileNameFromBank()).isEqualTo(UPDATED_FILE_NAME_FROM_BANK);

        // Validate the ExtendedUserGroup in Elasticsearch
        verify(mockExtendedUserGroupSearchRepository, times(1)).save(testExtendedUserGroup);
    }

    @Test
    @Transactional
    public void updateNonExistingExtendedUserGroup() throws Exception {
        int databaseSizeBeforeUpdate = extendedUserGroupRepository.findAll().size();

        // Create the ExtendedUserGroup

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExtendedUserGroupMockMvc.perform(put("/api/extended-user-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(extendedUserGroup)))
            .andExpect(status().isBadRequest());

        // Validate the ExtendedUserGroup in the database
        List<ExtendedUserGroup> extendedUserGroupList = extendedUserGroupRepository.findAll();
        assertThat(extendedUserGroupList).hasSize(databaseSizeBeforeUpdate);

        // Validate the ExtendedUserGroup in Elasticsearch
        verify(mockExtendedUserGroupSearchRepository, times(0)).save(extendedUserGroup);
    }

    @Test
    @Transactional
    public void deleteExtendedUserGroup() throws Exception {
        // Initialize the database
        extendedUserGroupService.save(extendedUserGroup);

        int databaseSizeBeforeDelete = extendedUserGroupRepository.findAll().size();

        // Delete the extendedUserGroup
        restExtendedUserGroupMockMvc.perform(delete("/api/extended-user-groups/{id}", extendedUserGroup.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ExtendedUserGroup> extendedUserGroupList = extendedUserGroupRepository.findAll();
        assertThat(extendedUserGroupList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the ExtendedUserGroup in Elasticsearch
        verify(mockExtendedUserGroupSearchRepository, times(1)).deleteById(extendedUserGroup.getId());
    }

    @Test
    @Transactional
    public void searchExtendedUserGroup() throws Exception {
        // Initialize the database
        extendedUserGroupService.save(extendedUserGroup);
        when(mockExtendedUserGroupSearchRepository.search(queryStringQuery("id:" + extendedUserGroup.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(extendedUserGroup), PageRequest.of(0, 1), 1));
        // Search the extendedUserGroup
        restExtendedUserGroupMockMvc.perform(get("/api/_search/extended-user-groups?query=id:" + extendedUserGroup.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(extendedUserGroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateCreated").value(hasItem(sameInstant(DEFAULT_DATE_CREATED))))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].fileName").value(hasItem(DEFAULT_FILE_NAME)))
            .andExpect(jsonPath("$.[*].fileNameFromBank").value(hasItem(DEFAULT_FILE_NAME_FROM_BANK)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExtendedUserGroup.class);
        ExtendedUserGroup extendedUserGroup1 = new ExtendedUserGroup();
        extendedUserGroup1.setId(1L);
        ExtendedUserGroup extendedUserGroup2 = new ExtendedUserGroup();
        extendedUserGroup2.setId(extendedUserGroup1.getId());
        assertThat(extendedUserGroup1).isEqualTo(extendedUserGroup2);
        extendedUserGroup2.setId(2L);
        assertThat(extendedUserGroup1).isNotEqualTo(extendedUserGroup2);
        extendedUserGroup1.setId(null);
        assertThat(extendedUserGroup1).isNotEqualTo(extendedUserGroup2);
    }
}
