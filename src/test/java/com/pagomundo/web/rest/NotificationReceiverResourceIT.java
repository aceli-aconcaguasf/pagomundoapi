package com.pagomundo.web.rest;

import com.pagomundo.PagoMundoApiApp;
import com.pagomundo.domain.NotificationReceiver;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.repository.NotificationReceiverRepository;
import com.pagomundo.repository.search.NotificationReceiverSearchRepository;
import com.pagomundo.service.ExtendedUserService;
import com.pagomundo.service.NotificationReceiverService;
import com.pagomundo.service.TranslateService;
import com.pagomundo.web.rest.errors.ExceptionTranslator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;

import static com.pagomundo.web.rest.TestUtil.createFormattingConversionService;
import static com.pagomundo.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration tests for the {@link NotificationReceiverResource} REST controller.
 */
@SpringBootTest(classes = PagoMundoApiApp.class)
class NotificationReceiverResourceIT {

    private static final ZonedDateTime DEFAULT_DATE_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_CREATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_DATE_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final ZonedDateTime DEFAULT_LAST_UPDATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_LAST_UPDATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_LAST_UPDATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final Integer DEFAULT_RETRIES = 1;
    private static final Integer UPDATED_RETRIES = 2;
    private static final Integer SMALLER_RETRIES = 1 - 1;

    private static final Integer DEFAULT_MAX_RETRIES = 1;
    private static final Integer UPDATED_MAX_RETRIES = 2;
    private static final Integer SMALLER_MAX_RETRIES = 1 - 1;

    private static final Status DEFAULT_STATUS = Status.CREATED;
    private static final Status UPDATED_STATUS = Status.IN_PROCESS;

    private static final String DEFAULT_REASON = "AAAAAAAAAA";
    private static final String UPDATED_REASON = "BBBBBBBBBB";

    private static final Boolean DEFAULT_MUST_SEND_EMAIL = false;
    private static final Boolean UPDATED_MUST_SEND_EMAIL = true;

    private static final String DEFAULT_EMAIL_TEMPLATE = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL_TEMPLATE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_EMAIL_SENT = false;
    private static final Boolean UPDATED_EMAIL_SENT = true;

    private static final String DEFAULT_DEBUG_REASON = "AAAAAAAAAA";
    private static final String UPDATED_DEBUG_REASON = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL_TITLE_KEY = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL_TITLE_KEY = "BBBBBBBBBB";

    @Autowired
    private NotificationReceiverRepository notificationReceiverRepository;

    @Autowired
    private NotificationReceiverService notificationReceiverService;

    @Autowired
    private ExtendedUserService extendedUserService;

    /**
     * This repository is mocked in the com.pagomundo.repository.search test package.
     *
     * @see com.pagomundo.repository.search.NotificationReceiverSearchRepositoryMockConfiguration
     */
    @Autowired
    private NotificationReceiverSearchRepository mockNotificationReceiverSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restNotificationReceiverMockMvc;

    private NotificationReceiver notificationReceiver;

    @Autowired
    private TranslateService translateService;

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NotificationReceiver createEntity(EntityManager em) {
        NotificationReceiver notificationReceiver = new NotificationReceiver()
            .dateCreated(DEFAULT_DATE_CREATED)
            .lastUpdated(DEFAULT_LAST_UPDATED)
            .retries(DEFAULT_RETRIES)
            .maxRetries(DEFAULT_MAX_RETRIES)
            .status(DEFAULT_STATUS)
            .reason(DEFAULT_REASON)
            .mustSendEmail(DEFAULT_MUST_SEND_EMAIL)
            .emailTemplate(DEFAULT_EMAIL_TEMPLATE)
            .emailSent(DEFAULT_EMAIL_SENT)
            .debugReason(DEFAULT_DEBUG_REASON)
            .emailTitleKey(DEFAULT_EMAIL_TITLE_KEY);
        return notificationReceiver;
    }

    /**
     * Create an updated entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NotificationReceiver createUpdatedEntity(EntityManager em) {
        NotificationReceiver notificationReceiver = new NotificationReceiver()
            .dateCreated(UPDATED_DATE_CREATED)
            .lastUpdated(UPDATED_LAST_UPDATED)
            .retries(UPDATED_RETRIES)
            .maxRetries(UPDATED_MAX_RETRIES)
            .status(UPDATED_STATUS)
            .reason(UPDATED_REASON)
            .mustSendEmail(UPDATED_MUST_SEND_EMAIL)
            .emailTemplate(UPDATED_EMAIL_TEMPLATE)
            .emailSent(UPDATED_EMAIL_SENT)
            .debugReason(UPDATED_DEBUG_REASON)
            .emailTitleKey(UPDATED_EMAIL_TITLE_KEY);
        return notificationReceiver;
    }

    @BeforeEach
    void setup() {
        MockitoAnnotations.initMocks(this);
        final NotificationReceiverResource notificationReceiverResource = new NotificationReceiverResource(notificationReceiverService, translateService);
        this.restNotificationReceiverMockMvc = MockMvcBuilders.standaloneSetup(notificationReceiverResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    @BeforeEach
    void initTest() {
        notificationReceiver = createEntity(em);
    }

    @Test
    @Transactional
    void createNotificationReceiver() throws Exception {
        int databaseSizeBeforeCreate = notificationReceiverRepository.findAll().size();

        // Create the NotificationReceiver
        restNotificationReceiverMockMvc.perform(post("/api/notification-receivers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notificationReceiver)))
            .andExpect(status().isCreated());

        // Validate the NotificationReceiver in the database
        List<NotificationReceiver> notificationReceiverList = notificationReceiverRepository.findAll();
        assertThat(notificationReceiverList).hasSize(databaseSizeBeforeCreate + 1);
        NotificationReceiver testNotificationReceiver = notificationReceiverList.get(notificationReceiverList.size() - 1);
        assertThat(testNotificationReceiver.getDateCreated()).isEqualTo(DEFAULT_DATE_CREATED);
        assertThat(testNotificationReceiver.getLastUpdated()).isEqualTo(DEFAULT_LAST_UPDATED);
        assertThat(testNotificationReceiver.getRetries()).isEqualTo(DEFAULT_RETRIES);
        assertThat(testNotificationReceiver.getMaxRetries()).isEqualTo(DEFAULT_MAX_RETRIES);
        assertThat(testNotificationReceiver.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testNotificationReceiver.getReason()).isEqualTo(DEFAULT_REASON);
        assertThat(testNotificationReceiver.isMustSendEmail()).isEqualTo(DEFAULT_MUST_SEND_EMAIL);
        assertThat(testNotificationReceiver.getEmailTemplate()).isEqualTo(DEFAULT_EMAIL_TEMPLATE);
        assertThat(testNotificationReceiver.isEmailSent()).isEqualTo(DEFAULT_EMAIL_SENT);
        assertThat(testNotificationReceiver.getDebugReason()).isEqualTo(DEFAULT_DEBUG_REASON);
        assertThat(testNotificationReceiver.getEmailTitleKey()).isEqualTo(DEFAULT_EMAIL_TITLE_KEY);

        // Validate the NotificationReceiver in Elasticsearch
        verify(mockNotificationReceiverSearchRepository, times(1)).save(testNotificationReceiver);
    }

    @Test
    @Transactional
    void createNotificationReceiverWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = notificationReceiverRepository.findAll().size();

        // Create the NotificationReceiver with an existing ID
        notificationReceiver.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNotificationReceiverMockMvc.perform(post("/api/notification-receivers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notificationReceiver)))
            .andExpect(status().isBadRequest());

        // Validate the NotificationReceiver in the database
        List<NotificationReceiver> notificationReceiverList = notificationReceiverRepository.findAll();
        assertThat(notificationReceiverList).hasSize(databaseSizeBeforeCreate);

        // Validate the NotificationReceiver in Elasticsearch
        verify(mockNotificationReceiverSearchRepository, times(0)).save(notificationReceiver);
    }


    @Test
    @Transactional
    void getAllNotificationReceivers() throws Exception {
        // Initialize the database
        notificationReceiverRepository.saveAndFlush(notificationReceiver);

        // Get all the notificationReceiverList
        restNotificationReceiverMockMvc.perform(get("/api/notification-receivers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(notificationReceiver.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateCreated").value(hasItem(sameInstant(DEFAULT_DATE_CREATED))))
            .andExpect(jsonPath("$.[*].lastUpdated").value(hasItem(sameInstant(DEFAULT_LAST_UPDATED))))
            .andExpect(jsonPath("$.[*].retries").value(hasItem(DEFAULT_RETRIES)))
            .andExpect(jsonPath("$.[*].maxRetries").value(hasItem(DEFAULT_MAX_RETRIES)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].reason").value(hasItem(DEFAULT_REASON)))
            .andExpect(jsonPath("$.[*].mustSendEmail").value(hasItem(DEFAULT_MUST_SEND_EMAIL.booleanValue())))
            .andExpect(jsonPath("$.[*].emailTemplate").value(hasItem(DEFAULT_EMAIL_TEMPLATE)))
            .andExpect(jsonPath("$.[*].emailSent").value(hasItem(DEFAULT_EMAIL_SENT.booleanValue())))
            .andExpect(jsonPath("$.[*].debugReason").value(hasItem(DEFAULT_DEBUG_REASON)))
            .andExpect(jsonPath("$.[*].emailTitleKey").value(hasItem(DEFAULT_EMAIL_TITLE_KEY)));
    }

    @Test
    @Transactional
    void getNotificationReceiver() throws Exception {
        // Initialize the database
        notificationReceiverRepository.saveAndFlush(notificationReceiver);

        // Get the notificationReceiver
        restNotificationReceiverMockMvc.perform(get("/api/notification-receivers/{id}", notificationReceiver.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(notificationReceiver.getId().intValue()))
            .andExpect(jsonPath("$.dateCreated").value(sameInstant(DEFAULT_DATE_CREATED)))
            .andExpect(jsonPath("$.lastUpdated").value(sameInstant(DEFAULT_LAST_UPDATED)))
            .andExpect(jsonPath("$.retries").value(DEFAULT_RETRIES))
            .andExpect(jsonPath("$.maxRetries").value(DEFAULT_MAX_RETRIES))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.reason").value(DEFAULT_REASON))
            .andExpect(jsonPath("$.mustSendEmail").value(DEFAULT_MUST_SEND_EMAIL.booleanValue()))
            .andExpect(jsonPath("$.emailTemplate").value(DEFAULT_EMAIL_TEMPLATE))
            .andExpect(jsonPath("$.emailSent").value(DEFAULT_EMAIL_SENT.booleanValue()))
            .andExpect(jsonPath("$.debugReason").value(DEFAULT_DEBUG_REASON))
            .andExpect(jsonPath("$.emailTitleKey").value(DEFAULT_EMAIL_TITLE_KEY));
    }

    @Test
    @Transactional
    void getNonExistingNotificationReceiver() throws Exception {
        // Get the notificationReceiver
        restNotificationReceiverMockMvc.perform(get("/api/notification-receivers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void updateNotificationReceiver() throws Exception {
        // Initialize the database
        notificationReceiverService.save(notificationReceiver);
        // As the test used the service layer, reset the Elasticsearch mock repository
        reset(mockNotificationReceiverSearchRepository);

        int databaseSizeBeforeUpdate = notificationReceiverRepository.findAll().size();

        // Update the notificationReceiver
        NotificationReceiver updatedNotificationReceiver = notificationReceiverRepository.findById(notificationReceiver.getId()).get();
        // Disconnect from session so that the updates on updatedNotificationReceiver are not directly saved in db
        em.detach(updatedNotificationReceiver);
        updatedNotificationReceiver
            .dateCreated(UPDATED_DATE_CREATED)
            .lastUpdated(UPDATED_LAST_UPDATED)
            .retries(UPDATED_RETRIES)
            .maxRetries(UPDATED_MAX_RETRIES)
            .status(UPDATED_STATUS)
            .reason(UPDATED_REASON)
            .mustSendEmail(UPDATED_MUST_SEND_EMAIL)
            .emailTemplate(UPDATED_EMAIL_TEMPLATE)
            .emailSent(UPDATED_EMAIL_SENT)
            .debugReason(UPDATED_DEBUG_REASON)
            .emailTitleKey(UPDATED_EMAIL_TITLE_KEY);

        restNotificationReceiverMockMvc.perform(put("/api/notification-receivers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedNotificationReceiver)))
            .andExpect(status().isOk());

        // Validate the NotificationReceiver in the database
        List<NotificationReceiver> notificationReceiverList = notificationReceiverRepository.findAll();
        assertThat(notificationReceiverList).hasSize(databaseSizeBeforeUpdate);
        NotificationReceiver testNotificationReceiver = notificationReceiverList.get(notificationReceiverList.size() - 1);
        assertThat(testNotificationReceiver.getDateCreated()).isEqualTo(UPDATED_DATE_CREATED);
        assertThat(testNotificationReceiver.getLastUpdated()).isEqualTo(UPDATED_LAST_UPDATED);
        assertThat(testNotificationReceiver.getRetries()).isEqualTo(UPDATED_RETRIES);
        assertThat(testNotificationReceiver.getMaxRetries()).isEqualTo(UPDATED_MAX_RETRIES);
        assertThat(testNotificationReceiver.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testNotificationReceiver.getReason()).isEqualTo(UPDATED_REASON);
        assertThat(testNotificationReceiver.isMustSendEmail()).isEqualTo(UPDATED_MUST_SEND_EMAIL);
        assertThat(testNotificationReceiver.getEmailTemplate()).isEqualTo(UPDATED_EMAIL_TEMPLATE);
        assertThat(testNotificationReceiver.isEmailSent()).isEqualTo(UPDATED_EMAIL_SENT);
        assertThat(testNotificationReceiver.getDebugReason()).isEqualTo(UPDATED_DEBUG_REASON);
        assertThat(testNotificationReceiver.getEmailTitleKey()).isEqualTo(UPDATED_EMAIL_TITLE_KEY);

        // Validate the NotificationReceiver in Elasticsearch
        verify(mockNotificationReceiverSearchRepository, times(1)).save(testNotificationReceiver);
    }

    @Test
    @Transactional
    void updateNonExistingNotificationReceiver() throws Exception {
        int databaseSizeBeforeUpdate = notificationReceiverRepository.findAll().size();

        // Create the NotificationReceiver

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNotificationReceiverMockMvc.perform(put("/api/notification-receivers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notificationReceiver)))
            .andExpect(status().isBadRequest());

        // Validate the NotificationReceiver in the database
        List<NotificationReceiver> notificationReceiverList = notificationReceiverRepository.findAll();
        assertThat(notificationReceiverList).hasSize(databaseSizeBeforeUpdate);

        // Validate the NotificationReceiver in Elasticsearch
        verify(mockNotificationReceiverSearchRepository, times(0)).save(notificationReceiver);
    }

    @Test
    @Transactional
    void deleteNotificationReceiver() throws Exception {
        // Initialize the database
        notificationReceiverService.save(notificationReceiver);

        int databaseSizeBeforeDelete = notificationReceiverRepository.findAll().size();

        // Delete the notificationReceiver
        restNotificationReceiverMockMvc.perform(delete("/api/notification-receivers/{id}", notificationReceiver.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<NotificationReceiver> notificationReceiverList = notificationReceiverRepository.findAll();
        assertThat(notificationReceiverList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the NotificationReceiver in Elasticsearch
        verify(mockNotificationReceiverSearchRepository, times(1)).deleteById(notificationReceiver.getId());
    }

    @Test
    @Transactional
    void searchNotificationReceiver() throws Exception {
        // Initialize the database
        notificationReceiverService.save(notificationReceiver);
        when(mockNotificationReceiverSearchRepository.search(queryStringQuery("id:" + notificationReceiver.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(notificationReceiver), PageRequest.of(0, 1), 1));
        // Search the notificationReceiver
        restNotificationReceiverMockMvc.perform(get("/api/_search/notification-receivers?query=id:" + notificationReceiver.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(notificationReceiver.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateCreated").value(hasItem(sameInstant(DEFAULT_DATE_CREATED))))
            .andExpect(jsonPath("$.[*].lastUpdated").value(hasItem(sameInstant(DEFAULT_LAST_UPDATED))))
            .andExpect(jsonPath("$.[*].retries").value(hasItem(DEFAULT_RETRIES)))
            .andExpect(jsonPath("$.[*].maxRetries").value(hasItem(DEFAULT_MAX_RETRIES)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].reason").value(hasItem(DEFAULT_REASON)))
            .andExpect(jsonPath("$.[*].mustSendEmail").value(hasItem(DEFAULT_MUST_SEND_EMAIL.booleanValue())))
            .andExpect(jsonPath("$.[*].emailTemplate").value(hasItem(DEFAULT_EMAIL_TEMPLATE)))
            .andExpect(jsonPath("$.[*].emailSent").value(hasItem(DEFAULT_EMAIL_SENT.booleanValue())))
            .andExpect(jsonPath("$.[*].debugReason").value(hasItem(DEFAULT_DEBUG_REASON)))
            .andExpect(jsonPath("$.[*].emailTitleKey").value(hasItem(DEFAULT_EMAIL_TITLE_KEY)));
    }

    @Test
    @Transactional
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NotificationReceiver.class);
        NotificationReceiver notificationReceiver1 = new NotificationReceiver();
        notificationReceiver1.setId(1L);
        NotificationReceiver notificationReceiver2 = new NotificationReceiver();
        notificationReceiver2.setId(notificationReceiver1.getId());
        assertThat(notificationReceiver1).isEqualTo(notificationReceiver2);
        notificationReceiver2.setId(2L);
        assertThat(notificationReceiver1).isNotEqualTo(notificationReceiver2);
        notificationReceiver1.setId(null);
        assertThat(notificationReceiver1).isNotEqualTo(notificationReceiver2);
    }
}
