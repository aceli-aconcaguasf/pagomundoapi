package com.pagomundo.web.rest;

import com.pagomundo.PagoMundoApiApp;
import com.pagomundo.domain.DirectPaymentBank;
import com.pagomundo.repository.DirectPaymentBankRepository;
import com.pagomundo.repository.search.DirectPaymentBankSearchRepository;
import com.pagomundo.service.CountryService;
import com.pagomundo.service.DirectPaymentBankService;
import com.pagomundo.service.ExtendedUserService;
import com.pagomundo.service.TranslateService;
import com.pagomundo.web.rest.errors.ExceptionTranslator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static com.pagomundo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration tests for the {@link DirectPaymentBankResource} REST controller.
 */
@SpringBootTest(classes = PagoMundoApiApp.class)
class DirectPaymentBankResourceIT {

    private static final String DEFAULT_DESTINY_BANK_CODE = "AAAAAAAAAA";
    private static final String UPDATED_DESTINY_BANK_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_ID_TYPE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ID_TYPE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ID_TYPE_CODE = "AAAAAAAAAA";
    private static final String UPDATED_ID_TYPE_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_CITY_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CITY_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_DEPARTMENT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_DEPARTMENT_CODE = "BBBBBBBBBB";

    @Autowired
    private DirectPaymentBankRepository directPaymentBankRepository;

    @Autowired
    private DirectPaymentBankService directPaymentBankService;

    @Autowired
    private ExtendedUserService extendedUserService;

    @Autowired
    private CountryService countryService;

    /**
     * This repository is mocked in the com.pagomundo.repository.search test package.
     *
     * @see com.pagomundo.repository.search.DirectPaymentBankSearchRepositoryMockConfiguration
     */
    @Autowired
    private DirectPaymentBankSearchRepository mockDirectPaymentBankSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restDirectPaymentBankMockMvc;

    private DirectPaymentBank directPaymentBank;

    @Autowired
    private TranslateService translateService;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DirectPaymentBank createEntity(EntityManager em) {
        DirectPaymentBank directPaymentBank = new DirectPaymentBank()
            .destinyBankCode(DEFAULT_DESTINY_BANK_CODE)
            .idTypeName(DEFAULT_ID_TYPE_NAME)
            .idTypeCode(DEFAULT_ID_TYPE_CODE)
            .cityCode(DEFAULT_CITY_CODE)
            .departmentCode(DEFAULT_DEPARTMENT_CODE);
        return directPaymentBank;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DirectPaymentBank createUpdatedEntity(EntityManager em) {
        DirectPaymentBank directPaymentBank = new DirectPaymentBank()
            .destinyBankCode(UPDATED_DESTINY_BANK_CODE)
            .idTypeName(UPDATED_ID_TYPE_NAME)
            .idTypeCode(UPDATED_ID_TYPE_CODE)
            .cityCode(UPDATED_CITY_CODE)
            .departmentCode(UPDATED_DEPARTMENT_CODE);
        return directPaymentBank;
    }

    @BeforeEach
    void setup() {
        MockitoAnnotations.initMocks(this);
        final DirectPaymentBankResource directPaymentBankResource = new DirectPaymentBankResource(directPaymentBankService, extendedUserService, countryService, translateService);
        this.restDirectPaymentBankMockMvc = MockMvcBuilders.standaloneSetup(directPaymentBankResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    @BeforeEach
    void initTest() {
        directPaymentBank = createEntity(em);
    }

    @Test
    @Transactional
    void createDirectPaymentBank() throws Exception {
        int databaseSizeBeforeCreate = directPaymentBankRepository.findAll().size();

        // Create the DirectPaymentBank
        restDirectPaymentBankMockMvc.perform(post("/api/direct-payment-banks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(directPaymentBank)))
            .andExpect(status().isCreated());

        // Validate the DirectPaymentBank in the database
        List<DirectPaymentBank> directPaymentBankList = directPaymentBankRepository.findAll();
        assertThat(directPaymentBankList).hasSize(databaseSizeBeforeCreate + 1);
        DirectPaymentBank testDirectPaymentBank = directPaymentBankList.get(directPaymentBankList.size() - 1);
        assertThat(testDirectPaymentBank.getDestinyBankCode()).isEqualTo(DEFAULT_DESTINY_BANK_CODE);
        assertThat(testDirectPaymentBank.getIdTypeName()).isEqualTo(DEFAULT_ID_TYPE_NAME);
        assertThat(testDirectPaymentBank.getIdTypeCode()).isEqualTo(DEFAULT_ID_TYPE_CODE);
        assertThat(testDirectPaymentBank.getCityCode()).isEqualTo(DEFAULT_CITY_CODE);
        assertThat(testDirectPaymentBank.getDepartmentCode()).isEqualTo(DEFAULT_DEPARTMENT_CODE);

        // Validate the DirectPaymentBank in Elasticsearch
        verify(mockDirectPaymentBankSearchRepository, times(1)).save(testDirectPaymentBank);
    }

    @Test
    @Transactional
    void createDirectPaymentBankWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = directPaymentBankRepository.findAll().size();

        // Create the DirectPaymentBank with an existing ID
        directPaymentBank.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDirectPaymentBankMockMvc.perform(post("/api/direct-payment-banks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(directPaymentBank)))
            .andExpect(status().isBadRequest());

        // Validate the DirectPaymentBank in the database
        List<DirectPaymentBank> directPaymentBankList = directPaymentBankRepository.findAll();
        assertThat(directPaymentBankList).hasSize(databaseSizeBeforeCreate);

        // Validate the DirectPaymentBank in Elasticsearch
        verify(mockDirectPaymentBankSearchRepository, times(0)).save(directPaymentBank);
    }


    @Test
    @Transactional
    void getAllDirectPaymentBanks() throws Exception {
        // Initialize the database
        directPaymentBankRepository.saveAndFlush(directPaymentBank);

        // Get all the directPaymentBankList
        restDirectPaymentBankMockMvc.perform(get("/api/direct-payment-banks?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(directPaymentBank.getId().intValue())))
            .andExpect(jsonPath("$.[*].destinyBankCode").value(hasItem(DEFAULT_DESTINY_BANK_CODE)))
            .andExpect(jsonPath("$.[*].idTypeName").value(hasItem(DEFAULT_ID_TYPE_NAME)))
            .andExpect(jsonPath("$.[*].idTypeCode").value(hasItem(DEFAULT_ID_TYPE_CODE)))
            .andExpect(jsonPath("$.[*].cityCode").value(hasItem(DEFAULT_CITY_CODE)))
            .andExpect(jsonPath("$.[*].departmentCode").value(hasItem(DEFAULT_DEPARTMENT_CODE)));
    }

    @Test
    @Transactional
    void getDirectPaymentBank() throws Exception {
        // Initialize the database
        directPaymentBankRepository.saveAndFlush(directPaymentBank);

        // Get the directPaymentBank
        restDirectPaymentBankMockMvc.perform(get("/api/direct-payment-banks/{id}", directPaymentBank.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(directPaymentBank.getId().intValue()))
            .andExpect(jsonPath("$.destinyBankCode").value(DEFAULT_DESTINY_BANK_CODE))
            .andExpect(jsonPath("$.idTypeName").value(DEFAULT_ID_TYPE_NAME))
            .andExpect(jsonPath("$.idTypeCode").value(DEFAULT_ID_TYPE_CODE))
            .andExpect(jsonPath("$.cityCode").value(DEFAULT_CITY_CODE))
            .andExpect(jsonPath("$.departmentCode").value(DEFAULT_DEPARTMENT_CODE));
    }

    @Test
    @Transactional
    void getNonExistingDirectPaymentBank() throws Exception {
        // Get the directPaymentBank
        restDirectPaymentBankMockMvc.perform(get("/api/direct-payment-banks/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void updateDirectPaymentBank() throws Exception {
        // Initialize the database
        directPaymentBankService.save(directPaymentBank);
        // As the test used the service layer, reset the Elasticsearch mock repository
        reset(mockDirectPaymentBankSearchRepository);

        int databaseSizeBeforeUpdate = directPaymentBankRepository.findAll().size();

        // Update the directPaymentBank
        DirectPaymentBank updatedDirectPaymentBank = directPaymentBankRepository.findById(directPaymentBank.getId()).get();
        // Disconnect from session so that the updates on updatedDirectPaymentBank are not directly saved in db
        em.detach(updatedDirectPaymentBank);
        updatedDirectPaymentBank
            .destinyBankCode(UPDATED_DESTINY_BANK_CODE)
            .idTypeName(UPDATED_ID_TYPE_NAME)
            .idTypeCode(UPDATED_ID_TYPE_CODE)
            .cityCode(UPDATED_CITY_CODE)
            .departmentCode(UPDATED_DEPARTMENT_CODE);

        restDirectPaymentBankMockMvc.perform(put("/api/direct-payment-banks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDirectPaymentBank)))
            .andExpect(status().isOk());

        // Validate the DirectPaymentBank in the database
        List<DirectPaymentBank> directPaymentBankList = directPaymentBankRepository.findAll();
        assertThat(directPaymentBankList).hasSize(databaseSizeBeforeUpdate);
        DirectPaymentBank testDirectPaymentBank = directPaymentBankList.get(directPaymentBankList.size() - 1);
        assertThat(testDirectPaymentBank.getDestinyBankCode()).isEqualTo(UPDATED_DESTINY_BANK_CODE);
        assertThat(testDirectPaymentBank.getIdTypeName()).isEqualTo(UPDATED_ID_TYPE_NAME);
        assertThat(testDirectPaymentBank.getIdTypeCode()).isEqualTo(UPDATED_ID_TYPE_CODE);
        assertThat(testDirectPaymentBank.getCityCode()).isEqualTo(UPDATED_CITY_CODE);
        assertThat(testDirectPaymentBank.getDepartmentCode()).isEqualTo(UPDATED_DEPARTMENT_CODE);

        // Validate the DirectPaymentBank in Elasticsearch
        verify(mockDirectPaymentBankSearchRepository, times(1)).save(testDirectPaymentBank);
    }

    @Test
    @Transactional
    void updateNonExistingDirectPaymentBank() throws Exception {
        int databaseSizeBeforeUpdate = directPaymentBankRepository.findAll().size();

        // Create the DirectPaymentBank

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDirectPaymentBankMockMvc.perform(put("/api/direct-payment-banks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(directPaymentBank)))
            .andExpect(status().isBadRequest());

        // Validate the DirectPaymentBank in the database
        List<DirectPaymentBank> directPaymentBankList = directPaymentBankRepository.findAll();
        assertThat(directPaymentBankList).hasSize(databaseSizeBeforeUpdate);

        // Validate the DirectPaymentBank in Elasticsearch
        verify(mockDirectPaymentBankSearchRepository, times(0)).save(directPaymentBank);
    }

    @Test
    @Transactional
    void deleteDirectPaymentBank() throws Exception {
        // Initialize the database
        directPaymentBankService.save(directPaymentBank);

        int databaseSizeBeforeDelete = directPaymentBankRepository.findAll().size();

        // Delete the directPaymentBank
        restDirectPaymentBankMockMvc.perform(delete("/api/direct-payment-banks/{id}", directPaymentBank.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DirectPaymentBank> directPaymentBankList = directPaymentBankRepository.findAll();
        assertThat(directPaymentBankList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the DirectPaymentBank in Elasticsearch
        verify(mockDirectPaymentBankSearchRepository, times(1)).deleteById(directPaymentBank.getId());
    }

    @Test
    @Transactional
    void searchDirectPaymentBank() throws Exception {
        // Initialize the database
        directPaymentBankService.save(directPaymentBank);
        when(mockDirectPaymentBankSearchRepository.search(queryStringQuery("id:" + directPaymentBank.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(directPaymentBank), PageRequest.of(0, 1), 1));
        // Search the directPaymentBank
        restDirectPaymentBankMockMvc.perform(get("/api/_search/direct-payment-banks?query=id:" + directPaymentBank.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(directPaymentBank.getId().intValue())))
            .andExpect(jsonPath("$.[*].destinyBankCode").value(hasItem(DEFAULT_DESTINY_BANK_CODE)))
            .andExpect(jsonPath("$.[*].idTypeName").value(hasItem(DEFAULT_ID_TYPE_NAME)))
            .andExpect(jsonPath("$.[*].idTypeCode").value(hasItem(DEFAULT_ID_TYPE_CODE)))
            .andExpect(jsonPath("$.[*].cityCode").value(hasItem(DEFAULT_CITY_CODE)))
            .andExpect(jsonPath("$.[*].departmentCode").value(hasItem(DEFAULT_DEPARTMENT_CODE)));
    }

    @Test
    @Transactional
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DirectPaymentBank.class);
        DirectPaymentBank directPaymentBank1 = new DirectPaymentBank();
        directPaymentBank1.setId(1L);
        DirectPaymentBank directPaymentBank2 = new DirectPaymentBank();
        directPaymentBank2.setId(directPaymentBank1.getId());
        assertThat(directPaymentBank1).isEqualTo(directPaymentBank2);
        directPaymentBank2.setId(2L);
        assertThat(directPaymentBank1).isNotEqualTo(directPaymentBank2);
        directPaymentBank1.setId(null);
        assertThat(directPaymentBank1).isNotEqualTo(directPaymentBank2);
    }
}
