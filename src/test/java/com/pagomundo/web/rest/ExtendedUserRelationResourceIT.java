package com.pagomundo.web.rest;

import com.pagomundo.PagoMundoApiApp;
import com.pagomundo.domain.ExtendedUserRelation;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.repository.ExtendedUserRelationRepository;
import com.pagomundo.repository.search.ExtendedUserRelationSearchRepository;
import com.pagomundo.repository.search.RefetchService;
import com.pagomundo.service.ExtendedUserRelationService;
import com.pagomundo.service.ExtendedUserService;
import com.pagomundo.service.FileService;
import com.pagomundo.service.TranslateService;
import com.pagomundo.service.UserService;
import com.pagomundo.web.rest.errors.ExceptionTranslator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;

import static com.pagomundo.web.rest.TestUtil.createFormattingConversionService;
import static com.pagomundo.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration tests for the {@link ExtendedUserRelationResource} REST controller.
 */
@SpringBootTest(classes = PagoMundoApiApp.class)
class ExtendedUserRelationResourceIT {

    private static final Status DEFAULT_STATUS = Status.CREATED;
    private static final Status UPDATED_STATUS = Status.IN_PROCESS;

    private static final String DEFAULT_STATUS_REASON = "AAAAAAAAAA";
    private static final String UPDATED_STATUS_REASON = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATE_RELATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_RELATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_DATE_RELATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final ZonedDateTime DEFAULT_DATE_UNRELATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_UNRELATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_DATE_UNRELATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final String DEFAULT_NICKNAME = "AAAAAAAAAA";
    private static final String UPDATED_NICKNAME = "BBBBBBBBBB";

    @Autowired
    private ExtendedUserRelationRepository extendedUserRelationRepository;

    @Autowired
    private ExtendedUserRelationService extendedUserRelationService;

    @Autowired
    private ExtendedUserService extendedUserService;

    @Autowired
    private UserService userService;

    @Autowired
    private RefetchService refetchService;

    /**
     * This repository is mocked in the com.pagomundo.repository.search test package.
     *
     * @see com.pagomundo.repository.search.ExtendedUserRelationSearchRepositoryMockConfiguration
     */
    @Autowired
    private ExtendedUserRelationSearchRepository mockExtendedUserRelationSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    @Autowired
    private FileService fileService;

    private MockMvc restExtendedUserRelationMockMvc;

    private ExtendedUserRelation extendedUserRelation;

    @Autowired
    private TranslateService translateService;

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExtendedUserRelation createEntity(EntityManager em) {
        ExtendedUserRelation extendedUserRelation = new ExtendedUserRelation()
            .status(DEFAULT_STATUS)
            .statusReason(DEFAULT_STATUS_REASON)
            .dateRelated(DEFAULT_DATE_RELATED)
            .dateUnrelated(DEFAULT_DATE_UNRELATED)
            .nickname(DEFAULT_NICKNAME);
        return extendedUserRelation;
    }

    /**
     * Create an updated entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExtendedUserRelation createUpdatedEntity(EntityManager em) {
        ExtendedUserRelation extendedUserRelation = new ExtendedUserRelation()
            .status(UPDATED_STATUS)
            .statusReason(UPDATED_STATUS_REASON)
            .dateRelated(UPDATED_DATE_RELATED)
            .dateUnrelated(UPDATED_DATE_UNRELATED)
            .nickname(UPDATED_NICKNAME);
        return extendedUserRelation;
    }

    @BeforeEach
    void setup() {
        MockitoAnnotations.initMocks(this);
        final ExtendedUserRelationResource extendedUserRelationResource = new ExtendedUserRelationResource(
            extendedUserRelationService, extendedUserService, refetchService, userService, fileService,
            translateService);
        this.restExtendedUserRelationMockMvc = MockMvcBuilders.standaloneSetup(extendedUserRelationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    @BeforeEach
    void initTest() {
        extendedUserRelation = createEntity(em);
    }

    @Test
    @Transactional
    void createExtendedUserRelation() throws Exception {
        int databaseSizeBeforeCreate = extendedUserRelationRepository.findAll().size();

        // Create the ExtendedUserRelation
        restExtendedUserRelationMockMvc.perform(post("/api/extended-user-relations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(extendedUserRelation)))
            .andExpect(status().isCreated());

        // Validate the ExtendedUserRelation in the database
        List<ExtendedUserRelation> extendedUserRelationList = extendedUserRelationRepository.findAll();
        assertThat(extendedUserRelationList).hasSize(databaseSizeBeforeCreate + 1);
        ExtendedUserRelation testExtendedUserRelation = extendedUserRelationList.get(extendedUserRelationList.size() - 1);
        assertThat(testExtendedUserRelation.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testExtendedUserRelation.getStatusReason()).isEqualTo(DEFAULT_STATUS_REASON);
        assertThat(testExtendedUserRelation.getDateRelated()).isEqualTo(DEFAULT_DATE_RELATED);
        assertThat(testExtendedUserRelation.getDateUnrelated()).isEqualTo(DEFAULT_DATE_UNRELATED);
        assertThat(testExtendedUserRelation.getNickname()).isEqualTo(DEFAULT_NICKNAME);

        // Validate the ExtendedUserRelation in Elasticsearch
        verify(mockExtendedUserRelationSearchRepository, times(1)).save(testExtendedUserRelation);
    }

    @Test
    @Transactional
    void createExtendedUserRelationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = extendedUserRelationRepository.findAll().size();

        // Create the ExtendedUserRelation with an existing ID
        extendedUserRelation.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restExtendedUserRelationMockMvc.perform(post("/api/extended-user-relations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(extendedUserRelation)))
            .andExpect(status().isBadRequest());

        // Validate the ExtendedUserRelation in the database
        List<ExtendedUserRelation> extendedUserRelationList = extendedUserRelationRepository.findAll();
        assertThat(extendedUserRelationList).hasSize(databaseSizeBeforeCreate);

        // Validate the ExtendedUserRelation in Elasticsearch
        verify(mockExtendedUserRelationSearchRepository, times(0)).save(extendedUserRelation);
    }


    @Test
    @Transactional
    void getAllExtendedUserRelations() throws Exception {
        // Initialize the database
        extendedUserRelationRepository.saveAndFlush(extendedUserRelation);

        // Get all the extendedUserRelationList
        restExtendedUserRelationMockMvc.perform(get("/api/extended-user-relations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(extendedUserRelation.getId().intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].statusReason").value(hasItem(DEFAULT_STATUS_REASON)))
            .andExpect(jsonPath("$.[*].dateRelated").value(hasItem(sameInstant(DEFAULT_DATE_RELATED))))
            .andExpect(jsonPath("$.[*].dateUnrelated").value(hasItem(sameInstant(DEFAULT_DATE_UNRELATED))))
            .andExpect(jsonPath("$.[*].nickname").value(hasItem(DEFAULT_NICKNAME)));
    }

    @Test
    @Transactional
    void getExtendedUserRelation() throws Exception {
        // Initialize the database
        extendedUserRelationRepository.saveAndFlush(extendedUserRelation);

        // Get the extendedUserRelation
        restExtendedUserRelationMockMvc.perform(get("/api/extended-user-relations/{id}", extendedUserRelation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(extendedUserRelation.getId().intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.statusReason").value(DEFAULT_STATUS_REASON))
            .andExpect(jsonPath("$.dateRelated").value(sameInstant(DEFAULT_DATE_RELATED)))
            .andExpect(jsonPath("$.dateUnrelated").value(sameInstant(DEFAULT_DATE_UNRELATED)))
            .andExpect(jsonPath("$.nickname").value(DEFAULT_NICKNAME));
    }

    @Test
    @Transactional
    void getNonExistingExtendedUserRelation() throws Exception {
        // Get the extendedUserRelation
        restExtendedUserRelationMockMvc.perform(get("/api/extended-user-relations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void updateExtendedUserRelation() throws Exception {
        // Initialize the database
        extendedUserRelationService.save(extendedUserRelation);
        // As the test used the service layer, reset the Elasticsearch mock repository
        reset(mockExtendedUserRelationSearchRepository);

        int databaseSizeBeforeUpdate = extendedUserRelationRepository.findAll().size();

        // Update the extendedUserRelation
        ExtendedUserRelation updatedExtendedUserRelation = extendedUserRelationRepository.findById(extendedUserRelation.getId()).get();
        // Disconnect from session so that the updates on updatedExtendedUserRelation are not directly saved in db
        em.detach(updatedExtendedUserRelation);
        updatedExtendedUserRelation
            .status(UPDATED_STATUS)
            .statusReason(UPDATED_STATUS_REASON)
            .dateRelated(UPDATED_DATE_RELATED)
            .dateUnrelated(UPDATED_DATE_UNRELATED)
            .nickname(UPDATED_NICKNAME);

        restExtendedUserRelationMockMvc.perform(put("/api/extended-user-relations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedExtendedUserRelation)))
            .andExpect(status().isOk());

        // Validate the ExtendedUserRelation in the database
        List<ExtendedUserRelation> extendedUserRelationList = extendedUserRelationRepository.findAll();
        assertThat(extendedUserRelationList).hasSize(databaseSizeBeforeUpdate);
        ExtendedUserRelation testExtendedUserRelation = extendedUserRelationList.get(extendedUserRelationList.size() - 1);
        assertThat(testExtendedUserRelation.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testExtendedUserRelation.getStatusReason()).isEqualTo(UPDATED_STATUS_REASON);
        assertThat(testExtendedUserRelation.getDateRelated()).isEqualTo(UPDATED_DATE_RELATED);
        assertThat(testExtendedUserRelation.getDateUnrelated()).isEqualTo(UPDATED_DATE_UNRELATED);
        assertThat(testExtendedUserRelation.getNickname()).isEqualTo(UPDATED_NICKNAME);

        // Validate the ExtendedUserRelation in Elasticsearch
        verify(mockExtendedUserRelationSearchRepository, times(1)).save(testExtendedUserRelation);
    }

    @Test
    @Transactional
    void updateNonExistingExtendedUserRelation() throws Exception {
        int databaseSizeBeforeUpdate = extendedUserRelationRepository.findAll().size();

        // Create the ExtendedUserRelation

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExtendedUserRelationMockMvc.perform(put("/api/extended-user-relations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(extendedUserRelation)))
            .andExpect(status().isBadRequest());

        // Validate the ExtendedUserRelation in the database
        List<ExtendedUserRelation> extendedUserRelationList = extendedUserRelationRepository.findAll();
        assertThat(extendedUserRelationList).hasSize(databaseSizeBeforeUpdate);

        // Validate the ExtendedUserRelation in Elasticsearch
        verify(mockExtendedUserRelationSearchRepository, times(0)).save(extendedUserRelation);
    }

    @Test
    @Transactional
    void deleteExtendedUserRelation() throws Exception {
        // Initialize the database
        extendedUserRelationService.save(extendedUserRelation);

        int databaseSizeBeforeDelete = extendedUserRelationRepository.findAll().size();

        // Delete the extendedUserRelation
        restExtendedUserRelationMockMvc.perform(delete("/api/extended-user-relations/{id}", extendedUserRelation.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ExtendedUserRelation> extendedUserRelationList = extendedUserRelationRepository.findAll();
        assertThat(extendedUserRelationList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the ExtendedUserRelation in Elasticsearch
        verify(mockExtendedUserRelationSearchRepository, times(1)).deleteById(extendedUserRelation.getId());
    }

    @Test
    @Transactional
    void searchExtendedUserRelation() throws Exception {
        // Initialize the database
        extendedUserRelationService.save(extendedUserRelation);
        when(mockExtendedUserRelationSearchRepository.search(queryStringQuery("id:" + extendedUserRelation.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(extendedUserRelation), PageRequest.of(0, 1), 1));
        // Search the extendedUserRelation
        restExtendedUserRelationMockMvc.perform(get("/api/_search/extended-user-relations?query=id:" + extendedUserRelation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(extendedUserRelation.getId().intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].statusReason").value(hasItem(DEFAULT_STATUS_REASON)))
            .andExpect(jsonPath("$.[*].dateRelated").value(hasItem(sameInstant(DEFAULT_DATE_RELATED))))
            .andExpect(jsonPath("$.[*].dateUnrelated").value(hasItem(sameInstant(DEFAULT_DATE_UNRELATED))))
            .andExpect(jsonPath("$.[*].nickname").value(hasItem(DEFAULT_NICKNAME)));
    }

    @Test
    @Transactional
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExtendedUserRelation.class);
        ExtendedUserRelation extendedUserRelation1 = new ExtendedUserRelation();
        extendedUserRelation1.setId(1L);
        ExtendedUserRelation extendedUserRelation2 = new ExtendedUserRelation();
        extendedUserRelation2.setId(extendedUserRelation1.getId());
        assertThat(extendedUserRelation1).isEqualTo(extendedUserRelation2);
        extendedUserRelation2.setId(2L);
        assertThat(extendedUserRelation1).isNotEqualTo(extendedUserRelation2);
        extendedUserRelation1.setId(null);
        assertThat(extendedUserRelation1).isNotEqualTo(extendedUserRelation2);
    }
}
