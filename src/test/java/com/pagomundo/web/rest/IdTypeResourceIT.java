package com.pagomundo.web.rest;

import com.pagomundo.PagoMundoApiApp;
import com.pagomundo.domain.IdType;
import com.pagomundo.repository.IdTypeRepository;
import com.pagomundo.repository.search.IdTypeSearchRepository;
import com.pagomundo.service.CountryService;
import com.pagomundo.service.ExtendedUserService;
import com.pagomundo.service.IdTypeService;
import com.pagomundo.service.TranslateService;
import com.pagomundo.web.rest.errors.ExceptionTranslator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static com.pagomundo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration tests for the {@link IdTypeResource} REST controller.
 */
@SpringBootTest(classes = PagoMundoApiApp.class)
class IdTypeResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    @Autowired
    private IdTypeRepository idTypeRepository;

    @Autowired
    private IdTypeService idTypeService;

    @Autowired
    private ExtendedUserService extendedUserService;

    @Autowired
    private CountryService countryService;

    /**
     * This repository is mocked in the com.pagomundo.repository.search test package.
     *
     * @see com.pagomundo.repository.search.IdTypeSearchRepositoryMockConfiguration
     */
    @Autowired
    private IdTypeSearchRepository mockIdTypeSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restIdTypeMockMvc;

    private IdType idType;

    @Autowired
    private TranslateService translateService;
    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IdType createEntity(EntityManager em) {
        IdType idType = new IdType()
            .name(DEFAULT_NAME)
            .code(DEFAULT_CODE);
        return idType;
    }

    /**
     * Create an updated entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IdType createUpdatedEntity(EntityManager em) {
        IdType idType = new IdType()
            .name(UPDATED_NAME)
            .code(UPDATED_CODE);
        return idType;
    }

    @BeforeEach
    void setup() {
        MockitoAnnotations.initMocks(this);
        final IdTypeResource idTypeResource = new IdTypeResource(idTypeService, extendedUserService, countryService, translateService);
        this.restIdTypeMockMvc = MockMvcBuilders.standaloneSetup(idTypeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    @BeforeEach
    void initTest() {
        idType = createEntity(em);
    }

    @Test
    @Transactional
    void createIdType() throws Exception {
        int databaseSizeBeforeCreate = idTypeRepository.findAll().size();

        // Create the IdType
        restIdTypeMockMvc.perform(post("/api/id-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(idType)))
            .andExpect(status().isCreated());

        // Validate the IdType in the database
        List<IdType> idTypeList = idTypeRepository.findAll();
        assertThat(idTypeList).hasSize(databaseSizeBeforeCreate + 1);
        IdType testIdType = idTypeList.get(idTypeList.size() - 1);
        assertThat(testIdType.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testIdType.getCode()).isEqualTo(DEFAULT_CODE);

        // Validate the IdType in Elasticsearch
        verify(mockIdTypeSearchRepository, times(1)).save(testIdType);
    }

    @Test
    @Transactional
    void createIdTypeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = idTypeRepository.findAll().size();

        // Create the IdType with an existing ID
        idType.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restIdTypeMockMvc.perform(post("/api/id-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(idType)))
            .andExpect(status().isBadRequest());

        // Validate the IdType in the database
        List<IdType> idTypeList = idTypeRepository.findAll();
        assertThat(idTypeList).hasSize(databaseSizeBeforeCreate);

        // Validate the IdType in Elasticsearch
        verify(mockIdTypeSearchRepository, times(0)).save(idType);
    }


    @Test
    @Transactional
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = idTypeRepository.findAll().size();
        // set the field null
        idType.setName(null);

        // Create the IdType, which fails.

        restIdTypeMockMvc.perform(post("/api/id-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(idType)))
            .andExpect(status().isBadRequest());

        List<IdType> idTypeList = idTypeRepository.findAll();
        assertThat(idTypeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllIdTypes() throws Exception {
        // Initialize the database
        idTypeRepository.saveAndFlush(idType);

        // Get all the idTypeList
        restIdTypeMockMvc.perform(get("/api/id-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(idType.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)));
    }

    @Test
    @Transactional
    void getIdType() throws Exception {
        // Initialize the database
        idTypeRepository.saveAndFlush(idType);

        // Get the idType
        restIdTypeMockMvc.perform(get("/api/id-types/{id}", idType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(idType.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE));
    }

    @Test
    @Transactional
    void getNonExistingIdType() throws Exception {
        // Get the idType
        restIdTypeMockMvc.perform(get("/api/id-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void updateIdType() throws Exception {
        // Initialize the database
        idTypeService.save(idType);
        // As the test used the service layer, reset the Elasticsearch mock repository
        reset(mockIdTypeSearchRepository);

        int databaseSizeBeforeUpdate = idTypeRepository.findAll().size();

        // Update the idType
        IdType updatedIdType = idTypeRepository.findById(idType.getId()).get();
        // Disconnect from session so that the updates on updatedIdType are not directly saved in db
        em.detach(updatedIdType);
        updatedIdType
            .name(UPDATED_NAME)
            .code(UPDATED_CODE);

        restIdTypeMockMvc.perform(put("/api/id-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedIdType)))
            .andExpect(status().isOk());

        // Validate the IdType in the database
        List<IdType> idTypeList = idTypeRepository.findAll();
        assertThat(idTypeList).hasSize(databaseSizeBeforeUpdate);
        IdType testIdType = idTypeList.get(idTypeList.size() - 1);
        assertThat(testIdType.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testIdType.getCode()).isEqualTo(UPDATED_CODE);

        // Validate the IdType in Elasticsearch
        verify(mockIdTypeSearchRepository, times(1)).save(testIdType);
    }

    @Test
    @Transactional
    void updateNonExistingIdType() throws Exception {
        int databaseSizeBeforeUpdate = idTypeRepository.findAll().size();

        // Create the IdType

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restIdTypeMockMvc.perform(put("/api/id-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(idType)))
            .andExpect(status().isBadRequest());

        // Validate the IdType in the database
        List<IdType> idTypeList = idTypeRepository.findAll();
        assertThat(idTypeList).hasSize(databaseSizeBeforeUpdate);

        // Validate the IdType in Elasticsearch
        verify(mockIdTypeSearchRepository, times(0)).save(idType);
    }

    @Test
    @Transactional
    void deleteIdType() throws Exception {
        // Initialize the database
        idTypeService.save(idType);

        int databaseSizeBeforeDelete = idTypeRepository.findAll().size();

        // Delete the idType
        restIdTypeMockMvc.perform(delete("/api/id-types/{id}", idType.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<IdType> idTypeList = idTypeRepository.findAll();
        assertThat(idTypeList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the IdType in Elasticsearch
        verify(mockIdTypeSearchRepository, times(1)).deleteById(idType.getId());
    }

    @Test
    @Transactional
    void searchIdType() throws Exception {
        // Initialize the database
        idTypeService.save(idType);
        when(mockIdTypeSearchRepository.search(queryStringQuery("id:" + idType.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(idType), PageRequest.of(0, 1), 1));
        // Search the idType
        restIdTypeMockMvc.perform(get("/api/_search/id-types?query=id:" + idType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(idType.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)));
    }

    @Test
    @Transactional
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(IdType.class);
        IdType idType1 = new IdType();
        idType1.setId(1L);
        IdType idType2 = new IdType();
        idType2.setId(idType1.getId());
        assertThat(idType1).isEqualTo(idType2);
        idType2.setId(2L);
        assertThat(idType1).isNotEqualTo(idType2);
        idType1.setId(null);
        assertThat(idType1).isNotEqualTo(idType2);
    }
}
