package com.pagomundo.web.rest;

import com.pagomundo.PagoMundoApiApp;
import com.pagomundo.domain.TransactionGroup;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.repository.TransactionGroupRepository;
import com.pagomundo.repository.search.TransactionGroupSearchRepository;
import com.pagomundo.service.ExtendedUserService;
import com.pagomundo.service.NotificationReceiverService;
import com.pagomundo.service.TransactionGroupService;
import com.pagomundo.service.TransactionService;
import com.pagomundo.service.TranslateService;
import com.pagomundo.web.rest.errors.ExceptionTranslator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;

import static com.pagomundo.web.rest.TestUtil.createFormattingConversionService;
import static com.pagomundo.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration tests for the {@link TransactionGroupResource} REST controller.
 */
@SpringBootTest(classes = PagoMundoApiApp.class)
public class TransactionGroupResourceIT {

    private static final ZonedDateTime DEFAULT_DATE_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_CREATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_DATE_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final Status DEFAULT_STATUS = Status.CREATED;
    private static final Status UPDATED_STATUS = Status.IN_PROCESS;

    private static final String DEFAULT_FILE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FILE_NAME = "BBBBBBBBBB";

    private static final Float DEFAULT_EXCHANGE_RATE = 1F;
    private static final Float UPDATED_EXCHANGE_RATE = 2F;
    private static final Float SMALLER_EXCHANGE_RATE = 1F - 1F;

    private static final String DEFAULT_FILE_NAME_FROM_BANK = "AAAAAAAAAA";
    private static final String UPDATED_FILE_NAME_FROM_BANK = "BBBBBBBBBB";

    @Autowired
    private TransactionGroupRepository transactionGroupRepository;

    @Autowired
    private TransactionGroupService transactionGroupService;

    @Autowired
    private NotificationReceiverService notificationReceiverService;

    @Autowired
    private ExtendedUserService extendedUserService;

    @Autowired
    private TransactionService transactionService;

    /**
     * This repository is mocked in the com.pagomundo.repository.search test package.
     *
     * @see com.pagomundo.repository.search.TransactionGroupSearchRepositoryMockConfiguration
     */
    @Autowired
    private TransactionGroupSearchRepository mockTransactionGroupSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTransactionGroupMockMvc;

    private TransactionGroup transactionGroup;

    @Autowired
    private TranslateService translateService;

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TransactionGroup createEntity(EntityManager em) {
        TransactionGroup transactionGroup = new TransactionGroup()
            .dateCreated(DEFAULT_DATE_CREATED)
            .status(DEFAULT_STATUS)
            .fileName(DEFAULT_FILE_NAME)
            .exchangeRate(DEFAULT_EXCHANGE_RATE)
            .fileNameFromBank(DEFAULT_FILE_NAME_FROM_BANK);
        return transactionGroup;
    }

    /**
     * Create an updated entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TransactionGroup createUpdatedEntity(EntityManager em) {
        TransactionGroup transactionGroup = new TransactionGroup()
            .dateCreated(UPDATED_DATE_CREATED)
            .status(UPDATED_STATUS)
            .fileName(UPDATED_FILE_NAME)
            .exchangeRate(UPDATED_EXCHANGE_RATE)
            .fileNameFromBank(UPDATED_FILE_NAME_FROM_BANK);
        return transactionGroup;
    }

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TransactionGroupResource transactionGroupResource = new TransactionGroupResource(transactionGroupService, transactionService, notificationReceiverService, extendedUserService, translateService);
        this.restTransactionGroupMockMvc = MockMvcBuilders.standaloneSetup(transactionGroupResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    @BeforeEach
    public void initTest() {
        transactionGroup = createEntity(em);
    }

    @Test
    @Transactional
    public void createTransactionGroup() throws Exception {
        int databaseSizeBeforeCreate = transactionGroupRepository.findAll().size();

        // Create the TransactionGroup
        restTransactionGroupMockMvc.perform(post("/api/transaction-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transactionGroup)))
            .andExpect(status().isCreated());

        // Validate the TransactionGroup in the database
        List<TransactionGroup> transactionGroupList = transactionGroupRepository.findAll();
        assertThat(transactionGroupList).hasSize(databaseSizeBeforeCreate + 1);
        TransactionGroup testTransactionGroup = transactionGroupList.get(transactionGroupList.size() - 1);
        assertThat(testTransactionGroup.getDateCreated()).isEqualTo(DEFAULT_DATE_CREATED);
        assertThat(testTransactionGroup.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testTransactionGroup.getFileName()).isEqualTo(DEFAULT_FILE_NAME);
        assertThat(testTransactionGroup.getExchangeRate()).isEqualTo(DEFAULT_EXCHANGE_RATE);
        assertThat(testTransactionGroup.getFileNameFromBank()).isEqualTo(DEFAULT_FILE_NAME_FROM_BANK);

        // Validate the TransactionGroup in Elasticsearch
        verify(mockTransactionGroupSearchRepository, times(1)).save(testTransactionGroup);
    }

    @Test
    @Transactional
    public void createTransactionGroupWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = transactionGroupRepository.findAll().size();

        // Create the TransactionGroup with an existing ID
        transactionGroup.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTransactionGroupMockMvc.perform(post("/api/transaction-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transactionGroup)))
            .andExpect(status().isBadRequest());

        // Validate the TransactionGroup in the database
        List<TransactionGroup> transactionGroupList = transactionGroupRepository.findAll();
        assertThat(transactionGroupList).hasSize(databaseSizeBeforeCreate);

        // Validate the TransactionGroup in Elasticsearch
        verify(mockTransactionGroupSearchRepository, times(0)).save(transactionGroup);
    }


    @Test
    @Transactional
    public void getAllTransactionGroups() throws Exception {
        // Initialize the database
        transactionGroupRepository.saveAndFlush(transactionGroup);

        // Get all the transactionGroupList
        restTransactionGroupMockMvc.perform(get("/api/transaction-groups?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(transactionGroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateCreated").value(hasItem(sameInstant(DEFAULT_DATE_CREATED))))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].fileName").value(hasItem(DEFAULT_FILE_NAME.toString())))
            .andExpect(jsonPath("$.[*].exchangeRate").value(hasItem(DEFAULT_EXCHANGE_RATE.doubleValue())))
            .andExpect(jsonPath("$.[*].fileNameFromBank").value(hasItem(DEFAULT_FILE_NAME_FROM_BANK.toString())));
    }

    @Test
    @Transactional
    public void getTransactionGroup() throws Exception {
        // Initialize the database
        transactionGroupRepository.saveAndFlush(transactionGroup);

        // Get the transactionGroup
        restTransactionGroupMockMvc.perform(get("/api/transaction-groups/{id}", transactionGroup.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(transactionGroup.getId().intValue()))
            .andExpect(jsonPath("$.dateCreated").value(sameInstant(DEFAULT_DATE_CREATED)))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.fileName").value(DEFAULT_FILE_NAME.toString()))
            .andExpect(jsonPath("$.exchangeRate").value(DEFAULT_EXCHANGE_RATE.doubleValue()))
            .andExpect(jsonPath("$.fileNameFromBank").value(DEFAULT_FILE_NAME_FROM_BANK.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTransactionGroup() throws Exception {
        // Get the transactionGroup
        restTransactionGroupMockMvc.perform(get("/api/transaction-groups/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTransactionGroup() throws Exception {
        // Initialize the database
        transactionGroupService.save(transactionGroup);
        // As the test used the service layer, reset the Elasticsearch mock repository
        reset(mockTransactionGroupSearchRepository);

        int databaseSizeBeforeUpdate = transactionGroupRepository.findAll().size();

        // Update the transactionGroup
        TransactionGroup updatedTransactionGroup = transactionGroupRepository.findById(transactionGroup.getId()).get();
        // Disconnect from session so that the updates on updatedTransactionGroup are not directly saved in db
        em.detach(updatedTransactionGroup);
        updatedTransactionGroup
            .dateCreated(UPDATED_DATE_CREATED)
            .status(UPDATED_STATUS)
            .fileName(UPDATED_FILE_NAME)
            .exchangeRate(UPDATED_EXCHANGE_RATE)
            .fileNameFromBank(UPDATED_FILE_NAME_FROM_BANK);

        restTransactionGroupMockMvc.perform(put("/api/transaction-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTransactionGroup)))
            .andExpect(status().isOk());

        // Validate the TransactionGroup in the database
        List<TransactionGroup> transactionGroupList = transactionGroupRepository.findAll();
        assertThat(transactionGroupList).hasSize(databaseSizeBeforeUpdate);
        TransactionGroup testTransactionGroup = transactionGroupList.get(transactionGroupList.size() - 1);
        assertThat(testTransactionGroup.getDateCreated()).isEqualTo(UPDATED_DATE_CREATED);
        assertThat(testTransactionGroup.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testTransactionGroup.getFileName()).isEqualTo(UPDATED_FILE_NAME);
        assertThat(testTransactionGroup.getExchangeRate()).isEqualTo(UPDATED_EXCHANGE_RATE);
        assertThat(testTransactionGroup.getFileNameFromBank()).isEqualTo(UPDATED_FILE_NAME_FROM_BANK);

        // Validate the TransactionGroup in Elasticsearch
        verify(mockTransactionGroupSearchRepository, times(1)).save(testTransactionGroup);
    }

    @Test
    @Transactional
    public void updateNonExistingTransactionGroup() throws Exception {
        int databaseSizeBeforeUpdate = transactionGroupRepository.findAll().size();

        // Create the TransactionGroup

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTransactionGroupMockMvc.perform(put("/api/transaction-groups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transactionGroup)))
            .andExpect(status().isBadRequest());

        // Validate the TransactionGroup in the database
        List<TransactionGroup> transactionGroupList = transactionGroupRepository.findAll();
        assertThat(transactionGroupList).hasSize(databaseSizeBeforeUpdate);

        // Validate the TransactionGroup in Elasticsearch
        verify(mockTransactionGroupSearchRepository, times(0)).save(transactionGroup);
    }

    @Test
    @Transactional
    public void deleteTransactionGroup() throws Exception {
        // Initialize the database
        transactionGroupService.save(transactionGroup);

        int databaseSizeBeforeDelete = transactionGroupRepository.findAll().size();

        // Delete the transactionGroup
        restTransactionGroupMockMvc.perform(delete("/api/transaction-groups/{id}", transactionGroup.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TransactionGroup> transactionGroupList = transactionGroupRepository.findAll();
        assertThat(transactionGroupList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the TransactionGroup in Elasticsearch
        verify(mockTransactionGroupSearchRepository, times(1)).deleteById(transactionGroup.getId());
    }

    @Test
    @Transactional
    public void searchTransactionGroup() throws Exception {
        // Initialize the database
        transactionGroupService.save(transactionGroup);
        when(mockTransactionGroupSearchRepository.search(queryStringQuery("id:" + transactionGroup.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(transactionGroup), PageRequest.of(0, 1), 1));
        // Search the transactionGroup
        restTransactionGroupMockMvc.perform(get("/api/_search/transaction-groups?query=id:" + transactionGroup.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(transactionGroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateCreated").value(hasItem(sameInstant(DEFAULT_DATE_CREATED))))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].fileName").value(hasItem(DEFAULT_FILE_NAME)))
            .andExpect(jsonPath("$.[*].exchangeRate").value(hasItem(DEFAULT_EXCHANGE_RATE.doubleValue())))
            .andExpect(jsonPath("$.[*].fileNameFromBank").value(hasItem(DEFAULT_FILE_NAME_FROM_BANK)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TransactionGroup.class);
        TransactionGroup transactionGroup1 = new TransactionGroup();
        transactionGroup1.setId(1L);
        TransactionGroup transactionGroup2 = new TransactionGroup();
        transactionGroup2.setId(transactionGroup1.getId());
        assertThat(transactionGroup1).isEqualTo(transactionGroup2);
        transactionGroup2.setId(2L);
        assertThat(transactionGroup1).isNotEqualTo(transactionGroup2);
        transactionGroup1.setId(null);
        assertThat(transactionGroup1).isNotEqualTo(transactionGroup2);
    }
}
