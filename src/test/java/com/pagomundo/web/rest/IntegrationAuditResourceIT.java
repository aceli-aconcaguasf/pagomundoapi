package com.pagomundo.web.rest;

import com.pagomundo.PagoMundoApiApp;
import com.pagomundo.domain.IntegrationAudit;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.repository.IntegrationAuditRepository;
import com.pagomundo.repository.search.IntegrationAuditSearchRepository;
import com.pagomundo.service.TranslateService;
import com.pagomundo.service.dto.integrations.IntegrationAuditDTO;
import com.pagomundo.service.integrations.IntegrationAuditService;
import com.pagomundo.service.mapper.IntegrationAuditMapper;
import com.pagomundo.web.rest.errors.ExceptionTranslator;
import com.pagomundo.web.rest.integrations.IntegrationAuditResource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;

import static com.pagomundo.web.rest.TestUtil.createFormattingConversionService;
import static com.pagomundo.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration tests for the {@link IntegrationAuditResource} REST controller.
 */
@SpringBootTest(classes = PagoMundoApiApp.class)
public class IntegrationAuditResourceIT {

    private static final ZonedDateTime DEFAULT_REQUEST_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_REQUEST_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_REQUEST_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final String DEFAULT_REQUEST_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_REQUEST_MESSAGE = "BBBBBBBBBB";

    private static final String DEFAULT_REQUEST_ENDPOINT = "AAAAAAAAAA";
    private static final String UPDATED_REQUEST_ENDPOINT = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_RESPONSE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_RESPONSE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_RESPONSE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final String DEFAULT_RESPONSE_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_RESPONSE_MESSAGE = "BBBBBBBBBB";

    private static final Long DEFAULT_ENTITY_ID = 1L;
    private static final Long UPDATED_ENTITY_ID = 2L;
    private static final Long SMALLER_ENTITY_ID = 1L - 1L;

    private static final String DEFAULT_ENTITY = "AAAAAAAAAA";
    private static final String UPDATED_ENTITY = "BBBBBBBBBB";

    private static final String DEFAULT_PROVIDER_ID = "AAAAAAAAAA";
    private static final String UPDATED_PROVIDER_ID = "BBBBBBBBBB";

    private static final String DEFAULT_PROVIDER = "AAAAAAAAAA";
    private static final String UPDATED_PROVIDER = "BBBBBBBBBB";

    private static final String DEFAULT_EXTRA_RESPONSE_ENDPOINT = "AAAAAAAAAA";
    private static final String UPDATED_EXTRA_RESPONSE_ENDPOINT = "BBBBBBBBBB";

    private static final Status DEFAULT_STATUS = Status.CREATED;
    private static final Status UPDATED_STATUS = Status.IN_PROCESS;

    private static final String DEFAULT_STATUS_REASON = "AAAAAAAAAA";
    private static final String UPDATED_STATUS_REASON = "BBBBBBBBBB";

    private static final Integer DEFAULT_RETRIES = 1;
    private static final Integer UPDATED_RETRIES = 2;
    private static final Integer SMALLER_RETRIES = 1 - 1;

    private static final Integer DEFAULT_MAX_RETRIES = 1;
    private static final Integer UPDATED_MAX_RETRIES = 2;
    private static final Integer SMALLER_MAX_RETRIES = 1 - 1;

    @Autowired
    private IntegrationAuditRepository integrationAuditRepository;

    @Autowired
    private IntegrationAuditMapper integrationAuditMapper;

    @Autowired
    private IntegrationAuditService integrationAuditService;

    @Autowired
    private TranslateService translateService;

    /**
     * This repository is mocked in the com.pagomundo.repository.search test package.
     *
     * @see com.pagomundo.repository.search.IntegrationAuditSearchRepositoryMockConfiguration
     */
    @Autowired
    private IntegrationAuditSearchRepository mockIntegrationAuditSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restIntegrationAuditMockMvc;

    private IntegrationAudit integrationAudit;


    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IntegrationAudit createEntity(EntityManager em) {
        IntegrationAudit integrationAudit = new IntegrationAudit()
            .requestDate(DEFAULT_REQUEST_DATE)
            .requestMessage(DEFAULT_REQUEST_MESSAGE)
            .requestEndpoint(DEFAULT_REQUEST_ENDPOINT)
            .responseDate(DEFAULT_RESPONSE_DATE)
            .responseMessage(DEFAULT_RESPONSE_MESSAGE)
            .entityId(DEFAULT_ENTITY_ID)
            .entity(DEFAULT_ENTITY)
            .providerId(DEFAULT_PROVIDER_ID)
            .provider(DEFAULT_PROVIDER)
            .extraResponseEndpoint(DEFAULT_EXTRA_RESPONSE_ENDPOINT)
            .status(DEFAULT_STATUS)
            .statusReason(DEFAULT_STATUS_REASON)
            .retries(DEFAULT_RETRIES)
            .maxRetries(DEFAULT_MAX_RETRIES);
        return integrationAudit;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IntegrationAudit createUpdatedEntity(EntityManager em) {
        IntegrationAudit integrationAudit = new IntegrationAudit()
            .requestDate(UPDATED_REQUEST_DATE)
            .requestMessage(UPDATED_REQUEST_MESSAGE)
            .requestEndpoint(UPDATED_REQUEST_ENDPOINT)
            .responseDate(UPDATED_RESPONSE_DATE)
            .responseMessage(UPDATED_RESPONSE_MESSAGE)
            .entityId(UPDATED_ENTITY_ID)
            .entity(UPDATED_ENTITY)
            .providerId(UPDATED_PROVIDER_ID)
            .provider(UPDATED_PROVIDER)
            .extraResponseEndpoint(UPDATED_EXTRA_RESPONSE_ENDPOINT)
            .status(UPDATED_STATUS)
            .statusReason(UPDATED_STATUS_REASON)
            .retries(UPDATED_RETRIES)
            .maxRetries(UPDATED_MAX_RETRIES);
        return integrationAudit;
    }

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final IntegrationAuditResource integrationAuditResource = new IntegrationAuditResource(integrationAuditService, translateService);
        this.restIntegrationAuditMockMvc = MockMvcBuilders.standaloneSetup(integrationAuditResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    @BeforeEach
    public void initTest() {
        integrationAudit = createEntity(em);
    }

    @Test
    @Transactional
    public void createIntegrationAudit() throws Exception {
        int databaseSizeBeforeCreate = integrationAuditRepository.findAll().size();

        // Create the IntegrationAudit
        IntegrationAuditDTO integrationAuditDTO = integrationAuditMapper.toDto(integrationAudit);
        restIntegrationAuditMockMvc.perform(post("/api/integration-audits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(integrationAuditDTO)))
            .andExpect(status().isCreated());

        // Validate the IntegrationAudit in the database
        List<IntegrationAudit> integrationAuditList = integrationAuditRepository.findAll();
        assertThat(integrationAuditList).hasSize(databaseSizeBeforeCreate + 1);
        IntegrationAudit testIntegrationAudit = integrationAuditList.get(integrationAuditList.size() - 1);
        assertThat(testIntegrationAudit.getRequestDate()).isEqualTo(DEFAULT_REQUEST_DATE);
        assertThat(testIntegrationAudit.getRequestMessage()).isEqualTo(DEFAULT_REQUEST_MESSAGE);
        assertThat(testIntegrationAudit.getRequestEndpoint()).isEqualTo(DEFAULT_REQUEST_ENDPOINT);
        assertThat(testIntegrationAudit.getResponseDate()).isEqualTo(DEFAULT_RESPONSE_DATE);
        assertThat(testIntegrationAudit.getResponseMessage()).isEqualTo(DEFAULT_RESPONSE_MESSAGE);
        assertThat(testIntegrationAudit.getEntityId()).isEqualTo(DEFAULT_ENTITY_ID);
        assertThat(testIntegrationAudit.getEntity()).isEqualTo(DEFAULT_ENTITY);
        assertThat(testIntegrationAudit.getProviderId()).isEqualTo(DEFAULT_PROVIDER_ID);
        assertThat(testIntegrationAudit.getProvider()).isEqualTo(DEFAULT_PROVIDER);
        assertThat(testIntegrationAudit.getExtraResponseEndpoint()).isEqualTo(DEFAULT_EXTRA_RESPONSE_ENDPOINT);
        assertThat(testIntegrationAudit.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testIntegrationAudit.getStatusReason()).isEqualTo(DEFAULT_STATUS_REASON);
        assertThat(testIntegrationAudit.getRetries()).isEqualTo(DEFAULT_RETRIES);
        assertThat(testIntegrationAudit.getMaxRetries()).isEqualTo(DEFAULT_MAX_RETRIES);

        // Validate the IntegrationAudit in Elasticsearch
        verify(mockIntegrationAuditSearchRepository, times(1)).save(testIntegrationAudit);
    }

    @Test
    @Transactional
    public void createIntegrationAuditWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = integrationAuditRepository.findAll().size();

        // Create the IntegrationAudit with an existing ID
        integrationAudit.setId(1L);
        IntegrationAuditDTO integrationAuditDTO = integrationAuditMapper.toDto(integrationAudit);

        // An entity with an existing ID cannot be created, so this API call must fail
        restIntegrationAuditMockMvc.perform(post("/api/integration-audits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(integrationAuditDTO)))
            .andExpect(status().isBadRequest());

        // Validate the IntegrationAudit in the database
        List<IntegrationAudit> integrationAuditList = integrationAuditRepository.findAll();
        assertThat(integrationAuditList).hasSize(databaseSizeBeforeCreate);

        // Validate the IntegrationAudit in Elasticsearch
        verify(mockIntegrationAuditSearchRepository, times(0)).save(integrationAudit);
    }


    @Test
    @Transactional
    public void getAllIntegrationAudits() throws Exception {
        // Initialize the database
        integrationAuditRepository.saveAndFlush(integrationAudit);

        // Get all the integrationAuditList
        restIntegrationAuditMockMvc.perform(get("/api/integration-audits?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(integrationAudit.getId().intValue())))
            .andExpect(jsonPath("$.[*].requestDate").value(hasItem(sameInstant(DEFAULT_REQUEST_DATE))))
            .andExpect(jsonPath("$.[*].requestMessage").value(hasItem(DEFAULT_REQUEST_MESSAGE)))
            .andExpect(jsonPath("$.[*].requestEndpoint").value(hasItem(DEFAULT_REQUEST_ENDPOINT)))
            .andExpect(jsonPath("$.[*].responseDate").value(hasItem(sameInstant(DEFAULT_RESPONSE_DATE))))
            .andExpect(jsonPath("$.[*].responseMessage").value(hasItem(DEFAULT_RESPONSE_MESSAGE)))
            .andExpect(jsonPath("$.[*].entityId").value(hasItem(DEFAULT_ENTITY_ID.intValue())))
            .andExpect(jsonPath("$.[*].entity").value(hasItem(DEFAULT_ENTITY)))
            .andExpect(jsonPath("$.[*].providerId").value(hasItem(DEFAULT_PROVIDER_ID)))
            .andExpect(jsonPath("$.[*].provider").value(hasItem(DEFAULT_PROVIDER)))
            .andExpect(jsonPath("$.[*].extraResponseEndpoint").value(hasItem(DEFAULT_EXTRA_RESPONSE_ENDPOINT)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].statusReason").value(hasItem(DEFAULT_STATUS_REASON)))
            .andExpect(jsonPath("$.[*].retries").value(hasItem(DEFAULT_RETRIES)))
            .andExpect(jsonPath("$.[*].maxRetries").value(hasItem(DEFAULT_MAX_RETRIES)));
    }

    @Test
    @Transactional
    public void getIntegrationAudit() throws Exception {
        // Initialize the database
        integrationAuditRepository.saveAndFlush(integrationAudit);

        // Get the integrationAudit
        restIntegrationAuditMockMvc.perform(get("/api/integration-audits/{id}", integrationAudit.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(integrationAudit.getId().intValue()))
            .andExpect(jsonPath("$.requestDate").value(sameInstant(DEFAULT_REQUEST_DATE)))
            .andExpect(jsonPath("$.requestMessage").value(DEFAULT_REQUEST_MESSAGE))
            .andExpect(jsonPath("$.requestEndpoint").value(DEFAULT_REQUEST_ENDPOINT))
            .andExpect(jsonPath("$.responseDate").value(sameInstant(DEFAULT_RESPONSE_DATE)))
            .andExpect(jsonPath("$.responseMessage").value(DEFAULT_RESPONSE_MESSAGE))
            .andExpect(jsonPath("$.entityId").value(DEFAULT_ENTITY_ID.intValue()))
            .andExpect(jsonPath("$.entity").value(DEFAULT_ENTITY))
            .andExpect(jsonPath("$.providerId").value(DEFAULT_PROVIDER_ID))
            .andExpect(jsonPath("$.provider").value(DEFAULT_PROVIDER))
            .andExpect(jsonPath("$.extraResponseEndpoint").value(DEFAULT_EXTRA_RESPONSE_ENDPOINT))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.statusReason").value(DEFAULT_STATUS_REASON))
            .andExpect(jsonPath("$.retries").value(DEFAULT_RETRIES))
            .andExpect(jsonPath("$.maxRetries").value(DEFAULT_MAX_RETRIES));
    }

    @Test
    @Transactional
    public void getNonExistingIntegrationAudit() throws Exception {
        // Get the integrationAudit
        restIntegrationAuditMockMvc.perform(get("/api/integration-audits/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateIntegrationAudit() throws Exception {
        // Initialize the database
        integrationAuditRepository.saveAndFlush(integrationAudit);

        int databaseSizeBeforeUpdate = integrationAuditRepository.findAll().size();

        // Update the integrationAudit
        IntegrationAudit updatedIntegrationAudit = integrationAuditRepository.findById(integrationAudit.getId()).get();
        // Disconnect from session so that the updates on updatedIntegrationAudit are not directly saved in db
        em.detach(updatedIntegrationAudit);
        updatedIntegrationAudit
            .requestDate(UPDATED_REQUEST_DATE)
            .requestMessage(UPDATED_REQUEST_MESSAGE)
            .requestEndpoint(UPDATED_REQUEST_ENDPOINT)
            .responseDate(UPDATED_RESPONSE_DATE)
            .responseMessage(UPDATED_RESPONSE_MESSAGE)
            .entityId(UPDATED_ENTITY_ID)
            .entity(UPDATED_ENTITY)
            .providerId(UPDATED_PROVIDER_ID)
            .provider(UPDATED_PROVIDER)
            .extraResponseEndpoint(UPDATED_EXTRA_RESPONSE_ENDPOINT)
            .status(UPDATED_STATUS)
            .statusReason(UPDATED_STATUS_REASON)
            .retries(UPDATED_RETRIES)
            .maxRetries(UPDATED_MAX_RETRIES);
        IntegrationAuditDTO integrationAuditDTO = integrationAuditMapper.toDto(updatedIntegrationAudit);

        restIntegrationAuditMockMvc.perform(put("/api/integration-audits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(integrationAuditDTO)))
            .andExpect(status().isOk());

        // Validate the IntegrationAudit in the database
        List<IntegrationAudit> integrationAuditList = integrationAuditRepository.findAll();
        assertThat(integrationAuditList).hasSize(databaseSizeBeforeUpdate);
        IntegrationAudit testIntegrationAudit = integrationAuditList.get(integrationAuditList.size() - 1);
        assertThat(testIntegrationAudit.getRequestDate()).isEqualTo(UPDATED_REQUEST_DATE);
        assertThat(testIntegrationAudit.getRequestMessage()).isEqualTo(UPDATED_REQUEST_MESSAGE);
        assertThat(testIntegrationAudit.getRequestEndpoint()).isEqualTo(UPDATED_REQUEST_ENDPOINT);
        assertThat(testIntegrationAudit.getResponseDate()).isEqualTo(UPDATED_RESPONSE_DATE);
        assertThat(testIntegrationAudit.getResponseMessage()).isEqualTo(UPDATED_RESPONSE_MESSAGE);
        assertThat(testIntegrationAudit.getEntityId()).isEqualTo(UPDATED_ENTITY_ID);
        assertThat(testIntegrationAudit.getEntity()).isEqualTo(UPDATED_ENTITY);
        assertThat(testIntegrationAudit.getProviderId()).isEqualTo(UPDATED_PROVIDER_ID);
        assertThat(testIntegrationAudit.getProvider()).isEqualTo(UPDATED_PROVIDER);
        assertThat(testIntegrationAudit.getExtraResponseEndpoint()).isEqualTo(UPDATED_EXTRA_RESPONSE_ENDPOINT);
        assertThat(testIntegrationAudit.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testIntegrationAudit.getStatusReason()).isEqualTo(UPDATED_STATUS_REASON);
        assertThat(testIntegrationAudit.getRetries()).isEqualTo(UPDATED_RETRIES);
        assertThat(testIntegrationAudit.getMaxRetries()).isEqualTo(UPDATED_MAX_RETRIES);

        // Validate the IntegrationAudit in Elasticsearch
        verify(mockIntegrationAuditSearchRepository, times(1)).save(testIntegrationAudit);
    }

    @Test
    @Transactional
    public void updateNonExistingIntegrationAudit() throws Exception {
        int databaseSizeBeforeUpdate = integrationAuditRepository.findAll().size();

        // Create the IntegrationAudit
        IntegrationAuditDTO integrationAuditDTO = integrationAuditMapper.toDto(integrationAudit);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restIntegrationAuditMockMvc.perform(put("/api/integration-audits")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(integrationAuditDTO)))
            .andExpect(status().isBadRequest());

        // Validate the IntegrationAudit in the database
        List<IntegrationAudit> integrationAuditList = integrationAuditRepository.findAll();
        assertThat(integrationAuditList).hasSize(databaseSizeBeforeUpdate);

        // Validate the IntegrationAudit in Elasticsearch
        verify(mockIntegrationAuditSearchRepository, times(0)).save(integrationAudit);
    }

    @Test
    @Transactional
    public void deleteIntegrationAudit() throws Exception {
        // Initialize the database
        integrationAuditRepository.saveAndFlush(integrationAudit);

        int databaseSizeBeforeDelete = integrationAuditRepository.findAll().size();

        // Delete the integrationAudit
        restIntegrationAuditMockMvc.perform(delete("/api/integration-audits/{id}", integrationAudit.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<IntegrationAudit> integrationAuditList = integrationAuditRepository.findAll();
        assertThat(integrationAuditList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the IntegrationAudit in Elasticsearch
        verify(mockIntegrationAuditSearchRepository, times(1)).deleteById(integrationAudit.getId());
    }

    @Test
    @Transactional
    public void searchIntegrationAudit() throws Exception {
        // Initialize the database
        integrationAuditRepository.saveAndFlush(integrationAudit);
        when(mockIntegrationAuditSearchRepository.search(queryStringQuery("id:" + integrationAudit.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(integrationAudit), PageRequest.of(0, 1), 1));
        // Search the integrationAudit
        restIntegrationAuditMockMvc.perform(get("/api/_search/integration-audits?query=id:" + integrationAudit.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(integrationAudit.getId().intValue())))
            .andExpect(jsonPath("$.[*].requestDate").value(hasItem(sameInstant(DEFAULT_REQUEST_DATE))))
            .andExpect(jsonPath("$.[*].requestMessage").value(hasItem(DEFAULT_REQUEST_MESSAGE)))
            .andExpect(jsonPath("$.[*].requestEndpoint").value(hasItem(DEFAULT_REQUEST_ENDPOINT)))
            .andExpect(jsonPath("$.[*].responseDate").value(hasItem(sameInstant(DEFAULT_RESPONSE_DATE))))
            .andExpect(jsonPath("$.[*].responseMessage").value(hasItem(DEFAULT_RESPONSE_MESSAGE)))
            .andExpect(jsonPath("$.[*].entityId").value(hasItem(DEFAULT_ENTITY_ID.intValue())))
            .andExpect(jsonPath("$.[*].entity").value(hasItem(DEFAULT_ENTITY)))
            .andExpect(jsonPath("$.[*].providerId").value(hasItem(DEFAULT_PROVIDER_ID)))
            .andExpect(jsonPath("$.[*].provider").value(hasItem(DEFAULT_PROVIDER)))
            .andExpect(jsonPath("$.[*].extraResponseEndpoint").value(hasItem(DEFAULT_EXTRA_RESPONSE_ENDPOINT)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].statusReason").value(hasItem(DEFAULT_STATUS_REASON)))
            .andExpect(jsonPath("$.[*].retries").value(hasItem(DEFAULT_RETRIES)))
            .andExpect(jsonPath("$.[*].maxRetries").value(hasItem(DEFAULT_MAX_RETRIES)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(IntegrationAudit.class);
        IntegrationAudit integrationAudit1 = new IntegrationAudit();
        integrationAudit1.setId(1L);
        IntegrationAudit integrationAudit2 = new IntegrationAudit();
        integrationAudit2.setId(integrationAudit1.getId());
        assertThat(integrationAudit1).isEqualTo(integrationAudit2);
        integrationAudit2.setId(2L);
        assertThat(integrationAudit1).isNotEqualTo(integrationAudit2);
        integrationAudit1.setId(null);
        assertThat(integrationAudit1).isNotEqualTo(integrationAudit2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(IntegrationAuditDTO.class);
        IntegrationAuditDTO integrationAuditDTO1 = new IntegrationAuditDTO();
        integrationAuditDTO1.setId(1L);
        IntegrationAuditDTO integrationAuditDTO2 = new IntegrationAuditDTO();
        assertThat(integrationAuditDTO1).isNotEqualTo(integrationAuditDTO2);
        integrationAuditDTO2.setId(integrationAuditDTO1.getId());
        assertThat(integrationAuditDTO1).isEqualTo(integrationAuditDTO2);
        integrationAuditDTO2.setId(2L);
        assertThat(integrationAuditDTO1).isNotEqualTo(integrationAuditDTO2);
        integrationAuditDTO1.setId(null);
        assertThat(integrationAuditDTO1).isNotEqualTo(integrationAuditDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(integrationAuditMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(integrationAuditMapper.fromId(null)).isNull();
    }
}
