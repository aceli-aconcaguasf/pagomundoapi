package com.pagomundo.web.rest;

import com.pagomundo.PagoMundoApiApp;
import com.pagomundo.domain.ExtendedUserFunds;
import com.pagomundo.repository.ExtendedUserFundsRepository;
import com.pagomundo.repository.search.ExtendedUserFundsSearchRepository;
import com.pagomundo.service.ExtendedUserFundsService;
import com.pagomundo.service.ExtendedUserService;
import com.pagomundo.service.NotificationReceiverService;
import com.pagomundo.service.TranslateService;
import com.pagomundo.web.rest.errors.ExceptionTranslator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;

import static com.pagomundo.web.rest.TestUtil.createFormattingConversionService;
import static com.pagomundo.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration tests for the {@link ExtendedUserFundsResource} REST controller.
 */
@SpringBootTest(classes = PagoMundoApiApp.class)
public class ExtendedUserFundsResourceIT {

    private static final ZonedDateTime DEFAULT_DATE_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_CREATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_DATE_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final BigDecimal DEFAULT_VALUE = new BigDecimal(1);
    private static final BigDecimal UPDATED_VALUE = new BigDecimal(2);
    private static final BigDecimal SMALLER_VALUE = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_BALANCE_BEFORE = new BigDecimal(1);
    private static final BigDecimal UPDATED_BALANCE_BEFORE = new BigDecimal(2);
    private static final BigDecimal SMALLER_BALANCE_BEFORE = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_BALANCE_AFTER = new BigDecimal(1);
    private static final BigDecimal UPDATED_BALANCE_AFTER = new BigDecimal(2);
    private static final BigDecimal SMALLER_BALANCE_AFTER = new BigDecimal(1 - 1);

    private static final String DEFAULT_REASON = "AAAAAAAAAA";
    private static final String UPDATED_REASON = "BBBBBBBBBB";

    @Autowired
    private ExtendedUserFundsRepository extendedUserFundsRepository;

    @Autowired
    private ExtendedUserFundsService extendedUserFundsService;

    @Autowired
    private ExtendedUserService extendedUserService;

    @Autowired
    private NotificationReceiverService notificationReceiverService;

    /**
     * This repository is mocked in the com.pagomundo.repository.search test package.
     *
     * @see com.pagomundo.repository.search.ExtendedUserFundsSearchRepositoryMockConfiguration
     */
    @Autowired
    private ExtendedUserFundsSearchRepository mockExtendedUserFundsSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restExtendedUserFundsMockMvc;

    private ExtendedUserFunds extendedUserFunds;

    @Autowired
    private TranslateService translateService;

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExtendedUserFunds createEntity(EntityManager em) {
        ExtendedUserFunds extendedUserFunds = new ExtendedUserFunds()
            .dateCreated(DEFAULT_DATE_CREATED)
            .value(DEFAULT_VALUE)
            .balanceBefore(DEFAULT_BALANCE_BEFORE)
            .balanceAfter(DEFAULT_BALANCE_AFTER)
            .reason(DEFAULT_REASON);
        return extendedUserFunds;
    }

    /**
     * Create an updated entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExtendedUserFunds createUpdatedEntity(EntityManager em) {
        ExtendedUserFunds extendedUserFunds = new ExtendedUserFunds()
            .dateCreated(UPDATED_DATE_CREATED)
            .value(UPDATED_VALUE)
            .balanceBefore(UPDATED_BALANCE_BEFORE)
            .balanceAfter(UPDATED_BALANCE_AFTER)
            .reason(UPDATED_REASON);
        return extendedUserFunds;
    }

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ExtendedUserFundsResource extendedUserFundsResource = new ExtendedUserFundsResource(extendedUserFundsService, extendedUserService, notificationReceiverService, translateService);
        this.restExtendedUserFundsMockMvc = MockMvcBuilders.standaloneSetup(extendedUserFundsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    @BeforeEach
    public void initTest() {
        extendedUserFunds = createEntity(em);
    }

    @Test
    @Transactional
    public void createExtendedUserFunds() throws Exception {
        int databaseSizeBeforeCreate = extendedUserFundsRepository.findAll().size();

        // Create the ExtendedUserFunds
        restExtendedUserFundsMockMvc.perform(post("/api/extended-user-funds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(extendedUserFunds)))
            .andExpect(status().isCreated());

        // Validate the ExtendedUserFunds in the database
        List<ExtendedUserFunds> extendedUserFundsList = extendedUserFundsRepository.findAll();
        assertThat(extendedUserFundsList).hasSize(databaseSizeBeforeCreate + 1);
        ExtendedUserFunds testExtendedUserFunds = extendedUserFundsList.get(extendedUserFundsList.size() - 1);
        assertThat(testExtendedUserFunds.getDateCreated()).isEqualTo(DEFAULT_DATE_CREATED);
        assertThat(testExtendedUserFunds.getValue()).isEqualTo(DEFAULT_VALUE);
        assertThat(testExtendedUserFunds.getBalanceBefore()).isEqualTo(DEFAULT_BALANCE_BEFORE);
        assertThat(testExtendedUserFunds.getBalanceAfter()).isEqualTo(DEFAULT_BALANCE_AFTER);
        assertThat(testExtendedUserFunds.getReason()).isEqualTo(DEFAULT_REASON);

        // Validate the ExtendedUserFunds in Elasticsearch
        verify(mockExtendedUserFundsSearchRepository, times(1)).save(testExtendedUserFunds);
    }

    @Test
    @Transactional
    public void createExtendedUserFundsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = extendedUserFundsRepository.findAll().size();

        // Create the ExtendedUserFunds with an existing ID
        extendedUserFunds.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restExtendedUserFundsMockMvc.perform(post("/api/extended-user-funds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(extendedUserFunds)))
            .andExpect(status().isBadRequest());

        // Validate the ExtendedUserFunds in the database
        List<ExtendedUserFunds> extendedUserFundsList = extendedUserFundsRepository.findAll();
        assertThat(extendedUserFundsList).hasSize(databaseSizeBeforeCreate);

        // Validate the ExtendedUserFunds in Elasticsearch
        verify(mockExtendedUserFundsSearchRepository, times(0)).save(extendedUserFunds);
    }


    @Test
    @Transactional
    public void getAllExtendedUserFunds() throws Exception {
        // Initialize the database
        extendedUserFundsRepository.saveAndFlush(extendedUserFunds);

        // Get all the extendedUserFundsList
        restExtendedUserFundsMockMvc.perform(get("/api/extended-user-funds?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(extendedUserFunds.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateCreated").value(hasItem(sameInstant(DEFAULT_DATE_CREATED))))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.intValue())))
            .andExpect(jsonPath("$.[*].balanceBefore").value(hasItem(DEFAULT_BALANCE_BEFORE.intValue())))
            .andExpect(jsonPath("$.[*].balanceAfter").value(hasItem(DEFAULT_BALANCE_AFTER.intValue())))
            .andExpect(jsonPath("$.[*].reason").value(hasItem(DEFAULT_REASON.toString())));
    }

    @Test
    @Transactional
    public void getExtendedUserFunds() throws Exception {
        // Initialize the database
        extendedUserFundsRepository.saveAndFlush(extendedUserFunds);

        // Get the extendedUserFunds
        restExtendedUserFundsMockMvc.perform(get("/api/extended-user-funds/{id}", extendedUserFunds.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(extendedUserFunds.getId().intValue()))
            .andExpect(jsonPath("$.dateCreated").value(sameInstant(DEFAULT_DATE_CREATED)))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE.intValue()))
            .andExpect(jsonPath("$.balanceBefore").value(DEFAULT_BALANCE_BEFORE.intValue()))
            .andExpect(jsonPath("$.balanceAfter").value(DEFAULT_BALANCE_AFTER.intValue()))
            .andExpect(jsonPath("$.reason").value(DEFAULT_REASON.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingExtendedUserFunds() throws Exception {
        // Get the extendedUserFunds
        restExtendedUserFundsMockMvc.perform(get("/api/extended-user-funds/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateExtendedUserFunds() throws Exception {
        // Initialize the database
        extendedUserFundsService.save(extendedUserFunds);
        // As the test used the service layer, reset the Elasticsearch mock repository
        reset(mockExtendedUserFundsSearchRepository);

        int databaseSizeBeforeUpdate = extendedUserFundsRepository.findAll().size();

        // Update the extendedUserFunds
        ExtendedUserFunds updatedExtendedUserFunds = extendedUserFundsRepository.findById(extendedUserFunds.getId()).get();
        // Disconnect from session so that the updates on updatedExtendedUserFunds are not directly saved in db
        em.detach(updatedExtendedUserFunds);
        updatedExtendedUserFunds
            .dateCreated(UPDATED_DATE_CREATED)
            .value(UPDATED_VALUE)
            .balanceBefore(UPDATED_BALANCE_BEFORE)
            .balanceAfter(UPDATED_BALANCE_AFTER)
            .reason(UPDATED_REASON);

        restExtendedUserFundsMockMvc.perform(put("/api/extended-user-funds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedExtendedUserFunds)))
            .andExpect(status().isOk());

        // Validate the ExtendedUserFunds in the database
        List<ExtendedUserFunds> extendedUserFundsList = extendedUserFundsRepository.findAll();
        assertThat(extendedUserFundsList).hasSize(databaseSizeBeforeUpdate);
        ExtendedUserFunds testExtendedUserFunds = extendedUserFundsList.get(extendedUserFundsList.size() - 1);
        assertThat(testExtendedUserFunds.getDateCreated()).isEqualTo(UPDATED_DATE_CREATED);
        assertThat(testExtendedUserFunds.getValue()).isEqualTo(UPDATED_VALUE);
        assertThat(testExtendedUserFunds.getBalanceBefore()).isEqualTo(UPDATED_BALANCE_BEFORE);
        assertThat(testExtendedUserFunds.getBalanceAfter()).isEqualTo(UPDATED_BALANCE_AFTER);
        assertThat(testExtendedUserFunds.getReason()).isEqualTo(UPDATED_REASON);

        // Validate the ExtendedUserFunds in Elasticsearch
        verify(mockExtendedUserFundsSearchRepository, times(1)).save(testExtendedUserFunds);
    }

    @Test
    @Transactional
    public void updateNonExistingExtendedUserFunds() throws Exception {
        int databaseSizeBeforeUpdate = extendedUserFundsRepository.findAll().size();

        // Create the ExtendedUserFunds

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExtendedUserFundsMockMvc.perform(put("/api/extended-user-funds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(extendedUserFunds)))
            .andExpect(status().isBadRequest());

        // Validate the ExtendedUserFunds in the database
        List<ExtendedUserFunds> extendedUserFundsList = extendedUserFundsRepository.findAll();
        assertThat(extendedUserFundsList).hasSize(databaseSizeBeforeUpdate);

        // Validate the ExtendedUserFunds in Elasticsearch
        verify(mockExtendedUserFundsSearchRepository, times(0)).save(extendedUserFunds);
    }

    @Test
    @Transactional
    public void deleteExtendedUserFunds() throws Exception {
        // Initialize the database
        extendedUserFundsService.save(extendedUserFunds);

        int databaseSizeBeforeDelete = extendedUserFundsRepository.findAll().size();

        // Delete the extendedUserFunds
        restExtendedUserFundsMockMvc.perform(delete("/api/extended-user-funds/{id}", extendedUserFunds.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ExtendedUserFunds> extendedUserFundsList = extendedUserFundsRepository.findAll();
        assertThat(extendedUserFundsList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the ExtendedUserFunds in Elasticsearch
        verify(mockExtendedUserFundsSearchRepository, times(1)).deleteById(extendedUserFunds.getId());
    }

    @Test
    @Transactional
    public void searchExtendedUserFunds() throws Exception {
        // Initialize the database
        extendedUserFundsService.save(extendedUserFunds);
        when(mockExtendedUserFundsSearchRepository.search(queryStringQuery("id:" + extendedUserFunds.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(extendedUserFunds), PageRequest.of(0, 1), 1));
        // Search the extendedUserFunds
        restExtendedUserFundsMockMvc.perform(get("/api/_search/extended-user-funds?query=id:" + extendedUserFunds.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(extendedUserFunds.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateCreated").value(hasItem(sameInstant(DEFAULT_DATE_CREATED))))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.intValue())))
            .andExpect(jsonPath("$.[*].balanceBefore").value(hasItem(DEFAULT_BALANCE_BEFORE.intValue())))
            .andExpect(jsonPath("$.[*].balanceAfter").value(hasItem(DEFAULT_BALANCE_AFTER.intValue())))
            .andExpect(jsonPath("$.[*].reason").value(hasItem(DEFAULT_REASON)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExtendedUserFunds.class);
        ExtendedUserFunds extendedUserFunds1 = new ExtendedUserFunds();
        extendedUserFunds1.setId(1L);
        ExtendedUserFunds extendedUserFunds2 = new ExtendedUserFunds();
        extendedUserFunds2.setId(extendedUserFunds1.getId());
        assertThat(extendedUserFunds1).isEqualTo(extendedUserFunds2);
        extendedUserFunds2.setId(2L);
        assertThat(extendedUserFunds1).isNotEqualTo(extendedUserFunds2);
        extendedUserFunds1.setId(null);
        assertThat(extendedUserFunds1).isNotEqualTo(extendedUserFunds2);
    }
}
