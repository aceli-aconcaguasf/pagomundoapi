package com.pagomundo.web.rest;

import com.pagomundo.PagoMundoApiApp;
import com.pagomundo.domain.Transaction;
import com.pagomundo.domain.enumeration.ProcessType;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.repository.TransactionRepository;
import com.pagomundo.repository.search.RefetchService;
import com.pagomundo.repository.search.TransactionSearchRepository;
import com.pagomundo.service.ExtendedUserService;
import com.pagomundo.service.FileService;
import com.pagomundo.service.NotificationReceiverService;
import com.pagomundo.service.TransactionService;
import com.pagomundo.service.TranslateService;
import com.pagomundo.web.rest.errors.ExceptionTranslator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;

import static com.pagomundo.web.rest.TestUtil.createFormattingConversionService;
import static com.pagomundo.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration tests for the {@link TransactionResource} REST controller.
 */
@SpringBootTest(classes = PagoMundoApiApp.class)
public class TransactionResourceIT {

    private static final ZonedDateTime DEFAULT_DATE_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_CREATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_DATE_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final ZonedDateTime DEFAULT_LAST_UPDATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_LAST_UPDATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_LAST_UPDATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final Status DEFAULT_STATUS = Status.CREATED;
    private static final Status UPDATED_STATUS = Status.IN_PROCESS;

    private static final String DEFAULT_STATUS_REASON = "AAAAAAAAAA";
    private static final String UPDATED_STATUS_REASON = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_AMOUNT_BEFORE_COMMISSION = new BigDecimal(1);
    private static final BigDecimal UPDATED_AMOUNT_BEFORE_COMMISSION = new BigDecimal(2);
    private static final BigDecimal SMALLER_AMOUNT_BEFORE_COMMISSION = new BigDecimal(1 - 1);

    private static final Float DEFAULT_BANK_COMMISSION = 1F;
    private static final Float UPDATED_BANK_COMMISSION = 2F;
    private static final Float SMALLER_BANK_COMMISSION = 1F - 1F;

    private static final Float DEFAULT_FX_COMMISSION = 1F;
    private static final Float UPDATED_FX_COMMISSION = 2F;
    private static final Float SMALLER_FX_COMMISSION = 1F - 1F;

    private static final Float DEFAULT_RECHARGE_COST = 1F;
    private static final Float UPDATED_RECHARGE_COST = 2F;
    private static final Float SMALLER_RECHARGE_COST = 1F - 1F;

    private static final BigDecimal DEFAULT_AMOUNT_AFTER_COMMISSION = new BigDecimal(1);
    private static final BigDecimal UPDATED_AMOUNT_AFTER_COMMISSION = new BigDecimal(2);
    private static final BigDecimal SMALLER_AMOUNT_AFTER_COMMISSION = new BigDecimal(1 - 1);

    private static final Float DEFAULT_EXCHANGE_RATE = 1F;
    private static final Float UPDATED_EXCHANGE_RATE = 2F;
    private static final Float SMALLER_EXCHANGE_RATE = 1F - 1F;

    private static final BigDecimal DEFAULT_AMOUNT_LOCAL_CURRENCY = new BigDecimal(1);
    private static final BigDecimal UPDATED_AMOUNT_LOCAL_CURRENCY = new BigDecimal(2);
    private static final BigDecimal SMALLER_AMOUNT_LOCAL_CURRENCY = new BigDecimal(1 - 1);

    private static final String DEFAULT_BANK_REFERENCE = "AAAAAAAAAA";
    private static final String UPDATED_BANK_REFERENCE = "BBBBBBBBBB";

    private static final String DEFAULT_FILE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FILE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_NOTES = "AAAAAAAAAA";
    private static final String UPDATED_NOTES = "BBBBBBBBBB";

    private static final Boolean DEFAULT_MUST_NOTIFY = false;
    private static final Boolean UPDATED_MUST_NOTIFY = true;

    private static final String DEFAULT_INFO_CHANGED = "AAAAAAAAAA";
    private static final String UPDATED_INFO_CHANGED = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_RESELLER_COMMISSION = new BigDecimal(1);
    private static final BigDecimal UPDATED_RESELLER_COMMISSION = new BigDecimal(2);
    private static final BigDecimal SMALLER_RESELLER_COMMISSION = new BigDecimal(1 - 1);

    private static final Float DEFAULT_RESELLER_FIXED_COMMISSION = 1F;
    private static final Float UPDATED_RESELLER_FIXED_COMMISSION = 2F;
    private static final Float SMALLER_RESELLER_FIXED_COMMISSION = 1F - 1F;

    private static final Float DEFAULT_RESELLER_PERCENTAGE_COMMISSION = 1F;
    private static final Float UPDATED_RESELLER_PERCENTAGE_COMMISSION = 2F;
    private static final Float SMALLER_RESELLER_PERCENTAGE_COMMISSION = 1F - 1F;

    private static final ProcessType DEFAULT_PROCESS_TYPE = ProcessType.FILE_EXPORT;
    private static final ProcessType UPDATED_PROCESS_TYPE = ProcessType.API_CALL;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private final TransactionService transactionService;

    @Autowired
    private RefetchService refetchService;

    @Autowired
    private ExtendedUserService extendedUserService;

    @Autowired
    private NotificationReceiverService notificationReceiverService;

    @Autowired
    private FileService fileService;


    /**
     * This repository is mocked in the com.pagomundo.repository.search test package.
     *
     * @see com.pagomundo.repository.search.TransactionSearchRepositoryMockConfiguration
     */
    @Autowired
    private TransactionSearchRepository mockTransactionSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTransactionMockMvc;

    private Transaction transaction;

    @Autowired
    private TranslateService translateService;

    public TransactionResourceIT(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Transaction createEntity(EntityManager em) {
        Transaction transaction = new Transaction()
            .dateCreated(DEFAULT_DATE_CREATED)
            .lastUpdated(DEFAULT_LAST_UPDATED)
            .status(DEFAULT_STATUS)
            .statusReason(DEFAULT_STATUS_REASON)
            .amountBeforeCommission(DEFAULT_AMOUNT_BEFORE_COMMISSION)
            .bankCommission(DEFAULT_BANK_COMMISSION)
            .fxCommission(DEFAULT_FX_COMMISSION)
            .rechargeCost(DEFAULT_RECHARGE_COST)
            .amountAfterCommission(DEFAULT_AMOUNT_AFTER_COMMISSION)
            .exchangeRate(DEFAULT_EXCHANGE_RATE)
            .amountLocalCurrency(DEFAULT_AMOUNT_LOCAL_CURRENCY)
            .bankReference(DEFAULT_BANK_REFERENCE)
            .fileName(DEFAULT_FILE_NAME)
            .description(DEFAULT_DESCRIPTION)
            .notes(DEFAULT_NOTES)
            .mustNotify(DEFAULT_MUST_NOTIFY)
            .infoChanged(DEFAULT_INFO_CHANGED)
            .resellerCommission(DEFAULT_RESELLER_COMMISSION)
            .resellerFixedCommission(DEFAULT_RESELLER_FIXED_COMMISSION)
            .resellerPercentageCommission(DEFAULT_RESELLER_PERCENTAGE_COMMISSION)
            .processType(DEFAULT_PROCESS_TYPE);
        return transaction;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Transaction createUpdatedEntity(EntityManager em) {
        Transaction transaction = new Transaction()
            .dateCreated(UPDATED_DATE_CREATED)
            .lastUpdated(UPDATED_LAST_UPDATED)
            .status(UPDATED_STATUS)
            .statusReason(UPDATED_STATUS_REASON)
            .amountBeforeCommission(UPDATED_AMOUNT_BEFORE_COMMISSION)
            .bankCommission(UPDATED_BANK_COMMISSION)
            .fxCommission(UPDATED_FX_COMMISSION)
            .rechargeCost(UPDATED_RECHARGE_COST)
            .amountAfterCommission(UPDATED_AMOUNT_AFTER_COMMISSION)
            .exchangeRate(UPDATED_EXCHANGE_RATE)
            .amountLocalCurrency(UPDATED_AMOUNT_LOCAL_CURRENCY)
            .bankReference(UPDATED_BANK_REFERENCE)
            .fileName(UPDATED_FILE_NAME)
            .description(UPDATED_DESCRIPTION)
            .notes(UPDATED_NOTES)
            .mustNotify(UPDATED_MUST_NOTIFY)
            .infoChanged(UPDATED_INFO_CHANGED)
            .resellerCommission(UPDATED_RESELLER_COMMISSION)
            .resellerFixedCommission(UPDATED_RESELLER_FIXED_COMMISSION)
            .resellerPercentageCommission(UPDATED_RESELLER_PERCENTAGE_COMMISSION)
            .processType(UPDATED_PROCESS_TYPE);
        return transaction;
    }

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TransactionResource transactionResource = new TransactionResource(
            refetchService, transactionService,
            extendedUserService,
            notificationReceiverService,
            fileService,
            translateService);
        this.restTransactionMockMvc = MockMvcBuilders.standaloneSetup(transactionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    @BeforeEach
    public void initTest() {
        transaction = createEntity(em);
    }

    @Test
    @Transactional
    public void createTransaction() throws Exception {
        int databaseSizeBeforeCreate = transactionRepository.findAll().size();

        // Create the Transaction
        restTransactionMockMvc.perform(post("/api/transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transaction)))
            .andExpect(status().isCreated());

        // Validate the Transaction in the database
        List<Transaction> transactionList = transactionRepository.findAll();
        assertThat(transactionList).hasSize(databaseSizeBeforeCreate + 1);
        Transaction testTransaction = transactionList.get(transactionList.size() - 1);
        assertThat(testTransaction.getDateCreated()).isEqualTo(DEFAULT_DATE_CREATED);
        assertThat(testTransaction.getLastUpdated()).isEqualTo(DEFAULT_LAST_UPDATED);
        assertThat(testTransaction.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testTransaction.getStatusReason()).isEqualTo(DEFAULT_STATUS_REASON);
        assertThat(testTransaction.getAmountBeforeCommission()).isEqualTo(DEFAULT_AMOUNT_BEFORE_COMMISSION);
        assertThat(testTransaction.getBankCommission()).isEqualTo(DEFAULT_BANK_COMMISSION);
        assertThat(testTransaction.getFxCommission()).isEqualTo(DEFAULT_FX_COMMISSION);
        assertThat(testTransaction.getRechargeCost()).isEqualTo(DEFAULT_RECHARGE_COST);
        assertThat(testTransaction.getAmountAfterCommission()).isEqualTo(DEFAULT_AMOUNT_AFTER_COMMISSION);
        assertThat(testTransaction.getExchangeRate()).isEqualTo(DEFAULT_EXCHANGE_RATE);
        assertThat(testTransaction.getAmountLocalCurrency()).isEqualTo(DEFAULT_AMOUNT_LOCAL_CURRENCY);
        assertThat(testTransaction.getBankReference()).isEqualTo(DEFAULT_BANK_REFERENCE);
        assertThat(testTransaction.getFileName()).isEqualTo(DEFAULT_FILE_NAME);
        assertThat(testTransaction.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testTransaction.getNotes()).isEqualTo(DEFAULT_NOTES);
        assertThat(testTransaction.isMustNotify()).isEqualTo(DEFAULT_MUST_NOTIFY);
        assertThat(testTransaction.getInfoChanged()).isEqualTo(DEFAULT_INFO_CHANGED);
        assertThat(testTransaction.getResellerCommission()).isEqualTo(DEFAULT_RESELLER_COMMISSION);
        assertThat(testTransaction.getResellerFixedCommission()).isEqualTo(DEFAULT_RESELLER_FIXED_COMMISSION);
        assertThat(testTransaction.getResellerPercentageCommission()).isEqualTo(DEFAULT_RESELLER_PERCENTAGE_COMMISSION);
        assertThat(testTransaction.getProcessType()).isEqualTo(DEFAULT_PROCESS_TYPE);

        // Validate the Transaction in Elasticsearch
        verify(mockTransactionSearchRepository, times(1)).save(testTransaction);
    }

    @Test
    @Transactional
    public void createTransactionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = transactionRepository.findAll().size();

        // Create the Transaction with an existing ID
        transaction.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTransactionMockMvc.perform(post("/api/transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transaction)))
            .andExpect(status().isBadRequest());

        // Validate the Transaction in the database
        List<Transaction> transactionList = transactionRepository.findAll();
        assertThat(transactionList).hasSize(databaseSizeBeforeCreate);

        // Validate the Transaction in Elasticsearch
        verify(mockTransactionSearchRepository, times(0)).save(transaction);
    }


    @Test
    @Transactional
    public void checkDateCreatedIsRequired() throws Exception {
        int databaseSizeBeforeTest = transactionRepository.findAll().size();
        // set the field null
        transaction.setDateCreated(null);

        // Create the Transaction, which fails.

        restTransactionMockMvc.perform(post("/api/transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transaction)))
            .andExpect(status().isBadRequest());

        List<Transaction> transactionList = transactionRepository.findAll();
        assertThat(transactionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastUpdatedIsRequired() throws Exception {
        int databaseSizeBeforeTest = transactionRepository.findAll().size();
        // set the field null
        transaction.setLastUpdated(null);

        // Create the Transaction, which fails.

        restTransactionMockMvc.perform(post("/api/transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transaction)))
            .andExpect(status().isBadRequest());

        List<Transaction> transactionList = transactionRepository.findAll();
        assertThat(transactionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = transactionRepository.findAll().size();
        // set the field null
        transaction.setStatus(null);

        // Create the Transaction, which fails.

        restTransactionMockMvc.perform(post("/api/transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transaction)))
            .andExpect(status().isBadRequest());

        List<Transaction> transactionList = transactionRepository.findAll();
        assertThat(transactionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTransactions() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get all the transactionList
        restTransactionMockMvc.perform(get("/api/transactions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(transaction.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateCreated").value(hasItem(sameInstant(DEFAULT_DATE_CREATED))))
            .andExpect(jsonPath("$.[*].lastUpdated").value(hasItem(sameInstant(DEFAULT_LAST_UPDATED))))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].statusReason").value(hasItem(DEFAULT_STATUS_REASON)))
            .andExpect(jsonPath("$.[*].amountBeforeCommission").value(hasItem(DEFAULT_AMOUNT_BEFORE_COMMISSION.intValue())))
            .andExpect(jsonPath("$.[*].bankCommission").value(hasItem(DEFAULT_BANK_COMMISSION.doubleValue())))
            .andExpect(jsonPath("$.[*].fxCommission").value(hasItem(DEFAULT_FX_COMMISSION.doubleValue())))
            .andExpect(jsonPath("$.[*].rechargeCost").value(hasItem(DEFAULT_RECHARGE_COST.doubleValue())))
            .andExpect(jsonPath("$.[*].amountAfterCommission").value(hasItem(DEFAULT_AMOUNT_AFTER_COMMISSION.intValue())))
            .andExpect(jsonPath("$.[*].exchangeRate").value(hasItem(DEFAULT_EXCHANGE_RATE.doubleValue())))
            .andExpect(jsonPath("$.[*].amountLocalCurrency").value(hasItem(DEFAULT_AMOUNT_LOCAL_CURRENCY.intValue())))
            .andExpect(jsonPath("$.[*].bankReference").value(hasItem(DEFAULT_BANK_REFERENCE)))
            .andExpect(jsonPath("$.[*].fileName").value(hasItem(DEFAULT_FILE_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].notes").value(hasItem(DEFAULT_NOTES)))
            .andExpect(jsonPath("$.[*].mustNotify").value(hasItem(DEFAULT_MUST_NOTIFY.booleanValue())))
            .andExpect(jsonPath("$.[*].infoChanged").value(hasItem(DEFAULT_INFO_CHANGED)))
            .andExpect(jsonPath("$.[*].resellerCommission").value(hasItem(DEFAULT_RESELLER_COMMISSION.intValue())))
            .andExpect(jsonPath("$.[*].resellerFixedCommission").value(hasItem(DEFAULT_RESELLER_FIXED_COMMISSION.doubleValue())))
            .andExpect(jsonPath("$.[*].resellerPercentageCommission").value(hasItem(DEFAULT_RESELLER_PERCENTAGE_COMMISSION.doubleValue())))
            .andExpect(jsonPath("$.[*].processType").value(hasItem(DEFAULT_PROCESS_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getTransaction() throws Exception {
        // Initialize the database
        transactionRepository.saveAndFlush(transaction);

        // Get the transaction
        restTransactionMockMvc.perform(get("/api/transactions/{id}", transaction.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(transaction.getId().intValue()))
            .andExpect(jsonPath("$.dateCreated").value(sameInstant(DEFAULT_DATE_CREATED)))
            .andExpect(jsonPath("$.lastUpdated").value(sameInstant(DEFAULT_LAST_UPDATED)))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.statusReason").value(DEFAULT_STATUS_REASON))
            .andExpect(jsonPath("$.amountBeforeCommission").value(DEFAULT_AMOUNT_BEFORE_COMMISSION.intValue()))
            .andExpect(jsonPath("$.bankCommission").value(DEFAULT_BANK_COMMISSION.doubleValue()))
            .andExpect(jsonPath("$.fxCommission").value(DEFAULT_FX_COMMISSION.doubleValue()))
            .andExpect(jsonPath("$.rechargeCost").value(DEFAULT_RECHARGE_COST.doubleValue()))
            .andExpect(jsonPath("$.amountAfterCommission").value(DEFAULT_AMOUNT_AFTER_COMMISSION.intValue()))
            .andExpect(jsonPath("$.exchangeRate").value(DEFAULT_EXCHANGE_RATE.doubleValue()))
            .andExpect(jsonPath("$.amountLocalCurrency").value(DEFAULT_AMOUNT_LOCAL_CURRENCY.intValue()))
            .andExpect(jsonPath("$.bankReference").value(DEFAULT_BANK_REFERENCE))
            .andExpect(jsonPath("$.fileName").value(DEFAULT_FILE_NAME))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.notes").value(DEFAULT_NOTES))
            .andExpect(jsonPath("$.mustNotify").value(DEFAULT_MUST_NOTIFY.booleanValue()))
            .andExpect(jsonPath("$.infoChanged").value(DEFAULT_INFO_CHANGED))
            .andExpect(jsonPath("$.resellerCommission").value(DEFAULT_RESELLER_COMMISSION.intValue()))
            .andExpect(jsonPath("$.resellerFixedCommission").value(DEFAULT_RESELLER_FIXED_COMMISSION.doubleValue()))
            .andExpect(jsonPath("$.resellerPercentageCommission").value(DEFAULT_RESELLER_PERCENTAGE_COMMISSION.doubleValue()))
            .andExpect(jsonPath("$.processType").value(DEFAULT_PROCESS_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTransaction() throws Exception {
        // Get the transaction
        restTransactionMockMvc.perform(get("/api/transactions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTransaction() throws Exception {
        // Initialize the database
        transactionService.save(transaction);
        // As the test used the service layer, reset the Elasticsearch mock repository
        reset(mockTransactionSearchRepository);

        int databaseSizeBeforeUpdate = transactionRepository.findAll().size();

        // Update the transaction
        Transaction updatedTransaction = transactionRepository.findById(transaction.getId()).get();
        // Disconnect from session so that the updates on updatedTransaction are not directly saved in db
        em.detach(updatedTransaction);
        updatedTransaction
            .dateCreated(UPDATED_DATE_CREATED)
            .lastUpdated(UPDATED_LAST_UPDATED)
            .status(UPDATED_STATUS)
            .statusReason(UPDATED_STATUS_REASON)
            .amountBeforeCommission(UPDATED_AMOUNT_BEFORE_COMMISSION)
            .bankCommission(UPDATED_BANK_COMMISSION)
            .fxCommission(UPDATED_FX_COMMISSION)
            .rechargeCost(UPDATED_RECHARGE_COST)
            .amountAfterCommission(UPDATED_AMOUNT_AFTER_COMMISSION)
            .exchangeRate(UPDATED_EXCHANGE_RATE)
            .amountLocalCurrency(UPDATED_AMOUNT_LOCAL_CURRENCY)
            .bankReference(UPDATED_BANK_REFERENCE)
            .fileName(UPDATED_FILE_NAME)
            .description(UPDATED_DESCRIPTION)
            .notes(UPDATED_NOTES)
            .mustNotify(UPDATED_MUST_NOTIFY)
            .infoChanged(UPDATED_INFO_CHANGED)
            .resellerCommission(UPDATED_RESELLER_COMMISSION)
            .resellerFixedCommission(UPDATED_RESELLER_FIXED_COMMISSION)
            .resellerPercentageCommission(UPDATED_RESELLER_PERCENTAGE_COMMISSION)
            .processType(UPDATED_PROCESS_TYPE);

        restTransactionMockMvc.perform(put("/api/transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTransaction)))
            .andExpect(status().isOk());

        // Validate the Transaction in the database
        List<Transaction> transactionList = transactionRepository.findAll();
        assertThat(transactionList).hasSize(databaseSizeBeforeUpdate);
        Transaction testTransaction = transactionList.get(transactionList.size() - 1);
        assertThat(testTransaction.getDateCreated()).isEqualTo(UPDATED_DATE_CREATED);
        assertThat(testTransaction.getLastUpdated()).isEqualTo(UPDATED_LAST_UPDATED);
        assertThat(testTransaction.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testTransaction.getStatusReason()).isEqualTo(UPDATED_STATUS_REASON);
        assertThat(testTransaction.getAmountBeforeCommission()).isEqualTo(UPDATED_AMOUNT_BEFORE_COMMISSION);
        assertThat(testTransaction.getBankCommission()).isEqualTo(UPDATED_BANK_COMMISSION);
        assertThat(testTransaction.getFxCommission()).isEqualTo(UPDATED_FX_COMMISSION);
        assertThat(testTransaction.getRechargeCost()).isEqualTo(UPDATED_RECHARGE_COST);
        assertThat(testTransaction.getAmountAfterCommission()).isEqualTo(UPDATED_AMOUNT_AFTER_COMMISSION);
        assertThat(testTransaction.getExchangeRate()).isEqualTo(UPDATED_EXCHANGE_RATE);
        assertThat(testTransaction.getAmountLocalCurrency()).isEqualTo(UPDATED_AMOUNT_LOCAL_CURRENCY);
        assertThat(testTransaction.getBankReference()).isEqualTo(UPDATED_BANK_REFERENCE);
        assertThat(testTransaction.getFileName()).isEqualTo(UPDATED_FILE_NAME);
        assertThat(testTransaction.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testTransaction.getNotes()).isEqualTo(UPDATED_NOTES);
        assertThat(testTransaction.isMustNotify()).isEqualTo(UPDATED_MUST_NOTIFY);
        assertThat(testTransaction.getInfoChanged()).isEqualTo(UPDATED_INFO_CHANGED);
        assertThat(testTransaction.getResellerCommission()).isEqualTo(UPDATED_RESELLER_COMMISSION);
        assertThat(testTransaction.getResellerFixedCommission()).isEqualTo(UPDATED_RESELLER_FIXED_COMMISSION);
        assertThat(testTransaction.getResellerPercentageCommission()).isEqualTo(UPDATED_RESELLER_PERCENTAGE_COMMISSION);
        assertThat(testTransaction.getProcessType()).isEqualTo(UPDATED_PROCESS_TYPE);

        // Validate the Transaction in Elasticsearch
        verify(mockTransactionSearchRepository, times(1)).save(testTransaction);
    }

    @Test
    @Transactional
    public void updateNonExistingTransaction() throws Exception {
        int databaseSizeBeforeUpdate = transactionRepository.findAll().size();

        // Create the Transaction

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTransactionMockMvc.perform(put("/api/transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transaction)))
            .andExpect(status().isBadRequest());

        // Validate the Transaction in the database
        List<Transaction> transactionList = transactionRepository.findAll();
        assertThat(transactionList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Transaction in Elasticsearch
        verify(mockTransactionSearchRepository, times(0)).save(transaction);
    }

    @Test
    @Transactional
    public void deleteTransaction() throws Exception {
        // Initialize the database
        transactionService.save(transaction);

        int databaseSizeBeforeDelete = transactionRepository.findAll().size();

        // Delete the transaction
        restTransactionMockMvc.perform(delete("/api/transactions/{id}", transaction.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Transaction> transactionList = transactionRepository.findAll();
        assertThat(transactionList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Transaction in Elasticsearch
        verify(mockTransactionSearchRepository, times(1)).deleteById(transaction.getId());
    }

    @Test
    @Transactional
    public void searchTransaction() throws Exception {
        // Initialize the database
        transactionService.save(transaction);
        when(mockTransactionSearchRepository.search(queryStringQuery("id:" + transaction.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(transaction), PageRequest.of(0, 1), 1));
        // Search the transaction
        restTransactionMockMvc.perform(get("/api/_search/transactions?query=id:" + transaction.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(transaction.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateCreated").value(hasItem(sameInstant(DEFAULT_DATE_CREATED))))
            .andExpect(jsonPath("$.[*].lastUpdated").value(hasItem(sameInstant(DEFAULT_LAST_UPDATED))))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].statusReason").value(hasItem(DEFAULT_STATUS_REASON)))
            .andExpect(jsonPath("$.[*].amountBeforeCommission").value(hasItem(DEFAULT_AMOUNT_BEFORE_COMMISSION.intValue())))
            .andExpect(jsonPath("$.[*].bankCommission").value(hasItem(DEFAULT_BANK_COMMISSION.doubleValue())))
            .andExpect(jsonPath("$.[*].fxCommission").value(hasItem(DEFAULT_FX_COMMISSION.doubleValue())))
            .andExpect(jsonPath("$.[*].rechargeCost").value(hasItem(DEFAULT_RECHARGE_COST.doubleValue())))
            .andExpect(jsonPath("$.[*].amountAfterCommission").value(hasItem(DEFAULT_AMOUNT_AFTER_COMMISSION.intValue())))
            .andExpect(jsonPath("$.[*].exchangeRate").value(hasItem(DEFAULT_EXCHANGE_RATE.doubleValue())))
            .andExpect(jsonPath("$.[*].amountLocalCurrency").value(hasItem(DEFAULT_AMOUNT_LOCAL_CURRENCY.intValue())))
            .andExpect(jsonPath("$.[*].bankReference").value(hasItem(DEFAULT_BANK_REFERENCE)))
            .andExpect(jsonPath("$.[*].fileName").value(hasItem(DEFAULT_FILE_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].notes").value(hasItem(DEFAULT_NOTES)))
            .andExpect(jsonPath("$.[*].mustNotify").value(hasItem(DEFAULT_MUST_NOTIFY.booleanValue())))
            .andExpect(jsonPath("$.[*].infoChanged").value(hasItem(DEFAULT_INFO_CHANGED)))
            .andExpect(jsonPath("$.[*].resellerCommission").value(hasItem(DEFAULT_RESELLER_COMMISSION.intValue())))
            .andExpect(jsonPath("$.[*].resellerFixedCommission").value(hasItem(DEFAULT_RESELLER_FIXED_COMMISSION.doubleValue())))
            .andExpect(jsonPath("$.[*].resellerPercentageCommission").value(hasItem(DEFAULT_RESELLER_PERCENTAGE_COMMISSION.doubleValue())))
            .andExpect(jsonPath("$.[*].processType").value(hasItem(DEFAULT_PROCESS_TYPE.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Transaction.class);
        Transaction transaction1 = new Transaction();
        transaction1.setId(1L);
        Transaction transaction2 = new Transaction();
        transaction2.setId(transaction1.getId());
        assertThat(transaction1).isEqualTo(transaction2);
        transaction2.setId(2L);
        assertThat(transaction1).isNotEqualTo(transaction2);
        transaction1.setId(null);
        assertThat(transaction1).isNotEqualTo(transaction2);
    }
}
