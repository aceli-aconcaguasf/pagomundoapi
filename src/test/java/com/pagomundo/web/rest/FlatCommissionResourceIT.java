package com.pagomundo.web.rest;

import com.pagomundo.PagoMundoApiApp;
import com.pagomundo.domain.FlatCommission;
import com.pagomundo.repository.FlatCommissionRepository;
import com.pagomundo.repository.search.FlatCommissionSearchRepository;
import com.pagomundo.service.ExtendedUserService;
import com.pagomundo.service.FlatCommissionService;
import com.pagomundo.web.rest.errors.ExceptionTranslator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;

import static com.pagomundo.web.rest.TestUtil.createFormattingConversionService;
import static com.pagomundo.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration tests for the {@link FlatCommissionResource} REST controller.
 */
@SpringBootTest(classes = PagoMundoApiApp.class)
public class FlatCommissionResourceIT {

    private static final BigDecimal DEFAULT_UP_TO = new BigDecimal(1);
    private static final BigDecimal UPDATED_UP_TO = new BigDecimal(2);
    private static final BigDecimal SMALLER_UP_TO = new BigDecimal(1 - 1);

    private static final Float DEFAULT_COMMISSION = 1F;
    private static final Float UPDATED_COMMISSION = 2F;
    private static final Float SMALLER_COMMISSION = 1F - 1F;

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATE_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_CREATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_DATE_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final ZonedDateTime DEFAULT_LAST_UPDATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_LAST_UPDATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_LAST_UPDATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    @Autowired
    private FlatCommissionRepository flatCommissionRepository;

    @Autowired
    private FlatCommissionService flatCommissionService;

    @Autowired
    private ExtendedUserService extendedUserService;

    /**
     * This repository is mocked in the com.pagomundo.repository.search test package.
     *
     * @see com.pagomundo.repository.search.FlatCommissionSearchRepositoryMockConfiguration
     */
    @Autowired
    private FlatCommissionSearchRepository mockFlatCommissionSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restFlatCommissionMockMvc;

    private FlatCommission flatCommission;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final FlatCommissionResource flatCommissionResource = new FlatCommissionResource(flatCommissionService, extendedUserService);
        this.restFlatCommissionMockMvc = MockMvcBuilders.standaloneSetup(flatCommissionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FlatCommission createEntity(EntityManager em) {
        FlatCommission flatCommission = new FlatCommission()
            .upTo(DEFAULT_UP_TO)
            .commission(DEFAULT_COMMISSION)
            .description(DEFAULT_DESCRIPTION)
            .dateCreated(DEFAULT_DATE_CREATED)
            .lastUpdated(DEFAULT_LAST_UPDATED);
        return flatCommission;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FlatCommission createUpdatedEntity(EntityManager em) {
        FlatCommission flatCommission = new FlatCommission()
            .upTo(UPDATED_UP_TO)
            .commission(UPDATED_COMMISSION)
            .description(UPDATED_DESCRIPTION)
            .dateCreated(UPDATED_DATE_CREATED)
            .lastUpdated(UPDATED_LAST_UPDATED);
        return flatCommission;
    }

    @BeforeEach
    public void initTest() {
        flatCommission = createEntity(em);
    }

    @Test
    @Transactional
    public void createFlatCommission() throws Exception {
        int databaseSizeBeforeCreate = flatCommissionRepository.findAll().size();

        // Create the FlatCommission
        restFlatCommissionMockMvc.perform(post("/api/flat-commissions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(flatCommission)))
            .andExpect(status().isCreated());

        // Validate the FlatCommission in the database
        List<FlatCommission> flatCommissionList = flatCommissionRepository.findAll();
        assertThat(flatCommissionList).hasSize(databaseSizeBeforeCreate + 1);
        FlatCommission testFlatCommission = flatCommissionList.get(flatCommissionList.size() - 1);
        assertThat(testFlatCommission.getUpTo()).isEqualTo(DEFAULT_UP_TO);
        assertThat(testFlatCommission.getCommission()).isEqualTo(DEFAULT_COMMISSION);
        assertThat(testFlatCommission.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testFlatCommission.getDateCreated()).isEqualTo(DEFAULT_DATE_CREATED);
        assertThat(testFlatCommission.getLastUpdated()).isEqualTo(DEFAULT_LAST_UPDATED);

        // Validate the FlatCommission in Elasticsearch
        verify(mockFlatCommissionSearchRepository, times(1)).save(testFlatCommission);
    }

    @Test
    @Transactional
    public void createFlatCommissionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = flatCommissionRepository.findAll().size();

        // Create the FlatCommission with an existing ID
        flatCommission.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFlatCommissionMockMvc.perform(post("/api/flat-commissions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(flatCommission)))
            .andExpect(status().isBadRequest());

        // Validate the FlatCommission in the database
        List<FlatCommission> flatCommissionList = flatCommissionRepository.findAll();
        assertThat(flatCommissionList).hasSize(databaseSizeBeforeCreate);

        // Validate the FlatCommission in Elasticsearch
        verify(mockFlatCommissionSearchRepository, times(0)).save(flatCommission);
    }


    @Test
    @Transactional
    public void getAllFlatCommissions() throws Exception {
        // Initialize the database
        flatCommissionRepository.saveAndFlush(flatCommission);

        // Get all the flatCommissionList
        restFlatCommissionMockMvc.perform(get("/api/flat-commissions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(flatCommission.getId().intValue())))
            .andExpect(jsonPath("$.[*].upTo").value(hasItem(DEFAULT_UP_TO.intValue())))
            .andExpect(jsonPath("$.[*].commission").value(hasItem(DEFAULT_COMMISSION.doubleValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].dateCreated").value(hasItem(sameInstant(DEFAULT_DATE_CREATED))))
            .andExpect(jsonPath("$.[*].lastUpdated").value(hasItem(sameInstant(DEFAULT_LAST_UPDATED))));
    }

    @Test
    @Transactional
    public void getFlatCommission() throws Exception {
        // Initialize the database
        flatCommissionRepository.saveAndFlush(flatCommission);

        // Get the flatCommission
        restFlatCommissionMockMvc.perform(get("/api/flat-commissions/{id}", flatCommission.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(flatCommission.getId().intValue()))
            .andExpect(jsonPath("$.upTo").value(DEFAULT_UP_TO.intValue()))
            .andExpect(jsonPath("$.commission").value(DEFAULT_COMMISSION.doubleValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.dateCreated").value(sameInstant(DEFAULT_DATE_CREATED)))
            .andExpect(jsonPath("$.lastUpdated").value(sameInstant(DEFAULT_LAST_UPDATED)));
    }

    @Test
    @Transactional
    public void getNonExistingFlatCommission() throws Exception {
        // Get the flatCommission
        restFlatCommissionMockMvc.perform(get("/api/flat-commissions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFlatCommission() throws Exception {
        // Initialize the database
        flatCommissionService.save(flatCommission);
        // As the test used the service layer, reset the Elasticsearch mock repository
        reset(mockFlatCommissionSearchRepository);

        int databaseSizeBeforeUpdate = flatCommissionRepository.findAll().size();

        // Update the flatCommission
        FlatCommission updatedFlatCommission = flatCommissionRepository.findById(flatCommission.getId()).get();
        // Disconnect from session so that the updates on updatedFlatCommission are not directly saved in db
        em.detach(updatedFlatCommission);
        updatedFlatCommission
            .upTo(UPDATED_UP_TO)
            .commission(UPDATED_COMMISSION)
            .description(UPDATED_DESCRIPTION)
            .dateCreated(UPDATED_DATE_CREATED)
            .lastUpdated(UPDATED_LAST_UPDATED);

        restFlatCommissionMockMvc.perform(put("/api/flat-commissions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedFlatCommission)))
            .andExpect(status().isOk());

        // Validate the FlatCommission in the database
        List<FlatCommission> flatCommissionList = flatCommissionRepository.findAll();
        assertThat(flatCommissionList).hasSize(databaseSizeBeforeUpdate);
        FlatCommission testFlatCommission = flatCommissionList.get(flatCommissionList.size() - 1);
        assertThat(testFlatCommission.getUpTo()).isEqualTo(UPDATED_UP_TO);
        assertThat(testFlatCommission.getCommission()).isEqualTo(UPDATED_COMMISSION);
        assertThat(testFlatCommission.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testFlatCommission.getDateCreated()).isEqualTo(UPDATED_DATE_CREATED);
        assertThat(testFlatCommission.getLastUpdated()).isEqualTo(UPDATED_LAST_UPDATED);

        // Validate the FlatCommission in Elasticsearch
        verify(mockFlatCommissionSearchRepository, times(1)).save(testFlatCommission);
    }

    @Test
    @Transactional
    public void updateNonExistingFlatCommission() throws Exception {
        int databaseSizeBeforeUpdate = flatCommissionRepository.findAll().size();

        // Create the FlatCommission

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFlatCommissionMockMvc.perform(put("/api/flat-commissions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(flatCommission)))
            .andExpect(status().isBadRequest());

        // Validate the FlatCommission in the database
        List<FlatCommission> flatCommissionList = flatCommissionRepository.findAll();
        assertThat(flatCommissionList).hasSize(databaseSizeBeforeUpdate);

        // Validate the FlatCommission in Elasticsearch
        verify(mockFlatCommissionSearchRepository, times(0)).save(flatCommission);
    }

    @Test
    @Transactional
    public void deleteFlatCommission() throws Exception {
        // Initialize the database
        flatCommissionService.save(flatCommission);

        int databaseSizeBeforeDelete = flatCommissionRepository.findAll().size();

        // Delete the flatCommission
        restFlatCommissionMockMvc.perform(delete("/api/flat-commissions/{id}", flatCommission.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<FlatCommission> flatCommissionList = flatCommissionRepository.findAll();
        assertThat(flatCommissionList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the FlatCommission in Elasticsearch
        verify(mockFlatCommissionSearchRepository, times(1)).deleteById(flatCommission.getId());
    }

    @Test
    @Transactional
    public void searchFlatCommission() throws Exception {
        // Initialize the database
        flatCommissionService.save(flatCommission);
        when(mockFlatCommissionSearchRepository.search(queryStringQuery("id:" + flatCommission.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(flatCommission), PageRequest.of(0, 1), 1));
        // Search the flatCommission
        restFlatCommissionMockMvc.perform(get("/api/_search/flat-commissions?query=id:" + flatCommission.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(flatCommission.getId().intValue())))
            .andExpect(jsonPath("$.[*].upTo").value(hasItem(DEFAULT_UP_TO.intValue())))
            .andExpect(jsonPath("$.[*].commission").value(hasItem(DEFAULT_COMMISSION.doubleValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].dateCreated").value(hasItem(sameInstant(DEFAULT_DATE_CREATED))))
            .andExpect(jsonPath("$.[*].lastUpdated").value(hasItem(sameInstant(DEFAULT_LAST_UPDATED))));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FlatCommission.class);
        FlatCommission flatCommission1 = new FlatCommission();
        flatCommission1.setId(1L);
        FlatCommission flatCommission2 = new FlatCommission();
        flatCommission2.setId(flatCommission1.getId());
        assertThat(flatCommission1).isEqualTo(flatCommission2);
        flatCommission2.setId(2L);
        assertThat(flatCommission1).isNotEqualTo(flatCommission2);
        flatCommission1.setId(null);
        assertThat(flatCommission1).isNotEqualTo(flatCommission2);
    }
}
