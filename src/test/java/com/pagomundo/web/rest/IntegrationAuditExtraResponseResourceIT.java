package com.pagomundo.web.rest;

import com.pagomundo.PagoMundoApiApp;
import com.pagomundo.domain.IntegrationAuditExtraResponse;
import com.pagomundo.repository.IntegrationAuditExtraResponseRepository;
import com.pagomundo.repository.search.IntegrationAuditExtraResponseSearchRepository;
import com.pagomundo.service.TranslateService;
import com.pagomundo.service.dto.integrations.IntegrationAuditExtraResponseDTO;
import com.pagomundo.service.integrations.IntegrationAuditExtraResponseService;
import com.pagomundo.service.mapper.IntegrationAuditExtraResponseMapper;
import com.pagomundo.web.rest.errors.ExceptionTranslator;
import com.pagomundo.web.rest.integrations.IntegrationAuditExtraResponseResource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;

import static com.pagomundo.web.rest.TestUtil.createFormattingConversionService;
import static com.pagomundo.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration tests for the {@link IntegrationAuditExtraResponseResource} REST controller.
 */
@SpringBootTest(classes = PagoMundoApiApp.class)
public class IntegrationAuditExtraResponseResourceIT {

    private static final ZonedDateTime DEFAULT_MESSAGE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_MESSAGE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_MESSAGE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final String DEFAULT_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_MESSAGE = "BBBBBBBBBB";

    @Autowired
    private IntegrationAuditExtraResponseRepository integrationAuditExtraResponseRepository;

    @Autowired
    private IntegrationAuditExtraResponseMapper integrationAuditExtraResponseMapper;

    @Autowired
    private IntegrationAuditExtraResponseService integrationAuditExtraResponseService;

    @Autowired
    private TranslateService translateService;

    /**
     * This repository is mocked in the com.pagomundo.repository.search test package.
     *
     * @see com.pagomundo.repository.search.IntegrationAuditExtraResponseSearchRepositoryMockConfiguration
     */
    @Autowired
    private IntegrationAuditExtraResponseSearchRepository mockIntegrationAuditExtraResponseSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restIntegrationAuditExtraResponseMockMvc;

    private IntegrationAuditExtraResponse integrationAuditExtraResponse;

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IntegrationAuditExtraResponse createEntity(EntityManager em) {
        IntegrationAuditExtraResponse integrationAuditExtraResponse = new IntegrationAuditExtraResponse()
            .messageDate(DEFAULT_MESSAGE_DATE)
            .message(DEFAULT_MESSAGE);
        return integrationAuditExtraResponse;
    }

    /**
     * Create an updated entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IntegrationAuditExtraResponse createUpdatedEntity(EntityManager em) {
        IntegrationAuditExtraResponse integrationAuditExtraResponse = new IntegrationAuditExtraResponse()
            .messageDate(UPDATED_MESSAGE_DATE)
            .message(UPDATED_MESSAGE);
        return integrationAuditExtraResponse;
    }

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final IntegrationAuditExtraResponseResource integrationAuditExtraResponseResource = new IntegrationAuditExtraResponseResource(integrationAuditExtraResponseService, translateService);
        this.restIntegrationAuditExtraResponseMockMvc = MockMvcBuilders.standaloneSetup(integrationAuditExtraResponseResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    @BeforeEach
    public void initTest() {
        integrationAuditExtraResponse = createEntity(em);
    }

    @Test
    @Transactional
    public void createIntegrationAuditExtraResponse() throws Exception {
        int databaseSizeBeforeCreate = integrationAuditExtraResponseRepository.findAll().size();

        // Create the IntegrationAuditExtraResponse
        IntegrationAuditExtraResponseDTO integrationAuditExtraResponseDTO = integrationAuditExtraResponseMapper.toDto(integrationAuditExtraResponse);
        restIntegrationAuditExtraResponseMockMvc.perform(post("/api/integration-audit-extra-responses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(integrationAuditExtraResponseDTO)))
            .andExpect(status().isCreated());

        // Validate the IntegrationAuditExtraResponse in the database
        List<IntegrationAuditExtraResponse> integrationAuditExtraResponseList = integrationAuditExtraResponseRepository.findAll();
        assertThat(integrationAuditExtraResponseList).hasSize(databaseSizeBeforeCreate + 1);
        IntegrationAuditExtraResponse testIntegrationAuditExtraResponse = integrationAuditExtraResponseList.get(integrationAuditExtraResponseList.size() - 1);
        assertThat(testIntegrationAuditExtraResponse.getMessageDate()).isEqualTo(DEFAULT_MESSAGE_DATE);
        assertThat(testIntegrationAuditExtraResponse.getMessage()).isEqualTo(DEFAULT_MESSAGE);

        // Validate the IntegrationAuditExtraResponse in Elasticsearch
        verify(mockIntegrationAuditExtraResponseSearchRepository, times(1)).save(testIntegrationAuditExtraResponse);
    }

    @Test
    @Transactional
    public void createIntegrationAuditExtraResponseWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = integrationAuditExtraResponseRepository.findAll().size();

        // Create the IntegrationAuditExtraResponse with an existing ID
        integrationAuditExtraResponse.setId(1L);
        IntegrationAuditExtraResponseDTO integrationAuditExtraResponseDTO = integrationAuditExtraResponseMapper.toDto(integrationAuditExtraResponse);

        // An entity with an existing ID cannot be created, so this API call must fail
        restIntegrationAuditExtraResponseMockMvc.perform(post("/api/integration-audit-extra-responses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(integrationAuditExtraResponseDTO)))
            .andExpect(status().isBadRequest());

        // Validate the IntegrationAuditExtraResponse in the database
        List<IntegrationAuditExtraResponse> integrationAuditExtraResponseList = integrationAuditExtraResponseRepository.findAll();
        assertThat(integrationAuditExtraResponseList).hasSize(databaseSizeBeforeCreate);

        // Validate the IntegrationAuditExtraResponse in Elasticsearch
        verify(mockIntegrationAuditExtraResponseSearchRepository, times(0)).save(integrationAuditExtraResponse);
    }


    @Test
    @Transactional
    public void getAllIntegrationAuditExtraResponses() throws Exception {
        // Initialize the database
        integrationAuditExtraResponseRepository.saveAndFlush(integrationAuditExtraResponse);

        // Get all the integrationAuditExtraResponseList
        restIntegrationAuditExtraResponseMockMvc.perform(get("/api/integration-audit-extra-responses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(integrationAuditExtraResponse.getId().intValue())))
            .andExpect(jsonPath("$.[*].messageDate").value(hasItem(sameInstant(DEFAULT_MESSAGE_DATE))))
            .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE)));
    }

    @Test
    @Transactional
    public void getIntegrationAuditExtraResponse() throws Exception {
        // Initialize the database
        integrationAuditExtraResponseRepository.saveAndFlush(integrationAuditExtraResponse);

        // Get the integrationAuditExtraResponse
        restIntegrationAuditExtraResponseMockMvc.perform(get("/api/integration-audit-extra-responses/{id}", integrationAuditExtraResponse.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(integrationAuditExtraResponse.getId().intValue()))
            .andExpect(jsonPath("$.messageDate").value(sameInstant(DEFAULT_MESSAGE_DATE)))
            .andExpect(jsonPath("$.message").value(DEFAULT_MESSAGE));
    }

    @Test
    @Transactional
    public void getNonExistingIntegrationAuditExtraResponse() throws Exception {
        // Get the integrationAuditExtraResponse
        restIntegrationAuditExtraResponseMockMvc.perform(get("/api/integration-audit-extra-responses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateIntegrationAuditExtraResponse() throws Exception {
        // Initialize the database
        integrationAuditExtraResponseRepository.saveAndFlush(integrationAuditExtraResponse);

        int databaseSizeBeforeUpdate = integrationAuditExtraResponseRepository.findAll().size();

        // Update the integrationAuditExtraResponse
        IntegrationAuditExtraResponse updatedIntegrationAuditExtraResponse = integrationAuditExtraResponseRepository.findById(integrationAuditExtraResponse.getId()).get();
        // Disconnect from session so that the updates on updatedIntegrationAuditExtraResponse are not directly saved in db
        em.detach(updatedIntegrationAuditExtraResponse);
        updatedIntegrationAuditExtraResponse
            .messageDate(UPDATED_MESSAGE_DATE)
            .message(UPDATED_MESSAGE);
        IntegrationAuditExtraResponseDTO integrationAuditExtraResponseDTO = integrationAuditExtraResponseMapper.toDto(updatedIntegrationAuditExtraResponse);

        restIntegrationAuditExtraResponseMockMvc.perform(put("/api/integration-audit-extra-responses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(integrationAuditExtraResponseDTO)))
            .andExpect(status().isOk());

        // Validate the IntegrationAuditExtraResponse in the database
        List<IntegrationAuditExtraResponse> integrationAuditExtraResponseList = integrationAuditExtraResponseRepository.findAll();
        assertThat(integrationAuditExtraResponseList).hasSize(databaseSizeBeforeUpdate);
        IntegrationAuditExtraResponse testIntegrationAuditExtraResponse = integrationAuditExtraResponseList.get(integrationAuditExtraResponseList.size() - 1);
        assertThat(testIntegrationAuditExtraResponse.getMessageDate()).isEqualTo(UPDATED_MESSAGE_DATE);
        assertThat(testIntegrationAuditExtraResponse.getMessage()).isEqualTo(UPDATED_MESSAGE);

        // Validate the IntegrationAuditExtraResponse in Elasticsearch
        verify(mockIntegrationAuditExtraResponseSearchRepository, times(1)).save(testIntegrationAuditExtraResponse);
    }

    @Test
    @Transactional
    public void updateNonExistingIntegrationAuditExtraResponse() throws Exception {
        int databaseSizeBeforeUpdate = integrationAuditExtraResponseRepository.findAll().size();

        // Create the IntegrationAuditExtraResponse
        IntegrationAuditExtraResponseDTO integrationAuditExtraResponseDTO = integrationAuditExtraResponseMapper.toDto(integrationAuditExtraResponse);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restIntegrationAuditExtraResponseMockMvc.perform(put("/api/integration-audit-extra-responses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(integrationAuditExtraResponseDTO)))
            .andExpect(status().isBadRequest());

        // Validate the IntegrationAuditExtraResponse in the database
        List<IntegrationAuditExtraResponse> integrationAuditExtraResponseList = integrationAuditExtraResponseRepository.findAll();
        assertThat(integrationAuditExtraResponseList).hasSize(databaseSizeBeforeUpdate);

        // Validate the IntegrationAuditExtraResponse in Elasticsearch
        verify(mockIntegrationAuditExtraResponseSearchRepository, times(0)).save(integrationAuditExtraResponse);
    }

    @Test
    @Transactional
    public void deleteIntegrationAuditExtraResponse() throws Exception {
        // Initialize the database
        integrationAuditExtraResponseRepository.saveAndFlush(integrationAuditExtraResponse);

        int databaseSizeBeforeDelete = integrationAuditExtraResponseRepository.findAll().size();

        // Delete the integrationAuditExtraResponse
        restIntegrationAuditExtraResponseMockMvc.perform(delete("/api/integration-audit-extra-responses/{id}", integrationAuditExtraResponse.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<IntegrationAuditExtraResponse> integrationAuditExtraResponseList = integrationAuditExtraResponseRepository.findAll();
        assertThat(integrationAuditExtraResponseList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the IntegrationAuditExtraResponse in Elasticsearch
        verify(mockIntegrationAuditExtraResponseSearchRepository, times(1)).deleteById(integrationAuditExtraResponse.getId());
    }

    @Test
    @Transactional
    public void searchIntegrationAuditExtraResponse() throws Exception {
        // Initialize the database
        integrationAuditExtraResponseRepository.saveAndFlush(integrationAuditExtraResponse);
        when(mockIntegrationAuditExtraResponseSearchRepository.search(queryStringQuery("id:" + integrationAuditExtraResponse.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(integrationAuditExtraResponse), PageRequest.of(0, 1), 1));
        // Search the integrationAuditExtraResponse
        restIntegrationAuditExtraResponseMockMvc.perform(get("/api/_search/integration-audit-extra-responses?query=id:" + integrationAuditExtraResponse.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(integrationAuditExtraResponse.getId().intValue())))
            .andExpect(jsonPath("$.[*].messageDate").value(hasItem(sameInstant(DEFAULT_MESSAGE_DATE))))
            .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(IntegrationAuditExtraResponse.class);
        IntegrationAuditExtraResponse integrationAuditExtraResponse1 = new IntegrationAuditExtraResponse();
        integrationAuditExtraResponse1.setId(1L);
        IntegrationAuditExtraResponse integrationAuditExtraResponse2 = new IntegrationAuditExtraResponse();
        integrationAuditExtraResponse2.setId(integrationAuditExtraResponse1.getId());
        assertThat(integrationAuditExtraResponse1).isEqualTo(integrationAuditExtraResponse2);
        integrationAuditExtraResponse2.setId(2L);
        assertThat(integrationAuditExtraResponse1).isNotEqualTo(integrationAuditExtraResponse2);
        integrationAuditExtraResponse1.setId(null);
        assertThat(integrationAuditExtraResponse1).isNotEqualTo(integrationAuditExtraResponse2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(IntegrationAuditExtraResponseDTO.class);
        IntegrationAuditExtraResponseDTO integrationAuditExtraResponseDTO1 = new IntegrationAuditExtraResponseDTO();
        integrationAuditExtraResponseDTO1.setId(1L);
        IntegrationAuditExtraResponseDTO integrationAuditExtraResponseDTO2 = new IntegrationAuditExtraResponseDTO();
        assertThat(integrationAuditExtraResponseDTO1).isNotEqualTo(integrationAuditExtraResponseDTO2);
        integrationAuditExtraResponseDTO2.setId(integrationAuditExtraResponseDTO1.getId());
        assertThat(integrationAuditExtraResponseDTO1).isEqualTo(integrationAuditExtraResponseDTO2);
        integrationAuditExtraResponseDTO2.setId(2L);
        assertThat(integrationAuditExtraResponseDTO1).isNotEqualTo(integrationAuditExtraResponseDTO2);
        integrationAuditExtraResponseDTO1.setId(null);
        assertThat(integrationAuditExtraResponseDTO1).isNotEqualTo(integrationAuditExtraResponseDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(integrationAuditExtraResponseMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(integrationAuditExtraResponseMapper.fromId(null)).isNull();
    }
}
