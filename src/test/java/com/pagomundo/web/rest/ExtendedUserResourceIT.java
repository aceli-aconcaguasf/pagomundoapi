package com.pagomundo.web.rest;

import com.pagomundo.PagoMundoApiApp;
import com.pagomundo.domain.ExtendedUser;
import com.pagomundo.domain.enumeration.BankAccountType;
import com.pagomundo.domain.enumeration.Gender;
import com.pagomundo.domain.enumeration.MaritalStatus;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.repository.ExtendedUserRepository;
import com.pagomundo.repository.search.ExtendedUserSearchRepository;
import com.pagomundo.service.ExtendedUserRelationService;
import com.pagomundo.service.ExtendedUserService;
import com.pagomundo.service.FileService;
import com.pagomundo.service.NotificationReceiverService;
import com.pagomundo.service.TranslateService;
import com.pagomundo.service.UserService;
import com.pagomundo.web.rest.errors.ExceptionTranslator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;

import static com.pagomundo.web.rest.TestUtil.createFormattingConversionService;
import static com.pagomundo.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration tests for the {@link ExtendedUserResource} REST controller.
 */
@SpringBootTest(classes = PagoMundoApiApp.class)
public class ExtendedUserResourceIT {

    private static final Status DEFAULT_STATUS = Status.CREATED;
    private static final Status UPDATED_STATUS = Status.IN_PROCESS;

    private static final String DEFAULT_LAST_NAME_1 = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME_1 = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME_2 = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME_2 = "BBBBBBBBBB";

    private static final String DEFAULT_FIRST_NAME_1 = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME_1 = "BBBBBBBBBB";

    private static final String DEFAULT_FIRST_NAME_2 = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME_2 = "BBBBBBBBBB";

    private static final String DEFAULT_FULL_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FULL_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ID_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_ID_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_TAX_ID = "AAAAAAAAAA";
    private static final String UPDATED_TAX_ID = "BBBBBBBBBB";

    private static final Gender DEFAULT_GENDER = Gender.MALE;
    private static final Gender UPDATED_GENDER = Gender.FEMALE;

    private static final MaritalStatus DEFAULT_MARITAL_STATUS = MaritalStatus.SINGLE;
    private static final MaritalStatus UPDATED_MARITAL_STATUS = MaritalStatus.MARRIED;

    private static final String DEFAULT_RESIDENCE_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_RESIDENCE_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_POSTAL_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_POSTAL_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_PHONE_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_MOBILE_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_MOBILE_NUMBER = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_BIRTH_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_BIRTH_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_BIRTH_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final ZonedDateTime DEFAULT_DATE_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_CREATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_DATE_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final ZonedDateTime DEFAULT_LAST_UPDATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_LAST_UPDATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_LAST_UPDATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_COMPANY = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY = "BBBBBBBBBB";

    private static final String DEFAULT_IMAGE_ID_URL = "AAAAAAAAAA";
    private static final String UPDATED_IMAGE_ID_URL = "BBBBBBBBBB";

    private static final String DEFAULT_IMAGE_ADDRESS_URL = "AAAAAAAAAA";
    private static final String UPDATED_IMAGE_ADDRESS_URL = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_BALANCE = new BigDecimal(1);
    private static final BigDecimal UPDATED_BALANCE = new BigDecimal(2);
    private static final BigDecimal SMALLER_BALANCE = new BigDecimal(1 - 1);

    private static final String DEFAULT_STATUS_REASON = "AAAAAAAAAA";
    private static final String UPDATED_STATUS_REASON = "BBBBBBBBBB";

    private static final String DEFAULT_ROLE = "AAAAAAAAAA";
    private static final String UPDATED_ROLE = "BBBBBBBBBB";

    private static final String DEFAULT_CARD_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_CARD_NUMBER = "BBBBBBBBBB";

    private static final Float DEFAULT_BANK_COMMISSION = 1F;
    private static final Float UPDATED_BANK_COMMISSION = 2F;
    private static final Float SMALLER_BANK_COMMISSION = 1F - 1F;

    private static final Float DEFAULT_FX_COMMISSION = 1F;
    private static final Float UPDATED_FX_COMMISSION = 2F;
    private static final Float SMALLER_FX_COMMISSION = 1F - 1F;

    private static final Float DEFAULT_RECHARGE_COST = 1F;
    private static final Float UPDATED_RECHARGE_COST = 2F;
    private static final Float SMALLER_RECHARGE_COST = 1F - 1F;

    private static final Boolean DEFAULT_USE_MERCHANT_COMMISSION = false;
    private static final Boolean UPDATED_USE_MERCHANT_COMMISSION = true;

    private static final String DEFAULT_BANK_ACCOUNT_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_BANK_ACCOUNT_NUMBER = "BBBBBBBBBB";

    private static final Boolean DEFAULT_USE_DIRECT_PAYMENT = false;
    private static final Boolean UPDATED_USE_DIRECT_PAYMENT = true;

    private static final BankAccountType DEFAULT_BANK_ACCOUNT_TYPE = BankAccountType.AHO;
    private static final BankAccountType UPDATED_BANK_ACCOUNT_TYPE = BankAccountType.CTE;

    private static final Boolean DEFAULT_CAN_CHANGE_PAYMENT_METHOD = false;
    private static final Boolean UPDATED_CAN_CHANGE_PAYMENT_METHOD = true;

    private static final Boolean DEFAULT_MUST_NOTIFY = false;
    private static final Boolean UPDATED_MUST_NOTIFY = true;

    private static final String DEFAULT_PROFILE_INFO_CHANGED = "AAAAAAAAAA";
    private static final String UPDATED_PROFILE_INFO_CHANGED = "BBBBBBBBBB";

    private static final Boolean DEFAULT_CONFIRMED_PROFILE = false;
    private static final Boolean UPDATED_CONFIRMED_PROFILE = true;

    private static final Float DEFAULT_RESELLER_FIXED_COMMISSION = 1F;
    private static final Float UPDATED_RESELLER_FIXED_COMMISSION = 2F;
    private static final Float SMALLER_RESELLER_FIXED_COMMISSION = 1F - 1F;

    private static final Float DEFAULT_RESELLER_PERCENTAGE_COMMISSION = 1F;
    private static final Float UPDATED_RESELLER_PERCENTAGE_COMMISSION = 2F;
    private static final Float SMALLER_RESELLER_PERCENTAGE_COMMISSION = 1F - 1F;

    private static final Boolean DEFAULT_USE_FLAT_COMMISSION = false;
    private static final Boolean UPDATED_USE_FLAT_COMMISSION = true;
    private static final String DEFAULT_BANK_BRANCH = "AAAAAAAAAA";
    private static final String UPDATED_BANK_BRANCH = "BBBBBBBBBB";

    private static final String DEFAULT_ALIAS = "AAAAAAAAAA";
    private static final String UPDATED_ALIAS = "BBBBBBBBBB";

    @Autowired
    private ExtendedUserRepository extendedUserRepository;

    @Autowired
    private ExtendedUserService extendedUserService;

    @Autowired
    private ExtendedUserRelationService extendedUserRelationService;

    @Autowired
    private UserService userService;

    @Autowired
    private FileService fileService;

    @Autowired
    private NotificationReceiverService notificationReceiverService;

    /**
     * This repository is mocked in the com.pagomundo.repository.search test package.
     *
     * @see com.pagomundo.repository.search.ExtendedUserSearchRepositoryMockConfiguration
     */
    @Autowired
    private ExtendedUserSearchRepository mockExtendedUserSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restExtendedUserMockMvc;

    private ExtendedUser extendedUser;

    @Autowired
    private TranslateService translateService;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExtendedUser createEntity(EntityManager em) {
        ExtendedUser extendedUser = new ExtendedUser()
            .status(DEFAULT_STATUS)
            .lastName1(DEFAULT_LAST_NAME_1)
            .lastName2(DEFAULT_LAST_NAME_2)
            .firstName1(DEFAULT_FIRST_NAME_1)
            .firstName2(DEFAULT_FIRST_NAME_2)
            .fullName(DEFAULT_FULL_NAME)
            .idNumber(DEFAULT_ID_NUMBER)
            .taxId(DEFAULT_TAX_ID)
            .gender(DEFAULT_GENDER)
            .maritalStatus(DEFAULT_MARITAL_STATUS)
            .residenceAddress(DEFAULT_RESIDENCE_ADDRESS)
            .postalAddress(DEFAULT_POSTAL_ADDRESS)
            .phoneNumber(DEFAULT_PHONE_NUMBER)
            .mobileNumber(DEFAULT_MOBILE_NUMBER)
            .birthDate(DEFAULT_BIRTH_DATE)
            .dateCreated(DEFAULT_DATE_CREATED)
            .lastUpdated(DEFAULT_LAST_UPDATED)
            .email(DEFAULT_EMAIL)
            .company(DEFAULT_COMPANY)
            .imageIdUrl(DEFAULT_IMAGE_ID_URL)
            .imageAddressUrl(DEFAULT_IMAGE_ADDRESS_URL)
            .balance(DEFAULT_BALANCE)
            .statusReason(DEFAULT_STATUS_REASON)
            .role(DEFAULT_ROLE)
            .cardNumber(DEFAULT_CARD_NUMBER)
            .bankCommission(DEFAULT_BANK_COMMISSION)
            .fxCommission(DEFAULT_FX_COMMISSION)
            .rechargeCost(DEFAULT_RECHARGE_COST)
            .useMerchantCommission(DEFAULT_USE_MERCHANT_COMMISSION)
            .bankAccountNumber(DEFAULT_BANK_ACCOUNT_NUMBER)
            .useDirectPayment(DEFAULT_USE_DIRECT_PAYMENT)
            .bankAccountType(DEFAULT_BANK_ACCOUNT_TYPE)
            .canChangePaymentMethod(DEFAULT_CAN_CHANGE_PAYMENT_METHOD)
            .mustNotify(DEFAULT_MUST_NOTIFY)
            .profileInfoChanged(DEFAULT_PROFILE_INFO_CHANGED)
            .confirmedProfile(DEFAULT_CONFIRMED_PROFILE)
            .resellerFixedCommission(DEFAULT_RESELLER_FIXED_COMMISSION)
            .resellerPercentageCommission(DEFAULT_RESELLER_PERCENTAGE_COMMISSION)
            .useFlatCommission(DEFAULT_USE_FLAT_COMMISSION)
            .bankBranch(DEFAULT_BANK_BRANCH)
            .alias(DEFAULT_ALIAS);
        return extendedUser;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ExtendedUser createUpdatedEntity(EntityManager em) {
        ExtendedUser extendedUser = new ExtendedUser()
            .status(UPDATED_STATUS)
            .lastName1(UPDATED_LAST_NAME_1)
            .lastName2(UPDATED_LAST_NAME_2)
            .firstName1(UPDATED_FIRST_NAME_1)
            .firstName2(UPDATED_FIRST_NAME_2)
            .fullName(UPDATED_FULL_NAME)
            .idNumber(UPDATED_ID_NUMBER)
            .taxId(UPDATED_TAX_ID)
            .gender(UPDATED_GENDER)
            .maritalStatus(UPDATED_MARITAL_STATUS)
            .residenceAddress(UPDATED_RESIDENCE_ADDRESS)
            .postalAddress(UPDATED_POSTAL_ADDRESS)
            .phoneNumber(UPDATED_PHONE_NUMBER)
            .mobileNumber(UPDATED_MOBILE_NUMBER)
            .birthDate(UPDATED_BIRTH_DATE)
            .dateCreated(UPDATED_DATE_CREATED)
            .lastUpdated(UPDATED_LAST_UPDATED)
            .email(UPDATED_EMAIL)
            .company(UPDATED_COMPANY)
            .imageIdUrl(UPDATED_IMAGE_ID_URL)
            .imageAddressUrl(UPDATED_IMAGE_ADDRESS_URL)
            .balance(UPDATED_BALANCE)
            .statusReason(UPDATED_STATUS_REASON)
            .role(UPDATED_ROLE)
            .cardNumber(UPDATED_CARD_NUMBER)
            .bankCommission(UPDATED_BANK_COMMISSION)
            .fxCommission(UPDATED_FX_COMMISSION)
            .rechargeCost(UPDATED_RECHARGE_COST)
            .useMerchantCommission(UPDATED_USE_MERCHANT_COMMISSION)
            .bankAccountNumber(UPDATED_BANK_ACCOUNT_NUMBER)
            .useDirectPayment(UPDATED_USE_DIRECT_PAYMENT)
            .bankAccountType(UPDATED_BANK_ACCOUNT_TYPE)
            .canChangePaymentMethod(UPDATED_CAN_CHANGE_PAYMENT_METHOD)
            .mustNotify(UPDATED_MUST_NOTIFY)
            .profileInfoChanged(UPDATED_PROFILE_INFO_CHANGED)
            .confirmedProfile(UPDATED_CONFIRMED_PROFILE)
            .resellerFixedCommission(UPDATED_RESELLER_FIXED_COMMISSION)
            .resellerPercentageCommission(UPDATED_RESELLER_PERCENTAGE_COMMISSION)
            .useFlatCommission(UPDATED_USE_FLAT_COMMISSION)
            .bankBranch(UPDATED_BANK_BRANCH)
            .alias(UPDATED_ALIAS);
        return extendedUser;
    }

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ExtendedUserResource extendedUserResource = new ExtendedUserResource(extendedUserService, notificationReceiverService, userService, fileService, translateService);
        this.restExtendedUserMockMvc = MockMvcBuilders.standaloneSetup(extendedUserResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    @BeforeEach
    public void initTest() {
        extendedUser = createEntity(em);
    }

    @Test
    @Transactional
    public void createExtendedUser() throws Exception {
        int databaseSizeBeforeCreate = extendedUserRepository.findAll().size();

        // Create the ExtendedUser
        restExtendedUserMockMvc.perform(post("/api/extended-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(extendedUser)))
            .andExpect(status().isCreated());

        // Validate the ExtendedUser in the database
        List<ExtendedUser> extendedUserList = extendedUserRepository.findAll();
        assertThat(extendedUserList).hasSize(databaseSizeBeforeCreate + 1);
        ExtendedUser testExtendedUser = extendedUserList.get(extendedUserList.size() - 1);
        assertThat(testExtendedUser.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testExtendedUser.getLastName1()).isEqualTo(DEFAULT_LAST_NAME_1);
        assertThat(testExtendedUser.getLastName2()).isEqualTo(DEFAULT_LAST_NAME_2);
        assertThat(testExtendedUser.getFirstName1()).isEqualTo(DEFAULT_FIRST_NAME_1);
        assertThat(testExtendedUser.getFirstName2()).isEqualTo(DEFAULT_FIRST_NAME_2);
        assertThat(testExtendedUser.getFullName()).isEqualTo(DEFAULT_FULL_NAME);
        assertThat(testExtendedUser.getIdNumber()).isEqualTo(DEFAULT_ID_NUMBER);
        assertThat(testExtendedUser.getTaxId()).isEqualTo(DEFAULT_TAX_ID);
        assertThat(testExtendedUser.getGender()).isEqualTo(DEFAULT_GENDER);
        assertThat(testExtendedUser.getMaritalStatus()).isEqualTo(DEFAULT_MARITAL_STATUS);
        assertThat(testExtendedUser.getResidenceAddress()).isEqualTo(DEFAULT_RESIDENCE_ADDRESS);
        assertThat(testExtendedUser.getPostalAddress()).isEqualTo(DEFAULT_POSTAL_ADDRESS);
        assertThat(testExtendedUser.getPhoneNumber()).isEqualTo(DEFAULT_PHONE_NUMBER);
        assertThat(testExtendedUser.getMobileNumber()).isEqualTo(DEFAULT_MOBILE_NUMBER);
        assertThat(testExtendedUser.getBirthDate()).isEqualTo(DEFAULT_BIRTH_DATE);
        assertThat(testExtendedUser.getDateCreated()).isEqualTo(DEFAULT_DATE_CREATED);
        assertThat(testExtendedUser.getLastUpdated()).isEqualTo(DEFAULT_LAST_UPDATED);
        assertThat(testExtendedUser.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testExtendedUser.getCompany()).isEqualTo(DEFAULT_COMPANY);
        assertThat(testExtendedUser.getImageIdUrl()).isEqualTo(DEFAULT_IMAGE_ID_URL);
        assertThat(testExtendedUser.getImageAddressUrl()).isEqualTo(DEFAULT_IMAGE_ADDRESS_URL);
        assertThat(testExtendedUser.getBalance()).isEqualTo(DEFAULT_BALANCE);
        assertThat(testExtendedUser.getStatusReason()).isEqualTo(DEFAULT_STATUS_REASON);
        assertThat(testExtendedUser.getRole()).isEqualTo(DEFAULT_ROLE);
        assertThat(testExtendedUser.getCardNumber()).isEqualTo(DEFAULT_CARD_NUMBER);
        assertThat(testExtendedUser.getBankCommission()).isEqualTo(DEFAULT_BANK_COMMISSION);
        assertThat(testExtendedUser.getFxCommission()).isEqualTo(DEFAULT_FX_COMMISSION);
        assertThat(testExtendedUser.getRechargeCost()).isEqualTo(DEFAULT_RECHARGE_COST);
        assertThat(testExtendedUser.isUseMerchantCommission()).isEqualTo(DEFAULT_USE_MERCHANT_COMMISSION);
        assertThat(testExtendedUser.getBankAccountNumber()).isEqualTo(DEFAULT_BANK_ACCOUNT_NUMBER);
        assertThat(testExtendedUser.isUseDirectPayment()).isEqualTo(DEFAULT_USE_DIRECT_PAYMENT);
        assertThat(testExtendedUser.getBankAccountType()).isEqualTo(DEFAULT_BANK_ACCOUNT_TYPE);
        assertThat(testExtendedUser.isCanChangePaymentMethod()).isEqualTo(DEFAULT_CAN_CHANGE_PAYMENT_METHOD);
        assertThat(testExtendedUser.isMustNotify()).isEqualTo(DEFAULT_MUST_NOTIFY);
        assertThat(testExtendedUser.getProfileInfoChanged()).isEqualTo(DEFAULT_PROFILE_INFO_CHANGED);
        assertThat(testExtendedUser.isConfirmedProfile()).isEqualTo(DEFAULT_CONFIRMED_PROFILE);
        assertThat(testExtendedUser.getResellerFixedCommission()).isEqualTo(DEFAULT_RESELLER_FIXED_COMMISSION);
        assertThat(testExtendedUser.getResellerPercentageCommission()).isEqualTo(DEFAULT_RESELLER_PERCENTAGE_COMMISSION);
        assertThat(testExtendedUser.isUseFlatCommission()).isEqualTo(DEFAULT_USE_FLAT_COMMISSION);
        assertThat(testExtendedUser.getBankBranch()).isEqualTo(DEFAULT_BANK_BRANCH);
        assertThat(testExtendedUser.getAlias()).isEqualTo(DEFAULT_ALIAS);

        // Validate the ExtendedUser in Elasticsearch
        verify(mockExtendedUserSearchRepository, times(1)).save(testExtendedUser);
    }

    @Test
    @Transactional
    public void createExtendedUserWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = extendedUserRepository.findAll().size();

        // Create the ExtendedUser with an existing ID
        extendedUser.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restExtendedUserMockMvc.perform(post("/api/extended-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(extendedUser)))
            .andExpect(status().isBadRequest());

        // Validate the ExtendedUser in the database
        List<ExtendedUser> extendedUserList = extendedUserRepository.findAll();
        assertThat(extendedUserList).hasSize(databaseSizeBeforeCreate);

        // Validate the ExtendedUser in Elasticsearch
        verify(mockExtendedUserSearchRepository, times(0)).save(extendedUser);
    }


    @Test
    @Transactional
    public void getAllExtendedUsers() throws Exception {
        // Initialize the database
        extendedUserRepository.saveAndFlush(extendedUser);

        // Get all the extendedUserList
        restExtendedUserMockMvc.perform(get("/api/extended-users?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(extendedUser.getId().intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].lastName1").value(hasItem(DEFAULT_LAST_NAME_1)))
            .andExpect(jsonPath("$.[*].lastName2").value(hasItem(DEFAULT_LAST_NAME_2)))
            .andExpect(jsonPath("$.[*].firstName1").value(hasItem(DEFAULT_FIRST_NAME_1)))
            .andExpect(jsonPath("$.[*].firstName2").value(hasItem(DEFAULT_FIRST_NAME_2)))
            .andExpect(jsonPath("$.[*].fullName").value(hasItem(DEFAULT_FULL_NAME)))
            .andExpect(jsonPath("$.[*].idNumber").value(hasItem(DEFAULT_ID_NUMBER)))
            .andExpect(jsonPath("$.[*].taxId").value(hasItem(DEFAULT_TAX_ID)))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER.toString())))
            .andExpect(jsonPath("$.[*].maritalStatus").value(hasItem(DEFAULT_MARITAL_STATUS.toString())))
            .andExpect(jsonPath("$.[*].residenceAddress").value(hasItem(DEFAULT_RESIDENCE_ADDRESS)))
            .andExpect(jsonPath("$.[*].postalAddress").value(hasItem(DEFAULT_POSTAL_ADDRESS)))
            .andExpect(jsonPath("$.[*].phoneNumber").value(hasItem(DEFAULT_PHONE_NUMBER)))
            .andExpect(jsonPath("$.[*].mobileNumber").value(hasItem(DEFAULT_MOBILE_NUMBER)))
            .andExpect(jsonPath("$.[*].birthDate").value(hasItem(sameInstant(DEFAULT_BIRTH_DATE))))
            .andExpect(jsonPath("$.[*].dateCreated").value(hasItem(sameInstant(DEFAULT_DATE_CREATED))))
            .andExpect(jsonPath("$.[*].lastUpdated").value(hasItem(sameInstant(DEFAULT_LAST_UPDATED))))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].company").value(hasItem(DEFAULT_COMPANY)))
            .andExpect(jsonPath("$.[*].imageIdUrl").value(hasItem(DEFAULT_IMAGE_ID_URL)))
            .andExpect(jsonPath("$.[*].imageAddressUrl").value(hasItem(DEFAULT_IMAGE_ADDRESS_URL)))
            .andExpect(jsonPath("$.[*].balance").value(hasItem(DEFAULT_BALANCE.intValue())))
            .andExpect(jsonPath("$.[*].statusReason").value(hasItem(DEFAULT_STATUS_REASON)))
            .andExpect(jsonPath("$.[*].role").value(hasItem(DEFAULT_ROLE)))
            .andExpect(jsonPath("$.[*].cardNumber").value(hasItem(DEFAULT_CARD_NUMBER)))
            .andExpect(jsonPath("$.[*].bankCommission").value(hasItem(DEFAULT_BANK_COMMISSION.doubleValue())))
            .andExpect(jsonPath("$.[*].fxCommission").value(hasItem(DEFAULT_FX_COMMISSION.doubleValue())))
            .andExpect(jsonPath("$.[*].rechargeCost").value(hasItem(DEFAULT_RECHARGE_COST.doubleValue())))
            .andExpect(jsonPath("$.[*].useMerchantCommission").value(hasItem(DEFAULT_USE_MERCHANT_COMMISSION.booleanValue())))
            .andExpect(jsonPath("$.[*].bankAccountNumber").value(hasItem(DEFAULT_BANK_ACCOUNT_NUMBER)))
            .andExpect(jsonPath("$.[*].useDirectPayment").value(hasItem(DEFAULT_USE_DIRECT_PAYMENT.booleanValue())))
            .andExpect(jsonPath("$.[*].bankAccountType").value(hasItem(DEFAULT_BANK_ACCOUNT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].canChangePaymentMethod").value(hasItem(DEFAULT_CAN_CHANGE_PAYMENT_METHOD.booleanValue())))
            .andExpect(jsonPath("$.[*].mustNotify").value(hasItem(DEFAULT_MUST_NOTIFY.booleanValue())))
            .andExpect(jsonPath("$.[*].profileInfoChanged").value(hasItem(DEFAULT_PROFILE_INFO_CHANGED.toString())))
            .andExpect(jsonPath("$.[*].confirmedProfile").value(hasItem(DEFAULT_CONFIRMED_PROFILE.booleanValue())))
            .andExpect(jsonPath("$.[*].resellerFixedCommission").value(hasItem(DEFAULT_RESELLER_FIXED_COMMISSION.doubleValue())))
            .andExpect(jsonPath("$.[*].resellerPercentageCommission").value(hasItem(DEFAULT_RESELLER_PERCENTAGE_COMMISSION.doubleValue())))
            .andExpect(jsonPath("$.[*].useFlatCommission").value(hasItem(DEFAULT_USE_FLAT_COMMISSION.booleanValue())))
            .andExpect(jsonPath("$.[*].profileInfoChanged").value(hasItem(DEFAULT_PROFILE_INFO_CHANGED)))
            .andExpect(jsonPath("$.[*].confirmedProfile").value(hasItem(DEFAULT_CONFIRMED_PROFILE.booleanValue())))
            .andExpect(jsonPath("$.[*].resellerFixedCommission").value(hasItem(DEFAULT_RESELLER_FIXED_COMMISSION.doubleValue())))
            .andExpect(jsonPath("$.[*].resellerPercentageCommission").value(hasItem(DEFAULT_RESELLER_PERCENTAGE_COMMISSION.doubleValue())))
            .andExpect(jsonPath("$.[*].bankBranch").value(hasItem(DEFAULT_BANK_BRANCH)))
            .andExpect(jsonPath("$.[*].alias").value(hasItem(DEFAULT_ALIAS)));
    }

    @Test
    @Transactional
    public void getExtendedUser() throws Exception {
        // Initialize the database
        extendedUserRepository.saveAndFlush(extendedUser);

        // Get the extendedUser
        restExtendedUserMockMvc.perform(get("/api/extended-users/{id}", extendedUser.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(extendedUser.getId().intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.lastName1").value(DEFAULT_LAST_NAME_1))
            .andExpect(jsonPath("$.lastName2").value(DEFAULT_LAST_NAME_2))
            .andExpect(jsonPath("$.firstName1").value(DEFAULT_FIRST_NAME_1))
            .andExpect(jsonPath("$.firstName2").value(DEFAULT_FIRST_NAME_2))
            .andExpect(jsonPath("$.fullName").value(DEFAULT_FULL_NAME))
            .andExpect(jsonPath("$.idNumber").value(DEFAULT_ID_NUMBER))
            .andExpect(jsonPath("$.taxId").value(DEFAULT_TAX_ID))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER.toString()))
            .andExpect(jsonPath("$.maritalStatus").value(DEFAULT_MARITAL_STATUS.toString()))
            .andExpect(jsonPath("$.residenceAddress").value(DEFAULT_RESIDENCE_ADDRESS))
            .andExpect(jsonPath("$.postalAddress").value(DEFAULT_POSTAL_ADDRESS))
            .andExpect(jsonPath("$.phoneNumber").value(DEFAULT_PHONE_NUMBER))
            .andExpect(jsonPath("$.mobileNumber").value(DEFAULT_MOBILE_NUMBER))
            .andExpect(jsonPath("$.birthDate").value(sameInstant(DEFAULT_BIRTH_DATE)))
            .andExpect(jsonPath("$.dateCreated").value(sameInstant(DEFAULT_DATE_CREATED)))
            .andExpect(jsonPath("$.lastUpdated").value(sameInstant(DEFAULT_LAST_UPDATED)))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.company").value(DEFAULT_COMPANY))
            .andExpect(jsonPath("$.imageIdUrl").value(DEFAULT_IMAGE_ID_URL))
            .andExpect(jsonPath("$.imageAddressUrl").value(DEFAULT_IMAGE_ADDRESS_URL))
            .andExpect(jsonPath("$.balance").value(DEFAULT_BALANCE.intValue()))
            .andExpect(jsonPath("$.statusReason").value(DEFAULT_STATUS_REASON))
            .andExpect(jsonPath("$.role").value(DEFAULT_ROLE))
            .andExpect(jsonPath("$.cardNumber").value(DEFAULT_CARD_NUMBER))
            .andExpect(jsonPath("$.bankCommission").value(DEFAULT_BANK_COMMISSION.doubleValue()))
            .andExpect(jsonPath("$.fxCommission").value(DEFAULT_FX_COMMISSION.doubleValue()))
            .andExpect(jsonPath("$.rechargeCost").value(DEFAULT_RECHARGE_COST.doubleValue()))
            .andExpect(jsonPath("$.useMerchantCommission").value(DEFAULT_USE_MERCHANT_COMMISSION.booleanValue()))
            .andExpect(jsonPath("$.bankAccountNumber").value(DEFAULT_BANK_ACCOUNT_NUMBER))
            .andExpect(jsonPath("$.useDirectPayment").value(DEFAULT_USE_DIRECT_PAYMENT.booleanValue()))
            .andExpect(jsonPath("$.bankAccountType").value(DEFAULT_BANK_ACCOUNT_TYPE.toString()))
            .andExpect(jsonPath("$.canChangePaymentMethod").value(DEFAULT_CAN_CHANGE_PAYMENT_METHOD.booleanValue()))
            .andExpect(jsonPath("$.mustNotify").value(DEFAULT_MUST_NOTIFY.booleanValue()))
            .andExpect(jsonPath("$.profileInfoChanged").value(DEFAULT_PROFILE_INFO_CHANGED.toString()))
            .andExpect(jsonPath("$.confirmedProfile").value(DEFAULT_CONFIRMED_PROFILE.booleanValue()))
            .andExpect(jsonPath("$.resellerFixedCommission").value(DEFAULT_RESELLER_FIXED_COMMISSION.doubleValue()))
            .andExpect(jsonPath("$.resellerPercentageCommission").value(DEFAULT_RESELLER_PERCENTAGE_COMMISSION.doubleValue()))
            .andExpect(jsonPath("$.useFlatCommission").value(DEFAULT_USE_FLAT_COMMISSION.booleanValue()))
            .andExpect(jsonPath("$.profileInfoChanged").value(DEFAULT_PROFILE_INFO_CHANGED))
            .andExpect(jsonPath("$.confirmedProfile").value(DEFAULT_CONFIRMED_PROFILE.booleanValue()))
            .andExpect(jsonPath("$.resellerFixedCommission").value(DEFAULT_RESELLER_FIXED_COMMISSION.doubleValue()))
            .andExpect(jsonPath("$.resellerPercentageCommission").value(DEFAULT_RESELLER_PERCENTAGE_COMMISSION.doubleValue()))
            .andExpect(jsonPath("$.bankBranch").value(DEFAULT_BANK_BRANCH))
            .andExpect(jsonPath("$.alias").value(DEFAULT_ALIAS));
    }

    @Test
    @Transactional
    public void getNonExistingExtendedUser() throws Exception {
        // Get the extendedUser
        restExtendedUserMockMvc.perform(get("/api/extended-users/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateExtendedUser() throws Exception {
        // Initialize the database
        extendedUserService.save(extendedUser);
        // As the test used the service layer, reset the Elasticsearch mock repository
        reset(mockExtendedUserSearchRepository);

        int databaseSizeBeforeUpdate = extendedUserRepository.findAll().size();

        // Update the extendedUser
        ExtendedUser updatedExtendedUser = extendedUserRepository.findById(extendedUser.getId()).get();
        // Disconnect from session so that the updates on updatedExtendedUser are not directly saved in db
        em.detach(updatedExtendedUser);
        updatedExtendedUser
            .status(UPDATED_STATUS)
            .lastName1(UPDATED_LAST_NAME_1)
            .lastName2(UPDATED_LAST_NAME_2)
            .firstName1(UPDATED_FIRST_NAME_1)
            .firstName2(UPDATED_FIRST_NAME_2)
            .fullName(UPDATED_FULL_NAME)
            .idNumber(UPDATED_ID_NUMBER)
            .taxId(UPDATED_TAX_ID)
            .gender(UPDATED_GENDER)
            .maritalStatus(UPDATED_MARITAL_STATUS)
            .residenceAddress(UPDATED_RESIDENCE_ADDRESS)
            .postalAddress(UPDATED_POSTAL_ADDRESS)
            .phoneNumber(UPDATED_PHONE_NUMBER)
            .mobileNumber(UPDATED_MOBILE_NUMBER)
            .birthDate(UPDATED_BIRTH_DATE)
            .dateCreated(UPDATED_DATE_CREATED)
            .lastUpdated(UPDATED_LAST_UPDATED)
            .email(UPDATED_EMAIL)
            .company(UPDATED_COMPANY)
            .imageIdUrl(UPDATED_IMAGE_ID_URL)
            .imageAddressUrl(UPDATED_IMAGE_ADDRESS_URL)
            .balance(UPDATED_BALANCE)
            .statusReason(UPDATED_STATUS_REASON)
            .role(UPDATED_ROLE)
            .cardNumber(UPDATED_CARD_NUMBER)
            .bankCommission(UPDATED_BANK_COMMISSION)
            .fxCommission(UPDATED_FX_COMMISSION)
            .rechargeCost(UPDATED_RECHARGE_COST)
            .useMerchantCommission(UPDATED_USE_MERCHANT_COMMISSION)
            .bankAccountNumber(UPDATED_BANK_ACCOUNT_NUMBER)
            .useDirectPayment(UPDATED_USE_DIRECT_PAYMENT)
            .bankAccountType(UPDATED_BANK_ACCOUNT_TYPE)
            .canChangePaymentMethod(UPDATED_CAN_CHANGE_PAYMENT_METHOD)
            .mustNotify(UPDATED_MUST_NOTIFY)
            .profileInfoChanged(UPDATED_PROFILE_INFO_CHANGED)
            .confirmedProfile(UPDATED_CONFIRMED_PROFILE)
            .resellerFixedCommission(UPDATED_RESELLER_FIXED_COMMISSION)
            .resellerPercentageCommission(UPDATED_RESELLER_PERCENTAGE_COMMISSION)
            .useFlatCommission(UPDATED_USE_FLAT_COMMISSION)
            .bankBranch(UPDATED_BANK_BRANCH)
            .alias(UPDATED_ALIAS);

        restExtendedUserMockMvc.perform(put("/api/extended-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedExtendedUser)))
            .andExpect(status().isOk());

        // Validate the ExtendedUser in the database
        List<ExtendedUser> extendedUserList = extendedUserRepository.findAll();
        assertThat(extendedUserList).hasSize(databaseSizeBeforeUpdate);
        ExtendedUser testExtendedUser = extendedUserList.get(extendedUserList.size() - 1);
        assertThat(testExtendedUser.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testExtendedUser.getLastName1()).isEqualTo(UPDATED_LAST_NAME_1);
        assertThat(testExtendedUser.getLastName2()).isEqualTo(UPDATED_LAST_NAME_2);
        assertThat(testExtendedUser.getFirstName1()).isEqualTo(UPDATED_FIRST_NAME_1);
        assertThat(testExtendedUser.getFirstName2()).isEqualTo(UPDATED_FIRST_NAME_2);
        assertThat(testExtendedUser.getFullName()).isEqualTo(UPDATED_FULL_NAME);
        assertThat(testExtendedUser.getIdNumber()).isEqualTo(UPDATED_ID_NUMBER);
        assertThat(testExtendedUser.getTaxId()).isEqualTo(UPDATED_TAX_ID);
        assertThat(testExtendedUser.getGender()).isEqualTo(UPDATED_GENDER);
        assertThat(testExtendedUser.getMaritalStatus()).isEqualTo(UPDATED_MARITAL_STATUS);
        assertThat(testExtendedUser.getResidenceAddress()).isEqualTo(UPDATED_RESIDENCE_ADDRESS);
        assertThat(testExtendedUser.getPostalAddress()).isEqualTo(UPDATED_POSTAL_ADDRESS);
        assertThat(testExtendedUser.getPhoneNumber()).isEqualTo(UPDATED_PHONE_NUMBER);
        assertThat(testExtendedUser.getMobileNumber()).isEqualTo(UPDATED_MOBILE_NUMBER);
        assertThat(testExtendedUser.getBirthDate()).isEqualTo(UPDATED_BIRTH_DATE);
        assertThat(testExtendedUser.getDateCreated()).isEqualTo(UPDATED_DATE_CREATED);
        assertThat(testExtendedUser.getLastUpdated()).isEqualTo(UPDATED_LAST_UPDATED);
        assertThat(testExtendedUser.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testExtendedUser.getCompany()).isEqualTo(UPDATED_COMPANY);
        assertThat(testExtendedUser.getImageIdUrl()).isEqualTo(UPDATED_IMAGE_ID_URL);
        assertThat(testExtendedUser.getImageAddressUrl()).isEqualTo(UPDATED_IMAGE_ADDRESS_URL);
        assertThat(testExtendedUser.getBalance()).isEqualTo(UPDATED_BALANCE);
        assertThat(testExtendedUser.getStatusReason()).isEqualTo(UPDATED_STATUS_REASON);
        assertThat(testExtendedUser.getRole()).isEqualTo(UPDATED_ROLE);
        assertThat(testExtendedUser.getCardNumber()).isEqualTo(UPDATED_CARD_NUMBER);
        assertThat(testExtendedUser.getBankCommission()).isEqualTo(UPDATED_BANK_COMMISSION);
        assertThat(testExtendedUser.getFxCommission()).isEqualTo(UPDATED_FX_COMMISSION);
        assertThat(testExtendedUser.getRechargeCost()).isEqualTo(UPDATED_RECHARGE_COST);
        assertThat(testExtendedUser.isUseMerchantCommission()).isEqualTo(UPDATED_USE_MERCHANT_COMMISSION);
        assertThat(testExtendedUser.getBankAccountNumber()).isEqualTo(UPDATED_BANK_ACCOUNT_NUMBER);
        assertThat(testExtendedUser.isUseDirectPayment()).isEqualTo(UPDATED_USE_DIRECT_PAYMENT);
        assertThat(testExtendedUser.getBankAccountType()).isEqualTo(UPDATED_BANK_ACCOUNT_TYPE);
        assertThat(testExtendedUser.isCanChangePaymentMethod()).isEqualTo(UPDATED_CAN_CHANGE_PAYMENT_METHOD);
        assertThat(testExtendedUser.isMustNotify()).isEqualTo(UPDATED_MUST_NOTIFY);
        assertThat(testExtendedUser.getProfileInfoChanged()).isEqualTo(UPDATED_PROFILE_INFO_CHANGED);
        assertThat(testExtendedUser.isConfirmedProfile()).isEqualTo(UPDATED_CONFIRMED_PROFILE);
        assertThat(testExtendedUser.getResellerFixedCommission()).isEqualTo(UPDATED_RESELLER_FIXED_COMMISSION);
        assertThat(testExtendedUser.getResellerPercentageCommission()).isEqualTo(UPDATED_RESELLER_PERCENTAGE_COMMISSION);
        assertThat(testExtendedUser.isUseFlatCommission()).isEqualTo(UPDATED_USE_FLAT_COMMISSION);
        assertThat(testExtendedUser.getBankBranch()).isEqualTo(UPDATED_BANK_BRANCH);
        assertThat(testExtendedUser.getAlias()).isEqualTo(UPDATED_ALIAS);

        // Validate the ExtendedUser in Elasticsearch
        verify(mockExtendedUserSearchRepository, times(1)).save(testExtendedUser);
    }

    @Test
    @Transactional
    public void updateNonExistingExtendedUser() throws Exception {
        int databaseSizeBeforeUpdate = extendedUserRepository.findAll().size();

        // Create the ExtendedUser

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restExtendedUserMockMvc.perform(put("/api/extended-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(extendedUser)))
            .andExpect(status().isBadRequest());

        // Validate the ExtendedUser in the database
        List<ExtendedUser> extendedUserList = extendedUserRepository.findAll();
        assertThat(extendedUserList).hasSize(databaseSizeBeforeUpdate);

        // Validate the ExtendedUser in Elasticsearch
        verify(mockExtendedUserSearchRepository, times(0)).save(extendedUser);
    }

    @Test
    @Transactional
    public void deleteExtendedUser() throws Exception {
        // Initialize the database
        extendedUserService.save(extendedUser);

        int databaseSizeBeforeDelete = extendedUserRepository.findAll().size();

        // Delete the extendedUser
        restExtendedUserMockMvc.perform(delete("/api/extended-users/{id}", extendedUser.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ExtendedUser> extendedUserList = extendedUserRepository.findAll();
        assertThat(extendedUserList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the ExtendedUser in Elasticsearch
        verify(mockExtendedUserSearchRepository, times(1)).deleteById(extendedUser.getId());
    }

    @Test
    @Transactional
    public void searchExtendedUser() throws Exception {
        // Initialize the database
        extendedUserService.save(extendedUser);
        when(mockExtendedUserSearchRepository.search(queryStringQuery("id:" + extendedUser.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(extendedUser), PageRequest.of(0, 1), 1));
        // Search the extendedUser
        restExtendedUserMockMvc.perform(get("/api/_search/extended-users?query=id:" + extendedUser.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(extendedUser.getId().intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].lastName1").value(hasItem(DEFAULT_LAST_NAME_1)))
            .andExpect(jsonPath("$.[*].lastName2").value(hasItem(DEFAULT_LAST_NAME_2)))
            .andExpect(jsonPath("$.[*].firstName1").value(hasItem(DEFAULT_FIRST_NAME_1)))
            .andExpect(jsonPath("$.[*].firstName2").value(hasItem(DEFAULT_FIRST_NAME_2)))
            .andExpect(jsonPath("$.[*].fullName").value(hasItem(DEFAULT_FULL_NAME)))
            .andExpect(jsonPath("$.[*].idNumber").value(hasItem(DEFAULT_ID_NUMBER)))
            .andExpect(jsonPath("$.[*].taxId").value(hasItem(DEFAULT_TAX_ID)))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER.toString())))
            .andExpect(jsonPath("$.[*].maritalStatus").value(hasItem(DEFAULT_MARITAL_STATUS.toString())))
            .andExpect(jsonPath("$.[*].residenceAddress").value(hasItem(DEFAULT_RESIDENCE_ADDRESS)))
            .andExpect(jsonPath("$.[*].postalAddress").value(hasItem(DEFAULT_POSTAL_ADDRESS)))
            .andExpect(jsonPath("$.[*].phoneNumber").value(hasItem(DEFAULT_PHONE_NUMBER)))
            .andExpect(jsonPath("$.[*].mobileNumber").value(hasItem(DEFAULT_MOBILE_NUMBER)))
            .andExpect(jsonPath("$.[*].birthDate").value(hasItem(sameInstant(DEFAULT_BIRTH_DATE))))
            .andExpect(jsonPath("$.[*].dateCreated").value(hasItem(sameInstant(DEFAULT_DATE_CREATED))))
            .andExpect(jsonPath("$.[*].lastUpdated").value(hasItem(sameInstant(DEFAULT_LAST_UPDATED))))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].company").value(hasItem(DEFAULT_COMPANY)))
            .andExpect(jsonPath("$.[*].imageIdUrl").value(hasItem(DEFAULT_IMAGE_ID_URL)))
            .andExpect(jsonPath("$.[*].imageAddressUrl").value(hasItem(DEFAULT_IMAGE_ADDRESS_URL)))
            .andExpect(jsonPath("$.[*].balance").value(hasItem(DEFAULT_BALANCE.intValue())))
            .andExpect(jsonPath("$.[*].statusReason").value(hasItem(DEFAULT_STATUS_REASON)))
            .andExpect(jsonPath("$.[*].role").value(hasItem(DEFAULT_ROLE)))
            .andExpect(jsonPath("$.[*].cardNumber").value(hasItem(DEFAULT_CARD_NUMBER)))
            .andExpect(jsonPath("$.[*].bankCommission").value(hasItem(DEFAULT_BANK_COMMISSION.doubleValue())))
            .andExpect(jsonPath("$.[*].fxCommission").value(hasItem(DEFAULT_FX_COMMISSION.doubleValue())))
            .andExpect(jsonPath("$.[*].rechargeCost").value(hasItem(DEFAULT_RECHARGE_COST.doubleValue())))
            .andExpect(jsonPath("$.[*].useMerchantCommission").value(hasItem(DEFAULT_USE_MERCHANT_COMMISSION.booleanValue())))
            .andExpect(jsonPath("$.[*].bankAccountNumber").value(hasItem(DEFAULT_BANK_ACCOUNT_NUMBER)))
            .andExpect(jsonPath("$.[*].useDirectPayment").value(hasItem(DEFAULT_USE_DIRECT_PAYMENT.booleanValue())))
            .andExpect(jsonPath("$.[*].bankAccountType").value(hasItem(DEFAULT_BANK_ACCOUNT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].canChangePaymentMethod").value(hasItem(DEFAULT_CAN_CHANGE_PAYMENT_METHOD.booleanValue())))
            .andExpect(jsonPath("$.[*].mustNotify").value(hasItem(DEFAULT_MUST_NOTIFY.booleanValue())))
            .andExpect(jsonPath("$.[*].profileInfoChanged").value(hasItem(DEFAULT_PROFILE_INFO_CHANGED.toString())))
            .andExpect(jsonPath("$.[*].confirmedProfile").value(hasItem(DEFAULT_CONFIRMED_PROFILE.booleanValue())))
            .andExpect(jsonPath("$.[*].resellerFixedCommission").value(hasItem(DEFAULT_RESELLER_FIXED_COMMISSION.doubleValue())))
            .andExpect(jsonPath("$.[*].resellerPercentageCommission").value(hasItem(DEFAULT_RESELLER_PERCENTAGE_COMMISSION.doubleValue())))
            .andExpect(jsonPath("$.[*].useFlatCommission").value(hasItem(DEFAULT_USE_FLAT_COMMISSION.booleanValue())))
            .andExpect(jsonPath("$.[*].profileInfoChanged").value(hasItem(DEFAULT_PROFILE_INFO_CHANGED)))
            .andExpect(jsonPath("$.[*].confirmedProfile").value(hasItem(DEFAULT_CONFIRMED_PROFILE.booleanValue())))
            .andExpect(jsonPath("$.[*].resellerFixedCommission").value(hasItem(DEFAULT_RESELLER_FIXED_COMMISSION.doubleValue())))
            .andExpect(jsonPath("$.[*].resellerPercentageCommission").value(hasItem(DEFAULT_RESELLER_PERCENTAGE_COMMISSION.doubleValue())))
            .andExpect(jsonPath("$.[*].bankBranch").value(hasItem(DEFAULT_BANK_BRANCH)))
            .andExpect(jsonPath("$.[*].alias").value(hasItem(DEFAULT_ALIAS)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ExtendedUser.class);
        ExtendedUser extendedUser1 = new ExtendedUser();
        extendedUser1.setId(1L);
        ExtendedUser extendedUser2 = new ExtendedUser();
        extendedUser2.setId(extendedUser1.getId());
        assertThat(extendedUser1).isEqualTo(extendedUser2);
        extendedUser2.setId(2L);
        assertThat(extendedUser1).isNotEqualTo(extendedUser2);
        extendedUser1.setId(null);
        assertThat(extendedUser1).isNotEqualTo(extendedUser2);
    }
}
