package com.pagomundo.web.rest;

import com.pagomundo.PagoMundoApiApp;
import com.pagomundo.domain.NotificationStatusChange;
import com.pagomundo.domain.enumeration.Status;
import com.pagomundo.repository.NotificationStatusChangeRepository;
import com.pagomundo.repository.search.NotificationStatusChangeSearchRepository;
import com.pagomundo.service.NotificationStatusChangeService;
import com.pagomundo.service.TranslateService;
import com.pagomundo.web.rest.errors.ExceptionTranslator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;

import static com.pagomundo.web.rest.TestUtil.createFormattingConversionService;
import static com.pagomundo.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
/**
 * Integration tests for the {@link NotificationStatusChangeResource} REST controller.
 */
@SpringBootTest(classes = PagoMundoApiApp.class)
public class NotificationStatusChangeResourceIT {

    private static final ZonedDateTime DEFAULT_DATE_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_CREATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_DATE_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final Status DEFAULT_STATUS = Status.CREATED;
    private static final Status UPDATED_STATUS = Status.IN_PROCESS;

    private static final String DEFAULT_REASON = "AAAAAAAAAA";
    private static final String UPDATED_REASON = "BBBBBBBBBB";

    @Autowired
    private NotificationStatusChangeRepository notificationStatusChangeRepository;

    @Autowired
    private NotificationStatusChangeService notificationStatusChangeService;

    /**
     * This repository is mocked in the com.pagomundo.repository.search test package.
     *
     * @see com.pagomundo.repository.search.NotificationStatusChangeSearchRepositoryMockConfiguration
     */
    @Autowired
    private NotificationStatusChangeSearchRepository mockNotificationStatusChangeSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restNotificationStatusChangeMockMvc;

    private NotificationStatusChange notificationStatusChange;

    @Autowired
    private TranslateService translateService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NotificationStatusChangeResource notificationStatusChangeResource = new NotificationStatusChangeResource(notificationStatusChangeService, translateService);
        this.restNotificationStatusChangeMockMvc = MockMvcBuilders.standaloneSetup(notificationStatusChangeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NotificationStatusChange createEntity(EntityManager em) {
        NotificationStatusChange notificationStatusChange = new NotificationStatusChange()
            .dateCreated(DEFAULT_DATE_CREATED)
            .status(DEFAULT_STATUS)
            .reason(DEFAULT_REASON);
        return notificationStatusChange;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NotificationStatusChange createUpdatedEntity(EntityManager em) {
        NotificationStatusChange notificationStatusChange = new NotificationStatusChange()
            .dateCreated(UPDATED_DATE_CREATED)
            .status(UPDATED_STATUS)
            .reason(UPDATED_REASON);
        return notificationStatusChange;
    }

    @BeforeEach
    public void initTest() {
        notificationStatusChange = createEntity(em);
    }

    @Test
    @Transactional
    public void createNotificationStatusChange() throws Exception {
        int databaseSizeBeforeCreate = notificationStatusChangeRepository.findAll().size();

        // Create the NotificationStatusChange
        restNotificationStatusChangeMockMvc.perform(post("/api/notification-status-changes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notificationStatusChange)))
            .andExpect(status().isCreated());

        // Validate the NotificationStatusChange in the database
        List<NotificationStatusChange> notificationStatusChangeList = notificationStatusChangeRepository.findAll();
        assertThat(notificationStatusChangeList).hasSize(databaseSizeBeforeCreate + 1);
        NotificationStatusChange testNotificationStatusChange = notificationStatusChangeList.get(notificationStatusChangeList.size() - 1);
        assertThat(testNotificationStatusChange.getDateCreated()).isEqualTo(DEFAULT_DATE_CREATED);
        assertThat(testNotificationStatusChange.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testNotificationStatusChange.getReason()).isEqualTo(DEFAULT_REASON);

        // Validate the NotificationStatusChange in Elasticsearch
        verify(mockNotificationStatusChangeSearchRepository, times(1)).save(testNotificationStatusChange);
    }

    @Test
    @Transactional
    public void createNotificationStatusChangeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = notificationStatusChangeRepository.findAll().size();

        // Create the NotificationStatusChange with an existing ID
        notificationStatusChange.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNotificationStatusChangeMockMvc.perform(post("/api/notification-status-changes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notificationStatusChange)))
            .andExpect(status().isBadRequest());

        // Validate the NotificationStatusChange in the database
        List<NotificationStatusChange> notificationStatusChangeList = notificationStatusChangeRepository.findAll();
        assertThat(notificationStatusChangeList).hasSize(databaseSizeBeforeCreate);

        // Validate the NotificationStatusChange in Elasticsearch
        verify(mockNotificationStatusChangeSearchRepository, times(0)).save(notificationStatusChange);
    }


    @Test
    @Transactional
    public void checkDateCreatedIsRequired() throws Exception {
        int databaseSizeBeforeTest = notificationStatusChangeRepository.findAll().size();
        // set the field null
        notificationStatusChange.setDateCreated(null);

        // Create the NotificationStatusChange, which fails.

        restNotificationStatusChangeMockMvc.perform(post("/api/notification-status-changes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notificationStatusChange)))
            .andExpect(status().isBadRequest());

        List<NotificationStatusChange> notificationStatusChangeList = notificationStatusChangeRepository.findAll();
        assertThat(notificationStatusChangeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = notificationStatusChangeRepository.findAll().size();
        // set the field null
        notificationStatusChange.setStatus(null);

        // Create the NotificationStatusChange, which fails.

        restNotificationStatusChangeMockMvc.perform(post("/api/notification-status-changes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notificationStatusChange)))
            .andExpect(status().isBadRequest());

        List<NotificationStatusChange> notificationStatusChangeList = notificationStatusChangeRepository.findAll();
        assertThat(notificationStatusChangeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNotificationStatusChanges() throws Exception {
        // Initialize the database
        notificationStatusChangeRepository.saveAndFlush(notificationStatusChange);

        // Get all the notificationStatusChangeList
        restNotificationStatusChangeMockMvc.perform(get("/api/notification-status-changes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(notificationStatusChange.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateCreated").value(hasItem(sameInstant(DEFAULT_DATE_CREATED))))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].reason").value(hasItem(DEFAULT_REASON.toString())));
    }

    @Test
    @Transactional
    public void getNotificationStatusChange() throws Exception {
        // Initialize the database
        notificationStatusChangeRepository.saveAndFlush(notificationStatusChange);

        // Get the notificationStatusChange
        restNotificationStatusChangeMockMvc.perform(get("/api/notification-status-changes/{id}", notificationStatusChange.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(notificationStatusChange.getId().intValue()))
            .andExpect(jsonPath("$.dateCreated").value(sameInstant(DEFAULT_DATE_CREATED)))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.reason").value(DEFAULT_REASON.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingNotificationStatusChange() throws Exception {
        // Get the notificationStatusChange
        restNotificationStatusChangeMockMvc.perform(get("/api/notification-status-changes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNotificationStatusChange() throws Exception {
        // Initialize the database
        notificationStatusChangeService.save(notificationStatusChange);
        // As the test used the service layer, reset the Elasticsearch mock repository
        reset(mockNotificationStatusChangeSearchRepository);

        int databaseSizeBeforeUpdate = notificationStatusChangeRepository.findAll().size();

        // Update the notificationStatusChange
        NotificationStatusChange updatedNotificationStatusChange = notificationStatusChangeRepository.findById(notificationStatusChange.getId()).get();
        // Disconnect from session so that the updates on updatedNotificationStatusChange are not directly saved in db
        em.detach(updatedNotificationStatusChange);
        updatedNotificationStatusChange
            .dateCreated(UPDATED_DATE_CREATED)
            .status(UPDATED_STATUS)
            .reason(UPDATED_REASON);

        restNotificationStatusChangeMockMvc.perform(put("/api/notification-status-changes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedNotificationStatusChange)))
            .andExpect(status().isOk());

        // Validate the NotificationStatusChange in the database
        List<NotificationStatusChange> notificationStatusChangeList = notificationStatusChangeRepository.findAll();
        assertThat(notificationStatusChangeList).hasSize(databaseSizeBeforeUpdate);
        NotificationStatusChange testNotificationStatusChange = notificationStatusChangeList.get(notificationStatusChangeList.size() - 1);
        assertThat(testNotificationStatusChange.getDateCreated()).isEqualTo(UPDATED_DATE_CREATED);
        assertThat(testNotificationStatusChange.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testNotificationStatusChange.getReason()).isEqualTo(UPDATED_REASON);

        // Validate the NotificationStatusChange in Elasticsearch
        verify(mockNotificationStatusChangeSearchRepository, times(1)).save(testNotificationStatusChange);
    }

    @Test
    @Transactional
    public void updateNonExistingNotificationStatusChange() throws Exception {
        int databaseSizeBeforeUpdate = notificationStatusChangeRepository.findAll().size();

        // Create the NotificationStatusChange

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNotificationStatusChangeMockMvc.perform(put("/api/notification-status-changes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notificationStatusChange)))
            .andExpect(status().isBadRequest());

        // Validate the NotificationStatusChange in the database
        List<NotificationStatusChange> notificationStatusChangeList = notificationStatusChangeRepository.findAll();
        assertThat(notificationStatusChangeList).hasSize(databaseSizeBeforeUpdate);

        // Validate the NotificationStatusChange in Elasticsearch
        verify(mockNotificationStatusChangeSearchRepository, times(0)).save(notificationStatusChange);
    }

    @Test
    @Transactional
    public void deleteNotificationStatusChange() throws Exception {
        // Initialize the database
        notificationStatusChangeService.save(notificationStatusChange);

        int databaseSizeBeforeDelete = notificationStatusChangeRepository.findAll().size();

        // Delete the notificationStatusChange
        restNotificationStatusChangeMockMvc.perform(delete("/api/notification-status-changes/{id}", notificationStatusChange.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<NotificationStatusChange> notificationStatusChangeList = notificationStatusChangeRepository.findAll();
        assertThat(notificationStatusChangeList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the NotificationStatusChange in Elasticsearch
        verify(mockNotificationStatusChangeSearchRepository, times(1)).deleteById(notificationStatusChange.getId());
    }

    @Test
    @Transactional
    public void searchNotificationStatusChange() throws Exception {
        // Initialize the database
        notificationStatusChangeService.save(notificationStatusChange);
        when(mockNotificationStatusChangeSearchRepository.search(queryStringQuery("id:" + notificationStatusChange.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(notificationStatusChange), PageRequest.of(0, 1), 1));
        // Search the notificationStatusChange
        restNotificationStatusChangeMockMvc.perform(get("/api/_search/notification-status-changes?query=id:" + notificationStatusChange.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(notificationStatusChange.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateCreated").value(hasItem(sameInstant(DEFAULT_DATE_CREATED))))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].reason").value(hasItem(DEFAULT_REASON.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NotificationStatusChange.class);
        NotificationStatusChange notificationStatusChange1 = new NotificationStatusChange();
        notificationStatusChange1.setId(1L);
        NotificationStatusChange notificationStatusChange2 = new NotificationStatusChange();
        notificationStatusChange2.setId(notificationStatusChange1.getId());
        assertThat(notificationStatusChange1).isEqualTo(notificationStatusChange2);
        notificationStatusChange2.setId(2L);
        assertThat(notificationStatusChange1).isNotEqualTo(notificationStatusChange2);
        notificationStatusChange1.setId(null);
        assertThat(notificationStatusChange1).isNotEqualTo(notificationStatusChange2);
    }
}
