package com.pagomundo.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of {@link DirectPaymentBankSearchRepository} to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class DirectPaymentBankSearchRepositoryMockConfiguration {

    @MockBean
    private DirectPaymentBankSearchRepository mockDirectPaymentBankSearchRepository;

}
