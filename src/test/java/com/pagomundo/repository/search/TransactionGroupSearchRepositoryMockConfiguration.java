package com.pagomundo.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of {@link TransactionGroupSearchRepository} to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class TransactionGroupSearchRepositoryMockConfiguration {

    @MockBean
    private TransactionGroupSearchRepository mockTransactionGroupSearchRepository;

}
