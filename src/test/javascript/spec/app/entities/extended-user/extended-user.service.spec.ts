import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { ExtendedUserService } from 'app/entities/extended-user/extended-user.service';
import { IExtendedUser, ExtendedUser } from 'app/shared/model/extended-user.model';
import { Status } from 'app/shared/model/enumerations/status.model';
import { Gender } from 'app/shared/model/enumerations/gender.model';
import { MaritalStatus } from 'app/shared/model/enumerations/marital-status.model';
import { BankAccountType } from 'app/shared/model/enumerations/bank-account-type.model';

describe('Service Tests', () => {
  describe('ExtendedUser Service', () => {
    let injector: TestBed;
    let service: ExtendedUserService;
    let httpMock: HttpTestingController;
    let elemDefault: IExtendedUser;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(ExtendedUserService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new ExtendedUser(
        0,
        Status.CREATED,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        Gender.MALE,
        MaritalStatus.SINGLE,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        currentDate,
        currentDate,
        currentDate,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        0,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        0,
        0,
        0,
        false,
        'AAAAAAA',
        false,
        BankAccountType.AHO,
        false,
        false,
        'AAAAAAA',
        false,
        0,
        0,
        false,
        'AAAAAAA',
        'AAAAAAA'
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            birthDate: currentDate.format(DATE_TIME_FORMAT),
            dateCreated: currentDate.format(DATE_TIME_FORMAT),
            lastUpdated: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a ExtendedUser', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            birthDate: currentDate.format(DATE_TIME_FORMAT),
            dateCreated: currentDate.format(DATE_TIME_FORMAT),
            lastUpdated: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            birthDate: currentDate,
            dateCreated: currentDate,
            lastUpdated: currentDate
          },
          returnedFromService
        );
        service
          .create(new ExtendedUser(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a ExtendedUser', () => {
        const returnedFromService = Object.assign(
          {
            status: 'BBBBBB',
            lastName1: 'BBBBBB',
            lastName2: 'BBBBBB',
            firstName1: 'BBBBBB',
            firstName2: 'BBBBBB',
            fullName: 'BBBBBB',
            idNumber: 'BBBBBB',
            taxId: 'BBBBBB',
            gender: 'BBBBBB',
            maritalStatus: 'BBBBBB',
            residenceAddress: 'BBBBBB',
            postalAddress: 'BBBBBB',
            phoneNumber: 'BBBBBB',
            mobileNumber: 'BBBBBB',
            birthDate: currentDate.format(DATE_TIME_FORMAT),
            dateCreated: currentDate.format(DATE_TIME_FORMAT),
            lastUpdated: currentDate.format(DATE_TIME_FORMAT),
            email: 'BBBBBB',
            company: 'BBBBBB',
            imageIdUrl: 'BBBBBB',
            imageAddressUrl: 'BBBBBB',
            balance: 1,
            statusReason: 'BBBBBB',
            role: 'BBBBBB',
            cardNumber: 'BBBBBB',
            bankCommission: 1,
            fxCommission: 1,
            rechargeCost: 1,
            useMerchantCommission: true,
            bankAccountNumber: 'BBBBBB',
            useDirectPayment: true,
            bankAccountType: 'BBBBBB',
            canChangePaymentMethod: true,
            mustNotify: true,
            profileInfoChanged: 'BBBBBB',
            confirmedProfile: true,
            resellerFixedCommission: 1,
            resellerPercentageCommission: 1,
            useFlatCommission: true,
            bankBranch: 'BBBBBB',
            alias: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            birthDate: currentDate,
            dateCreated: currentDate,
            lastUpdated: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of ExtendedUser', () => {
        const returnedFromService = Object.assign(
          {
            status: 'BBBBBB',
            lastName1: 'BBBBBB',
            lastName2: 'BBBBBB',
            firstName1: 'BBBBBB',
            firstName2: 'BBBBBB',
            fullName: 'BBBBBB',
            idNumber: 'BBBBBB',
            taxId: 'BBBBBB',
            gender: 'BBBBBB',
            maritalStatus: 'BBBBBB',
            residenceAddress: 'BBBBBB',
            postalAddress: 'BBBBBB',
            phoneNumber: 'BBBBBB',
            mobileNumber: 'BBBBBB',
            birthDate: currentDate.format(DATE_TIME_FORMAT),
            dateCreated: currentDate.format(DATE_TIME_FORMAT),
            lastUpdated: currentDate.format(DATE_TIME_FORMAT),
            email: 'BBBBBB',
            company: 'BBBBBB',
            imageIdUrl: 'BBBBBB',
            imageAddressUrl: 'BBBBBB',
            balance: 1,
            statusReason: 'BBBBBB',
            role: 'BBBBBB',
            cardNumber: 'BBBBBB',
            bankCommission: 1,
            fxCommission: 1,
            rechargeCost: 1,
            useMerchantCommission: true,
            bankAccountNumber: 'BBBBBB',
            useDirectPayment: true,
            bankAccountType: 'BBBBBB',
            canChangePaymentMethod: true,
            mustNotify: true,
            profileInfoChanged: 'BBBBBB',
            confirmedProfile: true,
            resellerFixedCommission: 1,
            resellerPercentageCommission: 1,
            useFlatCommission: true,
            bankBranch: 'BBBBBB',
            alias: 'BBBBBB'
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            birthDate: currentDate,
            dateCreated: currentDate,
            lastUpdated: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a ExtendedUser', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
