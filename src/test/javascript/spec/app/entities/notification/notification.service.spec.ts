import { getTestBed, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { map, take } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { NotificationService } from 'app/entities/notification/notification.service';
import { INotification, Notification } from 'app/shared/model/notification.model';
import { Status } from 'app/shared/model/enumerations/status.model';
import { NotificationCategory } from 'app/shared/model/enumerations/notification-category.model';
import { NotificationSubCategory } from 'app/shared/model/enumerations/notification-sub-category.model';
import { ReceiverGroup } from 'app/shared/model/enumerations/receiver-group.model';

describe('Service Tests', () => {
  describe('Notification Service', () => {
    let injector: TestBed;
    let service: NotificationService;
    let httpMock: HttpTestingController;
    let elemDefault: INotification;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(NotificationService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Notification(
        0,
        'AAAAAAA',
        'AAAAAAA',
        currentDate,
        currentDate,
        Status.CREATED,
        'AAAAAAA',
        NotificationCategory.ISSUE,
        'AAAAAAA',
        NotificationSubCategory.MESSAGE,
        ReceiverGroup.ALL_ADMINS,
        0
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            dateCreated: currentDate.format(DATE_TIME_FORMAT),
            lastUpdated: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a Notification', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            dateCreated: currentDate.format(DATE_TIME_FORMAT),
            lastUpdated: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            dateCreated: currentDate,
            lastUpdated: currentDate
          },
          returnedFromService
        );
        service
          .create(new Notification(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a Notification', () => {
        const returnedFromService = Object.assign(
          {
            subject: 'BBBBBB',
            description: 'BBBBBB',
            dateCreated: currentDate.format(DATE_TIME_FORMAT),
            lastUpdated: currentDate.format(DATE_TIME_FORMAT),
            status: 'BBBBBB',
            statusReason: 'BBBBBB',
            category: 'BBBBBB',
            notes: 'BBBBBB',
            subCategory: 'BBBBBB',
            receiverGroup: 'BBBBBB',
            entityId: 1
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateCreated: currentDate,
            lastUpdated: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of Notification', () => {
        const returnedFromService = Object.assign(
          {
            subject: 'BBBBBB',
            description: 'BBBBBB',
            dateCreated: currentDate.format(DATE_TIME_FORMAT),
            lastUpdated: currentDate.format(DATE_TIME_FORMAT),
            status: 'BBBBBB',
            statusReason: 'BBBBBB',
            category: 'BBBBBB',
            notes: 'BBBBBB',
            subCategory: 'BBBBBB',
            receiverGroup: 'BBBBBB',
            entityId: 1
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            dateCreated: currentDate,
            lastUpdated: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Notification', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
