import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { PagoMundoApiTestModule } from '../../../test.module';
import { TransactionStatusChangeUpdateComponent } from 'app/entities/transaction-status-change/transaction-status-change-update.component';
import { TransactionStatusChangeService } from 'app/entities/transaction-status-change/transaction-status-change.service';
import { TransactionStatusChange } from 'app/shared/model/transaction-status-change.model';

describe('Component Tests', () => {
  describe('TransactionStatusChange Management Update Component', () => {
    let comp: TransactionStatusChangeUpdateComponent;
    let fixture: ComponentFixture<TransactionStatusChangeUpdateComponent>;
    let service: TransactionStatusChangeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PagoMundoApiTestModule],
        declarations: [TransactionStatusChangeUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(TransactionStatusChangeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(TransactionStatusChangeUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TransactionStatusChangeService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new TransactionStatusChange(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new TransactionStatusChange();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
