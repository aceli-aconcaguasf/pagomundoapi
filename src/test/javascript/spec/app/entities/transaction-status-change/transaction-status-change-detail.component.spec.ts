import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PagoMundoApiTestModule } from '../../../test.module';
import { TransactionStatusChangeDetailComponent } from 'app/entities/transaction-status-change/transaction-status-change-detail.component';
import { TransactionStatusChange } from 'app/shared/model/transaction-status-change.model';

describe('Component Tests', () => {
  describe('TransactionStatusChange Management Detail Component', () => {
    let comp: TransactionStatusChangeDetailComponent;
    let fixture: ComponentFixture<TransactionStatusChangeDetailComponent>;
    const route = ({ data: of({ transactionStatusChange: new TransactionStatusChange(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PagoMundoApiTestModule],
        declarations: [TransactionStatusChangeDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(TransactionStatusChangeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TransactionStatusChangeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.transactionStatusChange).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
