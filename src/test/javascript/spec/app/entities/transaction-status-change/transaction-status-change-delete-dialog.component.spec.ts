import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PagoMundoApiTestModule } from '../../../test.module';
import { TransactionStatusChangeDeleteDialogComponent } from 'app/entities/transaction-status-change/transaction-status-change-delete-dialog.component';
import { TransactionStatusChangeService } from 'app/entities/transaction-status-change/transaction-status-change.service';

describe('Component Tests', () => {
  describe('TransactionStatusChange Management Delete Component', () => {
    let comp: TransactionStatusChangeDeleteDialogComponent;
    let fixture: ComponentFixture<TransactionStatusChangeDeleteDialogComponent>;
    let service: TransactionStatusChangeService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PagoMundoApiTestModule],
        declarations: [TransactionStatusChangeDeleteDialogComponent]
      })
        .overrideTemplate(TransactionStatusChangeDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TransactionStatusChangeDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TransactionStatusChangeService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
