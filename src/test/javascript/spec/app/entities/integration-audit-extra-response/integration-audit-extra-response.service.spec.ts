import { getTestBed, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { map, take } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { IntegrationAuditExtraResponseService } from 'app/entities/integration-audit-extra-response/integration-audit-extra-response.service';
import { IIntegrationAuditExtraResponse, IntegrationAuditExtraResponse } from 'app/shared/model/integration-audit-extra-response.model';

describe('Service Tests', () => {
  describe('IntegrationAuditExtraResponse Service', () => {
    let injector: TestBed;
    let service: IntegrationAuditExtraResponseService;
    let httpMock: HttpTestingController;
    let elemDefault: IIntegrationAuditExtraResponse;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(IntegrationAuditExtraResponseService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new IntegrationAuditExtraResponse(0, currentDate, 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            messageDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a IntegrationAuditExtraResponse', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            messageDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            messageDate: currentDate
          },
          returnedFromService
        );
        service
          .create(new IntegrationAuditExtraResponse(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a IntegrationAuditExtraResponse', () => {
        const returnedFromService = Object.assign(
          {
            messageDate: currentDate.format(DATE_TIME_FORMAT),
            message: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            messageDate: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of IntegrationAuditExtraResponse', () => {
        const returnedFromService = Object.assign(
          {
            messageDate: currentDate.format(DATE_TIME_FORMAT),
            message: 'BBBBBB'
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            messageDate: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a IntegrationAuditExtraResponse', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
