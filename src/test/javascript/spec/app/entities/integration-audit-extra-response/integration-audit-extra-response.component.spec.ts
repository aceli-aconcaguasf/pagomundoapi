import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Data } from '@angular/router';

import { PagoMundoApiTestModule } from '../../../test.module';
import { IntegrationAuditExtraResponseComponent } from 'app/entities/integration-audit-extra-response/integration-audit-extra-response.component';
import { IntegrationAuditExtraResponseService } from 'app/entities/integration-audit-extra-response/integration-audit-extra-response.service';
import { IntegrationAuditExtraResponse } from 'app/shared/model/integration-audit-extra-response.model';

describe('Component Tests', () => {
  describe('IntegrationAuditExtraResponse Management Component', () => {
    let comp: IntegrationAuditExtraResponseComponent;
    let fixture: ComponentFixture<IntegrationAuditExtraResponseComponent>;
    let service: IntegrationAuditExtraResponseService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PagoMundoApiTestModule],
        declarations: [IntegrationAuditExtraResponseComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: {
              data: {
                subscribe: (fn: (value: Data) => void) =>
                  fn({
                    pagingParams: {
                      predicate: 'id',
                      reverse: false,
                      page: 0
                    }
                  })
              }
            }
          }
        ]
      })
        .overrideTemplate(IntegrationAuditExtraResponseComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(IntegrationAuditExtraResponseComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(IntegrationAuditExtraResponseService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new IntegrationAuditExtraResponse(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.integrationAuditExtraResponses[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should load a page', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new IntegrationAuditExtraResponse(123)],
            headers
          })
        )
      );

      // WHEN
      comp.loadPage(1);

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.integrationAuditExtraResponses[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should not load a page is the page is the same as the previous page', () => {
      spyOn(service, 'query').and.callThrough();

      // WHEN
      comp.loadPage(0);

      // THEN
      expect(service.query).toHaveBeenCalledTimes(0);
    });

    it('should re-initialize the page', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new IntegrationAuditExtraResponse(123)],
            headers
          })
        )
      );

      // WHEN
      comp.loadPage(1);
      comp.clear();

      // THEN
      expect(comp.page).toEqual(0);
      expect(service.query).toHaveBeenCalledTimes(2);
      expect(comp.integrationAuditExtraResponses[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
    it('should calculate the sort attribute for an id', () => {
      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,desc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // GIVEN
      comp.predicate = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,desc', 'id']);
    });
  });
});
