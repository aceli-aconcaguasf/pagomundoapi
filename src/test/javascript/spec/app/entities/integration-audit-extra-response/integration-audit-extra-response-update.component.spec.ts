import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { PagoMundoApiTestModule } from '../../../test.module';
import { IntegrationAuditExtraResponseUpdateComponent } from 'app/entities/integration-audit-extra-response/integration-audit-extra-response-update.component';
import { IntegrationAuditExtraResponseService } from 'app/entities/integration-audit-extra-response/integration-audit-extra-response.service';
import { IntegrationAuditExtraResponse } from 'app/shared/model/integration-audit-extra-response.model';

describe('Component Tests', () => {
  describe('IntegrationAuditExtraResponse Management Update Component', () => {
    let comp: IntegrationAuditExtraResponseUpdateComponent;
    let fixture: ComponentFixture<IntegrationAuditExtraResponseUpdateComponent>;
    let service: IntegrationAuditExtraResponseService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PagoMundoApiTestModule],
        declarations: [IntegrationAuditExtraResponseUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(IntegrationAuditExtraResponseUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(IntegrationAuditExtraResponseUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(IntegrationAuditExtraResponseService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new IntegrationAuditExtraResponse(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new IntegrationAuditExtraResponse();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
