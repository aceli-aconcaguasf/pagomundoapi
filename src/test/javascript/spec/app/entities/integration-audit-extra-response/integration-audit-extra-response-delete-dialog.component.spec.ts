import { ComponentFixture, fakeAsync, inject, TestBed, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PagoMundoApiTestModule } from '../../../test.module';
import { IntegrationAuditExtraResponseDeleteDialogComponent } from 'app/entities/integration-audit-extra-response/integration-audit-extra-response-delete-dialog.component';
import { IntegrationAuditExtraResponseService } from 'app/entities/integration-audit-extra-response/integration-audit-extra-response.service';

describe('Component Tests', () => {
  describe('IntegrationAuditExtraResponse Management Delete Component', () => {
    let comp: IntegrationAuditExtraResponseDeleteDialogComponent;
    let fixture: ComponentFixture<IntegrationAuditExtraResponseDeleteDialogComponent>;
    let service: IntegrationAuditExtraResponseService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PagoMundoApiTestModule],
        declarations: [IntegrationAuditExtraResponseDeleteDialogComponent]
      })
        .overrideTemplate(IntegrationAuditExtraResponseDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(IntegrationAuditExtraResponseDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(IntegrationAuditExtraResponseService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
