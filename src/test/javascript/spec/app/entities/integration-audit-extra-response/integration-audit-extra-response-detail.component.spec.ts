import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PagoMundoApiTestModule } from '../../../test.module';
import { IntegrationAuditExtraResponseDetailComponent } from 'app/entities/integration-audit-extra-response/integration-audit-extra-response-detail.component';
import { IntegrationAuditExtraResponse } from 'app/shared/model/integration-audit-extra-response.model';

describe('Component Tests', () => {
  describe('IntegrationAuditExtraResponse Management Detail Component', () => {
    let comp: IntegrationAuditExtraResponseDetailComponent;
    let fixture: ComponentFixture<IntegrationAuditExtraResponseDetailComponent>;
    const route = ({ data: of({ integrationAuditExtraResponse: new IntegrationAuditExtraResponse(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PagoMundoApiTestModule],
        declarations: [IntegrationAuditExtraResponseDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(IntegrationAuditExtraResponseDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(IntegrationAuditExtraResponseDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.integrationAuditExtraResponse).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
