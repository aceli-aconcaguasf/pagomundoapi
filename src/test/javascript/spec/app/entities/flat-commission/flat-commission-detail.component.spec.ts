import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PagoMundoApiTestModule } from '../../../test.module';
import { FlatCommissionDetailComponent } from 'app/entities/flat-commission/flat-commission-detail.component';
import { FlatCommission } from 'app/shared/model/flat-commission.model';

describe('Component Tests', () => {
  describe('FlatCommission Management Detail Component', () => {
    let comp: FlatCommissionDetailComponent;
    let fixture: ComponentFixture<FlatCommissionDetailComponent>;
    const route = ({ data: of({ flatCommission: new FlatCommission(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PagoMundoApiTestModule],
        declarations: [FlatCommissionDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(FlatCommissionDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(FlatCommissionDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.flatCommission).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
