import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PagoMundoApiTestModule } from '../../../test.module';
import { FlatCommissionDeleteDialogComponent } from 'app/entities/flat-commission/flat-commission-delete-dialog.component';
import { FlatCommissionService } from 'app/entities/flat-commission/flat-commission.service';

describe('Component Tests', () => {
  describe('FlatCommission Management Delete Component', () => {
    let comp: FlatCommissionDeleteDialogComponent;
    let fixture: ComponentFixture<FlatCommissionDeleteDialogComponent>;
    let service: FlatCommissionService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PagoMundoApiTestModule],
        declarations: [FlatCommissionDeleteDialogComponent]
      })
        .overrideTemplate(FlatCommissionDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(FlatCommissionDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FlatCommissionService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
