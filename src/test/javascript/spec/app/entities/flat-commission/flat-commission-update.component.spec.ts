import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { PagoMundoApiTestModule } from '../../../test.module';
import { FlatCommissionUpdateComponent } from 'app/entities/flat-commission/flat-commission-update.component';
import { FlatCommissionService } from 'app/entities/flat-commission/flat-commission.service';
import { FlatCommission } from 'app/shared/model/flat-commission.model';

describe('Component Tests', () => {
  describe('FlatCommission Management Update Component', () => {
    let comp: FlatCommissionUpdateComponent;
    let fixture: ComponentFixture<FlatCommissionUpdateComponent>;
    let service: FlatCommissionService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PagoMundoApiTestModule],
        declarations: [FlatCommissionUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(FlatCommissionUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FlatCommissionUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FlatCommissionService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new FlatCommission(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new FlatCommission();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
