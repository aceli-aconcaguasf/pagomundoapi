import { getTestBed, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { map, take } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { IntegrationAuditService } from 'app/entities/integration-audit/integration-audit.service';
import { IIntegrationAudit, IntegrationAudit } from 'app/shared/model/integration-audit.model';
import { Status } from 'app/shared/model/enumerations/status.model';

describe('Service Tests', () => {
  describe('IntegrationAudit Service', () => {
    let injector: TestBed;
    let service: IntegrationAuditService;
    let httpMock: HttpTestingController;
    let elemDefault: IIntegrationAudit;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(IntegrationAuditService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new IntegrationAudit(
        0,
        currentDate,
        'AAAAAAA',
        'AAAAAAA',
        currentDate,
        'AAAAAAA',
        0,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        Status.CREATED,
        'AAAAAAA',
        0,
        0
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            requestDate: currentDate.format(DATE_TIME_FORMAT),
            responseDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a IntegrationAudit', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            requestDate: currentDate.format(DATE_TIME_FORMAT),
            responseDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            requestDate: currentDate,
            responseDate: currentDate
          },
          returnedFromService
        );
        service
          .create(new IntegrationAudit(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a IntegrationAudit', () => {
        const returnedFromService = Object.assign(
          {
            requestDate: currentDate.format(DATE_TIME_FORMAT),
            requestMessage: 'BBBBBB',
            requestEndpoint: 'BBBBBB',
            responseDate: currentDate.format(DATE_TIME_FORMAT),
            responseMessage: 'BBBBBB',
            entityId: 1,
            entity: 'BBBBBB',
            providerId: 'BBBBBB',
            provider: 'BBBBBB',
            extraResponseEndpoint: 'BBBBBB',
            status: 'BBBBBB',
            statusReason: 'BBBBBB',
            retries: 1,
            maxRetries: 1
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            requestDate: currentDate,
            responseDate: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of IntegrationAudit', () => {
        const returnedFromService = Object.assign(
          {
            requestDate: currentDate.format(DATE_TIME_FORMAT),
            requestMessage: 'BBBBBB',
            requestEndpoint: 'BBBBBB',
            responseDate: currentDate.format(DATE_TIME_FORMAT),
            responseMessage: 'BBBBBB',
            entityId: 1,
            entity: 'BBBBBB',
            providerId: 'BBBBBB',
            provider: 'BBBBBB',
            extraResponseEndpoint: 'BBBBBB',
            status: 'BBBBBB',
            statusReason: 'BBBBBB',
            retries: 1,
            maxRetries: 1
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            requestDate: currentDate,
            responseDate: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a IntegrationAudit', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
