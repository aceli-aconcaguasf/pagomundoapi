import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PagoMundoApiTestModule } from '../../../test.module';
import { IntegrationAuditDetailComponent } from 'app/entities/integration-audit/integration-audit-detail.component';
import { IntegrationAudit } from 'app/shared/model/integration-audit.model';

describe('Component Tests', () => {
  describe('IntegrationAudit Management Detail Component', () => {
    let comp: IntegrationAuditDetailComponent;
    let fixture: ComponentFixture<IntegrationAuditDetailComponent>;
    const route = ({ data: of({ integrationAudit: new IntegrationAudit(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PagoMundoApiTestModule],
        declarations: [IntegrationAuditDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(IntegrationAuditDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(IntegrationAuditDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.integrationAudit).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
