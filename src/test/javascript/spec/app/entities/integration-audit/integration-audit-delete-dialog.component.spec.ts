import { ComponentFixture, fakeAsync, inject, TestBed, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PagoMundoApiTestModule } from '../../../test.module';
import { IntegrationAuditDeleteDialogComponent } from 'app/entities/integration-audit/integration-audit-delete-dialog.component';
import { IntegrationAuditService } from 'app/entities/integration-audit/integration-audit.service';

describe('Component Tests', () => {
  describe('IntegrationAudit Management Delete Component', () => {
    let comp: IntegrationAuditDeleteDialogComponent;
    let fixture: ComponentFixture<IntegrationAuditDeleteDialogComponent>;
    let service: IntegrationAuditService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PagoMundoApiTestModule],
        declarations: [IntegrationAuditDeleteDialogComponent]
      })
        .overrideTemplate(IntegrationAuditDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(IntegrationAuditDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(IntegrationAuditService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
