import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PagoMundoApiTestModule } from '../../../test.module';
import { DirectPaymentBankDetailComponent } from 'app/entities/direct-payment-bank/direct-payment-bank-detail.component';
import { DirectPaymentBank } from 'app/shared/model/direct-payment-bank.model';

describe('Component Tests', () => {
  describe('DirectPaymentBank Management Detail Component', () => {
    let comp: DirectPaymentBankDetailComponent;
    let fixture: ComponentFixture<DirectPaymentBankDetailComponent>;
    const route = ({ data: of({ directPaymentBank: new DirectPaymentBank(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PagoMundoApiTestModule],
        declarations: [DirectPaymentBankDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(DirectPaymentBankDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DirectPaymentBankDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.directPaymentBank).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
