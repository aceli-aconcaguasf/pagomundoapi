import { ComponentFixture, fakeAsync, inject, TestBed, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PagoMundoApiTestModule } from '../../../test.module';
import { DirectPaymentBankDeleteDialogComponent } from 'app/entities/direct-payment-bank/direct-payment-bank-delete-dialog.component';
import { DirectPaymentBankService } from 'app/entities/direct-payment-bank/direct-payment-bank.service';

describe('Component Tests', () => {
  describe('DirectPaymentBank Management Delete Component', () => {
    let comp: DirectPaymentBankDeleteDialogComponent;
    let fixture: ComponentFixture<DirectPaymentBankDeleteDialogComponent>;
    let service: DirectPaymentBankService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PagoMundoApiTestModule],
        declarations: [DirectPaymentBankDeleteDialogComponent]
      })
        .overrideTemplate(DirectPaymentBankDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DirectPaymentBankDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(DirectPaymentBankService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
