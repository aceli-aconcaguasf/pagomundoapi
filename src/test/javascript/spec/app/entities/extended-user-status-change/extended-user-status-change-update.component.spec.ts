import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { PagoMundoApiTestModule } from '../../../test.module';
import { ExtendedUserStatusChangeUpdateComponent } from 'app/entities/extended-user-status-change/extended-user-status-change-update.component';
import { ExtendedUserStatusChangeService } from 'app/entities/extended-user-status-change/extended-user-status-change.service';
import { ExtendedUserStatusChange } from 'app/shared/model/extended-user-status-change.model';

describe('Component Tests', () => {
  describe('ExtendedUserStatusChange Management Update Component', () => {
    let comp: ExtendedUserStatusChangeUpdateComponent;
    let fixture: ComponentFixture<ExtendedUserStatusChangeUpdateComponent>;
    let service: ExtendedUserStatusChangeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PagoMundoApiTestModule],
        declarations: [ExtendedUserStatusChangeUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ExtendedUserStatusChangeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ExtendedUserStatusChangeUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ExtendedUserStatusChangeService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ExtendedUserStatusChange(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ExtendedUserStatusChange();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
