import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PagoMundoApiTestModule } from '../../../test.module';
import { ExtendedUserStatusChangeDetailComponent } from 'app/entities/extended-user-status-change/extended-user-status-change-detail.component';
import { ExtendedUserStatusChange } from 'app/shared/model/extended-user-status-change.model';

describe('Component Tests', () => {
  describe('ExtendedUserStatusChange Management Detail Component', () => {
    let comp: ExtendedUserStatusChangeDetailComponent;
    let fixture: ComponentFixture<ExtendedUserStatusChangeDetailComponent>;
    const route = ({ data: of({ extendedUserStatusChange: new ExtendedUserStatusChange(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PagoMundoApiTestModule],
        declarations: [ExtendedUserStatusChangeDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ExtendedUserStatusChangeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ExtendedUserStatusChangeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.extendedUserStatusChange).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
