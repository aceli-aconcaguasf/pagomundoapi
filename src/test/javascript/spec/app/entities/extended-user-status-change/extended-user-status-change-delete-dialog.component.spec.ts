import { ComponentFixture, fakeAsync, inject, TestBed, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PagoMundoApiTestModule } from '../../../test.module';
import { ExtendedUserStatusChangeDeleteDialogComponent } from 'app/entities/extended-user-status-change/extended-user-status-change-delete-dialog.component';
import { ExtendedUserStatusChangeService } from 'app/entities/extended-user-status-change/extended-user-status-change.service';

describe('Component Tests', () => {
  describe('ExtendedUserStatusChange Management Delete Component', () => {
    let comp: ExtendedUserStatusChangeDeleteDialogComponent;
    let fixture: ComponentFixture<ExtendedUserStatusChangeDeleteDialogComponent>;
    let service: ExtendedUserStatusChangeService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PagoMundoApiTestModule],
        declarations: [ExtendedUserStatusChangeDeleteDialogComponent]
      })
        .overrideTemplate(ExtendedUserStatusChangeDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ExtendedUserStatusChangeDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ExtendedUserStatusChangeService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
