import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { PagoMundoApiTestModule } from '../../../test.module';
import { NotificationStatusChangeUpdateComponent } from 'app/entities/notification-status-change/notification-status-change-update.component';
import { NotificationStatusChangeService } from 'app/entities/notification-status-change/notification-status-change.service';
import { NotificationStatusChange } from 'app/shared/model/notification-status-change.model';

describe('Component Tests', () => {
  describe('NotificationStatusChange Management Update Component', () => {
    let comp: NotificationStatusChangeUpdateComponent;
    let fixture: ComponentFixture<NotificationStatusChangeUpdateComponent>;
    let service: NotificationStatusChangeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PagoMundoApiTestModule],
        declarations: [NotificationStatusChangeUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(NotificationStatusChangeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(NotificationStatusChangeUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(NotificationStatusChangeService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new NotificationStatusChange(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new NotificationStatusChange();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
