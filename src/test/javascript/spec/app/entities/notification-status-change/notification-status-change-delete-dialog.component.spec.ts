import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PagoMundoApiTestModule } from '../../../test.module';
import { NotificationStatusChangeDeleteDialogComponent } from 'app/entities/notification-status-change/notification-status-change-delete-dialog.component';
import { NotificationStatusChangeService } from 'app/entities/notification-status-change/notification-status-change.service';

describe('Component Tests', () => {
  describe('NotificationStatusChange Management Delete Component', () => {
    let comp: NotificationStatusChangeDeleteDialogComponent;
    let fixture: ComponentFixture<NotificationStatusChangeDeleteDialogComponent>;
    let service: NotificationStatusChangeService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PagoMundoApiTestModule],
        declarations: [NotificationStatusChangeDeleteDialogComponent]
      })
        .overrideTemplate(NotificationStatusChangeDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(NotificationStatusChangeDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(NotificationStatusChangeService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
