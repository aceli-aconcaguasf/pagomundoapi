import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PagoMundoApiTestModule } from '../../../test.module';
import { NotificationStatusChangeDetailComponent } from 'app/entities/notification-status-change/notification-status-change-detail.component';
import { NotificationStatusChange } from 'app/shared/model/notification-status-change.model';

describe('Component Tests', () => {
  describe('NotificationStatusChange Management Detail Component', () => {
    let comp: NotificationStatusChangeDetailComponent;
    let fixture: ComponentFixture<NotificationStatusChangeDetailComponent>;
    const route = ({ data: of({ notificationStatusChange: new NotificationStatusChange(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PagoMundoApiTestModule],
        declarations: [NotificationStatusChangeDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(NotificationStatusChangeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(NotificationStatusChangeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.notificationStatusChange).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
