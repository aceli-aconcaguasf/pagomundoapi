import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PagoMundoApiTestModule } from '../../../test.module';
import { ExtendedUserFundsDetailComponent } from 'app/entities/extended-user-funds/extended-user-funds-detail.component';
import { ExtendedUserFunds } from 'app/shared/model/extended-user-funds.model';

describe('Component Tests', () => {
  describe('ExtendedUserFunds Management Detail Component', () => {
    let comp: ExtendedUserFundsDetailComponent;
    let fixture: ComponentFixture<ExtendedUserFundsDetailComponent>;
    const route = ({ data: of({ extendedUserFunds: new ExtendedUserFunds(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PagoMundoApiTestModule],
        declarations: [ExtendedUserFundsDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ExtendedUserFundsDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ExtendedUserFundsDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.extendedUserFunds).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
