import { ComponentFixture, fakeAsync, inject, TestBed, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PagoMundoApiTestModule } from '../../../test.module';
import { ExtendedUserFundsDeleteDialogComponent } from 'app/entities/extended-user-funds/extended-user-funds-delete-dialog.component';
import { ExtendedUserFundsService } from 'app/entities/extended-user-funds/extended-user-funds.service';

describe('Component Tests', () => {
  describe('ExtendedUserFunds Management Delete Component', () => {
    let comp: ExtendedUserFundsDeleteDialogComponent;
    let fixture: ComponentFixture<ExtendedUserFundsDeleteDialogComponent>;
    let service: ExtendedUserFundsService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PagoMundoApiTestModule],
        declarations: [ExtendedUserFundsDeleteDialogComponent]
      })
        .overrideTemplate(ExtendedUserFundsDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ExtendedUserFundsDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ExtendedUserFundsService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
