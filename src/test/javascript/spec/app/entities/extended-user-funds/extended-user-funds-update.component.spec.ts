import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { PagoMundoApiTestModule } from '../../../test.module';
import { ExtendedUserFundsUpdateComponent } from 'app/entities/extended-user-funds/extended-user-funds-update.component';
import { ExtendedUserFundsService } from 'app/entities/extended-user-funds/extended-user-funds.service';
import { ExtendedUserFunds } from 'app/shared/model/extended-user-funds.model';

describe('Component Tests', () => {
  describe('ExtendedUserFunds Management Update Component', () => {
    let comp: ExtendedUserFundsUpdateComponent;
    let fixture: ComponentFixture<ExtendedUserFundsUpdateComponent>;
    let service: ExtendedUserFundsService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PagoMundoApiTestModule],
        declarations: [ExtendedUserFundsUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ExtendedUserFundsUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ExtendedUserFundsUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ExtendedUserFundsService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ExtendedUserFunds(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ExtendedUserFunds();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
