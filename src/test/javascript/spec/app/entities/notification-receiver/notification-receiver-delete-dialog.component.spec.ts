import { ComponentFixture, fakeAsync, inject, TestBed, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PagoMundoApiTestModule } from '../../../test.module';
import { NotificationReceiverDeleteDialogComponent } from 'app/entities/notification-receiver/notification-receiver-delete-dialog.component';
import { NotificationReceiverService } from 'app/entities/notification-receiver/notification-receiver.service';

describe('Component Tests', () => {
  describe('NotificationReceiver Management Delete Component', () => {
    let comp: NotificationReceiverDeleteDialogComponent;
    let fixture: ComponentFixture<NotificationReceiverDeleteDialogComponent>;
    let service: NotificationReceiverService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PagoMundoApiTestModule],
        declarations: [NotificationReceiverDeleteDialogComponent]
      })
        .overrideTemplate(NotificationReceiverDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(NotificationReceiverDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(NotificationReceiverService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
