import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { PagoMundoApiTestModule } from '../../../test.module';
import { ExtendedUserGroupUpdateComponent } from 'app/entities/extended-user-group/extended-user-group-update.component';
import { ExtendedUserGroupService } from 'app/entities/extended-user-group/extended-user-group.service';
import { ExtendedUserGroup } from 'app/shared/model/extended-user-group.model';

describe('Component Tests', () => {
  describe('ExtendedUserGroup Management Update Component', () => {
    let comp: ExtendedUserGroupUpdateComponent;
    let fixture: ComponentFixture<ExtendedUserGroupUpdateComponent>;
    let service: ExtendedUserGroupService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PagoMundoApiTestModule],
        declarations: [ExtendedUserGroupUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ExtendedUserGroupUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ExtendedUserGroupUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ExtendedUserGroupService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ExtendedUserGroup(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ExtendedUserGroup();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
