import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PagoMundoApiTestModule } from '../../../test.module';
import { ExtendedUserGroupDetailComponent } from 'app/entities/extended-user-group/extended-user-group-detail.component';
import { ExtendedUserGroup } from 'app/shared/model/extended-user-group.model';

describe('Component Tests', () => {
  describe('ExtendedUserGroup Management Detail Component', () => {
    let comp: ExtendedUserGroupDetailComponent;
    let fixture: ComponentFixture<ExtendedUserGroupDetailComponent>;
    const route = ({ data: of({ extendedUserGroup: new ExtendedUserGroup(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PagoMundoApiTestModule],
        declarations: [ExtendedUserGroupDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ExtendedUserGroupDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ExtendedUserGroupDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.extendedUserGroup).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
