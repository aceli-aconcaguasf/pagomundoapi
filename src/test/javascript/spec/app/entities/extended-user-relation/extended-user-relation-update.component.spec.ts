import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { PagoMundoApiTestModule } from '../../../test.module';
import { ExtendedUserRelationUpdateComponent } from 'app/entities/extended-user-relation/extended-user-relation-update.component';
import { ExtendedUserRelationService } from 'app/entities/extended-user-relation/extended-user-relation.service';
import { ExtendedUserRelation } from 'app/shared/model/extended-user-relation.model';

describe('Component Tests', () => {
  describe('ExtendedUserRelation Management Update Component', () => {
    let comp: ExtendedUserRelationUpdateComponent;
    let fixture: ComponentFixture<ExtendedUserRelationUpdateComponent>;
    let service: ExtendedUserRelationService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PagoMundoApiTestModule],
        declarations: [ExtendedUserRelationUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ExtendedUserRelationUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ExtendedUserRelationUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ExtendedUserRelationService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ExtendedUserRelation(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ExtendedUserRelation();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
