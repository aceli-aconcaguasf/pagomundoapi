import { ComponentFixture, fakeAsync, inject, TestBed, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { PagoMundoApiTestModule } from '../../../test.module';
import { ExtendedUserRelationDeleteDialogComponent } from 'app/entities/extended-user-relation/extended-user-relation-delete-dialog.component';
import { ExtendedUserRelationService } from 'app/entities/extended-user-relation/extended-user-relation.service';

describe('Component Tests', () => {
  describe('ExtendedUserRelation Management Delete Component', () => {
    let comp: ExtendedUserRelationDeleteDialogComponent;
    let fixture: ComponentFixture<ExtendedUserRelationDeleteDialogComponent>;
    let service: ExtendedUserRelationService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PagoMundoApiTestModule],
        declarations: [ExtendedUserRelationDeleteDialogComponent]
      })
        .overrideTemplate(ExtendedUserRelationDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ExtendedUserRelationDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ExtendedUserRelationService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
