import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PagoMundoApiTestModule } from '../../../test.module';
import { ExtendedUserRelationDetailComponent } from 'app/entities/extended-user-relation/extended-user-relation-detail.component';
import { ExtendedUserRelation } from 'app/shared/model/extended-user-relation.model';

describe('Component Tests', () => {
  describe('ExtendedUserRelation Management Detail Component', () => {
    let comp: ExtendedUserRelationDetailComponent;
    let fixture: ComponentFixture<ExtendedUserRelationDetailComponent>;
    const route = ({ data: of({ extendedUserRelation: new ExtendedUserRelation(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [PagoMundoApiTestModule],
        declarations: [ExtendedUserRelationDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ExtendedUserRelationDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ExtendedUserRelationDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.extendedUserRelation).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
