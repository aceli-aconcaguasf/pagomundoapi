import { getTestBed, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { map, take } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { TransactionService } from 'app/entities/transaction/transaction.service';
import { ITransaction, Transaction } from 'app/shared/model/transaction.model';
import { Status } from 'app/shared/model/enumerations/status.model';
import { ProcessType } from 'app/shared/model/enumerations/process-type.model';

describe('Service Tests', () => {
  describe('Transaction Service', () => {
    let injector: TestBed;
    let service: TransactionService;
    let httpMock: HttpTestingController;
    let elemDefault: ITransaction;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(TransactionService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Transaction(
        0,
        currentDate,
        currentDate,
        Status.CREATED,
        'AAAAAAA',
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        false,
        'AAAAAAA',
        0,
        0,
        0,
        ProcessType.FILE_EXPORT
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            dateCreated: currentDate.format(DATE_TIME_FORMAT),
            lastUpdated: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a Transaction', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            dateCreated: currentDate.format(DATE_TIME_FORMAT),
            lastUpdated: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            dateCreated: currentDate,
            lastUpdated: currentDate
          },
          returnedFromService
        );
        service
          .create(new Transaction(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a Transaction', () => {
        const returnedFromService = Object.assign(
          {
            dateCreated: currentDate.format(DATE_TIME_FORMAT),
            lastUpdated: currentDate.format(DATE_TIME_FORMAT),
            status: 'BBBBBB',
            statusReason: 'BBBBBB',
            amountBeforeCommission: 1,
            bankCommission: 1,
            fxCommission: 1,
            rechargeCost: 1,
            amountAfterCommission: 1,
            exchangeRate: 1,
            amountLocalCurrency: 1,
            bankReference: 'BBBBBB',
            fileName: 'BBBBBB',
            description: 'BBBBBB',
            notes: 'BBBBBB',
            mustNotify: true,
            infoChanged: 'BBBBBB',
            resellerCommission: 1,
            resellerFixedCommission: 1,
            resellerPercentageCommission: 1,
            processType: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            dateCreated: currentDate,
            lastUpdated: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of Transaction', () => {
        const returnedFromService = Object.assign(
          {
            dateCreated: currentDate.format(DATE_TIME_FORMAT),
            lastUpdated: currentDate.format(DATE_TIME_FORMAT),
            status: 'BBBBBB',
            statusReason: 'BBBBBB',
            amountBeforeCommission: 1,
            bankCommission: 1,
            fxCommission: 1,
            rechargeCost: 1,
            amountAfterCommission: 1,
            exchangeRate: 1,
            amountLocalCurrency: 1,
            bankReference: 'BBBBBB',
            fileName: 'BBBBBB',
            description: 'BBBBBB',
            notes: 'BBBBBB',
            mustNotify: true,
            infoChanged: 'BBBBBB',
            resellerCommission: 1,
            resellerFixedCommission: 1,
            resellerPercentageCommission: 1,
            processType: 'BBBBBB'
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            dateCreated: currentDate,
            lastUpdated: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Transaction', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
