-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: pagomundo
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `extended_user_group`
--

DROP TABLE IF EXISTS `extended_user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `extended_user_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_created` datetime,
  `status` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_name_from_bank` varchar(255) DEFAULT NULL,
  `admin_id` bigint(20) DEFAULT NULL,
  `bank_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_extended_user_group_admin_id` (`admin_id`),
  KEY `fk_extended_user_group_bank_id` (`bank_id`),
  CONSTRAINT `fk_extended_user_group_admin_id` FOREIGN KEY (`admin_id`) REFERENCES `extended_user` (`id`),
  CONSTRAINT `fk_extended_user_group_bank_id` FOREIGN KEY (`bank_id`) REFERENCES `bank` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extended_user_group`
--

LOCK TABLES `extended_user_group` WRITE;
/*!40000 ALTER TABLE `extended_user_group` DISABLE KEYS */;
INSERT INTO `extended_user_group` VALUES (1,'2019-11-07 06:37:03','ACCEPTED','Dam','Practical Fresh Pants',NULL,NULL),(2,'2019-11-07 01:10:47','CANCELLED','Tasty Metal Bacon','red Chips Soap',NULL,NULL),(3,'2019-11-07 08:18:17','ACCEPTED','Namibia','Steel zero administration',NULL,NULL),(4,'2019-11-07 06:48:50','REJECTED','Savings Account syndicate Planner','Refined Rubber Chicken Table',NULL,NULL),(5,'2019-11-07 14:02:43','CREATED','South Dakota envisioneer Music','Nakfa',NULL,NULL),(6,'2019-11-07 05:02:34','ACCEPTED','incentivize Tuna copy','Costa Rica Mayotte',NULL,NULL),(7,'2019-11-07 11:10:12','IN_PROCESS','Views e-enable Communications','National Awesome sky blue',NULL,NULL),(8,'2019-11-07 14:31:16','CREATED','Louisiana Specialist Home Loan Account','Plastic',NULL,NULL),(9,'2019-11-06 20:21:14','CREATED','deliverables reinvent 24 hour','overriding',NULL,NULL),(10,'2019-11-06 22:38:31','IN_PROCESS','Internal','Investment Account archive Fantastic Plastic Chicken',NULL,NULL),(11,'2019-11-18 15:07:28','IN_PROCESS','fileBucketDev/extendedUser/exportedFiles/2019_11_18_12_07_28_37-Pichincha-Colombia.csv',NULL,NULL,1),(12,'2019-11-18 15:17:34','IN_PROCESS','fileBucketDev/extendedUser/exportedFiles/2019_11_18_12_17_33_78-Pichincha-Colombia.csv',NULL,NULL,1),(14,'2019-11-18 15:27:09','IN_PROCESS','fileBucketDev/extendedUser/exportedFiles/2019_11_18_12_27_09_31-Itau-Colombia.xlsx',NULL,NULL,2),(15,'2019-11-18 19:11:42','IN_PROCESS','fileBucketDev/extendedUser/exportedFiles/2019_11_18_16_11_42_33-Itau-Colombia.xlsx',NULL,NULL,2),(16,'2019-11-18 19:12:08','IN_PROCESS','fileBucketDev/extendedUser/exportedFiles/2019_11_18_16_12_07_68-Banxico-Mexico.csv',NULL,NULL,3),(17,'2019-11-18 19:28:49','IN_PROCESS','fileBucketDev/extendedUser/exportedFiles/2019_11_18_16_28_48_92-Banxico-Mexico.csv',NULL,NULL,3),(18,'2019-11-18 19:34:40','IN_PROCESS','fileBucketDev/extendedUser/exportedFiles/2019_11_18_16_34_40_15-Banxico-Mexico.csv',NULL,NULL,3),(19,'2019-11-21 15:48:29','CREATED',NULL,NULL,NULL,3),(20,'2019-11-21 15:52:28','CREATED',NULL,NULL,NULL,3),(21,'2019-11-21 15:54:36','CREATED',NULL,NULL,NULL,3),(22,'2019-11-21 15:56:26','CREATED',NULL,NULL,NULL,3),(24,'2019-11-21 17:49:19','ACCEPTED','fileBucketDev/extendedUser/exportedFiles/2019_11_21_14_49_19_00-Banxico-Mexico.csv',NULL,94,3),(25,'2019-11-21 17:52:34','ACCEPTED','fileBucketDev/extendedUser/exportedFiles/2019_11_21_14_52_34_00-Banxico-Mexico.csv',NULL,94,3),(26,'2019-11-21 17:54:00','ACCEPTED','fileBucketDev/extendedUser/exportedFiles/2019_11_21_14_54_00_00-Banxico-Mexico.csv',NULL,94,3),(27,'2019-11-21 17:54:44','ACCEPTED','fileBucketDev/extendedUser/exportedFiles/2019_11_21_14_54_44_00-Banxico-Mexico.csv',NULL,94,3),(28,'2019-11-21 17:55:42','ACCEPTED','fileBucketDev/extendedUser/exportedFiles/2019_11_21_14_55_42_00-Banxico-Mexico.csv',NULL,94,3),(29,'2019-11-21 17:56:34','CREATED',NULL,NULL,94,3),(30,'2019-11-21 18:02:19','IN_PROCESS',NULL,NULL,94,3),(31,'2019-11-21 18:02:19','ACCEPTED','fileBucketDev/extendedUser/exportedFiles/2019_11_21_15_02_18_93-Banxico-Mexico.csv',NULL,94,3),(32,'2019-11-21 18:04:53','IN_PROCESS',NULL,NULL,94,3),(33,'2019-11-21 18:04:53','ACCEPTED','fileBucketDev/extendedUser/exportedFiles/2019_11_21_15_04_52_94-Banxico-Mexico.csv',NULL,94,3),(39,'2019-11-21 18:11:59','ACCEPTED','fileBucketDev/extendedUser/exportedFiles/2019_11_21_15_11_58_83-Banxico-Mexico.csv',NULL,94,3),(41,'2019-11-21 18:14:54','ACCEPTED','fileBucketDev/extendedUser/exportedFiles/2019_11_21_15_14_54_08-Banxico-Mexico.csv',NULL,94,3),(43,'2019-11-21 18:15:01','ACCEPTED','fileBucketDev/extendedUser/exportedFiles/2019_11_21_15_15_01_41-Banxico-Mexico.csv',NULL,94,3),(45,'2019-11-21 18:15:05','ACCEPTED','fileBucketDev/extendedUser/exportedFiles/2019_11_21_15_15_05_44-Banxico-Mexico.csv',NULL,94,3),(46,'2019-11-21 18:24:31','CREATED',NULL,NULL,94,3),(47,'2019-11-21 18:24:31','ACCEPTED','fileBucketDev/extendedUser/exportedFiles/2019_11_21_15_24_31_48-Banxico-Mexico.csv',NULL,94,3),(64,'2019-11-22 14:50:47','CREATED',NULL,NULL,94,3),(65,'2019-11-22 14:53:06','CREATED',NULL,NULL,94,3),(66,'2019-11-22 14:55:42','ACCEPTED','fileBucketDev/extendedUser/exportedFiles/2019_11_22_11_55_42_00-Banxico-Mexico.csv',NULL,94,3),(67,'2019-11-22 15:11:45','ACCEPTED','fileBucketDev/extendedUser/exportedFiles/2019_11_22_12_11_45_00-Banxico-Mexico.csv',NULL,94,3),(69,'2019-11-25 13:19:10','ACCEPTED','fileBucketDev/extendedUser/exportedFiles/2019_11_25_10_19_10_00-Banxico-Mexico.csv',NULL,NULL,3);
/*!40000 ALTER TABLE `extended_user_group` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-26  7:49:32
