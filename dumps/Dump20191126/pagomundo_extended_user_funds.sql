-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: pagomundo
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `extended_user_funds`
--

DROP TABLE IF EXISTS `extended_user_funds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `extended_user_funds` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_created` datetime,
  `value` decimal(21,2) DEFAULT NULL,
  `balance_before` decimal(21,2) DEFAULT NULL,
  `balance_after` decimal(21,2) DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `extended_user_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_extended_user_funds_extended_user_id` (`extended_user_id`),
  KEY `fk_extended_user_funds_user_id` (`user_id`),
  CONSTRAINT `fk_extended_user_funds_extended_user_id` FOREIGN KEY (`extended_user_id`) REFERENCES `extended_user` (`id`),
  CONSTRAINT `fk_extended_user_funds_user_id` FOREIGN KEY (`user_id`) REFERENCES `jhi_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extended_user_funds`
--

LOCK TABLES `extended_user_funds` WRITE;
/*!40000 ALTER TABLE `extended_user_funds` DISABLE KEYS */;
INSERT INTO `extended_user_funds` VALUES (1,'2019-11-15 09:08:01',79654.00,95071.00,18343.00,'multi-byte Savings Account Dam',NULL,NULL),(2,'2019-11-15 15:43:23',5808.00,45924.00,86617.00,'Savings Account alarm',NULL,NULL),(3,'2019-11-15 17:37:42',5641.00,96990.00,72199.00,'Implementation Electronics Officer',NULL,NULL),(4,'2019-11-15 03:18:11',30424.00,61165.00,52475.00,'web-readiness',NULL,NULL),(5,'2019-11-15 11:07:59',52477.00,61185.00,39986.00,'Gorgeous Frozen Pants',NULL,NULL),(6,'2019-11-15 12:32:09',45606.00,9060.00,78517.00,'grey Cambridgeshire',NULL,NULL),(7,'2019-11-15 03:54:16',46676.00,4645.00,85994.00,'Yuan Renminbi Centers',NULL,NULL),(8,'2019-11-15 17:48:14',94888.00,94220.00,96563.00,'cross-platform access',NULL,NULL),(9,'2019-11-15 15:46:42',23089.00,68423.00,24102.00,'Bahamian Dollar Investment Account',NULL,NULL),(10,'2019-11-14 22:07:33',3438.00,17336.00,90932.00,'Digitized invoice',NULL,NULL),(11,NULL,1200.00,12500.00,13700.00,NULL,46,78),(12,'2019-11-15 19:28:48',1200.00,12500.00,13700.00,NULL,46,78),(13,'2019-11-15 19:43:45',1200.00,13700.00,14900.00,NULL,46,78),(14,'2019-11-15 20:00:10',100.00,14900.00,15000.00,'Founded money. Admin: albertoceli@googlemail.com',46,78),(15,'2019-11-15 20:01:03',-1100.00,15000.00,13900.00,'Founded money. Admin: albertoceli@googlemail.com',46,78),(18,'2019-11-25 14:52:41',-200.00,12900.00,12700.00,'Founded money. Admin: ag.ucaliri@gmail.com',95,106),(19,'2019-11-25 14:52:42',-200.00,12700.00,12500.00,'Founded money. Admin: ag.ucaliri@gmail.com',95,106),(20,'2019-11-25 14:52:43',-300.00,12500.00,12200.00,'Founded money. Admin: ag.ucaliri@gmail.com',95,106),(21,'2019-11-25 14:52:45',-300.00,12200.00,11900.00,'Founded money. Admin: ag.ucaliri@gmail.com',95,106),(22,'2019-11-25 14:56:22',200.00,11900.00,12100.00,'Founded money. Admin: admin',95,3),(23,'2019-11-25 14:56:22',300.00,12100.00,12400.00,'Founded money. Admin: admin',95,3),(24,'2019-11-25 15:08:27',200.00,12400.00,12600.00,'Founded money. Admin: admin',95,3),(25,'2019-11-25 15:11:15',200.00,12600.00,12800.00,'Founded money. Admin: admin',95,3);
/*!40000 ALTER TABLE `extended_user_funds` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-26  7:49:33
