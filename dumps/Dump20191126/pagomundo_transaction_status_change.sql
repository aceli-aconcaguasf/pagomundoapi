-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: pagomundo
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `transaction_status_change`
--

DROP TABLE IF EXISTS `transaction_status_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaction_status_change` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_created` datetime NOT NULL,
  `status` varchar(255) NOT NULL,
  `reason` longtext,
  `transaction_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_transaction_status_change_transaction_id` (`transaction_id`),
  KEY `fk_transaction_status_change_user_id` (`user_id`),
  CONSTRAINT `fk_transaction_status_change_transaction_id` FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`id`),
  CONSTRAINT `fk_transaction_status_change_user_id` FOREIGN KEY (`user_id`) REFERENCES `extended_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=229 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_status_change`
--

LOCK TABLES `transaction_status_change` WRITE;
/*!40000 ALTER TABLE `transaction_status_change` DISABLE KEYS */;
-- INSERT INTO `transaction_status_change` VALUES (221,'2019-11-25 14:52:40','CREATED',NULL,113,95),(222,'2019-11-25 14:52:41','CREATED',NULL,113,95),(223,'2019-11-25 14:52:42','CREATED',NULL,114,95),(224,'2019-11-25 14:52:44','CREATED',NULL,114,95),(225,'2019-11-25 14:56:22','IN_PROCESS',NULL,113,NULL),(226,'2019-11-25 14:56:22','IN_PROCESS',NULL,114,NULL),(227,'2019-11-25 15:08:26','REJECTED','The admin is the only one role who can reject a payment.',113,NULL),(228,'2019-11-25 15:11:14','REJECTED','The admin is the only one role who can reject a payment.',113,NULL);
/*!40000 ALTER TABLE `transaction_status_change` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-26  7:49:35
