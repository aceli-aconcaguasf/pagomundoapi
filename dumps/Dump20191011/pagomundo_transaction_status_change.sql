-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: 10.104.5.173    Database: pagomundo
-- ------------------------------------------------------
-- Server version	5.7.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `transaction_status_change`
--

DROP TABLE IF EXISTS `transaction_status_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaction_status_change` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_created` datetime NOT NULL,
  `status` varchar(255) NOT NULL,
  `reason` longtext,
  `transaction_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_transaction_status_change_transaction_id` (`transaction_id`),
  KEY `fk_transaction_status_change_user_id` (`user_id`),
  CONSTRAINT `fk_transaction_status_change_transaction_id` FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`id`),
  CONSTRAINT `fk_transaction_status_change_user_id` FOREIGN KEY (`user_id`) REFERENCES `extended_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_status_change`
--

LOCK TABLES `transaction_status_change` WRITE;
/*!40000 ALTER TABLE `transaction_status_change` DISABLE KEYS */;
INSERT INTO `transaction_status_change` VALUES (39,'2019-10-11 17:13:00','CREATED',NULL,33,1),(40,'2019-10-11 17:13:01','CREATED',NULL,34,1),(41,'2019-10-11 17:18:44','REJECTED','Lo cancelo porque quiero. Firma: el mejor admin!',33,3),(42,'2019-10-11 17:29:29','CREATED',NULL,35,1),(43,'2019-10-11 17:29:29','CREATED',NULL,36,1),(44,'2019-10-11 17:29:30','CREATED',NULL,37,1),(45,'2019-10-11 17:29:30','CREATED',NULL,38,1),(46,'2019-10-11 17:29:30','CREATED',NULL,39,1),(47,'2019-10-11 18:17:54','CANCELLED','Pruebita',39,1),(48,'2019-10-11 18:20:21','IN_PROCESS',NULL,34,3),(49,'2019-10-11 18:20:22','IN_PROCESS',NULL,35,3),(50,'2019-10-11 18:24:12','IN_PROCESS',NULL,37,3),(51,'2019-10-11 18:24:13','IN_PROCESS',NULL,36,3),(52,'2019-10-11 18:48:30','CREATED',NULL,40,1),(53,'2019-10-11 18:48:31','CREATED',NULL,41,1),(54,'2019-10-11 18:48:31','CREATED',NULL,42,1);
/*!40000 ALTER TABLE `transaction_status_change` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-11 16:16:41
