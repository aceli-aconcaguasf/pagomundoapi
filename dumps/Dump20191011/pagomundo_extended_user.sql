-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: 10.104.5.173    Database: pagomundo
-- ------------------------------------------------------
-- Server version	5.7.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `extended_user`
--

DROP TABLE IF EXISTS `extended_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `extended_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status` varchar(255) NOT NULL,
  `last_name_1` varchar(255) DEFAULT NULL,
  `last_name_2` varchar(255) DEFAULT NULL,
  `first_name_1` varchar(255) DEFAULT NULL,
  `first_name_2` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `id_number` varchar(255) DEFAULT NULL,
  `tax_id` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `marital_status` varchar(255) DEFAULT NULL,
  `residence_address` varchar(255) DEFAULT NULL,
  `postal_address` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `mobile_number` varchar(255) DEFAULT NULL,
  `birth_date` datetime,
  `date_created` datetime,
  `last_updated` datetime,
  `email` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `image_id_url` varchar(255) DEFAULT NULL,
  `image_address_url` varchar(255) DEFAULT NULL,
  `balance` decimal(21,2) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `residence_city_id` bigint(20) DEFAULT NULL,
  `postal_city_id` bigint(20) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `id_type_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_extended_user_user_id` (`user_id`),
  KEY `fk_extended_user_residence_city_id` (`residence_city_id`),
  KEY `fk_extended_user_postal_city_id` (`postal_city_id`),
  KEY `fk_extended_user_branch_id` (`branch_id`),
  KEY `fk_extended_user_id_type_id` (`id_type_id`),
  CONSTRAINT `fk_extended_user_branch_id` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`),
  CONSTRAINT `fk_extended_user_id_type_id` FOREIGN KEY (`id_type_id`) REFERENCES `id_type` (`id`),
  CONSTRAINT `fk_extended_user_postal_city_id` FOREIGN KEY (`postal_city_id`) REFERENCES `city` (`id`),
  CONSTRAINT `fk_extended_user_residence_city_id` FOREIGN KEY (`residence_city_id`) REFERENCES `city` (`id`),
  CONSTRAINT `fk_extended_user_user_id` FOREIGN KEY (`user_id`) REFERENCES `jhi_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extended_user`
--

LOCK TABLES `extended_user` WRITE;
/*!40000 ALTER TABLE `extended_user` DISABLE KEYS */;
INSERT INTO `extended_user` VALUES (1,'ACCEPTED','Koch','','Christina','','Christina Koch','','MALE','SINGLE','','','','','2019-08-16 11:46:00','2019-08-16 11:46:00','2019-08-16 11:46:00','system@localhost','','','',88983.02,1,1,1,1,1),(2,'ACCEPTED','Dell\'Agnolla','','Juan','Manuel','Juan Dell\'Agnolla','12345678','MALE','SINGLE','','','','','2019-08-16 11:46:00','2019-08-16 11:46:00','2019-08-16 11:46:00','anonymous@localhost','','','',NULL,2,1,1,1,1),(3,'ACCEPTED','Alex','','Pereira','','Alex Pereira','','MALE','SINGLE','','','','','2019-08-16 11:46:00','2019-08-16 11:46:00','2019-08-16 11:46:00','admin@localhost','','','',NULL,3,1,1,1,1),(4,'ACCEPTED','Rus','','Leticia','','Leticia Rus','456987','FEMALE','SINGLE','','','','','2019-08-16 11:46:00','2019-08-16 11:46:00','2019-08-16 11:46:00','letiRus@localhost','','','',NULL,4,1,1,1,1),(11,'ACCEPTED','Godoy','','Antonella','','Antonella Godoy','9875259','FEMALE','SINGLE','','','','',NULL,NULL,NULL,'','','','',NULL,NULL,1,1,1,1),(12,'ACCEPTED','Robert','','Amy','','Amy Robert','33625894','FEMALE','MARRIED','','','','',NULL,NULL,NULL,'','','','',NULL,NULL,1,1,1,1),(13,'ACCEPTED','Cercasi','','Juan','','Juan Cercasi','789456321','MALE','FREE_UNION','','','','',NULL,NULL,NULL,'','','','',NULL,NULL,1,1,1,1);
/*!40000 ALTER TABLE `extended_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-11 16:16:36
