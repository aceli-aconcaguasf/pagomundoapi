#!/bin/bash

echo "Starting PROD DB snapshot process..."

databaseName=pagomundoDB01
timestamp=$(date +%Y%m%d%H%M%S)
snapshotName=$databaseName-$timestamp
awsBackupCommand="/usr/local/bin/aws lightsail create-relational-database-snapshot --relational-database-name ${databaseName} --relational-database-snapshot-name ${snapshotName} --profile adminUser"

echo "Database name: $databaseName"
echo "Database snapshot name: $snapshotName"
echo "Executing command: $awsBackupCommand"

($awsBackupCommand)

echo "PROD DB snapshot process ended."


# crontab configuration
# 0 4 * * 4 /home/ubuntu/pagomundoapi/dumps/prod-db.snapshot.sh >> /home/ubuntu/dump.log 2>&1
# crontab configuration

