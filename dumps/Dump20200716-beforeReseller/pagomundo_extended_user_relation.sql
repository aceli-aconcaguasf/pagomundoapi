-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: ls-449eb857e6ae75cdb34254c1ef8b62ea8f7d95f1.cwxp9rzhaq1d.us-east-1.rds.amazonaws.com    Database: pagomundo
-- ------------------------------------------------------
-- Server version	5.7.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN = 0;

--
-- GTID state at the beginning of the backup
--

SET @@GLOBAL.GTID_PURGED = /*!80000 '+'*/ '';

--
-- Table structure for table `extended_user_relation`
--

DROP TABLE IF EXISTS `extended_user_relation`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `extended_user_relation`
(
    `id`               bigint(20) NOT NULL AUTO_INCREMENT,
    `extended_user_id` bigint(20) DEFAULT NULL,
    `user_related_id`  bigint(20) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_extended_user_relation_user_related_id` (`user_related_id`),
    KEY `fk_extended_user_relation_extended_user_id` (`extended_user_id`),
    CONSTRAINT `fk_extended_user_relation_extended_user_id` FOREIGN KEY (`extended_user_id`) REFERENCES `extended_user` (`id`),
    CONSTRAINT `fk_extended_user_relation_user_related_id` FOREIGN KEY (`user_related_id`) REFERENCES `jhi_user` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 11405
  DEFAULT CHARSET = utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extended_user_relation`
--

LOCK TABLES `extended_user_relation` WRITE;
/*!40000 ALTER TABLE `extended_user_relation`
    DISABLE KEYS */;
INSERT INTO `extended_user_relation`
VALUES (11341, 95, 8395),
       (11343, 3178, 106),
       (11344, 3180, 8398),
       (11346, 3181, 8397),
       (11347, 3180, 8400),
       (11349, 3180, 8401),
       (11350, 3180, 8403),
       (11352, 3184, 8397),
       (11353, 95, 8404),
       (11355, 3180, 8405),
       (11357, 3185, 8397),
       (11358, 3180, 8406),
       (11360, 3186, 8397),
       (11361, 95, 8407),
       (11363, 3187, 106),
       (11364, 3190, 8411),
       (11366, 3180, 8412),
       (11368, 3191, 8397),
       (11369, 95, 8413),
       (11371, 3192, 106),
       (11372, 3193, 8410),
       (11373, 3180, 8414),
       (11375, 3194, 8397),
       (11376, 3180, 8416),
       (11378, 3196, 8397),
       (11379, 3197, 8419),
       (11381, 3180, 8420),
       (11383, 3199, 8417),
       (11384, 3200, 8422),
       (11386, 3200, 8423),
       (11387, 3200, 8424),
       (11389, 3200, 8425),
       (11390, 95, 8426),
       (11392, 95, 8427),
       (11393, 95, 8428),
       (11394, 3201, 8421),
       (11395, 3200, 8429),
       (11397, 3202, 8421),
       (11398, 3198, 8430),
       (11400, 3198, 8431),
       (11402, 3203, 8418),
       (11403, 3200, 8432);
/*!40000 ALTER TABLE `extended_user_relation`
    ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2020-07-16 14:19:19
