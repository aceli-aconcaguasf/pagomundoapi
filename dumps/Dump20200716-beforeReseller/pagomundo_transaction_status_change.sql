-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: ls-449eb857e6ae75cdb34254c1ef8b62ea8f7d95f1.cwxp9rzhaq1d.us-east-1.rds.amazonaws.com    Database: pagomundo
-- ------------------------------------------------------
-- Server version	5.7.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN = 0;

--
-- GTID state at the beginning of the backup
--

SET @@GLOBAL.GTID_PURGED = /*!80000 '+'*/ '';

--
-- Table structure for table `transaction_status_change`
--

DROP TABLE IF EXISTS `transaction_status_change`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaction_status_change`
(
    `id`             bigint(20)   NOT NULL AUTO_INCREMENT,
    `date_created`   datetime     NOT NULL,
    `status`         varchar(255) NOT NULL,
    `reason`         longtext,
    `transaction_id` bigint(20) DEFAULT NULL,
    `user_id`        bigint(20) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_transaction_status_change_transaction_id` (`transaction_id`),
    KEY `fk_transaction_status_change_user_id` (`user_id`),
    CONSTRAINT `fk_transaction_status_change_transaction_id` FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`id`),
    CONSTRAINT `fk_transaction_status_change_user_id` FOREIGN KEY (`user_id`) REFERENCES `extended_user` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 74
  DEFAULT CHARSET = utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_status_change`
--

LOCK TABLES `transaction_status_change` WRITE;
/*!40000 ALTER TABLE `transaction_status_change`
    DISABLE KEYS */;
INSERT INTO `transaction_status_change`
VALUES (3, '2019-12-12 14:58:16', 'CREATED', NULL, 511, 95),
       (4, '2019-12-12 14:58:51', 'IN_PROCESS', NULL, 511, 94),
       (5, '2019-12-12 15:01:00', 'REJECTED', 'Reason for reject the payment', 511, 94),
       (6, '2020-01-14 16:48:48', 'CREATED', NULL, 512, 3180),
       (7, '2020-01-14 16:49:20', 'REJECTED', 'Not valid', 512, 3179),
       (8, '2020-02-18 14:38:50', 'CREATED', NULL, 513, 3180),
       (9, '2020-02-18 14:42:05', 'IN_PROCESS', NULL, 513, 3179),
       (10, '2020-02-18 14:43:26', 'ACCEPTED', 'Bank approved', 513, 3179),
       (11, '2020-02-20 17:31:05', 'CREATED', NULL, 514, 3180),
       (12, '2020-02-20 17:39:59', 'CREATED', NULL, 515, 3180),
       (13, '2020-02-20 17:40:44', 'IN_PROCESS', NULL, 515, 3179),
       (14, '2020-03-04 20:45:08', 'CREATED', NULL, 516, 3180),
       (15, '2020-03-04 20:45:09', 'CREATED', NULL, 517, 3180),
       (16, '2020-03-23 19:27:04', 'CREATED', NULL, 518, 3180),
       (17, '2020-03-30 14:47:55', 'CREATED', NULL, 519, 3180),
       (18, '2020-03-30 14:47:56', 'CREATED', NULL, 520, 3180),
       (19, '2020-04-01 18:03:37', 'REJECTED', 'Test', 520, 3179),
       (20, '2020-04-01 18:03:42', 'REJECTED', 'test', 519, 3179),
       (21, '2020-04-01 18:03:56', 'ACCEPTED', '', 518, 3179),
       (22, '2020-04-10 17:45:58', 'ACCEPTED', '', 515, 3179),
       (23, '2020-04-16 14:35:07', 'CREATED', NULL, 521, 3180),
       (24, '2020-04-16 14:35:07', 'CREATED', NULL, 522, 3180),
       (25, '2020-04-16 14:42:18', 'ACCEPTED', '', 517, 3179),
       (26, '2020-04-16 14:42:21', 'ACCEPTED', '', 514, 3179),
       (27, '2020-04-16 14:42:47', 'REJECTED', 'cuenta invalida', 522, 3179),
       (28, '2020-04-16 14:49:21', 'ACCEPTED', '', 516, 3179),
       (29, '2020-04-16 14:49:23', 'ACCEPTED', '', 521, 3179),
       (30, '2020-04-24 15:37:03', 'CREATED', NULL, 523, 3180),
       (31, '2020-04-24 15:37:03', 'CREATED', NULL, 524, 3180),
       (32, '2020-04-24 18:19:44', 'ACCEPTED', '\n', 523, 3179),
       (33, '2020-04-24 18:19:47', 'ACCEPTED', '', 524, 3179),
       (34, '2020-04-29 14:57:48', 'CREATED', NULL, 525, 3180),
       (35, '2020-04-29 19:32:45', 'REJECTED', 'Cuenta bloqueada', 525, 3179),
       (36, '2020-04-30 13:59:19', 'CREATED', NULL, 526, 3180),
       (37, '2020-05-05 15:17:03', 'ACCEPTED', '', 526, 3179),
       (38, '2020-05-06 13:54:52', 'CREATED', NULL, 527, 3190),
       (39, '2020-05-06 21:06:31', 'ACCEPTED', '', 527, 3179),
       (40, '2020-05-20 20:43:02', 'CREATED', NULL, 528, 3180),
       (41, '2020-05-20 21:57:46', 'ACCEPTED', '', 528, 3179),
       (42, '2020-06-03 16:20:23', 'CREATED', NULL, 529, 3197),
       (43, '2020-06-03 23:36:39', 'ACCEPTED', '', 529, 3179),
       (44, '2020-06-09 21:15:28', 'CREATED', NULL, 530, 3180),
       (45, '2020-06-09 21:15:28', 'CREATED', NULL, 531, 3180),
       (46, '2020-06-09 22:19:27', 'ACCEPTED', '', 531, 3179),
       (47, '2020-06-09 22:19:30', 'ACCEPTED', '', 530, 3179),
       (48, '2020-06-11 15:38:49', 'CREATED', NULL, 532, 3200),
       (49, '2020-06-11 20:03:43', 'ACCEPTED', '', 532, 3179),
       (50, '2020-06-12 18:08:10', 'CREATED', NULL, 533, 3200),
       (51, '2020-06-13 14:30:24', 'CREATED', NULL, 534, 3200),
       (52, '2020-06-15 00:50:04', 'ACCEPTED', '', 533, 3179),
       (53, '2020-06-16 19:07:55', 'ACCEPTED', '', 534, 3179),
       (54, '2020-06-19 17:14:32', 'CREATED', NULL, 535, 3200),
       (55, '2020-06-19 17:14:33', 'CREATED', NULL, 536, 3200),
       (56, '2020-06-23 15:01:38', 'ACCEPTED', '', 535, 3179),
       (57, '2020-06-23 15:01:42', 'ACCEPTED', '', 536, 3179),
       (58, '2020-06-26 23:44:15', 'CREATED', NULL, 537, 3200),
       (59, '2020-06-26 23:44:16', 'CREATED', NULL, 538, 3200),
       (60, '2020-06-30 15:24:12', 'ACCEPTED', '', 538, 3179),
       (61, '2020-06-30 15:24:16', 'ACCEPTED', '', 537, 3179),
       (62, '2020-07-03 19:11:08', 'CREATED', NULL, 539, 3200),
       (63, '2020-07-03 19:11:09', 'CREATED', NULL, 540, 3200),
       (64, '2020-07-06 19:10:37', 'ACCEPTED', '', 539, 3179),
       (65, '2020-07-06 19:10:41', 'ACCEPTED', '', 540, 3179),
       (66, '2020-07-08 20:32:39', 'CREATED', NULL, 541, 3180),
       (67, '2020-07-08 20:32:41', 'CREATED', NULL, 542, 3180),
       (68, '2020-07-09 17:08:35', 'ACCEPTED', '', 541, 3179),
       (69, '2020-07-09 17:08:38', 'ACCEPTED', '', 542, 3179),
       (70, '2020-07-10 22:14:12', 'CREATED', NULL, 543, 3200),
       (71, '2020-07-10 22:14:13', 'CREATED', NULL, 544, 3200),
       (72, '2020-07-13 16:41:09', 'ACCEPTED', '', 543, 3179),
       (73, '2020-07-13 16:41:12', 'ACCEPTED', '', 544, 3179);
/*!40000 ALTER TABLE `transaction_status_change`
    ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2020-07-16 14:18:12
