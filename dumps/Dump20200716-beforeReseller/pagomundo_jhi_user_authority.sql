-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: ls-449eb857e6ae75cdb34254c1ef8b62ea8f7d95f1.cwxp9rzhaq1d.us-east-1.rds.amazonaws.com    Database: pagomundo
-- ------------------------------------------------------
-- Server version	5.7.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN = 0;

--
-- GTID state at the beginning of the backup
--

SET @@GLOBAL.GTID_PURGED = /*!80000 '+'*/ '';

--
-- Table structure for table `jhi_user_authority`
--

DROP TABLE IF EXISTS `jhi_user_authority`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jhi_user_authority`
(
    `user_id`        bigint(20)  NOT NULL,
    `authority_name` varchar(50) NOT NULL,
    PRIMARY KEY (`user_id`, `authority_name`),
    KEY `fk_authority_name` (`authority_name`),
    CONSTRAINT `fk_authority_name` FOREIGN KEY (`authority_name`) REFERENCES `jhi_authority` (`name`),
    CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `jhi_user` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_user_authority`
--

LOCK TABLES `jhi_user_authority` WRITE;
/*!40000 ALTER TABLE `jhi_user_authority`
    DISABLE KEYS */;
INSERT INTO `jhi_user_authority`
VALUES (1, 'ROLE_ADMIN'),
       (3, 'ROLE_ADMIN'),
       (105, 'ROLE_ADMIN'),
       (8396, 'ROLE_ADMIN'),
       (8402, 'ROLE_ADMIN'),
       (8408, 'ROLE_ADMIN'),
       (8409, 'ROLE_ADMIN'),
       (8415, 'ROLE_ADMIN'),
       (106, 'ROLE_MERCHANT'),
       (8397, 'ROLE_MERCHANT'),
       (8399, 'ROLE_MERCHANT'),
       (8410, 'ROLE_MERCHANT'),
       (8417, 'ROLE_MERCHANT'),
       (8418, 'ROLE_MERCHANT'),
       (8421, 'ROLE_MERCHANT'),
       (8395, 'ROLE_PAYEE'),
       (8398, 'ROLE_PAYEE'),
       (8400, 'ROLE_PAYEE'),
       (8401, 'ROLE_PAYEE'),
       (8403, 'ROLE_PAYEE'),
       (8404, 'ROLE_PAYEE'),
       (8405, 'ROLE_PAYEE'),
       (8406, 'ROLE_PAYEE'),
       (8407, 'ROLE_PAYEE'),
       (8411, 'ROLE_PAYEE'),
       (8412, 'ROLE_PAYEE'),
       (8413, 'ROLE_PAYEE'),
       (8414, 'ROLE_PAYEE'),
       (8416, 'ROLE_PAYEE'),
       (8419, 'ROLE_PAYEE'),
       (8420, 'ROLE_PAYEE'),
       (8422, 'ROLE_PAYEE'),
       (8423, 'ROLE_PAYEE'),
       (8424, 'ROLE_PAYEE'),
       (8425, 'ROLE_PAYEE'),
       (8426, 'ROLE_PAYEE'),
       (8427, 'ROLE_PAYEE'),
       (8428, 'ROLE_PAYEE'),
       (8429, 'ROLE_PAYEE'),
       (8430, 'ROLE_PAYEE'),
       (8431, 'ROLE_PAYEE'),
       (8432, 'ROLE_PAYEE'),
       (5, 'ROLE_SUPER_ADMIN');
/*!40000 ALTER TABLE `jhi_user_authority`
    ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2020-07-16 14:19:26
