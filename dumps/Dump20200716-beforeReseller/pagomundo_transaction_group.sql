-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: ls-449eb857e6ae75cdb34254c1ef8b62ea8f7d95f1.cwxp9rzhaq1d.us-east-1.rds.amazonaws.com    Database: pagomundo
-- ------------------------------------------------------
-- Server version	5.7.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN = 0;

--
-- GTID state at the beginning of the backup
--

SET @@GLOBAL.GTID_PURGED = /*!80000 '+'*/ '';

--
-- Table structure for table `transaction_group`
--

DROP TABLE IF EXISTS `transaction_group`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaction_group`
(
    `id`                  bigint(20) NOT NULL AUTO_INCREMENT,
    `date_created`        datetime     DEFAULT NULL,
    `status`              varchar(255) DEFAULT NULL,
    `bank_account_id`     bigint(20)   DEFAULT NULL,
    `file_name`           varchar(255) DEFAULT NULL,
    `exchange_rate`       float        DEFAULT NULL,
    `admin_id`            bigint(20)   DEFAULT NULL,
    `file_name_from_bank` varchar(255) DEFAULT NULL,
    `direct_payment`      tinyint(1)   DEFAULT '0',
    PRIMARY KEY (`id`),
    KEY `fk_transaction_group_bank_account_id_index_5` (`bank_account_id`),
    KEY `fk_transaction_group_admin_id_index_5` (`admin_id`),
    CONSTRAINT `fk_transaction_group_bank_account_id` FOREIGN KEY (`bank_account_id`) REFERENCES `bank_account` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 23
  DEFAULT CHARSET = utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_group`
--

LOCK TABLES `transaction_group` WRITE;
/*!40000 ALTER TABLE `transaction_group`
    DISABLE KEYS */;
INSERT INTO `transaction_group`
VALUES (2, '2019-12-12 14:58:51', 'ACCEPTED', 1,
        'fileBucket/transaction/exportedFiles/2019_12_12_14_58_51_00-22365498-8852263-Pichincha-Colombia.xlsx', 100, 94,
        NULL, 0),
       (3, '2020-02-18 14:42:05', 'ACCEPTED', 2,
        'fileBucket/transaction/exportedFiles/2020_02_18_14_42_05_00-89456322-3622588-Itau-Colombia.xlsx', 3409, 3179,
        NULL, 0),
       (4, '2020-02-20 17:40:44', 'ACCEPTED', 2,
        'fileBucket/transaction/exportedFiles/2020_02_20_17_40_44_00-89456322-3622588-Itau-Colombia-DirectPayment.xlsx',
        3400, 3179, NULL, 1),
       (5, '2020-03-23 19:29:29', 'ACCEPTED', 2,
        'fileBucket/transaction/exportedFiles/2020_03_23_19_29_29_00-89456322-3622588-Itau-Colombia-DirectPayment.xlsx',
        4285, 3179, NULL, 1),
       (6, '2020-04-16 14:39:29', 'ACCEPTED', 2,
        'fileBucket/transaction/exportedFiles/2020_04_16_14_39_29_00-007468655-Itau-Colombia.xlsx', 3928, 3179, NULL,
        0),
       (7, '2020-04-16 14:48:56', 'ACCEPTED', 2,
        'fileBucket/transaction/exportedFiles/2020_04_16_14_48_56_00-007468655-Itau-Colombia-DirectPayment.txt', 3928,
        3179, NULL, 1),
       (8, '2020-04-24 15:44:05', 'ACCEPTED', 2,
        'fileBucket/transaction/exportedFiles/2020_04_24_15_44_05_00-007468655-Itau-Colombia.xlsx', 3957, 3179, NULL,
        0),
       (9, '2020-04-29 15:22:11', 'ACCEPTED', 2,
        'fileBucket/transaction/exportedFiles/2020_04_29_15_22_11_00-007468655-Itau-Colombia-DirectPayment.txt', 3990,
        3179, NULL, 1),
       (10, '2020-04-30 14:01:14', 'ACCEPTED', 2,
        'fileBucket/transaction/exportedFiles/2020_04_30_14_01_14_00-007468655-Itau-Colombia-DirectPayment.txt', 3920,
        3179, NULL, 1),
       (11, '2020-05-06 13:59:27', 'ACCEPTED', 2,
        'fileBucket/transaction/exportedFiles/2020_05_06_13_59_27_00-007468655-Itau-Colombia-DirectPayment.txt', 3943,
        3179, NULL, 1),
       (12, '2020-05-20 20:45:05', 'ACCEPTED', 2,
        'fileBucket/transaction/exportedFiles/2020_05_20_20_45_05_00-007468655-Itau-Colombia-DirectPayment.txt', 3813,
        3179, NULL, 1),
       (13, '2020-06-03 16:34:18', 'ACCEPTED', 2,
        'fileBucket/transaction/exportedFiles/2020_06_03_16_34_18_00-007468655-Itau-Colombia-DirectPayment.txt', 3990,
        3179, NULL, 1),
       (14, '2020-06-09 21:17:08', 'ACCEPTED', 2,
        'fileBucket/transaction/exportedFiles/2020_06_09_21_17_08_00-007468655-Itau-Colombia.xlsx', 3650, 3179, NULL,
        0),
       (15, '2020-06-11 15:51:53', 'ACCEPTED', 2,
        'fileBucket/transaction/exportedFiles/2020_06_11_15_51_53_00-007468655-Itau-Colombia-DirectPayment.txt', 3754,
        3179, NULL, 1),
       (16, '2020-06-12 18:39:38', 'ACCEPTED', 2,
        'fileBucket/transaction/exportedFiles/2020_06_12_18_39_38_00-007468655-Itau-Colombia-DirectPayment.txt', 3750,
        3179, NULL, 1),
       (17, '2020-06-15 18:22:30', 'ACCEPTED', 2,
        'fileBucket/transaction/exportedFiles/2020_06_15_18_22_30_00-007468655-Itau-Colombia-DirectPayment.txt', 3751,
        3179, NULL, 1),
       (18, '2020-06-19 18:38:42', 'ACCEPTED', 2,
        'fileBucket/transaction/exportedFiles/2020_06_19_18_38_42_00-007468655-Itau-Colombia-DirectPayment.txt', 3743,
        3179, NULL, 1),
       (19, '2020-06-27 02:29:02', 'ACCEPTED', 2,
        'fileBucket/transaction/exportedFiles/2020_06_27_02_29_02_00-007468655-Itau-Colombia-DirectPayment.txt', 3740,
        3179, NULL, 1),
       (20, '2020-07-06 14:27:12', 'ACCEPTED', 2,
        'fileBucket/transaction/exportedFiles/2020_07_06_14_27_12_00-007468655-Itau-Colombia-DirectPayment.txt', 3620,
        3179, NULL, 1),
       (21, '2020-07-08 20:33:46', 'ACCEPTED', 2,
        'fileBucket/transaction/exportedFiles/2020_07_08_20_33_46_00-007468655-Itau-Colombia.xlsx', 3600, 3179, NULL,
        0),
       (22, '2020-07-13 00:14:28', 'ACCEPTED', 2,
        'fileBucket/transaction/exportedFiles/2020_07_13_00_14_28_00-007468655-Itau-Colombia-DirectPayment.txt', 3600,
        3179, NULL, 1);
/*!40000 ALTER TABLE `transaction_group`
    ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2020-07-16 14:16:48
