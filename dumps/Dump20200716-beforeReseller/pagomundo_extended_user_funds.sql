-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: ls-449eb857e6ae75cdb34254c1ef8b62ea8f7d95f1.cwxp9rzhaq1d.us-east-1.rds.amazonaws.com    Database: pagomundo
-- ------------------------------------------------------
-- Server version	5.7.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN = 0;

--
-- GTID state at the beginning of the backup
--

SET @@GLOBAL.GTID_PURGED = /*!80000 '+'*/ '';

--
-- Table structure for table `extended_user_funds`
--

DROP TABLE IF EXISTS `extended_user_funds`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `extended_user_funds`
(
    `id`               bigint(20) NOT NULL AUTO_INCREMENT,
    `date_created`     datetime       DEFAULT NULL,
    `value`            decimal(21, 2) DEFAULT NULL,
    `balance_before`   decimal(21, 2) DEFAULT NULL,
    `balance_after`    decimal(21, 2) DEFAULT NULL,
    `reason`           varchar(255)   DEFAULT NULL,
    `extended_user_id` bigint(20)     DEFAULT NULL,
    `user_id`          bigint(20)     DEFAULT NULL,
    `status`           varchar(255)   DEFAULT NULL,
    `category`         varchar(255)   DEFAULT NULL,
    `last_updated`     datetime       DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_extended_user_funds_extended_user_id` (`extended_user_id`),
    KEY `fk_extended_user_funds_user_id` (`user_id`),
    CONSTRAINT `fk_extended_user_funds_extended_user_id` FOREIGN KEY (`extended_user_id`) REFERENCES `extended_user` (`id`),
    CONSTRAINT `fk_extended_user_funds_user_id` FOREIGN KEY (`user_id`) REFERENCES `jhi_user` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 872
  DEFAULT CHARSET = utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extended_user_funds`
--

LOCK TABLES `extended_user_funds` WRITE;
/*!40000 ALTER TABLE `extended_user_funds`
    DISABLE KEYS */;
INSERT INTO `extended_user_funds`
VALUES (814, '2019-12-12 14:57:44', 100.00, 0.00, 100.00, 'Founded money. Admin: a.gucaliri@gmail.com', 95, 105, NULL,
        NULL, NULL),
       (815, '2019-12-12 14:58:17', -100.00, 100.00, 0.00, 'CREATED payment 511', 95, 106, NULL, NULL, NULL),
       (816, '2019-12-12 15:01:00', 100.00, 0.00, 100.00, 'REJECTED payment 511', 95, 105, NULL, NULL, NULL),
       (817, '2020-01-14 16:46:26', 15000.00, 0.00, 15000.00, 'Founded money. Admin: alex@pmi-americas.com', 3180, 8396,
        NULL, NULL, NULL),
       (818, '2020-01-14 16:48:48', -1000.00, 15000.00, 14000.00, 'CREATED payment 512', 3180, 8397, NULL, NULL, NULL),
       (819, '2020-01-14 16:49:20', 1000.00, 14000.00, 15000.00, 'REJECTED payment 512', 3180, 8396, NULL, NULL, NULL),
       (820, '2020-01-14 16:51:10', -13500.00, 15000.00, 1500.00, 'Founded money. Admin: alex@pmi-americas.com', 3180,
        8396, NULL, NULL, NULL),
       (821, '2020-01-14 20:57:55', 4500.00, 1500.00, 6000.00, 'Founded money. Admin: alex@pmi-americas.com', 3180,
        8396, NULL, NULL, NULL),
       (822, '2020-02-18 14:38:50', -4324.00, 6000.00, 1676.00, 'CREATED payment 513', 3180, 8397, NULL, NULL, NULL),
       (823, '2020-02-20 17:31:06', -878.00, 1676.00, 798.00, 'CREATED payment 514', 3180, 8397, NULL, NULL, NULL),
       (824, '2020-02-20 17:40:00', -110.00, 798.00, 688.00, 'CREATED payment 515', 3180, 8397, NULL, NULL, NULL),
       (825, '2020-03-04 20:45:09', -120.00, 688.00, 568.00, 'CREATED payment 516', 3180, 8397, NULL, NULL, NULL),
       (826, '2020-03-04 20:45:09', -135.00, 568.00, 433.00, 'CREATED payment 517', 3180, 8397, NULL, NULL, NULL),
       (827, '2020-03-04 20:49:13', 1250.00, 433.00, 1683.00, 'Founded money. Admin: alex@pmi-americas.com', 3180, 8396,
        NULL, NULL, NULL),
       (828, '2020-03-23 19:27:04', -130.00, 1683.00, 1553.00, 'CREATED payment 518', 3180, 8397, NULL, NULL, NULL),
       (829, '2020-03-30 14:40:06', 100000.00, 1553.00, 101553.00,
        'Founded money. Admin: benjamin.bayr@processingpartners.com', 3180, 8408, NULL, NULL, NULL),
       (830, '2020-03-30 14:47:56', -540.00, 101553.00, 101013.00, 'CREATED payment 519', 3180, 8397, NULL, NULL, NULL),
       (831, '2020-03-30 14:47:56', -348.00, 101013.00, 100665.00, 'CREATED payment 520', 3180, 8397, NULL, NULL, NULL),
       (832, '2020-04-01 18:03:38', 348.00, 100665.00, 101013.00, 'REJECTED payment 520', 3180, 8396, NULL, NULL, NULL),
       (833, '2020-04-01 18:03:42', 540.00, 101013.00, 101553.00, 'REJECTED payment 519', 3180, 8396, NULL, NULL, NULL),
       (834, '2020-04-14 13:11:47', -100000.00, 101553.00, 1553.00, 'Founded money. Admin: alex@pmi-americas.com', 3180,
        8396, NULL, NULL, NULL),
       (835, '2020-04-14 19:58:25', 100.00, NULL, NULL, 'cancelado', 95, NULL, 'CANCELLED', 'REQUESTED',
        '2020-04-14 19:58:39'),
       (836, '2020-04-16 13:35:25', 2300.00, 1553.00, 3853.00, '', 3180, 8396, 'ACCEPTED', 'REQUESTED',
        '2020-04-16 13:38:35'),
       (837, '2020-04-16 14:35:07', -240.00, 3853.00, 3613.00, 'CREATED payment 521', 3180, 8397, 'ACCEPTED',
        'AUTOMATIC', '2020-04-16 14:35:07'),
       (838, '2020-04-16 14:35:07', -345.00, 3613.00, 3268.00, 'CREATED payment 522', 3180, 8397, 'ACCEPTED',
        'AUTOMATIC', '2020-04-16 14:35:07'),
       (839, '2020-04-16 14:42:47', 345.00, 3268.00, 3613.00, 'REJECTED payment 522', 3180, 8396, 'ACCEPTED',
        'AUTOMATIC', '2020-04-16 14:42:47'),
       (840, '2020-04-18 00:47:04', 16387.00, 3613.00, 20000.00, NULL, 3180, 8396, 'ACCEPTED', 'REQUESTED',
        '2020-04-18 00:47:04'),
       (841, '2020-04-24 15:37:03', -1850.00, 20000.00, 18150.00, 'CREATED payment 523', 3180, 8397, 'ACCEPTED',
        'AUTOMATIC', '2020-04-24 15:37:03'),
       (842, '2020-04-24 15:37:03', -1850.00, 18150.00, 16300.00, 'CREATED payment 524', 3180, 8397, 'ACCEPTED',
        'AUTOMATIC', '2020-04-24 15:37:03'),
       (843, '2020-04-29 14:57:48', -4400.00, 16300.00, 11900.00, 'CREATED payment 525', 3180, 8397, 'ACCEPTED',
        'AUTOMATIC', '2020-04-29 14:57:48'),
       (844, '2020-04-29 14:58:03', 4400.00, 11900.00, 16300.00, '', 3180, 8396, 'ACCEPTED', 'REQUESTED',
        '2020-04-29 14:59:09'),
       (845, '2020-04-29 19:32:45', 4400.00, 16300.00, 20700.00, 'REJECTED payment 525', 3180, 8396, 'ACCEPTED',
        'AUTOMATIC', '2020-04-29 19:32:45'),
       (846, '2020-04-30 13:59:19', -4400.00, 20700.00, 16300.00, 'CREATED payment 526', 3180, 8397, 'ACCEPTED',
        'AUTOMATIC', '2020-04-30 13:59:19'),
       (847, '2020-05-06 13:53:47', 500.00, 0.00, 500.00, NULL, 3190, 8396, 'ACCEPTED', 'REQUESTED',
        '2020-05-06 13:53:47'),
       (848, '2020-05-06 13:54:52', -500.00, 500.00, 0.00, 'CREATED payment 527', 3190, 8410, 'ACCEPTED', 'AUTOMATIC',
        '2020-05-06 13:54:52'),
       (849, '2020-05-20 20:43:02', -300.00, 16300.00, 16000.00, 'CREATED payment 528', 3180, 8397, 'ACCEPTED',
        'AUTOMATIC', '2020-05-20 20:43:02'),
       (850, '2020-05-29 19:44:00', 6357.00, 0.00, 6357.00, NULL, 3197, 8396, 'ACCEPTED', 'REQUESTED',
        '2020-05-29 19:44:00'),
       (851, '2020-06-02 13:22:11', 6357.00, 0.00, 6357.00, NULL, 3198, 8396, 'ACCEPTED', 'REQUESTED',
        '2020-06-02 13:22:11'),
       (852, '2020-06-03 16:20:24', -5477.07, 6357.00, 879.93, 'CREATED payment 529', 3197, 8417, 'ACCEPTED',
        'AUTOMATIC', '2020-06-03 16:20:24'),
       (853, '2020-06-04 19:46:34', 10000.00, 0.00, 10000.00, NULL, 3200, 8396, 'ACCEPTED', 'REQUESTED',
        '2020-06-04 19:46:34'),
       (854, '2020-06-04 20:02:31', -5477.07, 6357.00, 879.93, NULL, 3198, 8396, 'ACCEPTED', 'REQUESTED',
        '2020-06-04 20:02:31'),
       (855, '2020-06-09 21:15:28', -2100.00, 16000.00, 13900.00, 'CREATED payment 530', 3180, 8397, 'ACCEPTED',
        'AUTOMATIC', '2020-06-09 21:15:28'),
       (856, '2020-06-09 21:15:29', -2100.00, 13900.00, 11800.00, 'CREATED payment 531', 3180, 8397, 'ACCEPTED',
        'AUTOMATIC', '2020-06-09 21:15:29'),
       (857, '2020-06-11 15:38:49', -100.00, 10000.00, 9900.00, 'CREATED payment 532', 3200, 8421, 'ACCEPTED',
        'AUTOMATIC', '2020-06-11 15:38:49'),
       (858, '2020-06-12 18:08:10', -400.13, 9900.00, 9499.87, 'CREATED payment 533', 3200, 8421, 'ACCEPTED',
        'AUTOMATIC', '2020-06-12 18:08:10'),
       (859, '2020-06-13 14:30:24', -909.21, 9499.87, 8590.66, 'CREATED payment 534', 3200, 8421, 'ACCEPTED',
        'AUTOMATIC', '2020-06-13 14:30:24'),
       (860, '2020-06-16 21:20:07', -128.00, 8590.66, 8462.66, NULL, 3200, 8396, 'ACCEPTED', 'REQUESTED',
        '2020-06-16 21:20:07'),
       (861, '2020-06-19 17:14:33', -1113.18, 8462.66, 7349.48, 'CREATED payment 535', 3200, 8421, 'ACCEPTED',
        'AUTOMATIC', '2020-06-19 17:14:33'),
       (862, '2020-06-19 17:14:33', -718.37, 7349.48, 6631.11, 'CREATED payment 536', 3200, 8421, 'ACCEPTED',
        'AUTOMATIC', '2020-06-19 17:14:33'),
       (863, '2020-06-26 23:44:16', -1029.39, 6631.11, 5601.72, 'CREATED payment 537', 3200, 8421, 'ACCEPTED',
        'AUTOMATIC', '2020-06-26 23:44:16'),
       (864, '2020-06-26 23:44:16', -430.38, 5601.72, 5171.34, 'CREATED payment 538', 3200, 8421, 'ACCEPTED',
        'AUTOMATIC', '2020-06-26 23:44:16'),
       (865, '2020-07-03 19:11:09', -1267.28, 5171.34, 3904.06, 'CREATED payment 539', 3200, 8421, 'ACCEPTED',
        'AUTOMATIC', '2020-07-03 19:11:09'),
       (866, '2020-07-03 19:11:09', -701.47, 3904.06, 3202.59, 'CREATED payment 540', 3200, 8421, 'ACCEPTED',
        'AUTOMATIC', '2020-07-03 19:11:09'),
       (867, '2020-07-08 20:32:40', -2425.00, 11800.00, 9375.00, 'CREATED payment 541', 3180, 8397, 'ACCEPTED',
        'AUTOMATIC', '2020-07-08 20:32:40'),
       (868, '2020-07-08 20:32:41', -2425.00, 9375.00, 6950.00, 'CREATED payment 542', 3180, 8397, 'ACCEPTED',
        'AUTOMATIC', '2020-07-08 20:32:41'),
       (869, '2020-07-10 22:14:13', -757.32, 3202.59, 2445.27, 'CREATED payment 543', 3200, 8421, 'ACCEPTED',
        'AUTOMATIC', '2020-07-10 22:14:13'),
       (870, '2020-07-10 22:14:13', -2324.24, 2445.27, 121.03, 'CREATED payment 544', 3200, 8421, 'ACCEPTED',
        'AUTOMATIC', '2020-07-10 22:14:13'),
       (871, '2020-07-15 15:50:46', 10000.00, 121.03, 10121.03, NULL, 3200, 8396, 'ACCEPTED', 'REQUESTED',
        '2020-07-15 15:50:46');
/*!40000 ALTER TABLE `extended_user_funds`
    ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2020-07-16 14:16:35
