-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: ls-449eb857e6ae75cdb34254c1ef8b62ea8f7d95f1.cwxp9rzhaq1d.us-east-1.rds.amazonaws.com    Database: pagomundo
-- ------------------------------------------------------
-- Server version	5.7.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN = 0;

--
-- GTID state at the beginning of the backup
--

SET @@GLOBAL.GTID_PURGED = /*!80000 '+'*/ '';

--
-- Table structure for table `jhi_persistent_audit_event`
--

DROP TABLE IF EXISTS `jhi_persistent_audit_event`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jhi_persistent_audit_event`
(
    `event_id`   bigint(20)  NOT NULL AUTO_INCREMENT,
    `principal`  varchar(50) NOT NULL,
    `event_date` timestamp   NULL DEFAULT NULL,
    `event_type` varchar(255)     DEFAULT NULL,
    PRIMARY KEY (`event_id`),
    KEY `idx_persistent_audit_event` (`principal`, `event_date`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 4319
  DEFAULT CHARSET = utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_persistent_audit_event`
--

LOCK TABLES `jhi_persistent_audit_event` WRITE;
/*!40000 ALTER TABLE `jhi_persistent_audit_event`
    DISABLE KEYS */;
INSERT INTO `jhi_persistent_audit_event`
VALUES (4113, 'alex@pmi-americas.com', '2020-06-16 13:58:55', 'AUTHENTICATION_SUCCESS'),
       (4114, 'alex@pmi-americas.com', '2020-06-16 15:18:13', 'AUTHENTICATION_SUCCESS'),
       (4115, 'alex@pmi-americas.com', '2020-06-16 19:07:50', 'AUTHENTICATION_SUCCESS'),
       (4116, 'alex@pmi-americas.com', '2020-06-16 21:18:43', 'AUTHENTICATION_SUCCESS'),
       (4117, 'management@celeb.tv', '2020-06-16 23:32:22', 'AUTHENTICATION_SUCCESS'),
       (4118, 'alex@pmi-americas.com', '2020-06-17 14:47:19', 'AUTHENTICATION_SUCCESS'),
       (4119, 'alex@pmi-americas.com', '2020-06-17 17:50:05', 'AUTHENTICATION_SUCCESS'),
       (4120, 'alex@pmi-americas.com', '2020-06-18 14:07:17', 'AUTHENTICATION_SUCCESS'),
       (4121, 'alex@pmi-americas.com', '2020-06-18 18:31:23', 'AUTHENTICATION_SUCCESS'),
       (4122, 'alex@pmi-americas.com', '2020-06-18 19:18:45', 'AUTHENTICATION_SUCCESS'),
       (4123, 'a.gucaliri@gmail.com', '2020-06-19 11:39:24', 'AUTHENTICATION_SUCCESS'),
       (4124, 'alex@pmi-americas.com', '2020-06-19 18:36:20', 'AUTHENTICATION_SUCCESS'),
       (4125, 'andresrg2907@gmail.com', '2020-06-19 20:58:32', 'AUTHENTICATION_SUCCESS'),
       (4126, 'alex@pmi-americas.com', '2020-06-19 23:26:09', 'AUTHENTICATION_SUCCESS'),
       (4127, 'alex@pmi-americas.com', '2020-06-21 20:30:38', 'AUTHENTICATION_SUCCESS'),
       (4128, 'a.gucaliri@gmail.com', '2020-06-22 14:40:55', 'AUTHENTICATION_SUCCESS'),
       (4129, 'alex@pmi-americas.com', '2020-06-22 18:37:58', 'AUTHENTICATION_SUCCESS'),
       (4130, 'alex@pmi-americas.com', '2020-06-22 20:41:04', 'AUTHENTICATION_SUCCESS'),
       (4131, 'alex@pmi-americas.com', '2020-06-23 13:35:00', 'AUTHENTICATION_SUCCESS'),
       (4132, 'alex@pmi-americas.com', '2020-06-23 15:01:34', 'AUTHENTICATION_SUCCESS'),
       (4133, 'alex@pmi-americas.com', '2020-06-23 18:04:58', 'AUTHENTICATION_SUCCESS'),
       (4134, 'alex@pmi-americas.com', '2020-06-23 20:50:38', 'AUTHENTICATION_SUCCESS'),
       (4135, 'a.gucaliri@gmail.com', '2020-06-24 15:22:04', 'AUTHENTICATION_SUCCESS'),
       (4136, 'alex@pmi-americas.com', '2020-06-24 16:00:03', 'AUTHENTICATION_SUCCESS'),
       (4137, 'alex@pmi-americas.com', '2020-06-25 05:08:32', 'AUTHENTICATION_SUCCESS'),
       (4138, 'alex@pmi-americas.com', '2020-06-25 13:25:16', 'AUTHENTICATION_SUCCESS'),
       (4139, 'alex@pmi-americas.com', '2020-06-25 15:59:36', 'AUTHENTICATION_SUCCESS'),
       (4140, 'alex@pmi-americas.com', '2020-06-25 21:10:11', 'AUTHENTICATION_SUCCESS'),
       (4141, 'management@celeb.tv', '2020-06-26 16:32:50', 'AUTHENTICATION_SUCCESS'),
       (4142, 'management@celeb.tv', '2020-06-26 16:33:48', 'AUTHENTICATION_SUCCESS'),
       (4143, 'management@celeb.tv', '2020-06-26 16:33:51', 'AUTHENTICATION_SUCCESS'),
       (4144, 'management@celeb.tv', '2020-06-26 16:34:19', 'AUTHENTICATION_SUCCESS'),
       (4145, 'management@celeb.tv', '2020-06-26 16:34:27', 'AUTHENTICATION_SUCCESS'),
       (4146, 'management@celeb.tv', '2020-06-26 16:34:38', 'AUTHENTICATION_SUCCESS'),
       (4147, 'alex@pmi-americas.com', '2020-06-26 16:52:58', 'AUTHENTICATION_SUCCESS'),
       (4148, 'alex@danalex.org', '2020-06-26 16:53:09', 'AUTHENTICATION_SUCCESS'),
       (4149, 'alex@danalex.org', '2020-06-26 16:54:00', 'AUTHENTICATION_SUCCESS'),
       (4150, 'alex@pmi-americas.com', '2020-06-26 16:54:13', 'AUTHENTICATION_SUCCESS'),
       (4151, 'alex@pmi-americas.com', '2020-06-26 16:56:03', 'AUTHENTICATION_SUCCESS'),
       (4152, 'alex@pmi-americas.com', '2020-06-26 17:26:54', 'AUTHENTICATION_SUCCESS'),
       (4153, 'superadmin', '2020-06-26 17:36:16', 'AUTHENTICATION_SUCCESS'),
       (4154, 'a.gucaliri@gmail.com', '2020-06-26 17:37:00', 'AUTHENTICATION_SUCCESS'),
       (4155, 'a.gucaliri@gmail.com', '2020-06-26 17:40:57', 'AUTHENTICATION_SUCCESS'),
       (4156, 'superadmin', '2020-06-26 17:41:54', 'AUTHENTICATION_SUCCESS'),
       (4157, 'ag.ucaliri@gmail.com', '2020-06-26 17:44:11', 'AUTHENTICATION_SUCCESS'),
       (4158, 'superadmin', '2020-06-26 18:05:17', 'AUTHENTICATION_SUCCESS'),
       (4159, 'superadmin', '2020-06-26 18:06:08', 'AUTHENTICATION_SUCCESS'),
       (4160, 'ag.ucaliri@gmail.com', '2020-06-26 18:09:35', 'AUTHENTICATION_SUCCESS'),
       (4161, 'mai.aracercasi+pp@gmail.com', '2020-06-26 18:10:14', 'AUTHENTICATION_SUCCESS'),
       (4162, 'ag.ucaliri@gmail.com', '2020-06-26 18:11:15', 'AUTHENTICATION_SUCCESS'),
       (4163, 'mai.aracercasi+pp@gmail.com', '2020-06-26 18:11:54', 'AUTHENTICATION_SUCCESS'),
       (4164, 'alex@pmi-americas.com', '2020-06-26 18:16:11', 'AUTHENTICATION_SUCCESS'),
       (4165, 'management@celeb.tv', '2020-06-26 19:20:02', 'AUTHENTICATION_SUCCESS'),
       (4166, 'alex@pmi-americas.com', '2020-06-26 21:18:00', 'AUTHENTICATION_SUCCESS'),
       (4167, 'alex@pmi-americas.com', '2020-06-26 21:19:54', 'AUTHENTICATION_SUCCESS'),
       (4168, 'alex@danalex.org', '2020-06-26 21:21:06', 'AUTHENTICATION_SUCCESS'),
       (4169, 'alex@pmi-americas.com', '2020-06-26 21:25:34', 'AUTHENTICATION_SUCCESS'),
       (4170, 'alex@pmi-americas.com', '2020-06-26 21:33:08', 'AUTHENTICATION_SUCCESS'),
       (4171, 'alex@pmi-americas.com', '2020-06-26 21:37:06', 'AUTHENTICATION_SUCCESS'),
       (4172, 'alex@pmi-americas.com', '2020-06-26 21:40:55', 'AUTHENTICATION_SUCCESS'),
       (4173, 'a.gucaliri@gmail.com', '2020-06-26 21:41:08', 'AUTHENTICATION_SUCCESS'),
       (4174, 'a.gucaliri@gmail.com', '2020-06-26 21:49:57', 'AUTHENTICATION_SUCCESS'),
       (4175, 'a.gucaliri@gmail.com', '2020-06-26 21:51:03', 'AUTHENTICATION_SUCCESS'),
       (4176, 'alex@pmi-americas.com', '2020-06-26 21:55:25', 'AUTHENTICATION_SUCCESS'),
       (4177, 'management@celeb.tv', '2020-06-26 23:40:01', 'AUTHENTICATION_SUCCESS'),
       (4178, 'alex@pmi-americas.com', '2020-06-27 04:06:50', 'AUTHENTICATION_SUCCESS'),
       (4179, 'alex@pmi-americas.com', '2020-06-27 05:44:31', 'AUTHENTICATION_SUCCESS'),
       (4180, 'superadmin', '2020-06-27 13:30:43', 'AUTHENTICATION_SUCCESS'),
       (4181, 'superadmin', '2020-06-27 22:47:36', 'AUTHENTICATION_SUCCESS'),
       (4182, 'jose.lopera@colegiatura.edu.co', '2020-06-27 23:20:37', 'AUTHENTICATION_SUCCESS'),
       (4183, 'jose.lopera@colegiatura.edu.co', '2020-06-27 23:20:43', 'AUTHENTICATION_SUCCESS'),
       (4184, 'jose.lopera@colegiatura.edu.co', '2020-06-27 23:22:03', 'AUTHENTICATION_SUCCESS'),
       (4185, 'jose.lopera@colegiatura.edu.co', '2020-06-27 23:22:43', 'AUTHENTICATION_SUCCESS'),
       (4186, 'management@celeb.tv', '2020-06-27 23:25:35', 'AUTHENTICATION_SUCCESS'),
       (4187, 'jose.lopera@colegiatura.edu.co', '2020-06-27 23:26:05', 'AUTHENTICATION_SUCCESS'),
       (4188, 'alex@pmi-americas.com', '2020-06-27 23:52:37', 'AUTHENTICATION_SUCCESS'),
       (4189, 'alex@pmi-americas.com', '2020-06-27 23:52:43', 'AUTHENTICATION_SUCCESS'),
       (4190, 'superadmin', '2020-06-28 01:29:21', 'AUTHENTICATION_SUCCESS'),
       (4191, 'superadmin', '2020-06-28 01:39:45', 'AUTHENTICATION_SUCCESS'),
       (4192, 'superadmin', '2020-06-28 01:41:15', 'AUTHENTICATION_SUCCESS'),
       (4193, 'alex@pmi-americas.com', '2020-06-28 01:43:55', 'AUTHENTICATION_SUCCESS'),
       (4194, 'jose.lopera@colegiatura.edu.co', '2020-06-28 03:31:29', 'AUTHENTICATION_SUCCESS'),
       (4195, 'jose.lopera@colegiatura.edu.co', '2020-06-28 03:32:19', 'AUTHENTICATION_SUCCESS'),
       (4196, 'superadmin', '2020-06-28 14:32:09', 'AUTHENTICATION_SUCCESS'),
       (4197, 'superadmin', '2020-06-28 14:34:27', 'AUTHENTICATION_SUCCESS'),
       (4198, 'superadmin', '2020-06-28 14:34:58', 'AUTHENTICATION_SUCCESS'),
       (4199, 'management@celeb.tv', '2020-06-28 15:48:17', 'AUTHENTICATION_SUCCESS'),
       (4200, 'alex@pmi-americas.com', '2020-06-29 00:04:22', 'AUTHENTICATION_SUCCESS'),
       (4201, 'andresrg2907@gmail.com', '2020-06-29 02:54:15', 'AUTHENTICATION_SUCCESS'),
       (4202, 'superadmin', '2020-06-29 11:18:23', 'AUTHENTICATION_SUCCESS'),
       (4203, 'ag.ucaliri@gmail.com', '2020-06-29 12:08:57', 'AUTHENTICATION_SUCCESS'),
       (4204, 'superadmin', '2020-06-29 12:09:38', 'AUTHENTICATION_SUCCESS'),
       (4205, 'alex@pmi-americas.com', '2020-06-29 15:18:29', 'AUTHENTICATION_SUCCESS'),
       (4206, 'superadmin', '2020-06-29 17:30:21', 'AUTHENTICATION_SUCCESS'),
       (4207, 'superadmin', '2020-06-29 17:57:35', 'AUTHENTICATION_SUCCESS'),
       (4208, 'alex@pmi-americas.com', '2020-06-29 20:00:02', 'AUTHENTICATION_SUCCESS'),
       (4209, 'alex@pmi-americas.com', '2020-06-30 14:48:45', 'AUTHENTICATION_SUCCESS'),
       (4210, 'superadmin', '2020-06-30 15:22:14', 'AUTHENTICATION_SUCCESS'),
       (4211, 'alex@pmi-americas.com', '2020-06-30 15:24:09', 'AUTHENTICATION_SUCCESS'),
       (4212, 'alex@pmi-americas.com', '2020-06-30 16:16:12', 'AUTHENTICATION_SUCCESS'),
       (4213, 'superadmin', '2020-06-30 17:41:03', 'AUTHENTICATION_SUCCESS'),
       (4214, 'superadmin', '2020-06-30 19:15:07', 'AUTHENTICATION_SUCCESS'),
       (4215, 'alex@pmi-americas.com', '2020-06-30 19:30:01', 'AUTHENTICATION_SUCCESS'),
       (4216, 'management@celeb.tv', '2020-06-30 19:32:35', 'AUTHENTICATION_SUCCESS'),
       (4217, 'management@celeb.tv', '2020-06-30 19:34:58', 'AUTHENTICATION_SUCCESS'),
       (4218, 'superadmin', '2020-06-30 19:57:10', 'AUTHENTICATION_SUCCESS'),
       (4219, 'ag.ucaliri@gmail.com', '2020-06-30 19:59:01', 'AUTHENTICATION_SUCCESS'),
       (4220, 'alex@pmi-americas.com', '2020-07-01 05:43:16', 'AUTHENTICATION_SUCCESS'),
       (4221, 'alex@pmi-americas.com', '2020-07-01 05:44:28', 'AUTHENTICATION_SUCCESS'),
       (4222, 'alex@pmi-americas.com', '2020-07-01 15:52:12', 'AUTHENTICATION_SUCCESS'),
       (4223, 'superadmin', '2020-07-02 13:34:51', 'AUTHENTICATION_SUCCESS'),
       (4224, 'superadmin', '2020-07-02 14:57:25', 'AUTHENTICATION_SUCCESS'),
       (4225, 'superadmin', '2020-07-02 16:57:40', 'AUTHENTICATION_SUCCESS'),
       (4226, 'alex@pmi-americas.com', '2020-07-02 18:22:55', 'AUTHENTICATION_SUCCESS'),
       (4227, 'alex@danalex.org', '2020-07-02 20:15:46', 'AUTHENTICATION_SUCCESS'),
       (4228, 'management@celeb.tv', '2020-07-03 19:10:17', 'AUTHENTICATION_SUCCESS'),
       (4229, 'jose.lopera@colegiatura.edu.co', '2020-07-04 09:49:29', 'AUTHENTICATION_SUCCESS'),
       (4230, 'jose.lopera@colegiatura.edu.co', '2020-07-05 02:49:04', 'AUTHENTICATION_SUCCESS'),
       (4231, 'jose.lopera@colegiatura.edu.co', '2020-07-05 03:36:12', 'AUTHENTICATION_SUCCESS'),
       (4232, 'Jose.lopera@colegiatura.edu.co', '2020-07-05 03:38:09', 'AUTHENTICATION_FAILURE'),
       (4233, 'jose.lopera@colegiatura.edu.co', '2020-07-05 03:38:53', 'AUTHENTICATION_SUCCESS'),
       (4234, 'jose.lopera@colegiatura.edu.co', '2020-07-05 03:39:10', 'AUTHENTICATION_SUCCESS'),
       (4235, 'jose.lopera@colegiatura.edu.co', '2020-07-05 15:14:41', 'AUTHENTICATION_SUCCESS'),
       (4236, 'jose.lopera@colegiatura.edu.co', '2020-07-05 15:32:08', 'AUTHENTICATION_SUCCESS'),
       (4237, 'andresrg2907@gmail.com', '2020-07-05 23:05:55', 'AUTHENTICATION_SUCCESS'),
       (4238, 'Andresrg2907@gmail.com', '2020-07-05 23:06:41', 'AUTHENTICATION_FAILURE'),
       (4239, 'andresrg2907@gmail.com', '2020-07-05 23:06:55', 'AUTHENTICATION_SUCCESS'),
       (4240, 'jose.lopera@colegiatura.edu.co', '2020-07-06 01:07:27', 'AUTHENTICATION_SUCCESS'),
       (4241, 'jose.lopera@colegiatura.edu.co', '2020-07-06 11:15:46', 'AUTHENTICATION_SUCCESS'),
       (4242, 'andresrg2907@gmail.com', '2020-07-06 12:05:32', 'AUTHENTICATION_SUCCESS'),
       (4243, 'alex@pmi-americas.com', '2020-07-06 14:25:59', 'AUTHENTICATION_SUCCESS'),
       (4244, 'alex@pmi-americas.com', '2020-07-06 16:38:59', 'AUTHENTICATION_SUCCESS'),
       (4245, 'alex@pmi-americas.com', '2020-07-06 19:10:30', 'AUTHENTICATION_SUCCESS'),
       (4246, 'andresrg2907@gmail.com', '2020-07-06 20:21:04', 'AUTHENTICATION_SUCCESS'),
       (4247, 'superadmin', '2020-07-06 21:32:27', 'AUTHENTICATION_SUCCESS'),
       (4248, 'andresrg2907@gmail.com', '2020-07-06 23:03:33', 'AUTHENTICATION_SUCCESS'),
       (4249, 'alex@pmi-americas.com', '2020-07-07 02:08:50', 'AUTHENTICATION_SUCCESS'),
       (4250, 'superadmin', '2020-07-07 12:18:02', 'AUTHENTICATION_SUCCESS'),
       (4251, 'stuart@iml.ad', '2020-07-07 13:32:51', 'AUTHENTICATION_SUCCESS'),
       (4252, 'yoke@iml.ad', '2020-07-07 13:39:04', 'AUTHENTICATION_SUCCESS'),
       (4253, 'stuart@iml.ad', '2020-07-07 13:42:05', 'AUTHENTICATION_SUCCESS'),
       (4254, 'yoke@iml.ad', '2020-07-07 13:42:27', 'AUTHENTICATION_SUCCESS'),
       (4255, 'alex@pmi-americas.com', '2020-07-07 13:57:52', 'AUTHENTICATION_SUCCESS'),
       (4256, 'superadmin', '2020-07-08 12:41:21', 'AUTHENTICATION_SUCCESS'),
       (4257, 'alex@pmi-americas.com', '2020-07-08 14:45:45', 'AUTHENTICATION_SUCCESS'),
       (4258, 'management@celeb.tv', '2020-07-08 19:27:57', 'AUTHENTICATION_SUCCESS'),
       (4259, 'alex@pmi-americas.com', '2020-07-08 20:30:53', 'AUTHENTICATION_SUCCESS'),
       (4260, 'alex@danalex.org', '2020-07-08 20:31:06', 'AUTHENTICATION_SUCCESS'),
       (4261, 'alex@pmi-americas.com', '2020-07-08 20:32:52', 'AUTHENTICATION_SUCCESS'),
       (4262, 'alex@pmi-americas.com', '2020-07-09 00:05:19', 'AUTHENTICATION_SUCCESS'),
       (4263, 'stuart@iml.ad', '2020-07-09 13:24:56', 'AUTHENTICATION_SUCCESS'),
       (4264, 'yoke@iml.ad', '2020-07-09 16:06:17', 'AUTHENTICATION_SUCCESS'),
       (4265, 'alex@pmi-americas.com', '2020-07-09 17:07:47', 'AUTHENTICATION_SUCCESS'),
       (4266, 'alex@pmi-americas.com', '2020-07-09 18:00:32', 'AUTHENTICATION_SUCCESS'),
       (4267, 'superadmin', '2020-07-09 19:31:56', 'AUTHENTICATION_SUCCESS'),
       (4268, 'alex@pmi-americas.com', '2020-07-09 21:59:28', 'AUTHENTICATION_SUCCESS'),
       (4269, 'alex@pmi-americas.com', '2020-07-09 23:40:10', 'AUTHENTICATION_SUCCESS'),
       (4270, 'yoke@iml.ad', '2020-07-10 13:49:28', 'AUTHENTICATION_SUCCESS'),
       (4271, 'alex@pmi-americas.com', '2020-07-10 14:01:12', 'AUTHENTICATION_SUCCESS'),
       (4272, 'alex@pmi-americas.com', '2020-07-10 18:45:01', 'AUTHENTICATION_SUCCESS'),
       (4273, 'alex@pmi-americas.com', '2020-07-10 19:48:30', 'AUTHENTICATION_SUCCESS'),
       (4274, 'management@celeb.tv', '2020-07-10 22:13:48', 'AUTHENTICATION_SUCCESS'),
       (4275, 'henrytejadag@hotmail.com', '2020-07-11 00:59:26', 'AUTHENTICATION_SUCCESS'),
       (4276, 'henrytejadag@hotmail.com', '2020-07-11 01:18:46', 'AUTHENTICATION_SUCCESS'),
       (4277, 'alex@pmi-americas.com', '2020-07-13 00:13:43', 'AUTHENTICATION_SUCCESS'),
       (4278, 'andresrg2907@gmail.com', '2020-07-13 02:21:26', 'AUTHENTICATION_SUCCESS'),
       (4279, 'henrytejadag@hotmail.com', '2020-07-13 13:33:14', 'AUTHENTICATION_SUCCESS'),
       (4280, 'jose.lopera@colegiatura.edu.co', '2020-07-13 14:06:54', 'AUTHENTICATION_SUCCESS'),
       (4281, 'alex@pmi-americas.com', '2020-07-13 14:42:38', 'AUTHENTICATION_SUCCESS'),
       (4282, 'stuart@iml.ad', '2020-07-13 15:13:18', 'AUTHENTICATION_SUCCESS'),
       (4283, 'yoke@iml.ad', '2020-07-13 16:09:58', 'AUTHENTICATION_SUCCESS'),
       (4284, 'stuart@iml.ad', '2020-07-13 16:10:31', 'AUTHENTICATION_FAILURE'),
       (4285, 'stuart@iml.ad', '2020-07-13 16:10:45', 'AUTHENTICATION_SUCCESS'),
       (4286, 'yoke@iml.ad', '2020-07-13 16:22:49', 'AUTHENTICATION_SUCCESS'),
       (4287, 'management@celeb.tv', '2020-07-13 17:17:44', 'AUTHENTICATION_SUCCESS'),
       (4288, 'alex@pmi-americas.com', '2020-07-13 18:53:27', 'AUTHENTICATION_SUCCESS'),
       (4289, 'andresrg2907@gmail.com', '2020-07-13 20:40:24', 'AUTHENTICATION_SUCCESS'),
       (4290, 'alex@pmi-americas.com', '2020-07-13 21:39:43', 'AUTHENTICATION_SUCCESS'),
       (4291, 'jose.lopera@colegiatura.edu.co', '2020-07-13 22:38:50', 'AUTHENTICATION_SUCCESS'),
       (4292, 'alex@pmi-americas.com', '2020-07-13 23:49:08', 'AUTHENTICATION_SUCCESS'),
       (4293, 'stuart@iml.ad', '2020-07-14 06:06:28', 'AUTHENTICATION_SUCCESS'),
       (4294, 'jose.lopera@colegiatura.edu.co', '2020-07-14 09:46:53', 'AUTHENTICATION_SUCCESS'),
       (4295, 'jose.lopera@colegiatura.edu.co', '2020-07-14 12:24:39', 'AUTHENTICATION_SUCCESS'),
       (4296, 'jose.lopera@colegiatura.edu.co', '2020-07-14 16:49:02', 'AUTHENTICATION_SUCCESS'),
       (4297, 'andresrg2907@gmail.com', '2020-07-14 20:13:30', 'AUTHENTICATION_SUCCESS'),
       (4298, 'alex@pmi-americas.com', '2020-07-14 20:16:07', 'AUTHENTICATION_SUCCESS'),
       (4299, 'management@celeb.tv', '2020-07-14 21:04:09', 'AUTHENTICATION_SUCCESS'),
       (4300, 'management@celeb.tv', '2020-07-14 21:08:16', 'AUTHENTICATION_SUCCESS'),
       (4301, 'alex@pmi-americas.com', '2020-07-14 22:41:53', 'AUTHENTICATION_SUCCESS'),
       (4302, 'management@celeb.tv', '2020-07-14 23:17:46', 'AUTHENTICATION_SUCCESS'),
       (4303, 'yoke@iml.ad', '2020-07-15 07:06:49', 'AUTHENTICATION_SUCCESS'),
       (4304, 'yoke@iml.ad', '2020-07-15 07:50:42', 'AUTHENTICATION_SUCCESS'),
       (4305, 'andresrg2907@gmail.com', '2020-07-15 13:39:43', 'AUTHENTICATION_SUCCESS'),
       (4306, 'alex@pmi-americas.com', '2020-07-15 14:25:03', 'AUTHENTICATION_SUCCESS'),
       (4307, 'alex@pmi-americas.com', '2020-07-15 15:29:36', 'AUTHENTICATION_SUCCESS'),
       (4308, 'alex@pmi-americas.com', '2020-07-15 15:50:14', 'AUTHENTICATION_SUCCESS'),
       (4309, 'alex@pmi-americas.com', '2020-07-15 18:10:31', 'AUTHENTICATION_SUCCESS'),
       (4310, 'superadmin', '2020-07-15 21:19:10', 'AUTHENTICATION_SUCCESS'),
       (4311, 'alex@pmi-americas.com', '2020-07-15 21:54:50', 'AUTHENTICATION_SUCCESS'),
       (4312, 'superadmin', '2020-07-15 22:39:49', 'AUTHENTICATION_SUCCESS'),
       (4313, 'superadmin', '2020-07-15 22:49:07', 'AUTHENTICATION_SUCCESS'),
       (4314, 'superadmin', '2020-07-15 23:33:08', 'AUTHENTICATION_SUCCESS'),
       (4315, 'yoke@iml.ad', '2020-07-16 06:53:04', 'AUTHENTICATION_SUCCESS'),
       (4316, 'yoke@iml.ad', '2020-07-16 12:38:26', 'AUTHENTICATION_SUCCESS'),
       (4317, 'alex@pmi-americas.com', '2020-07-16 14:36:06', 'AUTHENTICATION_SUCCESS'),
       (4318, 'alex@pmi-americas.com', '2020-07-16 15:06:38', 'AUTHENTICATION_SUCCESS');
/*!40000 ALTER TABLE `jhi_persistent_audit_event`
    ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2020-07-16 14:18:27
