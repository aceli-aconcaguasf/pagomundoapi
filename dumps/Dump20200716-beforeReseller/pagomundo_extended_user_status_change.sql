-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: ls-449eb857e6ae75cdb34254c1ef8b62ea8f7d95f1.cwxp9rzhaq1d.us-east-1.rds.amazonaws.com    Database: pagomundo
-- ------------------------------------------------------
-- Server version	5.7.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN = 0;

--
-- GTID state at the beginning of the backup
--

SET @@GLOBAL.GTID_PURGED = /*!80000 '+'*/ '';

--
-- Table structure for table `extended_user_status_change`
--

DROP TABLE IF EXISTS `extended_user_status_change`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `extended_user_status_change`
(
    `id`               bigint(20) NOT NULL AUTO_INCREMENT,
    `date_created`     datetime     DEFAULT NULL,
    `status`           varchar(255) DEFAULT NULL,
    `reason`           longtext,
    `extended_user_id` bigint(20)   DEFAULT NULL,
    `user_id`          bigint(20)   DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_extended_user_status_change_extended_user_id` (`extended_user_id`),
    KEY `fk_extended_user_status_change_user_id_id` (`user_id`),
    CONSTRAINT `fk_extended_user_status_change_extended_user_id` FOREIGN KEY (`extended_user_id`) REFERENCES `extended_user` (`id`),
    CONSTRAINT `fk_extended_user_status_change_user_id_id` FOREIGN KEY (`user_id`) REFERENCES `extended_user` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 5755
  DEFAULT CHARSET = utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extended_user_status_change`
--

LOCK TABLES `extended_user_status_change` WRITE;
/*!40000 ALTER TABLE `extended_user_status_change`
    DISABLE KEYS */;
INSERT INTO `extended_user_status_change`
VALUES (17, '2019-11-13 17:16:51', 'ACCEPTED', NULL, 93, 93),
       (18, '2019-11-13 18:47:55', 'ACCEPTED', NULL, 93, NULL),
       (19, '2019-11-13 18:52:29', 'ACCEPTED', NULL, 94, 94),
       (20, '2019-11-13 18:57:37', 'ACCEPTED', NULL, 95, 95),
       (25, '2019-11-13 19:27:36', 'ACCEPTED', NULL, 95, 94),
       (5719, '2019-12-12 13:37:14', 'ACCEPTED', NULL, 95, NULL),
       (5720, '2019-12-12 14:08:48', 'CREATED', NULL, 3178, 3178),
       (5721, '2019-12-12 14:18:52', 'ACCEPTED', NULL, 3178, 94),
       (5722, '2020-01-14 16:23:32', 'ACCEPTED', NULL, 3179, 93),
       (5723, '2020-01-14 16:27:55', 'ACCEPTED', NULL, 3180, 3179),
       (5724, '2020-01-14 16:42:17', 'CREATED', NULL, 3181, 3181),
       (5725, '2020-01-14 16:45:44', 'ACCEPTED', '', 3181, 3179),
       (5726, '2020-02-05 22:16:05', 'ACCEPTED', NULL, 3182, 3179),
       (5727, '2020-02-18 16:58:59', 'REJECTED', 'Test sending email to smtp port 587', 95, 94),
       (5728, '2020-02-20 17:07:17', 'ACCEPTED', NULL, 3183, 93),
       (5729, '2020-02-20 17:35:48', 'CREATED', NULL, 3184, 3184),
       (5730, '2020-02-20 17:38:38', 'ACCEPTED', NULL, 3184, 3179),
       (5731, '2020-02-28 11:42:34', 'ACCEPTED', 'Test sending email to smtp port 587', 95, 93),
       (5732, '2020-03-12 20:00:42', 'CREATED', NULL, 3185, 3185),
       (5733, '2020-03-18 16:49:50', 'CREATED', NULL, 3186, 3186),
       (5734, '2020-03-18 19:39:48', 'CREATED', NULL, 3187, 3187),
       (5735, '2020-03-18 19:41:18', 'CANCELLED', 'User de prueba', 3178, 93),
       (5736, '2020-03-18 19:44:04', 'CANCELLED', 'User de prueba', 3187, 93),
       (5737, '2020-03-23 19:20:12', 'ACCEPTED', '', 3186, 3179),
       (5738, '2020-03-30 13:50:23', 'ACCEPTED', NULL, 3188, 93),
       (5739, '2020-03-30 14:39:49', 'ACCEPTED', NULL, 3189, 93),
       (5740, '2020-04-01 18:03:10', 'ACCEPTED', NULL, 3190, 3179),
       (5741, '2020-04-15 18:32:35', 'ACCEPTED', NULL, 3191, 3191),
       (5742, '2020-04-18 00:44:29', 'ACCEPTED', '', 3185, 3179),
       (5743, '2020-04-22 14:33:10', 'ACCEPTED', NULL, 3192, 3192),
       (5744, '2020-04-28 17:04:12', 'ACCEPTED', NULL, 3193, 3193),
       (5745, '2020-04-29 20:46:22', 'ACCEPTED', NULL, 3194, 3194),
       (5746, '2020-04-30 13:53:13', 'ACCEPTED', NULL, 3195, 93),
       (5747, '2020-05-05 15:41:40', 'ACCEPTED', NULL, 3196, 3196),
       (5748, '2020-05-27 14:21:54', 'ACCEPTED', NULL, 3197, 3188),
       (5749, '2020-05-30 04:44:17', 'ACCEPTED', NULL, 3198, 3188),
       (5750, '2020-06-03 14:57:51', 'ACCEPTED', NULL, 3199, 3199),
       (5751, '2020-06-03 15:00:52', 'ACCEPTED', NULL, 3200, 3179),
       (5752, '2020-06-11 14:37:21', 'ACCEPTED', NULL, 3201, 3201),
       (5753, '2020-06-13 13:14:31', 'ACCEPTED', NULL, 3202, 3202),
       (5754, '2020-07-11 01:05:18', 'ACCEPTED', NULL, 3203, 3203);
/*!40000 ALTER TABLE `extended_user_status_change`
    ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2020-07-16 14:18:35
