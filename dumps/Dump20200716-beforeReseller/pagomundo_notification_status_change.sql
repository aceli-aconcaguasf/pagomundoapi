-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: ls-449eb857e6ae75cdb34254c1ef8b62ea8f7d95f1.cwxp9rzhaq1d.us-east-1.rds.amazonaws.com    Database: pagomundo
-- ------------------------------------------------------
-- Server version	5.7.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN = 0;

--
-- GTID state at the beginning of the backup
--

SET @@GLOBAL.GTID_PURGED = /*!80000 '+'*/ '';

--
-- Table structure for table `notification_status_change`
--

DROP TABLE IF EXISTS `notification_status_change`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notification_status_change`
(
    `id`              bigint(20)   NOT NULL AUTO_INCREMENT,
    `date_created`    datetime     NOT NULL,
    `status`          varchar(255) NOT NULL,
    `reason`          longtext,
    `notification_id` bigint(20) DEFAULT NULL,
    `user_id`         bigint(20) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_notification_status_change_notification_id` (`notification_id`),
    KEY `fk_notification_status_change_user_id` (`user_id`),
    CONSTRAINT `fk_notification_status_change_notification_id` FOREIGN KEY (`notification_id`) REFERENCES `notification` (`id`),
    CONSTRAINT `fk_notification_status_change_user_id` FOREIGN KEY (`user_id`) REFERENCES `extended_user` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 143
  DEFAULT CHARSET = utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification_status_change`
--

LOCK TABLES `notification_status_change` WRITE;
/*!40000 ALTER TABLE `notification_status_change`
    DISABLE KEYS */;
INSERT INTO `notification_status_change`
VALUES (1, '2020-02-05 22:16:05', 'CREATED', '', 1, 3179),
       (2, '2020-02-05 22:16:06', 'CREATED', '', 2, 3179),
       (3, '2020-02-18 14:38:50', 'CREATED', '', 3, 3180),
       (4, '2020-02-18 14:43:26', 'CREATED', '', 4, 3179),
       (5, '2020-02-18 15:01:17', 'CREATED', '', 5, 3180),
       (6, '2020-02-18 15:14:28', 'CREATED', '', 6, 3180),
       (7, '2020-02-18 16:58:59', 'CREATED', '', 7, 94),
       (8, '2020-02-18 17:00:07', 'CREATED', '', 8, 3178),
       (9, '2020-02-19 15:59:22', 'CREATED', '', 9, 3181),
       (11, '2020-02-20 17:07:17', 'CREATED', '', 11, 93),
       (12, '2020-02-20 17:26:05', 'CREATED', '', 12, 3180),
       (13, '2020-02-20 17:31:06', 'CREATED', '', 13, 3180),
       (14, '2020-02-20 17:35:49', 'CREATED', '', 14, 3184),
       (16, '2020-02-20 17:40:00', 'CREATED', '', 16, 3180),
       (17, '2020-02-28 11:42:34', 'CREATED', '', 17, 93),
       (18, '2020-03-04 20:45:11', 'CREATED', '', 18, 3180),
       (19, '2020-03-11 14:37:42', 'CREATED', '', 19, 95),
       (20, '2020-03-12 19:54:46', 'CREATED', '', 20, 3180),
       (21, '2020-03-12 20:00:42', 'CREATED', '', 21, 3185),
       (22, '2020-03-18 16:00:04', 'CREATED', '', 22, 3180),
       (23, '2020-03-18 16:49:51', 'CREATED', '', 23, 3186),
       (24, '2020-03-18 19:35:30', 'CREATED', '', 24, 95),
       (25, '2020-03-18 19:39:48', 'CREATED', '', 25, 3187),
       (26, '2020-03-18 19:41:18', 'CREATED', '', 26, 93),
       (27, '2020-03-18 19:44:04', 'CREATED', '', 27, 93),
       (28, '2020-03-23 19:20:12', 'CREATED', '', 28, 3179),
       (29, '2020-03-23 19:27:04', 'CREATED', '', 29, 3180),
       (30, '2020-03-30 13:50:24', 'CREATED', '', 30, 93),
       (31, '2020-03-30 14:39:49', 'CREATED', '', 31, 93),
       (32, '2020-03-30 14:47:56', 'CREATED', '', 32, 3180),
       (33, '2020-04-01 18:03:10', 'CREATED', '', 33, 3179),
       (34, '2020-04-01 18:03:10', 'CREATED', '', 34, 3179),
       (35, '2020-04-01 18:03:38', 'CREATED', '', 35, 3179),
       (36, '2020-04-01 18:03:42', 'CREATED', '', 36, 3179),
       (37, '2020-04-01 18:03:56', 'CREATED', '', 37, 3179),
       (38, '2020-04-10 16:37:26', 'CREATED', '', 38, 3190),
       (39, '2020-04-10 16:44:02', 'CREATED', '', 39, 3190),
       (40, '2020-04-10 17:45:59', 'CREATED', '', 40, 3179),
       (41, '2020-04-13 21:40:24', 'CREATED', '', 41, 3190),
       (42, '2020-04-13 21:43:30', 'CREATED', '', 42, 3190),
       (43, '2020-04-14 19:58:25', 'CREATED', '', 43, 95),
       (44, '2020-04-15 15:52:33', 'CREATED', '', 44, 3180),
       (45, '2020-04-15 18:32:35', 'CREATED', '', 45, 3191),
       (46, '2020-04-16 13:35:26', 'CREATED', '', 46, 3180),
       (47, '2020-04-16 13:38:35', 'CREATED', '', 47, 3179),
       (48, '2020-04-16 14:35:07', 'CREATED', '', 48, 3180),
       (49, '2020-04-16 14:42:19', 'CREATED', '', 49, 3179),
       (50, '2020-04-16 14:42:21', 'CREATED', '', 50, 3179),
       (51, '2020-04-16 14:42:48', 'CREATED', '', 51, 3179),
       (52, '2020-04-16 14:49:21', 'CREATED', '', 52, 3179),
       (53, '2020-04-16 14:49:23', 'CREATED', '', 53, 3179),
       (54, '2020-04-18 00:44:29', 'CREATED', '', 54, 3179),
       (55, '2020-04-18 00:47:04', 'CREATED', '', 55, 3179),
       (56, '2020-04-22 14:28:51', 'CREATED', '', 56, 95),
       (57, '2020-04-22 14:33:10', 'CREATED', '', 57, 3192),
       (58, '2020-04-24 15:37:04', 'CREATED', '', 58, 3180),
       (59, '2020-04-24 15:40:09', 'CREATED', '', 59, 3179),
       (60, '2020-04-24 18:19:45', 'CREATED', '', 60, 3179),
       (61, '2020-04-24 18:19:47', 'CREATED', '', 61, 3179),
       (62, '2020-04-28 17:04:12', 'CREATED', '', 62, 3193),
       (63, '2020-04-29 14:57:48', 'CREATED', '', 63, 3180),
       (64, '2020-04-29 14:58:03', 'CREATED', '', 64, 3180),
       (65, '2020-04-29 14:59:09', 'CREATED', '', 65, 3179),
       (66, '2020-04-29 19:32:21', 'CREATED', '', 66, 3180),
       (67, '2020-04-29 19:32:46', 'CREATED', '', 67, 3179),
       (68, '2020-04-29 20:46:22', 'CREATED', '', 68, 3194),
       (69, '2020-04-30 13:53:13', 'CREATED', '', 69, 93),
       (70, '2020-04-30 13:59:19', 'CREATED', '', 70, 3180),
       (71, '2020-05-05 15:17:04', 'CREATED', '', 71, 3179),
       (72, '2020-05-05 15:18:53', 'CREATED', '', 72, 3180),
       (73, '2020-05-05 15:41:41', 'CREATED', '', 73, 3196),
       (74, '2020-05-06 13:53:48', 'CREATED', '', 74, 3179),
       (75, '2020-05-06 13:54:52', 'CREATED', '', 75, 3190),
       (76, '2020-05-06 21:06:31', 'CREATED', '', 76, 3179),
       (77, '2020-05-07 21:36:37', 'CREATED', '', 77, 3188),
       (78, '2020-05-20 20:43:02', 'CREATED', '', 78, 3180),
       (79, '2020-05-20 21:57:46', 'CREATED', '', 79, 3179),
       (80, '2020-05-27 12:29:59', 'CREATED', '', 80, 3188),
       (81, '2020-05-27 14:21:53', 'CREATED', '', 81, 3188),
       (82, '2020-05-27 14:21:54', 'CREATED', '', 82, 3188),
       (83, '2020-05-29 19:44:00', 'CREATED', '', 83, 3179),
       (84, '2020-05-30 04:44:17', 'CREATED', '', 84, 3188),
       (85, '2020-05-30 04:44:17', 'CREATED', '', 85, 3188),
       (86, '2020-06-02 10:25:37', 'CREATED', '', 86, 3197),
       (87, '2020-06-02 13:22:11', 'CREATED', '', 87, 3179),
       (88, '2020-06-02 20:12:33', 'CREATED', '', 88, 3180),
       (89, '2020-06-03 14:57:51', 'CREATED', '', 89, 3199),
       (90, '2020-06-03 15:00:52', 'CREATED', '', 90, 3179),
       (91, '2020-06-03 15:00:52', 'CREATED', '', 91, 3179),
       (92, '2020-06-03 16:20:24', 'CREATED', '', 92, 3197),
       (93, '2020-06-03 23:36:39', 'CREATED', '', 93, 3179),
       (94, '2020-06-04 19:12:37', 'CREATED', '', 94, 3200),
       (95, '2020-06-04 19:14:38', 'CREATED', '', 95, 3200),
       (96, '2020-06-04 19:46:34', 'CREATED', '', 96, 3179),
       (97, '2020-06-04 20:02:31', 'CREATED', '', 97, 3179),
       (98, '2020-06-04 22:06:36', 'CREATED', '', 98, 3200),
       (99, '2020-06-04 22:09:03', 'CREATED', '', 99, 3200),
       (100, '2020-06-09 17:37:26', 'CREATED', '', 100, 3200),
       (101, '2020-06-09 18:13:42', 'CREATED', '', 101, 3200),
       (102, '2020-06-09 21:15:29', 'CREATED', '', 102, 3180),
       (103, '2020-06-09 22:19:27', 'CREATED', '', 103, 3179),
       (104, '2020-06-09 22:19:30', 'CREATED', '', 104, 3179),
       (105, '2020-06-10 12:04:32', 'CREATED', '', 105, 95),
       (106, '2020-06-10 18:36:52', 'CREATED', '', 106, 95),
       (107, '2020-06-10 19:33:59', 'CREATED', '', 107, 95),
       (108, '2020-06-11 14:37:21', 'CREATED', '', 108, 3201),
       (109, '2020-06-11 15:38:49', 'CREATED', '', 109, 3200),
       (110, '2020-06-11 20:03:44', 'CREATED', '', 110, 3179),
       (111, '2020-06-12 18:02:12', 'CREATED', '', 111, 3200),
       (112, '2020-06-12 18:08:10', 'CREATED', '', 112, 3200),
       (113, '2020-06-13 13:14:31', 'CREATED', '', 113, 3202),
       (114, '2020-06-13 14:30:24', 'CREATED', '', 114, 3200),
       (115, '2020-06-15 00:50:05', 'CREATED', '', 115, 3179),
       (116, '2020-06-16 19:07:55', 'CREATED', '', 116, 3179),
       (117, '2020-06-16 21:20:07', 'CREATED', '', 117, 3179),
       (118, '2020-06-19 11:40:37', 'CREATED', '', 118, 94),
       (119, '2020-06-19 17:14:33', 'CREATED', '', 119, 3200),
       (120, '2020-06-23 15:01:39', 'CREATED', '', 120, 3179),
       (121, '2020-06-23 15:01:42', 'CREATED', '', 121, 3179),
       (122, '2020-06-26 23:39:59', 'CREATED', '', 122, 3200),
       (123, '2020-06-26 23:44:16', 'CREATED', '', 123, 3200),
       (124, '2020-06-27 23:21:03', 'CREATED', '', 124, 3201),
       (125, '2020-06-30 15:24:13', 'CREATED', '', 125, 3179),
       (126, '2020-06-30 15:24:16', 'CREATED', '', 126, 3179),
       (127, '2020-07-03 19:11:09', 'CREATED', '', 127, 3200),
       (128, '2020-07-06 19:10:37', 'CREATED', '', 128, 3179),
       (129, '2020-07-06 19:10:41', 'CREATED', '', 129, 3179),
       (130, '2020-07-07 13:44:47', 'CREATED', '', 130, 3198),
       (131, '2020-07-08 20:32:41', 'CREATED', '', 131, 3180),
       (132, '2020-07-09 17:08:36', 'CREATED', '', 132, 3179),
       (133, '2020-07-09 17:08:40', 'CREATED', '', 133, 3179),
       (134, '2020-07-10 13:51:37', 'CREATED', '', 134, 3198),
       (135, '2020-07-10 22:14:13', 'CREATED', '', 135, 3200),
       (136, '2020-07-11 01:05:19', 'CREATED', '', 136, 3203),
       (137, '2020-07-13 16:41:10', 'CREATED', '', 137, 3179),
       (138, '2020-07-13 16:41:13', 'CREATED', '', 138, 3179),
       (139, '2020-07-14 21:14:42', 'CREATED', '', 139, 3200),
       (140, '2020-07-14 23:19:48', 'CREATED', '', 140, 3200),
       (141, '2020-07-15 15:50:46', 'CREATED', '', 141, 3179),
       (142, '2020-07-15 16:59:48', 'CREATED', '', 142, 3200);
/*!40000 ALTER TABLE `notification_status_change`
    ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2020-07-16 14:19:13
