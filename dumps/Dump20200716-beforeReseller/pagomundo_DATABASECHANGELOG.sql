-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: ls-449eb857e6ae75cdb34254c1ef8b62ea8f7d95f1.cwxp9rzhaq1d.us-east-1.rds.amazonaws.com    Database: pagomundo
-- ------------------------------------------------------
-- Server version	5.7.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN = 0;

--
-- GTID state at the beginning of the backup
--

SET @@GLOBAL.GTID_PURGED = /*!80000 '+'*/ '';

--
-- Table structure for table `DATABASECHANGELOG`
--

DROP TABLE IF EXISTS `DATABASECHANGELOG`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `DATABASECHANGELOG`
(
    `ID`            varchar(255) NOT NULL,
    `AUTHOR`        varchar(255) NOT NULL,
    `FILENAME`      varchar(255) NOT NULL,
    `DATEEXECUTED`  datetime     NOT NULL,
    `ORDEREXECUTED` int(11)      NOT NULL,
    `EXECTYPE`      varchar(10)  NOT NULL,
    `MD5SUM`        varchar(35)  DEFAULT NULL,
    `DESCRIPTION`   varchar(255) DEFAULT NULL,
    `COMMENTS`      varchar(255) DEFAULT NULL,
    `TAG`           varchar(255) DEFAULT NULL,
    `LIQUIBASE`     varchar(20)  DEFAULT NULL,
    `CONTEXTS`      varchar(255) DEFAULT NULL,
    `LABELS`        varchar(255) DEFAULT NULL,
    `DEPLOYMENT_ID` varchar(10)  DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DATABASECHANGELOG`
--

LOCK TABLES `DATABASECHANGELOG` WRITE;
/*!40000 ALTER TABLE `DATABASECHANGELOG`
    DISABLE KEYS */;
INSERT INTO `DATABASECHANGELOG`
VALUES ('00000000000001', 'jhipster', 'config/liquibase/changelog/00000000000000_initial_schema.xml',
        '2019-11-26 17:30:04', 1, 'EXECUTED', '8:c5bfc567913b118109a43e981cd02883',
        'createTable tableName=jhi_user; createTable tableName=jhi_authority; createTable tableName=jhi_user_authority; addPrimaryKey tableName=jhi_user_authority; addForeignKeyConstraint baseTableName=jhi_user_authority, constraintName=fk_authority_name, ...',
        '', NULL, '3.6.3', NULL, NULL, '4789403698'),
       ('20190816135649-1', 'jhipster', 'config/liquibase/changelog/20190816135649_added_entity_Country.xml',
        '2019-11-26 17:30:04', 2, 'EXECUTED', '8:c0313c480f83eeb9413affcf1da730b8', 'createTable tableName=country', '',
        NULL, '3.6.3', NULL, NULL, '4789403698'),
       ('20190816135649-1-relations', 'jhipster', 'config/liquibase/changelog/20190816135649_added_entity_Country.xml',
        '2019-11-26 17:30:04', 3, 'EXECUTED', '8:d41d8cd98f00b204e9800998ecf8427e', 'empty', '', NULL, '3.6.3', NULL,
        NULL, '4789403698'),
       ('20190816135650-1', 'jhipster', 'config/liquibase/changelog/20190816135650_added_entity_Bank.xml',
        '2019-11-26 17:30:04', 4, 'EXECUTED', '8:81861a1a2f6be2308fe88187b63e8390', 'createTable tableName=bank', '',
        NULL, '3.6.3', NULL, NULL, '4789403698'),
       ('20190816135650-1-relations', 'jhipster', 'config/liquibase/changelog/20190816135650_added_entity_Bank.xml',
        '2019-11-26 17:30:04', 5, 'EXECUTED', '8:d41d8cd98f00b204e9800998ecf8427e', 'empty', '', NULL, '3.6.3', NULL,
        NULL, '4789403698'),
       ('20190816135651-1', 'jhipster', 'config/liquibase/changelog/20190816135651_added_entity_Branch.xml',
        '2019-11-26 17:30:04', 6, 'EXECUTED', '8:aee2fea44f0cf6a0a23da68227b81c69', 'createTable tableName=branch', '',
        NULL, '3.6.3', NULL, NULL, '4789403698'),
       ('20190816135651-1-relations', 'jhipster', 'config/liquibase/changelog/20190816135651_added_entity_Branch.xml',
        '2019-11-26 17:30:04', 7, 'EXECUTED', '8:d41d8cd98f00b204e9800998ecf8427e', 'empty', '', NULL, '3.6.3', NULL,
        NULL, '4789403698'),
       ('20190816135652-1', 'jhipster', 'config/liquibase/changelog/20190816135652_added_entity_Currency.xml',
        '2019-11-26 17:30:04', 8, 'EXECUTED', '8:cf97a7044587df6aec15e180ec2906de', 'createTable tableName=currency',
        '', NULL, '3.6.3', NULL, NULL, '4789403698'),
       ('20190816135652-1-relations', 'jhipster', 'config/liquibase/changelog/20190816135652_added_entity_Currency.xml',
        '2019-11-26 17:30:04', 9, 'EXECUTED', '8:d41d8cd98f00b204e9800998ecf8427e', 'empty', '', NULL, '3.6.3', NULL,
        NULL, '4789403698'),
       ('20190816135653-1', 'jhipster', 'config/liquibase/changelog/20190816135653_added_entity_City.xml',
        '2019-11-26 17:30:04', 10, 'EXECUTED', '8:109684723b939b851c5f29f527e8a2d8', 'createTable tableName=city', '',
        NULL, '3.6.3', NULL, NULL, '4789403698'),
       ('20190816135653-1-relations', 'jhipster', 'config/liquibase/changelog/20190816135653_added_entity_City.xml',
        '2019-11-26 17:30:04', 11, 'EXECUTED', '8:d41d8cd98f00b204e9800998ecf8427e', 'empty', '', NULL, '3.6.3', NULL,
        NULL, '4789403698'),
       ('20190816135654-1', 'jhipster', 'config/liquibase/changelog/20190816135654_added_entity_IdType.xml',
        '2019-11-26 17:30:04', 12, 'EXECUTED', '8:9f3d9621b809c168a737431d828ea893', 'createTable tableName=id_type',
        '', NULL, '3.6.3', NULL, NULL, '4789403698'),
       ('20190816135654-1-relations', 'jhipster', 'config/liquibase/changelog/20190816135654_added_entity_IdType.xml',
        '2019-11-26 17:30:04', 13, 'EXECUTED', '8:d41d8cd98f00b204e9800998ecf8427e', 'empty', '', NULL, '3.6.3', NULL,
        NULL, '4789403698'),
       ('20190816135655-1', 'jhipster', 'config/liquibase/changelog/20190816135655_added_entity_ExtendedUser.xml',
        '2019-11-26 17:30:04', 14, 'EXECUTED', '8:6bcea7c8a563db0b70e7cefeb561e558',
        'createTable tableName=extended_user; dropDefaultValue columnName=birth_date, tableName=extended_user; dropDefaultValue columnName=date_created, tableName=extended_user; dropDefaultValue columnName=last_updated, tableName=extended_user',
        '', NULL, '3.6.3', NULL, NULL, '4789403698'),
       ('20190816135655-1-relations', 'jhipster',
        'config/liquibase/changelog/20190816135655_added_entity_ExtendedUser.xml', '2019-11-26 17:30:04', 15,
        'EXECUTED', '8:d41d8cd98f00b204e9800998ecf8427e', 'empty', '', NULL, '3.6.3', NULL, NULL, '4789403698'),
       ('20190816135656-1', 'jhipster', 'config/liquibase/changelog/20190816135656_added_entity_Transaction.xml',
        '2019-11-26 17:30:04', 16, 'EXECUTED', '8:09355cd0cd1639cc9a7d302b3619db9c',
        'createTable tableName=transaction; dropDefaultValue columnName=date_created, tableName=transaction; dropDefaultValue columnName=last_updated, tableName=transaction',
        '', NULL, '3.6.3', NULL, NULL, '4789403698'),
       ('20190816135656-1-relations', 'jhipster',
        'config/liquibase/changelog/20190816135656_added_entity_Transaction.xml', '2019-11-26 17:30:04', 17, 'EXECUTED',
        '8:d41d8cd98f00b204e9800998ecf8427e', 'empty', '', NULL, '3.6.3', NULL, NULL, '4789403698'),
       ('20190816135657-1', 'jhipster',
        'config/liquibase/changelog/20190816135657_added_entity_TransactionStatusChange.xml', '2019-11-26 17:30:05', 18,
        'EXECUTED', '8:e865367c5055385509b6e23f509755eb',
        'createTable tableName=transaction_status_change; dropDefaultValue columnName=date_created, tableName=transaction_status_change',
        '', NULL, '3.6.3', NULL, NULL, '4789403698'),
       ('20190816135657-1-relations', 'jhipster',
        'config/liquibase/changelog/20190816135657_added_entity_TransactionStatusChange.xml', '2019-11-26 17:30:05', 19,
        'EXECUTED', '8:d41d8cd98f00b204e9800998ecf8427e', 'empty', '', NULL, '3.6.3', NULL, NULL, '4789403698'),
       ('20190816135658-1', 'jhipster', 'config/liquibase/changelog/20190816135658_added_entity_Notification.xml',
        '2019-11-26 17:30:05', 20, 'EXECUTED', '8:4b425336498732c70ca88a1f119ea273',
        'createTable tableName=notification; dropDefaultValue columnName=date_created, tableName=notification; dropDefaultValue columnName=last_updated, tableName=notification',
        '', NULL, '3.6.3', NULL, NULL, '4789403698'),
       ('20190816135658-1-relations', 'jhipster',
        'config/liquibase/changelog/20190816135658_added_entity_Notification.xml', '2019-11-26 17:30:05', 21,
        'EXECUTED', '8:d41d8cd98f00b204e9800998ecf8427e', 'empty', '', NULL, '3.6.3', NULL, NULL, '4789403698'),
       ('20190816135659-1', 'jhipster',
        'config/liquibase/changelog/20190816135659_added_entity_NotificationStatusChange.xml', '2019-11-26 17:30:05',
        22, 'EXECUTED', '8:827431874784b4aac6dc6563c16aa538',
        'createTable tableName=notification_status_change; dropDefaultValue columnName=date_created, tableName=notification_status_change',
        '', NULL, '3.6.3', NULL, NULL, '4789403698'),
       ('20190816135659-1-relations', 'jhipster',
        'config/liquibase/changelog/20190816135659_added_entity_NotificationStatusChange.xml', '2019-11-26 17:30:05',
        23, 'EXECUTED', '8:d41d8cd98f00b204e9800998ecf8427e', 'empty', '', NULL, '3.6.3', NULL, NULL, '4789403698'),
       ('20190816135700-1', 'jhipster', 'config/liquibase/changelog/20190816135700_added_entity_BankAccount.xml',
        '2019-11-26 17:30:05', 24, 'EXECUTED', '8:15d8861050c2e8cb1dc194e2487cff15',
        'createTable tableName=bank_account', '', NULL, '3.6.3', NULL, NULL, '4789403698'),
       ('20190816135700-1-relations', 'jhipster',
        'config/liquibase/changelog/20190816135700_added_entity_BankAccount.xml', '2019-11-26 17:30:05', 25, 'EXECUTED',
        '8:d41d8cd98f00b204e9800998ecf8427e', 'empty', '', NULL, '3.6.3', NULL, NULL, '4789403698'),
       ('20190816135701-1', 'jhipster',
        'config/liquibase/changelog/20190816135701_added_entity_ExtendedUserRelation.xml', '2019-11-26 17:30:05', 26,
        'EXECUTED', '8:64ea7fbac6ea4fda6c4f52bf60de8623', 'createTable tableName=extended_user_relation', '', NULL,
        '3.6.3', NULL, NULL, '4789403698'),
       ('20190816135701-1-relations', 'jhipster',
        'config/liquibase/changelog/20190816135701_added_entity_ExtendedUserRelation.xml', '2019-11-26 17:30:05', 27,
        'EXECUTED', '8:d41d8cd98f00b204e9800998ecf8427e', 'empty', '', NULL, '3.6.3', NULL, NULL, '4789403698'),
       ('20190926180154-1', 'jhipster', 'config/liquibase/changelog/20190926180154_added_entity_TransactionGroup.xml',
        '2019-11-26 17:30:05', 28, 'EXECUTED', '8:00a7275f0190e6f3a692a410bd35dd07',
        'createTable tableName=transaction_group; dropDefaultValue columnName=date_created, tableName=transaction_group',
        '', NULL, '3.6.3', NULL, NULL, '4789403698'),
       ('20190926180154-1-relations', 'jhipster',
        'config/liquibase/changelog/20190926180154_added_entity_TransactionGroup.xml', '2019-11-26 17:30:05', 29,
        'EXECUTED', '8:d41d8cd98f00b204e9800998ecf8427e', 'empty', '', NULL, '3.6.3', NULL, NULL, '4789403698'),
       ('20191105145255-1', 'jhipster', 'config/liquibase/changelog/20191105145255_added_entity_ExtendedUserGroup.xml',
        '2019-11-26 17:30:05', 30, 'EXECUTED', '8:720ed571daf712c51b3037d8aa37681a',
        'createTable tableName=extended_user_group; dropDefaultValue columnName=date_created, tableName=extended_user_group',
        '', NULL, '3.6.3', NULL, NULL, '4789403698'),
       ('20191105145255-1-relations', 'jhipster',
        'config/liquibase/changelog/20191105145255_added_entity_ExtendedUserGroup.xml', '2019-11-26 17:30:05', 31,
        'EXECUTED', '8:d41d8cd98f00b204e9800998ecf8427e', 'empty', '', NULL, '3.6.3', NULL, NULL, '4789403698'),
       ('20191105191359-1', 'jhipster',
        'config/liquibase/changelog/20191105191359_added_entity_ExtendedUserStatusChange.xml', '2019-11-26 17:30:05',
        32, 'EXECUTED', '8:78c2d3bb15747172cf67d9f20aee6c86',
        'createTable tableName=extended_user_status_change; dropDefaultValue columnName=date_created, tableName=extended_user_status_change',
        '', NULL, '3.6.3', NULL, NULL, '4789403698'),
       ('20191105191359-1-relations', 'jhipster',
        'config/liquibase/changelog/20191105191359_added_entity_ExtendedUserStatusChange.xml', '2019-11-26 17:30:05',
        33, 'EXECUTED', '8:d41d8cd98f00b204e9800998ecf8427e', 'empty', '', NULL, '3.6.3', NULL, NULL, '4789403698'),
       ('20191115150721-1', 'jhipster', 'config/liquibase/changelog/20191115150721_added_entity_ExtendedUserFunds.xml',
        '2019-11-26 17:30:05', 34, 'EXECUTED', '8:3cd058765a325f8659b5cf11f9f04930',
        'createTable tableName=extended_user_funds; dropDefaultValue columnName=date_created, tableName=extended_user_funds',
        '', NULL, '3.6.3', NULL, NULL, '4789403698'),
       ('20191115150721-1-relations', 'jhipster',
        'config/liquibase/changelog/20191115150721_added_entity_ExtendedUserFunds.xml', '2019-11-26 17:30:05', 35,
        'EXECUTED', '8:d41d8cd98f00b204e9800998ecf8427e', 'empty', '', NULL, '3.6.3', NULL, NULL, '4789403698'),
       ('20190816135650-2', 'jhipster', 'config/liquibase/changelog/20190816135650_added_entity_constraints_Bank.xml',
        '2019-11-26 17:30:05', 36, 'EXECUTED', '8:79722d8c4559e96100928f29f007f6e9',
        'addForeignKeyConstraint baseTableName=bank, constraintName=fk_bank_country_id, referencedTableName=country',
        '', NULL, '3.6.3', NULL, NULL, '4789403698'),
       ('20190816135651-2', 'jhipster', 'config/liquibase/changelog/20190816135651_added_entity_constraints_Branch.xml',
        '2019-11-26 17:56:10', 37, 'EXECUTED', '8:d41d8cd98f00b204e9800998ecf8427e', 'empty', '', NULL, '3.6.3', NULL,
        NULL, '4790970279'),
       ('20190816135652-2', 'jhipster',
        'config/liquibase/changelog/20190816135652_added_entity_constraints_Currency.xml', '2019-11-26 17:56:10', 38,
        'EXECUTED', '8:7232682c08d894fa35ec0e2cacfd8955',
        'addForeignKeyConstraint baseTableName=currency, constraintName=fk_currency_country_id, referencedTableName=country',
        '', NULL, '3.6.3', NULL, NULL, '4790970279'),
       ('20190816135653-2', 'jhipster', 'config/liquibase/changelog/20190816135653_added_entity_constraints_City.xml',
        '2019-11-26 17:56:10', 39, 'EXECUTED', '8:a8988ce3dbe0ee19336d70d3eb4ade3c',
        'addForeignKeyConstraint baseTableName=city, constraintName=fk_city_country_id, referencedTableName=country',
        '', NULL, '3.6.3', NULL, NULL, '4790970279'),
       ('20190816135654-2', 'jhipster', 'config/liquibase/changelog/20190816135654_added_entity_constraints_IdType.xml',
        '2019-11-26 17:56:10', 40, 'EXECUTED', '8:a69ee9b9a7911521b1a3e2fa49eb0b02',
        'addForeignKeyConstraint baseTableName=id_type, constraintName=fk_id_type_country_id, referencedTableName=country',
        '', NULL, '3.6.3', NULL, NULL, '4790970279'),
       ('20190816135655-2', 'jhipster',
        'config/liquibase/changelog/20190816135655_added_entity_constraints_ExtendedUser.xml', '2019-11-26 17:56:10',
        41, 'EXECUTED', '8:eb6ed56e272ebabe7c4b2539b981100a',
        'addForeignKeyConstraint baseTableName=extended_user, constraintName=fk_extended_user_user_id, referencedTableName=jhi_user; addForeignKeyConstraint baseTableName=extended_user, constraintName=fk_extended_user_residence_city_id, referencedTableName...',
        '', NULL, '3.6.3', NULL, NULL, '4790970279'),
       ('20190816135656-2', 'jhipster',
        'config/liquibase/changelog/20190816135656_added_entity_constraints_Transaction.xml', '2019-11-26 17:56:11', 42,
        'EXECUTED', '8:77b51f1043795f5f68985ec3b25316a9',
        'addForeignKeyConstraint baseTableName=transaction, constraintName=fk_transaction_currency_id, referencedTableName=currency; addForeignKeyConstraint baseTableName=transaction, constraintName=fk_transaction_merchant_id, referencedTableName=extended_...',
        '', NULL, '3.6.3', NULL, NULL, '4790970279'),
       ('20190816135657-2', 'jhipster',
        'config/liquibase/changelog/20190816135657_added_entity_constraints_TransactionStatusChange.xml',
        '2019-11-26 17:56:11', 43, 'EXECUTED', '8:9de6f2aef9430f12478db70e380bbb43',
        'addForeignKeyConstraint baseTableName=transaction_status_change, constraintName=fk_transaction_status_change_transaction_id, referencedTableName=transaction; addForeignKeyConstraint baseTableName=transaction_status_change, constraintName=fk_transa...',
        '', NULL, '3.6.3', NULL, NULL, '4790970279'),
       ('20190816135658-2', 'jhipster',
        'config/liquibase/changelog/20190816135658_added_entity_constraints_Notification.xml', '2019-11-26 17:56:11',
        44, 'EXECUTED', '8:d46d086418fb3e49c9e4e7de6080dc21',
        'addForeignKeyConstraint baseTableName=notification, constraintName=fk_notification_sender_id, referencedTableName=extended_user; addForeignKeyConstraint baseTableName=notification, constraintName=fk_notification_receiver_id, referencedTableName=ex...',
        '', NULL, '3.6.3', NULL, NULL, '4790970279'),
       ('20190816135659-2', 'jhipster',
        'config/liquibase/changelog/20190816135659_added_entity_constraints_NotificationStatusChange.xml',
        '2019-11-26 17:56:11', 45, 'EXECUTED', '8:7ccbd6f83760004d9bb6b6e8082287b2',
        'addForeignKeyConstraint baseTableName=notification_status_change, constraintName=fk_notification_status_change_notification_id, referencedTableName=notification; addForeignKeyConstraint baseTableName=notification_status_change, constraintName=fk_n...',
        '', NULL, '3.6.3', NULL, NULL, '4790970279'),
       ('20190816135700-2', 'jhipster',
        'config/liquibase/changelog/20190816135700_added_entity_constraints_BankAccount.xml', '2019-11-26 17:56:11', 46,
        'EXECUTED', '8:0bb98310633fd6909317cc02e9ac430a',
        'addForeignKeyConstraint baseTableName=bank_account, constraintName=fk_bank_account_country_id, referencedTableName=country; addForeignKeyConstraint baseTableName=bank_account, constraintName=fk_bank_account_bank_id, referencedTableName=bank',
        '', NULL, '3.6.3', NULL, NULL, '4790970279'),
       ('20190816135701-2', 'jhipster',
        'config/liquibase/changelog/20190816135701_added_entity_constraints_ExtendedUserRelation.xml',
        '2019-11-26 17:56:11', 47, 'EXECUTED', '8:e035566083a2e4717af5d8ab119ecf10',
        'addForeignKeyConstraint baseTableName=extended_user_relation, constraintName=fk_extended_user_relation_extended_user_id, referencedTableName=extended_user; addForeignKeyConstraint baseTableName=extended_user_relation, constraintName=fk_extended_us...',
        '', NULL, '3.6.3', NULL, NULL, '4790970279'),
       ('20190926180154-2', 'jhipster',
        'config/liquibase/changelog/20190926180154_added_entity_constraints_TransactionGroup.xml',
        '2019-11-26 17:56:11', 48, 'EXECUTED', '8:7f47f7df5e3d59155199dd7498056893',
        'addForeignKeyConstraint baseTableName=transaction_group, constraintName=fk_transaction_group_bank_account_id, referencedTableName=bank_account; addForeignKeyConstraint baseTableName=transaction_group, constraintName=fk_transaction_group_admin_id, ...',
        '', NULL, '3.6.3', NULL, NULL, '4790970279'),
       ('20191105145255-2', 'jhipster',
        'config/liquibase/changelog/20191105145255_added_entity_constraints_ExtendedUserGroup.xml',
        '2019-11-26 17:56:11', 49, 'EXECUTED', '8:89bc8ddc2a7af3d1c123e6228771b4a0',
        'addForeignKeyConstraint baseTableName=extended_user_group, constraintName=fk_extended_user_group_admin_id, referencedTableName=extended_user; addForeignKeyConstraint baseTableName=extended_user_group, constraintName=fk_extended_user_group_bank_id,...',
        '', NULL, '3.6.3', NULL, NULL, '4790970279'),
       ('20191105191359-2', 'jhipster',
        'config/liquibase/changelog/20191105191359_added_entity_constraints_ExtendedUserStatusChange.xml',
        '2019-11-26 17:56:11', 50, 'EXECUTED', '8:49c96e3ffe47a6495229be6b3eb71ade',
        'addForeignKeyConstraint baseTableName=extended_user_status_change, constraintName=fk_extended_user_status_change_extended_user_id, referencedTableName=extended_user; addForeignKeyConstraint baseTableName=extended_user_status_change, constraintName...',
        '', NULL, '3.6.3', NULL, NULL, '4790970279'),
       ('20191115150721-2', 'jhipster',
        'config/liquibase/changelog/20191115150721_added_entity_constraints_ExtendedUserFunds.xml',
        '2019-11-26 17:56:11', 51, 'EXECUTED', '8:bb75ed043e41074edb1a4673be807196',
        'addForeignKeyConstraint baseTableName=extended_user_funds, constraintName=fk_extended_user_funds_extended_user_id, referencedTableName=extended_user; addForeignKeyConstraint baseTableName=extended_user_funds, constraintName=fk_extended_user_funds_...',
        '', NULL, '3.6.3', NULL, NULL, '4790970279'),
       ('20191230120443-1', 'jhipster',
        'config/liquibase/changelog/20191230120443_added_entity_NotificationReceiver.xml', '2020-01-23 19:35:30', 52,
        'EXECUTED', '8:ad22790bcfb962ca8ca817340c2f68c1',
        'createTable tableName=notification_receiver; dropDefaultValue columnName=date_created, tableName=notification_receiver; dropDefaultValue columnName=last_updated, tableName=notification_receiver',
        '', NULL, '3.6.3', NULL, NULL, '9808129752'),
       ('20191230120443-1-relations', 'jhipster',
        'config/liquibase/changelog/20191230120443_added_entity_NotificationReceiver.xml', '2020-01-23 19:35:30', 53,
        'EXECUTED', '8:d41d8cd98f00b204e9800998ecf8427e', 'empty', '', NULL, '3.6.3', NULL, NULL, '9808129752'),
       ('20191230175054-1', 'jhipster', 'config/liquibase/changelog/20191230175054_added_entity_DirectPaymentBank.xml',
        '2020-01-23 19:35:30', 54, 'EXECUTED', '8:b1cf55c3ed82a092267f753f388cfbcb',
        'createTable tableName=direct_payment_bank', '', NULL, '3.6.3', NULL, NULL, '9808129752'),
       ('20191230175054-1-relations', 'jhipster',
        'config/liquibase/changelog/20191230175054_added_entity_DirectPaymentBank.xml', '2020-01-23 19:35:30', 55,
        'EXECUTED', '8:d41d8cd98f00b204e9800998ecf8427e', 'empty', '', NULL, '3.6.3', NULL, NULL, '9808129752'),
       ('20191230120443-2', 'jhipster',
        'config/liquibase/changelog/20191230120443_added_entity_constraints_NotificationReceiver.xml',
        '2020-01-23 19:35:30', 56, 'EXECUTED', '8:15d318fae09b0200305fc319a8da369d',
        'addForeignKeyConstraint baseTableName=notification_receiver, constraintName=fk_notification_receiver_notification_id, referencedTableName=notification; addForeignKeyConstraint baseTableName=notification_receiver, constraintName=fk_notification_rec...',
        '', NULL, '3.6.3', NULL, NULL, '9808129752'),
       ('20191230175054-2', 'jhipster',
        'config/liquibase/changelog/20191230175054_added_entity_constraints_DirectPaymentBank.xml',
        '2020-01-23 19:35:30', 57, 'EXECUTED', '8:3d975eeb7a415576cf78695eefcda82c',
        'addForeignKeyConstraint baseTableName=direct_payment_bank, constraintName=fk_direct_payment_bank_origin_bank_id, referencedTableName=bank; addForeignKeyConstraint baseTableName=direct_payment_bank, constraintName=fk_direct_payment_bank_destiny_ban...',
        '', NULL, '3.6.3', NULL, NULL, '9808129752');
/*!40000 ALTER TABLE `DATABASECHANGELOG`
    ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2020-07-16 14:19:06
