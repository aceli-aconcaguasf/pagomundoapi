-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: ls-449eb857e6ae75cdb34254c1ef8b62ea8f7d95f1.cwxp9rzhaq1d.us-east-1.rds.amazonaws.com    Database: pagomundo
-- ------------------------------------------------------
-- Server version	5.7.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN = 0;

--
-- GTID state at the beginning of the backup
--

SET @@GLOBAL.GTID_PURGED = /*!80000 '+'*/ '';

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaction`
(
    `id`                       bigint(20)   NOT NULL AUTO_INCREMENT,
    `date_created`             datetime     NOT NULL,
    `last_updated`             datetime     NOT NULL,
    `status`                   varchar(255) NOT NULL,
    `status_reason`            longtext,
    `amount_before_commission` decimal(21, 2) DEFAULT NULL,
    `bank_commission`          float          DEFAULT NULL,
    `fx_commission`            float          DEFAULT NULL,
    `recharge_cost`            float          DEFAULT NULL,
    `amount_after_commission`  decimal(21, 2) DEFAULT NULL,
    `exchange_rate`            float          DEFAULT NULL,
    `amount_local_currency`    decimal(21, 2) DEFAULT NULL,
    `bank_reference`           varchar(255)   DEFAULT NULL,
    `file_name`                varchar(255)   DEFAULT NULL,
    `currency_id`              bigint(20)     DEFAULT NULL,
    `merchant_id`              bigint(20)     DEFAULT NULL,
    `payee_id`                 bigint(20)     DEFAULT NULL,
    `admin_id`                 bigint(20)     DEFAULT NULL,
    `bank_account_id`          bigint(20)     DEFAULT NULL,
    `description`              varchar(255)   DEFAULT NULL,
    `notes`                    longtext,
    `transaction_group_id`     bigint(20)     DEFAULT NULL,
    `info_changed`             blob,
    `must_notify`              tinyint(1)     DEFAULT '1',
    PRIMARY KEY (`id`),
    KEY `fk_transaction_currency_id` (`currency_id`),
    KEY `fk_transaction_merchant_id` (`merchant_id`),
    KEY `fk_transaction_admin_id` (`admin_id`),
    KEY `fk_transaction_bank_account_id` (`bank_account_id`),
    KEY `fk_transaction_transaction_group_id_index_5` (`transaction_group_id`),
    KEY `fk_transaction_payee_id` (`payee_id`),
    CONSTRAINT `fk_transaction_admin_id` FOREIGN KEY (`admin_id`) REFERENCES `extended_user` (`id`),
    CONSTRAINT `fk_transaction_bank_account_id` FOREIGN KEY (`bank_account_id`) REFERENCES `bank_account` (`id`),
    CONSTRAINT `fk_transaction_currency_id` FOREIGN KEY (`currency_id`) REFERENCES `currency` (`id`),
    CONSTRAINT `fk_transaction_merchant_id` FOREIGN KEY (`merchant_id`) REFERENCES `extended_user` (`id`),
    CONSTRAINT `fk_transaction_payee_id` FOREIGN KEY (`payee_id`) REFERENCES `extended_user` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 545
  DEFAULT CHARSET = utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction`
--

LOCK TABLES `transaction` WRITE;
/*!40000 ALTER TABLE `transaction`
    DISABLE KEYS */;
INSERT INTO `transaction`
VALUES (511, '2019-12-12 14:58:16', '2019-12-12 15:01:00', 'REJECTED', 'Reason for reject the payment', 100.00, 0.02,
        0.015, 2, 94.50, 100, 9450.00, NULL, NULL, 1, 95, 3178, 94, 1, 'Transaction description (optional)', NULL, 2,
        NULL, 1),
       (512, '2020-01-14 16:48:48', '2020-01-14 16:49:20', 'REJECTED', 'Not valid', 1000.00, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, 1, 3180, 3181, 3179, NULL, 'Nomina Enero', NULL, NULL, NULL, 1),
       (513, '2020-02-18 14:38:50', '2020-02-18 14:43:26', 'ACCEPTED', 'Bank approved', 4324.00, 0.02, 0.015, 2,
        4170.66, 3409, 14217780.00, NULL, NULL, 1, 3180, 3181, 3179, 2, 'Feb 18th', NULL, 3, '', 1),
       (514, '2020-02-20 17:31:05', '2020-04-16 14:42:21', 'ACCEPTED', '', 878.00, 0.01, 0.01, 2, 858.44, 3928,
        3371952.00, NULL, NULL, 1, 3180, 3181, 3179, 2, 'Pago Feb 20', NULL, 6, '', 1),
       (515, '2020-02-20 17:39:59', '2020-04-10 17:45:58', 'ACCEPTED', '', 110.00, 0, 0.015, 2, 106.35, 3400, 361590.00,
        NULL, NULL, 1, 3180, 3184, 3179, 2, 'Prueba Feb. 20', NULL, 4, '', 1),
       (516, '2020-03-04 20:45:08', '2020-04-16 14:49:21', 'ACCEPTED', '', 120.00, 0.01, 0.01, 2, 115.60, 3928,
        454076.81, NULL, NULL, 1, 3180, 3184, 3179, 2, '', NULL, 7, '', 1),
       (517, '2020-03-04 20:45:09', '2020-04-16 14:42:18', 'ACCEPTED', '', 135.00, 0.01, 0.01, 2, 130.30, 3928,
        511818.34, NULL, NULL, 1, 3180, 3181, 3179, 2, '', NULL, 6, '', 1),
       (518, '2020-03-23 19:27:04', '2020-04-01 18:03:56', 'ACCEPTED', '', 130.00, 0, 0.015, 2, 126.05, 4285, 540124.25,
        NULL, NULL, 1, 3180, 3186, 3179, 2, '', NULL, 5, '', 1),
       (519, '2020-03-30 14:47:55', '2020-04-01 18:03:42', 'REJECTED', 'test', 540.00, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, 1, 3180, 3181, 3179, NULL, 'March 30 2020', NULL, NULL, '', 1),
       (520, '2020-03-30 14:47:56', '2020-04-01 18:03:37', 'REJECTED', 'Test', 348.00, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, 1, 3180, 3184, 3179, NULL, 'March 30 2020', NULL, NULL, '', 1),
       (521, '2020-04-16 14:35:07', '2020-04-16 14:49:23', 'ACCEPTED', '', 240.00, 0.01, 0.01, 2, 233.20, 3928,
        916009.63, NULL, NULL, 1, 3180, 3191, 3179, 2, 'Nomina Abril 15', NULL, 7, '', 1),
       (522, '2020-04-16 14:35:07', '2020-04-16 14:42:47', 'REJECTED', 'cuenta invalida', 345.00, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, 1, 3180, 3184, 3179, NULL, 'Nomina Abril 15', NULL, NULL, '', 1),
       (523, '2020-04-24 15:37:03', '2020-04-24 18:19:44', 'ACCEPTED', '\n', 1850.00, 0.01, 0.01, 2, 1811.00, 3957,
        7166127.00, NULL, NULL, 1, 3180, 3185, 3179, 2, '', NULL, 8, '', 1),
       (524, '2020-04-24 15:37:03', '2020-04-24 18:19:47', 'ACCEPTED', '', 1850.00, 0.01, 0.01, 2, 1811.00, 3957,
        7166127.00, NULL, NULL, 1, 3180, 3186, 3179, 2, '', NULL, 8, '', 1),
       (525, '2020-04-29 14:57:48', '2020-04-29 19:32:45', 'REJECTED', 'Cuenta bloqueada', 4400.00, 0.02, 0.01, 2,
        4266.00, 3990, 17021340.00, NULL, NULL, 1, 3180, 3191, 3179, 2, '', NULL, 9, '', 1),
       (526, '2020-04-30 13:59:19', '2020-05-05 15:17:03', 'ACCEPTED', '', 4400.00, 0.02, 0.01, 2, 4266.00, 3920,
        16722720.00, NULL, NULL, 1, 3180, 3194, 3179, 2, '', NULL, 10, '', 1),
       (527, '2020-05-06 13:54:52', '2020-05-06 21:06:31', 'ACCEPTED', '', 500.00, 0.01, 0.02, 3, 482.00, 3943,
        1900526.00, NULL, NULL, 1, 3190, 3193, 3179, 2, '1', NULL, 11, '', 1),
       (528, '2020-05-20 20:43:02', '2020-05-20 21:57:46', 'ACCEPTED', '', 300.00, 0.02, 0.01, 2, 289.00, 3813,
        1101957.00, NULL, NULL, 1, 3180, 3191, 3179, 2, '', NULL, 12, '', 1),
       (529, '2020-06-03 16:20:23', '2020-06-03 23:36:39', 'ACCEPTED', '', 5477.07, 0.03, 0.005, 0, 5285.37, 3990,
        21088636.00, NULL, NULL, 1, 3197, 3199, 3179, 2, 'imlive_03062020_S_72888', NULL, 13, '', 1),
       (530, '2020-06-09 21:15:28', '2020-06-09 22:19:30', 'ACCEPTED', '', 2100.00, 0.02, 0.01, 2, 2035.00, 3650,
        7427750.00, NULL, NULL, 1, 3180, 3186, 3179, 2, '', NULL, 14, '', 1),
       (531, '2020-06-09 21:15:28', '2020-06-09 22:19:27', 'ACCEPTED', '', 2100.00, 0.02, 0.01, 2, 2035.00, 3650,
        7427750.00, NULL, NULL, 1, 3180, 3185, 3179, 2, '', NULL, 14, '', 1),
       (532, '2020-06-11 15:38:49', '2020-06-11 20:03:43', 'ACCEPTED', '', 100.00, 0.02, 0.01, 2, 95.00, 3754,
        356630.00, NULL, NULL, 1, 3200, 3201, 3179, 2, '1', NULL, 15, '', 1),
       (533, '2020-06-12 18:08:10', '2020-06-15 00:50:04', 'ACCEPTED', '', 400.13, 0.02, 0.01, 2, 386.13, 3750,
        1447972.88, NULL, NULL, 1, 3200, 3201, 3179, 2, '', NULL, 16, '', 1),
       (534, '2020-06-13 14:30:24', '2020-06-16 19:07:55', 'ACCEPTED', '', 909.21, 0.02, 0.01, 2, 879.93, 3751,
        3300631.25, NULL, NULL, 1, 3200, 3202, 3179, 2, '', NULL, 17, '', 1),
       (535, '2020-06-19 17:14:32', '2020-06-23 15:01:38', 'ACCEPTED', '', 1113.18, 0.02, 0.01, 2, 1077.78, 3743,
        4034148.00, NULL, NULL, 1, 3200, 3202, 3179, 2, '', NULL, 18, '', 1),
       (536, '2020-06-19 17:14:33', '2020-06-23 15:01:42', 'ACCEPTED', '', 718.37, 0.02, 0.01, 2, 694.82, 3743,
        2600707.25, NULL, NULL, 1, 3200, 3201, 3179, 2, '', NULL, 18, '', 1),
       (537, '2020-06-26 23:44:15', '2020-06-30 15:24:16', 'ACCEPTED', '', 1029.39, 0.02, 0.01, 2, 996.51, 3740,
        3726941.25, NULL, NULL, 1, 3200, 3202, 3179, 2, '', NULL, 19, '', 1),
       (538, '2020-06-26 23:44:16', '2020-06-30 15:24:12', 'ACCEPTED', '', 430.38, 0.02, 0.01, 2, 415.47, 3740,
        1553852.50, NULL, NULL, 1, 3200, 3201, 3179, 2, '', NULL, 19, '', 1),
       (539, '2020-07-03 19:11:08', '2020-07-06 19:10:37', 'ACCEPTED', '', 1267.28, 0.02, 0.01, 2, 1227.26, 3620,
        4442687.00, NULL, NULL, 1, 3200, 3202, 3179, 2, '', NULL, 20, '', 1),
       (540, '2020-07-03 19:11:09', '2020-07-06 19:10:41', 'ACCEPTED', '', 701.47, 0.02, 0.01, 2, 678.43, 3620,
        2455901.50, NULL, NULL, 1, 3200, 3201, 3179, 2, '', NULL, 20, '', 1),
       (541, '2020-07-08 20:32:39', '2020-07-09 17:08:35', 'ACCEPTED', '', 2425.00, 0.02, 0.01, 2, 2350.25, 3600,
        8460900.00, NULL, NULL, 1, 3180, 3186, 3179, 2, '', NULL, 21, '', 1),
       (542, '2020-07-08 20:32:41', '2020-07-09 17:08:38', 'ACCEPTED', '', 2425.00, 0.02, 0.01, 2, 2350.25, 3600,
        8460900.00, NULL, NULL, 1, 3180, 3185, 3179, 2, '', NULL, 21, '', 1),
       (543, '2020-07-10 22:14:12', '2020-07-13 16:41:09', 'ACCEPTED', '', 757.32, 0.02, 0.01, 2, 732.60, 3600,
        2637361.50, NULL, NULL, 1, 3200, 3202, 3179, 2, '', NULL, 22, '', 1),
       (544, '2020-07-10 22:14:13', '2020-07-13 16:41:12', 'ACCEPTED', '', 2324.24, 0.02, 0.01, 2, 2252.51, 3600,
        8109045.50, NULL, NULL, 1, 3200, 3201, 3179, 2, '', NULL, 22, '', 1);
/*!40000 ALTER TABLE `transaction`
    ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2020-07-16 14:17:18
