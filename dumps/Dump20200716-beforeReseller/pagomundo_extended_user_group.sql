-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: ls-449eb857e6ae75cdb34254c1ef8b62ea8f7d95f1.cwxp9rzhaq1d.us-east-1.rds.amazonaws.com    Database: pagomundo
-- ------------------------------------------------------
-- Server version	5.7.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN = 0;

--
-- GTID state at the beginning of the backup
--

SET @@GLOBAL.GTID_PURGED = /*!80000 '+'*/ '';

--
-- Table structure for table `extended_user_group`
--

DROP TABLE IF EXISTS `extended_user_group`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `extended_user_group`
(
    `id`                  bigint(20) NOT NULL AUTO_INCREMENT,
    `date_created`        datetime     DEFAULT NULL,
    `status`              varchar(255) DEFAULT NULL,
    `file_name`           varchar(255) DEFAULT NULL,
    `file_name_from_bank` varchar(255) DEFAULT NULL,
    `admin_id`            bigint(20)   DEFAULT NULL,
    `bank_id`             bigint(20)   DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_extended_user_group_admin_id` (`admin_id`),
    KEY `fk_extended_user_group_bank_id` (`bank_id`),
    CONSTRAINT `fk_extended_user_group_admin_id` FOREIGN KEY (`admin_id`) REFERENCES `extended_user` (`id`),
    CONSTRAINT `fk_extended_user_group_bank_id` FOREIGN KEY (`bank_id`) REFERENCES `bank` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 534
  DEFAULT CHARSET = utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extended_user_group`
--

LOCK TABLES `extended_user_group` WRITE;
/*!40000 ALTER TABLE `extended_user_group`
    DISABLE KEYS */;
INSERT INTO `extended_user_group`
VALUES (529, '2019-12-12 14:14:13', 'ACCEPTED',
        'fileBucket/extendedUser/exportedFiles/2019_12_12_14_14_13_00-Pichincha-Colombia.xlsx', NULL, 94, 1),
       (530, '2020-01-14 16:43:13', 'ACCEPTED',
        'fileBucket/extendedUser/exportedFiles/2020_01_14_16_43_13_00-Pichincha-Colombia.xlsx', NULL, 3179, 1),
       (531, '2020-02-20 17:36:30', 'ACCEPTED',
        'fileBucket/extendedUser/exportedFiles/2020_02_20_17_36_30_00-Itau-Colombia.xlsx', NULL, 3179, 2),
       (532, '2020-03-12 20:06:56', 'ACCEPTED',
        'fileBucket/extendedUser/exportedFiles/2020_03_12_20_06_56_00-Itau-Colombia.xlsx', NULL, 3179, 2),
       (533, '2020-03-18 19:31:41', 'ACCEPTED',
        'fileBucket/extendedUser/exportedFiles/2020_03_18_19_31_41_00-Itau-Colombia.xlsx', NULL, 3179, 2);
/*!40000 ALTER TABLE `extended_user_group`
    ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2020-07-16 14:16:23
