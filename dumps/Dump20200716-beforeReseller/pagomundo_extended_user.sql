-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: ls-449eb857e6ae75cdb34254c1ef8b62ea8f7d95f1.cwxp9rzhaq1d.us-east-1.rds.amazonaws.com    Database: pagomundo
-- ------------------------------------------------------
-- Server version	5.7.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN = 0;

--
-- GTID state at the beginning of the backup
--

SET @@GLOBAL.GTID_PURGED = /*!80000 '+'*/ '';

--
-- Table structure for table `extended_user`
--

DROP TABLE IF EXISTS `extended_user`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `extended_user`
(
    `id`                        bigint(20) NOT NULL AUTO_INCREMENT,
    `status`                    varchar(255)   DEFAULT NULL,
    `last_name_1`               varchar(255)   DEFAULT NULL,
    `last_name_2`               varchar(255)   DEFAULT NULL,
    `first_name_1`              varchar(255)   DEFAULT NULL,
    `first_name_2`              varchar(255)   DEFAULT NULL,
    `full_name`                 varchar(255)   DEFAULT NULL,
    `id_number`                 varchar(255)   DEFAULT NULL,
    `gender`                    varchar(255)   DEFAULT NULL,
    `marital_status`            varchar(255)   DEFAULT NULL,
    `residence_address`         varchar(255)   DEFAULT NULL,
    `postal_address`            varchar(255)   DEFAULT NULL,
    `phone_number`              varchar(255)   DEFAULT NULL,
    `mobile_number`             varchar(255)   DEFAULT NULL,
    `birth_date`                datetime       DEFAULT NULL,
    `date_created`              datetime       DEFAULT NULL,
    `last_updated`              datetime       DEFAULT NULL,
    `email`                     varchar(255)   DEFAULT NULL,
    `company`                   varchar(255)   DEFAULT NULL,
    `image_id_url`              varchar(255)   DEFAULT NULL,
    `image_address_url`         varchar(255)   DEFAULT NULL,
    `balance`                   decimal(21, 2) DEFAULT NULL,
    `user_id`                   bigint(20)     DEFAULT NULL,
    `residence_city_id`         bigint(20)     DEFAULT NULL,
    `postal_city_id`            bigint(20)     DEFAULT NULL,
    `id_type_id`                bigint(20)     DEFAULT NULL,
    `tax_id`                    varchar(255)   DEFAULT NULL,
    `extended_user_group_id`    bigint(20)     DEFAULT NULL,
    `status_reason`             longtext,
    `role`                      varchar(255)   DEFAULT NULL,
    `card_number`               varchar(255)   DEFAULT NULL,
    `bank_id`                   bigint(20)     DEFAULT NULL,
    `residence_country_id`      bigint(20)     DEFAULT NULL,
    `postal_country_id`         bigint(20)     DEFAULT NULL,
    `bank_commission`           float          DEFAULT NULL,
    `fx_commission`             float          DEFAULT NULL,
    `recharge_cost`             float          DEFAULT NULL,
    `use_merchant_commission`   tinyint(1)     DEFAULT '0',
    `bank_account_number`       varchar(255)   DEFAULT NULL,
    `direct_payment_bank_id`    bigint(20)     DEFAULT NULL,
    `direct_payment_city_id`    bigint(20)     DEFAULT NULL,
    `use_direct_payment`        tinyint(1)     DEFAULT '0',
    `bank_account_type`         varchar(255)   DEFAULT NULL,
    `can_change_payment_method` tinyint(1)     DEFAULT '1',
    `must_notify`               tinyint(1)     DEFAULT '1',
    `profile_info_changed`      blob,
    PRIMARY KEY (`id`),
    UNIQUE KEY `ux_extended_user_user_id` (`user_id`),
    KEY `fk_extended_user_residence_city_id` (`residence_city_id`),
    KEY `fk_extended_user_postal_city_id` (`postal_city_id`),
    KEY `fk_extended_user_id_type_id` (`id_type_id`),
    KEY `fk_extended_user_extended_user_group_id` (`extended_user_group_id`),
    KEY `fk_extended_user_bank_id_id` (`bank_id`),
    KEY `fk_extended_user_residence_country_id` (`residence_country_id`),
    KEY `fk_extended_user_postal_country_id` (`postal_country_id`),
    KEY `fk_extended_user_direct_payment_bank_id` (`direct_payment_bank_id`),
    KEY `fk_extended_user_direct_payment_city_id` (`direct_payment_city_id`),
    CONSTRAINT `fk_extended_user_bank_id` FOREIGN KEY (`bank_id`) REFERENCES `bank` (`id`),
    CONSTRAINT `fk_extended_user_direct_payment_bank_id` FOREIGN KEY (`direct_payment_bank_id`) REFERENCES `bank` (`id`),
    CONSTRAINT `fk_extended_user_direct_payment_city_id` FOREIGN KEY (`direct_payment_city_id`) REFERENCES `city` (`id`),
    CONSTRAINT `fk_extended_user_id_type_id` FOREIGN KEY (`id_type_id`) REFERENCES `id_type` (`id`),
    CONSTRAINT `fk_extended_user_postal_city_id` FOREIGN KEY (`postal_city_id`) REFERENCES `city` (`id`),
    CONSTRAINT `fk_extended_user_postal_country_id` FOREIGN KEY (`postal_country_id`) REFERENCES `country` (`id`),
    CONSTRAINT `fk_extended_user_residence_city_id` FOREIGN KEY (`residence_city_id`) REFERENCES `city` (`id`),
    CONSTRAINT `fk_extended_user_residence_country_id` FOREIGN KEY (`residence_country_id`) REFERENCES `country` (`id`),
    CONSTRAINT `fk_extended_user_user_id` FOREIGN KEY (`user_id`) REFERENCES `jhi_user` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 3204
  DEFAULT CHARSET = utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extended_user`
--

LOCK TABLES `extended_user` WRITE;
/*!40000 ALTER TABLE `extended_user`
    DISABLE KEYS */;
INSERT INTO `extended_user`
VALUES (93, 'ACCEPTED', 'Superadmin', NULL, 'Superadmin', NULL, 'Superadmin Superadmin', NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, '2019-11-13 17:16:51', '2019-11-13 18:47:55', 'agucaliri@gmail.com', NULL, NULL, NULL, 0.00,
        5, NULL, NULL, NULL, NULL, NULL, NULL, 'ROLE_SUPER_ADMIN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL,
        NULL, NULL, 0, NULL, 1, 1, NULL),
       (94, 'ACCEPTED', 'Admin', NULL, 'Agu', NULL, 'Agu Admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        '2019-11-13 18:52:29', '2020-02-28 11:42:16', 'a.gucaliri@gmail.com', NULL, NULL, NULL, 0.00, 105, NULL, NULL,
        NULL, NULL, NULL, NULL, 'ROLE_ADMIN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, 1,
        0, ''),
       (95, 'ACCEPTED', 'Merchant', NULL, 'Agu', NULL, 'Agu Merchant', '123', NULL, NULL, 'ASF 123', 'ASF 123', NULL,
        '261123123', '2019-11-30 00:00:00', '2019-11-13 18:57:37', '2020-04-16 15:41:26', 'ag.ucaliri@gmail.com', 'ASF',
        NULL, NULL, 100.00, 106, 40, 14, NULL, 'ASF 123', NULL, 'Test sending email to smtp port 587', 'ROLE_MERCHANT',
        NULL, NULL, 66, 66, 0.02, 0.015, 2, 1, NULL, NULL, NULL, 0, NULL, 1, 0, ''),
       (3178, 'CANCELLED', 'Caliri', 'Caliri', 'Agu', 'Agu', 'Agu Caliri', '123654', 'MALE', 'MARRIED', 'Address 123',
        'Address 123', '123654789', '123654789', '1974-03-16 00:00:00', '2019-12-12 14:08:48', '2020-03-18 19:41:18',
        'agu.caliri@gmail.com', NULL, NULL, NULL, 0.00, 8395, 149, 149, 1, '123654789', 529, 'User de prueba',
        'ROLE_PAYEE', '1234567891234657', 1, 1, 1, NULL, NULL, NULL, 0, '64801681861681', 11, 293, 1, 'CTE', 1, 1, ''),
       (3179, 'ACCEPTED', 'Pereira', NULL, 'Alex', NULL, 'Alex Pereira', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        '2020-01-14 16:23:32', '2020-01-14 16:23:32', 'alex@pmi-americas.com', NULL, NULL, NULL, 0.00, 8396, NULL, NULL,
        NULL, NULL, NULL, NULL, 'ROLE_ADMIN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, 1,
        1, NULL),
       (3180, 'ACCEPTED', 'Giraldo', NULL, 'Alba', NULL, 'Alba Giraldo', '1234567', NULL, NULL, '4700 Lake Rd | Miami',
        '4700 Lake Rd | Miami', NULL, '3107173343', '1977-02-08 00:00:00', '2020-01-14 16:27:55', '2020-07-08 20:32:41',
        'alex@danalex.org', 'Danalex Properties', NULL, NULL, 6950.00, 8397, NULL, NULL, NULL, '47-13134222', NULL,
        NULL, 'ROLE_MERCHANT', NULL, NULL, 120, 120, 0.02, 0.01, 2, 1, NULL, NULL, NULL, 0, NULL, 1, 0, ''),
       (3181, 'ACCEPTED', 'Pereira', 'Wild', 'Axel', 'Mateo', 'Axel Pereira', '36761616', 'MALE', 'SINGLE',
        '4700 Lake Rd', '4700 Lake Rd', '3107173343', '3107173343', '1980-01-29 00:00:00', '2020-01-14 16:42:17',
        '2020-01-14 16:45:44', 'paraisoalex@gmail.com', NULL, NULL, NULL, 0.00, 8398, 149, 149, 1, '', 530, '',
        'ROLE_PAYEE', '1234566778889999', 1, 1, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, 1, 1, NULL),
       (3182, 'ACCEPTED', 'Monroy', NULL, 'Margarita', NULL, 'Margarita Monroy', '502637982', NULL, NULL,
        '5710 Maggiore St. | Miami', '5710 Maggiore St. | Miami', NULL, '+1 786 553-1557', '1973-01-21 00:00:00',
        '2020-02-05 22:16:05', '2020-02-10 15:11:07', 'margarita.monroy@controlbox.net', 'ControlBox Corp.', NULL, NULL,
        0.00, 8399, NULL, NULL, NULL, '20-0674874', NULL, NULL, 'ROLE_MERCHANT', NULL, NULL, 120, 120, NULL, NULL, NULL,
        0, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
       (3183, 'ACCEPTED', 'Bogota', NULL, 'Oficina', NULL, 'Oficina Bogota', NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, '2020-02-20 17:07:17', '2020-02-20 17:07:17', 'oficinabogota@pmi-americas.com', NULL, NULL, NULL, 0.00,
        8402, NULL, NULL, NULL, NULL, NULL, NULL, 'ROLE_ADMIN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL,
        NULL, NULL, NULL, NULL, 1, ''),
       (3184, 'ACCEPTED', 'Clavijo', 'Traslavina', 'Bridgeth', 'Tatiana', 'Bridgeth Clavijo', '1101760465', 'FEMALE',
        'SINGLE', 'Carrera 48 174b 07', 'Carrera 48 174b 07', '3213773032', '3213773032', '1998-09-24 00:00:00',
        '2020-02-20 17:35:48', '2020-02-20 17:38:38', 'bridgethtatiana@hotmail.es', NULL, NULL, NULL, 0.00, 8403, 149,
        149, 1, '', 531, NULL, 'ROLE_PAYEE', NULL, 2, 1, 1, NULL, NULL, NULL, 0, '085326346', 5, 149, 1, 'AHO', 1, 1,
        ''),
       (3185, 'ACCEPTED', 'Djurovic', 'Alzate', 'Maria Teresa', 'Teresa', 'Maria Teresa Djurovic', '24314170', 'FEMALE',
        'WIDOWER', 'Kra 23B #64-66 apto 401', 'Kra 23B #64-66 apto 401', '', '3155412242', '1953-07-15 04:00:00',
        '2020-03-12 20:00:42', '2020-04-18 00:44:29', 'alex@paraisopictures.com', NULL, NULL, NULL, 0.00, 8405, 319,
        319, 1, '', 532, '', 'ROLE_PAYEE', '6279500140008339', 2, 1, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL,
        1, 1, ''),
       (3186, 'ACCEPTED', 'Barrientos', 'Alvarez', 'Mauricio', 'Mauricio', 'Mauricio Barrientos', '10255336', 'MALE',
        'MARRIED', 'Calle 72 A No. 28 B 35 Casa 2', 'Calle 72 A No. 28 B 35 Casa 2', '+578872173', '3103744503',
        '1962-03-26 05:00:00', '2020-03-18 16:49:50', '2020-04-24 15:40:09', 'mauriciobarrientos1@hotmail.com', NULL,
        NULL, NULL, 0.00, 8406, 319, 319, 1, '', 533, '', 'ROLE_PAYEE', '6279500140008340', 2, 1, 1, NULL, NULL, NULL,
        0, '086170187388', 17, 319, 0, 'AHO', 0, 1, _binary 'Payment method changed'),
       (3187, 'CANCELLED', 'Perez', 'Garcia', 'Juan', 'Ernesto', 'Juan Perez', '2131221', 'MALE', 'MARRIED',
        'Beltran 485', 'Beltran 485', '', '261678008', '1982-05-25 03:00:00', '2020-03-18 19:39:48',
        '2020-03-18 19:44:04', 'agucaliri+jp@gmail.com', NULL, NULL, NULL, 0.00, 8407, 319, 319, 1, '', NULL,
        'User de prueba', 'ROLE_PAYEE', NULL, NULL, 1, 1, NULL, NULL, NULL, 0, '74854756', 17, 319, 1, 'AHO', 1, 1, ''),
       (3188, 'ACCEPTED', 'Bayr', NULL, 'Benjamin', NULL, 'Benjamin Bayr', NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, '2020-03-30 13:50:23', '2020-03-30 13:50:23', 'benjamin.bayr@processingpartners.com', NULL, NULL, NULL,
        0.00, 8408, NULL, NULL, NULL, NULL, NULL, NULL, 'ROLE_ADMIN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL,
        NULL, NULL, 0, NULL, 0, 1, ''),
       (3189, 'ACCEPTED', 'Caliri', NULL, 'Agustín', NULL, 'Agustín Caliri', NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, '2020-03-30 14:39:49', '2020-03-30 14:39:49', 'agucaliri+ac@gmail.com', NULL, NULL, NULL, 0.00, 8409,
        NULL, NULL, NULL, NULL, NULL, NULL, 'ROLE_ADMIN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL,
        0, NULL, 0, 1, ''),
       (3190, 'ACCEPTED', 'Young', NULL, 'Robert', NULL, 'Robert Young', '498660135', NULL, NULL,
        '12681 N. Stonebrook Cir. | Davie', '2 Biscayne Blvd. | Miami', NULL, '13054699313', '1974-05-08 04:00:00',
        '2020-04-01 18:03:10', '2020-05-06 13:54:52', 'jeffyoung7@gmail.com', 'Nobetek LLC', NULL, NULL, 0.00, 8410,
        NULL, NULL, NULL, '20-4114825', NULL, NULL, 'ROLE_MERCHANT', NULL, NULL, 120, 120, 0.01, 0.02, 3, 1, NULL, NULL,
        NULL, 0, NULL, 0, 0, ''),
       (3191, 'ACCEPTED', 'Diaz', 'Medina', 'Santiago', 'O', 'Santiago Diaz', '80421955', 'MALE', 'SINGLE',
        '4700 lake road', 'Calle Matuna', '3232247404', '2132978885', '1971-07-08 05:00:00', '2020-04-15 18:32:35',
        '2020-04-29 20:43:15', 'oso@oso.media', NULL, NULL, NULL, 0.00, 8412, 150, 150, 1, '', NULL, NULL, 'ROLE_PAYEE',
        NULL, NULL, 1, 1, NULL, NULL, NULL, 0, '056670266529', 17, 150, 1, 'AHO', 1, 0, ''),
       (3192, 'ACCEPTED', 'Payee', 'payee', 'Test', 'test', 'Test Payee', '33222333', 'FEMALE', 'SINGLE', 'España 698',
        'España 698', '549261444444', '549261444443', '1986-08-24 03:00:00', '2020-04-22 14:33:10',
        '2020-04-22 14:33:10', 'mai.aracercasi+pp@gmail.com', NULL, NULL, NULL, 0.00, 8413, 126, 126, 1, '123', NULL,
        NULL, 'ROLE_PAYEE', NULL, NULL, 1, 1, NULL, NULL, NULL, 0, '00111122444', 7, 1, 1, 'AHO', 1, 1, ''),
       (3193, 'ACCEPTED', 'Santana Martinez', 'Martinez', 'Adys', 'Deilena', 'Adys Santana Martinez', '43924041',
        'FEMALE', 'SINGLE', 'Calle 62D #98a29 Robledo Porvenir', 'Calle 62D #98a29 Robledo Porvenir', '302 4286499',
        '302 4286499', '1985-01-08 05:00:00', '2020-04-28 17:04:12', '2020-04-28 17:04:12', 'adissanta@gmail.com', NULL,
        NULL, NULL, 0.00, 8411, 1, 1, 1, '', NULL, NULL, 'ROLE_PAYEE', NULL, NULL, 1, 1, NULL, NULL, NULL, 0,
        '00165120311', 7, 1, 1, 'AHO', 1, 1, ''),
       (3194, 'ACCEPTED', 'Diaz', 'Medina', 'Maria Lucia', 'Lucia', 'Maria Lucia Diaz', '52251922', 'FEMALE',
        'FREE_UNION', 'cra. 14 #108-18', 'cra. 14 #108-18', '3153540219', '3153540219', '1974-09-26 05:00:00',
        '2020-04-29 20:46:22', '2020-04-29 20:46:22', 'marialuciadiaz@gmail.com', NULL, NULL, NULL, 0.00, 8414, 149,
        149, 1, '', NULL, NULL, 'ROLE_PAYEE', NULL, NULL, 1, 1, NULL, NULL, NULL, 0, '005770159928', 17, 149, 1, 'AHO',
        1, 1, ''),
       (3195, 'ACCEPTED', 'Processing Partners', NULL, 'Risk', NULL, 'Risk Processing Partners', NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, '2020-04-30 13:53:13', '2020-04-30 13:53:13', 'risk@processingpartners.com', NULL, NULL,
        NULL, 0.00, 8415, NULL, NULL, NULL, NULL, NULL, NULL, 'ROLE_ADMIN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0,
        NULL, NULL, NULL, 0, NULL, 0, 1, ''),
       (3196, 'ACCEPTED', 'Studios', 'Corp', 'AG', 'G', 'AG Studios', '79793619', 'MALE', 'SINGLE', 'Calle 71 #1-18 ',
        'Calle 71 #1-18 ', '3107173343', '3107173343', '2009-01-01 05:00:00', '2020-05-05 15:41:40',
        '2020-05-05 15:41:40', 'crm@pmi-americas.com', NULL, NULL, NULL, 0.00, 8416, 149, 149, 1, '79793617', NULL,
        NULL, 'ROLE_PAYEE', NULL, NULL, 1, 1, NULL, NULL, NULL, 0, '12345678', 5, 149, 1, 'AHO', 1, 1, ''),
       (3197, 'ACCEPTED', 'Foster', NULL, 'Stuart', NULL, 'Stuart Foster', '547199440', NULL, NULL,
        'Attic Porta B Edificio Closa del Rull, Esc B Av Jovell 6 Sispony La Massana AD 400  | La Massana ',
        'Sant Antoni Avenue, Burgués builing, 3rd floor, No. 3-A and 3-B, La Massana, Andorra | La Massana', NULL,
        '00376 646208', '1970-06-25 23:00:00', '2020-05-27 14:21:54', '2020-06-03 16:20:24', 'stuart@iml.ad',
        'I.M.L., SLU', NULL, NULL, 879.93, 8417, NULL, NULL, NULL, 'L-707933-N', NULL, NULL, 'ROLE_MERCHANT', NULL,
        NULL, 201, 201, 0.03, 0.005, 0, 1, NULL, NULL, NULL, 0, NULL, 0, 0, ''),
       (3198, 'ACCEPTED', 'Flores Torres', NULL, 'Yokebed', NULL, 'Yokebed Flores Torres', '06220017111', NULL, NULL,
        'Ed. Terratorta Bloc b 5 1a Ordino | Andorra',
        'Sant Antoni Avenue, Burgués builing, 3rd floor, No. 3-A and 3-B, La Massana, Andorra | Andorra', NULL,
        '+376 653 790', '1982-03-31 22:00:00', '2020-05-30 04:44:17', '2020-06-04 20:02:31', 'yoke@iml.ad',
        'I.M.L., SLU', NULL, NULL, 879.93, 8418, NULL, NULL, NULL, 'L-707933-N', NULL, NULL, 'ROLE_MERCHANT', NULL,
        NULL, 201, 201, 0.03, 0.005, 0, 1, NULL, NULL, NULL, 0, NULL, 0, 0, ''),
       (3199, 'ACCEPTED', 'López Bohorquez', NULL, 'Yuli Jineth', NULL, 'Yuli Jineth López Bohorquez', '1032362366',
        NULL, NULL, 'calle 49 b No. 64c 54 (1303) | medellin', 'cra 91 No. 44 67 (202)', '3112190530', '3112190530',
        '2018-09-21 14:48:08', '2020-06-03 14:57:51', '2020-06-03 14:57:51', 'studioyjlb@gmail.com', 'Amatista Studio',
        NULL, NULL, 0.00, 8419, NULL, 1, 16, '1032362366-0', NULL, NULL, 'ROLE_PAYEE', NULL, NULL, 1, 1, NULL, NULL,
        NULL, 0, '69048230245', 7, 1, 1, 'AHO', 0, 1, ''),
       (3200, 'ACCEPTED', 'Shulmister', NULL, 'Meryl', NULL, 'Meryl Shulmister', '66-0896957', NULL, NULL,
        '542 Ave. Escorial Urb Caparra Heights  | San Juan', '542 Ave Escorial Urb Caparra Heights | San Juan', NULL,
        '3054699313', '1981-03-10 05:00:00', '2020-06-03 15:00:52', '2020-07-15 15:50:46', 'management@celeb.tv',
        'MCI LLC', NULL, NULL, 10121.03, 8421, NULL, NULL, NULL, '66-0896957', NULL, NULL, 'ROLE_MERCHANT', NULL, NULL,
        102, 102, 0.02, 0.01, 2, 1, NULL, NULL, NULL, 0, NULL, 0, 0, ''),
       (3201, 'ACCEPTED', 'Lopera Agudelo', 'agudelo', 'Jose David', 'david', 'Jose David Lopera Agudelo', '1037644466',
        'MALE', 'SINGLE', 'calle 16b # 29/155', 'calle 16b # 29/155', '', '+573052383826', '1995-08-27 05:00:00',
        '2020-06-11 14:37:21', '2020-06-11 14:37:21', 'jose.lopera@colegiatura.edu.co', NULL, NULL, NULL, 0.00, 8424, 1,
        1, 1, '', NULL, NULL, 'ROLE_PAYEE', NULL, NULL, 1, 1, NULL, NULL, NULL, 0, '61149225349', 7, 1, 1, 'AHO', 1, 1,
        ''),
       (3202, 'ACCEPTED', 'Rincón Gaviria', 'Gaviria', 'Andrés Felipe', 'Felipe', 'Andrés Felipe Rincón Gaviria',
        '1036635468', 'MALE', 'FREE_UNION', '055460', '055460', '3158351090', '3158351090', '1991-04-13 05:00:00',
        '2020-06-13 13:14:31', '2020-06-13 13:14:31', 'andresrg2907@gmail.com', NULL, NULL, NULL, 0.00, 8429, 64, 64, 1,
        '', NULL, NULL, 'ROLE_PAYEE', NULL, NULL, 1, 1, NULL, NULL, NULL, 0, '01572076772', 7, 1, 1, 'AHO', 1, 1, ''),
       (3203, 'ACCEPTED', 'Tejada Guerrero', NULL, 'Henry', NULL, 'Henry Tejada Guerrero', '16684315', NULL, NULL,
        'Avenida Ambala Calle 101 Mz G Cs 3 Rincon Del Bosque | Ibague',
        'Manzana A Casa No.2 Urbanizacion SanFrancisco', '3183310058', '3183310058', '2019-03-18 05:00:00',
        '2020-07-11 01:05:18', '2020-07-11 01:05:45', 'henrytejadag@hotmail.com', 'FPT INVERSIONES SAS', NULL, NULL,
        0.00, 8431, NULL, 957, 16, '9012662569', NULL, NULL, 'ROLE_PAYEE', NULL, NULL, 1, 1, NULL, NULL, NULL, 0,
        '00130435000100021985', 10, 957, 1, 'CTE', 0, 0, '');
/*!40000 ALTER TABLE `extended_user`
    ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2020-07-16 14:16:03
