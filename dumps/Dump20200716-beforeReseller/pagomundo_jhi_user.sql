-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: ls-449eb857e6ae75cdb34254c1ef8b62ea8f7d95f1.cwxp9rzhaq1d.us-east-1.rds.amazonaws.com    Database: pagomundo
-- ------------------------------------------------------
-- Server version	5.7.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN = 0;

--
-- GTID state at the beginning of the backup
--

SET @@GLOBAL.GTID_PURGED = /*!80000 '+'*/ '';

--
-- Table structure for table `jhi_user`
--

DROP TABLE IF EXISTS `jhi_user`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jhi_user`
(
    `id`                 bigint(20)  NOT NULL AUTO_INCREMENT,
    `login`              varchar(50) NOT NULL,
    `password_hash`      varchar(60) NOT NULL,
    `first_name`         varchar(50)      DEFAULT NULL,
    `last_name`          varchar(50)      DEFAULT NULL,
    `email`              varchar(191)     DEFAULT NULL,
    `image_url`          varchar(256)     DEFAULT NULL,
    `activated`          bit(1)      NOT NULL,
    `lang_key`           varchar(10)      DEFAULT NULL,
    `activation_key`     varchar(20)      DEFAULT NULL,
    `reset_key`          varchar(20)      DEFAULT NULL,
    `created_by`         varchar(50) NOT NULL,
    `created_date`       timestamp   NULL DEFAULT NULL,
    `reset_date`         timestamp   NULL DEFAULT NULL,
    `last_modified_by`   varchar(50)      DEFAULT NULL,
    `last_modified_date` timestamp   NULL DEFAULT NULL,
    `payee_as_company`   tinyint(1)       DEFAULT '0',
    PRIMARY KEY (`id`),
    UNIQUE KEY `ux_user_login` (`login`),
    UNIQUE KEY `ux_user_email` (`email`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 8433
  DEFAULT CHARSET = utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_user`
--

LOCK TABLES `jhi_user` WRITE;
/*!40000 ALTER TABLE `jhi_user`
    DISABLE KEYS */;
INSERT INTO `jhi_user`
VALUES (1, 'system', '$2a$10$mE.qmcV0mFU5NcKh73TZx.z4ueI/.bDWbj0T1BYyqP481kGGarKLG', 'System', 'System',
        'system@localhost', '', _binary '', 'en', NULL, NULL, 'system', NULL, NULL, 'system', NULL, 0),
       (3, 'admin', '$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC', 'Administrator', 'Administrator',
        'admin@localhost', '', _binary '', 'en', NULL, NULL, 'system', NULL, NULL, 'system', NULL, 0),
       (5, 'superadmin', '$2a$10$f/IK7HpG6zSN2bbAd/qHF.8.lwYUfr/Z6XTRWl40rSqrOz4eV0O5S', 'Super', 'Administrator',
        'agucaliri@gmail.com', NULL, _binary '', 'en', NULL, NULL, 'system', NULL, NULL, 'anonymousUser',
        '2019-11-13 12:41:46', 0),
       (105, 'a.gucaliri@gmail.com', '$2a$10$h1gau9.AOYKsjtuoyJ0qfee0vjcE0nA6iBP.XRLwrAOHa8./dAdOu', 'Agu', 'Admin',
        'a.gucaliri@gmail.com', NULL, _binary '', 'en', NULL, 'MOSnFxveTrCt8lBaTT9e', 'superadmin',
        '2019-11-13 18:37:47', '2019-11-25 20:36:44', 'a.gucaliri@gmail.com', '2019-11-25 20:36:44', 0),
       (106, 'ag.ucaliri@gmail.com', '$2a$10$ns1QU9mqGH1oVkjYuTIAHeiSqd3AxMWYhRsK8iu.lkd/jVeRYUKQ2', 'Agu', 'Merchant',
        'ag.ucaliri@gmail.com', NULL, _binary '', 'en', NULL, NULL, 'a.gucaliri@gmail.com', '2019-11-13 18:54:05',
        NULL, 'anonymousUser', '2019-11-13 18:54:51', 0),
       (8395, 'agu.caliri@gmail.com', '$2a$10$IYcMgxUPXEDPTt0cu6eMi.y/lftxSyvu7x61PvxStm.8RAhuf0Aam', 'Agu', 'Caliri',
        'agu.caliri@gmail.com', NULL, _binary '', 'es', NULL, 'i4xW6huPKYCkrhrCAmXR', 'ag.ucaliri@gmail.com',
        '2019-12-12 14:02:55', '2020-02-18 17:00:07', 'a.gucaliri@gmail.com', '2020-02-18 17:00:07', 0),
       (8396, 'alex@pmi-americas.com', '$2a$10$65x0yqRaL0tNVXBh5fnYKuA0t90Yhp1S8OrzMxIQcguVTrw44kL2y', 'Alex',
        'Pereira', 'alex@pmi-americas.com', NULL, _binary '', 'en', NULL, NULL, 'superadmin', '2020-01-14 16:23:32',
        NULL, 'anonymousUser', '2020-01-14 16:24:58', 0),
       (8397, 'alex@danalex.org', '$2a$10$TnRpfC1ggXKGjLgDNxjB5uuJ1T4pXrcVllDM/igQX1me12xxAueKq', 'Alba', 'Giraldo',
        'alex@danalex.org', NULL, _binary '', 'en', NULL, NULL, 'alex@pmi-americas.com', '2020-01-14 16:27:55', NULL,
        'anonymousUser', '2020-01-14 16:28:43', 0),
       (8398, 'paraisoalex@gmail.com', '$2a$10$iJceDwvUB6c1K4nA53RPqeAfChgRl5ZyQK0Hg2giHe5EURnVeCKhi', 'Axel',
        'Pereira', 'paraisoalex@gmail.com', NULL, _binary '', 'es', NULL, NULL, 'alex@danalex.org',
        '2020-01-14 16:29:58', NULL, 'anonymousUser', '2020-02-19 16:00:54', 0),
       (8399, 'margarita.monroy@controlbox.net', '$2a$10$7VcXUPIp76nEwJq34qeb9ezKEDwAfezJdX0qbvfkwEZcRg19sg2hu',
        'Margarita', 'Monroy', 'margarita.monroy@controlbox.net', NULL, _binary '', 'en', NULL, 'C0Ba6eDWe6IF1nztLGUQ',
        'alex@pmi-americas.com', '2020-02-05 22:16:05', '2020-02-05 22:16:05', 'alex@pmi-americas.com',
        '2020-02-05 22:16:05', 0),
       (8400, 'ganders72@gmail.com', '$2a$10$m0uIu6fC.eJhGb.aNKzh4OwYAbDxPWdm3Nm6dbG7JbWRXvXP4m2lO', 'Greg', 'Anders',
        'ganders72@gmail.com', NULL, _binary '', 'es', NULL, '2nj6Sv7TYcJJaiXUbAeO', 'alex@danalex.org',
        '2020-02-18 15:01:17', '2020-02-18 21:45:23', 'anonymousUser', '2020-02-18 21:45:23', 0),
       (8401, 'ganders72@aim.com', '$2a$10$pwd5VqBHPLQ4iIJbSIeWo.Jk9CGI6ie9FJ3PAbu0MtlZz1fyHrZDm', 'Gregory', 'Anders',
        'ganders72@aim.com', NULL, _binary '', 'es', NULL, 'e3wuYOhq0z8YJ61fEpaS', 'alex@danalex.org',
        '2020-02-18 15:14:28', '2020-02-18 15:14:28', 'alex@danalex.org', '2020-02-18 15:14:28', 0),
       (8402, 'oficinabogota@pmi-americas.com', '$2a$10$02ab19U3dEj3E4/wu4q6Te3Cu64wCBAbdF9vJuGth4hH3/hZRqdse',
        'Oficina', 'Bogota', 'oficinabogota@pmi-americas.com', NULL, _binary '', 'en', NULL, NULL, 'superadmin',
        '2020-02-20 17:07:16', NULL, 'anonymousUser', '2020-02-20 17:08:45', 0),
       (8403, 'bridgethtatiana@hotmail.es', '$2a$10$SwoJYbDxLRpKYzUE5f4Eq.zJJl9y3XaFTQ43AWc/BSM8PmsxQtJ/2', 'Bridgeth',
        'Clavijo', 'bridgethtatiana@hotmail.es', NULL, _binary '', 'es', NULL, NULL, 'alex@danalex.org',
        '2020-02-20 17:26:05', NULL, 'anonymousUser', '2020-02-20 17:32:39', 0),
       (8404, 'alberto.celi+ag@gmail.com', '$2a$10$79ZNnp2B.mbfE372VAa.bOClmauLq2xdfxxhqWf/afp1CVD0EhRQW', 'Antonella',
        'Godoy', 'alberto.celi+ag@gmail.com', NULL, _binary '', 'es', NULL, 'SSOWvXhIE6RxqUj0iKiC',
        'ag.ucaliri@gmail.com', '2020-03-11 14:37:42', '2020-03-11 14:37:42', 'ag.ucaliri@gmail.com',
        '2020-03-11 14:37:42', 0),
       (8405, 'alex@paraisopictures.com', '$2a$10$Wjm5Xcx3mlwjUw9XP0QiU.gBnmjBGuk9j1RCQT7WyqLVxdmUzSEpW',
        'Maria Teresa', 'Djurovic', 'alex@paraisopictures.com', NULL, _binary '', 'es', NULL, NULL, 'alex@danalex.org',
        '2020-03-12 19:54:46', NULL, 'anonymousUser', '2020-03-12 19:55:40', 0),
       (8406, 'mauriciobarrientos1@hotmail.com', '$2a$10$DCbMmzpfg3To4/F1DE4GE.UfM1T5jUso62AjrqCzyfHraY2O8CINy',
        'Mauricio', 'Barrientos', 'mauriciobarrientos1@hotmail.com', NULL, _binary '', 'es', NULL, NULL,
        'alex@danalex.org', '2020-03-18 16:00:04', NULL, 'anonymousUser', '2020-03-18 16:32:07', 0),
       (8407, 'agucaliri+jp@gmail.com', '$2a$10$qY4RhzavPPS7/RCrt3cIUepbKCBSusJpaijPVu2nC8dsWOaSCNdSC', 'Juan', 'Perez',
        'agucaliri+jp@gmail.com', NULL, _binary '', 'es', NULL, NULL, 'ag.ucaliri@gmail.com', '2020-03-18 19:35:30',
        NULL, 'anonymousUser', '2020-03-18 19:37:05', 0),
       (8408, 'benjamin.bayr@processingpartners.com', '$2a$10$7f.DikF.PmzrIKcr7aCJ0.U6uD7.cZ3FTKs1DxQrMqG36WgIVJRCa',
        'Benjamin', 'Bayr', 'benjamin.bayr@processingpartners.com', NULL, _binary '', 'en', NULL, NULL, 'superadmin',
        '2020-03-30 13:50:23', NULL, 'anonymousUser', '2020-05-27 13:50:42', 0),
       (8409, 'agucaliri+ac@gmail.com', '$2a$10$o9VK9dWMJnLgUstE6kwbLeFmKmWw.Axk8BqiJqsmoJc10rfyXaOeq', 'Agustín',
        'Caliri', 'agucaliri+ac@gmail.com', NULL, _binary '', 'en', NULL, NULL, 'superadmin', '2020-03-30 14:39:49',
        NULL, 'anonymousUser', '2020-03-30 14:40:40', 0),
       (8410, 'jeffyoung7@gmail.com', '$2a$10$.NktwS1Gpy9ZAgVXrJ39OeoaK4h9UPEDDGen8apMXc4SjkeMSnDMC', 'Robert', 'Young',
        'jeffyoung7@gmail.com', NULL, _binary '', 'en', NULL, NULL, 'alex@pmi-americas.com', '2020-04-01 18:03:10',
        NULL, 'anonymousUser', '2020-04-13 21:40:48', 0),
       (8411, 'adissanta@gmail.com', '$2a$10$wjHHQ1h3zWCov3DjIdn6VuEyXyQQlYtfZmIC/zvhOaK1Sh7klGxbW', 'Adys',
        'Santana Martinez', 'adissanta@gmail.com', NULL, _binary '', 'es', NULL, NULL, 'jeffyoung7@gmail.com',
        '2020-04-13 21:43:30', NULL, 'anonymousUser', '2020-04-17 20:39:03', 0),
       (8412, 'oso@oso.media', '$2a$10$JypDhq1sZFxHq7mmyW1oCuy6YYSmIhYjHsInOcOWdeB6H4zSkwaci', 'Santiago', 'Diaz',
        'oso@oso.media', NULL, _binary '', 'es', NULL, NULL, 'alex@danalex.org', '2020-04-15 15:52:33', NULL,
        'anonymousUser', '2020-04-15 16:19:45', 0),
       (8413, 'mai.aracercasi+pp@gmail.com', '$2a$10$B6qCnXZ4P.9J4wne8iN8OO/TadK.vqXUYOCtn5JGaH0K4mHGxPD9K', 'Test',
        'Payee', 'mai.aracercasi+pp@gmail.com', NULL, _binary '', 'es', NULL, NULL, 'ag.ucaliri@gmail.com',
        '2020-04-22 14:28:51', NULL, 'anonymousUser', '2020-04-22 14:29:54', 0),
       (8414, 'marialuciadiaz@gmail.com', '$2a$10$JgupcjVDM05JNaKvKI/.bOTaM8YDmAW1gTz5454M40SumTiMQcr6O', 'Maria Lucia',
        'Diaz', 'marialuciadiaz@gmail.com', NULL, _binary '', 'es', NULL, NULL, 'alex@danalex.org',
        '2020-04-29 19:32:21', NULL, 'anonymousUser', '2020-04-29 19:44:05', 0),
       (8415, 'risk@processingpartners.com', '$2a$10$6Oh.oawyAOHi8K6VgB4U1.z/gbMFwMQePlk/yVUpkeOlfDHQEAOq2', 'Risk',
        'Processing Partners', 'risk@processingpartners.com', NULL, _binary '', 'en', NULL, NULL, 'superadmin',
        '2020-04-30 13:53:13', NULL, 'anonymousUser', '2020-04-30 14:20:57', 0),
       (8416, 'crm@pmi-americas.com', '$2a$10$3T0iUTd2ey/qQPGHl7lXK.Cr5JdkUTTm9ycM5eWd8Vh.qCA1d1s/a', 'AG', 'Studios',
        'crm@pmi-americas.com', NULL, _binary '', 'es', NULL, NULL, 'alex@danalex.org', '2020-05-05 15:18:53', NULL,
        'anonymousUser', '2020-05-05 15:20:12', 0),
       (8417, 'stuart@iml.ad', '$2a$10$BTP7v5MUVcQg/Yy.HimIseBSRNjeoTxmujgi5SDQ6F.2djhWYdTDq', 'Stuart', 'Foster',
        'stuart@iml.ad', NULL, _binary '', 'en', NULL, NULL, 'benjamin.bayr@processingpartners.com',
        '2020-05-27 14:21:53', NULL, 'anonymousUser', '2020-05-27 14:24:12', 0),
       (8418, 'yoke@iml.ad', '$2a$10$DlfNO7LzEd0skswxRunFvueeACk27tGj5YM53jOyDeGBquHgHBd5O', 'Yokebed', 'Flores Torres',
        'yoke@iml.ad', NULL, _binary '', 'en', NULL, NULL, 'benjamin.bayr@processingpartners.com',
        '2020-05-30 04:44:17', NULL, 'anonymousUser', '2020-06-02 09:39:21', 0),
       (8419, 'studioyjlb@gmail.com', '$2a$10$QhnE.b7vOUpLlAtsrn72yOmQVi9IR.LMBFa2iu.HgYvHUzquDP3U2', 'Yuli Jineth',
        'López Bohorquez', 'studioyjlb@gmail.com', NULL, _binary '', 'es', NULL, NULL, 'stuart@iml.ad',
        '2020-06-02 10:25:37', NULL, 'anonymousUser', '2020-06-03 14:48:40', 1),
       (8420, 'danitzad@gmail.com', '$2a$10$W9Qu/tiy6g2i24OejJV4ouPkz2ddDkQ/ZE9DbCfUkbgDaN.TiZ0Py', 'Danitza',
        'Djutovic', 'danitzad@gmail.com', NULL, _binary '', 'es', NULL, 'PEFhZxEUyHnsQJU8YZH0', 'alex@danalex.org',
        '2020-06-02 20:12:33', '2020-06-02 20:12:33', 'alex@danalex.org', '2020-06-02 20:12:33', 1),
       (8421, 'management@celeb.tv', '$2a$10$L4ZY2Z/MHatAAhKHiWtn6O1.rrCxAypGg2D8T/cTGg1GcBoR2a.P6', 'Meryl',
        'Shulmister', 'management@celeb.tv', NULL, _binary '', 'en', NULL, 'a83kSnMJ53JZpo0QmLRA',
        'alex@pmi-americas.com', '2020-06-03 15:00:52', '2020-06-26 23:39:59', 'management@celeb.tv',
        '2020-06-26 23:39:59', 0),
       (8422, 'jamiebaker4181@gmail.com', '$2a$10$E/Uv96JtGpK7XZ6APzw.d.xufj0IGNh9M/J76l4KWxByT20de1OH.', 'jamie',
        'baker', 'jamiebaker4181@gmail.com', NULL, _binary '', 'es', NULL, NULL, 'management@celeb.tv',
        '2020-06-04 22:06:35', NULL, 'anonymousUser', '2020-06-04 22:08:48', 0),
       (8423, 'campalsinfo@gmail.com', '$2a$10$wtE8yrO.Y4kG4upQAB9cUO5w23ts2TplnmrKVZepzrolN84gqlFbG', 'adys', 'young',
        'campalsinfo@gmail.com', NULL, _binary '', 'es', NULL, NULL, 'management@celeb.tv', '2020-06-04 22:09:03',
        NULL, 'anonymousUser', '2020-06-04 22:12:19', 0),
       (8424, 'jose.lopera@colegiatura.edu.co', '$2a$10$nPSSfQdg2s89qGMMiJ7BsOpbT6SgVm.WCx9yN5SP7rkCp7UgOUXQ.',
        'Jose David', 'Lopera Agudelo', 'jose.lopera@colegiatura.edu.co', NULL, _binary '', 'es', NULL, NULL,
        'management@celeb.tv', '2020-06-09 17:37:26', NULL, 'anonymousUser', '2020-06-27 23:21:48', 0),
       (8425, 'amanda.falves@hotmail.com', '$2a$10$uNUT9T7XvDCwpNeIy7lsLOuxcZywvIG1BT3c1xlcB7zKhBmwXGetS', 'Amanda',
        'Ferreira Alves', 'amanda.falves@hotmail.com', NULL, _binary '', 'es', NULL, NULL, 'management@celeb.tv',
        '2020-06-09 18:13:42', NULL, 'anonymousUser', '2020-06-09 18:15:50', 0),
       (8426, 'alberto.celi+payee001@gmail.com', '$2a$10$O7VCXD1sb6KEKycJ0uGvq./6rFXQ8inXLQ/Pc5dTEnmFwfsHB2wYO',
        'Alberto', 'Celi', 'alberto.celi+payee001@gmail.com', NULL, _binary '', 'es', NULL, 'ZIqanezIJfqF9Azw42VW',
        'ag.ucaliri@gmail.com', '2020-06-10 12:04:32', '2020-06-10 12:04:32', 'ag.ucaliri@gmail.com',
        '2020-06-10 12:04:32', 0),
       (8427, 'maiaqa2020+payemp9@gmail.com', '$2a$10$iFVHkrc3HemMxNUY14BUbO9yy6VinQusIYBqJOGP/YlcMu6Aoz.Ei', 'JJ',
        'Legal', 'maiaqa2020+payemp9@gmail.com', NULL, _binary '', 'es', NULL, 'LvFoYlqaCYfIvcROlQZ9',
        'ag.ucaliri@gmail.com', '2020-06-10 18:36:52', '2020-06-10 18:36:52', 'ag.ucaliri@gmail.com',
        '2020-06-10 18:36:52', 1),
       (8428, 'maiaqa2020+payemp10@gmail.com', '$2a$10$t12ipoDkZCJcv5eCUXrWbupFgcqqZzRJ1Fc2CiJhQ4M2byipOu4Ty', 'JJ',
        'company 10', 'maiaqa2020+payemp10@gmail.com', NULL, _binary '', 'es', NULL, 'lFxcBEq0kiQLrQogWbq7',
        'ag.ucaliri@gmail.com', '2020-06-10 19:33:58', '2020-06-10 19:33:58', 'ag.ucaliri@gmail.com',
        '2020-06-10 19:33:58', 1),
       (8429, 'andresrg2907@gmail.com', '$2a$10$vSLT9I./p4Tl4k4aP6LkLeM16hTaKu7O9RIsZlFLk1gmcqOOacb/m', 'Andrés Felipe',
        'Rincón Gaviria', 'andresrg2907@gmail.com', NULL, _binary '', 'es', NULL, NULL, 'management@celeb.tv',
        '2020-06-12 18:02:12', NULL, 'anonymousUser', '2020-06-13 02:43:29', 0),
       (8430, 'fptinversionessas@outlook.es', '$2a$10$hODQtcVn458hsBLx8p71VOyyo4vOoTfpcL3J6d6AEMLG//7cB4lg2', 'Henry',
        'Tejada Guerrero', 'fptinversionessas@outlook.es', NULL, _binary '', 'es', NULL, 'asha42wmO2Yd6GiuQItf',
        'yoke@iml.ad', '2020-07-07 13:44:47', '2020-07-07 13:44:47', 'yoke@iml.ad', '2020-07-07 13:44:47', 1),
       (8431, 'henrytejadag@hotmail.com', '$2a$10$gMwrxGIGMlWE6kYWV/mdI.JM5B2.etz/EU8jvfz0oF6xL3R/CJE6u', 'Henry',
        'Tejada Guerrero', 'henrytejadag@hotmail.com', NULL, _binary '', 'es', NULL, NULL, 'yoke@iml.ad',
        '2020-07-10 13:51:37', NULL, 'anonymousUser', '2020-07-11 00:59:09', 1),
       (8432, 'mariapazbohorquez@gmail.com', '$2a$10$iCCCIvCz2LNSPoToahU6HuCbbBxSk4qhvXwc7/HoYUCZ2GPvL8Kt2', 'Mariapaz',
        'Bohorquez Rojas', 'mariapazbohorquez@gmail.com', NULL, _binary '', 'es', NULL, 'c8s3tI5C3yETUoDHgMLe',
        'management@celeb.tv', '2020-07-15 16:59:48', '2020-07-15 16:59:48', 'management@celeb.tv',
        '2020-07-15 16:59:48', 0);
/*!40000 ALTER TABLE `jhi_user`
    ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2020-07-16 14:17:27
