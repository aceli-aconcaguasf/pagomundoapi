-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: ls-449eb857e6ae75cdb34254c1ef8b62ea8f7d95f1.cwxp9rzhaq1d.us-east-1.rds.amazonaws.com    Database: pagomundo
-- ------------------------------------------------------
-- Server version	5.7.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Dumping data for table `jhi_user`
--

LOCK TABLES `jhi_user` WRITE;
/*!40000 ALTER TABLE `jhi_user` DISABLE KEYS */;
INSERT INTO `jhi_user` VALUES (1,'system','$2a$10$mE.qmcV0mFU5NcKh73TZx.z4ueI/.bDWbj0T1BYyqP481kGGarKLG','System','System','system@localhost','',_binary '','en',NULL,NULL,'system',NULL,NULL,'system',NULL),(3,'admin','$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC','Administrator','Administrator','admin@localhost','',_binary '','en',NULL,NULL,'system',NULL,NULL,'system',NULL),(5,'agucaliri@gmail.com','$2a$10$f/IK7HpG6zSN2bbAd/qHF.8.lwYUfr/Z6XTRWl40rSqrOz4eV0O5S','Super','Administrator','agucaliri@gmail.com',NULL,_binary '','en',NULL,NULL,'system',NULL,NULL,'anonymousUser','2019-11-13 12:41:46'),(105,'a.gucaliri@gmail.com','$2a$10$h1gau9.AOYKsjtuoyJ0qfee0vjcE0nA6iBP.XRLwrAOHa8./dAdOu','Agu','Admin','a.gucaliri@gmail.com',NULL,_binary '','en',NULL,'MOSnFxveTrCt8lBaTT9e','superadmin','2019-11-13 18:37:47','2019-11-25 20:36:44','a.gucaliri@gmail.com','2019-11-25 20:36:44'),(106,'ag.ucaliri@gmail.com','$2a$10$ns1QU9mqGH1oVkjYuTIAHeiSqd3AxMWYhRsK8iu.lkd/jVeRYUKQ2','Agu','Merchant','ag.ucaliri@gmail.com',NULL,_binary '','en',NULL,NULL,'a.gucaliri@gmail.com','2019-11-13 18:54:05',NULL,'anonymousUser','2019-11-13 18:54:51'),(8395,'agu.caliri@gmail.com','$2a$10$IYcMgxUPXEDPTt0cu6eMi.y/lftxSyvu7x61PvxStm.8RAhuf0Aam','Agu','Caliri','agu.caliri@gmail.com',NULL,_binary '','es',NULL,NULL,'ag.ucaliri@gmail.com','2019-12-12 14:02:55',NULL,'anonymousUser','2019-12-12 14:04:48');
/*!40000 ALTER TABLE `jhi_user` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-12 12:09:49
