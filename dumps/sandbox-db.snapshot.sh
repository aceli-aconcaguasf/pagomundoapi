#!/bin/bash
echo "Starting Sandbox DB snapshot process..."

databaseName=sandbox-pagomundoDB
timestamp=$(date +%Y%m%d%H%M%S)
snapshotName=$databaseName-$timestamp
awsBackupCommand="aws lightsail create-relational-database-snapshot --relational-database-name ${databaseName} --relational-database-snapshot-name ${snapshotName} --profile adminUser"

echo "Database name: $databaseName"
echo "Database snapshot name: $snapshotName"
echo "Executing command: $awsBackupCommand"

($awsBackupCommand)

echo "Sandbox DB snapshot process ended."
