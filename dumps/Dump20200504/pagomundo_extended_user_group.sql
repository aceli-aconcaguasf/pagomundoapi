-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: pagomundo
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `extended_user_group`
--

DROP TABLE IF EXISTS `extended_user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `extended_user_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_created` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_name_from_bank` varchar(255) DEFAULT NULL,
  `admin_id` bigint(20) DEFAULT NULL,
  `bank_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_extended_user_group_admin_id` (`admin_id`),
  KEY `fk_extended_user_group_bank_id` (`bank_id`),
  CONSTRAINT `fk_extended_user_group_admin_id` FOREIGN KEY (`admin_id`) REFERENCES `extended_user` (`id`),
  CONSTRAINT `fk_extended_user_group_bank_id` FOREIGN KEY (`bank_id`) REFERENCES `bank` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extended_user_group`
--

LOCK TABLES `extended_user_group` WRITE;
/*!40000 ALTER TABLE `extended_user_group` DISABLE KEYS */;
INSERT INTO `extended_user_group` VALUES (1,'2019-11-19 18:14:17','IN_PROCESS','fileBucket/extendedUser/exportedFiles/2019_11_19_18_14_16_96-Itau-Colombia.xlsx',NULL,94,2),(7,'2019-11-20 12:07:49','IN_PROCESS','fileBucket/extendedUser/exportedFiles/2019_11_20_12_07_48_95-Itau-Colombia.xlsx',NULL,94,2),(9,'2019-11-21 12:21:15','IN_PROCESS','fileBucket/extendedUser/exportedFiles/2019_11_21_12_21_15_46-Itau-Colombia.xlsx',NULL,94,2),(14,'2019-11-21 15:41:46','IN_PROCESS','fileBucket/extendedUser/exportedFiles/2019_11_21_15_41_45_60-Itau-Colombia.xlsx',NULL,226,2),(15,'2019-11-21 15:50:10','IN_PROCESS','fileBucket/extendedUser/exportedFiles/2019_11_21_15_50_09_73-Itau-Colombia.xlsx',NULL,226,2),(16,'2019-11-21 15:50:16','IN_PROCESS','fileBucket/extendedUser/exportedFiles/2019_11_21_15_50_15_60-Pichincha-Colombia.xlsx',NULL,226,1),(17,'2019-11-21 15:50:26','IN_PROCESS','fileBucket/extendedUser/exportedFiles/2019_11_21_15_50_26_08-Pichincha-Colombia.xlsx',NULL,226,1),(19,'2019-11-21 19:46:49','IN_PROCESS','fileBucket/extendedUser/exportedFiles/2019_11_21_19_46_48_54-Pichincha-Colombia.xlsx',NULL,226,1),(20,'2019-11-22 11:59:27','IN_PROCESS','fileBucket/extendedUser/exportedFiles/2019_11_22_11_59_26_58-Itau-Colombia.xlsx',NULL,94,2),(21,'2019-11-22 15:54:32','IN_PROCESS','fileBucket/extendedUser/exportedFiles/2019_11_22_15_54_32_29-Itau-Colombia.xlsx',NULL,94,2),(25,'2019-11-23 21:35:02','IN_PROCESS','fileBucket/extendedUser/exportedFiles/2019_11_23_21_35_01_75-Banxico-Mexico.csv',NULL,231,3),(26,'2019-11-25 20:06:07','IN_PROCESS','fileBucket/extendedUser/exportedFiles/2019_11_25_20_06_07_09-Banxico-Mexico.csv',NULL,94,3),(27,'2019-11-26 19:30:07','IN_PROCESS','fileBucket/extendedUser/exportedFiles/2019_11_26_19_30_06_51-Pichincha-Colombia.xlsx',NULL,94,1),(28,'2019-11-27 12:08:15','IN_PROCESS','fileBucket/extendedUser/exportedFiles/2019_11_27_12_08_15_42-Pichincha-Colombia.xlsx',NULL,94,1),(29,'2019-12-02 18:58:55','ACCEPTED','fileBucket/extendedUser/exportedFiles/2019_12_02_18_58_55_00-Pichincha-Colombia.xlsx',NULL,94,1),(30,'2019-12-02 19:24:25','ACCEPTED','fileBucket/extendedUser/exportedFiles/2019_12_02_19_24_25_00-Itau-Colombia.xlsx',NULL,248,2),(31,'2019-12-09 14:44:14','ACCEPTED','fileBucket/extendedUser/exportedFiles/2019_12_09_14_44_14_00-Itau-Colombia.xlsx',NULL,94,2),(32,'2019-12-09 14:50:54','ACCEPTED','fileBucket/extendedUser/exportedFiles/2019_12_09_14_50_54_00-Pichincha-Colombia.xlsx',NULL,94,1),(33,'2019-12-09 15:23:08','ACCEPTED','fileBucket/extendedUser/exportedFiles/2019_12_09_15_23_08_00-Pichincha-Colombia.xlsx',NULL,94,1),(34,'2019-12-10 13:48:56','ACCEPTED','fileBucket/extendedUser/exportedFiles/2019_12_10_13_48_56_00-Pichincha-Colombia.xlsx',NULL,94,1),(35,'2019-12-10 14:36:24','ACCEPTED','fileBucket/extendedUser/exportedFiles/2019_12_10_14_36_24_00-Banxico-Mexico.csv',NULL,94,3),(36,'2019-12-10 17:59:11','ACCEPTED','fileBucket/extendedUser/exportedFiles/2019_12_10_17_59_11_00-Banxico-Mexico.csv',NULL,94,3),(37,'2019-12-12 18:34:55','ACCEPTED','fileBucket/extendedUser/exportedFiles/2019_12_12_18_34_55_00-Banxico-Mexico.csv',NULL,94,3),(38,'2019-12-19 14:42:49','ACCEPTED','fileBucket/extendedUser/exportedFiles/2019_12_19_14_42_49_00-Banxico-Mexico.csv',NULL,255,3),(39,'2019-12-19 14:44:14','ACCEPTED','fileBucket/extendedUser/exportedFiles/2019_12_19_14_44_14_00-Pichincha-Colombia.xlsx',NULL,255,1),(40,'2019-12-19 14:44:35','ACCEPTED','fileBucket/extendedUser/exportedFiles/2019_12_19_14_44_35_00-Pichincha-Colombia.xlsx',NULL,255,1),(41,'2019-12-19 17:21:48','ACCEPTED','fileBucket/extendedUser/exportedFiles/2019_12_19_17_21_48_00-Pichincha-Colombia.xlsx',NULL,93,1),(43,'2019-12-20 12:25:20','ACCEPTED','fileBucket/extendedUser/exportedFiles/2019_12_20_12_25_20_00-Banxico-Mexico.csv',NULL,261,3),(44,'2019-12-23 14:14:36','ACCEPTED','fileBucket/extendedUser/exportedFiles/2019_12_23_14_14_36_00-Pichincha-Colombia.xlsx',NULL,94,1),(45,'2020-01-02 12:26:02','ACCEPTED','fileBucket/extendedUser/exportedFiles/2020_01_02_12_26_02_00-Banxico-Mexico.csv',NULL,261,3),(46,'2020-01-02 19:49:21','ACCEPTED','fileBucket/extendedUser/exportedFiles/2020_01_02_19_49_21_00-Itau-Colombia.xlsx',NULL,94,2),(47,'2020-01-03 11:53:48','ACCEPTED','fileBucket/extendedUser/exportedFiles/2020_01_03_11_53_48_00-Pichincha-Colombia.xlsx',NULL,93,1),(49,'2020-01-07 12:34:53','ACCEPTED','fileBucket/extendedUser/exportedFiles/2020_01_07_12_34_53_00-Itau-Colombia.xlsx',NULL,261,2),(50,'2020-01-08 20:05:35','ACCEPTED','fileBucket/extendedUser/exportedFiles/2020_01_08_20_05_35_00-Itau-Colombia.xlsx',NULL,93,2),(51,'2020-01-09 19:00:09','ACCEPTED','fileBucket/extendedUser/exportedFiles/2020_01_09_19_00_09_00-Banxico-Mexico.csv',NULL,94,3),(52,'2020-01-13 12:03:38','CANCELLED',NULL,NULL,261,27),(53,'2020-01-13 12:54:20','ACCEPTED','fileBucket/extendedUser/exportedFiles/2020_01_13_12_54_20_00-Banxico-Mexico.csv',NULL,261,3),(54,'2020-01-13 13:13:21','ACCEPTED','fileBucket/extendedUser/exportedFiles/2020_01_13_13_13_21_00-Itau-Colombia.xlsx',NULL,261,2),(55,'2020-01-13 20:02:56','ACCEPTED','fileBucket/extendedUser/exportedFiles/2020_01_13_20_02_56_00-Itau-Colombia.xlsx',NULL,94,2),(56,'2020-01-14 11:51:54','ACCEPTED','fileBucket/extendedUser/exportedFiles/2020_01_14_11_51_54_00-Banxico-Mexico.csv',NULL,94,3),(57,'2020-01-14 11:52:28','ACCEPTED','fileBucket/extendedUser/exportedFiles/2020_01_14_11_52_28_00-Itau-Colombia.xlsx',NULL,261,2),(58,'2020-01-14 15:58:28','ACCEPTED','fileBucket/extendedUser/exportedFiles/2020_01_14_15_58_28_00-Itau-Colombia.xlsx',NULL,261,2),(59,'2020-01-15 18:11:29','ACCEPTED','fileBucket/extendedUser/exportedFiles/2020_01_15_18_11_29_00-Itau-Colombia.xlsx',NULL,94,2),(60,'2020-01-16 10:51:25','ACCEPTED','fileBucket/extendedUser/exportedFiles/2020_01_16_10_51_25_00-Itau-Colombia.xlsx',NULL,94,2),(61,'2020-01-16 16:06:04','ACCEPTED','fileBucket/extendedUser/exportedFiles/2020_01_16_16_06_04_00-Itau-Colombia.xlsx',NULL,94,2),(62,'2020-01-17 11:02:46','ACCEPTED','fileBucket/extendedUser/exportedFiles/2020_01_17_11_02_46_00-Banxico-Mexico.csv',NULL,94,3),(63,'2020-01-17 11:15:54','ACCEPTED','fileBucket/extendedUser/exportedFiles/2020_01_17_11_15_54_00-Banxico-Mexico.csv',NULL,94,3),(64,'2020-01-17 12:00:20','ACCEPTED','fileBucket/extendedUser/exportedFiles/2020_01_17_12_00_20_00-Banxico-Mexico.csv',NULL,94,3),(65,'2020-01-17 12:29:52','ACCEPTED','fileBucket/extendedUser/exportedFiles/2020_01_17_12_29_52_00-Itau-Colombia.xlsx',NULL,94,2),(66,'2020-01-17 18:56:08','ACCEPTED','fileBucket/extendedUser/exportedFiles/2020_01_17_18_56_08_00-Pichincha-Colombia.xlsx',NULL,93,1),(67,'2020-01-22 18:27:20','ACCEPTED','fileBucket/extendedUser/exportedFiles/2020_01_22_18_27_20_00-Banxico-Mexico.csv',NULL,93,3),(68,'2020-02-10 14:51:03','ACCEPTED','fileBucket/extendedUser/exportedFiles/2020_02_10_14_51_03_00-Itau-Colombia.xlsx',NULL,261,2),(69,'2020-02-10 14:51:19','ACCEPTED','fileBucket/extendedUser/exportedFiles/2020_02_10_14_51_19_00-Banxico-Mexico.csv',NULL,261,3);
/*!40000 ALTER TABLE `extended_user_group` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-04 15:42:21
