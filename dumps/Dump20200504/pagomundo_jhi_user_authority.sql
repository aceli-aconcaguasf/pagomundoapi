-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: pagomundo
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `jhi_user_authority`
--

DROP TABLE IF EXISTS `jhi_user_authority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jhi_user_authority` (
  `user_id` bigint(20) NOT NULL,
  `authority_name` varchar(50) NOT NULL,
  PRIMARY KEY (`user_id`,`authority_name`),
  KEY `fk_authority_name` (`authority_name`),
  CONSTRAINT `fk_authority_name` FOREIGN KEY (`authority_name`) REFERENCES `jhi_authority` (`name`),
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `jhi_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_user_authority`
--

LOCK TABLES `jhi_user_authority` WRITE;
/*!40000 ALTER TABLE `jhi_user_authority` DISABLE KEYS */;
INSERT INTO `jhi_user_authority` VALUES (1,'ROLE_ADMIN'),(3,'ROLE_ADMIN'),(105,'ROLE_ADMIN'),(232,'ROLE_ADMIN'),(233,'ROLE_ADMIN'),(238,'ROLE_ADMIN'),(243,'ROLE_ADMIN'),(244,'ROLE_ADMIN'),(262,'ROLE_ADMIN'),(263,'ROLE_ADMIN'),(267,'ROLE_ADMIN'),(299,'ROLE_ADMIN'),(312,'ROLE_ADMIN'),(313,'ROLE_ADMIN'),(319,'ROLE_ADMIN'),(325,'ROLE_ADMIN'),(354,'ROLE_ADMIN'),(358,'ROLE_ADMIN'),(106,'ROLE_MERCHANT'),(109,'ROLE_MERCHANT'),(231,'ROLE_MERCHANT'),(237,'ROLE_MERCHANT'),(264,'ROLE_MERCHANT'),(268,'ROLE_MERCHANT'),(273,'ROLE_MERCHANT'),(290,'ROLE_MERCHANT'),(291,'ROLE_MERCHANT'),(293,'ROLE_MERCHANT'),(301,'ROLE_MERCHANT'),(302,'ROLE_MERCHANT'),(303,'ROLE_MERCHANT'),(311,'ROLE_MERCHANT'),(314,'ROLE_MERCHANT'),(320,'ROLE_MERCHANT'),(324,'ROLE_MERCHANT'),(326,'ROLE_MERCHANT'),(330,'ROLE_MERCHANT'),(359,'ROLE_MERCHANT'),(360,'ROLE_MERCHANT'),(377,'ROLE_MERCHANT'),(378,'ROLE_MERCHANT'),(379,'ROLE_MERCHANT'),(380,'ROLE_MERCHANT'),(381,'ROLE_MERCHANT'),(382,'ROLE_MERCHANT'),(383,'ROLE_MERCHANT'),(384,'ROLE_MERCHANT'),(385,'ROLE_MERCHANT'),(386,'ROLE_MERCHANT'),(401,'ROLE_MERCHANT'),(410,'ROLE_MERCHANT'),(107,'ROLE_PAYEE'),(108,'ROLE_PAYEE'),(110,'ROLE_PAYEE'),(111,'ROLE_PAYEE'),(112,'ROLE_PAYEE'),(113,'ROLE_PAYEE'),(114,'ROLE_PAYEE'),(115,'ROLE_PAYEE'),(116,'ROLE_PAYEE'),(117,'ROLE_PAYEE'),(118,'ROLE_PAYEE'),(119,'ROLE_PAYEE'),(120,'ROLE_PAYEE'),(121,'ROLE_PAYEE'),(122,'ROLE_PAYEE'),(123,'ROLE_PAYEE'),(124,'ROLE_PAYEE'),(125,'ROLE_PAYEE'),(126,'ROLE_PAYEE'),(127,'ROLE_PAYEE'),(128,'ROLE_PAYEE'),(129,'ROLE_PAYEE'),(130,'ROLE_PAYEE'),(131,'ROLE_PAYEE'),(132,'ROLE_PAYEE'),(133,'ROLE_PAYEE'),(134,'ROLE_PAYEE'),(135,'ROLE_PAYEE'),(136,'ROLE_PAYEE'),(137,'ROLE_PAYEE'),(138,'ROLE_PAYEE'),(139,'ROLE_PAYEE'),(140,'ROLE_PAYEE'),(141,'ROLE_PAYEE'),(142,'ROLE_PAYEE'),(143,'ROLE_PAYEE'),(144,'ROLE_PAYEE'),(145,'ROLE_PAYEE'),(146,'ROLE_PAYEE'),(147,'ROLE_PAYEE'),(148,'ROLE_PAYEE'),(149,'ROLE_PAYEE'),(150,'ROLE_PAYEE'),(151,'ROLE_PAYEE'),(152,'ROLE_PAYEE'),(153,'ROLE_PAYEE'),(154,'ROLE_PAYEE'),(155,'ROLE_PAYEE'),(156,'ROLE_PAYEE'),(157,'ROLE_PAYEE'),(158,'ROLE_PAYEE'),(159,'ROLE_PAYEE'),(160,'ROLE_PAYEE'),(161,'ROLE_PAYEE'),(162,'ROLE_PAYEE'),(163,'ROLE_PAYEE'),(164,'ROLE_PAYEE'),(165,'ROLE_PAYEE'),(166,'ROLE_PAYEE'),(167,'ROLE_PAYEE'),(168,'ROLE_PAYEE'),(169,'ROLE_PAYEE'),(170,'ROLE_PAYEE'),(171,'ROLE_PAYEE'),(172,'ROLE_PAYEE'),(173,'ROLE_PAYEE'),(174,'ROLE_PAYEE'),(175,'ROLE_PAYEE'),(176,'ROLE_PAYEE'),(177,'ROLE_PAYEE'),(178,'ROLE_PAYEE'),(179,'ROLE_PAYEE'),(180,'ROLE_PAYEE'),(181,'ROLE_PAYEE'),(182,'ROLE_PAYEE'),(183,'ROLE_PAYEE'),(184,'ROLE_PAYEE'),(185,'ROLE_PAYEE'),(186,'ROLE_PAYEE'),(187,'ROLE_PAYEE'),(188,'ROLE_PAYEE'),(189,'ROLE_PAYEE'),(190,'ROLE_PAYEE'),(191,'ROLE_PAYEE'),(192,'ROLE_PAYEE'),(193,'ROLE_PAYEE'),(194,'ROLE_PAYEE'),(195,'ROLE_PAYEE'),(196,'ROLE_PAYEE'),(197,'ROLE_PAYEE'),(198,'ROLE_PAYEE'),(199,'ROLE_PAYEE'),(200,'ROLE_PAYEE'),(201,'ROLE_PAYEE'),(202,'ROLE_PAYEE'),(203,'ROLE_PAYEE'),(204,'ROLE_PAYEE'),(205,'ROLE_PAYEE'),(206,'ROLE_PAYEE'),(207,'ROLE_PAYEE'),(208,'ROLE_PAYEE'),(209,'ROLE_PAYEE'),(210,'ROLE_PAYEE'),(211,'ROLE_PAYEE'),(212,'ROLE_PAYEE'),(213,'ROLE_PAYEE'),(214,'ROLE_PAYEE'),(215,'ROLE_PAYEE'),(216,'ROLE_PAYEE'),(217,'ROLE_PAYEE'),(218,'ROLE_PAYEE'),(219,'ROLE_PAYEE'),(220,'ROLE_PAYEE'),(221,'ROLE_PAYEE'),(222,'ROLE_PAYEE'),(223,'ROLE_PAYEE'),(224,'ROLE_PAYEE'),(225,'ROLE_PAYEE'),(226,'ROLE_PAYEE'),(227,'ROLE_PAYEE'),(228,'ROLE_PAYEE'),(229,'ROLE_PAYEE'),(230,'ROLE_PAYEE'),(234,'ROLE_PAYEE'),(235,'ROLE_PAYEE'),(236,'ROLE_PAYEE'),(254,'ROLE_PAYEE'),(255,'ROLE_PAYEE'),(256,'ROLE_PAYEE'),(257,'ROLE_PAYEE'),(258,'ROLE_PAYEE'),(259,'ROLE_PAYEE'),(260,'ROLE_PAYEE'),(265,'ROLE_PAYEE'),(266,'ROLE_PAYEE'),(269,'ROLE_PAYEE'),(270,'ROLE_PAYEE'),(271,'ROLE_PAYEE'),(272,'ROLE_PAYEE'),(274,'ROLE_PAYEE'),(275,'ROLE_PAYEE'),(276,'ROLE_PAYEE'),(277,'ROLE_PAYEE'),(278,'ROLE_PAYEE'),(279,'ROLE_PAYEE'),(280,'ROLE_PAYEE'),(281,'ROLE_PAYEE'),(282,'ROLE_PAYEE'),(283,'ROLE_PAYEE'),(284,'ROLE_PAYEE'),(285,'ROLE_PAYEE'),(286,'ROLE_PAYEE'),(287,'ROLE_PAYEE'),(288,'ROLE_PAYEE'),(289,'ROLE_PAYEE'),(292,'ROLE_PAYEE'),(294,'ROLE_PAYEE'),(295,'ROLE_PAYEE'),(296,'ROLE_PAYEE'),(297,'ROLE_PAYEE'),(298,'ROLE_PAYEE'),(300,'ROLE_PAYEE'),(304,'ROLE_PAYEE'),(305,'ROLE_PAYEE'),(306,'ROLE_PAYEE'),(307,'ROLE_PAYEE'),(308,'ROLE_PAYEE'),(309,'ROLE_PAYEE'),(310,'ROLE_PAYEE'),(315,'ROLE_PAYEE'),(316,'ROLE_PAYEE'),(317,'ROLE_PAYEE'),(318,'ROLE_PAYEE'),(321,'ROLE_PAYEE'),(322,'ROLE_PAYEE'),(323,'ROLE_PAYEE'),(327,'ROLE_PAYEE'),(328,'ROLE_PAYEE'),(329,'ROLE_PAYEE'),(331,'ROLE_PAYEE'),(332,'ROLE_PAYEE'),(333,'ROLE_PAYEE'),(334,'ROLE_PAYEE'),(335,'ROLE_PAYEE'),(336,'ROLE_PAYEE'),(337,'ROLE_PAYEE'),(338,'ROLE_PAYEE'),(339,'ROLE_PAYEE'),(340,'ROLE_PAYEE'),(341,'ROLE_PAYEE'),(342,'ROLE_PAYEE'),(343,'ROLE_PAYEE'),(344,'ROLE_PAYEE'),(345,'ROLE_PAYEE'),(346,'ROLE_PAYEE'),(347,'ROLE_PAYEE'),(348,'ROLE_PAYEE'),(349,'ROLE_PAYEE'),(350,'ROLE_PAYEE'),(351,'ROLE_PAYEE'),(352,'ROLE_PAYEE'),(353,'ROLE_PAYEE'),(355,'ROLE_PAYEE'),(356,'ROLE_PAYEE'),(357,'ROLE_PAYEE'),(361,'ROLE_PAYEE'),(362,'ROLE_PAYEE'),(363,'ROLE_PAYEE'),(364,'ROLE_PAYEE'),(365,'ROLE_PAYEE'),(366,'ROLE_PAYEE'),(367,'ROLE_PAYEE'),(368,'ROLE_PAYEE'),(369,'ROLE_PAYEE'),(370,'ROLE_PAYEE'),(371,'ROLE_PAYEE'),(372,'ROLE_PAYEE'),(373,'ROLE_PAYEE'),(374,'ROLE_PAYEE'),(375,'ROLE_PAYEE'),(376,'ROLE_PAYEE'),(387,'ROLE_PAYEE'),(388,'ROLE_PAYEE'),(389,'ROLE_PAYEE'),(390,'ROLE_PAYEE'),(391,'ROLE_PAYEE'),(392,'ROLE_PAYEE'),(393,'ROLE_PAYEE'),(394,'ROLE_PAYEE'),(395,'ROLE_PAYEE'),(396,'ROLE_PAYEE'),(397,'ROLE_PAYEE'),(398,'ROLE_PAYEE'),(399,'ROLE_PAYEE'),(400,'ROLE_PAYEE'),(402,'ROLE_PAYEE'),(403,'ROLE_PAYEE'),(404,'ROLE_PAYEE'),(405,'ROLE_PAYEE'),(406,'ROLE_PAYEE'),(407,'ROLE_PAYEE'),(408,'ROLE_PAYEE'),(409,'ROLE_PAYEE'),(411,'ROLE_PAYEE'),(5,'ROLE_SUPER_ADMIN');
/*!40000 ALTER TABLE `jhi_user_authority` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-04 15:42:26
