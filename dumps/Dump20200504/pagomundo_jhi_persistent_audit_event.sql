-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: pagomundo
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `jhi_persistent_audit_event`
--

DROP TABLE IF EXISTS `jhi_persistent_audit_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jhi_persistent_audit_event` (
  `event_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `principal` varchar(50) NOT NULL,
  `event_date` timestamp NULL DEFAULT NULL,
  `event_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`event_id`),
  KEY `idx_persistent_audit_event` (`principal`,`event_date`)
) ENGINE=InnoDB AUTO_INCREMENT=4636 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_persistent_audit_event`
--

LOCK TABLES `jhi_persistent_audit_event` WRITE;
/*!40000 ALTER TABLE `jhi_persistent_audit_event` DISABLE KEYS */;
INSERT INTO `jhi_persistent_audit_event` VALUES (4610,'ag.ucaliri@gmail.com','2020-04-08 00:12:23','AUTHENTICATION_SUCCESS'),(4611,'albertoceli+778899@gmail.com','2020-04-08 00:14:29','AUTHENTICATION_SUCCESS'),(4612,'albertoceli+778899@gmail.com','2020-04-08 00:23:34','AUTHENTICATION_SUCCESS'),(4613,'ag.ucaliri@gmail.com','2020-04-08 17:03:45','AUTHENTICATION_SUCCESS'),(4614,'ag.ucaliri@gmail.com','2020-04-08 17:08:36','AUTHENTICATION_SUCCESS'),(4615,'a.gucaliri@gmail.com','2020-04-08 17:28:27','AUTHENTICATION_SUCCESS'),(4616,'albertoceli+123456@gmail.com','2020-04-08 17:31:49','AUTHENTICATION_SUCCESS'),(4617,'a.gucaliri@gmail.com','2020-04-08 17:34:45','AUTHENTICATION_SUCCESS'),(4618,'ag.ucaliri@gmail.com','2020-04-21 22:36:31','AUTHENTICATION_SUCCESS'),(4619,'ag.ucaliri@gmail.com','2020-04-22 17:39:12','AUTHENTICATION_SUCCESS'),(4620,'alberto.celi+pruebatest@gmail.com','2020-04-22 17:41:53','AUTHENTICATION_SUCCESS'),(4621,'ag.ucaliri@gmail.com','2020-04-23 14:45:21','AUTHENTICATION_SUCCESS'),(4622,'a.gucaliri@gmail.com','2020-04-23 14:47:01','AUTHENTICATION_SUCCESS'),(4623,'a.gucaliri@gmail.com','2020-04-23 17:16:34','AUTHENTICATION_SUCCESS'),(4624,'ag.ucaliri@gmail.com','2020-05-04 18:15:44','AUTHENTICATION_SUCCESS'),(4625,'a.gucaliri@gmail.com','2020-05-04 18:16:27','AUTHENTICATION_SUCCESS'),(4626,'agu.caliri@gmail.com','2020-05-04 18:17:04','AUTHENTICATION_SUCCESS'),(4627,'ag.ucaliri@gmail.com','2020-05-04 18:17:41','AUTHENTICATION_SUCCESS'),(4628,'ag.ucaliri@gmail.com','2020-05-04 18:18:26','AUTHENTICATION_SUCCESS'),(4629,'a.gucaliri@gmail.com','2020-05-04 18:18:41','AUTHENTICATION_SUCCESS'),(4630,'ag.ucaliri@gmail.com','2020-05-04 18:25:36','AUTHENTICATION_SUCCESS'),(4631,'a.gucaliri@gmail.com','2020-05-04 18:26:07','AUTHENTICATION_SUCCESS'),(4632,'ag.ucaliri@gmail.com','2020-05-04 18:27:49','AUTHENTICATION_SUCCESS'),(4633,'a.gucaliri@gmail.com','2020-05-04 18:28:26','AUTHENTICATION_SUCCESS'),(4634,'agu.caliri@gmail.com','2020-05-04 18:35:34','AUTHENTICATION_SUCCESS'),(4635,'ag.ucaliri@gmail.com','2020-05-04 20:58:26','AUTHENTICATION_SUCCESS');
/*!40000 ALTER TABLE `jhi_persistent_audit_event` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-04 15:42:25
