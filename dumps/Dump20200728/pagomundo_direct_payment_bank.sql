-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: pagomundo
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `direct_payment_bank`
--

DROP TABLE IF EXISTS `direct_payment_bank`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `direct_payment_bank`
(
    `id`                bigint(20) NOT NULL AUTO_INCREMENT,
    `origin_bank_id`    bigint(20)   DEFAULT NULL,
    `destiny_bank_id`   bigint(20)   DEFAULT NULL,
    `destiny_city_id`   bigint(20)   DEFAULT NULL,
    `destiny_bank_code` varchar(255) DEFAULT NULL,
    `id_type_name`      varchar(255) DEFAULT NULL,
    `id_type_code`      varchar(255) DEFAULT NULL,
    `id_type_id`        bigint(20)   DEFAULT NULL,
    `city_code`         varchar(255) DEFAULT NULL,
    `department_code`   varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_direct_payment_bank_destiny_bank_id` (`destiny_bank_id`),
    KEY `fk_direct_payment_bank_destiny_city_id` (`destiny_city_id`),
    KEY `fk_direct_payment_bank_origin_bank_id` (`origin_bank_id`),
    KEY `fk_direct_payment_bank_id_type_id` (`id_type_id`),
    CONSTRAINT `fk_direct_payment_bank_destiny_bank_id` FOREIGN KEY (`destiny_bank_id`) REFERENCES `bank` (`id`),
    CONSTRAINT `fk_direct_payment_bank_destiny_city_id` FOREIGN KEY (`destiny_city_id`) REFERENCES `city` (`id`),
    CONSTRAINT `fk_direct_payment_bank_id_type_id` FOREIGN KEY (`id_type_id`) REFERENCES `id_type` (`id`),
    CONSTRAINT `fk_direct_payment_bank_origin_bank_id` FOREIGN KEY (`origin_bank_id`) REFERENCES `bank` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 101
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `direct_payment_bank`
--

LOCK TABLES `direct_payment_bank` WRITE;
/*!40000 ALTER TABLE `direct_payment_bank`
    DISABLE KEYS */;
INSERT INTO `direct_payment_bank`
VALUES (1, 2, 1, 1, '1060', 'CEDULA', '01', 1, '05001', '05'),
       (2, 2, 2, 13, '1014', 'NIT', '03', 16, '05045', '05'),
       (3, 2, 5, 16, '1001', NULL, NULL, NULL, NULL, NULL),
       (4, 2, 6, 19, '1002', NULL, NULL, NULL, '05088', '05'),
       (6, 2, 8, 23, '1009', NULL, NULL, NULL, NULL, NULL),
       (7, 2, 9, 35, '1012', NULL, NULL, NULL, '05154', '05'),
       (8, 2, 10, 40, '1013', NULL, NULL, NULL, NULL, NULL),
       (9, 2, 11, 47, '1006', NULL, NULL, NULL, '05266', '05'),
       (11, 2, 13, 59, '1023', NULL, NULL, NULL, '05360', '05'),
       (12, 2, 14, 61, '1031', NULL, NULL, NULL, '05364', '05'),
       (14, 2, 16, 85, '1040', NULL, NULL, NULL, NULL, NULL),
       (15, 2, 17, 87, '1051', NULL, NULL, NULL, '05631', '05'),
       (16, 2, 18, 126, '1052', NULL, NULL, NULL, '08001', '08'),
       (17, 2, 19, 145, '1053', NULL, NULL, NULL, '08758', '08'),
       (18, 2, 20, 149, '1058', NULL, NULL, NULL, '11001', '11'),
       (19, 2, 21, 150, '1059', NULL, NULL, NULL, '13001', '13'),
       (21, 2, 23, 196, '1062', NULL, NULL, NULL, '15001', '15'),
       (24, 2, 26, 227, '1292', NULL, NULL, NULL, '15238', '15'),
       (26, 2, 28, 293, '1507', NULL, NULL, NULL, '15759', '15'),
       (27, 2, 29, 319, '1551', NULL, NULL, NULL, '17001', '17'),
       (28, 2, 22, 346, '1061', NULL, NULL, NULL, NULL, NULL),
       (30, 2, 24, 362, '1065', NULL, NULL, NULL, '19001', '19'),
       (31, 2, 7, 366, '1007', NULL, NULL, NULL, NULL, NULL),
       (33, 2, 25, 404, '1066', NULL, NULL, NULL, '20001', '20'),
       (35, 2, 12, 429, '1019', NULL, NULL, NULL, '23001', '23'),
       (37, 2, 15, 467, '1032', NULL, NULL, NULL, '25126', '25'),
       (38, 2, 27, 472, '1502', NULL, NULL, NULL, '25175', '25'),
       (39, 2, NULL, 474, NULL, NULL, NULL, NULL, '25181', '25'),
       (40, 2, NULL, 477, NULL, NULL, NULL, NULL, '25214', '25'),
       (41, 2, NULL, 482, NULL, NULL, NULL, NULL, '25269', '25'),
       (42, 2, NULL, 485, NULL, NULL, NULL, NULL, '25286', '25'),
       (43, 2, NULL, 487, NULL, NULL, NULL, NULL, '25290', '25'),
       (44, 2, NULL, 492, NULL, NULL, NULL, NULL, '25307', '25'),
       (45, 2, NULL, 543, NULL, NULL, NULL, NULL, '25754', '25'),
       (46, 2, NULL, 557, NULL, NULL, NULL, NULL, '25817', '25'),
       (47, 2, NULL, 568, NULL, NULL, NULL, NULL, '25875', '25'),
       (48, 2, NULL, 572, NULL, NULL, NULL, NULL, '25899', '25'),
       (49, 2, NULL, 573, NULL, NULL, NULL, NULL, '27001', '27'),
       (50, 2, NULL, 603, NULL, NULL, NULL, NULL, '41001', '41'),
       (51, 2, NULL, 640, NULL, NULL, NULL, NULL, '44001', '44'),
       (52, 2, NULL, 655, NULL, NULL, NULL, NULL, '47001', '47'),
       (53, 2, NULL, 661, NULL, NULL, NULL, NULL, '47189', '47'),
       (55, 2, NULL, 685, NULL, NULL, NULL, NULL, '50001', '50'),
       (56, 2, NULL, 686, NULL, NULL, NULL, NULL, '50006', '50'),
       (58, 2, NULL, 714, NULL, NULL, NULL, NULL, '52001', '52'),
       (59, 2, NULL, 741, NULL, NULL, NULL, NULL, '52356', '52'),
       (62, 2, NULL, 778, NULL, NULL, NULL, NULL, '54001', '54'),
       (67, 2, NULL, 830, NULL, NULL, NULL, NULL, '66001', '66'),
       (69, 2, NULL, 844, NULL, NULL, NULL, NULL, '68001', '68'),
       (70, 2, NULL, 850, NULL, NULL, NULL, NULL, '68081', '68'),
       (73, 2, NULL, 876, NULL, NULL, NULL, NULL, '68276', '68'),
       (74, 2, NULL, 879, NULL, NULL, NULL, NULL, '68307', '68'),
       (76, 2, NULL, 913, NULL, NULL, NULL, NULL, '68679', '68'),
       (78, 2, NULL, 931, NULL, NULL, NULL, NULL, '70001', '70'),
       (79, 2, NULL, 957, NULL, NULL, NULL, NULL, '73001', '73'),
       (80, 2, NULL, 972, NULL, NULL, NULL, NULL, '73268', '73'),
       (81, 2, NULL, 974, NULL, NULL, NULL, NULL, '73275', '73'),
       (82, 2, NULL, 978, NULL, NULL, NULL, NULL, '73349', '73'),
       (83, 2, NULL, 995, NULL, NULL, NULL, NULL, '73671', '73'),
       (84, 2, NULL, 1004, NULL, NULL, NULL, NULL, '76001', '76'),
       (86, 2, NULL, 1010, NULL, NULL, NULL, NULL, '76109', '76'),
       (87, 2, NULL, 1011, NULL, NULL, NULL, NULL, '76111', '76'),
       (89, 2, NULL, 1016, NULL, NULL, NULL, NULL, '76147', '76'),
       (90, 2, NULL, 1030, NULL, NULL, NULL, NULL, '76520', '76'),
       (91, 2, NULL, 1039, NULL, NULL, NULL, NULL, '76834', '76'),
       (92, 2, NULL, 1044, NULL, NULL, NULL, NULL, '76892', '76'),
       (93, 2, NULL, 1053, NULL, NULL, NULL, NULL, '85001', '85'),
       (94, 2, NULL, 1054, NULL, NULL, NULL, NULL, '85010', '85'),
       (96, 2, NULL, 2975, NULL, NULL, NULL, NULL, NULL, NULL),
       (97, 2, NULL, 2976, NULL, NULL, NULL, NULL, NULL, NULL),
       (98, 2, NULL, 834, NULL, NULL, NULL, NULL, '66170', '66'),
       (99, 2, NULL, 561, NULL, NULL, NULL, NULL, '25843', '25'),
       (100, 2, NULL, 775, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `direct_payment_bank`
    ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2020-07-28 15:35:21
