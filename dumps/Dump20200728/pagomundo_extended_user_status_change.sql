-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: pagomundo
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `extended_user_status_change`
--

DROP TABLE IF EXISTS `extended_user_status_change`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `extended_user_status_change`
(
    `id`               bigint(20) NOT NULL AUTO_INCREMENT,
    `date_created`     datetime     DEFAULT NULL,
    `status`           varchar(255) DEFAULT NULL,
    `reason`           longtext,
    `extended_user_id` bigint(20)   DEFAULT NULL,
    `user_id`          bigint(20)   DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_extended_user_status_change_extended_user_id` (`extended_user_id`),
    KEY `fk_extended_user_status_change_user_id_id` (`user_id`),
    CONSTRAINT `fk_extended_user_status_change_extended_user_id` FOREIGN KEY (`extended_user_id`) REFERENCES `extended_user` (`id`),
    CONSTRAINT `fk_extended_user_status_change_user_id_id` FOREIGN KEY (`user_id`) REFERENCES `extended_user` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 971
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extended_user_status_change`
--

LOCK TABLES `extended_user_status_change` WRITE;
/*!40000 ALTER TABLE `extended_user_status_change`
    DISABLE KEYS */;
INSERT INTO `extended_user_status_change`
VALUES (960, '2020-06-29 13:05:02', 'ACCEPTED', NULL, 349, 93),
       (961, '2020-06-29 13:10:13', 'CREATED', NULL, 350, 349),
       (962, '2020-06-29 14:58:51', 'CREATED', NULL, 351, 349),
       (963, '2020-07-04 16:05:15', 'IN_PROCESS', NULL, 351, 93),
       (964, '2020-07-04 16:05:15', 'IN_PROCESS', NULL, 350, 93),
       (965, '2020-07-04 16:05:27', 'ACCEPTED', '', 351, 93),
       (966, '2020-07-04 16:05:30', 'ACCEPTED', '', 350, 93),
       (967, '2020-07-04 16:05:40', 'ACCEPTED', NULL, 352, 93),
       (968, '2020-07-04 16:05:41', 'ACCEPTED', NULL, 353, 93),
       (969, '2020-07-09 19:19:20', 'ACCEPTED', NULL, 354, 93),
       (970, '2020-07-17 20:21:35', 'CREATED', NULL, 355, 355);
/*!40000 ALTER TABLE `extended_user_status_change`
    ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2020-07-28 15:35:25
