-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: pagomundo
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `notification_status_change`
--

DROP TABLE IF EXISTS `notification_status_change`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notification_status_change`
(
    `id`              bigint(20)   NOT NULL AUTO_INCREMENT,
    `date_created`    datetime     NOT NULL,
    `status`          varchar(255) NOT NULL,
    `reason`          longtext,
    `notification_id` bigint(20) DEFAULT NULL,
    `user_id`         bigint(20) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_notification_status_change_notification_id` (`notification_id`),
    KEY `fk_notification_status_change_user_id` (`user_id`),
    CONSTRAINT `fk_notification_status_change_notification_id` FOREIGN KEY (`notification_id`) REFERENCES `notification` (`id`),
    CONSTRAINT `fk_notification_status_change_user_id` FOREIGN KEY (`user_id`) REFERENCES `extended_user` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 317
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification_status_change`
--

LOCK TABLES `notification_status_change` WRITE;
/*!40000 ALTER TABLE `notification_status_change`
    DISABLE KEYS */;
INSERT INTO `notification_status_change`
VALUES (288, '2020-06-29 13:05:03', 'CREATED', '', 280, 93),
       (289, '2020-06-29 13:10:14', 'CREATED', '', 281, 349),
       (290, '2020-06-29 14:58:53', 'CREATED', '', 282, 349),
       (291, '2020-06-29 14:58:53', 'CREATED', '', 283, 349),
       (292, '2020-06-29 18:26:09', 'CREATED', '', 284, 93),
       (293, '2020-06-29 18:27:22', 'CREATED', '', 285, 93),
       (294, '2020-07-04 16:05:28', 'CREATED', '', 286, 93),
       (295, '2020-07-04 16:05:30', 'CREATED', '', 287, 93),
       (296, '2020-07-04 16:05:41', 'CREATED', '', 288, 93),
       (297, '2020-07-04 16:05:42', 'CREATED', '', 289, 93),
       (298, '2020-07-04 16:05:42', 'CREATED', '', 290, 93),
       (299, '2020-07-04 16:05:42', 'CREATED', '', 291, 93),
       (300, '2020-07-08 14:43:27', 'CREATED', '', 292, 351),
       (301, '2020-07-08 14:49:01', 'CREATED', '', 293, 351),
       (302, '2020-07-08 14:49:45', 'CREATED', '', 294, 93),
       (303, '2020-07-08 14:57:30', 'CREATED', '', 295, 351),
       (304, '2020-07-08 14:58:35', 'CREATED', '', 296, 93),
       (305, '2020-07-08 14:58:37', 'CREATED', '', 297, 93),
       (306, '2020-07-09 19:19:30', 'CREATED', '', 298, 93),
       (307, '2020-07-09 19:21:40', 'CREATED', '', 299, 351),
       (308, '2020-07-09 19:23:02', 'CREATED', '', 300, 93),
       (309, '2020-07-13 21:04:23', 'CREATED', '', 301, 351),
       (310, '2020-07-13 21:15:44', 'CREATED', '', 302, 93),
       (311, '2020-07-13 21:19:01', 'CREATED', '', 303, 93),
       (312, '2020-07-17 20:17:48', 'CREATED', '', 304, 93),
       (313, '2020-07-17 20:21:36', 'CREATED', '', 305, 355),
       (314, '2020-07-18 15:38:26', 'CREATED', '', 306, 349),
       (316, '2020-07-18 15:47:04', 'CREATED', '', 308, 349);
/*!40000 ALTER TABLE `notification_status_change`
    ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2020-07-28 15:35:26
