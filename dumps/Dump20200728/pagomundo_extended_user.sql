-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: pagomundo
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `extended_user`
--

DROP TABLE IF EXISTS `extended_user`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `extended_user`
(
    `id`                             bigint(20) NOT NULL AUTO_INCREMENT,
    `status`                         varchar(255)   DEFAULT NULL,
    `last_name_1`                    varchar(255)   DEFAULT NULL,
    `last_name_2`                    varchar(255)   DEFAULT NULL,
    `first_name_1`                   varchar(255)   DEFAULT NULL,
    `first_name_2`                   varchar(255)   DEFAULT NULL,
    `full_name`                      varchar(255)   DEFAULT NULL,
    `id_number`                      varchar(255)   DEFAULT NULL,
    `gender`                         varchar(255)   DEFAULT NULL,
    `marital_status`                 varchar(255)   DEFAULT NULL,
    `residence_address`              varchar(255)   DEFAULT NULL,
    `postal_address`                 varchar(255)   DEFAULT NULL,
    `phone_number`                   varchar(255)   DEFAULT NULL,
    `mobile_number`                  varchar(255)   DEFAULT NULL,
    `birth_date`                     datetime       DEFAULT NULL,
    `date_created`                   datetime       DEFAULT NULL,
    `last_updated`                   datetime       DEFAULT NULL,
    `email`                          varchar(255)   DEFAULT NULL,
    `company`                        varchar(255)   DEFAULT NULL,
    `image_id_url`                   varchar(255)   DEFAULT NULL,
    `image_address_url`              varchar(255)   DEFAULT NULL,
    `balance`                        decimal(21, 2) DEFAULT NULL,
    `user_id`                        bigint(20)     DEFAULT NULL,
    `residence_city_id`              bigint(20)     DEFAULT NULL,
    `postal_city_id`                 bigint(20)     DEFAULT NULL,
    `branch_id`                      bigint(20)     DEFAULT NULL,
    `id_type_id`                     bigint(20)     DEFAULT NULL,
    `tax_id`                         varchar(255)   DEFAULT NULL,
    `extended_user_group_id`         bigint(20)     DEFAULT NULL,
    `status_reason`                  longtext,
    `role`                           varchar(255)   DEFAULT NULL,
    `card_number`                    varchar(255)   DEFAULT NULL,
    `bank_id`                        bigint(20)     DEFAULT NULL,
    `residence_country_id`           bigint(20)     DEFAULT NULL,
    `postal_country_id`              bigint(20)     DEFAULT NULL,
    `bank_commission`                float          DEFAULT NULL,
    `fx_commission`                  float          DEFAULT NULL,
    `recharge_cost`                  float          DEFAULT NULL,
    `use_merchant_commission`        tinyint(1)     DEFAULT '0',
    `bank_account_number`            varchar(255)   DEFAULT NULL,
    `direct_payment_bank_id`         bigint(20)     DEFAULT NULL,
    `direct_payment_city_id`         bigint(20)     DEFAULT NULL,
    `use_direct_payment`             tinyint(1)     DEFAULT '0',
    `bank_account_type`              varchar(255)   DEFAULT NULL,
    `can_change_payment_method`      tinyint(1)     DEFAULT '1',
    `must_notify`                    tinyint(1)     DEFAULT '1',
    `profile_info_changed`           blob,
    `id_type_tax_id_id`              bigint(20)     DEFAULT NULL,
    `confirmed_profile`              tinyint(1)     DEFAULT '0',
    `reseller_id`                    bigint(20)     DEFAULT NULL,
    `reseller_fixed_commission`      float          DEFAULT NULL,
    `reseller_percentage_commission` float          DEFAULT NULL,
    `fixed_commission`               tinyint(1)     DEFAULT '0',
    `reseller_commission`            float          DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `ux_extended_user_user_id` (`user_id`),
    KEY `fk_extended_user_residence_city_id` (`residence_city_id`),
    KEY `fk_extended_user_postal_city_id` (`postal_city_id`),
    KEY `fk_extended_user_branch_id` (`branch_id`),
    KEY `fk_extended_user_id_type_id` (`id_type_id`),
    KEY `fk_extended_user_extended_user_group_id` (`extended_user_group_id`),
    KEY `fk_extended_user_bank_id_id` (`bank_id`),
    KEY `fk_extended_user_residence_country_id` (`residence_country_id`),
    KEY `fk_extended_user_postal_country_id` (`postal_country_id`),
    KEY `fk_extended_user_direct_payment_bank_id` (`direct_payment_bank_id`),
    KEY `fk_extended_user_direct_payment_city_id` (`direct_payment_city_id`),
    KEY `fk_extended_user_id_type_tax_id_id` (`id_type_tax_id_id`),
    KEY `fk_extended_user_reseller_id` (`reseller_id`),
    CONSTRAINT `fk_extended_user_bank_id` FOREIGN KEY (`bank_id`) REFERENCES `bank` (`id`),
    CONSTRAINT `fk_extended_user_branch_id` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`),
    CONSTRAINT `fk_extended_user_direct_payment_bank_id` FOREIGN KEY (`direct_payment_bank_id`) REFERENCES `bank` (`id`),
    CONSTRAINT `fk_extended_user_direct_payment_city_id` FOREIGN KEY (`direct_payment_city_id`) REFERENCES `city` (`id`),
    CONSTRAINT `fk_extended_user_id_type_id` FOREIGN KEY (`id_type_id`) REFERENCES `id_type` (`id`),
    CONSTRAINT `fk_extended_user_id_type_tax_id_id` FOREIGN KEY (`id_type_tax_id_id`) REFERENCES `id_type` (`id`),
    CONSTRAINT `fk_extended_user_postal_city_id` FOREIGN KEY (`postal_city_id`) REFERENCES `city` (`id`),
    CONSTRAINT `fk_extended_user_postal_country_id` FOREIGN KEY (`postal_country_id`) REFERENCES `country` (`id`),
    CONSTRAINT `fk_extended_user_reseller_id` FOREIGN KEY (`reseller_id`) REFERENCES `extended_user` (`id`),
    CONSTRAINT `fk_extended_user_residence_city_id` FOREIGN KEY (`residence_city_id`) REFERENCES `city` (`id`),
    CONSTRAINT `fk_extended_user_residence_country_id` FOREIGN KEY (`residence_country_id`) REFERENCES `country` (`id`),
    CONSTRAINT `fk_extended_user_user_id` FOREIGN KEY (`user_id`) REFERENCES `jhi_user` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 356
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extended_user`
--

LOCK TABLES `extended_user` WRITE;
/*!40000 ALTER TABLE `extended_user`
    DISABLE KEYS */;
INSERT INTO `extended_user`
VALUES (93, 'ACCEPTED', 'Superadmin', NULL, 'Superadmin', NULL, 'Superadmin Superadmin', NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, '2019-11-13 17:16:51', '2019-11-13 18:47:55', 'alberto.celi+superadmin@gmail.com', NULL, NULL,
        NULL, 0.00, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ROLE_SUPER_ADMIN', NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, 0, NULL, NULL, NULL, 0, NULL, 1, 1, NULL, NULL, 1, NULL, NULL, NULL, 0, NULL),
       (94, 'ACCEPTED', 'Admin', NULL, 'Agu', NULL, 'Agu Admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        '2019-11-13 18:52:29', '2020-06-11 21:42:07', 'alberto.celi+admin001@gmail.com', NULL, NULL, NULL, 0.00, 105,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ROLE_ADMIN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL,
        NULL, 0, NULL, 1, 1, NULL, NULL, 1, NULL, NULL, NULL, 0, NULL),
       (349, 'ACCEPTED', 'reseller001', NULL, 'reseller001', NULL, 'reseller001 reseller001', 'reseller001', NULL, NULL,
        'reseller001 | reseller001', 'reseller001 | reseller001', NULL, 'reseller001', '1974-01-01 03:00:00',
        '2020-06-29 13:05:02', '2020-07-20 13:15:01', 'alberto.celi+reseller001@gmail.com', 'reseller001', NULL,
        'fileBucketDev/logo/349-A.jpg', 1000.00, 433, NULL, NULL, NULL, NULL, 'reseller001', NULL, NULL,
        'ROLE_RESELLER', NULL, NULL, 66, 66, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, 0, 1, '', NULL, 0, NULL,
        NULL, NULL, 0, NULL),
       (350, 'ACCEPTED', 'merchant001', NULL, 'merchant001', NULL, 'merchant001 merchant001', 'merchant001', NULL, NULL,
        'merchant001 | merchant001', 'merchant001 | merchant001', NULL, 'merchant001', '1982-02-16 03:00:00',
        '2020-06-29 13:10:13', '2020-07-20 13:15:01', 'alberto.celi+merchant001@gmail.com', 'merchant001', NULL,
        'fileBucketDev/logo/349-A.jpg', 0.00, 434, NULL, NULL, NULL, NULL, 'merchant001', NULL, '', 'ROLE_MERCHANT',
        NULL, NULL, 66, 66, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, 0, 1, '', NULL, 0, 349, NULL, NULL, 0,
        NULL),
       (351, 'ACCEPTED', 'merchant002', NULL, 'merchant002', NULL, 'merchant002 merchant002', 'merchant002', NULL, NULL,
        'merchant002 | merchant002', 'merchant002 | merchant002', NULL, 'merchant002', '1987-05-26 03:00:00',
        '2020-06-29 14:58:51', '2020-07-20 13:15:01', 'alberto.celi+merchant002@gmail.com', 'merchant002', NULL,
        'fileBucketDev/logo/349-A.jpg', 9530.00, 435, NULL, NULL, NULL, NULL, 'merchant002', NULL, '', 'ROLE_MERCHANT',
        NULL, NULL, 66, 66, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, 0, 1, '', NULL, 0, 349, NULL, NULL, 0,
        NULL),
       (352, 'ACCEPTED', 'payee001', NULL, 'payee001', NULL, 'payee001 payee001', 'payee001', NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, '2020-07-04 16:05:40', '2020-07-20 13:15:01', 'alberto.celi+payee001@gmail.com', NULL, NULL,
        NULL, 0.00, 436, NULL, NULL, NULL, 1, NULL, NULL, NULL, 'ROLE_PAYEE', NULL, NULL, NULL, 1, NULL, NULL, NULL, 0,
        'payee001', 16, 686, 1, 'AHO', 1, 1, '', NULL, 0, NULL, NULL, NULL, 0, NULL),
       (353, 'ACCEPTED', 'payee002', NULL, 'payee002', NULL, 'payee002 payee002', NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, '2020-07-04 16:05:41', '2020-07-20 13:15:01', 'alberto.celi+payee002@gmail.com', NULL, NULL, NULL,
        0.00, 437, NULL, NULL, NULL, NULL, 'payee002', NULL, NULL, 'ROLE_PAYEE', NULL, NULL, NULL, 1, NULL, NULL, NULL,
        0, 'payee002', 18, 1054, 1, 'AHO', 1, 1, '', 16, 0, NULL, NULL, NULL, 0, NULL),
       (354, 'ACCEPTED', 'payee005', NULL, 'payee005', NULL, 'payee005 payee005', '', NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, '2020-07-09 19:19:20', '2020-07-20 13:15:01', 'alberto.celi+payee005@gmail.com', NULL, NULL,
        'fileBucketDev/logo/349-A.jpg', 0.00, 440, NULL, NULL, NULL, 15, NULL, NULL, NULL, 'ROLE_PAYEE', NULL, NULL,
        NULL, 2, NULL, NULL, NULL, 0, '123456789012345678', NULL, NULL, 1, NULL, 1, 1, '', NULL, 0, NULL, NULL, NULL, 0,
        NULL),
       (355, 'IN_PROCESS', 'payee006', 'payee006', 'payee006', 'payee006', 'payee006 payee006', 'payee006', 'MALE',
        'SINGLE', 'payee006', 'payee006', 'payee006', 'payee006', '1997-05-30 03:00:00', '2020-07-17 20:21:35',
        '2020-07-20 13:15:01', 'alberto.celi+payee006@gmail.com', NULL, NULL, 'fileBucketDev/logo/349-A.jpg', 0.00, 441,
        149, 149, NULL, 1, 'payee006', 72, 'Invalid user status cycle: from CREATED to IN_PROCESS', 'ROLE_PAYEE', NULL,
        2, 1, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, 1, 0, '', NULL, 1, NULL, NULL, NULL, 0, NULL);
/*!40000 ALTER TABLE `extended_user`
    ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2020-07-28 15:35:18
