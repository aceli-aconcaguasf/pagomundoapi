-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: pagomundo
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `jhi_persistent_audit_event`
--

DROP TABLE IF EXISTS `jhi_persistent_audit_event`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jhi_persistent_audit_event`
(
    `event_id`   bigint(20)  NOT NULL AUTO_INCREMENT,
    `principal`  varchar(50) NOT NULL,
    `event_date` timestamp   NULL DEFAULT NULL,
    `event_type` varchar(255)     DEFAULT NULL,
    PRIMARY KEY (`event_id`),
    KEY `idx_persistent_audit_event` (`principal`, `event_date`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 4737
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_persistent_audit_event`
--

LOCK TABLES `jhi_persistent_audit_event` WRITE;
/*!40000 ALTER TABLE `jhi_persistent_audit_event`
    DISABLE KEYS */;
INSERT INTO `jhi_persistent_audit_event`
VALUES (4699, 'superadmin', '2020-06-29 00:44:09', 'AUTHENTICATION_SUCCESS'),
       (4700, 'superadmin', '2020-06-29 00:45:19', 'AUTHENTICATION_SUCCESS'),
       (4701, 'superadmin', '2020-06-29 15:21:34', 'AUTHENTICATION_SUCCESS'),
       (4702, 'superadmin', '2020-06-29 15:48:48', 'AUTHENTICATION_SUCCESS'),
       (4703, 'alberto.celi+reseller001@gmail.com', '2020-06-29 16:06:55', 'AUTHENTICATION_SUCCESS'),
       (4704, 'alberto.celi+reseller001@gmail.com', '2020-06-29 17:58:05', 'AUTHENTICATION_SUCCESS'),
       (4705, 'superadmin', '2020-06-29 21:25:14', 'AUTHENTICATION_SUCCESS'),
       (4706, 'superadmin', '2020-07-01 21:55:34', 'AUTHENTICATION_SUCCESS'),
       (4707, 'superadmin', '2020-07-01 21:56:13', 'AUTHENTICATION_SUCCESS'),
       (4708, 'superadmin', '2020-07-02 21:06:45', 'AUTHENTICATION_FAILURE'),
       (4709, 'superadmin', '2020-07-02 21:06:58', 'AUTHENTICATION_SUCCESS'),
       (4710, 'superadmin', '2020-07-04 19:00:32', 'AUTHENTICATION_SUCCESS'),
       (4711, 'superadmin', '2020-07-04 19:05:04', 'AUTHENTICATION_SUCCESS'),
       (4712, 'superadmin', '2020-07-05 17:01:02', 'AUTHENTICATION_SUCCESS'),
       (4713, 'superadmin', '2020-07-05 17:06:44', 'AUTHENTICATION_SUCCESS'),
       (4714, 'superadmin', '2020-07-06 00:20:01', 'AUTHENTICATION_SUCCESS'),
       (4715, 'superadmin', '2020-07-08 17:39:48', 'AUTHENTICATION_SUCCESS'),
       (4716, 'superadmin', '2020-07-08 17:42:13', 'AUTHENTICATION_SUCCESS'),
       (4717, 'alberto.celi+merchant002@gmail.com', '2020-07-08 17:43:09', 'AUTHENTICATION_FAILURE'),
       (4718, 'alberto.celi+merchant002@gmail.com', '2020-07-08 17:43:18', 'AUTHENTICATION_FAILURE'),
       (4719, 'alberto.celi+merchant002@gmail.com', '2020-07-08 17:48:06', 'AUTHENTICATION_SUCCESS'),
       (4720, 'alberto.celi+merchant002@gmail.com', '2020-07-08 17:49:56', 'AUTHENTICATION_SUCCESS'),
       (4721, 'superadmin', '2020-07-09 22:11:33', 'AUTHENTICATION_SUCCESS'),
       (4722, 'superadmin', '2020-07-09 22:12:08', 'AUTHENTICATION_SUCCESS'),
       (4723, 'alberto.celi+merchant002@gmail.com', '2020-07-09 22:20:48', 'AUTHENTICATION_SUCCESS'),
       (4724, 'superadmin', '2020-07-14 00:02:56', 'AUTHENTICATION_SUCCESS'),
       (4725, 'alberto.celi+merchant002@gmail.com', '2020-07-14 00:03:42', 'AUTHENTICATION_SUCCESS'),
       (4726, 'superadmin', '2020-07-14 17:41:58', 'AUTHENTICATION_SUCCESS'),
       (4727, 'alberto.celi+reseller001@gmail.com', '2020-07-14 21:20:59', 'AUTHENTICATION_SUCCESS'),
       (4728, 'superadmin', '2020-07-14 21:42:57', 'AUTHENTICATION_SUCCESS'),
       (4729, 'superadmin', '2020-07-15 16:22:01', 'AUTHENTICATION_SUCCESS'),
       (4730, 'superadmin', '2020-07-17 17:27:04', 'AUTHENTICATION_SUCCESS'),
       (4731, 'superadmin', '2020-07-17 23:16:06', 'AUTHENTICATION_SUCCESS'),
       (4732, 'alberto.celi+payee006@gmail.com', '2020-07-17 23:20:30', 'AUTHENTICATION_SUCCESS'),
       (4733, 'superadmin', '2020-07-18 17:51:33', 'AUTHENTICATION_SUCCESS'),
       (4734, 'alberto.celi+merchant002@gmail.com', '2020-07-18 17:52:24', 'AUTHENTICATION_SUCCESS'),
       (4735, 'alberto.celi+reseller001@gmail.com', '2020-07-18 18:35:59', 'AUTHENTICATION_SUCCESS'),
       (4736, 'superadmin', '2020-07-18 18:48:26', 'AUTHENTICATION_SUCCESS');
/*!40000 ALTER TABLE `jhi_persistent_audit_event`
    ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2020-07-28 15:35:24
