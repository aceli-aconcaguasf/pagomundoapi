-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: pagomundo
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `transaction_status_change`
--

DROP TABLE IF EXISTS `transaction_status_change`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaction_status_change`
(
    `id`             bigint(20)   NOT NULL AUTO_INCREMENT,
    `date_created`   datetime     NOT NULL,
    `status`         varchar(255) NOT NULL,
    `reason`         longtext,
    `transaction_id` bigint(20) DEFAULT NULL,
    `user_id`        bigint(20) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_transaction_status_change_transaction_id` (`transaction_id`),
    KEY `fk_transaction_status_change_user_id` (`user_id`),
    CONSTRAINT `fk_transaction_status_change_transaction_id` FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`id`),
    CONSTRAINT `fk_transaction_status_change_user_id` FOREIGN KEY (`user_id`) REFERENCES `extended_user` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1125
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_status_change`
--

LOCK TABLES `transaction_status_change` WRITE;
/*!40000 ALTER TABLE `transaction_status_change`
    DISABLE KEYS */;
INSERT INTO `transaction_status_change`
VALUES (1115, '2020-07-08 14:57:29', 'CREATED', NULL, 587, 351),
       (1116, '2020-07-08 14:57:30', 'CREATED', NULL, 588, 351),
       (1117, '2020-07-08 14:58:34', 'ACCEPTED', '', 588, 93),
       (1118, '2020-07-08 14:58:36', 'ACCEPTED', '', 587, 93),
       (1119, '2020-07-09 19:21:39', 'CREATED', NULL, 589, 351),
       (1120, '2020-07-09 19:23:00', 'ACCEPTED', '', 589, 93),
       (1121, '2020-07-13 21:04:20', 'CREATED', NULL, 590, 351),
       (1122, '2020-07-13 21:04:22', 'CREATED', NULL, 591, 351),
       (1123, '2020-07-13 21:15:37', 'ACCEPTED', '', 590, 93),
       (1124, '2020-07-13 21:18:57', 'REJECTED', 'asdasd', 591, 93);
/*!40000 ALTER TABLE `transaction_status_change`
    ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2020-07-28 15:35:24
