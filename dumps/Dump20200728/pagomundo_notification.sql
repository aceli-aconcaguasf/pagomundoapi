-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: pagomundo
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notification`
(
    `id`               bigint(20)   NOT NULL AUTO_INCREMENT,
    `subject`          varchar(255) NOT NULL,
    `description`      longtext,
    `date_created`     datetime     NOT NULL,
    `last_updated`     datetime     NOT NULL,
    `status`           varchar(255) NOT NULL,
    `status_reason`    longtext,
    `category`         varchar(255) NOT NULL,
    `sender_id`        bigint(20)   DEFAULT NULL,
    `receiver_id`      bigint(20)   DEFAULT NULL,
    `responsible_id`   bigint(20)   DEFAULT NULL,
    `notes`            longtext,
    `sub_category`     varchar(255) DEFAULT NULL,
    `extended_user_id` bigint(20)   DEFAULT NULL,
    `receiver_group`   varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_notification_sender_id` (`sender_id`),
    KEY `fk_notification_receiver_id` (`receiver_id`),
    KEY `fk_notification_responsible_id` (`responsible_id`),
    KEY `fk_notification_extended_user_id` (`extended_user_id`),
    CONSTRAINT `fk_notification_extended_user_id` FOREIGN KEY (`extended_user_id`) REFERENCES `extended_user` (`id`),
    CONSTRAINT `fk_notification_receiver_id` FOREIGN KEY (`receiver_id`) REFERENCES `extended_user` (`id`),
    CONSTRAINT `fk_notification_responsible_id` FOREIGN KEY (`responsible_id`) REFERENCES `extended_user` (`id`),
    CONSTRAINT `fk_notification_sender_id` FOREIGN KEY (`sender_id`) REFERENCES `extended_user` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 309
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification`
--

LOCK TABLES `notification` WRITE;
/*!40000 ALTER TABLE `notification`
    DISABLE KEYS */;
INSERT INTO `notification`
VALUES (280, '', '', '2020-06-29 13:05:03', '2020-06-29 13:05:03', 'CREATED', '', 'USER_ACCOUNT', 93, NULL, NULL, '',
        'USER_CREATED', NULL, 'RECEIVERS'),
       (281, '', '', '2020-06-29 13:10:14', '2020-06-29 13:10:14', 'CREATED', '', 'USER_ACCOUNT', 349, NULL, NULL, '',
        'USER_CREATED', NULL, 'RECEIVERS'),
       (282, '', '', '2020-06-29 14:58:53', '2020-06-29 14:58:53', 'CREATED', '', 'USER_ACCOUNT', 349, NULL, NULL, '',
        'USER_CREATED', NULL, 'RECEIVERS'),
       (283, 'There are users to be processed..', 'There is one created user to be processed.', '2020-06-29 14:58:53',
        '2020-06-29 14:58:53', 'CREATED', '', 'EXTENDED_USER', 349, NULL, NULL, '', 'EXTENDED_USER_CREATED', NULL,
        'RECEIVERS'),
       (284, 'prueba message', 'prueba message', '2020-06-29 18:26:09', '2020-06-29 18:26:09', 'CREATED', '', 'MESSAGE',
        93, NULL, NULL, '', 'MESSAGE', NULL, 'RECEIVERS'),
       (285, 'nuevo message', 'nuevo message', '2020-06-29 18:27:22', '2020-06-29 18:27:22', 'CREATED', '', 'MESSAGE',
        93, NULL, NULL, '', 'MESSAGE', NULL, 'ALL_MERCHANTS'),
       (286, 'Su cuenta de usuario ha sido aprobada.',
        'Estimado usuario, \n\nNos da gusto confirmarte que tu registro de usuario ha sido aprobado. \n\nDesde este momento tendras acceso a la plataforma de pagos PMI Americas. \n\nCordial saludo,\n\nSoporte PMI Americas\n.',
        '2020-07-04 16:05:28', '2020-07-04 16:05:28', 'CREATED', '', 'EXTENDED_USER', 93, NULL, NULL, '',
        'EXTENDED_USER_ACCEPTED', NULL, 'RECEIVERS'),
       (287, 'Su cuenta de usuario ha sido aprobada.',
        'Estimado usuario, \n\nNos da gusto confirmarte que tu registro de usuario ha sido aprobado. \n\nDesde este momento tendras acceso a la plataforma de pagos PMI Americas. \n\nCordial saludo,\n\nSoporte PMI Americas\n.',
        '2020-07-04 16:05:30', '2020-07-04 16:05:30', 'CREATED', '', 'EXTENDED_USER', 93, NULL, NULL, '',
        'EXTENDED_USER_ACCEPTED', NULL, 'RECEIVERS'),
       (288, '', '', '2020-07-04 16:05:41', '2020-07-04 16:05:41', 'CREATED', '', 'USER_ACCOUNT', 93, NULL, NULL, '',
        'USER_CREATED', NULL, 'RECEIVERS'),
       (289, '', '', '2020-07-04 16:05:42', '2020-07-04 16:05:42', 'CREATED', '', 'USER_ACCOUNT', 93, NULL, NULL, '',
        'USER_CREATED', NULL, 'RECEIVERS'),
       (290, '', '', '2020-07-04 16:05:42', '2020-07-04 16:05:42', 'CREATED', '', 'USER_ACCOUNT', 93, NULL, NULL, '',
        'USER_CREATED', NULL, 'RECEIVERS'),
       (291, '', '', '2020-07-04 16:05:42', '2020-07-04 16:05:42', 'CREATED', '', 'USER_ACCOUNT', 93, NULL, NULL, '',
        'USER_CREATED', NULL, 'RECEIVERS'),
       (292, 'User forgot password', '', '2020-07-08 14:43:27', '2020-07-08 14:43:27', 'CREATED', '', 'USER_ACCOUNT',
        351, NULL, NULL, '', 'USER_FORGOT_PASSWORD', NULL, 'RECEIVERS'),
       (293, 'There are funding to be processed.',
        'There is one requested funding to be processed from merchant merchant002 merchant002. Total amount: USD 10000',
        '2020-07-08 14:49:01', '2020-07-08 14:49:01', 'CREATED', '', 'FUND', 351, NULL, NULL, '', 'FUND_CREATED', NULL,
        NULL),
       (294, 'Your requested funding has been approved.', 'Your requested funding of USD 10000.00 has been approved.',
        '2020-07-08 14:49:45', '2020-07-08 14:49:45', 'CREATED', '', 'FUND', 93, NULL, NULL, '', 'FUND_ACCEPTED', NULL,
        NULL),
       (295, 'There are payments to be processed.',
        'There are 2 payments pending to be processed. Total amount: USD 200', '2020-07-08 14:57:30',
        '2020-07-08 14:57:30', 'CREATED', '', 'TRANSACTION', 351, NULL, NULL, '', 'TRANSACTION_CREATED', NULL,
        'RECEIVERS'),
       (296, 'You have a new completed payment.',
        'There is one completed payment from merchant002 merchant002. Total amount: COP 299866.19',
        '2020-07-08 14:58:35', '2020-07-08 14:58:35', 'CREATED', '', 'TRANSACTION', 93, NULL, NULL, '',
        'TRANSACTION_ACCEPTED', NULL, 'RECEIVERS'),
       (297, 'You have a new completed payment.',
        'There is one completed payment from merchant002 merchant002. Total amount: COP 299866.19',
        '2020-07-08 14:58:37', '2020-07-08 14:58:37', 'CREATED', '', 'TRANSACTION', 93, NULL, NULL, '',
        'TRANSACTION_ACCEPTED', NULL, 'RECEIVERS'),
       (298, '', '', '2020-07-09 19:19:30', '2020-07-09 19:19:30', 'CREATED', '', 'USER_ACCOUNT', 93, NULL, NULL, '',
        'USER_CREATED', NULL, 'RECEIVERS'),
       (299, 'There are payments to be processed.', 'There is 1 payment pending to be processed. Total amount: USD 150',
        '2020-07-09 19:21:40', '2020-07-09 19:21:40', 'CREATED', '', 'TRANSACTION', 351, NULL, NULL, '',
        'TRANSACTION_CREATED', NULL, 'RECEIVERS'),
       (300, 'You have a new completed payment.',
        'There is one completed payment from merchant002 merchant002. Total amount: MXN 17651608.00',
        '2020-07-09 19:23:02', '2020-07-09 19:23:02', 'CREATED', '', 'TRANSACTION', 93, NULL, NULL, '',
        'TRANSACTION_ACCEPTED', NULL, 'RECEIVERS'),
       (301, 'There are payments to be processed.',
        'There are 2 payments pending to be processed. Total amount: USD 240', '2020-07-13 21:04:23',
        '2020-07-13 21:04:23', 'CREATED', '', 'TRANSACTION', 351, NULL, NULL, '', 'TRANSACTION_CREATED', NULL,
        'RECEIVERS'),
       (302, 'You have a new completed payment.',
        'There is one completed payment from merchant002 merchant002. Total amount: COP 170787.20',
        '2020-07-13 21:15:44', '2020-07-13 21:15:44', 'CREATED', '', 'TRANSACTION', 93, NULL, NULL, '',
        'TRANSACTION_ACCEPTED', NULL, 'RECEIVERS'),
       (303, 'Your payment has been rejected.',
        'Dear Merchant,\n\nWe want to inform you that one of your recent payments has been rejected. Please log into your PMI Americas account for more details. \n\nIf you would like more information on this issue, please contact our support team. \n\nsupport@pmi-americas.com \n\nSincerely, \n\nAccount Manager\nPMI Americas\n',
        '2020-07-13 21:19:01', '2020-07-13 21:19:01', 'CREATED', '', 'TRANSACTION', 93, NULL, NULL, '',
        'TRANSACTION_REJECTED', NULL, 'RECEIVERS'),
       (304, '', '', '2020-07-17 20:17:48', '2020-07-17 20:17:48', 'CREATED', '', 'USER_ACCOUNT', 93, NULL, NULL, '',
        'USER_CREATED', NULL, 'RECEIVERS'),
       (305, 'There are users to be processed..', 'There is one created user to be processed.', '2020-07-17 20:21:36',
        '2020-07-17 20:21:36', 'CREATED', '', 'EXTENDED_USER', 355, NULL, NULL, '', 'EXTENDED_USER_CREATED', NULL,
        'RECEIVERS'),
       (306, 'There are withdrawals to be processed.',
        'There is one requested withdrawal to be processed from reseller reseller001 reseller001. Total amount: USD 500',
        '2020-07-18 15:38:26', '2020-07-18 15:38:26', 'CREATED', '', 'WITHDRAWAL', 349, NULL, NULL, '',
        'WITHDRAWAL_CREATED', NULL, NULL),
       (308, 'There are withdrawals to be processed.',
        'There is one requested withdrawal to be processed from reseller reseller001 reseller001. Total amount: USD 500',
        '2020-07-18 15:47:04', '2020-07-18 15:47:04', 'CREATED', '', 'WITHDRAWAL', 349, NULL, NULL, '',
        'WITHDRAWAL_CREATED', NULL, NULL);
/*!40000 ALTER TABLE `notification`
    ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2020-07-28 15:35:23
