-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: pagomundo
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `extended_user_relation`
--

DROP TABLE IF EXISTS `extended_user_relation`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `extended_user_relation`
(
    `id`               bigint(20) NOT NULL AUTO_INCREMENT,
    `extended_user_id` bigint(20)   DEFAULT NULL,
    `user_related_id`  bigint(20)   DEFAULT NULL,
    `status`           varchar(255) DEFAULT NULL,
    `status_reason`    longtext,
    `date_related`     datetime     DEFAULT NULL,
    `related_by_id`    bigint(20)   DEFAULT NULL,
    `date_unrelated`   datetime     DEFAULT NULL,
    `unrelated_by_id`  bigint(20)   DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_extended_user_relation_user_related_id` (`user_related_id`),
    KEY `fk_extended_user_relation_extended_user_id` (`extended_user_id`),
    KEY `fk_extended_user_relation_related_by_id` (`related_by_id`),
    KEY `fk_extended_user_relation_unrelated_by_id` (`unrelated_by_id`),
    CONSTRAINT `fk_extended_user_relation_extended_user_id` FOREIGN KEY (`extended_user_id`) REFERENCES `extended_user` (`id`),
    CONSTRAINT `fk_extended_user_relation_related_by_id` FOREIGN KEY (`related_by_id`) REFERENCES `extended_user` (`id`),
    CONSTRAINT `fk_extended_user_relation_unrelated_by_id` FOREIGN KEY (`unrelated_by_id`) REFERENCES `extended_user` (`id`),
    CONSTRAINT `fk_extended_user_relation_user_related_id` FOREIGN KEY (`user_related_id`) REFERENCES `jhi_user` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 615
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extended_user_relation`
--

LOCK TABLES `extended_user_relation` WRITE;
/*!40000 ALTER TABLE `extended_user_relation`
    DISABLE KEYS */;
INSERT INTO `extended_user_relation`
VALUES (593, 349, 434, 'CANCELLED', 'sad', '2020-07-05 14:23:26', 349, '2020-07-05 21:20:27', 93),
       (594, 350, 433, 'ACCEPTED', NULL, '2020-07-05 21:20:47', 93, '2020-07-05 21:20:26', 93),
       (595, 349, 435, 'ACCEPTED', NULL, '2020-06-29 14:58:51', 349, NULL, NULL),
       (596, 351, 433, 'ACCEPTED', NULL, '2020-06-29 14:58:51', 349, NULL, NULL),
       (597, 350, 436, 'ACCEPTED', NULL, '2020-07-04 16:05:39', 93, NULL, NULL),
       (598, 350, 437, 'ACCEPTED', NULL, '2020-07-04 16:05:39', 93, NULL, NULL),
       (599, 350, 438, 'ACCEPTED', NULL, '2020-07-04 16:05:39', 93, NULL, NULL),
       (600, 350, 439, 'ACCEPTED', NULL, '2020-07-04 16:05:40', 93, NULL, NULL),
       (601, 352, 434, 'ACCEPTED', NULL, '2020-07-04 16:05:40', 93, NULL, NULL),
       (602, 353, 434, 'ACCEPTED', NULL, '2020-07-04 16:05:41', 93, NULL, NULL),
       (603, 351, 437, 'ACCEPTED', NULL, '2020-07-08 14:56:42', 351, NULL, NULL),
       (604, 353, 435, 'ACCEPTED', NULL, '2020-07-08 14:56:42', 351, NULL, NULL),
       (605, 351, 436, 'ACCEPTED', NULL, '2020-07-08 14:56:42', 351, NULL, NULL),
       (606, 352, 435, 'ACCEPTED', NULL, '2020-07-08 14:56:42', 351, NULL, NULL),
       (609, 351, 438, 'ACCEPTED', NULL, '2020-07-09 19:18:01', 93, NULL, NULL),
       (610, 351, 439, 'ACCEPTED', NULL, '2020-07-09 19:18:43', 93, NULL, NULL),
       (611, 351, 440, 'ACCEPTED', NULL, '2020-07-09 19:19:20', 93, NULL, NULL),
       (612, 354, 435, 'ACCEPTED', NULL, '2020-07-09 19:19:20', 93, NULL, NULL),
       (613, 351, 441, 'ACCEPTED', NULL, '2020-07-17 20:17:48', 93, NULL, NULL),
       (614, 355, 435, 'ACCEPTED', NULL, '2020-07-17 20:21:36', 93, NULL, NULL);
/*!40000 ALTER TABLE `extended_user_relation`
    ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2020-07-28 15:35:27
