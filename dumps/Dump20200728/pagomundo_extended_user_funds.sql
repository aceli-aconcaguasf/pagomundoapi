-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: pagomundo
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `extended_user_funds`
--

DROP TABLE IF EXISTS `extended_user_funds`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `extended_user_funds`
(
    `id`               bigint(20) NOT NULL AUTO_INCREMENT,
    `date_created`     datetime       DEFAULT NULL,
    `value`            decimal(21, 2) DEFAULT NULL,
    `balance_before`   decimal(21, 2) DEFAULT NULL,
    `balance_after`    decimal(21, 2) DEFAULT NULL,
    `reason`           varchar(255)   DEFAULT NULL,
    `extended_user_id` bigint(20)     DEFAULT NULL,
    `user_id`          bigint(20)     DEFAULT NULL,
    `status`           varchar(255)   DEFAULT NULL,
    `category`         varchar(255)   DEFAULT NULL,
    `last_updated`     datetime       DEFAULT NULL,
    `type`             varchar(255)   DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_extended_user_funds_extended_user_id` (`extended_user_id`),
    KEY `fk_extended_user_funds_user_id` (`user_id`),
    CONSTRAINT `fk_extended_user_funds_extended_user_id` FOREIGN KEY (`extended_user_id`) REFERENCES `extended_user` (`id`),
    CONSTRAINT `fk_extended_user_funds_user_id` FOREIGN KEY (`user_id`) REFERENCES `jhi_user` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 555
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extended_user_funds`
--

LOCK TABLES `extended_user_funds` WRITE;
/*!40000 ALTER TABLE `extended_user_funds`
    DISABLE KEYS */;
INSERT INTO `extended_user_funds`
VALUES (545, '2020-07-08 14:49:00', 10000.00, 0.00, 10000.00, '', 351, 5, 'ACCEPTED', 'REQUESTED',
        '2020-07-08 14:49:44', 'FUNDING'),
       (546, '2020-07-08 14:57:29', -100.00, 10000.00, 9900.00, 'CREATED payment 587', 351, 435, 'ACCEPTED',
        'AUTOMATIC', '2020-07-08 14:57:29', 'FUNDING'),
       (547, '2020-07-08 14:57:30', -100.00, 9900.00, 9800.00, 'CREATED payment 588', 351, 435, 'ACCEPTED', 'AUTOMATIC',
        '2020-07-08 14:57:30', 'FUNDING'),
       (548, '2020-07-09 19:21:40', -150.00, 9800.00, 9650.00, 'CREATED payment 589', 351, 435, 'ACCEPTED', 'AUTOMATIC',
        '2020-07-09 19:21:40', 'FUNDING'),
       (549, '2020-07-13 21:04:21', -120.00, 9650.00, 9530.00, 'CREATED payment 590', 351, 435, 'ACCEPTED', 'AUTOMATIC',
        '2020-07-13 21:04:21', 'FUNDING'),
       (550, '2020-07-13 21:04:23', -120.00, 9530.00, 9410.00, 'CREATED payment 591', 351, 435, 'ACCEPTED', 'AUTOMATIC',
        '2020-07-13 21:04:23', 'FUNDING'),
       (551, '2020-07-13 21:18:57', 120.00, 9410.00, 9530.00, 'REJECTED payment 591', 351, 5, 'ACCEPTED', 'AUTOMATIC',
        '2020-07-13 21:18:57', 'FUNDING'),
       (552, '2020-07-18 15:38:26', 500.00, NULL, NULL, '', 349, NULL, 'CANCELLED', 'REQUESTED', '2020-07-18 15:48:07',
        'WITHDRAWAL'),
       (553, '2020-07-18 15:40:36', 500.00, NULL, NULL, '', 349, NULL, 'CANCELLED', 'REQUESTED', '2020-07-18 15:48:10',
        'WITHDRAWAL'),
       (554, '2020-07-18 15:46:46', 500.00, NULL, NULL, '', 349, NULL, 'CANCELLED', 'REQUESTED', '2020-07-18 15:48:12',
        'WITHDRAWAL');
/*!40000 ALTER TABLE `extended_user_funds`
    ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2020-07-28 15:35:20
