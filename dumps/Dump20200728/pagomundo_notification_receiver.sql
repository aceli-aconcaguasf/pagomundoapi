-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: pagomundo
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `notification_receiver`
--

DROP TABLE IF EXISTS `notification_receiver`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notification_receiver`
(
    `id`               bigint(20) NOT NULL AUTO_INCREMENT,
    `date_created`     datetime     DEFAULT NULL,
    `last_updated`     datetime     DEFAULT NULL,
    `retries`          int(11)      DEFAULT NULL,
    `max_retries`      int(11)      DEFAULT NULL,
    `status`           varchar(255) DEFAULT NULL,
    `reason`           longtext,
    `notification_id`  bigint(20)   DEFAULT NULL,
    `receiver_id`      bigint(20)   DEFAULT NULL,
    `must_send_email`  tinyint(1)   DEFAULT '0',
    `email_template`   varchar(255) DEFAULT NULL,
    `sender_id`        bigint(20)   DEFAULT NULL,
    `email_sent`       tinyint(1)   DEFAULT '0',
    `user_receiver_id` bigint(20)   DEFAULT NULL,
    `debug_reason`     varchar(255) DEFAULT NULL,
    `email_title_key`  varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_notification_receiver_notification_id` (`notification_id`),
    KEY `fk_notification_receiver_receiver_id` (`receiver_id`),
    KEY `fk_notification_receiver_sender_id` (`sender_id`),
    KEY `fk_notification_receiver_user_receiver_id` (`user_receiver_id`),
    CONSTRAINT `fk_notification_receiver_notification_id` FOREIGN KEY (`notification_id`) REFERENCES `notification` (`id`),
    CONSTRAINT `fk_notification_receiver_receiver_id` FOREIGN KEY (`receiver_id`) REFERENCES `extended_user` (`id`),
    CONSTRAINT `fk_notification_receiver_sender_id` FOREIGN KEY (`sender_id`) REFERENCES `extended_user` (`id`),
    CONSTRAINT `fk_notification_receiver_user_id` FOREIGN KEY (`user_receiver_id`) REFERENCES `jhi_user` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1576
  DEFAULT CHARSET = latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification_receiver`
--

LOCK TABLES `notification_receiver` WRITE;
/*!40000 ALTER TABLE `notification_receiver`
    DISABLE KEYS */;
INSERT INTO `notification_receiver`
VALUES (1510, '2020-06-29 13:05:03', '2020-06-29 13:05:24', 1, 3, 'CREATED', NULL, 280, NULL, 1,
        'mail/activationEmailToMerchant', NULL, 1, 433, NULL, 'email.activation.title'),
       (1511, '2020-06-29 13:05:03', '2020-06-29 13:05:03', 0, 3, 'CANCELLED', '', 280, NULL, 0, NULL, 93, 0, NULL,
        NULL, NULL),
       (1512, '2020-06-29 13:10:14', '2020-06-29 13:10:33', 1, 3, 'CREATED', NULL, 281, NULL, 1,
        'mail/activationEmailToMerchant', NULL, 1, 434, NULL, 'email.activation.title'),
       (1513, '2020-06-29 13:10:14', '2020-06-29 13:10:15', 0, 3, 'CANCELLED', '', 281, NULL, 0, NULL, 349, 0, NULL,
        NULL, NULL),
       (1514, '2020-06-29 14:58:53', '2020-06-29 14:59:15', 1, 3, 'CREATED', NULL, 283, 94, 1,
        'mail/extendedUserCreatedEmailToAdmin', NULL, 1, NULL, NULL, 'email.extendedUser.created.title'),
       (1515, '2020-06-29 14:58:53', '2020-06-29 14:59:28', 1, 3, 'CREATED', NULL, 282, NULL, 1,
        'mail/activationEmailToMerchant', NULL, 1, 435, NULL, 'email.activation.title'),
       (1516, '2020-06-29 14:58:53', '2020-06-29 14:58:53', 0, 3, 'CANCELLED', '', 282, NULL, 0, NULL, 349, 0, NULL,
        NULL, NULL),
       (1517, '2020-06-29 14:58:53', '2020-07-05 21:21:51', 1, 3, 'ACCEPTED', NULL, 283, 93, 1,
        'mail/extendedUserCreatedEmailToAdmin', NULL, 1, NULL, NULL, 'email.extendedUser.created.title'),
       (1518, '2020-06-29 14:58:53', '2020-06-29 14:58:53', 0, 3, 'CANCELLED', '', 283, NULL, 0, NULL, 349, 0, NULL,
        NULL, NULL),
       (1519, '2020-06-29 18:26:09', '2020-07-09 19:21:05', 0, 3, 'IN_PROCESS', '', 284, 351, 0, '', NULL, 0, NULL,
        NULL, ''),
       (1520, '2020-06-29 18:26:09', '2020-06-29 18:26:10', 0, 3, 'ACCEPTED', '', 284, NULL, 0, NULL, 93, 0, NULL, NULL,
        NULL),
       (1521, '2020-06-29 18:27:22', '2020-06-29 18:27:22', 0, 3, 'CREATED', '', 285, 350, 0, '', NULL, 0, NULL, NULL,
        ''),
       (1522, '2020-06-29 18:27:22', '2020-07-09 19:21:05', 0, 3, 'IN_PROCESS', '', 285, 351, 0, '', NULL, 0, NULL,
        NULL, ''),
       (1523, '2020-06-29 18:27:22', '2020-06-29 18:27:23', 0, 3, 'ACCEPTED', '', 285, NULL, 0, NULL, 93, 0, NULL, NULL,
        NULL),
       (1524, '2020-07-04 16:05:28', '2020-07-09 19:21:05', 0, 3, 'IN_PROCESS', '', 286, 351, 0, '', NULL, 0, NULL,
        NULL, ''),
       (1525, '2020-07-04 16:05:28', '2020-07-04 16:05:28', 0, 3, 'CANCELLED', '', 286, NULL, 0, NULL, 93, 0, NULL,
        NULL, NULL),
       (1526, '2020-07-04 16:05:30', '2020-07-04 16:05:30', 0, 3, 'CREATED', '', 287, 350, 0, '', NULL, 0, NULL, NULL,
        ''),
       (1527, '2020-07-04 16:05:30', '2020-07-04 16:05:30', 0, 3, 'CANCELLED', '', 287, NULL, 0, NULL, 93, 0, NULL,
        NULL, NULL),
       (1528, '2020-07-04 16:05:41', '2020-07-04 16:06:05', 1, 3, 'CREATED', NULL, 288, NULL, 1,
        'mail/activationEmailToPayee', NULL, 1, 436, NULL, 'email.activation.title'),
       (1529, '2020-07-04 16:05:41', '2020-07-04 16:05:41', 0, 3, 'CANCELLED', '', 288, NULL, 0, NULL, 93, 0, NULL,
        NULL, NULL),
       (1530, '2020-07-04 16:05:42', '2020-07-04 16:06:18', 1, 3, 'CREATED', NULL, 289, NULL, 1,
        'mail/activationEmailToPayee', NULL, 1, 437, NULL, 'email.activation.title'),
       (1531, '2020-07-04 16:05:42', '2020-07-04 16:05:42', 0, 3, 'CANCELLED', '', 289, NULL, 0, NULL, 93, 0, NULL,
        NULL, NULL),
       (1532, '2020-07-04 16:05:42', '2020-07-04 16:06:30', 1, 3, 'CREATED', NULL, 290, NULL, 1,
        'mail/activationEmailToPayee', NULL, 1, 438, NULL, 'email.activation.title'),
       (1533, '2020-07-04 16:05:42', '2020-07-04 16:05:42', 0, 3, 'CANCELLED', '', 290, NULL, 0, NULL, 93, 0, NULL,
        NULL, NULL),
       (1534, '2020-07-04 16:05:42', '2020-07-04 16:06:44', 1, 3, 'CREATED', NULL, 291, NULL, 1,
        'mail/activationEmailToPayee', NULL, 1, 439, NULL, 'email.activation.title'),
       (1535, '2020-07-04 16:05:42', '2020-07-04 16:05:42', 0, 3, 'CANCELLED', '', 291, NULL, 0, NULL, 93, 0, NULL,
        NULL, NULL),
       (1536, '2020-07-08 14:43:27', '2020-07-08 14:43:35', 1, 3, 'CREATED', NULL, 292, NULL, 1,
        'mail/passwordResetEmail.html', NULL, 1, 435, NULL, 'email.reset.title'),
       (1537, '2020-07-08 14:43:27', '2020-07-08 14:43:28', 0, 3, 'CANCELLED', '', 292, NULL, 0, NULL, 351, 0, NULL,
        NULL, NULL),
       (1538, '2020-07-08 14:49:01', '2020-07-08 14:49:14', 1, 3, 'CREATED', NULL, 293, 94, 1,
        'mail/fundCreatedEmailToAdmin', NULL, 1, NULL, NULL, 'email.fund.created.title'),
       (1539, '2020-07-08 14:49:01', '2020-07-08 14:49:18', 1, 3, 'IN_PROCESS', NULL, 293, 93, 1,
        'mail/fundCreatedEmailToAdmin', NULL, 1, NULL, NULL, 'email.fund.created.title'),
       (1540, '2020-07-08 14:49:01', '2020-07-08 14:49:01', 0, 3, 'CANCELLED', '', 293, NULL, 0, NULL, 351, 0, NULL,
        NULL, NULL),
       (1541, '2020-07-08 14:49:45', '2020-07-09 19:21:05', 0, 3, 'IN_PROCESS', '', 294, 351, 0, '', NULL, 0, NULL,
        NULL, ''),
       (1542, '2020-07-08 14:49:45', '2020-07-08 14:49:45', 0, 3, 'CANCELLED', '', 294, NULL, 0, NULL, 93, 0, NULL,
        NULL, NULL),
       (1543, '2020-07-08 14:57:30', '2020-07-08 14:57:44', 1, 3, 'CREATED', NULL, 295, 94, 1, '', NULL, 1, NULL, NULL,
        ''),
       (1544, '2020-07-08 14:57:30', '2020-07-08 14:57:52', 1, 3, 'IN_PROCESS', NULL, 295, 93, 1, '', NULL, 1, NULL,
        NULL, ''),
       (1545, '2020-07-08 14:57:30', '2020-07-08 14:57:31', 0, 3, 'CANCELLED', '', 295, NULL, 0, NULL, 351, 0, NULL,
        NULL, NULL),
       (1546, '2020-07-08 14:58:35', '2020-07-08 14:58:35', 0, 3, 'CREATED', '', 296, 352, 0, '', NULL, 0, NULL, NULL,
        ''),
       (1547, '2020-07-08 14:58:35', '2020-07-08 14:58:35', 0, 3, 'CANCELLED', '', 296, NULL, 0, NULL, 93, 0, NULL,
        NULL, NULL),
       (1548, '2020-07-08 14:58:37', '2020-07-08 14:58:37', 0, 3, 'CREATED', '', 297, 353, 0, '', NULL, 0, NULL, NULL,
        ''),
       (1549, '2020-07-08 14:58:37', '2020-07-08 14:58:37', 0, 3, 'CANCELLED', '', 297, NULL, 0, NULL, 93, 0, NULL,
        NULL, NULL),
       (1550, '2020-07-09 19:19:30', '2020-07-09 19:19:45', 1, 3, 'CREATED', NULL, 298, NULL, 1,
        'mail/activationEmailToPayee', NULL, 1, 440, NULL, 'email.activation.title'),
       (1551, '2020-07-09 19:19:30', '2020-07-09 19:19:30', 0, 3, 'CANCELLED', '', 298, NULL, 0, NULL, 93, 0, NULL,
        NULL, NULL),
       (1552, '2020-07-09 19:21:40', '2020-07-09 19:21:54', 1, 3, 'CREATED', NULL, 299, 94, 1, '', NULL, 1, NULL, NULL,
        ''),
       (1553, '2020-07-09 19:21:40', '2020-07-09 19:22:13', 1, 3, 'IN_PROCESS', NULL, 299, 93, 1, '', NULL, 1, NULL,
        NULL, ''),
       (1554, '2020-07-09 19:21:40', '2020-07-09 19:21:41', 0, 3, 'CANCELLED', '', 299, NULL, 0, NULL, 351, 0, NULL,
        NULL, NULL),
       (1555, '2020-07-09 19:23:02', '2020-07-09 19:23:02', 0, 3, 'CREATED', '', 300, 354, 0, '', NULL, 0, NULL, NULL,
        ''),
       (1556, '2020-07-09 19:23:02', '2020-07-09 19:23:02', 0, 3, 'CANCELLED', '', 300, NULL, 0, NULL, 93, 0, NULL,
        NULL, NULL),
       (1557, '2020-07-13 21:04:23', '2020-07-13 21:04:49', 1, 3, 'CREATED', NULL, 301, 94, 1, '', NULL, 1, NULL, NULL,
        ''),
       (1558, '2020-07-13 21:04:23', '2020-07-13 21:05:03', 1, 3, 'CREATED', NULL, 301, 93, 1, '', NULL, 1, NULL, NULL,
        ''),
       (1559, '2020-07-13 21:04:23', '2020-07-13 21:04:27', 0, 3, 'CANCELLED', '', 301, NULL, 0, NULL, 351, 0, NULL,
        NULL, NULL),
       (1560, '2020-07-13 21:15:44', '2020-07-13 21:15:45', 0, 3, 'CREATED', '', 302, 352, 0, '', NULL, 0, NULL, NULL,
        ''),
       (1561, '2020-07-13 21:15:44', '2020-07-13 21:15:45', 0, 3, 'CANCELLED', '', 302, NULL, 0, NULL, 93, 0, NULL,
        NULL, NULL),
       (1562, '2020-07-13 21:19:01', '2020-07-13 21:19:02', 0, 3, 'CREATED', '', 303, 353, 0, '', NULL, 0, NULL, NULL,
        ''),
       (1563, '2020-07-13 21:19:01', '2020-07-13 21:19:26', 1, 3, 'CREATED', NULL, 303, 351, 1,
        'mail/paymentRejectedEmailToMerchant', NULL, 1, NULL, NULL, 'email.payment.rejected.title'),
       (1564, '2020-07-13 21:19:01', '2020-07-13 21:19:02', 0, 3, 'CANCELLED', '', 303, NULL, 0, NULL, 93, 0, NULL,
        NULL, NULL),
       (1565, '2020-07-17 20:17:48', '2020-07-17 20:18:04', 1, 3, 'CREATED', NULL, 304, NULL, 1,
        'mail/activationEmailToPayee', NULL, 1, 441, NULL, 'email.activation.title'),
       (1566, '2020-07-17 20:17:48', '2020-07-17 20:17:49', 0, 3, 'CANCELLED', '', 304, NULL, 0, NULL, 93, 0, NULL,
        NULL, NULL),
       (1567, '2020-07-17 20:21:36', '2020-07-17 20:21:54', 1, 3, 'CREATED', NULL, 305, 94, 1,
        'mail/extendedUserCreatedEmailToAdmin', NULL, 1, NULL, NULL, 'email.extendedUser.created.title'),
       (1568, '2020-07-17 20:21:36', '2020-07-17 20:22:07', 1, 3, 'CREATED', NULL, 305, 93, 1,
        'mail/extendedUserCreatedEmailToAdmin', NULL, 1, NULL, NULL, 'email.extendedUser.created.title'),
       (1569, '2020-07-17 20:21:36', '2020-07-17 20:21:37', 0, 3, 'CANCELLED', '', 305, NULL, 0, NULL, 355, 0, NULL,
        NULL, NULL),
       (1570, '2020-07-18 15:38:26', '2020-07-18 15:38:45', 1, 3, 'CREATED', NULL, 306, 94, 1,
        'mail/withdrawalCreatedEmailToAdmin', NULL, 1, NULL, NULL, 'email.withdrawal.created.title'),
       (1571, '2020-07-18 15:38:26', '2020-07-18 15:38:58', 1, 3, 'CREATED', NULL, 306, 93, 1,
        'mail/withdrawalCreatedEmailToAdmin', NULL, 1, NULL, NULL, 'email.withdrawal.created.title'),
       (1572, '2020-07-18 15:38:26', '2020-07-18 15:38:27', 0, 3, 'CANCELLED', '', 306, NULL, 0, NULL, 349, 0, NULL,
        NULL, NULL),
       (1574, '2020-07-18 15:47:04', '2020-07-18 15:47:24', 1, 3, 'CREATED', NULL, 308, 93, 1,
        'mail/withdrawalCreatedEmailToAdmin', NULL, 1, NULL, NULL, 'email.withdrawal.created.title'),
       (1575, '2020-07-18 15:47:04', '2020-07-18 15:47:07', 0, 3, 'CANCELLED', '', 308, NULL, 0, NULL, 349, 0, NULL,
        NULL, NULL);
/*!40000 ALTER TABLE `notification_receiver`
    ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2020-07-28 15:35:22
