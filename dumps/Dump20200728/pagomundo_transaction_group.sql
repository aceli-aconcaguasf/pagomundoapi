-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: pagomundo
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `transaction_group`
--

DROP TABLE IF EXISTS `transaction_group`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaction_group`
(
    `id`                  bigint(20) NOT NULL AUTO_INCREMENT,
    `date_created`        datetime     DEFAULT NULL,
    `status`              varchar(255) DEFAULT NULL,
    `bank_account_id`     bigint(20)   DEFAULT NULL,
    `file_name`           varchar(255) DEFAULT NULL,
    `exchange_rate`       float        DEFAULT NULL,
    `admin_id`            bigint(20)   DEFAULT NULL,
    `file_name_from_bank` varchar(255) DEFAULT NULL,
    `direct_payment`      tinyint(1)   DEFAULT '0',
    PRIMARY KEY (`id`),
    KEY `fk_transaction_group_bank_account_id_index_5` (`bank_account_id`),
    KEY `fk_transaction_group_admin_id_index_5` (`admin_id`),
    CONSTRAINT `fk_transaction_group_bank_account_id` FOREIGN KEY (`bank_account_id`) REFERENCES `bank_account` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 211
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_group`
--

LOCK TABLES `transaction_group` WRITE;
/*!40000 ALTER TABLE `transaction_group`
    DISABLE KEYS */;
INSERT INTO `transaction_group`
VALUES (208, '2020-07-08 14:58:24', 'ACCEPTED', 2,
        'fileBucketDev/transaction/exportedFiles/2020_07_08_11_58_24_00-007468655-Itau-Colombia-DirectPayment.txt',
        3214, 93, NULL, 1),
       (209, '2020-07-09 19:22:42', 'ACCEPTED', 5,
        'fileBucketDev/transaction/exportedFiles/2020_07_09_16_22_42_00-STP-STP-Mexico-DirectPayment.txt', 123654, 93,
        NULL, 1),
       (210, '2020-07-13 21:05:33', 'ACCEPTED', 2,
        'fileBucketDev/transaction/exportedFiles/2020_07_13_18_05_33_00-007468655-Itau-Colombia-DirectPayment.txt',
        1520, 93, NULL, 1);
/*!40000 ALTER TABLE `transaction_group`
    ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2020-07-28 15:35:21
