-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: pagomundo
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `jhi_user`
--

DROP TABLE IF EXISTS `jhi_user`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jhi_user`
(
    `id`                 bigint(20)  NOT NULL AUTO_INCREMENT,
    `login`              varchar(50) NOT NULL,
    `password_hash`      varchar(60) NOT NULL,
    `first_name`         varchar(50)      DEFAULT NULL,
    `last_name`          varchar(50)      DEFAULT NULL,
    `email`              varchar(191)     DEFAULT NULL,
    `image_url`          varchar(256)     DEFAULT NULL,
    `activated`          bit(1)      NOT NULL,
    `lang_key`           varchar(10)      DEFAULT NULL,
    `activation_key`     varchar(20)      DEFAULT NULL,
    `reset_key`          varchar(20)      DEFAULT NULL,
    `created_by`         varchar(50) NOT NULL,
    `created_date`       timestamp   NULL DEFAULT NULL,
    `reset_date`         timestamp   NULL DEFAULT NULL,
    `last_modified_by`   varchar(50)      DEFAULT NULL,
    `last_modified_date` timestamp   NULL DEFAULT NULL,
    `payee_as_company`   tinyint(1)       DEFAULT '0',
    `role`               varchar(255)     DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `ux_user_login` (`login`),
    UNIQUE KEY `ux_user_email` (`email`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 442
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_user`
--

LOCK TABLES `jhi_user` WRITE;
/*!40000 ALTER TABLE `jhi_user`
    DISABLE KEYS */;
INSERT INTO `jhi_user`
VALUES (1, 'system', '$2a$10$mE.qmcV0mFU5NcKh73TZx.z4ueI/.bDWbj0T1BYyqP481kGGarKLG', 'System', 'System',
        'system@localhost', '', _binary '', 'en', NULL, NULL, 'system', NULL, NULL, 'system', NULL, 0, NULL),
       (2, 'anonymoususer', '$2a$10$j8S5d7Sr7.8VTOYNviDPOeWX8KcYILUVJBsYV83Y5NtECayypx9lO', 'Anonymous', 'User',
        'anonymous@localhost', '', _binary '', 'en', NULL, NULL, 'system', NULL, NULL, 'system', NULL, 0, NULL),
       (3, 'admin', '$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC', 'Administrator', 'Administrator',
        'admin@localhost', '', _binary '', 'en', NULL, NULL, 'system', NULL, NULL, 'system', NULL, 0, NULL),
       (4, 'user', '$2a$10$VEjxo0jq2YG9Rbk2HmX9S.k1uZBGYUHdUcid3g/vfiEl7lwWgOH/K', 'User', 'User', 'user@localhost', '',
        _binary '', 'en', NULL, NULL, 'system', NULL, NULL, 'system', NULL, 0, NULL),
       (5, 'superadmin', '$2a$10$f/IK7HpG6zSN2bbAd/qHF.8.lwYUfr/Z6XTRWl40rSqrOz4eV0O5S', 'Super', 'Administrator',
        'alberto.celi+superadmin@gmail.com', NULL, _binary '', 'en', NULL, NULL, 'system', NULL, NULL, 'anonymousUser',
        '2019-11-13 12:41:46', 0, NULL),
       (105, 'alberto.celi+admin001@gmail.com', '$2a$10$a/BlInicn6o5Tt1FHLEHaOQxgAFaqKhZaCCVu5yq51QJm5BKwH8WS', 'Agu',
        'Admin', 'alberto.celi+admin001@gmail.com', NULL, _binary '', 'en', NULL, 'us6YIbLXGnhtmF2RB5nZ', 'superadmin',
        '2019-11-13 18:37:47', '2019-11-14 18:59:10', 'superadmin', '2020-07-02 23:51:51', 0, 'ROLE_ADMIN'),
       (433, 'alberto.celi+reseller001@gmail.com', '$2a$10$EZ0vK.cy4Z9ZJ0UmsB/oweWBsMFYygkdb7XLE31OQ0LiYBVafeHte',
        'reseller001', 'reseller001', 'alberto.celi+reseller001@gmail.com', NULL, _binary '', 'en', NULL, NULL,
        'superadmin', '2020-06-29 16:05:02', NULL, 'superadmin', '2020-07-02 23:51:51', 0, 'ROLE_RESELLER'),
       (434, 'alberto.celi+merchant001@gmail.com', '$2a$10$Y5ZbIqOouG2hA99zLroLGO591EH/4IaVucGAB9ymkFRuHe3Q/LF8S',
        'merchant001', 'merchant001', 'alberto.celi+merchant001@gmail.com', NULL, _binary '', 'en', NULL, NULL,
        'alberto.celi+reseller001@gmail.com', '2020-06-29 16:10:12', NULL, 'superadmin', '2020-07-02 23:51:51', 0,
        'ROLE_MERCHANT'),
       (435, 'alberto.celi+merchant002@gmail.com', '$2a$10$RWvi2OXq6y7kqPoexZIiS.Ik1.0yt.z4hz3HaT/rB1SrJvmQ.nV5q',
        'merchant002', 'merchant002', 'alberto.celi+merchant002@gmail.com', NULL, _binary '', 'en', NULL, NULL,
        'alberto.celi+reseller001@gmail.com', '2020-06-29 17:58:50', NULL, 'anonymousUser', '2020-07-08 17:47:00', 0,
        'ROLE_MERCHANT'),
       (436, 'alberto.celi+payee001@gmail.com', '$2a$10$QVK7kCMs4YvpT78pP8DMmu90wXVKaEv782bh4A7qr06zTrEd84T/y',
        'payee001', 'payee001', 'alberto.celi+payee001@gmail.com', NULL, _binary '', 'es', NULL,
        '8dcVUmndJrS62HTh6QKK', 'superadmin', '2020-07-04 19:05:39', '2020-07-04 19:05:39', 'superadmin',
        '2020-07-04 19:05:39', 0, 'ROLE_PAYEE'),
       (437, 'alberto.celi+payee002@gmail.com', '$2a$10$u1sphi9M6W82bJlrnD8RRu0CA.e4wPX23TMj/RwpBtMAJC76JxU9u',
        'payee002', 'payee002', 'alberto.celi+payee002@gmail.com', NULL, _binary '', 'es', NULL,
        'ifnrYsmaAtq4CBmVRoSi', 'superadmin', '2020-07-04 19:05:39', '2020-07-04 19:05:39', 'superadmin',
        '2020-07-04 19:05:39', 1, 'ROLE_PAYEE'),
       (438, 'alberto.celi+payee003@gmail.com', '$2a$10$o94Mc58Zn9C9odx7L7j7mOk0cHyO.2Hy5LG8UnE2bHUqpkmndN80G',
        'payee003', 'payee003', 'alberto.celi+payee003@gmail.com', NULL, _binary '', 'es', NULL,
        'OZ5sUIIsa1gGAU1bAhXr', 'superadmin', '2020-07-04 19:05:39', '2020-07-04 19:05:39', 'superadmin',
        '2020-07-04 19:05:39', 0, 'ROLE_PAYEE'),
       (439, 'alberto.celi+payee004@gmail.com', '$2a$10$sflmmye4x20BW8bpQAwjxePk2l7ycC4ipmtdR/dp7IGbav31KaaZi',
        'payee004', 'payee004', 'alberto.celi+payee004@gmail.com', NULL, _binary '', 'es', NULL,
        'Vqj9l651U50bXzSz8UTu', 'superadmin', '2020-07-04 19:05:39', '2020-07-04 19:05:39', 'superadmin',
        '2020-07-04 19:05:39', 1, 'ROLE_PAYEE'),
       (440, 'alberto.celi+payee005@gmail.com', '$2a$10$YUenG6NCDktytZKZYuQu.uht0DFyVM/q/in.UtJmXZ9k8CoNkkdLm',
        'payee005', 'payee005', 'alberto.celi+payee005@gmail.com', NULL, _binary '', 'es', NULL,
        'Rtj6Cba5ks7Frny0RLuZ', 'superadmin', '2020-07-09 22:19:20', '2020-07-09 22:19:20', 'superadmin',
        '2020-07-09 22:19:20', 0, 'ROLE_PAYEE'),
       (441, 'alberto.celi+payee006@gmail.com', '$2a$10$9Yccd1aMR7UWFgp3aEvDlOxbxpzGNNBeQSYAmhO/hkpr0iAuQkwKS',
        'payee006', 'payee006', 'alberto.celi+payee006@gmail.com', NULL, _binary '', 'es', NULL, NULL, 'superadmin',
        '2020-07-17 23:17:48', NULL, 'anonymousUser', '2020-07-17 23:20:13', 0, 'ROLE_PAYEE');
/*!40000 ALTER TABLE `jhi_user`
    ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2020-07-28 15:35:23
