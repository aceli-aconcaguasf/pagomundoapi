INSERT INTO pagomundo.extended_user (id, status, last_name_1, last_name_2, first_name_1, first_name_2, full_name,
                                     id_number, gender, marital_status, residence_address, postal_address, phone_number,
                                     mobile_number, birth_date, date_created, last_updated, email, company,
                                     image_id_url, image_address_url, balance, user_id, residence_city_id,
                                     postal_city_id, branch_id, id_type_id, tax_id)
VALUES (1, 'ACCEPTED', 'System', '', 'System', '', 'System System', '', 'MALE', 'SINGLE', '', '', '', '',
        '2019-08-16 11:46:00', '2019-08-16 11:46:00', '2019-08-16 11:46:00', 'system@localhost', '', '', '', 0.00, 1, 1,
        1, 1, 1, null);
INSERT INTO pagomundo.extended_user (id, status, last_name_1, last_name_2, first_name_1, first_name_2, full_name,
                                     id_number, gender, marital_status, residence_address, postal_address, phone_number,
                                     mobile_number, birth_date, date_created, last_updated, email, company,
                                     image_id_url, image_address_url, balance, user_id, residence_city_id,
                                     postal_city_id, branch_id, id_type_id, tax_id)
VALUES (2, 'ACCEPTED', 'Admin', '', 'Super', '', 'Super Admin', '', 'MALE', 'SINGLE', '', '', '', '',
        '2019-08-16 11:46:00', '2019-08-16 11:46:00', '2019-08-16 11:46:00', 'anonymous@localhost', '', '', '', 0.00, 2,
        1, 1, 1, 1, null);
INSERT INTO pagomundo.extended_user (id, status, last_name_1, last_name_2, first_name_1, first_name_2, full_name,
                                     id_number, gender, marital_status, residence_address, postal_address, phone_number,
                                     mobile_number, birth_date, date_created, last_updated, email, company,
                                     image_id_url, image_address_url, balance, user_id, residence_city_id,
                                     postal_city_id, branch_id, id_type_id, tax_id)
VALUES (3, 'ACCEPTED', 'Admin', '', 'Admin', '', 'Admin Admin', '', 'MALE', 'SINGLE', '', '', '', '',
        '2019-08-16 11:46:00', '2019-08-16 11:46:00', '2019-08-16 11:46:00', 'admin@localhost', '', '', '', 0.00, 3, 1,
        1, 1, 1, null);
INSERT INTO pagomundo.extended_user (id, status, last_name_1, last_name_2, first_name_1, first_name_2, full_name,
                                     id_number, gender, marital_status, residence_address, postal_address, phone_number,
                                     mobile_number, birth_date, date_created, last_updated, email, company,
                                     image_id_url, image_address_url, balance, user_id, residence_city_id,
                                     postal_city_id, branch_id, id_type_id, tax_id)
VALUES (4, 'ACCEPTED', 'User', '', 'User', '', 'User User', '', 'MALE', 'SINGLE', '', '', '', '', '2019-08-16 11:46:00',
        '2019-08-16 11:46:00', '2019-08-16 11:46:00', 'letiRus@localhost', '', '', '', 0.00, 4, 1, 1, 1, 1, null);
INSERT INTO pagomundo.jhi_authority (name)
VALUES ('ROLE_ADMIN');
INSERT INTO pagomundo.jhi_authority (name)
VALUES ('ROLE_MERCHANT');
INSERT INTO pagomundo.jhi_authority (name)
VALUES ('ROLE_PAYEE');
INSERT INTO pagomundo.jhi_authority (name)
VALUES ('ROLE_RECIPIENT');
INSERT INTO pagomundo.jhi_authority (name)
VALUES ('ROLE_SUPER_ADMIN');
INSERT INTO pagomundo.jhi_authority (name)
VALUES ('ROLE_USER');
INSERT INTO pagomundo.jhi_user (id, login, password_hash, first_name, last_name, email, image_url, activated, lang_key,
                                activation_key, reset_key, created_by, created_date, reset_date, last_modified_by,
                                last_modified_date)
VALUES (1, 'system', '$2a$10$mE.qmcV0mFU5NcKh73TZx.z4ueI/.bDWbj0T1BYyqP481kGGarKLG', 'System', 'System',
        'system@localhost', '', true, 'en', null, null, 'system', null, null, 'admin', '2019-10-16 19:51:10');
INSERT INTO pagomundo.jhi_user (id, login, password_hash, first_name, last_name, email, image_url, activated, lang_key,
                                activation_key, reset_key, created_by, created_date, reset_date, last_modified_by,
                                last_modified_date)
VALUES (2, 'superadmin', '$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC', 'Super', 'Admin',
        'superadmin@localhost', '', true, 'en', null, null, 'system', null, null, 'system', null);
INSERT INTO pagomundo.jhi_user (id, login, password_hash, first_name, last_name, email, image_url, activated, lang_key,
                                activation_key, reset_key, created_by, created_date, reset_date, last_modified_by,
                                last_modified_date)
VALUES (3, 'admin', '$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC', 'Administrator', 'Administrator',
        'admin@localhost', '', true, 'en', null, null, 'system', null, null, 'system', null);
INSERT INTO pagomundo.jhi_user (id, login, password_hash, first_name, last_name, email, image_url, activated, lang_key,
                                activation_key, reset_key, created_by, created_date, reset_date, last_modified_by,
                                last_modified_date)
VALUES (4, 'user', '$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC', 'User', 'User', 'user@localhost', '',
        true, 'en', null, null, 'system', null, null, 'system', null);
INSERT INTO pagomundo.jhi_user_authority (user_id, authority_name)
VALUES (1, 'ROLE_ADMIN');
INSERT INTO pagomundo.jhi_user_authority (user_id, authority_name)
VALUES (3, 'ROLE_ADMIN');
INSERT INTO pagomundo.jhi_user_authority (user_id, authority_name)
VALUES (2, 'ROLE_SUPER_ADMIN');
INSERT INTO pagomundo.jhi_user_authority (user_id, authority_name)
VALUES (1, 'ROLE_USER');
INSERT INTO pagomundo.jhi_user_authority (user_id, authority_name)
VALUES (3, 'ROLE_USER');
INSERT INTO pagomundo.jhi_user_authority (user_id, authority_name)
VALUES (4, 'ROLE_USER');
