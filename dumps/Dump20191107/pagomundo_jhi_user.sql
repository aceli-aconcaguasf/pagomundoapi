-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: pagomundo
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `jhi_user`
--

DROP TABLE IF EXISTS `jhi_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jhi_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL,
  `password_hash` varchar(60) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(191) DEFAULT NULL,
  `image_url` varchar(256) DEFAULT NULL,
  `activated` bit(1) NOT NULL,
  `lang_key` varchar(10) DEFAULT NULL,
  `activation_key` varchar(20) DEFAULT NULL,
  `reset_key` varchar(20) DEFAULT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `reset_date` timestamp NULL DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_user_login` (`login`),
  UNIQUE KEY `ux_user_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_user`
--

LOCK TABLES `jhi_user` WRITE;
/*!40000 ALTER TABLE `jhi_user` DISABLE KEYS */;
INSERT INTO `jhi_user` VALUES (1,'system','$2a$10$mE.qmcV0mFU5NcKh73TZx.z4ueI/.bDWbj0T1BYyqP481kGGarKLG','System','System','system@localhost','',_binary '','en',NULL,NULL,'system',NULL,NULL,'admin','2019-10-16 22:51:10'),(2,'superadmin','$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC','Super','Admin','superadmin@localhost','',_binary '','en',NULL,NULL,'system',NULL,NULL,'system',NULL),(3,'admin','$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC','Administrator','Administrator','admin@localhost','',_binary '','en',NULL,NULL,'system',NULL,NULL,'system',NULL),(4,'user','$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC','User','User','user@localhost','',_binary '','en',NULL,NULL,'system',NULL,NULL,'system',NULL),(51,'albertoceli@gmail.com','$2a$10$3FGbE1YlJMCj71CTT3s7HOE5dMHK4TEesx3CjDCyDEvX7udORvBy2','Admin','Admin','albertoceli@gmail.com',NULL,_binary '','en',NULL,'Mv1cCH6uPAb6tvuDlfNg','superadmin','2019-10-31 14:28:15','2019-11-04 14:07:20','a.l.bertoceli@gmail.com','2019-11-04 14:07:20'),(52,'a.lbertoceli@gmail.com','$2a$10$pBVm1Uc0W5uBzorB9Gtw..CceTmJwzMJEFagvVH2A4WAvz.t.iRQG','Merchant','Merchant','a.lbertoceli@gmail.com',NULL,_binary '','en',NULL,'sg6yAOx8zEtzGqG0RiUG','superadmin','2019-10-31 14:36:16','2019-11-04 14:16:48','a.lbertoceli@gmail.com','2019-11-04 14:16:48'),(53,'a.l.bertoceli@gmail.com','$2a$10$KAcRUx9ZuUOY4yuAQ0aEHO2XsFK74wb5rzJ7Y1Iby3QuX8a6tXg5a','Merchant','Merchant','a.l.bertoceli@gmail.com',NULL,_binary '','es',NULL,'V6YcpXNt7EOWvtMglNku','a.lbertoceli@gmail.com','2019-10-31 14:42:01','2019-11-04 14:19:29','a.l.bertoceli@gmail.com','2019-11-04 14:19:29'),(54,'a.l.b.ertoceli@gmail.com','$2a$10$89yUp6fQKBntnSPPvhIMZ.wNLw7OJePid34xiGXDL6/tHAHeP7KUK','Payee2','Payee2','a.l.b.ertoceli@gmail.com',NULL,_binary '','es',NULL,'a7wNLpfqrE5KZhFWjohJ','a.lbertoceli@gmail.com','2019-10-31 14:50:47','2019-11-04 14:25:41','a.l.b.ertoceli@gmail.com','2019-11-04 14:25:41'),(55,'a.l.b.ertocel.i@gmail.com','$2a$10$w9S7fIS2GNAcOVLNw3oQM.qwP/xdtBynW69ds2IXmUUli/Ybutkci','Payee3','Payee3','a.l.b.ertocel.i@gmail.com',NULL,_binary '','es',NULL,'cvJzwER4bYFXA8XLXhWq','a.lbertoceli@gmail.com','2019-10-31 17:42:33','2019-10-31 17:42:33','a.lbertoceli@gmail.com','2019-10-31 17:42:33'),(56,'a.l.b.ertoce.l.i@gmail.com','$2a$10$l4cKXN2SM0GloEgsJmzUeOO01TcQv7LNnIoaBLSquhGMX5Y2xuFmu','Payee4','Payee4','a.l.b.ertoce.l.i@gmail.com',NULL,_binary '','es',NULL,'HwPjjZcw66CZdWRFmtRI','a.lbertoceli@gmail.com','2019-10-31 17:42:47','2019-10-31 17:42:47','a.lbertoceli@gmail.com','2019-10-31 17:42:47'),(57,'a.l.b.ertoc.el.i@gmail.com','$2a$10$LMU4hNniIWcI7H.mM33jf./YnvzWNF5zJJjtnjxvthGsw2LmicHPK','Payee5','Payee5','a.l.b.ertoc.el.i@gmail.com',NULL,_binary '','es',NULL,'3K9j5P3lYsaK0Z84wLSc','a.lbertoceli@gmail.com','2019-10-31 17:55:25','2019-10-31 17:55:25','a.lbertoceli@gmail.com','2019-10-31 17:55:25'),(58,'a.l.b.ertoc.e.l.i@gmail.com','$2a$10$lTIignwQDEMkY.JP68aSnOnyesbWVB2dguVA1qItZTVyuej9gqC8m','Payee6','Payee6','a.l.b.ertoc.e.l.i@gmail.com',NULL,_binary '','es',NULL,'2mg4t0qlqe42Xn1PlnaD','a.lbertoceli@gmail.com','2019-10-31 17:55:25','2019-10-31 17:55:25','a.lbertoceli@gmail.com','2019-10-31 17:55:25'),(59,'a.l.b.er.toc.el.i@gmail.com','$2a$10$.BYLRxsQv.dV.O5j6XCc/ONbqCugIph37x22xe./ZxX/.PBY5A/DS','Payee7','Payee7','a.l.b.er.toc.el.i@gmail.com',NULL,_binary '','es',NULL,'4crzcubx6RxvkHLIwBDK','a.lbertoceli@gmail.com','2019-10-31 18:16:24','2019-10-31 18:16:24','a.lbertoceli@gmail.com','2019-10-31 18:16:24'),(60,'a.l.b.er.toc.e.l.i@gmail.com','$2a$10$XDkxi1Lxtb8eeYeO4Yl/sO5DECvgqqO/Bf0r8H8Qv23GNkBJCKQUa','Payee8','Payee8','a.l.b.er.toc.e.l.i@gmail.com',NULL,_binary '','es',NULL,'KVRflyq3UkxTBTs4giA8','a.lbertoceli@gmail.com','2019-10-31 18:16:25','2019-10-31 18:16:25','a.lbertoceli@gmail.com','2019-10-31 18:16:25'),(61,'a.l.b.er.t.oc.el.i@gmail.com','$2a$10$zIXg4tUlMLaL3mDr13CUPOWcje5KYgJejM1/KVzfgD.2AXeH.xo9C','Payee9','Payee9','a.l.b.er.t.oc.el.i@gmail.com',NULL,_binary '','es',NULL,'JxtibVmrUI0uqK37ekEx','a.lbertoceli@gmail.com','2019-10-31 18:17:24','2019-10-31 18:17:24','a.lbertoceli@gmail.com','2019-10-31 18:17:24'),(62,'a.l.b.er.t.oc.e.l.i@gmail.com','$2a$10$wSH0nF20d27Zoikj5reRbuOEmubhnpXDzrXE4oBH/O7kNPWZCTrfe','Payee10','Payee10','a.l.b.er.t.oc.e.l.i@gmail.com',NULL,_binary '','es',NULL,'omYro70HpVR8dP1PdAIj','a.lbertoceli@gmail.com','2019-10-31 18:17:24','2019-10-31 18:17:24','a.lbertoceli@gmail.com','2019-10-31 18:17:24'),(63,'a.l.b.e.r.t.oc.el.i@gmail.com','$2a$10$4u6E4M7yhcSEQM8u0WDRLeuhtVvBxAi7ZJ7drLOfYwfkL9det6ZIG','Payee11','Payee11','a.l.b.e.r.t.oc.el.i@gmail.com',NULL,_binary '','es',NULL,'P9FuWD3V14bAWx3LAt4Q','a.lbertoceli@gmail.com','2019-10-31 21:23:05','2019-10-31 21:23:05','a.lbertoceli@gmail.com','2019-10-31 21:23:05'),(64,'a.l.b.e.r.t.oc.e.l.i@gmail.com','$2a$10$DQbFDZdMyMsvkC6WlNwpueH.ScNcpPp4uIYUNczo/oOkbVUVTwcZy','Payee12','Payee12','a.l.b.e.r.t.oc.e.l.i@gmail.com',NULL,_binary '','es',NULL,'MzAJuJvbs057E93z2ked','a.lbertoceli@gmail.com','2019-10-31 21:23:05','2019-10-31 21:23:05','a.lbertoceli@gmail.com','2019-10-31 21:23:05'),(65,'aceli@aconcaguasf.com','$2a$10$Wp8GFOg1mExvGWfQwOIhjuKNdLXGz/is8v/ZKnKKqT3dKHZFsQQHW','Payee12','Payee12','aceli@aconcaguasf.com',NULL,_binary '','es',NULL,'dPgC5tDpmziNaeiOO2ya','a.lbertoceli@gmail.com','2019-10-31 21:39:03','2019-10-31 21:39:03','a.lbertoceli@gmail.com','2019-10-31 21:39:03'),(67,'alber.t.oc.el.i@gmail.com','$2a$10$lxdrh4UmwvgVKOMk3EHPweNLIx3aaQU2DXR1SucVYcQEetorNLMvm','Payee15','Payee15','alber.t.oc.el.i@gmail.com',NULL,_binary '','es',NULL,'moQ8m3KAKEMK6KvnNo05','a.lbertoceli@gmail.com','2019-10-31 22:21:22','2019-10-31 22:21:22','a.lbertoceli@gmail.com','2019-10-31 22:21:22'),(68,'alber.t.oc.e.l.i@gmail.com','$2a$10$0jEUCDC93TdOIj/AadpZre..QjMl9iRjAcAGBlO/5mssy5o1I7HCq','Payee16','Payee16','alber.t.oc.e.l.i@gmail.com',NULL,_binary '','es',NULL,'VB3fM8JjnJX8zbsVHGZl','a.lbertoceli@gmail.com','2019-10-31 22:21:23','2019-10-31 22:21:23','a.lbertoceli@gmail.com','2019-10-31 22:21:23'),(69,'a.lber.t.oc.el.i@gmail.com','$2a$10$7YoWT4VMLF4HLc/ocpR6NOX45CamJ3usWNvdCX2HhZrtY6neEX3Ia','Payee17','Payee17','a.lber.t.oc.el.i@gmail.com',NULL,_binary '','es',NULL,'VkSMi3YExmLvhtflwKpj','a.lbertoceli@gmail.com','2019-10-31 22:35:32','2019-10-31 22:35:32','a.lbertoceli@gmail.com','2019-10-31 22:35:32'),(70,'a.lber.t.oc.e.l.i@gmail.com','$2a$10$a9TGtBVy7SOJoANtsDpd0e5TqoNDxwZq6NGox8MPm/R7OUJvnWRIK','Payee18','Payee18','a.lber.t.oc.e.l.i@gmail.com',NULL,_binary '','es',NULL,'Jh1A6lotzEbM0TzBRDPu','a.lbertoceli@gmail.com','2019-10-31 22:35:32','2019-10-31 22:35:32','a.lbertoceli@gmail.com','2019-10-31 22:35:32'),(71,'a.lbe.r.t.oc.el.i@gmail.com','$2a$10$7vgkVQhwJXWVNF0PA2h1wO.f3mwSFpiDGmrTTDqDwywMdh3wpw0uK','Payee19','Payee19','a.lbe.r.t.oc.el.i@gmail.com',NULL,_binary '','es',NULL,'QvIEauVvymseVmfL3xOC','a.lbertoceli@gmail.com','2019-10-31 22:44:54','2019-10-31 22:44:54','a.lbertoceli@gmail.com','2019-10-31 22:44:54'),(72,'a.lbe.r.t.oc.e.l.i@gmail.com','$2a$10$AIKaGEco3AfTEHMj9CRooeDwaNHWJhsQqG5zlBXSWI1Aku.FQyHA2','Payee20','Payee20','a.lbe.r.t.oc.e.l.i@gmail.com',NULL,_binary '','es',NULL,'TgBIkQXq0OlRs8pMlPBx','a.lbertoceli@gmail.com','2019-10-31 22:44:54','2019-10-31 22:44:54','a.lbertoceli@gmail.com','2019-10-31 22:44:54');
/*!40000 ALTER TABLE `jhi_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-07 16:40:07
