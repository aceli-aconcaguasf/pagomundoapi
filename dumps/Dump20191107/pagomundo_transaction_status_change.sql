-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: pagomundo
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `transaction_status_change`
--

DROP TABLE IF EXISTS `transaction_status_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaction_status_change` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_created` datetime NOT NULL,
  `status` varchar(255) NOT NULL,
  `reason` longtext,
  `transaction_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_transaction_status_change_transaction_id` (`transaction_id`),
  KEY `fk_transaction_status_change_user_id` (`user_id`),
  CONSTRAINT `fk_transaction_status_change_transaction_id` FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`id`),
  CONSTRAINT `fk_transaction_status_change_user_id` FOREIGN KEY (`user_id`) REFERENCES `extended_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=217 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_status_change`
--

LOCK TABLES `transaction_status_change` WRITE;
/*!40000 ALTER TABLE `transaction_status_change` DISABLE KEYS */;
INSERT INTO `transaction_status_change` VALUES (196,'2019-10-31 13:02:32','CREATED',NULL,102,31),(197,'2019-10-31 13:02:33','CREATED',NULL,103,31),(198,'2019-10-31 13:05:09','IN_PROCESS',NULL,103,30),(199,'2019-10-31 13:05:09','IN_PROCESS',NULL,102,30),(200,'2019-10-31 14:46:58','CREATED',NULL,104,31),(201,'2019-10-31 14:46:59','CREATED',NULL,105,31),(202,'2019-11-06 14:40:52','CREATED',NULL,106,31),(203,'2019-11-06 14:49:13','CANCELLED','The merchant is the only one role who can cancel a payment.',106,3),(204,'2019-11-06 14:49:55','CREATED',NULL,107,31),(205,'2019-11-06 14:51:42','REJECTED','The admin is the only one role who can accept a payment.',107,3),(206,'2019-11-06 14:52:13','CREATED',NULL,108,31),(207,'2019-11-06 14:56:49','IN_PROCESS',NULL,108,31),(208,'2019-11-06 14:58:15','ACCEPTED','The admin is the only one role who can accept a payment.',108,3),(209,'2019-11-06 14:58:31','ACCEPTED','The admin is the only one role who can accept a payment.',108,3),(210,'2019-11-06 14:59:07','CREATED',NULL,109,31),(211,'2019-11-06 14:59:09','CREATED',NULL,110,31),(212,'2019-11-06 14:59:40','IN_PROCESS',NULL,109,31),(213,'2019-11-06 14:59:40','IN_PROCESS',NULL,110,31),(214,'2019-11-06 14:59:56','REJECTED','The admin is the only one role who can accept a payment.',109,3),(215,'2019-11-06 15:22:30','ACCEPTED','The merchant is the only one role who can cancel a payment.',110,3),(216,'2019-11-06 15:27:15','CREATED',NULL,111,31);
/*!40000 ALTER TABLE `transaction_status_change` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-07 16:40:08
