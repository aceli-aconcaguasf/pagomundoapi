-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: pagomundo
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `extended_user_status_change`
--

DROP TABLE IF EXISTS `extended_user_status_change`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `extended_user_status_change` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_created` datetime NOT NULL,
  `status` varchar(255) NOT NULL,
  `reason` longtext,
  `extended_user_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_extended_user_status_change_extended_user_id` (`extended_user_id`),
  KEY `fk_extended_user_status_change_user_id` (`user_id`),
  CONSTRAINT `fk_extended_user_status_change_extended_user_id` FOREIGN KEY (`extended_user_id`) REFERENCES `extended_user` (`id`),
  CONSTRAINT `fk_extended_user_status_change_user_id` FOREIGN KEY (`user_id`) REFERENCES `extended_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extended_user_status_change`
--

LOCK TABLES `extended_user_status_change` WRITE;
/*!40000 ALTER TABLE `extended_user_status_change` DISABLE KEYS */;
INSERT INTO `extended_user_status_change` VALUES (1,'2019-11-06 15:38:55','CREATED',NULL,38,33),(2,'2019-11-06 15:39:55','REJECTED',NULL,38,31),(3,'2019-11-06 15:40:56','ACCEPTED',NULL,38,31),(4,'2019-11-06 19:40:23','CANCELLED',NULL,38,30),(5,'2019-11-07 15:46:00','CREATED',NULL,39,33),(6,'2019-11-07 15:50:53','IN_PROCESS',NULL,39,31),(7,'2019-11-07 17:40:33','CREATED',NULL,40,33),(8,'2019-11-07 17:40:47','CREATED',NULL,41,33),(9,'2019-11-07 17:40:58','CREATED',NULL,42,33),(10,'2019-11-07 17:43:03','CREATED',NULL,43,33),(11,'2019-11-07 17:43:40','CREATED',NULL,44,33),(17,'2019-11-07 17:49:02','CREATED',NULL,40,31),(18,'2019-11-07 17:49:02','CREATED',NULL,41,31),(19,'2019-11-07 17:49:02','CREATED',NULL,42,31),(20,'2019-11-07 17:49:02','CREATED',NULL,43,31),(21,'2019-11-07 17:49:03','CREATED',NULL,44,31),(22,'2019-11-07 17:49:33','IN_PROCESS',NULL,40,31),(23,'2019-11-07 17:49:33','IN_PROCESS',NULL,41,31),(24,'2019-11-07 17:49:33','IN_PROCESS',NULL,42,31),(25,'2019-11-07 17:49:33','IN_PROCESS',NULL,43,31),(26,'2019-11-07 17:49:33','IN_PROCESS',NULL,44,31);
/*!40000 ALTER TABLE `extended_user_status_change` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-07 16:40:08
