-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: pagomundo
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `extended_user`
--

DROP TABLE IF EXISTS `extended_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `extended_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status` varchar(255) DEFAULT NULL,
  `last_name_1` varchar(255) DEFAULT NULL,
  `last_name_2` varchar(255) DEFAULT NULL,
  `first_name_1` varchar(255) DEFAULT NULL,
  `first_name_2` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `id_number` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `marital_status` varchar(255) DEFAULT NULL,
  `residence_address` varchar(255) DEFAULT NULL,
  `postal_address` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `mobile_number` varchar(255) DEFAULT NULL,
  `birth_date` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `last_updated` datetime DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `image_id_url` varchar(255) DEFAULT NULL,
  `image_address_url` varchar(255) DEFAULT NULL,
  `balance` decimal(21,2) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `residence_city_id` bigint(20) DEFAULT NULL,
  `postal_city_id` bigint(20) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `id_type_id` bigint(20) DEFAULT NULL,
  `tax_id` varchar(255) DEFAULT NULL,
  `extended_user_group_id` bigint(20) DEFAULT NULL,
  `status_reason` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_extended_user_user_id` (`user_id`),
  KEY `fk_extended_user_residence_city_id` (`residence_city_id`),
  KEY `fk_extended_user_postal_city_id` (`postal_city_id`),
  KEY `fk_extended_user_branch_id` (`branch_id`),
  KEY `fk_extended_user_id_type_id` (`id_type_id`),
  KEY `fk_extended_user_extended_user_group_id` (`extended_user_group_id`),
  CONSTRAINT `fk_extended_user_branch_id` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`id`),
  CONSTRAINT `fk_extended_user_id_type_id` FOREIGN KEY (`id_type_id`) REFERENCES `id_type` (`id`),
  CONSTRAINT `fk_extended_user_postal_city_id` FOREIGN KEY (`postal_city_id`) REFERENCES `city` (`id`),
  CONSTRAINT `fk_extended_user_residence_city_id` FOREIGN KEY (`residence_city_id`) REFERENCES `city` (`id`),
  CONSTRAINT `fk_extended_user_user_id` FOREIGN KEY (`user_id`) REFERENCES `jhi_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extended_user`
--

LOCK TABLES `extended_user` WRITE;
/*!40000 ALTER TABLE `extended_user` DISABLE KEYS */;
INSERT INTO `extended_user` VALUES (1,'ACCEPTED','System','','System','','System System','','MALE','SINGLE','','','','','2019-08-16 11:46:00','2019-08-16 11:46:00','2019-08-16 11:46:00','system@localhost','','','',0.00,1,1,1,1,1,NULL,NULL,NULL),(2,'ACCEPTED','Admin','','Super','','Super Admin','','MALE','SINGLE','','','','','2019-08-16 11:46:00','2019-08-16 11:46:00','2019-08-16 11:46:00','anonymous@localhost','','','',0.00,2,1,1,1,1,NULL,NULL,NULL),(3,'ACCEPTED','Admin','','Admin','','Admin Admin','','MALE','SINGLE','','','','','2019-08-16 11:46:00','2019-08-16 11:46:00','2019-11-06 14:49:14','admin@localhost','','','',1000.00,3,1,1,1,1,NULL,NULL,NULL),(4,'ACCEPTED','User','','User','','User User','','MALE','SINGLE','','','','','2019-08-16 11:46:00','2019-08-16 11:46:00','2019-08-16 11:46:00','letiRus@localhost','','','',0.00,4,1,1,1,1,NULL,NULL,NULL),(30,'ACCEPTED','Admin',NULL,'Admin',NULL,'Admin Admin',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-10-31 11:33:23','2019-10-31 11:33:23','albertoceli@gmail.com',NULL,NULL,NULL,0.00,51,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(31,'ACCEPTED','Merchant',NULL,'Merchant',NULL,'Merchant Merchant',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-10-31 11:38:55','2019-11-06 15:27:15','a.lbertoceli@gmail.com',NULL,NULL,NULL,3000.00,52,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(32,'ACCEPTED','Payee',NULL,'Payee',NULL,'Payee Payee',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-10-31 11:44:10','2019-10-31 11:46:18','a.l.bertoceli@gmail.com',NULL,NULL,NULL,0.00,53,NULL,1,NULL,NULL,NULL,NULL,NULL),(33,'ACCEPTED','Payee2',NULL,'Payee2',NULL,'Payee2 Payee2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-10-31 11:53:41','2019-11-04 14:40:59','a.l.b.ertoceli@gmail.com',NULL,NULL,NULL,0.00,54,NULL,1,NULL,NULL,NULL,NULL,NULL),(34,'CREATED',NULL,NULL,NULL,NULL,'null null',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-11-04 15:18:18','2019-11-04 15:18:18','a.l.b.ertocel.i@gmail.com',NULL,NULL,NULL,0.00,55,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(36,'CREATED','lastName',NULL,'firstName',NULL,'firstName lastName',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-11-05 12:20:56','2019-11-05 12:20:56','a.l.b.ertoc.el.i@gmail.com',NULL,NULL,NULL,0.00,57,NULL,1,NULL,NULL,NULL,NULL,NULL),(38,'CANCELLED','payee',NULL,'payee',NULL,'payee payee',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-11-06 15:38:55','2019-11-06 19:40:23','a.l.b.ertoc.e.l.i@gmail.com',NULL,NULL,NULL,0.00,58,NULL,1,NULL,NULL,NULL,NULL,NULL),(39,'IN_PROCESS','payeeLastName1','payeeLastName2','payeeFirstName1','payeeFirstName2','payeeFirstName1 payeeLastName1','idNumber',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-11-07 15:46:00','2019-11-07 15:58:48','a.l.b.er.toc.el.i@gmail.com',NULL,NULL,NULL,0.00,59,1,1,NULL,1,NULL,NULL,NULL),(40,'IN_PROCESS','payeeLastName1','payeeLastName2','payeeFirstName1','payeeFirstName2','payeeFirstName1 payeeLastName1','idNumber',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-11-07 17:40:33','2019-11-07 17:49:33','a.l.b.er.toc.e.l.i@gmail.com',NULL,NULL,NULL,0.00,60,1,1,NULL,1,NULL,NULL,NULL),(41,'IN_PROCESS','payeeLastName1','payeeLastName2','payeeFirstName1','payeeFirstName2','payeeFirstName1 payeeLastName1','idNumber',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-11-07 17:40:47','2019-11-07 17:49:33','a.l.b.er.t.oc.el.i@gmail.com',NULL,NULL,NULL,0.00,61,1,1,NULL,1,NULL,NULL,NULL),(42,'IN_PROCESS','payeeLastName1','payeeLastName2','payeeFirstName1','payeeFirstName2','payeeFirstName1 payeeLastName1','idNumber',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-11-07 17:40:58','2019-11-07 17:49:33','alber.t.oc.e.l.i@gmail.com',NULL,NULL,NULL,0.00,68,1,1,NULL,1,NULL,NULL,NULL),(43,'IN_PROCESS','Sosa ','Sosa','Juan','Pablo','Juan Sosa ','123789','MALE','SINGLE','calle 1','calle 2',NULL,'123456','1986-06-17 00:00:00','2019-11-07 17:43:03','2019-11-07 17:49:33','aceli@aconcaguasf.com',NULL,NULL,NULL,0.00,65,22,89,NULL,1,NULL,NULL,NULL),(44,'IN_PROCESS','Sosa ','Sosa','Juan','Pablo','Juan Sosa ','123789','FEMALE','MARRIED','calle 1','calle 2',NULL,'123456','1986-06-17 00:00:00','2019-11-07 17:43:40','2019-11-07 17:49:33','a.l.b.e.r.t.oc.el.i@gmail.com',NULL,NULL,NULL,0.00,63,22,89,NULL,1,NULL,NULL,NULL);
/*!40000 ALTER TABLE `extended_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-07 16:40:04
