-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: pagomundo
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaction` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_created` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `status` varchar(255) NOT NULL,
  `status_reason` longtext,
  `amount_before_commission` decimal(21,2) DEFAULT NULL,
  `bank_commission` float DEFAULT NULL,
  `fx_commission` float DEFAULT NULL,
  `recharge_cost` float DEFAULT NULL,
  `amount_after_commission` decimal(21,2) DEFAULT NULL,
  `exchange_rate` float DEFAULT NULL,
  `amount_local_currency` decimal(21,2) DEFAULT NULL,
  `bank_reference` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `currency_id` bigint(20) DEFAULT NULL,
  `merchant_id` bigint(20) DEFAULT NULL,
  `payee_id` bigint(20) DEFAULT NULL,
  `admin_id` bigint(20) DEFAULT NULL,
  `bank_account_id` bigint(20) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `notes` longtext,
  `transaction_group_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_transaction_currency_id` (`currency_id`),
  KEY `fk_transaction_merchant_id` (`merchant_id`),
  KEY `fk_transaction_admin_id` (`admin_id`),
  KEY `fk_transaction_bank_account_id` (`bank_account_id`),
  KEY `fk_transaction_transaction_group_id_index_5` (`transaction_group_id`),
  KEY `fk_transaction_payee_id` (`payee_id`),
  CONSTRAINT `fk_transaction_admin_id` FOREIGN KEY (`admin_id`) REFERENCES `extended_user` (`id`),
  CONSTRAINT `fk_transaction_bank_account_id` FOREIGN KEY (`bank_account_id`) REFERENCES `bank_account` (`id`),
  CONSTRAINT `fk_transaction_currency_id` FOREIGN KEY (`currency_id`) REFERENCES `currency` (`id`),
  CONSTRAINT `fk_transaction_merchant_id` FOREIGN KEY (`merchant_id`) REFERENCES `extended_user` (`id`),
  CONSTRAINT `fk_transaction_payee_id` FOREIGN KEY (`payee_id`) REFERENCES `extended_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction`
--

LOCK TABLES `transaction` WRITE;
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
INSERT INTO `transaction` VALUES (102,'2019-10-31 13:02:32','2019-10-31 13:05:09','IN_PROCESS',NULL,200.00,0.02,0.015,2,191.00,3400,649400.00,NULL,NULL,1,31,33,30,1,'Desc. for this test transaction',NULL,51),(103,'2019-10-31 13:02:33','2019-10-31 13:05:09','IN_PROCESS',NULL,300.00,0.02,0.015,2,287.50,3400,977500.00,NULL,NULL,1,31,32,30,1,'Desc. for this test transaction',NULL,51),(104,'2019-10-31 14:46:58','2019-10-31 14:46:58','CREATED',NULL,200.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,31,33,NULL,NULL,'Desc. for this test transaction',NULL,NULL),(105,'2019-10-31 14:46:59','2019-10-31 14:46:59','CREATED',NULL,300.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,31,32,NULL,NULL,'Desc. for this test transaction',NULL,NULL),(106,'2019-11-06 14:40:52','2019-11-06 14:49:13','CANCELLED','The merchant is the only one role who can cancel a payment.',1000.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,3,33,NULL,NULL,'Dscription 1000','Put your notes here!',NULL),(107,'2019-11-06 14:49:55','2019-11-06 14:51:42','REJECTED','The admin is the only one role who can accept a payment.',1000.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,31,33,3,NULL,'Dscription 1000','Put yourddddd notes here!',NULL),(108,'2019-11-06 14:52:13','2019-11-06 14:58:31','ACCEPTED','The admin is the only one role who can accept a payment.',1000.00,0.02,0.015,2,963.00,19.17,18460.71,NULL,NULL,1,31,33,3,1,'Dscription 1000',NULL,52),(109,'2019-11-06 14:59:07','2019-11-06 14:59:56','REJECTED','The admin is the only one role who can accept a payment.',1000.00,0.02,0.015,2,963.00,10,9630.00,NULL,NULL,1,31,33,3,1,'Dscription 1000',NULL,53),(110,'2019-11-06 14:59:09','2019-11-06 15:22:30','ACCEPTED','The merchant is the only one role who can cancel a payment.',1000.00,0.02,0.015,2,963.00,10,9630.00,NULL,NULL,1,31,33,3,1,'Dscription 1000',NULL,53),(111,'2019-11-06 15:27:15','2019-11-06 15:27:15','CREATED',NULL,1000.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,31,33,NULL,NULL,'Dscription 1000',NULL,NULL);
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-07 16:40:06
