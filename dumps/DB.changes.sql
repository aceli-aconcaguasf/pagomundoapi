alter table bank_account
    modify column bank_commission float;
alter table bank_account
    modify column fx_commission float;
alter table bank_account
    modify column recharge_cost float;

#---------------------------------

alter table transaction
    add description VARCHAR(255);
alter table transaction
    add notes BLOB;

alter table transaction
    add transaction_group_id bigint;
create index fk_transaction_transaction_group_id_index_5 on transaction (transaction_group_id asc);
alter table transaction
    add constraint fk_transaction_transaction_group_id foreign key (transaction_group_id) references transaction_group (id);


#--------------------------------

alter table transaction_group
    add bank_account_id bigint;
create index fk_transaction_group_bank_account_id_index_5 on transaction_group (bank_account_id asc);

alter table transaction_group
    add file_name varchar(255);

#Executed 2019.09.27
#-------------------------------

alter table transaction_group
    add transaction_exchange float;
#--------------------------------

alter table transaction_group
    add admin_id bigint;
create index fk_transaction_group_admin_id_index_5 on transaction_group (admin_id asc);

#Executed 2019.10.07
#--------------------------------

alter table transaction_group
    add file_name_from_bank varchar(255);

alter table extended_user
    add tax_id varchar(255) null;
alter table extended_user
    modify status varchar(255) null;

#Executed 2019.10.22
#--------------------------------


DROP TABLE extended_user_relation;

CREATE TABLE extended_user_relation
(
    id               bigint(20) NOT NULL AUTO_INCREMENT,
    extended_user_id bigint(20) DEFAULT NULL,
    user_related_id  bigint(20) DEFAULT NULL,
    PRIMARY KEY (id),
    KEY fk_extended_user_relation_user_related_id (user_related_id),
    KEY fk_extended_user_relation_extended_user_id (extended_user_id),
    CONSTRAINT fk_extended_user_relation_extended_user_id FOREIGN KEY (extended_user_id) REFERENCES extended_user (`id`),
    CONSTRAINT fk_extended_user_relation_user_related_id FOREIGN KEY (user_related_id) REFERENCES jhi_user (id)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = latin1;

#Executed 2019.10.24
#--------------------------------

alter table transaction
    change recipient_id payee_id bigint null;


alter table transaction
    drop foreign key fk_transaction_recipient_id;

drop index fk_transaction_recipient_id on transaction;

create index fk_transaction_payee_id
    on transaction (payee_id);

alter table transaction
    add constraint fk_transaction_payee_id
        foreign key (payee_id) references extended_user (id);

#--------------------------------
# Executed 2019.10.28

CREATE TABLE `extended_user_group`
(
    `id`                  bigint(20) NOT NULL AUTO_INCREMENT,
    `date_created`        datetime     DEFAULT NULL,
    `status`              varchar(255) DEFAULT NULL,
    `file_name`           varchar(255) DEFAULT NULL,
    `file_name_from_bank` varchar(255) DEFAULT NULL,
    `admin_id`            bigint(20)   DEFAULT NULL,
    `bank_id`             bigint(20)   DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_extended_user_group_admin_id` (`admin_id`) KEY `fk_extended_user_group_bank_id` (`bank_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = latin1;


ALTER TABLE `extended_user`
    ADD COLUMN `extended_user_group_id` BIGINT(20) NULL DEFAULT NULL;

CREATE INDEX `fk_extended_user_extended_user_group_id` ON `extended_user` (extended_user_group_id);

alter table extended_user
    add status_reason longtext null;

CREATE TABLE `extended_user_status_change`
(
    `id`               bigint(20)   NOT NULL AUTO_INCREMENT,
    `date_created`     datetime     NOT NULL,
    `status`           varchar(255) NOT NULL,
    `reason`           longtext,
    `extended_user_id` bigint(20) DEFAULT NULL,
    `user_id`          bigint(20) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_extended_user_status_change_extended_user_id` (`extended_user_id`),
    KEY `fk_extended_user_status_change_user_id` (`user_id`),
    CONSTRAINT `fk_extended_user_status_change_extended_user_id` FOREIGN KEY (`extended_user_id`) REFERENCES `extended_user` (`id`),
    CONSTRAINT `fk_extended_user_status_change_user_id` FOREIGN KEY (`user_id`) REFERENCES `extended_user` (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = latin1;


#--------------------------------
# Executed 2019.11.08

alter table extended_user
    add role varchar(255) null;


SELECT a.authority_name role, u.id as user_id, eu.id as extended_id, eu.user_id as extended_user_id, eu.role as eu_role
FROM jhi_user_authority a
         INNER JOIN jhi_user u ON u.id = a.user_id
         INNER JOIN extended_user eu ON u.id = eu.user_id;

UPDATE `extended_user`,
    (SELECT a.authority_name role, u.id as user_id, eu.user_id as extended_user_id
     FROM jhi_user_authority a
              INNER JOIN jhi_user u ON u.id = a.user_id
              LEFT JOIN extended_user eu ON u.id = eu.user_id
     WHERE eu.user_id IS NOT NULL) as `roleData`
SET extended_user.role = roleData.role
WHERE extended_user.user_id = roleData.extended_user_id;


alter table extended_user
    add card_number varchar(255) null;


# -------------------------

ALTER TABLE `bank_account`
    ADD COLUMN `bank_id` BIGINT(20) NULL DEFAULT NULL;
CREATE INDEX `fk_bank_account_bank_id_id` ON `bank_account` (bank_id);
alter table bank_account
    add constraint fk_bank_account_bank_id foreign key (bank_id) references bank (id);


alter table bank_account
    drop foreign key fk_bank_account_branch_id;
drop index fk_bank_account_branch_id on bank_account;
alter table bank_account
    drop column branch_id;


ALTER TABLE `extended_user`
    ADD COLUMN `bank_id` BIGINT(20) NULL DEFAULT NULL;
CREATE INDEX `fk_extended_user_bank_id_id` ON `extended_user` (bank_id);
alter table extended_user
    add constraint fk_extended_user_bank_id foreign key (bank_id) references bank (id);


alter table extended_user
    drop foreign key fk_extended_user_branch_id;
drop index fk_extended_user_branch_id on extended_user;
alter table extended_user
    drop column branch_id;

UPDATE `extended_user`
set extended_user.id_type_id = 1
where extended_user.role = 'ROLE_PAYEE';


UPDATE `extended_user`
set extended_user.status = 'CREATED'
where extended_user.role = 'ROLE_PAYEE';


# -------------------
alter table country
    add spanish_name varchar(255) null;

alter table country
    add sub_region_name varchar(255) null;

alter table country
    add region_name varchar(255) null;

alter table country
    add code varchar(255) null;

alter table country
    add for_payee boolean default false null;

UPDATE `country`
SET `spanish_name`    = 'Colombia',
    `sub_region_name` = 'Latin America and the Caribbean',
    `region_name`     = 'Americas',
    `code`            = 'COL',
    `for_payee`       = '1'
WHERE (`id` = '1');

UPDATE `country`
SET `spanish_name`    = 'México',
    `sub_region_name` = 'Latin America and the Caribbean',
    `region_name`     = 'Americas',
    `code`            = 'MEX',
    `for_payee`       = '1'
WHERE (`id` = '2');

INSERT INTO `country`
VALUES (3, 'Algeria', 'Argelia', 'Northern Africa', 'Africa', 'DZA', 0),
       (4, 'Canary Islands (Spain)', 'Canary Islands (Spain)', 'Northern Africa', 'Africa', '', 0),
       (5, 'Egypt', 'Egipto', 'Northern Africa', 'Africa', 'EGY', 0),
       (6, 'Libya', 'Libia', 'Northern Africa', 'Africa', 'LBY', 0),
       (7, 'Morocco', 'Marruecos', 'Northern Africa', 'Africa', 'MAR', 0),
       (8, 'Sudan', 'Sudán (el)', 'Northern Africa', 'Africa', 'SDN', 0),
       (9, 'Tunisia', 'Túnez', 'Northern Africa', 'Africa', 'TUN', 0),
       (10, 'Western Sahara', 'Western Sahara', 'Northern Africa', 'Africa', 'ESH', 0),
       (11, 'Angola', 'Angola', 'Sub-Saharan Africa', 'Africa', 'AGO', 0),
       (12, 'Benin', 'Benin', 'Sub-Saharan Africa', 'Africa', 'BEN', 0),
       (13, 'Botswana', 'Botswana', 'Sub-Saharan Africa', 'Africa', 'BWA', 0),
       (14, 'British Indian Ocean Territory', 'British Indian Ocean Territory', 'Sub-Saharan Africa', 'Africa', 'IOT',
        0),
       (15, 'Burkina Faso', 'Burkina Faso', 'Sub-Saharan Africa', 'Africa', 'BFA', 0),
       (16, 'Burundi', 'Burundi', 'Sub-Saharan Africa', 'Africa', 'BDI', 0),
       (17, 'Cabo Verde', 'Cabo Verde', 'Sub-Saharan Africa', 'Africa', 'CPV', 0),
       (18, 'Cameroon', 'Camerún (el)', 'Sub-Saharan Africa', 'Africa', 'CMR', 0),
       (19, 'Central African Republic', 'República Centroafricana (la)', 'Sub-Saharan Africa', 'Africa', 'CAF', 0),
       (20, 'Chad', 'Chad (el)', 'Sub-Saharan Africa', 'Africa', 'TCD', 0),
       (21, 'Comoros', 'Comoras (las)', 'Sub-Saharan Africa', 'Africa', 'COM', 0),
       (22, 'Congo', 'Congo (el)', 'Sub-Saharan Africa', 'Africa', 'COG', 0),
       (23, 'Côte dIvoire', 'Côte dIvoire', 'Sub-Saharan Africa', 'Africa', 'CIV', 0),
       (24, 'Democratic Republic of the Congo', 'República Democrática del Congo (la)', 'Sub-Saharan Africa', 'Africa',
        'COD', 0),
       (25, 'Djibouti', 'Djibouti', 'Sub-Saharan Africa', 'Africa', 'DJI', 0),
       (26, 'Equatorial Guinea', 'Guinea Ecuatorial', 'Sub-Saharan Africa', 'Africa', 'GNQ', 0),
       (27, 'Eritrea', 'Eritrea', 'Sub-Saharan Africa', 'Africa', 'ERI', 0),
       (28, 'Ethiopia', 'Etiopía', 'Sub-Saharan Africa', 'Africa', 'ETH', 0),
       (29, 'French Southern and Antarctic Territories', 'French Southern and Antarctic Territories',
        'Sub-Saharan Africa', 'Africa', 'ATF', 0),
       (30, 'Gabon', 'Gabón (el)', 'Sub-Saharan Africa', 'Africa', 'GAB', 0),
       (31, 'Gambia', 'Gambia', 'Sub-Saharan Africa', 'Africa', 'GMB', 0),
       (32, 'Ghana', 'Ghana', 'Sub-Saharan Africa', 'Africa', 'GHA', 0),
       (33, 'Guinea', 'Guinea', 'Sub-Saharan Africa', 'Africa', 'GIN', 0),
       (34, 'Guinea-Bissau', 'Guinea-Bissau', 'Sub-Saharan Africa', 'Africa', 'GNB', 0),
       (35, 'Kenya', 'Kenya', 'Sub-Saharan Africa', 'Africa', 'KEN', 0),
       (36, 'Kingdom of Eswatini', 'Swazilandia', 'Sub-Saharan Africa', 'Africa', 'SWZ', 0),
       (37, 'Lesotho', 'Lesotho', 'Sub-Saharan Africa', 'Africa', 'LSO', 0),
       (38, 'Liberia', 'Liberia', 'Sub-Saharan Africa', 'Africa', 'LBR', 0),
       (39, 'Madagascar', 'Madagascar', 'Sub-Saharan Africa', 'Africa', 'MDG', 0),
       (40, 'Malawi', 'Malawi', 'Sub-Saharan Africa', 'Africa', 'MWI', 0),
       (41, 'Mali', 'Malí', 'Sub-Saharan Africa', 'Africa', 'MLI', 0),
       (42, 'Mauritania', 'Mauritania', 'Sub-Saharan Africa', 'Africa', 'MRT', 0),
       (43, 'Mauritius', 'Mauricio', 'Sub-Saharan Africa', 'Africa', 'MUS', 0),
       (44, 'Mayotte', 'Mayotte', 'Sub-Saharan Africa', 'Africa', 'MYT', 0),
       (45, 'Mozambique', 'Mozambique', 'Sub-Saharan Africa', 'Africa', 'MOZ', 0),
       (46, 'Namibia', 'Namibia', 'Sub-Saharan Africa', 'Africa', 'NAM', 0),
       (47, 'Niger', 'Níger (el)', 'Sub-Saharan Africa', 'Africa', 'NER', 0),
       (48, 'Nigeria', 'Nigeria', 'Sub-Saharan Africa', 'Africa', 'NGA', 0),
       (49, 'Réunion', 'Réunion', 'Sub-Saharan Africa', 'Africa', 'REU', 0),
       (50, 'Rwanda', 'Rwanda', 'Sub-Saharan Africa', 'Africa', 'RWA', 0),
       (51, 'Saint Helena', 'Saint Helena', 'Sub-Saharan Africa', 'Africa', 'SHN', 0),
       (52, 'Sao Tome and Principe', 'Santo Tomé y Príncipe', 'Sub-Saharan Africa', 'Africa', 'STP', 0),
       (53, 'Senegal', 'Senegal (el)', 'Sub-Saharan Africa', 'Africa', 'SEN', 0),
       (54, 'Seychelles', 'Seychelles', 'Sub-Saharan Africa', 'Africa', 'SYC', 0),
       (55, 'Sierra Leone', 'Sierra Leona', 'Sub-Saharan Africa', 'Africa', 'SLE', 0),
       (56, 'Somalia', 'Somalia', 'Sub-Saharan Africa', 'Africa', 'SOM', 0),
       (57, 'South Africa', 'Sudáfrica', 'Sub-Saharan Africa', 'Africa', 'ZAF', 0),
       (58, 'South Sudan', 'Sudán del Sur', 'Sub-Saharan Africa', 'Africa', 'SSD', 0),
       (59, 'Togo', 'Togo (el)', 'Sub-Saharan Africa', 'Africa', 'TGO', 0),
       (60, 'Uganda', 'Uganda', 'Sub-Saharan Africa', 'Africa', 'UGA', 0),
       (61, 'United Republic of Tanzania', 'República Unida de Tanzanía (la)', 'Sub-Saharan Africa', 'Africa', 'TZA',
        0),
       (62, 'Zambia', 'Zambia', 'Sub-Saharan Africa', 'Africa', 'ZMB', 0),
       (63, 'Zimbabwe', 'Zimbabwe', 'Sub-Saharan Africa', 'Africa', 'ZWE', 0),
       (64, 'Anguilla', 'Anguilla', 'Latin America and the Caribbean', 'Americas', 'AIA', 0),
       (65, 'Antigua and Barbuda', 'Antigua y Barbuda', 'Latin America and the Caribbean', 'Americas', 'ATG', 0),
       (66, 'Argentina', 'Argentina (la)', 'Latin America and the Caribbean', 'Americas', 'ARG', 0),
       (67, 'Aruba', 'Aruba', 'Latin America and the Caribbean', 'Americas', 'ABW', 0),
       (68, 'Bahamas', 'Bahamas (las)', 'Latin America and the Caribbean', 'Americas', 'BHS', 0),
       (69, 'Barbados', 'Barbados', 'Latin America and the Caribbean', 'Americas', 'BRB', 0),
       (70, 'Belize', 'Belice', 'Latin America and the Caribbean', 'Americas', 'BLZ', 0),
       (71, 'Bolivia (Plurinational State of)', 'Bolivia (Estado Plurinacional de)', 'Latin America and the Caribbean',
        'Americas', 'BOL', 0),
       (72, 'Bonaire, Sint Eustatius and Saba', 'Bonaire, Sint Eustatius and Saba', 'Latin America and the Caribbean',
        'Americas', 'BES', 0),
       (73, 'Bouvet Island', 'Bouvet Island', 'Latin America and the Caribbean', 'Americas', 'BVT', 0),
       (74, 'Brazil', 'Brasil (el)', 'Latin America and the Caribbean', 'Americas', 'BRA', 0),
       (75, 'British Virgin Islands', 'British Virgin Islands', 'Latin America and the Caribbean', 'Americas', 'VGB',
        0),
       (76, 'Cayman Islands', 'Cayman Islands', 'Latin America and the Caribbean', 'Americas', 'CYM', 0),
       (77, 'Chile', 'Chile', 'Latin America and the Caribbean', 'Americas', 'CHL', 0),
       (78, 'Costa Rica', 'Costa Rica', 'Latin America and the Caribbean', 'Americas', 'CRI', 0),
       (79, 'Cuba', 'Cuba', 'Latin America and the Caribbean', 'Americas', 'CUB', 0),
       (80, 'Curaçao', 'Curaçao', 'Latin America and the Caribbean', 'Americas', 'CUW', 0),
       (81, 'Dominica', 'Dominica', 'Latin America and the Caribbean', 'Americas', 'DMA', 0),
       (82, 'Dominican Republic', 'República Dominicana (la)', 'Latin America and the Caribbean', 'Americas', 'DOM', 0),
       (83, 'Ecuador', 'Ecuador (el)', 'Latin America and the Caribbean', 'Americas', 'ECU', 0),
       (84, 'El Salvador', 'El Salvador', 'Latin America and the Caribbean', 'Americas', 'SLV', 0),
       (85, 'Falkland Islands (Malvinas)', 'Falkland Islands (Malvinas)', 'Latin America and the Caribbean', 'Americas',
        'FLK', 0),
       (86, 'French Guiana', 'French Guiana', 'Latin America and the Caribbean', 'Americas', 'GUF', 0),
       (87, 'Galapagos Islands (Ecuador)', 'Galapagos Islands (Ecuador)', 'Latin America and the Caribbean', 'Americas',
        '', 0),
       (88, 'Grenada', 'Granada', 'Latin America and the Caribbean', 'Americas', 'GRD', 0),
       (89, 'Guadeloupe', 'Guadeloupe', 'Latin America and the Caribbean', 'Americas', 'GLP', 0),
       (90, 'Guatemala', 'Guatemala', 'Latin America and the Caribbean', 'Americas', 'GTM', 0),
       (91, 'Guyana', 'Guyana', 'Latin America and the Caribbean', 'Americas', 'GUY', 0),
       (92, 'Haiti', 'Haití', 'Latin America and the Caribbean', 'Americas', 'HTI', 0),
       (93, 'Honduras', 'Honduras', 'Latin America and the Caribbean', 'Americas', 'HND', 0),
       (94, 'Jamaica', 'Jamaica', 'Latin America and the Caribbean', 'Americas', 'JAM', 0),
       (95, 'Martinique', 'Martinique', 'Latin America and the Caribbean', 'Americas', 'MTQ', 0),
       (96, 'Montserrat', 'Montserrat', 'Latin America and the Caribbean', 'Americas', 'MSR', 0),
       (97, 'Netherlands Antilles', 'Netherlands Antilles', 'Latin America and the Caribbean', 'Americas', '', 0),
       (98, 'Nicaragua', 'Nicaragua', 'Latin America and the Caribbean', 'Americas', 'NIC', 0),
       (99, 'Panama', 'Panamá', 'Latin America and the Caribbean', 'Americas', 'PAN', 0),
       (100, 'Paraguay', 'Paraguay (el)', 'Latin America and the Caribbean', 'Americas', 'PRY', 0),
       (101, 'Peru', 'Perú (el)', 'Latin America and the Caribbean', 'Americas', 'PER', 0),
       (102, 'Puerto Rico', 'Puerto Rico', 'Latin America and the Caribbean', 'Americas', 'PRI', 0),
       (103, 'Saint Barthélemy', 'Saint Barthélemy', 'Latin America and the Caribbean', 'Americas', 'BLM', 0),
       (104, 'Saint Kitts and Nevis', 'Saint Kitts y Nevis', 'Latin America and the Caribbean', 'Americas', 'KNA', 0),
       (105, 'Saint Lucia', 'Santa Lucía', 'Latin America and the Caribbean', 'Americas', 'LCA', 0),
       (106, 'Saint Martin (French part)', 'Saint Martin (French part)', 'Latin America and the Caribbean', 'Americas',
        'MAF', 0),
       (107, 'Saint Vincent and The Grenadines', 'San Vicente y las Granadinas', 'Latin America and the Caribbean',
        'Americas', 'VCT', 0),
       (108, 'Sint Maarten (Dutch part)', 'Sint Maarten (Dutch part)', 'Latin America and the Caribbean', 'Americas',
        'SXM', 0),
       (109, 'South Georgia and the South Sandwich Islands', 'South Georgia and the South Sandwich Islands',
        'Latin America and the Caribbean', 'Americas', 'SGS', 0),
       (110, 'Suriname', 'Suriname', 'Latin America and the Caribbean', 'Americas', 'SUR', 0),
       (111, 'Trinidad and Tobago', 'Trinidad y Tabago', 'Latin America and the Caribbean', 'Americas', 'TTO', 0),
       (112, 'Turks and Caicos Islands', 'Turks and Caicos Islands', 'Latin America and the Caribbean', 'Americas',
        'TCA', 0),
       (113, 'United States Virgin Islands', 'United States Virgin Islands', 'Latin America and the Caribbean',
        'Americas', 'VIR', 0),
       (114, 'Uruguay', 'Uruguay (el)', 'Latin America and the Caribbean', 'Americas', 'URY', 0),
       (115, 'Venezuela (Bolivarian Republic of)', 'Venezuela (República Bolivariana de)',
        'Latin America and the Caribbean', 'Americas', 'VEN', 0),
       (116, 'Bermuda', 'Bermuda', 'Northern America', 'Americas', 'BMU', 0),
       (117, 'Canada', 'Canadá (el)', 'Northern America', 'Americas', 'CAN', 0),
       (118, 'Greenland', 'Greenland', 'Northern America', 'Americas', 'GRL', 0),
       (119, 'Saint Pierre and Miquelon', 'Saint Pierre and Miquelon', 'Northern America', 'Americas', 'SPM', 0),
       (120, 'United States of America', 'Estados Unidos de América (los)', 'Northern America', 'Americas', 'USA', 0),
       (121, 'Kazakhstan', 'Kazajstán', 'Central Asia', 'Asia', 'KAZ', 0),
       (122, 'Kyrgyzstan', 'Kirguistán', 'Central Asia', 'Asia', 'KGZ', 0),
       (123, 'Tajikistan', 'Tayikistán', 'Central Asia', 'Asia', 'TJK', 0),
       (124, 'Turkmenistan', 'Turkmenistán', 'Central Asia', 'Asia', 'TKM', 0),
       (125, 'Uzbekistan', 'Uzbekistán', 'Central Asia', 'Asia', 'UZB', 0),
       (126, 'China', 'China', 'Eastern Asia', 'Asia', 'CHN', 0),
       (127, 'China, Hong Kong Special Administrative Region', 'China, Hong Kong Special Administrative Region',
        'Eastern Asia', 'Asia', 'HKG', 0),
       (128, 'China, Macao Special Administrative Region', 'China, Macao Special Administrative Region', 'Eastern Asia',
        'Asia', 'MAC', 0),
       (129, 'Democratic Peoples Republic of Korea', 'República Popular Democrática de Corea (la)', 'Eastern Asia',
        'Asia', 'PRK', 0),
       (130, 'Japan', 'Japón (el)', 'Eastern Asia', 'Asia', 'JPN', 0),
       (131, 'Mongolia', 'Mongolia', 'Eastern Asia', 'Asia', 'MNG', 0),
       (132, 'Republic of Korea', 'República de Corea (la)', 'Eastern Asia', 'Asia', 'KOR', 0),
       (133, 'Taiwan (Province of China)', 'Taiwan (Province of China)', 'Eastern Asia', 'Asia', 'TWN', 0),
       (134, 'Brunei Darussalam', 'Brunei Darussalam', 'South-eastern Asia', 'Asia', 'BRN', 0),
       (135, 'Cambodia', 'Camboya', 'South-eastern Asia', 'Asia', 'KHM', 0),
       (136, 'Indonesia', 'Indonesia', 'South-eastern Asia', 'Asia', 'IDN', 0),
       (137, 'Lao Peoples Democratic Republic', 'República Democrática Popular Lao (la)', 'South-eastern Asia', 'Asia',
        'LAO', 0),
       (138, 'Malaysia', 'Malasia', 'South-eastern Asia', 'Asia', 'MYS', 0),
       (139, 'Myanmar', 'Myanmar', 'South-eastern Asia', 'Asia', 'MMR', 0),
       (140, 'Philippines', 'Filipinas', 'South-eastern Asia', 'Asia', 'PHL', 0),
       (141, 'Singapore', 'Singapur', 'South-eastern Asia', 'Asia', 'SGP', 0),
       (142, 'Thailand', 'Tailandia', 'South-eastern Asia', 'Asia', 'THA', 0),
       (143, 'Timor-Leste', 'Timor-Leste', 'South-eastern Asia', 'Asia', 'TLS', 0),
       (144, 'Viet Nam', 'Viet Nam', 'South-eastern Asia', 'Asia', 'VNM', 0),
       (145, 'Afghanistan', 'Afganistán (el)', 'Southern Asia', 'Asia', 'AFG', 0),
       (146, 'Bangladesh', 'Bangladesh', 'Southern Asia', 'Asia', 'BGD', 0),
       (147, 'Bhutan', 'Bhután', 'Southern Asia', 'Asia', 'BTN', 0),
       (148, 'India', 'India (la)', 'Southern Asia', 'Asia', 'IND', 0),
       (149, 'Iran (Islamic Republic of)', 'Irán (República Islámica del)', 'Southern Asia', 'Asia', 'IRN', 0),
       (150, 'Maldives', 'Maldivas', 'Southern Asia', 'Asia', 'MDV', 0),
       (151, 'Nepal', 'Nepal', 'Southern Asia', 'Asia', 'NPL', 0),
       (152, 'Pakistan', 'Pakistán (el)', 'Southern Asia', 'Asia', 'PAK', 0),
       (153, 'Sri Lanka', 'Sri Lanka', 'Southern Asia', 'Asia', 'LKA', 0),
       (154, 'Armenia', 'Armenia', 'Western Asia', 'Asia', 'ARM', 0),
       (155, 'Azerbaijan', 'Azerbaiyán', 'Western Asia', 'Asia', 'AZE', 0),
       (156, 'Bahrain', 'Bahrein', 'Western Asia', 'Asia', 'BHR', 0),
       (157, 'Cyprus', 'Chipre', 'Western Asia', 'Asia', 'CYP', 0),
       (158, 'Georgia', 'Georgia', 'Western Asia', 'Asia', 'GEO', 0),
       (159, 'Iraq', 'Iraq (el)', 'Western Asia', 'Asia', 'IRQ', 0),
       (160, 'Israel', 'Israel', 'Western Asia', 'Asia', 'ISR', 0),
       (161, 'Jordan', 'Jordania', 'Western Asia', 'Asia', 'JOR', 0),
       (162, 'Kuwait', 'Kuwait', 'Western Asia', 'Asia', 'KWT', 0),
       (163, 'Lebanon', 'Líbano (el)', 'Western Asia', 'Asia', 'LBN', 0),
       (164, 'Oman', 'Omán', 'Western Asia', 'Asia', 'OMN', 0),
       (165, 'Qatar', 'Qatar', 'Western Asia', 'Asia', 'QAT', 0),
       (166, 'Saudi Arabia', 'Arabia Saudita (la)', 'Western Asia', 'Asia', 'SAU', 0),
       (167, 'State of Palestine', 'Estado de Palestina', 'Western Asia', 'Asia', 'PSE', 0),
       (168, 'Syrian Arab Republic', 'República Árabe Siria (la)', 'Western Asia', 'Asia', 'SYR', 0),
       (169, 'Turkey', 'Turquía', 'Western Asia', 'Asia', 'TUR', 0),
       (170, 'United Arab Emirates', 'Emiratos Árabes Unidos (los)', 'Western Asia', 'Asia', 'ARE', 0),
       (171, 'Yemen', 'Yemen (el)', 'Western Asia', 'Asia', 'YEM', 0),
       (172, 'Belarus', 'Belarús', 'Eastern Europe', 'Europe', 'BLR', 0),
       (173, 'Bulgaria', 'Bulgaria', 'Eastern Europe', 'Europe', 'BGR', 0),
       (174, 'Czechia', 'República Checa (la)', 'Eastern Europe', 'Europe', 'CZE', 0),
       (175, 'Hungary', 'Hungría', 'Eastern Europe', 'Europe', 'HUN', 0),
       (176, 'Poland', 'Polonia', 'Eastern Europe', 'Europe', 'POL', 0),
       (177, 'Republic of Moldova', 'la República de Moldova', 'Eastern Europe', 'Europe', 'MDA', 0),
       (178, 'Romania', 'Rumania', 'Eastern Europe', 'Europe', 'ROU', 0),
       (179, 'Russian Federation', 'Federación de Rusia (la)', 'Eastern Europe', 'Europe', 'RUS', 0),
       (180, 'Slovakia', 'Eslovaquia', 'Eastern Europe', 'Europe', 'SVK', 0),
       (181, 'Ukraine', 'Ucrania', 'Eastern Europe', 'Europe', 'UKR', 0),
       (182, 'Åland Islands', 'Åland Islands', 'Northern Europe', 'Europe', 'ALA', 0),
       (183, 'Channel Islands', 'Channel Islands', 'Northern Europe', 'Europe', '', 0),
       (184, 'Denmark', 'Dinamarca', 'Northern Europe', 'Europe', 'DNK', 0),
       (185, 'Estonia', 'Estonia', 'Northern Europe', 'Europe', 'EST', 0),
       (186, 'Faroe Islands', 'Faroe Islands', 'Northern Europe', 'Europe', 'FRO', 0),
       (187, 'Finland', 'Finlandia', 'Northern Europe', 'Europe', 'FIN', 0),
       (188, 'Guernsey', 'Guernsey', 'Northern Europe', 'Europe', 'GGY', 0),
       (189, 'Iceland', 'Islandia', 'Northern Europe', 'Europe', 'ISL', 0),
       (190, 'Ireland', 'Irlanda', 'Northern Europe', 'Europe', 'IRL', 0),
       (191, 'Isle of Man', 'Isle of Man', 'Northern Europe', 'Europe', 'IMN', 0),
       (192, 'Jersey', 'Jersey', 'Northern Europe', 'Europe', 'JEY', 0),
       (193, 'Latvia', 'Letonia', 'Northern Europe', 'Europe', 'LVA', 0),
       (194, 'Lithuania', 'Lituania', 'Northern Europe', 'Europe', 'LTU', 0),
       (195, 'Norway', 'Noruega', 'Northern Europe', 'Europe', 'NOR', 0),
       (196, 'Sark', 'Sark', 'Northern Europe', 'Europe', '', 0),
       (197, 'Svalbard and Jan Mayen Islands', 'Svalbard and Jan Mayen Islands', 'Northern Europe', 'Europe', 'SJM', 0),
       (198, 'Sweden', 'Suecia', 'Northern Europe', 'Europe', 'SWE', 0),
       (199, 'United Kingdom of Great Britain and Northern Ireland',
        'Reino Unido de Gran Bretaña e Irlanda del Norte (el)', 'Northern Europe', 'Europe', 'GBR', 0),
       (200, 'Albania', 'Albania', 'Southern Europe', 'Europe', 'ALB', 0),
       (201, 'Andorra', 'Andorra', 'Southern Europe', 'Europe', 'AND', 0),
       (202, 'Azores Islands (Portugal)', 'Azores Islands (Portugal)', 'Southern Europe', 'Europe', '', 0),
       (203, 'Bosnia and Herzegovina', 'Bosnia y Herzegovina', 'Southern Europe', 'Europe', 'BIH', 0),
       (204, 'Croatia', 'Croacia', 'Southern Europe', 'Europe', 'HRV', 0),
       (205, 'Gibraltar', 'Gibraltar', 'Southern Europe', 'Europe', 'GIB', 0),
       (206, 'Greece', 'Grecia', 'Southern Europe', 'Europe', 'GRC', 0),
       (207, 'Holy See', 'Santa Sede (la)', 'Southern Europe', 'Europe', 'VAT', 0),
       (208, 'Italy', 'Italia', 'Southern Europe', 'Europe', 'ITA', 0),
       (209, 'Malta', 'Malta', 'Southern Europe', 'Europe', 'MLT', 0),
       (210, 'Montenegro', 'Montenegro', 'Southern Europe', 'Europe', 'MNE', 0),
       (211, 'Portugal', 'Portugal', 'Southern Europe', 'Europe', 'PRT', 0),
       (212, 'Republic of North Macedonia', 'Macedonia del Norte', 'Southern Europe', 'Europe', 'MKD', 0),
       (213, 'San Marino', 'San Marino', 'Southern Europe', 'Europe', 'SMR', 0),
       (214, 'Serbia', 'Serbia', 'Southern Europe', 'Europe', 'SRB', 0),
       (215, 'Slovenia', 'Eslovenia', 'Southern Europe', 'Europe', 'SVN', 0),
       (216, 'Spain', 'España', 'Southern Europe', 'Europe', 'ESP', 0),
       (217, 'Austria', 'Austria', 'Western Europe', 'Europe', 'AUT', 0),
       (218, 'Belgium', 'Bélgica', 'Western Europe', 'Europe', 'BEL', 0),
       (219, 'France', 'Francia', 'Western Europe', 'Europe', 'FRA', 0),
       (220, 'Germany', 'Alemania', 'Western Europe', 'Europe', 'DEU', 0),
       (221, 'Liechtenstein', 'Liechtenstein', 'Western Europe', 'Europe', 'LIE', 0),
       (222, 'Luxembourg', 'Luxemburgo', 'Western Europe', 'Europe', 'LUX', 0),
       (223, 'Monaco', 'Mónaco', 'Western Europe', 'Europe', 'MCO', 0),
       (224, 'Netherlands', 'Países Bajos (los)', 'Western Europe', 'Europe', 'NLD', 0),
       (225, 'Switzerland', 'Suiza', 'Western Europe', 'Europe', 'CHE', 0),
       (226, 'Australia', 'Australia', 'Australia and New Zealand', 'Oceania', 'AUS', 0),
       (227, 'Christmas Island', 'Christmas Island', 'Australia and New Zealand', 'Oceania', 'CXR', 0),
       (228, 'Cocos (Keeling) Islands', 'Cocos (Keeling) Islands', 'Australia and New Zealand', 'Oceania', 'CCK', 0),
       (229, 'Heard Island and McDonald Islands', 'Heard Island and McDonald Islands', 'Australia and New Zealand',
        'Oceania', 'HMD', 0),
       (230, 'New Zealand', 'Nueva Zelandia', 'Australia and New Zealand', 'Oceania', 'NZL', 0),
       (231, 'Norfolk Island', 'Norfolk Island', 'Australia and New Zealand', 'Oceania', 'NFK', 0),
       (232, 'Fiji', 'Fiji', 'Melanesia', 'Oceania', 'FJI', 0),
       (233, 'New Caledonia', 'New Caledonia', 'Melanesia', 'Oceania', 'NCL', 0),
       (234, 'Papua New Guinea', 'Papua Nueva Guinea', 'Melanesia', 'Oceania', 'PNG', 0),
       (235, 'Solomon Islands', 'Islas Salomón (las)', 'Melanesia', 'Oceania', 'SLB', 0),
       (236, 'Vanuatu', 'Vanuatu', 'Melanesia', 'Oceania', 'VUT', 0),
       (237, 'Guam', 'Guam', 'Micronesia', 'Oceania', 'GUM', 0),
       (238, 'Kiribati', 'Kiribati', 'Micronesia', 'Oceania', 'KIR', 0),
       (239, 'Marshall Islands', 'Islas Marshall (las)', 'Micronesia', 'Oceania', 'MHL', 0),
       (240, 'Micronesia (Federated States of)', 'Micronesia (Estados Federados de)', 'Micronesia', 'Oceania', 'FSM',
        0),
       (241, 'Nauru', 'Nauru', 'Micronesia', 'Oceania', 'NRU', 0),
       (242, 'Northern Mariana Islands', 'Northern Mariana Islands', 'Micronesia', 'Oceania', 'MNP', 0),
       (243, 'Palau', 'Palau', 'Micronesia', 'Oceania', 'PLW', 0),
       (244, 'United States Minor Outlying Islands (the)', 'United States Minor Outlying Islands (the)', 'Micronesia',
        'Oceania', 'UMI', 0),
       (245, 'American Samoa', 'American Samoa', 'Polynesia', 'Oceania', 'ASM', 0),
       (246, 'Cook Islands', 'Islas Cook (las)', 'Polynesia', 'Oceania', 'COK', 0),
       (247, 'French Polynesia', 'French Polynesia', 'Polynesia', 'Oceania', 'PYF', 0),
       (248, 'Niue', 'Niue', 'Polynesia', 'Oceania', 'NIU', 0),
       (249, 'Pitcairn', 'Pitcairn', 'Polynesia', 'Oceania', 'PCN', 0),
       (250, 'Samoa', 'Samoa', 'Polynesia', 'Oceania', 'WSM', 0),
       (251, 'Tokelau', 'Tokelau', 'Polynesia', 'Oceania', 'TKL', 0),
       (252, 'Tonga', 'Tonga', 'Polynesia', 'Oceania', 'TON', 0),
       (253, 'Tuvalu', 'Tuvalu', 'Polynesia', 'Oceania', 'TUV', 0),
       (254, 'Wallis and Futuna Islands', 'Wallis and Futuna Islands', 'Polynesia', 'Oceania', 'WLF', 0),
       (255, 'Antarctica', 'Antarctica', '', '', 'ATA', 0);


UPDATE `pagomundo`.`city`
SET `name` = 'Ciudad de Mexico'
WHERE (`id` = '1121');

INSERT INTO `city`
VALUES (1122, 'Abalá', '', 2),
       (1123, 'Abasolo', '', 2),
       (1124, 'Abasolo', '', 2),
       (1125, 'Abasolo', '', 2),
       (1126, 'Abasolo', '', 2),
       (1127, 'Acacoyagua', '', 2),
       (1128, 'Acajete', '', 2),
       (1129, 'Acajete', '', 2),
       (1130, 'Acala', '', 2),
       (1131, 'Acámbaro', '', 2),
       (1132, 'Acambay', '', 2),
       (1133, 'Acanceh', '', 2),
       (1134, 'Acapetahua', '', 2),
       (1135, 'Acaponeta', '', 2),
       (1136, 'Acapulco de Juárez', '', 2),
       (1137, 'Acateno', '', 2),
       (1138, 'Acatic', '', 2),
       (1139, 'Acatlán', '', 2),
       (1140, 'Acatlán', '', 2),
       (1141, 'Acatlán', '', 2),
       (1142, 'Acatlán de Juárez', '', 2),
       (1143, 'Acatzingo', '', 2),
       (1144, 'Acaxochitlán', '', 2),
       (1145, 'Acayucan', '', 2),
       (1146, 'Acolman', '', 2),
       (1147, 'Aconchi', '', 2),
       (1148, 'Acteopan', '', 2),
       (1149, 'Actopan', '', 2),
       (1150, 'Actopan', '', 2),
       (1151, 'Acuamanala de Miguel Hidalgo', '', 2),
       (1152, 'Acuitzio', '', 2),
       (1153, 'Acula', '', 2),
       (1154, 'Aculco', '', 2),
       (1155, 'Acultzingo', '', 2),
       (1156, 'Acuña', '', 2),
       (1157, 'Agua Blanca de Iturbide', '', 2),
       (1158, 'Agua Prieta', '', 2),
       (1159, 'Agualeguas', '', 2),
       (1160, 'Aguascalientes', '', 2),
       (1161, 'Aguililla', '', 2),
       (1162, 'Ahome', '', 2),
       (1163, 'Ahuacatlán', '', 2),
       (1164, 'Ahuacatlán', '', 2),
       (1165, 'Ahuacuotzingo', '', 2),
       (1166, 'Ahualulco', '', 2),
       (1167, 'Ahualulco de Mercado', '', 2),
       (1168, 'Ahuatlán', '', 2),
       (1169, 'Ahuazotepec', '', 2),
       (1170, 'Ahuehuetitla', '', 2),
       (1171, 'Ahumada', '', 2),
       (1172, 'Ajacuba', '', 2),
       (1173, 'Ajalpan', '', 2),
       (1174, 'Ajuchitlán del Progreso', '', 2),
       (1175, 'Akil', '', 2),
       (1176, 'Álamos', '', 2),
       (1177, 'Alaquines', '', 2),
       (1178, 'Albino Zertuche', '', 2),
       (1179, 'Alcozauca de Guerrero', '', 2),
       (1180, 'Aldama', '', 2),
       (1181, 'Aldama', '', 2),
       (1182, 'Alfajayucan', '', 2),
       (1183, 'Aljojuca', '', 2),
       (1184, 'Allende', '', 2),
       (1185, 'Allende', '', 2),
       (1186, 'Allende', '', 2),
       (1187, 'Allende', '', 2),
       (1188, 'Almoloya', '', 2),
       (1189, 'Almoloya de Alquisiras', '', 2),
       (1190, 'Almoloya de Juárez', '', 2),
       (1191, 'Almoloya del Río', '', 2),
       (1192, 'Alpatláhuac', '', 2),
       (1193, 'Alpoyeca', '', 2),
       (1194, 'Altamira', '', 2),
       (1195, 'Altamirano', '', 2),
       (1196, 'Altar', '', 2),
       (1197, 'Altepexi', '', 2),
       (1198, 'Alto Lucero de Gutiérrez Barrios', '', 2),
       (1199, 'Altotonga', '', 2),
       (1200, 'Altzayanca', '', 2),
       (1201, 'Alvarado', '', 2),
       (1202, 'Alvaro Obregón', '', 2),
       (1203, 'Álvaro Obregón', '', 2),
       (1204, 'Amacueca', '', 2),
       (1205, 'Amacuzac', '', 2),
       (1206, 'Amanalco', '', 2),
       (1207, 'Amatán', '', 2),
       (1208, 'Amatenango de la Frontera', '', 2),
       (1209, 'Amatenango del Valle', '', 2),
       (1210, 'Amatepec', '', 2),
       (1211, 'Amatitán', '', 2),
       (1212, 'Amatitlán', '', 2),
       (1213, 'Amatitlán de los Reyes', '', 2),
       (1214, 'Amatlán de Cañas', '', 2),
       (1215, 'Amatlan Tuxpan', '', 2),
       (1216, 'Amaxac de Guerrero', '', 2),
       (1217, 'Amealco de Bonfin', '', 2),
       (1218, 'Ameca', '', 2),
       (1219, 'Amecameca', '', 2),
       (1220, 'Amixtlán', '', 2),
       (1221, 'Amozoc', '', 2),
       (1222, 'Anáhuac', '', 2),
       (1223, 'Angamacutiro', '', 2),
       (1224, 'Angangueo', '', 2),
       (1225, 'Angel Albino Corzo', '', 2),
       (1226, 'Ángel R. Cabada', '', 2),
       (1227, 'Angostura', '', 2),
       (1228, 'Antiguo Morelos', '', 2),
       (1229, 'Antonio Escobedo', '', 2),
       (1230, 'Apan', '', 2),
       (1231, 'Apaseo el Alto', '', 2),
       (1232, 'Apaseo el Grande', '', 2),
       (1233, 'Apatzingán', '', 2),
       (1234, 'Apaxco', '', 2),
       (1235, 'Apaxtla', '', 2),
       (1236, 'Apazapan', '', 2),
       (1237, 'Apetatitlán de Antonio Carvajal', '', 2),
       (1238, 'Apizaco', '', 2),
       (1239, 'Apodaca', '', 2),
       (1240, 'Aporo', '', 2),
       (1241, 'Apozol', '', 2),
       (1242, 'Apulco', '', 2),
       (1243, 'Aquila', '', 2),
       (1244, 'Aquila', '', 2),
       (1245, 'Aquiles Serdán', '', 2),
       (1246, 'Aquismón', '', 2),
       (1247, 'Aquixtla', '', 2),
       (1248, 'Aramberri', '', 2),
       (1249, 'Arandas', '', 2),
       (1250, 'Arcelia', '', 2),
       (1251, 'Ario', '', 2),
       (1252, 'Arizpe', '', 2),
       (1253, 'Armadillo de los Infante', '', 2),
       (1254, 'Armería', '', 2),
       (1255, 'Arriaga', '', 2),
       (1256, 'Arroyo Seco', '', 2),
       (1257, 'Arteaga', '', 2),
       (1258, 'Arteaga', '', 2),
       (1259, 'Ascensión', '', 2),
       (1260, 'Asientos', '', 2),
       (1261, 'Astacinga', '', 2),
       (1262, 'Atarjea', '', 2),
       (1263, 'Atemajac de Brizuela', '', 2),
       (1264, 'Atempan', '', 2),
       (1265, 'Atenango del Río', '', 2),
       (1266, 'Atenco', '', 2),
       (1267, 'Atengo', '', 2),
       (1268, 'Atenguillo', '', 2),
       (1269, 'Atexcal', '', 2),
       (1270, 'Atil', '', 2),
       (1271, 'Atitalaquia', '', 2),
       (1272, 'Atizapán', '', 2),
       (1273, 'Atizapán de Zaragoza', '', 2),
       (1274, 'Atlacomulco', '', 2),
       (1275, 'Atlahuilco', '', 2),
       (1276, 'Atlamajalcingo del Monte', '', 2),
       (1277, 'Atlangatepec', '', 2),
       (1278, 'Atlapexco', '', 2),
       (1279, 'Atlatlahucan', '', 2),
       (1280, 'Atlautla', '', 2),
       (1281, 'Atlixco', '', 2),
       (1282, 'Atlixtac', '', 2),
       (1283, 'Atolinga', '', 2),
       (1284, 'Atotonilco de Tula', '', 2),
       (1285, 'Atotonilco El Alto', '', 2),
       (1286, 'Atotonilco el Grande', '', 2),
       (1287, 'Atoyac', '', 2),
       (1288, 'Atoyac', '', 2),
       (1289, 'Atoyac de Alvarez', '', 2),
       (1290, 'Atoyatempan', '', 2),
       (1291, 'Atzacan', '', 2),
       (1292, 'Atzala', '', 2),
       (1293, 'Atzalan', '', 2),
       (1294, 'Atzitzihuacán', '', 2),
       (1295, 'Autlán de Navarro', '', 2),
       (1296, 'Axapusco', '', 2),
       (1297, 'Axochiapan', '', 2),
       (1298, 'Axtla de Terrazas', '', 2),
       (1299, 'Axutla', '', 2),
       (1300, 'Ayahualulco', '', 2),
       (1301, 'Ayala', '', 2),
       (1302, 'Ayapango', '', 2),
       (1303, 'Ayotlán', '', 2),
       (1304, 'Ayotoxco de Guerrero', '', 2),
       (1305, 'Ayutla', '', 2),
       (1306, 'Ayutla de los Libres', '', 2),
       (1307, 'Azcapotzalco', '', 2),
       (1308, 'Azoyú', '', 2),
       (1309, 'Baca', '', 2),
       (1310, 'Bacadéhuachi', '', 2),
       (1311, 'Bacanora', '', 2),
       (1312, 'Bacerac', '', 2),
       (1313, 'Bachíniva', '', 2),
       (1314, 'Bacoachi', '', 2),
       (1315, 'Bácum', '', 2),
       (1316, 'Badiraguato', '', 2),
       (1317, 'Balancán', '', 2),
       (1318, 'Balleza', '', 2),
       (1319, 'Banámichi', '', 2),
       (1320, 'Banderilla', '', 2),
       (1321, 'Batopilas', '', 2),
       (1322, 'Baviácora', '', 2),
       (1323, 'Bavispe', '', 2),
       (1324, 'Bejucal de Ocampo', '', 2),
       (1325, 'Bella Vista', '', 2),
       (1326, 'Benito Juárez', '', 2),
       (1327, 'Benito Juárez', '', 2),
       (1328, 'Benito Juárez', '', 2),
       (1329, 'Benito Juárez', '', 2),
       (1330, 'Benito Juárez', '', 2),
       (1331, 'Benjamín Hill', '', 2),
       (1332, 'Berriozábal', '', 2),
       (1333, 'Boca del Río', '', 2),
       (1334, 'Boca San Lorenzo', '', 2),
       (1335, 'Bochil', '', 2),
       (1336, 'Bocoyna', '', 2),
       (1337, 'Bokobá', '', 2),
       (1338, 'Bolaños', '', 2),
       (1339, 'Briseñas', '', 2),
       (1340, 'Buctzotz', '', 2),
       (1341, 'Buenaventura', '', 2),
       (1342, 'Buenavista', '', 2),
       (1343, 'Buenavista de Cuéllar', '', 2),
       (1344, 'Burgos', '', 2),
       (1345, 'Bustamante', '', 2),
       (1346, 'Bustamante', '', 2),
       (1347, 'Cabo Corrientes', '', 2),
       (1348, 'Caborca', '', 2),
       (1349, 'Cacahoatán', '', 2),
       (1350, 'Cacalchén', '', 2),
       (1351, 'Cadereyta de Montes', '', 2),
       (1352, 'Cadereyta Jiménez', '', 2),
       (1353, 'Cajeme', '', 2),
       (1354, 'Calcahualco', '', 2),
       (1355, 'Calera', '', 2),
       (1356, 'Calimaya', '', 2),
       (1357, 'Calkiní', '', 2),
       (1358, 'Calnali', '', 2),
       (1359, 'Calotmul', '', 2),
       (1360, 'Calpan', '', 2),
       (1361, 'Calpulalpan', '', 2),
       (1362, 'Caltepec', '', 2),
       (1363, 'Calvillo', '', 2),
       (1364, 'Camargo', '', 2),
       (1365, 'Camargo', '', 2),
       (1366, 'Camarón de Tejada', '', 2),
       (1367, 'Camerino Z. Mendoza', '', 2),
       (1368, 'Camocuautla', '', 2),
       (1369, 'Campeche', '', 2),
       (1370, 'Cañada Morelos', '', 2),
       (1371, 'Cañadas de Obregón', '', 2),
       (1372, 'Cananea', '', 2),
       (1373, 'Canatlán', '', 2),
       (1374, 'Candela', '', 2),
       (1375, 'Canelas', '', 2),
       (1376, 'Cañitas de Felipe Pescador', '', 2),
       (1377, 'Cansahcab', '', 2),
       (1378, 'Cantamayec', '', 2),
       (1379, 'Capulhuac', '', 2),
       (1380, 'Carácuaro', '', 2),
       (1381, 'Carbó', '', 2),
       (1382, 'Cárdenas', '', 2),
       (1383, 'Cárdenas', '', 2),
       (1384, 'Cardonal', '', 2),
       (1385, 'Carichic', '', 2),
       (1386, 'Carmen', '', 2),
       (1387, 'Carmen', '', 2),
       (1388, 'Carrillo Puerto', '', 2),
       (1389, 'Casas', '', 2),
       (1390, 'Casas Grandes', '', 2),
       (1391, 'Casimiro Castillo', '', 2),
       (1392, 'Castaños', '', 2),
       (1393, 'Castillo de Teayo', '', 2),
       (1394, 'Catazajá', '', 2),
       (1395, 'Catazajá', '', 2),
       (1396, 'Catemaco', '', 2),
       (1397, 'Catorce', '', 2),
       (1398, 'Caxhuacan', '', 2),
       (1399, 'Cazones de Herrera', '', 2),
       (1400, 'Cedral', '', 2),
       (1401, 'Celaya', '', 2),
       (1402, 'Celestún', '', 2),
       (1403, 'Cenotillo', '', 2),
       (1404, 'Centla', '', 2),
       (1405, 'Centro', '', 2),
       (1406, 'Cerralvo', '', 2),
       (1407, 'Cerritos', '', 2),
       (1408, 'Cerro Azul', '', 2),
       (1409, 'Cerro de San Pedro', '', 2),
       (1410, 'Chacaltianguis', '', 2),
       (1411, 'Chacsinkín', '', 2),
       (1412, 'Chalchicomula de Sesma', '', 2),
       (1413, 'Chalchihuitán', '', 2),
       (1414, 'Chalchihuites', '', 2),
       (1415, 'Chalco', '', 2),
       (1416, 'Chalma', '', 2),
       (1417, 'Champotón', '', 2),
       (1418, 'Chamula', '', 2),
       (1419, 'Chanal', '', 2),
       (1420, 'Chankom', '', 2),
       (1421, 'Chapa de Mota', '', 2),
       (1422, 'Chapab', '', 2),
       (1423, 'Chapala', '', 2),
       (1424, 'Chapantongo', '', 2),
       (1425, 'Chapulco', '', 2),
       (1426, 'Chapulhuacán', '', 2),
       (1427, 'Chapultenango', '', 2),
       (1428, 'Chapultepec', '', 2),
       (1429, 'Charapan', '', 2),
       (1430, 'Charcas', '', 2),
       (1431, 'Charo', '', 2),
       (1432, 'Chavinda', '', 2),
       (1433, 'Chemax', '', 2),
       (1434, 'Chenalhó', '', 2),
       (1435, 'Cherán', '', 2),
       (1436, 'Chiapa de Corzo', '', 2),
       (1437, 'Chiapilla', '', 2),
       (1438, 'Chiautempan', '', 2),
       (1439, 'Chiautla', '', 2),
       (1440, 'Chiautla', '', 2),
       (1441, 'Chiautzingo', '', 2),
       (1442, 'Chichimilá', '', 2),
       (1443, 'Chichiquila', '', 2),
       (1444, 'Chicoasén', '', 2),
       (1445, 'Chicoloapan', '', 2),
       (1446, 'Chicomuselo', '', 2),
       (1447, 'Chiconamel', '', 2),
       (1448, 'Chiconcuac', '', 2),
       (1449, 'Chiconcuautla', '', 2),
       (1450, 'Chiconquiaco', '', 2),
       (1451, 'Chicontepec', '', 2),
       (1452, 'Chicxulub Pueblo', '', 2),
       (1453, 'Chietla', '', 2),
       (1454, 'Chigmecatitlán', '', 2),
       (1455, 'Chignahuapan', '', 2),
       (1456, 'Chignautla', '', 2),
       (1457, 'Chihuahua', '', 2),
       (1458, 'Chikindzonot', '', 2),
       (1459, 'Chila', '', 2),
       (1460, 'Chila de la Sal', '', 2),
       (1461, 'Chilapa de Alvarez', '', 2),
       (1462, 'Chilchota', '', 2),
       (1463, 'Chilchotla', '', 2),
       (1464, 'Chilcuautla', '', 2),
       (1465, 'Chilón', '', 2),
       (1466, 'Chilpancingo de los Bravo', '', 2),
       (1467, 'Chimalhuacán', '', 2),
       (1468, 'Chimaltitán', '', 2),
       (1469, 'China', '', 2),
       (1470, 'Chinameca', '', 2),
       (1471, 'Chinampa de Gorostiza', '', 2),
       (1472, 'Chinantla', '', 2),
       (1473, 'Chinicuila', '', 2),
       (1474, 'Chínipas', '', 2),
       (1475, 'Chiquilistlán', '', 2),
       (1476, 'Choapam', '', 2),
       (1477, 'Chocholá', '', 2),
       (1478, 'Chocomán', '', 2),
       (1479, 'Choix', '', 2),
       (1480, 'Chontla', '', 2),
       (1481, 'Chucándiro', '', 2),
       (1482, 'Chumatlán', '', 2),
       (1483, 'Chumayel', '', 2),
       (1484, 'Churintzio', '', 2),
       (1485, 'Churumuco', '', 2),
       (1486, 'Ciénega de Flores', '', 2),
       (1487, 'Cihuatlán', '', 2),
       (1488, 'Cintalapa', '', 2),
       (1489, 'Citlaltépetl', '', 2),
       (1490, 'Ciudad del Maíz', '', 2),
       (1491, 'Ciudad Fernández', '', 2),
       (1492, 'Ciudad Guzman', '', 2),
       (1493, 'Ciudad Madero', '', 2),
       (1494, 'Ciudad Valles', '', 2),
       (1495, 'Ciudad Venustiano Carranza', '', 2),
       (1496, 'Coacalco de Berriozábal', '', 2),
       (1497, 'Coacoatzintla', '', 2),
       (1498, 'Coahuayana', '', 2),
       (1499, 'Coalcomán de Vázquez Pallares', '', 2),
       (1500, 'Coapilla', '', 2),
       (1501, 'Coatepec', '', 2),
       (1502, 'Coatepec', '', 2),
       (1503, 'Coatepec Harinas', '', 2),
       (1504, 'Coatlán del Río', '', 2),
       (1505, 'Coatzacoalcos', '', 2),
       (1506, 'Coatzingo', '', 2),
       (1507, 'Coatzintla', '', 2),
       (1508, 'Cocotitlán', '', 2),
       (1509, 'Cocula', '', 2),
       (1510, 'Cocula', '', 2),
       (1511, 'Coeneo', '', 2),
       (1512, 'Coetzala', '', 2),
       (1513, 'Cohetzala', '', 2),
       (1514, 'Cohuecán', '', 2),
       (1515, 'Coixtlahuaca', '', 2),
       (1516, 'Cojumatlán de Régules', '', 2),
       (1517, 'Colima', '', 2),
       (1518, 'Colipa', '', 2),
       (1519, 'Colón', '', 2),
       (1520, 'Colotlán', '', 2),
       (1521, 'Comala', '', 2),
       (1522, 'Comalcalco', '', 2),
       (1523, 'Comapa', '', 2),
       (1524, 'Comitán de Domínguez', '', 2),
       (1525, 'Comondú', '', 2),
       (1526, 'Comonfort', '', 2),
       (1527, 'Compostela', '', 2),
       (1528, 'Concepción de Buenos Aires', '', 2),
       (1529, 'Concepción del Oro', '', 2),
       (1530, 'Concordia', '', 2),
       (1531, 'Coneto de Comonfort', '', 2),
       (1532, 'Conkal', '', 2),
       (1533, 'Contepec', '', 2),
       (1534, 'Contla de Juan Cuamatzi', '', 2),
       (1535, 'Copainalá', '', 2),
       (1536, 'Copala', '', 2),
       (1537, 'Copalillo', '', 2),
       (1538, 'Copanatoyac', '', 2),
       (1539, 'Copándaro', '', 2),
       (1540, 'Coquimatlán', '', 2),
       (1541, 'Córdoba', '', 2),
       (1542, 'Coronado', '', 2),
       (1543, 'Coronango', '', 2),
       (1544, 'Coroneo', '', 2),
       (1545, 'Corregidora', '', 2),
       (1546, 'Cortazar', '', 2),
       (1547, 'Cosalá', '', 2),
       (1548, 'Cosamaloapan', '', 2),
       (1549, 'Cosautlán de Carvajal', '', 2),
       (1550, 'Coscomatepec', '', 2),
       (1551, 'Cosío', '', 2),
       (1552, 'Cosoleacaque', '', 2),
       (1553, 'Cotaxtla', '', 2),
       (1554, 'Cotija', '', 2),
       (1555, 'Coxcatlán', '', 2),
       (1556, 'Coxcatlán', '', 2),
       (1557, 'Coxquihi', '', 2),
       (1558, 'Coyame del Sotol', '', 2),
       (1559, 'Coyoacán', '', 2),
       (1560, 'Coyomeapan', '', 2),
       (1561, 'Coyotepec', '', 2),
       (1562, 'Coyotepec', '', 2),
       (1563, 'Coyuca de Benítez', '', 2),
       (1564, 'Coyuca de Catalán', '', 2),
       (1565, 'Coyutla', '', 2),
       (1566, 'Cozumel', '', 2),
       (1567, 'Cruillas', '', 2),
       (1568, 'Cuajimalpa de Morelos', '', 2),
       (1569, 'Cuajinicuilapa', '', 2),
       (1570, 'Cualác', '', 2),
       (1571, 'Cuapiaxtla', '', 2),
       (1572, 'Cuapiaxtla de Madero', '', 2),
       (1573, 'Cuatrociénegas', '', 2),
       (1574, 'Cuauhtémoc', '', 2),
       (1575, 'Cuauhtémoc', '', 2),
       (1576, 'Cuauhtémoc', '', 2),
       (1577, 'Cuauhtémoc', '', 2),
       (1578, 'Cuautempan', '', 2),
       (1579, 'Cuautepec', '', 2),
       (1580, 'Cuautepec de Hinojosa', '', 2),
       (1581, 'Cuautinchán', '', 2),
       (1582, 'Cuautitlán', '', 2),
       (1583, 'Cuautitlán de García Barragán', '', 2),
       (1584, 'Cuautitlán Izcalli', '', 2),
       (1585, 'Cuautla', '', 2),
       (1586, 'Cuautla', '', 2),
       (1587, 'Cuautlancingo', '', 2),
       (1588, 'Cuaxomulco', '', 2),
       (1589, 'Cuayuca de Andrade', '', 2),
       (1590, 'Cucurpe', '', 2),
       (1591, 'Cuencamé', '', 2),
       (1592, 'Cuerámaro', '', 2),
       (1593, 'Cuernavaca', '', 2),
       (1594, 'Cuetzala del Progreso', '', 2),
       (1595, 'Cuetzalan del Progreso', '', 2),
       (1596, 'Cuicatlan', '', 2),
       (1597, 'Cuichapa', '', 2),
       (1598, 'Cuitláhuac', '', 2),
       (1599, 'Cuitzeo', '', 2),
       (1600, 'Culiacán', '', 2),
       (1601, 'Cumpas', '', 2),
       (1602, 'Cuncunul', '', 2),
       (1603, 'Cunduacán', '', 2),
       (1604, 'Cuquito', '', 2),
       (1605, 'Cusihuiriachic', '', 2),
       (1606, 'Cutzamala de Pinzón', '', 2),
       (1607, 'Cuyoaco', '', 2),
       (1608, 'Cuzamá', '', 2),
       (1609, 'Degollado', '', 2),
       (1610, 'Del Centro', '', 2),
       (1611, 'Delicias', '', 2),
       (1612, 'Dist. Ejutla', '', 2),
       (1613, 'Dist. Etla', '', 2),
       (1614, 'Dist. Huajuapam', '', 2),
       (1615, 'Dist. Ixtlan', '', 2),
       (1616, 'Dist. Jamiltepec', '', 2),
       (1617, 'Dist. Juchitan', '', 2),
       (1618, 'Dist. Juquila', '', 2),
       (1619, 'Dist. Juxtlahuaca', '', 2),
       (1620, 'Dist. Miahuatlan', '', 2),
       (1621, 'Dist. Mixe', '', 2),
       (1622, 'Dist. Nochixtlan', '', 2),
       (1623, 'Dist. Ocotlan', '', 2),
       (1624, 'Dist. Pochutla', '', 2),
       (1625, 'Dist. Putla', '', 2),
       (1626, 'Dist. Silacayoapam', '', 2),
       (1627, 'Dist. Sola de Vega', '', 2),
       (1628, 'Dist. Tehuantepec', '', 2),
       (1629, 'Dist. Teotitlan', '', 2),
       (1630, 'Dist. Teposcolula', '', 2),
       (1631, 'Dist. Tlacolula', '', 2),
       (1632, 'Dist. Tlaxiaco', '', 2),
       (1633, 'Dist. Tuxtepec', '', 2),
       (1634, 'Dist. Villa Alta', '', 2),
       (1635, 'Dist. Yautepec', '', 2),
       (1636, 'Dist. Zaachila', '', 2),
       (1637, 'Dist. Zimatlan', '', 2),
       (1638, 'Divisaderos', '', 2),
       (1639, 'Doctor Arroyo', '', 2),
       (1640, 'Doctor Coss', '', 2),
       (1641, 'Doctor González', '', 2),
       (1642, 'Doctor Mora', '', 2),
       (1643, 'Dolores Hidalgo', '', 2),
       (1644, 'Domingo Arenas', '', 2),
       (1645, 'Donato Guerra', '', 2),
       (1646, 'Dr. Belisario Domínguez', '', 2),
       (1647, 'Durango', '', 2),
       (1648, 'Dzan', '', 2),
       (1649, 'Dzemul', '', 2),
       (1650, 'Dzidzantún', '', 2),
       (1651, 'Dzilam de Bravo', '', 2),
       (1652, 'Dzilam González', '', 2),
       (1653, 'Dzitás', '', 2),
       (1654, 'Dzoncauich', '', 2),
       (1655, 'Ebano', '', 2),
       (1656, 'Ecatepec de Morelos', '', 2),
       (1657, 'Ecatzingo', '', 2),
       (1658, 'Ecuandureo', '', 2),
       (1659, 'Eduardo Neri', '', 2),
       (1660, 'Ejutla', '', 2),
       (1661, 'El Arenal', '', 2),
       (1662, 'El Arenal', '', 2),
       (1663, 'El Bosque', '', 2),
       (1664, 'El Carmen Tequexquitla', '', 2),
       (1665, 'El Fuerte', '', 2),
       (1666, 'El Grullo', '', 2),
       (1667, 'El Limón', '', 2),
       (1668, 'El Mante', '', 2),
       (1669, 'El Marqués', '', 2),
       (1670, 'El Nayar', '', 2),
       (1671, 'El Oro', '', 2),
       (1672, 'El Oro', '', 2),
       (1673, 'El Porvenir', '', 2),
       (1674, 'El Rosario', '', 2),
       (1675, 'El Salto', '', 2),
       (1676, 'El Salvador', '', 2),
       (1677, 'El Tule', '', 2),
       (1678, 'Elota', '', 2),
       (1679, 'Eloxochitlán', '', 2),
       (1680, 'Eloxochitlán', '', 2),
       (1681, 'Emiliano Zapata', '', 2),
       (1682, 'Emiliano Zapata', '', 2),
       (1683, 'Emiliano Zapata', '', 2),
       (1684, 'Emiliano Zapata', '', 2),
       (1685, 'Empalme', '', 2),
       (1686, 'Encarnación de Díaz', '', 2),
       (1687, 'Ensenada', '', 2),
       (1688, 'Epatlán', '', 2),
       (1689, 'Epazoyucan', '', 2),
       (1690, 'Epitacio Huerta', '', 2),
       (1691, 'Erongarícuaro', '', 2),
       (1692, 'Erongarícuaro', '', 2),
       (1693, 'Escobedo', '', 2),
       (1694, 'Escuinapa', '', 2),
       (1695, 'Escuintla', '', 2),
       (1696, 'Españita', '', 2),
       (1697, 'Esperanza', '', 2),
       (1698, 'Espinal', '', 2),
       (1699, 'Espita', '', 2),
       (1700, 'Etchojoa', '', 2),
       (1701, 'Etzatlán', '', 2),
       (1702, 'Ezequiel Montes', '', 2),
       (1703, 'Felipe Carrillo', '', 2),
       (1704, 'Felipe Carrillo Puerto', '', 2),
       (1705, 'Filomeno Mata', '', 2),
       (1706, 'Florencio Villarreal', '', 2),
       (1707, 'Fortín', '', 2),
       (1708, 'Francisco I. Madero', '', 2),
       (1709, 'Francisco I. Madero', '', 2),
       (1710, 'Francisco León', '', 2),
       (1711, 'Francisco Z. Mena', '', 2),
       (1712, 'Fresnillo', '', 2),
       (1713, 'Frontera', '', 2),
       (1714, 'Frontera Comalapa', '', 2),
       (1715, 'Frontera Hidalgo', '', 2),
       (1716, 'Fronteras', '', 2),
       (1717, 'Gabriel Zamora', '', 2),
       (1718, 'Galeana', '', 2),
       (1719, 'Galeana', '', 2),
       (1720, 'García', '', 2),
       (1721, 'Genaro Codina', '', 2),
       (1722, 'General Bravo', '', 2),
       (1723, 'General Canuto A. Neri', '', 2),
       (1724, 'General Cepeda', '', 2),
       (1725, 'General Enrique Estrada', '', 2),
       (1726, 'General Escobedo', '', 2),
       (1727, 'General Felipe Angeles', '', 2),
       (1728, 'General Francisco R. Murguía', '', 2),
       (1729, 'General Heliodoro Castillo', '', 2),
       (1730, 'General Joaquin Amaro', '', 2),
       (1731, 'General Pánfilo Natera', '', 2),
       (1732, 'General Simón Bolívar', '', 2),
       (1733, 'General Terán', '', 2),
       (1734, 'General Treviño', '', 2),
       (1735, 'General Trias', '', 2),
       (1736, 'General Zaragoza', '', 2),
       (1737, 'General Zuazua', '', 2),
       (1738, 'Gómez Farias', '', 2),
       (1739, 'Gómez Farías', '', 2),
       (1740, 'Gómez Palacio', '', 2),
       (1741, 'González', '', 2),
       (1742, 'Gran Morelos', '', 2),
       (1743, 'Granados', '', 2),
       (1744, 'Guachinango', '', 2),
       (1745, 'Guachochic', '', 2),
       (1746, 'Guadalajara', '', 2),
       (1747, 'Guadalcazar', '', 2),
       (1748, 'Guadalupe', '', 2),
       (1749, 'Guadalupe', '', 2),
       (1750, 'Guadalupe', '', 2),
       (1751, 'Guadalupe', '', 2),
       (1752, 'Guadalupe Victoria', '', 2),
       (1753, 'Guadalupe Victoria', '', 2),
       (1754, 'Guadalupe y Calvo', '', 2),
       (1755, 'Guanaceví', '', 2),
       (1756, 'Guanajuato', '', 2),
       (1757, 'Guasave', '', 2),
       (1758, 'Guaymas', '', 2),
       (1759, 'Guazapares', '', 2),
       (1760, 'Güémez', '', 2),
       (1761, 'Guerrero', '', 2),
       (1762, 'Guerrero', '', 2),
       (1763, 'Guerrero', '', 2),
       (1764, 'Gustavo A. Madero', '', 2),
       (1765, 'Gustavo Díaz Ordáz', '', 2),
       (1766, 'Gutiérrez Zamora', '', 2),
       (1767, 'Halachó', '', 2),
       (1768, 'Hecelchakán', '', 2),
       (1769, 'Hermenegildo Galeana', '', 2),
       (1770, 'Hermosillo', '', 2),
       (1771, 'Hidalgo', '', 2),
       (1772, 'Hidalgo', '', 2),
       (1773, 'Hidalgo', '', 2),
       (1774, 'Hidalgo', '', 2),
       (1775, 'Hidalgo', '', 2),
       (1776, 'Hidalgo del Parral', '', 2),
       (1777, 'Hidalgotitlán', '', 2),
       (1778, 'Higueras', '', 2),
       (1779, 'Hocabá', '', 2),
       (1780, 'Hoctún', '', 2),
       (1781, 'Homún', '', 2),
       (1782, 'Honey', '', 2),
       (1783, 'Hopelchén', '', 2),
       (1784, 'Hostotipaquillo', '', 2),
       (1785, 'Huachinera', '', 2),
       (1786, 'Huajicori', '', 2),
       (1787, 'Huamantla', '', 2),
       (1788, 'Huamuxtitlán', '', 2),
       (1789, 'Huandacareo', '', 2),
       (1790, 'Huanímaro', '', 2),
       (1791, 'Huaniqueo', '', 2),
       (1792, 'Huanusco', '', 2),
       (1793, 'Huaquechula', '', 2),
       (1794, 'Huásabas', '', 2),
       (1795, 'Huasca de Ocampo', '', 2),
       (1796, 'Huatabampo', '', 2),
       (1797, 'Huatlatlauca', '', 2),
       (1798, 'Huatusco', '', 2),
       (1799, 'Huauchinango', '', 2),
       (1800, 'Huautla', '', 2),
       (1801, 'Huayacocotla', '', 2),
       (1802, 'Huazalingo', '', 2),
       (1803, 'Huehuetán', '', 2),
       (1804, 'Huehuetla', '', 2),
       (1805, 'Huehuetla', '', 2),
       (1806, 'Huehuetlán', '', 2),
       (1807, 'Huehuetlán el Chico', '', 2),
       (1808, 'Huehuetlán el Grande', '', 2),
       (1809, 'Huehuetoca', '', 2),
       (1810, 'Huejotitán', '', 2),
       (1811, 'Huejotzingo', '', 2),
       (1812, 'Huejúcar', '', 2),
       (1813, 'Huejuquilla el Alto', '', 2),
       (1814, 'Huejutla de Reyes', '', 2),
       (1815, 'Huépac', '', 2),
       (1816, 'Huetamo', '', 2),
       (1817, 'Hueyapan', '', 2),
       (1818, 'Hueyapan de Ocampo', '', 2),
       (1819, 'Hueyotlipan', '', 2),
       (1820, 'Hueypoxtla', '', 2),
       (1821, 'Hueytamalco', '', 2),
       (1822, 'Hueytlalpan', '', 2),
       (1823, 'Huhí', '', 2),
       (1824, 'Huichapan', '', 2),
       (1825, 'Huiloapan de Cuauhtémoc', '', 2),
       (1826, 'Huimanguillo', '', 2),
       (1827, 'Huimilpan', '', 2),
       (1828, 'Huiramba', '', 2),
       (1829, 'Huitiupán', '', 2),
       (1830, 'Huitzilac', '', 2),
       (1831, 'Huitzilan de Serdán', '', 2),
       (1832, 'Huitziltepec', '', 2),
       (1833, 'Huitzuco de los Figueroa', '', 2),
       (1834, 'Huixquilucan', '', 2),
       (1835, 'Huixtán', '', 2),
       (1836, 'Huixtla', '', 2),
       (1837, 'Hunucmá', '', 2),
       (1838, 'Ignacio Allende', '', 2),
       (1839, 'Ignacio de la Llave', '', 2),
       (1840, 'Ignacio Zaragoza', '', 2),
       (1841, 'Iguala de la Independencia', '', 2),
       (1842, 'Igualapa', '', 2),
       (1843, 'Ilamatlán', '', 2),
       (1844, 'Imuris', '', 2),
       (1845, 'Indé', '', 2),
       (1846, 'Indoparapeo', '', 2),
       (1847, 'Irapuato', '', 2),
       (1848, 'Irimbo', '', 2),
       (1849, 'Isidro Fabela', '', 2),
       (1850, 'Isla', '', 2),
       (1851, 'Isla Altamira', '', 2),
       (1852, 'Isla Mujeres', '', 2),
       (1853, 'Isla Tiburon', '', 2),
       (1854, 'Iturbide', '', 2),
       (1855, 'Ixcamilpa de Guerrero', '', 2),
       (1856, 'Ixcaquixtla', '', 2),
       (1857, 'Ixcateopan de Cuauhtémoc', '', 2),
       (1858, 'Ixcatepec', '', 2),
       (1859, 'Ixhuacán de los Reyes', '', 2),
       (1860, 'Ixhuatán', '', 2),
       (1861, 'Ixhuatlán de Madero', '', 2),
       (1862, 'Ixhuatlán del Café', '', 2),
       (1863, 'Ixhuatlán del Sureste', '', 2),
       (1864, 'Ixhuatlancillo', '', 2),
       (1865, 'Ixil', '', 2),
       (1866, 'Ixmatlahuacan', '', 2),
       (1867, 'Ixmiquilpan', '', 2),
       (1868, 'Ixtacamaxtitlán', '', 2),
       (1869, 'Ixtacomitán', '', 2),
       (1870, 'Ixtacuixtla de Mariano Matamoros', '', 2),
       (1871, 'Ixtaczoquitlán', '', 2),
       (1872, 'Ixtapa', '', 2),
       (1873, 'Ixtapaluca', '', 2),
       (1874, 'Ixtapan de la Sal', '', 2),
       (1875, 'Ixtapan del Oro', '', 2),
       (1876, 'Ixtapangajoya', '', 2),
       (1877, 'Ixtenco', '', 2),
       (1878, 'Ixtepec', '', 2),
       (1879, 'Ixtlahuaca', '', 2),
       (1880, 'Ixtlahuacán', '', 2),
       (1881, 'Ixtlahuacán de los Membrillos', '', 2),
       (1882, 'Ixtlahuacán del Río', '', 2),
       (1883, 'Ixtlán', '', 2),
       (1884, 'Ixtlán del Río', '', 2),
       (1885, 'Izamal', '', 2),
       (1886, 'Iztacalco', '', 2),
       (1887, 'Iztapalapa', '', 2),
       (1888, 'Izúcar de Matamoros', '', 2),
       (1889, 'Jacala de Ledezma', '', 2),
       (1890, 'Jacona', '', 2),
       (1891, 'Jala', '', 2),
       (1892, 'Jalancingo', '', 2),
       (1893, 'Jalapa', '', 2),
       (1894, 'Jalatlaco', '', 2),
       (1895, 'Jalcomulco', '', 2),
       (1896, 'Jalostotitlán', '', 2),
       (1897, 'Jalpa', '', 2),
       (1898, 'Jalpa de Méndez', '', 2),
       (1899, 'Jalpan de Serra', '', 2),
       (1900, 'Jaltenco', '', 2),
       (1901, 'Jáltipan', '', 2),
       (1902, 'Jaltocán', '', 2),
       (1903, 'Jamapa', '', 2),
       (1904, 'Jamay', '', 2),
       (1905, 'Janos', '', 2),
       (1906, 'Jantetelco', '', 2),
       (1907, 'Jaral del Progreso', '', 2),
       (1908, 'Jaumave', '', 2),
       (1909, 'Jerécuaro', '', 2),
       (1910, 'Jerez', '', 2),
       (1911, 'Jesús Carranza', '', 2),
       (1912, 'Jesús María', '', 2),
       (1913, 'Jesús María', '', 2),
       (1914, 'Jilotepec', '', 2),
       (1915, 'Jilotepec', '', 2),
       (1916, 'Jilotlán de los Dolores', '', 2),
       (1917, 'Jilotzingo', '', 2),
       (1918, 'Jiménez', '', 2),
       (1919, 'Jiménez', '', 2),
       (1920, 'Jiménez', '', 2),
       (1921, 'Jiménez', '', 2),
       (1922, 'Jiménez del Teul', '', 2),
       (1923, 'Jiquilpan', '', 2),
       (1924, 'Jiquipilas', '', 2),
       (1925, 'Jiquipilco', '', 2),
       (1926, 'Jitotol', '', 2),
       (1927, 'Jiutepec', '', 2),
       (1928, 'Jocotepec', '', 2),
       (1929, 'Jocotitlán', '', 2),
       (1930, 'Jojutla', '', 2),
       (1931, 'Jolalpan', '', 2),
       (1932, 'Jonacatepec', '', 2),
       (1933, 'Jonotla', '', 2),
       (1934, 'Jonuta', '', 2),
       (1935, 'Jopala', '', 2),
       (1936, 'Joquicingo', '', 2),
       (1937, 'José Azueta', '', 2),
       (1938, 'José Azueta', '', 2),
       (1939, 'José María Morelos', '', 2),
       (1940, 'José Sixto Verduzco', '', 2),
       (1941, 'Juan Aldama', '', 2),
       (1942, 'Juan C. Bonilla', '', 2),
       (1943, 'Juan Galindo', '', 2),
       (1944, 'Juan N. Méndez', '', 2),
       (1945, 'Juan R. Escudero', '', 2),
       (1946, 'Juan Rodríguez Clara', '', 2),
       (1947, 'Juanacatlán', '', 2),
       (1948, 'Juárez', '', 2),
       (1949, 'Juárez', '', 2),
       (1950, 'Juárez', '', 2),
       (1951, 'Juárez', '', 2),
       (1952, 'Juárez', '', 2),
       (1953, 'Juárez Hidalgo', '', 2),
       (1954, 'Juchipila', '', 2),
       (1955, 'Juchique de Ferrer', '', 2),
       (1956, 'Juchitepec', '', 2),
       (1957, 'Juchitlán', '', 2),
       (1958, 'Julimes', '', 2),
       (1959, 'Jungapeo', '', 2),
       (1960, 'Kanasín', '', 2),
       (1961, 'Kantunil', '', 2),
       (1962, 'Kaua', '', 2),
       (1963, 'Kinchil', '', 2),
       (1964, 'Kopomá', '', 2),
       (1965, 'La Antigua', '', 2),
       (1966, 'La Barca', '', 2),
       (1967, 'La Colorada', '', 2),
       (1968, 'La Concordia', '', 2),
       (1969, 'La Cruz', '', 2),
       (1970, 'La Grandeza', '', 2),
       (1971, 'La Huacana', '', 2),
       (1972, 'La Huerta', '', 2),
       (1973, 'La Independencia', '', 2),
       (1974, 'La Libertad', '', 2),
       (1975, 'La Magdalena Tlatlauquitepec', '', 2),
       (1976, 'La Manzanilla de la Paz', '', 2),
       (1977, 'La Misión', '', 2),
       (1978, 'La Paz', '', 2),
       (1979, 'La Paz', '', 2),
       (1980, 'La Perla', '', 2),
       (1981, 'La Piedad', '', 2),
       (1982, 'La Trinitaria', '', 2),
       (1983, 'La Union', '', 2),
       (1984, 'La Yesca', '', 2),
       (1985, 'Lafragua', '', 2),
       (1986, 'Lago de Chapala', '', 2),
       (1987, 'Lagos de Moreno', '', 2),
       (1988, 'Lagunillas', '', 2),
       (1989, 'Lagunillas', '', 2),
       (1990, 'Lamadrid', '', 2),
       (1991, 'Lampazos de Naranjo', '', 2),
       (1992, 'Landa de Matamoros', '', 2),
       (1993, 'Landero y Coss', '', 2),
       (1994, 'Larráinzar', '', 2),
       (1995, 'Las Choapas', '', 2),
       (1996, 'Las Margaritas', '', 2),
       (1997, 'Las Minas', '', 2),
       (1998, 'Las Rosas', '', 2),
       (1999, 'Las Vigas de Ramírez', '', 2),
       (2000, 'Lázaro Cárdenas', '', 2),
       (2001, 'Lázaro Cárdenas', '', 2),
       (2002, 'León', '', 2),
       (2003, 'Leonardo Bravo', '', 2),
       (2004, 'Lerdo', '', 2),
       (2005, 'Lerdo de Tejada', '', 2),
       (2006, 'Lerma', '', 2),
       (2007, 'Linares', '', 2),
       (2008, 'Llera', '', 2),
       (2009, 'Lolotla', '', 2),
       (2010, 'López', '', 2),
       (2011, 'Loreto', '', 2),
       (2012, 'Los Aldamas', '', 2),
       (2013, 'Los Herreras', '', 2),
       (2014, 'Los Ramones', '', 2),
       (2015, 'Los Reyes', '', 2),
       (2016, 'Los Reyes', '', 2),
       (2017, 'Los Reyes de Juárez', '', 2),
       (2018, 'Luis Moya', '', 2),
       (2019, 'Macuspana', '', 2),
       (2020, 'Madera', '', 2),
       (2021, 'Madero', '', 2),
       (2022, 'Magdalena', '', 2),
       (2023, 'Magdalena', '', 2),
       (2024, 'Magdalena', '', 2),
       (2025, 'Magdalena Contreras', '', 2),
       (2026, 'Maguarichic', '', 2),
       (2027, 'Mainero', '', 2),
       (2028, 'Malinalco', '', 2),
       (2029, 'Malinaltepec', '', 2),
       (2030, 'Maltrata', '', 2),
       (2031, 'Mama', '', 2),
       (2032, 'Maní', '', 2),
       (2033, 'Manlio Fabio Altamirano', '', 2),
       (2034, 'Manuel Benavides', '', 2),
       (2035, 'Manuel Doblado', '', 2),
       (2036, 'Manuel M. Dieguez', '', 2),
       (2037, 'Manzanillo', '', 2),
       (2038, 'Mapastepec', '', 2),
       (2039, 'Mapimí', '', 2),
       (2040, 'Maravatío', '', 2),
       (2041, 'Marcos Castellanos', '', 2),
       (2042, 'Mariano Escobedo', '', 2),
       (2043, 'Marín', '', 2),
       (2044, 'Martínez de la Torre', '', 2),
       (2045, 'Mártir de Cuilapan', '', 2),
       (2046, 'Mascota', '', 2),
       (2047, 'Matachic', '', 2),
       (2048, 'Matamoros', '', 2),
       (2049, 'Matamoros', '', 2),
       (2050, 'Matamoros', '', 2),
       (2051, 'Matehuala', '', 2),
       (2052, 'Maxcanú', '', 2),
       (2053, 'Mayapán', '', 2),
       (2054, 'Mazamitla', '', 2),
       (2055, 'Mazapa de Madero', '', 2),
       (2056, 'Mazapil', '', 2),
       (2057, 'Mazapiltepec de Juárez', '', 2),
       (2058, 'Mazatán', '', 2),
       (2059, 'Mazatán', '', 2),
       (2060, 'Mazatecochco de José María Morelos', '', 2),
       (2061, 'Mazatepec', '', 2),
       (2062, 'Mazatlán', '', 2),
       (2063, 'Mecatlán', '', 2),
       (2064, 'Mecayapan', '', 2),
       (2065, 'Medellín', '', 2),
       (2066, 'Melchor Ocampo', '', 2),
       (2067, 'Melchor Ocampo', '', 2),
       (2068, 'Melchor Ocampo', '', 2),
       (2069, 'Méndez', '', 2),
       (2070, 'Meoqui', '', 2),
       (2071, 'Mérida', '', 2),
       (2072, 'Metapa', '', 2),
       (2073, 'Metepec', '', 2),
       (2074, 'Metepec', '', 2),
       (2075, 'Metlatónoc', '', 2),
       (2076, 'Metztitlán', '', 2),
       (2077, 'Mexicali', '', 2),
       (2078, 'Mexicaltzingo', '', 2),
       (2079, 'Mexquitic de Carmona', '', 2),
       (2080, 'Mexticacán', '', 2),
       (2081, 'Mezquital', '', 2),
       (2082, 'Mezquital del Oro', '', 2),
       (2083, 'Mezquitic', '', 2),
       (2084, 'Miacatlán', '', 2),
       (2085, 'Mier', '', 2),
       (2086, 'Mier y Noriega', '', 2),
       (2087, 'Miguel Alemán', '', 2),
       (2088, 'Miguel Auza', '', 2),
       (2089, 'Miguel Hidalgo', '', 2),
       (2090, 'Mihuatlán', '', 2),
       (2091, 'Milpa Alta', '', 2),
       (2092, 'Mina', '', 2),
       (2093, 'Minatitlán', '', 2),
       (2094, 'Minatitlán', '', 2),
       (2095, 'Mineral de la Reforma', '', 2),
       (2096, 'Mineral del Chico', '', 2),
       (2097, 'Mineral del Monte', '', 2),
       (2098, 'Miquihuana', '', 2),
       (2099, 'Misantla', '', 2),
       (2100, 'Mitontic', '', 2),
       (2101, 'Mixquiahuala de Juárez', '', 2),
       (2102, 'Mixtla', '', 2),
       (2103, 'Mixtla de Altamirano', '', 2),
       (2104, 'Mixtlán', '', 2),
       (2105, 'Mochitlán', '', 2),
       (2106, 'Mocochá', '', 2),
       (2107, 'Mocorito', '', 2),
       (2108, 'Moctezuma', '', 2),
       (2109, 'Moctezuma', '', 2),
       (2110, 'Molango de Escamilla', '', 2),
       (2111, 'Molcaxac', '', 2),
       (2112, 'Moloacán', '', 2),
       (2113, 'Momax', '', 2),
       (2114, 'Monclova', '', 2),
       (2115, 'Monte Escobedo', '', 2),
       (2116, 'Montemorelos', '', 2),
       (2117, 'Monterrey', '', 2),
       (2118, 'Morelia', '', 2),
       (2119, 'Morelos', '', 2),
       (2120, 'Morelos', '', 2),
       (2121, 'Morelos', '', 2),
       (2122, 'Morelos', '', 2),
       (2123, 'Morelos', '', 2),
       (2124, 'Moris', '', 2),
       (2125, 'Moroleón', '', 2),
       (2126, 'Motozintla', '', 2),
       (2127, 'Motul', '', 2),
       (2128, 'Moyahua de Estrada', '', 2),
       (2129, 'Múgica', '', 2),
       (2130, 'Mulegé', '', 2),
       (2131, 'Mulegé', '', 2),
       (2132, 'Muna', '', 2),
       (2133, 'Muñoz de Domingo Arenas', '', 2),
       (2134, 'Muxupip', '', 2),
       (2135, 'Múzquiz', '', 2),
       (2136, 'NA', '', 2),
       (2137, 'Nacajuca', '', 2),
       (2138, 'Naco', '', 2),
       (2139, 'Nácori Chico', '', 2),
       (2140, 'Nacozari de García', '', 2),
       (2141, 'Nadadores', '', 2),
       (2142, 'Nahuatzen', '', 2),
       (2143, 'Namiquipa', '', 2),
       (2144, 'Nanacamilpa de Mariano Arista', '', 2),
       (2145, 'Naolinco', '', 2),
       (2146, 'Naranjal', '', 2),
       (2147, 'Natívitas', '', 2),
       (2148, 'Naucalpan de Juárez', '', 2),
       (2149, 'Naupan', '', 2),
       (2150, 'Nautla', '', 2),
       (2151, 'Nauzontla', '', 2),
       (2152, 'Nava', '', 2),
       (2153, 'Navojoa', '', 2),
       (2154, 'Nazas', '', 2),
       (2155, 'Nealtican', '', 2),
       (2156, 'Nextlalpan', '', 2),
       (2157, 'Nezahualcóyotl', '', 2),
       (2158, 'Nicolás Bravo', '', 2),
       (2159, 'Nicolás Flores', '', 2),
       (2160, 'Nicolás Romero', '', 2),
       (2161, 'Nicolás Ruíz', '', 2),
       (2162, 'Nochistlán de Mejía', '', 2),
       (2163, 'Nocupétaro', '', 2),
       (2164, 'Nogales', '', 2),
       (2165, 'Nogales', '', 2),
       (2166, 'Nombre de Dios', '', 2),
       (2167, 'Nonoava', '', 2),
       (2168, 'Nopala de Villagrán', '', 2),
       (2169, 'Nopaltepec', '', 2),
       (2170, 'Nopalucan', '', 2),
       (2171, 'Noria de Angeles', '', 2),
       (2172, 'Nueva Rosita', '', 2),
       (2173, 'Nuevo Casas Grandes', '', 2),
       (2174, 'Nuevo Laredo', '', 2),
       (2175, 'Nuevo Morelos', '', 2),
       (2176, 'Nuevo Paranguricutiro', '', 2),
       (2177, 'Nuevo Urecho', '', 2),
       (2178, 'Numarán', '', 2),
       (2179, 'Ocampo', '', 2),
       (2180, 'Ocampo', '', 2),
       (2181, 'Ocampo', '', 2),
       (2182, 'Ocampo', '', 2),
       (2183, 'Ocampo', '', 2),
       (2184, 'Ocampo', '', 2),
       (2185, 'Ocosingo', '', 2),
       (2186, 'Ocotepec', '', 2),
       (2187, 'Ocotepec', '', 2),
       (2188, 'Ocotlán', '', 2),
       (2189, 'Ocoyoacac', '', 2),
       (2190, 'Ocoyucan', '', 2),
       (2191, 'Ocozocoautla de Espinosa', '', 2),
       (2192, 'Ocuilan', '', 2),
       (2193, 'Ocuituco', '', 2),
       (2194, 'Ojinaga', '', 2),
       (2195, 'Ojocaliente', '', 2),
       (2196, 'Ojuelos de Jalisco', '', 2),
       (2197, 'Olinalá', '', 2),
       (2198, 'Olintla', '', 2),
       (2199, 'Oluta', '', 2),
       (2200, 'Omealca', '', 2),
       (2201, 'Ometepec', '', 2),
       (2202, 'Omitlán de Juárez', '', 2),
       (2203, 'Onavas', '', 2),
       (2204, 'Opichén', '', 2),
       (2205, 'Opodepe', '', 2),
       (2206, 'Oquitoa', '', 2),
       (2207, 'Oriental', '', 2),
       (2208, 'Orizaba', '', 2),
       (2209, 'Ostuacán', '', 2),
       (2210, 'Osumacinta', '', 2),
       (2211, 'Otáez', '', 2),
       (2212, 'Otatitlán', '', 2),
       (2213, 'Oteapan', '', 2),
       (2214, 'Othón P. Blanco', '', 2),
       (2215, 'Otumba', '', 2),
       (2216, 'Otzoloapan', '', 2),
       (2217, 'Otzolotepec', '', 2),
       (2218, 'Oxchuc', '', 2),
       (2219, 'Oxkutzcab', '', 2),
       (2220, 'Ozuluama', '', 2),
       (2221, 'Ozumba', '', 2),
       (2222, 'Pabellón de Arteaga', '', 2),
       (2223, 'Pachuca de Soto', '', 2),
       (2224, 'Pacula', '', 2),
       (2225, 'Padilla', '', 2),
       (2226, 'Pahuatlán', '', 2),
       (2227, 'Pajacuarán', '', 2),
       (2228, 'Pajapan', '', 2),
       (2229, 'Palenque', '', 2),
       (2230, 'Palizada', '', 2),
       (2231, 'Palmar de Bravo', '', 2),
       (2232, 'Palmillas', '', 2),
       (2233, 'Panabá', '', 2),
       (2234, 'Panindícuaro', '', 2),
       (2235, 'Panotla', '', 2),
       (2236, 'Pantelhó', '', 2),
       (2237, 'Pantepec', '', 2),
       (2238, 'Pantepec', '', 2),
       (2239, 'Pánuco', '', 2),
       (2240, 'Pánuco', '', 2),
       (2241, 'Pánuco de Coronado', '', 2),
       (2242, 'Papalotla', '', 2),
       (2243, 'Papalotla de Xicohténcatl', '', 2),
       (2244, 'Papantla', '', 2),
       (2245, 'Paracho', '', 2),
       (2246, 'Parácuaro', '', 2),
       (2247, 'Paraíso', '', 2),
       (2248, 'Parás', '', 2),
       (2249, 'Parras', '', 2),
       (2250, 'Paso de Ovejas', '', 2),
       (2251, 'Paso del Macho', '', 2),
       (2252, 'Pátzcuaro', '', 2),
       (2253, 'Pedro Ascencio Alquisiras', '', 2),
       (2254, 'Pedro Escobedo', '', 2),
       (2255, 'Peñamiller', '', 2),
       (2256, 'Penjamillo', '', 2),
       (2257, 'Pénjamo', '', 2),
       (2258, 'Peñón Blanco', '', 2),
       (2259, 'Peribán', '', 2),
       (2260, 'Perote', '', 2),
       (2261, 'Pesquería', '', 2),
       (2262, 'Petatlán', '', 2),
       (2263, 'Petlalcingo', '', 2),
       (2264, 'Peto', '', 2),
       (2265, 'Piaxtla', '', 2),
       (2266, 'Pichucalco', '', 2),
       (2267, 'Piedras Negras', '', 2),
       (2268, 'Pihuamo', '', 2),
       (2269, 'Pijijiapan', '', 2),
       (2270, 'Pilcaya', '', 2),
       (2271, 'Pinal de Amoles', '', 2),
       (2272, 'Pinos', '', 2),
       (2273, 'Pinos', '', 2),
       (2274, 'Pisaflores', '', 2),
       (2275, 'Pitiquito', '', 2),
       (2276, 'Platón Sánchez', '', 2),
       (2277, 'Playa Vicente', '', 2),
       (2278, 'Poanas', '', 2),
       (2279, 'Polotitlán', '', 2),
       (2280, 'Poncitlán', '', 2),
       (2281, 'Poza Rica de Hidalgo', '', 2),
       (2282, 'Praxedis G. Guerrero', '', 2),
       (2283, 'Progreso', '', 2),
       (2284, 'Progreso', '', 2),
       (2285, 'Progreso de Obregón', '', 2),
       (2286, 'Progreso de Zaragoza', '', 2),
       (2287, 'Puebla', '', 2),
       (2288, 'Pueblo Nuevo', '', 2),
       (2289, 'Pueblo Nuevo', '', 2),
       (2290, 'Pueblo Nuevo Solistahuacán', '', 2),
       (2291, 'Pueblo Viejo', '', 2),
       (2292, 'Puente de Ixtla', '', 2),
       (2293, 'Puente Nacional', '', 2),
       (2294, 'Puerto Peñasco', '', 2),
       (2295, 'Puerto Vallarta', '', 2),
       (2296, 'Pungarabato', '', 2),
       (2297, 'Purépero', '', 2),
       (2298, 'Purísima del Rincón', '', 2),
       (2299, 'Puruándiro', '', 2),
       (2300, 'Quecholac', '', 2),
       (2301, 'Quechultenango', '', 2),
       (2302, 'Queréndaro', '', 2),
       (2303, 'Querétaro', '', 2),
       (2304, 'Quimixtlán', '', 2),
       (2305, 'Quintana Roo', '', 2),
       (2306, 'Quiriego', '', 2),
       (2307, 'Quiroga', '', 2),
       (2308, 'Quitupan', '', 2),
       (2309, 'Rafael Delgado', '', 2),
       (2310, 'Rafael Lara Grajales', '', 2),
       (2311, 'Rafael Lucio', '', 2),
       (2312, 'Ramos Arizpe', '', 2),
       (2313, 'Rayón', '', 2),
       (2314, 'Rayón', '', 2),
       (2315, 'Rayón', '', 2),
       (2316, 'Rayón', '', 2),
       (2317, 'Rayones', '', 2),
       (2318, 'Reforma', '', 2),
       (2319, 'Reynosa', '', 2),
       (2320, 'Rincón de Romos', '', 2),
       (2321, 'Río Blanco', '', 2),
       (2322, 'Río Bravo', '', 2),
       (2323, 'Río Grande', '', 2),
       (2324, 'Río Lagartos', '', 2),
       (2325, 'Rioverde', '', 2),
       (2326, 'Riva Palacio', '', 2),
       (2327, 'Rodeo', '', 2),
       (2328, 'Romita', '', 2),
       (2329, 'Rosales', '', 2),
       (2330, 'Rosamorada', '', 2),
       (2331, 'Rosario', '', 2),
       (2332, 'Rosario', '', 2),
       (2333, 'Ruíz', '', 2),
       (2334, 'Sabanilla', '', 2),
       (2335, 'Sabinas', '', 2),
       (2336, 'Sabinas Hidalgo', '', 2),
       (2337, 'Sacalum', '', 2),
       (2338, 'Sacramento', '', 2),
       (2339, 'Sahuaripa', '', 2),
       (2340, 'Sahuayo', '', 2),
       (2341, 'Saín Alto', '', 2),
       (2342, 'Salamanca', '', 2),
       (2343, 'Salinas', '', 2),
       (2344, 'Salinas Victoria', '', 2),
       (2345, 'Saltabarranca', '', 2),
       (2346, 'Saltillo', '', 2),
       (2347, 'Salto de Agua', '', 2),
       (2348, 'Salvador Alvarado', '', 2),
       (2349, 'Salvador Escalante', '', 2),
       (2350, 'Salvatierra', '', 2),
       (2351, 'Samahil', '', 2),
       (2352, 'San Agustín Metzquititlán', '', 2),
       (2353, 'San Agustín Tlaxiaca', '', 2),
       (2354, 'San Andrés Cholula', '', 2),
       (2355, 'San Andrés Tenejapan', '', 2),
       (2356, 'San Andrés Tuxtla', '', 2),
       (2357, 'San Antonio', '', 2),
       (2358, 'San Antonio Cañada', '', 2),
       (2359, 'San Antonio La Isla', '', 2),
       (2360, 'San Bartolo Tutotepec', '', 2),
       (2361, 'San Bernardo', '', 2),
       (2362, 'San Blas', '', 2),
       (2363, 'San Buenaventura', '', 2),
       (2364, 'San Carlos', '', 2),
       (2365, 'San Ciro de Acosta', '', 2),
       (2366, 'San Cristóbal de la Barranca', '', 2),
       (2367, 'San Cristóbal de las Casas', '', 2),
       (2368, 'San Diego de Alejandría', '', 2),
       (2369, 'San Diego de la Unión', '', 2),
       (2370, 'San Diego la Mesa Tochimiltzingo', '', 2),
       (2371, 'San Dimas', '', 2),
       (2372, 'San Felipe', '', 2),
       (2373, 'San Felipe', '', 2),
       (2374, 'San Felipe de Jesús', '', 2),
       (2375, 'San Felipe del Progreso', '', 2),
       (2376, 'San Felipe Orizatlán', '', 2),
       (2377, 'San Felipe Teotlalcingo', '', 2),
       (2378, 'San Felipe Tepatlán', '', 2),
       (2379, 'San Fernando', '', 2),
       (2380, 'San Fernando', '', 2),
       (2381, 'San Francisco de Borja', '', 2),
       (2382, 'San Francisco de Conchos', '', 2),
       (2383, 'San Francisco del Oro', '', 2),
       (2384, 'San Francisco del Rincón', '', 2),
       (2385, 'San Gabriel Chilac', '', 2),
       (2386, 'San Gregorio Atzompa', '', 2),
       (2387, 'San Ignacio', '', 2),
       (2388, 'San Javier', '', 2),
       (2389, 'San Jerónimo Tecuanipan', '', 2),
       (2390, 'San Jerónimo Xayacatlán', '', 2),
       (2391, 'San Joaquín', '', 2),
       (2392, 'San José Chiapa', '', 2),
       (2393, 'San José de Gracia', '', 2),
       (2394, 'San José Iturbide', '', 2),
       (2395, 'San José Miahuatlán', '', 2),
       (2396, 'San Juan Atenco', '', 2),
       (2397, 'San Juan Atzompa', '', 2),
       (2398, 'San Juan de Guadalupe', '', 2),
       (2399, 'San Juan de los Lagos', '', 2),
       (2400, 'San Juan del Río', '', 2),
       (2401, 'San Juan del Río', '', 2),
       (2402, 'San Juan Evangelista', '', 2),
       (2403, 'San Julián', '', 2),
       (2404, 'San Lucas', '', 2),
       (2405, 'San Lucas', '', 2),
       (2406, 'San Luis Acatlán', '', 2),
       (2407, 'San Luis de Cordero', '', 2),
       (2408, 'San Luis de la Paz', '', 2),
       (2409, 'San Luis Potosí', '', 2),
       (2410, 'San Luis Río Colorado', '', 2),
       (2411, 'San Marcos', '', 2),
       (2412, 'San Marcos', '', 2),
       (2413, 'San Martín Chalchicuautla', '', 2),
       (2414, 'San Martín de Bolaños', '', 2),
       (2415, 'San Martín de las Piráámides', '', 2),
       (2416, 'San Martín Hidalgo', '', 2),
       (2417, 'San Martín Texmelucan', '', 2),
       (2418, 'San Martín Totoltepec', '', 2),
       (2419, 'San Mateo Atenco', '', 2),
       (2420, 'San Matías Tlalancaleca', '', 2),
       (2421, 'San Miguel de Horcasitas', '', 2),
       (2422, 'San Miguel el Alto', '', 2),
       (2423, 'San Miguel Ixitlán', '', 2),
       (2424, 'San Miguel Totolapan', '', 2),
       (2425, 'San Miguel Xoxtla', '', 2),
       (2426, 'San Nicolás', '', 2),
       (2427, 'San Nicolás Buenos Aires', '', 2),
       (2428, 'San Nicolás de los Garza', '', 2),
       (2429, 'San Nicolás de los Ranchos', '', 2),
       (2430, 'San Nicolás Tolentino', '', 2),
       (2431, 'San Pablo Anicano', '', 2),
       (2432, 'San Pablo del Monte', '', 2),
       (2433, 'San Pedro', '', 2),
       (2434, 'San Pedro Cholula', '', 2),
       (2435, 'San Pedro de la Cueva', '', 2),
       (2436, 'San Pedro del Gallo', '', 2),
       (2437, 'San Pedro Garza García', '', 2),
       (2438, 'San Pedro Lagunillas', '', 2),
       (2439, 'San Pedro Yeloixtlahuaca', '', 2),
       (2440, 'San Salvador', '', 2),
       (2441, 'San Salvador el Seco', '', 2),
       (2442, 'San Salvador el Verde', '', 2),
       (2443, 'San Salvador Huixcolotla', '', 2),
       (2444, 'San Sebastián del Oeste', '', 2),
       (2445, 'San Sebastián Tlacotepec', '', 2),
       (2446, 'San Simón de Guerrero', '', 2),
       (2447, 'San Vicente Tancuayalab', '', 2),
       (2448, 'Sanahcat', '', 2),
       (2449, 'Sanctórum de Lázaro Cárdenas', '', 2),
       (2450, 'Santa Ana', '', 2),
       (2451, 'Santa Ana Maya', '', 2),
       (2452, 'Santa Bárbara', '', 2),
       (2453, 'Santa Catarina', '', 2),
       (2454, 'Santa Catarina', '', 2),
       (2455, 'Santa Catarina', '', 2),
       (2456, 'Santa Catarina Tlaltempan', '', 2),
       (2457, 'Santa Clara', '', 2),
       (2458, 'Santa Cruz', '', 2),
       (2459, 'Santa Cruz de Juventino Rosas', '', 2),
       (2460, 'Santa Cruz Tlaxcala', '', 2),
       (2461, 'Santa Elena', '', 2),
       (2462, 'Santa Inés Ahuatempan', '', 2),
       (2463, 'Santa Isabel Cholula', '', 2),
       (2464, 'Santa María de los Angeles', '', 2),
       (2465, 'Santa María del Oro', '', 2),
       (2466, 'Santa María del Río', '', 2),
       (2467, 'Santiago', '', 2),
       (2468, 'Santiago de Anaya', '', 2),
       (2469, 'Santiago Ixcuintla', '', 2),
       (2470, 'Santiago Maravatío', '', 2),
       (2471, 'Santiago Miahuatlán', '', 2),
       (2472, 'Santiago Papasquiaro', '', 2),
       (2473, 'Santiago Tulantepec de Lugo Guerrero', '', 2),
       (2474, 'Santiago Tuxtla', '', 2),
       (2475, 'Santo Domingo', '', 2),
       (2476, 'Santo Tomás', '', 2),
       (2477, 'Santo Tomás Hueyotlipan', '', 2),
       (2478, 'Sáric', '', 2),
       (2479, 'Satevó', '', 2),
       (2480, 'Saucillo', '', 2),
       (2481, 'Sayula', '', 2),
       (2482, 'Sayula de Alemán', '', 2),
       (2483, 'Senguio', '', 2),
       (2484, 'Seyé', '', 2),
       (2485, 'Sierra Mojada', '', 2),
       (2486, 'Silao', '', 2),
       (2487, 'Siltepec', '', 2),
       (2488, 'Simojovel', '', 2),
       (2489, 'Sinaloa', '', 2),
       (2490, 'Sinanché', '', 2),
       (2491, 'Singuilucan', '', 2),
       (2492, 'Sitalá', '', 2),
       (2493, 'Sochiapa', '', 2),
       (2494, 'Socoltenango', '', 2),
       (2495, 'Soconusco', '', 2),
       (2496, 'Soledad Atzompa', '', 2),
       (2497, 'Soledad de Doblado', '', 2),
       (2498, 'Soledad de Graciano Sánchez', '', 2),
       (2499, 'Solosuchiapa', '', 2),
       (2500, 'Soltepec', '', 2),
       (2501, 'Sombrerete', '', 2),
       (2502, 'Soteapan', '', 2),
       (2503, 'Soto La Marina', '', 2),
       (2504, 'Sotuta', '', 2),
       (2505, 'Soyaló', '', 2),
       (2506, 'Soyaniquilpan de Juárez', '', 2),
       (2507, 'Soyopa', '', 2),
       (2508, 'Suaqui Grande', '', 2),
       (2509, 'Suchiapa', '', 2),
       (2510, 'Suchiate', '', 2),
       (2511, 'Súchil', '', 2),
       (2512, 'Sucilá', '', 2),
       (2513, 'Sudzal', '', 2),
       (2514, 'Sultepec', '', 2),
       (2515, 'Suma', '', 2),
       (2516, 'Sunuapa', '', 2),
       (2517, 'Susticacán', '', 2),
       (2518, 'Susupuato', '', 2),
       (2519, 'Tabasco', '', 2),
       (2520, 'Tacámbaro', '', 2),
       (2521, 'Tacotalpa', '', 2),
       (2522, 'Tahdziú', '', 2),
       (2523, 'Tahmek', '', 2),
       (2524, 'Tala', '', 2),
       (2525, 'Talpa de Allende', '', 2),
       (2526, 'Tamalín', '', 2),
       (2527, 'Tamasopo', '', 2),
       (2528, 'Tamazula', '', 2),
       (2529, 'Tamazula de Gordiano', '', 2),
       (2530, 'Tamazunchale', '', 2),
       (2531, 'Tamiahua', '', 2),
       (2532, 'Tampacan', '', 2),
       (2533, 'Tampamolón Corona', '', 2),
       (2534, 'Tampico', '', 2),
       (2535, 'Tampico Alto', '', 2),
       (2536, 'Tamuín', '', 2),
       (2537, 'Tancanhuitz de Santos', '', 2),
       (2538, 'Tancoco', '', 2),
       (2539, 'Tangamandapio', '', 2),
       (2540, 'Tangancícuaro', '', 2),
       (2541, 'Tanhuato', '', 2),
       (2542, 'Tanlajás', '', 2),
       (2543, 'Tanquián de Escobedo', '', 2),
       (2544, 'Tantima', '', 2),
       (2545, 'Tantoyuca', '', 2),
       (2546, 'Tapachula', '', 2),
       (2547, 'Tapalapa', '', 2),
       (2548, 'Tapalpa', '', 2),
       (2549, 'Tapilula', '', 2),
       (2550, 'Tarandacuao', '', 2),
       (2551, 'Taretan', '', 2),
       (2552, 'Tarímbaro', '', 2),
       (2553, 'Tarimoro', '', 2),
       (2554, 'Tasquillo', '', 2),
       (2555, 'Tatatila', '', 2),
       (2556, 'Taxco de Alarcón', '', 2),
       (2557, 'Teabo', '', 2),
       (2558, 'Teapa', '', 2),
       (2559, 'Tecali de Herrera', '', 2),
       (2560, 'Tecalitlán', '', 2),
       (2561, 'Tecámac', '', 2),
       (2562, 'Tecamachalco', '', 2),
       (2563, 'Tecate', '', 2),
       (2564, 'Techaluta de Montenegro', '', 2),
       (2565, 'Tecoanapa', '', 2),
       (2566, 'Tecoh', '', 2),
       (2567, 'Tecolotlán', '', 2),
       (2568, 'Tecolutla', '', 2),
       (2569, 'Tecomán', '', 2),
       (2570, 'Tecomatlán', '', 2),
       (2571, 'Tecozautla', '', 2),
       (2572, 'Técpan de Galeana', '', 2),
       (2573, 'Tecpatán', '', 2),
       (2574, 'Tecuala', '', 2),
       (2575, 'Tehuacán', '', 2),
       (2576, 'Tehuipango', '', 2),
       (2577, 'Tehuitzingo', '', 2),
       (2578, 'Tejupilco', '', 2),
       (2579, 'Tekal de Venegas', '', 2),
       (2580, 'Tekantó', '', 2),
       (2581, 'Tekax', '', 2),
       (2582, 'Tekit', '', 2),
       (2583, 'Tekom', '', 2),
       (2584, 'Telchac Pueblo', '', 2),
       (2585, 'Telchac Puerto', '', 2),
       (2586, 'Teloloapan', '', 2),
       (2587, 'Temamatla', '', 2),
       (2588, 'Temapache', '', 2),
       (2589, 'Temascalapa', '', 2),
       (2590, 'Temascalcingo', '', 2),
       (2591, 'Temascaltepec', '', 2),
       (2592, 'Temax', '', 2),
       (2593, 'Temixco', '', 2),
       (2594, 'Temoac', '', 2),
       (2595, 'Temoaya', '', 2),
       (2596, 'Temósachic', '', 2),
       (2597, 'Temozón', '', 2),
       (2598, 'Tempoal', '', 2),
       (2599, 'Tenabo', '', 2),
       (2600, 'Tenamaxtlán', '', 2),
       (2601, 'Tenampa', '', 2),
       (2602, 'Tenampulco', '', 2),
       (2603, 'Tenancingo', '', 2),
       (2604, 'Tenancingo', '', 2),
       (2605, 'Tenango de Doria', '', 2),
       (2606, 'Tenango del Aire', '', 2),
       (2607, 'Tenango del Valle', '', 2),
       (2608, 'Tenejapa', '', 2),
       (2609, 'Tenochtitlán', '', 2),
       (2610, 'Tenosique', '', 2),
       (2611, 'Teocaltiche', '', 2),
       (2612, 'Teocelo', '', 2),
       (2613, 'Teocuitatlán de Corona', '', 2),
       (2614, 'Teolocholco', '', 2),
       (2615, 'Teoloyucán', '', 2),
       (2616, 'Teopantlán', '', 2),
       (2617, 'Teopisca', '', 2),
       (2618, 'Teotihuacán', '', 2),
       (2619, 'Teotlalco', '', 2),
       (2620, 'Tepache', '', 2),
       (2621, 'Tepakán', '', 2),
       (2622, 'Tepalcatepec', '', 2),
       (2623, 'Tepalcingo', '', 2),
       (2624, 'Tepanco de López', '', 2),
       (2625, 'Tepango de Rodríguez', '', 2),
       (2626, 'Tepatitlán de Morelos', '', 2),
       (2627, 'Tepatlaxco', '', 2),
       (2628, 'Tepatlaxco de Hidalgo', '', 2),
       (2629, 'Tepeaca', '', 2),
       (2630, 'Tepeapulco', '', 2),
       (2631, 'Tepechitlán', '', 2),
       (2632, 'Tepecoacuilco de Trujano', '', 2),
       (2633, 'Tepehuacán de Guerrero', '', 2),
       (2634, 'Tepehuanes', '', 2),
       (2635, 'Tepeji del Río de Ocampo', '', 2),
       (2636, 'Tepemaxalco', '', 2),
       (2637, 'Tepeojuma', '', 2),
       (2638, 'Tepetitla de Lardizábal', '', 2),
       (2639, 'Tepetitlán', '', 2),
       (2640, 'Tepetlán', '', 2),
       (2641, 'Tepetlaoxtoc', '', 2),
       (2642, 'Tepetlixpa', '', 2),
       (2643, 'Tepetongo', '', 2),
       (2644, 'Tepetzintla', '', 2),
       (2645, 'Tepetzintla', '', 2),
       (2646, 'Tepexco', '', 2),
       (2647, 'Tepexi de Rodríguez', '', 2),
       (2648, 'Tepeyahualco', '', 2),
       (2649, 'Tepeyahualco de Cuauhtémoc', '', 2),
       (2650, 'Tepeyanco', '', 2),
       (2651, 'Tepezalá', '', 2),
       (2652, 'Tepic', '', 2),
       (2653, 'Tepotzotlán', '', 2),
       (2654, 'Tepoztlán', '', 2),
       (2655, 'Tequila', '', 2),
       (2656, 'Tequila', '', 2),
       (2657, 'Tequisquiapan', '', 2),
       (2658, 'Tequixquiac', '', 2),
       (2659, 'Terrenate', '', 2),
       (2660, 'Tetecala', '', 2),
       (2661, 'Tetela de Ocampo', '', 2),
       (2662, 'Tetela del Volcán', '', 2),
       (2663, 'Teteles de Avila Castillo', '', 2),
       (2664, 'Tetepango', '', 2),
       (2665, 'Tetipac', '', 2),
       (2666, 'Tetiz', '', 2),
       (2667, 'Tetla de la Solidaridad', '', 2),
       (2668, 'Tetlatlahuca', '', 2),
       (2669, 'Teuchitlán', '', 2),
       (2670, 'Teul de González Ortega', '', 2),
       (2671, 'Texcaltitlán', '', 2),
       (2672, 'Texcalyacac', '', 2),
       (2673, 'Texcatepec', '', 2),
       (2674, 'Texcoco', '', 2),
       (2675, 'Texhuacán', '', 2),
       (2676, 'Texistepec', '', 2),
       (2677, 'Teya', '', 2),
       (2678, 'Teziutlán', '', 2),
       (2679, 'Tezonapa', '', 2),
       (2680, 'Tezontepec de Aldama', '', 2),
       (2681, 'Tezoyuca', '', 2),
       (2682, 'Tianguismanalco', '', 2),
       (2683, 'Tianguistenco', '', 2),
       (2684, 'Tianguistengo', '', 2),
       (2685, 'Ticul', '', 2),
       (2686, 'Tierra Blanca', '', 2),
       (2687, 'Tierra Blanca', '', 2),
       (2688, 'Tierra Nueva', '', 2),
       (2689, 'Tihuatlán', '', 2),
       (2690, 'Tijuana', '', 2),
       (2691, 'Tila', '', 2),
       (2692, 'Tilapa', '', 2),
       (2693, 'Timilpan', '', 2),
       (2694, 'Timucuy', '', 2),
       (2695, 'Tingambato', '', 2),
       (2696, 'Tinguindín', '', 2),
       (2697, 'Tinúm', '', 2),
       (2698, 'Tiquicheo de Nicolás Romero', '', 2),
       (2699, 'Tixcacalcupul', '', 2),
       (2700, 'Tixkokob', '', 2),
       (2701, 'Tixméhuac', '', 2),
       (2702, 'Tixpéhual', '', 2),
       (2703, 'Tixtla de Guerrero', '', 2),
       (2704, 'Tizapán el Alto', '', 2),
       (2705, 'Tizayuca', '', 2),
       (2706, 'Tizimín', '', 2),
       (2707, 'Tlachichilco', '', 2),
       (2708, 'Tlachichuca', '', 2),
       (2709, 'Tlacoachistlahuaca', '', 2),
       (2710, 'Tlacoapa', '', 2),
       (2711, 'Tlacolulan', '', 2),
       (2712, 'Tlacotalpan', '', 2),
       (2713, 'Tlacotepec de Benito Juárez', '', 2),
       (2714, 'Tlacotepec de Mejía', '', 2),
       (2715, 'Tlacuilotepec', '', 2),
       (2716, 'Tláhuac', '', 2),
       (2717, 'Tlahualilo', '', 2),
       (2718, 'Tlahuapan', '', 2),
       (2719, 'Tlahuelilpan', '', 2),
       (2720, 'Tlahuiltepa', '', 2),
       (2721, 'Tlajocalpan', '', 2),
       (2722, 'Tlajomulco de Zúñiga', '', 2),
       (2723, 'Tlalchapa', '', 2),
       (2724, 'Tlalixcoyan', '', 2),
       (2725, 'Tlalixtaquilla de Maldonado', '', 2),
       (2726, 'Tlalmanalco', '', 2),
       (2727, 'Tlalnelhuayocan', '', 2),
       (2728, 'Tlalnepantla', '', 2),
       (2729, 'Tlalnepantla', '', 2),
       (2730, 'Tlalpan', '', 2),
       (2731, 'Tlalpujahua', '', 2),
       (2732, 'Tlaltenango', '', 2),
       (2733, 'Tlaltenango de Sánchez Román', '', 2),
       (2734, 'Tlaltetela', '', 2),
       (2735, 'Tlaltizapán', '', 2),
       (2736, 'Tlanalapa', '', 2),
       (2737, 'Tlanchinol', '', 2),
       (2738, 'Tlanepantla', '', 2),
       (2739, 'Tlaola', '', 2),
       (2740, 'Tlapa de Comonfort', '', 2),
       (2741, 'Tlapacoya', '', 2),
       (2742, 'Tlapacoyan', '', 2),
       (2743, 'Tlapanalá', '', 2),
       (2744, 'Tlapehuala', '', 2),
       (2745, 'Tlaquepaque', '', 2),
       (2746, 'Tlaquilpan', '', 2),
       (2747, 'Tlaquiltenango', '', 2),
       (2748, 'Tlatlauquitepec', '', 2),
       (2749, 'Tlatlaya', '', 2),
       (2750, 'Tlaxcala', '', 2),
       (2751, 'Tlaxco', '', 2),
       (2752, 'Tlaxco', '', 2),
       (2753, 'Tlaxcoapan', '', 2),
       (2754, 'Tlayacapan', '', 2),
       (2755, 'Tlazazalca', '', 2),
       (2756, 'Tlilapan', '', 2),
       (2757, 'Tocatlán', '', 2),
       (2758, 'Tochimilco', '', 2),
       (2759, 'Tochtepec', '', 2),
       (2760, 'Tocumbo', '', 2),
       (2761, 'Tolcayuca', '', 2),
       (2762, 'Tolimán', '', 2),
       (2763, 'Tolimán', '', 2),
       (2764, 'Toluca', '', 2),
       (2765, 'Tomatlán', '', 2),
       (2766, 'Tomatlán', '', 2),
       (2767, 'Tonalá', '', 2),
       (2768, 'Tonalá', '', 2),
       (2769, 'Tonatico', '', 2),
       (2770, 'Tonaya', '', 2),
       (2771, 'Tonayán', '', 2),
       (2772, 'Tonila', '', 2),
       (2773, 'Topia', '', 2),
       (2774, 'Torreón', '', 2),
       (2775, 'Totatiche', '', 2),
       (2776, 'Totolac', '', 2),
       (2777, 'Totolapa', '', 2),
       (2778, 'Totolapan', '', 2),
       (2779, 'Totoltepec de Guerrero', '', 2),
       (2780, 'Tototlán', '', 2),
       (2781, 'Totutla', '', 2),
       (2782, 'Trincheras', '', 2),
       (2783, 'Trinidad García de la Cadena', '', 2),
       (2784, 'Tubutama', '', 2),
       (2785, 'Tula', '', 2),
       (2786, 'Tula de Allende', '', 2),
       (2787, 'Tulancingo de Bravo', '', 2),
       (2788, 'Tulcingo', '', 2),
       (2789, 'Tultepec', '', 2),
       (2790, 'Tultitlán', '', 2),
       (2791, 'Tumbalá', '', 2),
       (2792, 'Tumbiscatío', '', 2),
       (2793, 'Tunkás', '', 2),
       (2794, 'Turicato', '', 2),
       (2795, 'Tuxcacuesco', '', 2),
       (2796, 'Tuxcueca', '', 2),
       (2797, 'Tuxpan', '', 2),
       (2798, 'Tuxpan', '', 2),
       (2799, 'Tuxpan', '', 2),
       (2800, 'Tuxpan', '', 2),
       (2801, 'Tuxtilla', '', 2),
       (2802, 'Tuxtla Chico', '', 2),
       (2803, 'Tuxtla Gutiérrez', '', 2),
       (2804, 'Tuzantán', '', 2),
       (2805, 'Tuzantla', '', 2),
       (2806, 'Tzicatlacoyan', '', 2),
       (2807, 'Tzimol', '', 2),
       (2808, 'Tzintzuntzan', '', 2),
       (2809, 'Tzitzio', '', 2),
       (2810, 'Tzompantepec', '', 2),
       (2811, 'Tzucacab', '', 2),
       (2812, 'Uayma', '', 2),
       (2813, 'Ucú', '', 2),
       (2814, 'Umán', '', 2),
       (2815, 'Unión de San Antonio', '', 2),
       (2816, 'Unión de Tula', '', 2),
       (2817, 'Ures', '', 2),
       (2818, 'Uriangato', '', 2),
       (2819, 'Urique', '', 2),
       (2820, 'Úrsulo Galván', '', 2),
       (2821, 'Uruachic', '', 2),
       (2822, 'Uruapan', '', 2),
       (2823, 'Valladolid', '', 2),
       (2824, 'Valle de Bravo', '', 2),
       (2825, 'Valle de Guadalupe', '', 2),
       (2826, 'Valle de Juárez', '', 2),
       (2827, 'Valle de Santiago', '', 2),
       (2828, 'Valle de Zaragoza', '', 2),
       (2829, 'Valle Hermoso', '', 2),
       (2830, 'Vallecillo', '', 2),
       (2831, 'Valparaíso', '', 2),
       (2832, 'Vanegas', '', 2),
       (2833, 'Vega de Alatorre', '', 2),
       (2834, 'Venado', '', 2),
       (2835, 'Venustiano Carranza', '', 2),
       (2836, 'Venustiano Carranza', '', 2),
       (2837, 'Venustiano Carranza', '', 2),
       (2838, 'Venustiano Carranza', '', 2),
       (2839, 'Veracruz', '', 2),
       (2840, 'Vetagrande', '', 2),
       (2841, 'Vicente Guerrero', '', 2),
       (2842, 'Vicente Guerrero', '', 2),
       (2843, 'Victoria', '', 2),
       (2844, 'Victoria', '', 2),
       (2845, 'Viesca', '', 2),
       (2846, 'Villa Aldama', '', 2),
       (2847, 'Villa Comaltitlán', '', 2),
       (2848, 'Villa Corona', '', 2),
       (2849, 'Villa Corzo', '', 2),
       (2850, 'Villa de Allende', '', 2),
       (2851, 'Villa de Alvarez', '', 2),
       (2852, 'Villa de Arista', '', 2),
       (2853, 'Villa de Arriaga', '', 2),
       (2854, 'Villa de Cos', '', 2),
       (2855, 'Villa de Guadalupe', '', 2),
       (2856, 'Villa de la Paz', '', 2),
       (2857, 'Villa de Ramos', '', 2),
       (2858, 'Villa de Reyes', '', 2),
       (2859, 'Villa de Tezontepec', '', 2),
       (2860, 'Villa del Carbón', '', 2),
       (2861, 'Villa García', '', 2),
       (2862, 'Villa González Ortega', '', 2),
       (2863, 'Villa Guerrero', '', 2),
       (2864, 'Villa Guerrero', '', 2),
       (2865, 'Villa Hidalgo', '', 2),
       (2866, 'Villa Hidalgo', '', 2),
       (2867, 'Villa Hidalgo', '', 2),
       (2868, 'Villa Hidalgo', '', 2),
       (2869, 'Villa Juárez', '', 2),
       (2870, 'Villa Pesqueira', '', 2),
       (2871, 'Villa Purificación', '', 2),
       (2872, 'Villa Unión', '', 2),
       (2873, 'Villa Victoria', '', 2),
       (2874, 'Villaflores', '', 2),
       (2875, 'Villagrán', '', 2),
       (2876, 'Villagrán', '', 2),
       (2877, 'Villaldama', '', 2),
       (2878, 'Villamar', '', 2),
       (2879, 'Villanueva', '', 2),
       (2880, 'Vista Hermosa', '', 2),
       (2881, 'Xalapa', '', 2),
       (2882, 'Xalisco', '', 2),
       (2883, 'Xaloztoc', '', 2),
       (2884, 'Xalpan', '', 2),
       (2885, 'Xalpatláhuac', '', 2),
       (2886, 'Xaltocan', '', 2),
       (2887, 'Xayacatlán de Bravo', '', 2),
       (2888, 'Xichú', '', 2),
       (2889, 'Xico', '', 2),
       (2890, 'Xicohtzinco', '', 2),
       (2891, 'Xicoténcatl', '', 2),
       (2892, 'Xicotepec', '', 2),
       (2893, 'Xicotlán', '', 2),
       (2894, 'Xilitla', '', 2),
       (2895, 'Xiutetelco', '', 2),
       (2896, 'Xocchel', '', 2),
       (2897, 'Xochiapulco', '', 2),
       (2898, 'Xochiatipan', '', 2),
       (2899, 'Xochicoatlán', '', 2),
       (2900, 'Xochihuehuetlán', '', 2),
       (2901, 'Xochiltepec', '', 2),
       (2902, 'Xochimilco', '', 2),
       (2903, 'Xochistlahuaca', '', 2),
       (2904, 'Xochitepec', '', 2),
       (2905, 'Xochitlán de Vicente Suárez', '', 2),
       (2906, 'Xochitlán Todos Santos', '', 2),
       (2907, 'Xonacatlán', '', 2),
       (2908, 'Xoxocotla', '', 2),
       (2909, 'Yahualica', '', 2),
       (2910, 'Yahualica de González Gallo', '', 2),
       (2911, 'Yajalón', '', 2),
       (2912, 'Yanga', '', 2),
       (2913, 'Yaonáhuac', '', 2),
       (2914, 'Yauhquemecan', '', 2),
       (2915, 'Yautepec', '', 2),
       (2916, 'Yaxcabá', '', 2),
       (2917, 'Yaxkukul', '', 2),
       (2918, 'Yecapixtla', '', 2),
       (2919, 'Yécora', '', 2),
       (2920, 'Yecuatla', '', 2),
       (2921, 'Yehualtepec', '', 2),
       (2922, 'Yobaín', '', 2),
       (2923, 'Yurécuaro', '', 2),
       (2924, 'Yuriria', '', 2),
       (2925, 'Zacapala', '', 2),
       (2926, 'Zacapoaxtla', '', 2),
       (2927, 'Zacapu', '', 2),
       (2928, 'Zacatecas', '', 2),
       (2929, 'Zacatelco', '', 2),
       (2930, 'Zacatepec de Hidalgo', '', 2),
       (2931, 'Zacatlán', '', 2),
       (2932, 'Zacazonapan', '', 2),
       (2933, 'Zacoalco de Torres', '', 2),
       (2934, 'Zacualpan', '', 2),
       (2935, 'Zacualpan', '', 2),
       (2936, 'Zacualpan', '', 2),
       (2937, 'Zacualtipán de Angeles', '', 2),
       (2938, 'Zamora', '', 2),
       (2939, 'Zapopan', '', 2),
       (2940, 'Zapotiltic', '', 2),
       (2941, 'Zapotitlán', '', 2),
       (2942, 'Zapotitlán de Méndez', '', 2),
       (2943, 'Zapotitlán de Vadillo', '', 2),
       (2944, 'Zapotitlán Tablas', '', 2),
       (2945, 'Zapotlán de Juárez', '', 2),
       (2946, 'Zapotlán del Rey', '', 2),
       (2947, 'Zapotlanejo', '', 2),
       (2948, 'Zaragoza', '', 2),
       (2949, 'Zaragoza', '', 2),
       (2950, 'Zaragoza', '', 2),
       (2951, 'Zaragoza', '', 2),
       (2952, 'Zautla', '', 2),
       (2953, 'Zempoala', '', 2),
       (2954, 'Zentla', '', 2),
       (2955, 'Zihuateutla', '', 2),
       (2956, 'Zimapán', '', 2),
       (2957, 'Zinacantán', '', 2),
       (2958, 'Zinacatepec', '', 2),
       (2959, 'Zinacatepec', '', 2),
       (2960, 'Zináparo', '', 2),
       (2961, 'Zinapécuaro', '', 2),
       (2962, 'Ziracuaretiro', '', 2),
       (2963, 'Zirándaro', '', 2),
       (2964, 'Zirándaro', '', 2),
       (2965, 'Zitácuaro', '', 2),
       (2966, 'Zitlala', '', 2),
       (2967, 'Zitlaltepec de Trinidad Sánchez Santos', '', 2),
       (2968, 'Zongolica', '', 2),
       (2969, 'Zontecomatlán', '', 2),
       (2970, 'Zoquiapan', '', 2),
       (2971, 'Zoquitlan', '', 2),
       (2972, 'Zozocolco de Hidalgo', '', 2),
       (2973, 'Zumpahuacan', '', 2),
       (2974, 'Zumpango', '', 2);



ALTER TABLE `extended_user`
    ADD COLUMN `residence_country_id` BIGINT(20) NULL DEFAULT NULL;
CREATE INDEX `fk_extended_user_residence_country_id` ON `extended_user` (residence_country_id);
alter table extended_user
    add constraint fk_extended_user_residence_country_id foreign key (residence_country_id) references country (id);

ALTER TABLE `extended_user`
    ADD COLUMN `postal_country_id` BIGINT(20) NULL DEFAULT NULL;
CREATE INDEX `fk_extended_user_postal_country_id` ON `extended_user` (postal_country_id);
alter table extended_user
    add constraint fk_extended_user_postal_country_id foreign key (postal_country_id) references country (id);


alter table notification
    add notes longtext null;

alter table extended_user
    add bank_commission float;
alter table extended_user
    add fx_commission float;
alter table extended_user
    add recharge_cost float;

alter table extended_user
    add use_merchant_commission boolean default false null;



alter table extended_user
    add bank_account_number varchar(255) null;


ALTER TABLE `extended_user`
    ADD COLUMN `direct_payment_bank_id` BIGINT(20) NULL DEFAULT NULL;
CREATE INDEX `fk_extended_user_direct_payment_bank_id` ON `extended_user` (direct_payment_bank_id);
ALTER TABLE extended_user
    add constraint fk_extended_user_direct_payment_bank_id foreign key (direct_payment_bank_id) references direct_payment_bank (id);
alter table extended_user
    drop foreign key fk_extended_user_direct_payment_bank_id;
ALTER TABLE extended_user
    add constraint fk_extended_user_direct_payment_bank_id foreign key (direct_payment_bank_id) references bank (id);


ALTER TABLE `extended_user`
    ADD COLUMN `direct_payment_city_id` BIGINT(20) NULL DEFAULT NULL;
CREATE INDEX `fk_extended_user_direct_payment_city_id` ON `extended_user` (direct_payment_city_id);
ALTER TABLE extended_user
    add constraint fk_extended_user_direct_payment_city_id foreign key (direct_payment_city_id) references city (id);


#####
alter table notification_receiver
    add must_send_email boolean default false null;

alter table notification_receiver
    add email_template varchar(255) null;

ALTER TABLE `notification_receiver`
    ADD COLUMN `sender_id` BIGINT(20) NULL DEFAULT NULL;
CREATE INDEX `fk_notification_receiver_sender_id` ON `notification_receiver` (sender_id);
ALTER TABLE `notification_receiver`
    add constraint `fk_notification_receiver_sender_id` foreign key (sender_id) references `extended_user` (id);

alter table notification_receiver
    add email_sent boolean default false null;

alter table extended_user
    add use_direct_payment boolean default false null;


alter table transaction_group
    add direct_payment boolean default false null;

alter table extended_user
    add bank_account_type varchar(255) null;

alter table direct_payment_bank
    add destiny_bank_code varchar(255) null;


alter table extended_user
    add can_change_payment_method boolean default true null;


alter table direct_payment_bank
    add id_type_name varchar(255) null;

alter table direct_payment_bank
    add id_type_code varchar(255) null;


ALTER TABLE `direct_payment_bank`
    ADD COLUMN `id_type_id` BIGINT(20) NULL DEFAULT NULL;
CREATE INDEX `fk_direct_payment_bank_id_type_id` ON `direct_payment_bank` (id_type_id);
ALTER TABLE `direct_payment_bank`
    add constraint `fk_direct_payment_bank_id_type_id` foreign key (id_type_id) references `id_type` (id);


alter table notification
    add sub_category varchar(255) null;


############


ALTER TABLE `notification_receiver`
    ADD COLUMN `user_receiver_id` BIGINT(20) NULL DEFAULT NULL;
CREATE INDEX `fk_notification_receiver_user_receiver_id` ON `notification_receiver` (user_receiver_id);
ALTER TABLE `notification_receiver`
    add constraint `fk_notification_receiver_user_id` foreign key (user_receiver_id) references `jhi_user` (id);


update extended_user
set id_type_id = 1
where id_type_id NOT IN (1, 15);


alter table notification_receiver
    add debug_reason varchar(255) null;


alter table notification_receiver
    add email_title_key varchar(255) null;

alter table extended_user
    add must_notify boolean default true null;


alter table extended_user
    add profile_info_changed BLOB;


alter table transaction
    add info_changed BLOB;

alter table transaction
    add must_notify boolean default true null;

#--------------- NO ejecutar
ALTER TABLE `notification_receiver`
    ADD COLUMN `extended_user_id` BIGINT(20) NULL DEFAULT NULL;
CREATE INDEX `fk_notification_receiver_extended_user_id` ON `notification_receiver` (extended_user_id);
ALTER TABLE `notification_receiver`
    add constraint `fk_notification_receiver_extended_user_id` foreign key (extended_user_id) references `extended_user` (id);

alter table notification_receiver
    add receiver_group varchar(255) null;
#---------------

ALTER TABLE `notification`
    ADD COLUMN `extended_user_id` BIGINT(20) NULL DEFAULT NULL;
CREATE INDEX `fk_notification_extended_user_id` ON `notification` (extended_user_id);
ALTER TABLE `notification`
    add constraint `fk_notification_extended_user_id` foreign key (extended_user_id) references `extended_user` (id);

alter table notification
    add receiver_group varchar(255) null;


alter table notification_receiver
    drop foreign key fk_notification_receiver_extended_user_id;
drop index fk_notification_receiver_extended_user_id on notification_receiver;
alter table notification_receiver
    drop column extended_user_id;
alter table notification_receiver
    drop column receiver_group;


#--------------------------

alter table country
    add currency_name varchar(255) null;

alter table country
    add currency_code varchar(255) null;

alter table country
    add currency_value float null;

UPDATE country t
SET t.currency_name = 'Colombian Peso',
    t.currency_code = 'COP'
WHERE t.id = 1;

UPDATE country t
SET t.currency_name = 'Mexican Peso',
    t.currency_code = 'MXN'
WHERE t.id = 2;

alter table country
    add currency_base varchar(255) null;

alter table country
    add currency_last_updated datetime null;


#------------

alter table direct_payment_bank
    add city_code varchar(255) null;

alter table direct_payment_bank
    add department_code varchar(255) null;

## IMPORTANTE!!!!
## actualizar bankAccount con el número de cuenta de Alex para ITAU - Colombia -> 007468655
## IMPORTANTE!!!!


#--------------

alter table extended_user_funds
    add status varchar(255) null;

alter table extended_user_funds
    add category varchar(255) null;

alter table extended_user_funds
    add last_updated datetime null;


#--- Payee as company - 2020.05.06
alter table jhi_user
    add payee_as_company boolean default false null;

INSERT INTO pagomundo.id_type (name, code, country_id)
VALUES ('NIT', '03', 1);

## -- La tabla id_type debería tener los siguientes datos:
#   id,name,code,country_id
#   1,CEDULA DE CIUDADANIA,01,1
#   15,RFC,,2
#   16,NIT,03,1

UPDATE pagomundo.direct_payment_bank t
SET t.id_type_name = 'NIT',
    t.id_type_code = '03',
    t.id_type_id   = 16 # este es el 16 del NIT de la tabla anterior
WHERE t.id = 2;


#-- IMPORTANT: Hay que recrear el ES si fuera necesario


#---------------------------
# --> RESELLER <--
#---------------------------

INSERT INTO `jhi_authority` (name)
VALUES ('ROLE_RESELLER');

alter table extended_user_relation
    add status varchar(255) null;
alter table extended_user_relation
    add status_reason longtext null;

alter table `extended_user_relation`
    add `date_related` datetime null;
ALTER TABLE `extended_user_relation`
    ADD COLUMN `related_by_id` BIGINT(20) NULL DEFAULT NULL;
CREATE INDEX `fk_extended_user_relation_related_by_id` ON `extended_user_relation` (related_by_id);
ALTER TABLE `extended_user_relation`
    add constraint `fk_extended_user_relation_related_by_id` foreign key (related_by_id) references `extended_user` (id);

alter table `extended_user_relation`
    add `date_unrelated` datetime null;
ALTER TABLE `extended_user_relation`
    ADD COLUMN `unrelated_by_id` BIGINT(20) NULL DEFAULT NULL;
CREATE INDEX `fk_extended_user_relation_unrelated_by_id` ON `extended_user_relation` (unrelated_by_id);
ALTER TABLE `extended_user_relation`
    add constraint `fk_extended_user_relation_unrelated_by_id` foreign key (unrelated_by_id) references `extended_user` (id);

UPDATE `extended_user_relation` t
SET t.status        = 'ACCEPTED',
    t.status_reason = null,
    t.related_by_id = t.extended_user_id
WHERE 1;

## ---------- IMPORTANTE -----------#
##
## Asegurarse de que en cualquier entorno (TEST & PROD) haya un superadmin con cuenta de correo support@pmi-americas.com
## para que funcione que ciertas acciones / notificaciones envíen mails a esa cuenta
##
## ---------- IMPORTANTE -----------#


## Nuevo tipo de documento para taxId
ALTER TABLE `extended_user`
    ADD COLUMN `id_type_tax_id_id` BIGINT(20) NULL DEFAULT NULL;
CREATE INDEX `fk_extended_user_id_type_tax_id_id` ON `extended_user` (id_type_tax_id_id);
ALTER TABLE `extended_user`
    add constraint `fk_extended_user_id_type_tax_id_id` foreign key (id_type_tax_id_id) references `id_type` (id);

# ejecutar si se crearon mal los campos e íncidee anteriores
alter table `extended_user`
    drop foreign key `fk_extended_user_id_type_tax_id`;
drop index `fk_extended_user_id_type_tax_id` on `extended_user`;
alter table `extended_user`
    drop column `id_type_tax_id`;


# Definición si el tipo de documento es para persona física o jurídica o ambos
alter table id_type
    add natural_or_legal varchar(255) null;

UPDATE pagomundo.id_type t
SET t.natural_or_legal = 'BOTH'
WHERE t.id = 15;
UPDATE pagomundo.id_type t
SET t.natural_or_legal = 'LEGAL'
WHERE t.id = 16;
UPDATE pagomundo.id_type t
SET t.natural_or_legal = 'NATURAL'
WHERE t.id = 1;


update `pagomundo`.`notification` n
set n.`sub_category` = 'ISSUE_CREATED'
where n.`category` = 'ISSUE'
  and n.`status` = 'CREATED';

update `pagomundo`.`notification` n
set n.`sub_category` = 'ISSUE_IN_PROCESS'
where n.`category` = 'ISSUE'
  and n.`status` = 'IN_PROCESS';

update `pagomundo`.`notification` n
set n.`sub_category` = 'ISSUE_ACCEPTED'
where n.`category` = 'ISSUE'
  and n.`status` = 'ACCEPTED';

update `pagomundo`.`notification` n
set n.`sub_category` = 'ISSUE_REJECTED'
where n.`category` = 'ISSUE'
  and n.`status` = 'REJECTED';

update `pagomundo`.`notification` n
set n.`sub_category` = 'ISSUE_CANCELLED'
where n.`category` = 'ISSUE'
  and n.`status` = 'CANCELLED';


# payments related to a reseller

ALTER TABLE `transaction`
    ADD COLUMN `reseller_id` BIGINT(20) NULL DEFAULT NULL;

create index `fk_transaction_reseller_id`
    on `transaction` (reseller_id);

alter table `transaction`
    add constraint `fk_transaction_reseller_id`
        foreign key (reseller_id) references `extended_user` (id);

# END payments related to a reseller


# user confirms his profile
alter table `extended_user`
    add `confirmed_profile` boolean default false null;

UPDATE `extended_user`
set `confirmed_profile` = true
where 1;
# END user confirms his profile

# user confirms his profile
alter table `extended_user_funds`
    add `type` varchar(255) null;

UPDATE `extended_user_funds`
set `type` = 'FUNDING'
where 1;

# END user confirms his profile

# Reseller commission
alter table `extended_user`
    add `fixed_commission` boolean default false null;

alter table `extended_user`
    add `reseller_commission` float;

alter table `transaction`
    add `reseller_commission` float;
# END Reseller commission


## Reseller attribute for merchants (ExtendedUser)
ALTER TABLE `extended_user`
    ADD COLUMN `reseller_id` BIGINT(20) NULL DEFAULT NULL;
CREATE INDEX `fk_extended_user_reseller_id` ON `extended_user` (reseller_id);
ALTER TABLE `extended_user`
    add constraint `fk_extended_user_reseller_id` foreign key (reseller_id) references `extended_user` (id);
## END Reseller attribute for merchants (ExtendedUser)


ALTER TABLE `jhi_user`
    ADD COLUMN `role` varchar(255) null;


#--- RESELLER new COMMISSION
alter table `extended_user`
    drop column `fixed_commission`;

alter table `extended_user`
    drop column `reseller_commission`;

alter table `extended_user`
    add `reseller_fixed_commission` float;
alter table `extended_user`
    add `reseller_percentage_commission` float;

alter table `transaction`
    add `reseller_fixed_commission` float;
alter table `transaction`
    add `reseller_percentage_commission` float;


#--- RESELLER new COMMISSION

Update extended_user
set reseller_fixed_commission = 0
where role = 'ROLE_RESELLER'
  AND reseller_fixed_commission IS NULL;

Update extended_user
set reseller_percentage_commission = 0
where role = 'ROLE_RESELLER'
  AND reseller_percentage_commission IS NULL;

Update extended_user
set use_merchant_commission = 0,
    reseller_id             = NULL
where role = 'ROLE_RESELLER';


#--- MultiMessageNotification

alter table `notification`
    drop foreign key `fk_notification_receiver_id`;
drop index `fk_notification_receiver_id` on `notification`;
alter table `notification`
    drop column `receiver_id`;

alter table `notification`
    modify `subject` varchar(255) null;

ALTER TABLE `notification_receiver`
    ADD COLUMN `subject` varchar(255) null;
ALTER TABLE `notification_receiver`
    ADD COLUMN `description` longtext null;

alter table `notification`
    ADD COLUMN `entity_id` bigint(20) null;


UPDATE `notification_receiver` nr
    INNER JOIN `notification` n ON (n.`id` = nr.`notification_id`)
SET nr.`subject`     = n.`subject`,
    nr.`description` = n.`description`
WHERE 1;

#--- MultiMessageNotification

#--- Process type
alter table `transaction`
    add `process_type` varchar(255) null;

update `transaction` t
set t.process_type = 'FILE_EXPORT'
where 1;

#--- Process type

#---------- Special characters Bug Desarrollo #27157
ALTER TABLE notification_receiver
    CONVERT TO CHARACTER SET UTF8MB4;
#----------

#--- Change email
alter table jhi_user
    ADD COLUMN `new_email` varchar(255) null;
#--- Change email

#--- Reference data for ARG & BRA
INSERT INTO `pagomundo`.`currency` (`name`, `code`, `country_id`)
VALUES ('Argentine peso', 'ARS', 66);

INSERT INTO `pagomundo`.`currency` (`name`, `code`, `country_id`)
VALUES ('Brazilian real', 'BRL', 74);

UPDATE `pagomundo`.`country` t
SET t.`for_payee`     = 1,
    t.`currency_name` = 'Argentine peso',
    t.`currency_code` = 'ARS'
WHERE t.`id` = 66;

UPDATE `pagomundo`.`country` t
SET t.`for_payee`     = 1,
    t.`currency_name` = 'Brazilian real',
    t.`currency_code` = 'BRL'
WHERE t.`id` = 74;

# To Update currencies exchangeRate execute the following request
# PUT to /api/countries/currencies

# ARG cities
INSERT INTO `city`
VALUES (2977, 'Avellaneda [Buenos Aires]', '', 66),
       (2978, 'Azul [Buenos Aires]', '', 66),
       (2979, 'Bahía Blanca [Buenos Aires]', '', 66),
       (2980, 'Banfield [Buenos Aires]', '', 66),
       (2981, 'Béccar [Buenos Aires]', '', 66),
       (2982, 'Belén de Escobar [Buenos Aires]', '', 66),
       (2983, 'Bella Vista [Buenos Aires]', '', 66),
       (2984, 'Berazategui [Buenos Aires]', '', 66),
       (2985, 'Berisso [Buenos Aires]', '', 66),
       (2986, 'Bernal [Buenos Aires]', '', 66),
       (2987, 'Bosques [Buenos Aires]', '', 66),
       (2988, 'Boulogne Sur Mer [Buenos Aires]', '', 66),
       (2989, 'Buenos Aires [Buenos Aires]', '', 66),
       (2990, 'Burzaco [Buenos Aires]', '', 66),
       (2991, 'Campana [Buenos Aires]', '', 66),
       (2992, 'Caseros [Buenos Aires]', '', 66),
       (2993, 'Castelar [Buenos Aires]', '', 66),
       (2994, 'Chivilcoy [Buenos Aires]', '', 66),
       (2995, 'Ciudad Evita [Buenos Aires]', '', 66),
       (2996, 'Ciudad Jardín El Libertador [Buenos Aires]', '', 66),
       (2997, 'Ciudadela [Buenos Aires]', '', 66),
       (2998, 'Don Torcuato [Buenos Aires]', '', 66),
       (2999, 'El Jagüel [Buenos Aires]', '', 66),
       (3000, 'El Palomar [Buenos Aires]', '', 66),
       (3001, 'Ezeiza [Buenos Aires]', '', 66),
       (3002, 'Ezpeleta [Buenos Aires]', '', 66),
       (3003, 'Florencio Varela [Buenos Aires]', '', 66),
       (3004, 'Florida [Buenos Aires]', '', 66),
       (3005, 'General Rodríguez [Buenos Aires]', '', 66),
       (3006, 'Glew [Buenos Aires]', '', 66),
       (3007, 'Gobernador Julio A Costa [Buenos Aires]', '', 66),
       (3008, 'González Catán [Buenos Aires]', '', 66),
       (3009, 'Grand Bourg [Buenos Aires]', '', 66),
       (3010, 'Gregorio de Laferrere [Buenos Aires]', '', 66),
       (3011, 'Isidro Casanova [Buenos Aires]', '', 66),
       (3012, 'Ituzaingó [Buenos Aires]', '', 66),
       (3013, 'José C. Paz [Buenos Aires]', '', 66),
       (3014, 'Junín [Buenos Aires]', '', 66),
       (3015, 'La Plata [Buenos Aires]', '', 66),
       (3016, 'La Tablada [Buenos Aires]', '', 66),
       (3017, 'Lanús [Buenos Aires]', '', 66),
       (3018, 'Libertad [Buenos Aires]', '', 66),
       (3019, 'Lomas de Zamora [Buenos Aires]', '', 66),
       (3020, 'Lomas del Mirador [Buenos Aires]', '', 66),
       (3021, 'Longchamps [Buenos Aires]', '', 66),
       (3022, 'Los Polvorines [Buenos Aires]', '', 66),
       (3023, 'Luján [Buenos Aires]', '', 66),
       (3024, 'Mar del Plata [Buenos Aires]', '', 66),
       (3025, 'Mariano Acosta [Buenos Aires]', '', 66),
       (3026, 'Martínez [Buenos Aires]', '', 66),
       (3027, 'Mercedes [Buenos Aires]', '', 66),
       (3028, 'Merlo [Buenos Aires]', '', 66),
       (3029, 'Monte Chingolo [Buenos Aires]', '', 66),
       (3030, 'Monte Grande [Buenos Aires]', '', 66),
       (3031, 'Moreno [Buenos Aires]', '', 66),
       (3032, 'Morón [Buenos Aires]', '', 66),
       (3033, 'Necochea [Buenos Aires]', '', 66),
       (3034, 'Olavarría [Buenos Aires]', '', 66),
       (3035, 'Olivos [Buenos Aires]', '', 66),
       (3036, 'Pergamino [Buenos Aires]', '', 66),
       (3037, 'Pilar [Buenos Aires]', '', 66),
       (3038, 'Presidente Perón [Buenos Aires]', '', 66),
       (3039, 'Punta Alta [Buenos Aires]', '', 66),
       (3040, 'Quilmes [Buenos Aires]', '', 66),
       (3041, 'Rafael Calzada [Buenos Aires]', '', 66),
       (3042, 'Rafael Castillo [Buenos Aires]', '', 66),
       (3043, 'Ramos Mejía [Buenos Aires]', '', 66),
       (3044, 'Remedios de Escalada [Buenos Aires]', '', 66),
       (3045, 'San Fernando [Buenos Aires]', '', 66),
       (3046, 'San Francisco Solano [Buenos Aires]', '', 66),
       (3047, 'San Isidro [Buenos Aires]', '', 66),
       (3048, 'San Justo [Buenos Aires]', '', 66),
       (3049, 'San Miguel [Buenos Aires]', '', 66),
       (3050, 'San Nicolás de los Arroyos [Buenos Aires]', '', 66),
       (3051, 'Sarandí [Buenos Aires]', '', 66),
       (3052, 'Tandil [Buenos Aires]', '', 66),
       (3053, 'Temperley [Buenos Aires]', '', 66),
       (3054, 'Tres Arroyos [Buenos Aires]', '', 66),
       (3055, 'Trujui [Buenos Aires]', '', 66),
       (3056, 'Vicente López [Buenos Aires]', '', 66),
       (3057, 'Villa Centenario [Buenos Aires]', '', 66),
       (3058, 'Villa Domínico [Buenos Aires]', '', 66),
       (3059, 'Villa Luzuriaga [Buenos Aires]', '', 66),
       (3060, 'Villa Madero [Buenos Aires]', '', 66),
       (3061, 'Villa Tesei [Buenos Aires]', '', 66),
       (3062, 'Virrey del Pino [Buenos Aires]', '', 66),
       (3063, 'Wilde [Buenos Aires]', '', 66),
       (3064, 'William Morris [Buenos Aires]', '', 66),
       (3065, 'Zárate [Buenos Aires]', '', 66),
       (3066, 'San Fernando del Valle de Catamarca [Catamarca]', '', 66),
       (3067, 'Barranqueras [Chaco]', '', 66),
       (3068, 'Presidencia Roque Sáenz Peña [Chaco]', '', 66),
       (3069, 'Resistencia [Chaco]', '', 66),
       (3070, 'Comodoro Rivadavia [Chubut]', '', 66),
       (3071, 'Esquel [Chubut]', '', 66),
       (3072, 'Puerto Madryn [Chubut]', '', 66),
       (3073, 'Rawson [Chubut]', '', 66),
       (3074, 'Trelew [Chubut]', '', 66),
       (3075, 'Córdoba [Córdoba]', '', 66),
       (3076, 'Río Cuarto [Córdoba]', '', 66),
       (3077, 'San Francisco [Córdoba]', '', 66),
       (3078, 'Villa Carlos Paz [Córdoba]', '', 66),
       (3079, 'Villa Dolores [Córdoba]', '', 66),
       (3080, 'Villa María [Córdoba]', '', 66),
       (3081, 'Corrientes [Corrientes]', '', 66),
       (3082, 'Goya [Corrientes]', '', 66),
       (3083, 'Concepción del Uruguay [Entre Ríos]', '', 66),
       (3084, 'Concordia [Entre Ríos]', '', 66),
       (3085, 'Gualeguaychú [Entre Ríos]', '', 66),
       (3086, 'Paraná [Entre Ríos]', '', 66),
       (3087, 'Clorinda [Formosa]', '', 66),
       (3088, 'Formosa [Formosa]', '', 66),
       (3089, 'Palpalá [Jujuy]', '', 66),
       (3090, 'San Pedro de Jujuy [Jujuy]', '', 66),
       (3091, 'San Salvador de Jujuy [Jujuy]', '', 66),
       (3092, 'General Pico [La Pampa]', '', 66),
       (3093, 'Santa Rosa [La Pampa]', '', 66),
       (3094, 'La Rioja [La Rioja]', '', 66),
       (3095, 'Godoy Cruz [Mendoza]', '', 66),
       (3096, 'Guaymallén [Mendoza]', '', 66),
       (3097, 'Las Heras [Mendoza]', '', 66),
       (3098, 'Luján de Cuyo [Mendoza]', '', 66),
       (3099, 'Maipú [Mendoza]', '', 66),
       (3100, 'Mendoza [Mendoza]', '', 66),
       (3101, 'San Martín [Mendoza]', '', 66),
       (3102, 'San Rafael [Mendoza]', '', 66),
       (3103, 'Eldorado [Misiones]', '', 66),
       (3104, 'Oberá [Misiones]', '', 66),
       (3105, 'Posadas [Misiones]', '', 66),
       (3106, 'Neuquén [Neuquén]', '', 66),
       (3107, 'Cipolletti [Río Negro]', '', 66),
       (3108, 'General Roca [Río Negro]', '', 66),
       (3109, 'San Carlos de Bariloche [Río Negro]', '', 66),
       (3110, 'Viedma [Río Negro]', '', 66),
       (3111, 'Salta [Salta]', '', 66),
       (3112, 'San Ramón de la Nueva Orán [Salta]', '', 66),
       (3113, 'Tartagal [Salta]', '', 66),
       (3114, 'Chimbas [San Juan]', '', 66),
       (3115, 'Rivadavia [San Juan]', '', 66),
       (3116, 'San Juan [San Juan]', '', 66),
       (3117, 'San Luis [San Luis]', '', 66),
       (3118, 'Villa Mercedes [San Luis]', '', 66),
       (3119, 'Río Gallegos [Santa Cruz]', '', 66),
       (3120, 'Esperanza [Santa Fe]', '', 66),
       (3121, 'Rafaela [Santa Fe]', '', 66),
       (3122, 'Reconquista [Santa Fe]', '', 66),
       (3123, 'Rosario [Santa Fe]', '', 66),
       (3124, 'Santa Fe [Santa Fe]', '', 66),
       (3125, 'Venado Tuerto [Santa Fe]', '', 66),
       (3126, 'Villa Gobernador Gálvez [Santa Fe]', '', 66),
       (3127, 'La Banda [Santiago del Estero]', '', 66),
       (3128, 'Santiago del Estero [Santiago del Estero]', '', 66),
       (3129, 'Río Grande [Tierra del Fuego]', '', 66),
       (3130, 'Ushuaia [Tierra del Fuego]', '', 66),
       (3131, 'Banda del Río Salí [Tucumán]', '', 66),
       (3132, 'Concepción [Tucumán]', '', 66),
       (3133, 'San Miguel de Tucumán [Tucumán]', '', 66),
       (3134, 'Villa Mariano Moreno-El Colmenar [Tucumán]', '', 66),
       (3135, 'Yerba Buena-Marcos Paz [Tucumán]', '', 66);

# BRA cities
INSERT INTO `city`
VALUES (3136, 'Rio Branco [Acre]', '', 74),
       (3137, 'Arapiraca [Alagoas]', '', 74),
       (3138, 'Maceió [Alagoas]', '', 74),
       (3139, 'Laranjal do Jari [Amapá]', '', 74),
       (3140, 'Macapá [Amapá]', '', 74),
       (3141, 'Santana [Amapá]', '', 74),
       (3142, 'Coari [Amazonas]', '', 74),
       (3143, 'Humaitá [Amazonas]', '', 74),
       (3144, 'Itacoatiara [Amazonas]', '', 74),
       (3145, 'Manacapuru [Amazonas]', '', 74),
       (3146, 'Manaos [Amazonas]', '', 74),
       (3147, 'Maués [Amazonas]', '', 74),
       (3148, 'Parintins [Amazonas]', '', 74),
       (3149, 'Tabatinga [Amazonas]', '', 74),
       (3150, 'Tefé [Amazonas]', '', 74),
       (3151, 'Feira de Santana [Bahía]', '', 74),
       (3152, 'Ilhéus [Bahía]', '', 74),
       (3153, 'Itabuna [Bahía]', '', 74),
       (3154, 'Juazeiro [Bahía]', '', 74),
       (3155, 'Lauro de Freitas [Bahía]', '', 74),
       (3156, 'Salvador de Bahía [Bahía]', '', 74),
       (3157, 'Vitória da Conquista [Bahía]', '', 74),
       (3158, 'Caucaia [Ceará]', '', 74),
       (3159, 'Crateús [Ceará]', '', 74),
       (3160, 'Fortaleza [Ceará]', '', 74),
       (3161, 'Juazeiro do Norte [Ceará]', '', 74),
       (3162, 'Maracanaú [Ceará]', '', 74),
       (3163, 'Sobral [Ceará]', '', 74),
       (3164, 'Brasilia [Distrito Federal]', '', 74),
       (3165, 'Aracruz [Espírito Santo]', '', 74),
       (3166, 'Baixo Guandu [Espírito Santo]', '', 74),
       (3167, 'Cachoeiro de Itapemirim [Espírito Santo]', '', 74),
       (3168, 'Cariacica [Espírito Santo]', '', 74),
       (3169, 'Colatina [Espírito Santo]', '', 74),
       (3170, 'Guarapari [Espírito Santo]', '', 74),
       (3171, 'Linhares [Espírito Santo]', '', 74),
       (3172, 'São Mateus [Espírito Santo]', '', 74),
       (3173, 'Serra [Espírito Santo]', '', 74),
       (3174, 'Viana [Espírito Santo]', '', 74),
       (3175, 'Vila Velha [Espírito Santo]', '', 74),
       (3176, 'Vitória [Espírito Santo]', '', 74),
       (3177, 'Acreúna [Goiás]', '', 74),
       (3178, 'Águas Lindas de Goiás [Goiás]', '', 74),
       (3179, 'Alexânia [Goiás]', '', 74),
       (3180, 'Anápolis [Goiás]', '', 74),
       (3181, 'Aparecida de Goiânia [Goiás]', '', 74),
       (3182, 'Aragarças [Goiás]', '', 74),
       (3183, 'Caldas Novas [Goiás]', '', 74),
       (3184, 'Catalão [Goiás]', '', 74),
       (3185, 'ciudad Ocidental [Goiás]', '', 74),
       (3186, 'Cristalina [Goiás]', '', 74),
       (3187, 'Formosa [Goiás]', '', 74),
       (3188, 'Goianésia [Goiás]', '', 74),
       (3189, 'Goiânia [Goiás]', '', 74),
       (3190, 'Goiás [Goiás]', '', 74),
       (3191, 'Goiatuba [Goiás]', '', 74),
       (3192, 'Inhumas [Goiás]', '', 74),
       (3193, 'Itaberaí [Goiás]', '', 74),
       (3194, 'Itapuranga [Goiás]', '', 74),
       (3195, 'Itumbiara [Goiás]', '', 74),
       (3196, 'Jaraguá [Goiás]', '', 74),
       (3197, 'Jataí [Goiás]', '', 74),
       (3198, 'Luziânia [Goiás]', '', 74),
       (3199, 'Minaçu [Goiás]', '', 74),
       (3200, 'Mineiros [Goiás]', '', 74),
       (3201, 'Morrinhos [Goiás]', '', 74),
       (3202, 'Niquelândia [Goiás]', '', 74),
       (3203, 'Novo Gama [Goiás]', '', 74),
       (3204, 'Piracanjuba [Goiás]', '', 74),
       (3205, 'Pires do Rio [Goiás]', '', 74),
       (3206, 'Planaltina [Goiás]', '', 74),
       (3207, 'Porangatu [Goiás]', '', 74),
       (3208, 'Quirinópolis [Goiás]', '', 74),
       (3209, 'Rio Verde [Goiás]', '', 74),
       (3210, 'Santa Helena de Goiás [Goiás]', '', 74),
       (3211, 'Santo Antônio do Descoberto [Goiás]', '', 74),
       (3212, 'São Luís de Montes Belos [Goiás]', '', 74),
       (3213, 'Senador Canedo [Goiás]', '', 74),
       (3214, 'Trindade [Goiás]', '', 74),
       (3215, 'Uruaçu [Goiás]', '', 74),
       (3216, 'Valparaíso de Goiás [Goiás]', '', 74),
       (3217, 'Balsas [Maranhão]', '', 74),
       (3218, 'Caxias [Maranhão]', '', 74),
       (3219, 'Coroatá [Maranhão]', '', 74),
       (3220, 'Estreito [Maranhão]', '', 74),
       (3221, 'Imperatriz [Maranhão]', '', 74),
       (3222, 'Paço do Lumiar [Maranhão]', '', 74),
       (3223, 'Raposa [Maranhão]', '', 74),
       (3224, 'Santa Quitéria do Maranhão [Maranhão]', '', 74),
       (3225, 'São José de Ribamar [Maranhão]', '', 74),
       (3226, 'São Luís [Maranhão]', '', 74),
       (3227, 'Timon [Maranhão]', '', 74),
       (3228, 'Alta Floresta [Mato Grosso]', '', 74),
       (3229, 'Barra do Bugres [Mato Grosso]', '', 74),
       (3230, 'Barra do Garças [Mato Grosso]', '', 74),
       (3231, 'Cáceres [Mato Grosso]', '', 74),
       (3232, 'Colíder [Mato Grosso]', '', 74),
       (3233, 'Cuiabá [Mato Grosso]', '', 74),
       (3234, 'Guarantã do Norte [Mato Grosso]', '', 74),
       (3235, 'Jaciara [Mato Grosso]', '', 74),
       (3236, 'Juína [Mato Grosso]', '', 74),
       (3237, 'Peixoto de Azevedo [Mato Grosso]', '', 74),
       (3238, 'Poconé [Mato Grosso]', '', 74),
       (3239, 'Pontes e Lacerda [Mato Grosso]', '', 74),
       (3240, 'Primavera do Leste [Mato Grosso]', '', 74),
       (3241, 'Rondonópolis [Mato Grosso]', '', 74),
       (3242, 'Sinop [Mato Grosso]', '', 74),
       (3243, 'Sorriso [Mato Grosso]', '', 74),
       (3244, 'Tangará da Serra [Mato Grosso]', '', 74),
       (3245, 'Várzea Grande [Mato Grosso]', '', 74),
       (3246, 'Campo Grande [Mato Grosso del Sur]', '', 74),
       (3247, 'Corumbá [Mato Grosso del Sur]', '', 74),
       (3248, 'Dourados [Mato Grosso del Sur]', '', 74),
       (3249, 'MS-Amambai [Mato Grosso del Sur]', '', 74),
       (3250, 'MS-Aquidauana [Mato Grosso del Sur]', '', 74),
       (3251, 'MS-Bela Vista [Mato Grosso del Sur]', '', 74),
       (3252, 'MS-Coxim [Mato Grosso del Sur]', '', 74),
       (3253, 'MS-Jardim [Mato Grosso del Sur]', '', 74),
       (3254, 'MS-Maracaju [Mato Grosso del Sur]', '', 74),
       (3255, 'MS-Mundo Novo [Mato Grosso del Sur]', '', 74),
       (3256, 'MS-Naviraí [Mato Grosso del Sur]', '', 74),
       (3257, 'MS-Nova Andradina [Mato Grosso del Sur]', '', 74),
       (3258, 'MS-Paranaíba [Mato Grosso del Sur]', '', 74),
       (3259, 'MS-Ponta Porã [Mato Grosso del Sur]', '', 74),
       (3260, 'MS-São Gabriel do Oeste [Mato Grosso del Sur]', '', 74),
       (3261, 'Três Lagoas [Mato Grosso del Sur]', '', 74),
       (3262, 'Além Paraíba [Minas Gerais]', '', 74),
       (3263, 'Alfenas [Minas Gerais]', '', 74),
       (3264, 'Araguari [Minas Gerais]', '', 74),
       (3265, 'Araxá [Minas Gerais]', '', 74),
       (3266, 'Arcos [Minas Gerais]', '', 74),
       (3267, 'Barbacena [Minas Gerais]', '', 74),
       (3268, 'Belo Horizonte [Minas Gerais]', '', 74),
       (3269, 'Betim [Minas Gerais]', '', 74),
       (3270, 'Boa Esperança [Minas Gerais]', '', 74),
       (3271, 'Bocaiúva [Minas Gerais]', '', 74),
       (3272, 'Bom Despacho [Minas Gerais]', '', 74),
       (3273, 'Caeté [Minas Gerais]', '', 74),
       (3274, 'Campo Belo [Minas Gerais]', '', 74),
       (3275, 'Caratinga [Minas Gerais]', '', 74),
       (3276, 'Cataguases [Minas Gerais]', '', 74),
       (3277, 'Congonhas [Minas Gerais]', '', 74),
       (3278, 'Conselheiro Lafaiete [Minas Gerais]', '', 74),
       (3279, 'Contagem [Minas Gerais]', '', 74),
       (3280, 'Coronel Fabriciano [Minas Gerais]', '', 74),
       (3281, 'Curvelo [Minas Gerais]', '', 74),
       (3282, 'Diamantina [Minas Gerais]', '', 74),
       (3283, 'Esmeraldas [Minas Gerais]', '', 74),
       (3284, 'Formiga [Minas Gerais]', '', 74),
       (3285, 'Frutal [Minas Gerais]', '', 74),
       (3286, 'Governador Valadares [Minas Gerais]', '', 74),
       (3287, 'Guaxupé [Minas Gerais]', '', 74),
       (3288, 'Ibirité [Minas Gerais]', '', 74),
       (3289, 'Ipatinga [Minas Gerais]', '', 74),
       (3290, 'Itabira [Minas Gerais]', '', 74),
       (3291, 'Itabirito [Minas Gerais]', '', 74),
       (3292, 'Itajubá [Minas Gerais]', '', 74),
       (3293, 'Itaúna [Minas Gerais]', '', 74),
       (3294, 'Ituiutaba [Minas Gerais]', '', 74),
       (3295, 'Janaúba [Minas Gerais]', '', 74),
       (3296, 'Japaraíba [Minas Gerais]', '', 74),
       (3297, 'João Pinheiro [Minas Gerais]', '', 74),
       (3298, 'Joaquim Felício [Minas Gerais]', '', 74),
       (3299, 'Juiz de Fora [Minas Gerais]', '', 74),
       (3300, 'Lagoa da Prata [Minas Gerais]', '', 74),
       (3301, 'Lavras [Minas Gerais]', '', 74),
       (3302, 'Leopoldina [Minas Gerais]', '', 74),
       (3303, 'Manhuaçu [Minas Gerais]', '', 74),
       (3304, 'Mariana [Minas Gerais]', '', 74),
       (3305, 'Monte Carmelo [Minas Gerais]', '', 74),
       (3306, 'Montes Claros [Minas Gerais]', '', 74),
       (3307, 'Muriaé [Minas Gerais]', '', 74),
       (3308, 'Nanuque [Minas Gerais]', '', 74),
       (3309, 'Nova Lima [Minas Gerais]', '', 74),
       (3310, 'Nova Serrana [Minas Gerais]', '', 74),
       (3311, 'Oliveira [Minas Gerais]', '', 74),
       (3312, 'Ouro Preto [Minas Gerais]', '', 74),
       (3313, 'Pará de Minas [Minas Gerais]', '', 74),
       (3314, 'Paracatu [Minas Gerais]', '', 74),
       (3315, 'Passos [Minas Gerais]', '', 74),
       (3316, 'Patos de Minas [Minas Gerais]', '', 74),
       (3317, 'Patrocínio [Minas Gerais]', '', 74),
       (3318, 'Poços de Caldas [Minas Gerais]', '', 74),
       (3319, 'Ponte Nova [Minas Gerais]', '', 74),
       (3320, 'Pouso Alegre [Minas Gerais]', '', 74),
       (3321, 'Ribeirão das Neves [Minas Gerais]', '', 74),
       (3322, 'Sabará [Minas Gerais]', '', 74),
       (3323, 'Santa Luzia [Minas Gerais]', '', 74),
       (3324, 'Santos Dumont [Minas Gerais]', '', 74),
       (3325, 'São João del Rei [Minas Gerais]', '', 74),
       (3326, 'São Lourenço [Minas Gerais]', '', 74),
       (3327, 'São Sebastião do Paraíso [Minas Gerais]', '', 74),
       (3328, 'Sarzedo [Minas Gerais]', '', 74),
       (3329, 'Sete Lagoas [Minas Gerais]', '', 74),
       (3330, 'Teófilo Otoni [Minas Gerais]', '', 74),
       (3331, 'Timóteo [Minas Gerais]', '', 74),
       (3332, 'Três Corações [Minas Gerais]', '', 74),
       (3333, 'Três Pontas [Minas Gerais]', '', 74),
       (3334, 'Ubá [Minas Gerais]', '', 74),
       (3335, 'Uberaba [Minas Gerais]', '', 74),
       (3336, 'Uberlândia [Minas Gerais]', '', 74),
       (3337, 'Unaí [Minas Gerais]', '', 74),
       (3338, 'Varginha [Minas Gerais]', '', 74),
       (3339, 'Vespasiano [Minas Gerais]', '', 74),
       (3340, 'Viçosa [Minas Gerais]', '', 74),
       (3341, 'Abaetetuba [Pará]', '', 74),
       (3342, 'Alenquer [Pará]', '', 74),
       (3343, 'Altamira [Pará]', '', 74),
       (3344, 'Ananindeua [Pará]', '', 74),
       (3345, 'Barcarena [Pará]', '', 74),
       (3346, 'Belém [Pará]', '', 74),
       (3347, 'Benevides [Pará]', '', 74),
       (3348, 'Bragança [Pará]', '', 74),
       (3349, 'Cametá [Pará]', '', 74),
       (3350, 'Capanema [Pará]', '', 74),
       (3351, 'Capitão Poço [Pará]', '', 74),
       (3352, 'Castanhal [Pará]', '', 74),
       (3353, 'Conceição do Araguaia [Pará]', '', 74),
       (3354, 'Dom Eliseu [Pará]', '', 74),
       (3355, 'Igarapé-Miri [Pará]', '', 74),
       (3356, 'Itaituba [Pará]', '', 74),
       (3357, 'Jacundá [Pará]', '', 74),
       (3358, 'Marabá [Pará]', '', 74),
       (3359, 'Marituba [Pará]', '', 74),
       (3360, 'Monte Alegre [Pará]', '', 74),
       (3361, 'Óbidos [Pará]', '', 74),
       (3362, 'Oriximiná [Pará]', '', 74),
       (3363, 'Paragominas [Pará]', '', 74),
       (3364, 'Parauapebas [Pará]', '', 74),
       (3365, 'Redenção [Pará]', '', 74),
       (3366, 'Rondon do Pará [Pará]', '', 74),
       (3367, 'Salinópolis [Pará]', '', 74),
       (3368, 'Santa Isabel do Pará [Pará]', '', 74),
       (3369, 'Santarém [Pará]', '', 74),
       (3370, 'São Miguel do Guamá [Pará]', '', 74),
       (3371, 'Tailândia [Pará]', '', 74),
       (3372, 'Tomé-Açu [Pará]', '', 74),
       (3373, 'Tucuruí [Pará]', '', 74),
       (3374, 'Vigia [Pará]', '', 74),
       (3375, 'Xinguara [Pará]', '', 74),
       (3376, 'Cabedelo [Paraíba]', '', 74),
       (3377, 'Campina Grande [Paraíba]', '', 74),
       (3378, 'João Pessoa [Paraíba]', '', 74),
       (3379, 'Patos [Paraíba]', '', 74),
       (3380, 'Santa Rita [Paraíba]', '', 74),
       (3381, 'Almirante Tamandaré [Paraná]', '', 74),
       (3382, 'Apucarana [Paraná]', '', 74),
       (3383, 'Arapongas [Paraná]', '', 74),
       (3384, 'Araucária [Paraná]', '', 74),
       (3385, 'Assis Chateaubriand [Paraná]', '', 74),
       (3386, 'Bandeirantes [Paraná]', '', 74),
       (3387, 'Cambé [Paraná]', '', 74),
       (3388, 'Campina Grande do Sul [Paraná]', '', 74),
       (3389, 'Campo Largo [Paraná]', '', 74),
       (3390, 'Campo Magro [Paraná]', '', 74),
       (3391, 'Campo Mourão [Paraná]', '', 74),
       (3392, 'Cascavel [Paraná]', '', 74),
       (3393, 'Cianorte [Paraná]', '', 74),
       (3394, 'Cianorte [Paraná]', '', 74),
       (3395, 'Colombo [Paraná]', '', 74),
       (3396, 'Cornélio Procópio [Paraná]', '', 74),
       (3397, 'Curitiba [Paraná]', '', 74),
       (3398, 'Fazenda Rio Grande [Paraná]', '', 74),
       (3399, 'Foz do Iguaçu [Paraná]', '', 74),
       (3400, 'Francisco Beltrão [Paraná]', '', 74),
       (3401, 'Guarapuava [Paraná]', '', 74),
       (3402, 'Ibiporã [Paraná]', '', 74),
       (3403, 'Irati [Paraná]', '', 74),
       (3404, 'Ivaiporã [Paraná]', '', 74),
       (3405, 'Jaguariaíva [Paraná]', '', 74),
       (3406, 'Londrina [Paraná]', '', 74),
       (3407, 'Mandaguari [Paraná]', '', 74),
       (3408, 'Marechal Cândido Rondon [Paraná]', '', 74),
       (3409, 'Maringá [Paraná]', '', 74),
       (3410, 'Medianeira [Paraná]', '', 74),
       (3411, 'Paiçandu [Paraná]', '', 74),
       (3412, 'Palmas [Paraná]', '', 74),
       (3413, 'Paranaguá [Paraná]', '', 74),
       (3414, 'Paranavaí [Paraná]', '', 74),
       (3415, 'Pato Branco [Paraná]', '', 74),
       (3416, 'Pinhais [Paraná]', '', 74),
       (3417, 'Piraquara [Paraná]', '', 74),
       (3418, 'Ponta Grossa [Paraná]', '', 74),
       (3419, 'Rolândia [Paraná]', '', 74),
       (3420, 'Santo Antônio da Platina [Paraná]', '', 74),
       (3421, 'São José dos Pinhais [Paraná]', '', 74),
       (3422, 'Sarandi [Paraná]', '', 74),
       (3423, 'Telêmaco Borba [Paraná]', '', 74),
       (3424, 'Toledo [Paraná]', '', 74),
       (3425, 'Umuarama [Paraná]', '', 74),
       (3426, 'União da Vitória [Paraná]', '', 74),
       (3427, 'Arcoverde [Pernambuco]', '', 74),
       (3428, 'Cabo de Santo Agostinho [Pernambuco]', '', 74),
       (3429, 'Camaragibe [Pernambuco]', '', 74),
       (3430, 'Caruarub [Pernambuco]', '', 74),
       (3431, 'Garanhuns [Pernambuco]', '', 74),
       (3432, 'Jaboatão dos Guararapes [Pernambuco]', '', 74),
       (3433, 'Olinda [Pernambuco]', '', 74),
       (3434, 'Paulista [Pernambuco]', '', 74),
       (3435, 'Petrolina [Pernambuco]', '', 74),
       (3436, 'Recife [Pernambuco]', '', 74),
       (3437, 'Salgueiro [Pernambuco]', '', 74),
       (3438, 'Vitória de Santo Antão [Pernambuco]', '', 74),
       (3439, 'José de Freitas [Piauí]', '', 74),
       (3440, 'Parnaíba [Piauí]', '', 74),
       (3441, 'Piracuruca [Piauí]', '', 74),
       (3442, 'Teresina [Piauí]', '', 74),
       (3443, 'Angra dos Reis [Río de Janeiro]', '', 74),
       (3444, 'Araruama [Río de Janeiro]', '', 74),
       (3445, 'Barra Mansa [Río de Janeiro]', '', 74),
       (3446, 'Belford Roxo [Río de Janeiro]', '', 74),
       (3447, 'Cabo Frío [Río de Janeiro]', '', 74),
       (3448, 'Cachoeiras de Macacu [Río de Janeiro]', '', 74),
       (3449, 'Campos dos Goytacazes [Río de Janeiro]', '', 74),
       (3450, 'Duque de Caxias [Río de Janeiro]', '', 74),
       (3451, 'Itaboraí [Río de Janeiro]', '', 74),
       (3452, 'Itaguaí [Río de Janeiro]', '', 74),
       (3453, 'Itaperuna [Río de Janeiro]', '', 74),
       (3454, 'Japeri [Río de Janeiro]', '', 74),
       (3455, 'Macaé [Río de Janeiro]', '', 74),
       (3456, 'Magé [Río de Janeiro]', '', 74),
       (3457, 'Nilópolis [Río de Janeiro]', '', 74),
       (3458, 'Niterói [Río de Janeiro]', '', 74),
       (3459, 'Nova Friburgo [Río de Janeiro]', '', 74),
       (3460, 'Nova Iguaçu [Río de Janeiro]', '', 74),
       (3461, 'Paracambi [Río de Janeiro]', '', 74),
       (3462, 'Petrópolis [Río de Janeiro]', '', 74),
       (3463, 'Queimados [Río de Janeiro]', '', 74),
       (3464, 'Resende [Río de Janeiro]', '', 74),
       (3465, 'Rio Bonito [Río de Janeiro]', '', 74),
       (3466, 'Rio das Ostras [Río de Janeiro]', '', 74),
       (3467, 'Río de Janeiro [Río de Janeiro]', '', 74),
       (3468, 'São Gonçalo [Río de Janeiro]', '', 74),
       (3469, 'São João de Meriti [Río de Janeiro]', '', 74),
       (3470, 'São Pedro da Aldeia [Río de Janeiro]', '', 74),
       (3471, 'Saquarema [Río de Janeiro]', '', 74),
       (3472, 'Seropédica [Río de Janeiro]', '', 74),
       (3473, 'Teresópolis [Río de Janeiro]', '', 74),
       (3474, 'Três Rios [Río de Janeiro]', '', 74),
       (3475, 'Valença [Río de Janeiro]', '', 74),
       (3476, 'Volta Redonda [Río de Janeiro]', '', 74),
       (3477, 'Mossoró [Río Grande del Norte]', '', 74),
       (3478, 'Natal [Río Grande del Norte]', '', 74),
       (3479, 'Parnamirim [Río Grande del Norte]', '', 74),
       (3480, 'Alegrete [Río Grande del Sur]', '', 74),
       (3481, 'Alvorada [Río Grande del Sur]', '', 74),
       (3482, 'Bagé [Río Grande del Sur]', '', 74),
       (3483, 'Bento Gonçalves [Río Grande del Sur]', '', 74),
       (3484, 'Cachoeira do Sul [Río Grande del Sur]', '', 74),
       (3485, 'Cachoeirinha [Río Grande del Sur]', '', 74),
       (3486, 'Camaquã [Río Grande del Sur]', '', 74),
       (3487, 'Campo Bom [Río Grande del Sur]', '', 74),
       (3488, 'Canela [Río Grande del Sur]', '', 74),
       (3489, 'Canoas [Río Grande del Sur]', '', 74),
       (3490, 'Capão da Canoa [Río Grande del Sur]', '', 74),
       (3491, 'Carazinho [Río Grande del Sur]', '', 74),
       (3492, 'Caxias do Sul [Río Grande del Sur]', '', 74),
       (3493, 'Charqueadas [Río Grande del Sur]', '', 74),
       (3494, 'Cruz Alta [Río Grande del Sur]', '', 74),
       (3495, 'Dom Pedrito [Río Grande del Sur]', '', 74),
       (3496, 'Encantado [Río Grande del Sur]', '', 74),
       (3497, 'Erechim [Río Grande del Sur]', '', 74),
       (3498, 'Estância Velha [Río Grande del Sur]', '', 74),
       (3499, 'Esteio [Río Grande del Sur]', '', 74),
       (3500, 'Farroupilha [Río Grande del Sur]', '', 74),
       (3501, 'Gravataí [Río Grande del Sur]', '', 74),
       (3502, 'Guaíba [Río Grande del Sur]', '', 74),
       (3503, 'Igrejinha [Río Grande del Sur]', '', 74),
       (3504, 'Ijuí [Río Grande del Sur]', '', 74),
       (3505, 'Itaqui [Río Grande del Sur]', '', 74),
       (3506, 'Jaguarão [Río Grande del Sur]', '', 74),
       (3507, 'Lajeado [Río Grande del Sur]', '', 74),
       (3508, 'Montenegro [Río Grande del Sur]', '', 74),
       (3509, 'Novo Hamburgo [Río Grande del Sur]', '', 74),
       (3510, 'Osório [Río Grande del Sur]', '', 74),
       (3511, 'Palmeira das Missões [Río Grande del Sur]', '', 74),
       (3512, 'Panambi [Río Grande del Sur]', '', 74),
       (3513, 'Parobé [Río Grande del Sur]', '', 74),
       (3514, 'Passo Fundo [Río Grande del Sur]', '', 74),
       (3515, 'Pelotas [Río Grande del Sur]', '', 74),
       (3516, 'Porto Alegre [Río Grande del Sur]', '', 74),
       (3517, 'Rio Grande [Río Grande del Sur]', '', 74),
       (3518, 'Rio Pardo [Río Grande del Sur]', '', 74),
       (3519, 'Rosário do Sul [Río Grande del Sur]', '', 74),
       (3520, 'Santa Cruz do Sul [Río Grande del Sur]', '', 74),
       (3521, 'Santa Maria [Río Grande del Sur]', '', 74),
       (3522, 'Santa Rosa [Río Grande del Sur]', '', 74),
       (3523, 'Santa Vitória do Palmar [Río Grande del Sur]', '', 74),
       (3524, 'Santana do Livramento [Río Grande del Sur]', '', 74),
       (3525, 'Santiago [Río Grande del Sur]', '', 74),
       (3526, 'Santo Ângelo [Río Grande del Sur]', '', 74),
       (3527, 'São Borja [Río Grande del Sur]', '', 74),
       (3528, 'São Gabriel [Río Grande del Sur]', '', 74),
       (3529, 'São Leopoldo [Río Grande del Sur]', '', 74),
       (3530, 'São Luiz Gonzaga [Río Grande del Sur]', '', 74),
       (3531, 'São Sepé [Río Grande del Sur]', '', 74),
       (3532, 'Sapiranga [Río Grande del Sur]', '', 74),
       (3533, 'Sapucaia do Sul [Río Grande del Sur]', '', 74),
       (3534, 'Taquara [Río Grande del Sur]', '', 74),
       (3535, 'Torres [Río Grande del Sur]', '', 74),
       (3536, 'Tramandaí [Río Grande del Sur]', '', 74),
       (3537, 'Uruguaiana [Río Grande del Sur]', '', 74),
       (3538, 'Vacaria [Río Grande del Sur]', '', 74),
       (3539, 'Venâncio Aires [Río Grande del Sur]', '', 74),
       (3540, 'Viamão [Río Grande del Sur]', '', 74),
       (3541, 'Xangri-lá [Río Grande del Sur]', '', 74),
       (3542, 'Ariquemes [Rondonia]', '', 74),
       (3543, 'Cacoal [Rondonia]', '', 74),
       (3544, 'Espigão dOeste [Rondonia]', '', 74),
       (3545, 'Guajará-Mirim [Rondonia]', '', 74),
       (3546, 'Jaru [Rondonia]', '', 74),
       (3547, 'Ji-Paraná [Rondonia]', '', 74),
       (3548, 'Pimenta Bueno [Rondonia]', '', 74),
       (3549, 'Porto Velho [Rondonia]', '', 74),
       (3550, 'Rolim de Moura [Rondonia]', '', 74),
       (3551, 'Vilhena [Rondonia]', '', 74),
       (3552, 'Boa Vista [Roraima]', '', 74),
       (3553, 'Balneário Camboriú [Santa Catarina]', '', 74),
       (3554, 'Biguaçu [Santa Catarina]', '', 74),
       (3555, 'Blumenau [Santa Catarina]', '', 74),
       (3556, 'Brusque [Santa Catarina]', '', 74),
       (3557, 'Caçador [Santa Catarina]', '', 74),
       (3558, 'Camboriú [Santa Catarina]', '', 74),
       (3559, 'Canoinhas [Santa Catarina]', '', 74),
       (3560, 'Chapecó [Santa Catarina]', '', 74),
       (3561, 'Concórdia [Santa Catarina]', '', 74),
       (3562, 'Criciúma [Santa Catarina]', '', 74),
       (3563, 'Curitibanos [Santa Catarina]', '', 74),
       (3564, 'Florianópolis [Santa Catarina]', '', 74),
       (3565, 'Fraiburgo [Santa Catarina]', '', 74),
       (3566, 'Garopaba [Santa Catarina]', '', 74),
       (3567, 'Gaspar [Santa Catarina]', '', 74),
       (3568, 'Içara [Santa Catarina]', '', 74),
       (3569, 'Imbituba [Santa Catarina]', '', 74),
       (3570, 'Indaial [Santa Catarina]', '', 74),
       (3571, 'Itajaí [Santa Catarina]', '', 74),
       (3572, 'Itapoá [Santa Catarina]', '', 74),
       (3573, 'Jaraguá do Sul [Santa Catarina]', '', 74),
       (3574, 'Joinville [Santa Catarina]', '', 74),
       (3575, 'Lages [Santa Catarina]', '', 74),
       (3576, 'Laguna [Santa Catarina]', '', 74),
       (3577, 'Mafra [Santa Catarina]', '', 74),
       (3578, 'Navegantes [Santa Catarina]', '', 74),
       (3579, 'Palhoça [Santa Catarina]', '', 74),
       (3580, 'Pomerode [Santa Catarina]', '', 74),
       (3581, 'Porto União [Santa Catarina]', '', 74),
       (3582, 'Rio do Sul [Santa Catarina]', '', 74),
       (3583, 'São Bento do Sul [Santa Catarina]', '', 74),
       (3584, 'São Francisco do Sul [Santa Catarina]', '', 74),
       (3585, 'São José [Santa Catarina]', '', 74),
       (3586, 'São Miguel dOeste [Santa Catarina]', '', 74),
       (3587, 'Timbó [Santa Catarina]', '', 74),
       (3588, 'Tubarão [Santa Catarina]', '', 74),
       (3589, 'Videira [Santa Catarina]', '', 74),
       (3590, 'Xanxerê [Santa Catarina]', '', 74),
       (3591, 'Xaxim [Santa Catarina]', '', 74),
       (3592, 'Adamantina [São Paulo]', '', 74),
       (3593, 'Agudos [São Paulo]', '', 74),
       (3594, 'Americana [São Paulo]', '', 74),
       (3595, 'Amparo [São Paulo]', '', 74),
       (3596, 'Andradina [São Paulo]', '', 74),
       (3597, 'Aparecida [São Paulo]', '', 74),
       (3598, 'Araçatuba [São Paulo]', '', 74),
       (3599, 'Araraquara [São Paulo]', '', 74),
       (3600, 'Araras [São Paulo]', '', 74),
       (3601, 'Artur Nogueira [São Paulo]', '', 74),
       (3602, 'Arujá [São Paulo]', '', 74),
       (3603, 'Assis [São Paulo]', '', 74),
       (3604, 'Atibaia [São Paulo]', '', 74),
       (3605, 'Avaré [São Paulo]', '', 74),
       (3606, 'Barra Bonita [São Paulo]', '', 74),
       (3607, 'Barretos [São Paulo]', '', 74),
       (3608, 'Barueri [São Paulo]', '', 74),
       (3609, 'Batatais [São Paulo]', '', 74),
       (3610, 'Bauru [São Paulo]', '', 74),
       (3611, 'Bebedouro [São Paulo]', '', 74),
       (3612, 'Bertioga [São Paulo]', '', 74),
       (3613, 'Birigui [São Paulo]', '', 74),
       (3614, 'Boituva [São Paulo]', '', 74),
       (3615, 'Botucatu [São Paulo]', '', 74),
       (3616, 'Bragança Paulista [São Paulo]', '', 74),
       (3617, 'Caçapava [São Paulo]', '', 74),
       (3618, 'Caieiras [São Paulo]', '', 74),
       (3619, 'Cajamar [São Paulo]', '', 74),
       (3620, 'Campinas [São Paulo]', '', 74),
       (3621, 'Campo Limpo Paulista [São Paulo]', '', 74),
       (3622, 'Campos do Jordão [São Paulo]', '', 74),
       (3623, 'Capivari [São Paulo]', '', 74),
       (3624, 'Caraguatatuba [São Paulo]', '', 74),
       (3625, 'Carapicuíba [São Paulo]', '', 74),
       (3626, 'Catanduva [São Paulo]', '', 74),
       (3627, 'Cerquilho [São Paulo]', '', 74),
       (3628, 'Cordeirópolis [São Paulo]', '', 74),
       (3629, 'Cosmópolis [São Paulo]', '', 74),
       (3630, 'Cruzeiro [São Paulo]', '', 74),
       (3631, 'Diadema [São Paulo]', '', 74),
       (3632, 'Dracena [São Paulo]', '', 74),
       (3633, 'Embu [São Paulo]', '', 74),
       (3634, 'Embu-Guaçu [São Paulo]', '', 74),
       (3635, 'Espírito Santo do Pinhal [São Paulo]', '', 74),
       (3636, 'Fernandópolis [São Paulo]', '', 74),
       (3637, 'Ferraz de Vasconcelos [São Paulo]', '', 74),
       (3638, 'Franca [São Paulo]', '', 74),
       (3639, 'Francisco Morato [São Paulo]', '', 74),
       (3640, 'Franco da Rocha [São Paulo]', '', 74),
       (3641, 'Garça [São Paulo]', '', 74),
       (3642, 'Guaíra [São Paulo]', '', 74),
       (3643, 'Guaratinguetá [São Paulo]', '', 74),
       (3644, 'Guariba [São Paulo]', '', 74),
       (3645, 'Guarujá [São Paulo]', '', 74),
       (3646, 'Guarulhos [São Paulo]', '', 74),
       (3647, 'Hortolândia [São Paulo]', '', 74),
       (3648, 'Ibitinga [São Paulo]', '', 74),
       (3649, 'Indaiatuba [São Paulo]', '', 74),
       (3650, 'Itanhaém [São Paulo]', '', 74),
       (3651, 'Itapecerica da Serra [São Paulo]', '', 74),
       (3652, 'Itapetininga [São Paulo]', '', 74),
       (3653, 'Itapeva [São Paulo]', '', 74),
       (3654, 'Itapevi [São Paulo]', '', 74),
       (3655, 'Itapira [São Paulo]', '', 74),
       (3656, 'Itápolis [São Paulo]', '', 74),
       (3657, 'Itaquaquecetuba [São Paulo]', '', 74),
       (3658, 'Itararé [São Paulo]', '', 74),
       (3659, 'Itatiba [São Paulo]', '', 74),
       (3660, 'Itu [São Paulo]', '', 74),
       (3661, 'Ituverava [São Paulo]', '', 74),
       (3662, 'Jaboticabal [São Paulo]', '', 74),
       (3663, 'Jacareí [São Paulo]', '', 74),
       (3664, 'Jales [São Paulo]', '', 74),
       (3665, 'Jandira [São Paulo]', '', 74),
       (3666, 'Jarinu [São Paulo]', '', 74),
       (3667, 'Jaú [São Paulo]', '', 74),
       (3668, 'Jundiaí [São Paulo]', '', 74),
       (3669, 'Leme [São Paulo]', '', 74),
       (3670, 'Lençóis Paulista [São Paulo]', '', 74),
       (3671, 'Limeira [São Paulo]', '', 74),
       (3672, 'Lins [São Paulo]', '', 74),
       (3673, 'Lorena [São Paulo]', '', 74),
       (3674, 'Mairinque [São Paulo]', '', 74),
       (3675, 'Mairiporã [São Paulo]', '', 74),
       (3676, 'Marília [São Paulo]', '', 74),
       (3677, 'Matão [São Paulo]', '', 74),
       (3678, 'Mauá [São Paulo]', '', 74),
       (3679, 'Mirassol [São Paulo]', '', 74),
       (3680, 'Mococa [São Paulo]', '', 74),
       (3681, 'Mogi das Cruzes [São Paulo]', '', 74),
       (3682, 'Mogi Guaçu [São Paulo]', '', 74),
       (3683, 'Moji-Mirim [São Paulo]', '', 74),
       (3684, 'Mongaguá [São Paulo]', '', 74),
       (3685, 'Monte Alto [São Paulo]', '', 74),
       (3686, 'Monte Mor [São Paulo]', '', 74),
       (3687, 'Nova Odessa [São Paulo]', '', 74),
       (3688, 'Olímpia [São Paulo]', '', 74),
       (3689, 'Orlândia [São Paulo]', '', 74),
       (3690, 'Osasco [São Paulo]', '', 74),
       (3691, 'Ourinhos [São Paulo]', '', 74),
       (3692, 'Paraguaçu Paulista [São Paulo]', '', 74),
       (3693, 'Pederneiras [São Paulo]', '', 74),
       (3694, 'Pedreira [São Paulo]', '', 74),
       (3695, 'Penápolis [São Paulo]', '', 74),
       (3696, 'Peruíbe [São Paulo]', '', 74),
       (3697, 'Pindamonhangaba [São Paulo]', '', 74),
       (3698, 'Piracicaba [São Paulo]', '', 74),
       (3699, 'Pirassununga [São Paulo]', '', 74),
       (3700, 'Pitangueiras [São Paulo]', '', 74),
       (3701, 'Poá [São Paulo]', '', 74),
       (3702, 'Porto Feliz [São Paulo]', '', 74),
       (3703, 'Praia Grande [São Paulo]', '', 74),
       (3704, 'Presidente Epitácio [São Paulo]', '', 74),
       (3705, 'Presidente Prudente [São Paulo]', '', 74),
       (3706, 'Registro [São Paulo]', '', 74),
       (3707, 'Ribeirão Pires [São Paulo]', '', 74),
       (3708, 'Ribeirão Preto [São Paulo]', '', 74),
       (3709, 'Rio Claro [São Paulo]', '', 74),
       (3710, 'Rio Grande da Serra [São Paulo]', '', 74),
       (3711, 'Salto [São Paulo]', '', 74),
       (3712, 'Santa Bárbara dOeste [São Paulo]', '', 74),
       (3713, 'Santa Cruz do Rio Pardo [São Paulo]', '', 74),
       (3714, 'Santa Isabel [São Paulo]', '', 74),
       (3715, 'Santana de Parnaíba [São Paulo]', '', 74),
       (3716, 'Santo André [São Paulo]', '', 74),
       (3717, 'Santos [São Paulo]', '', 74),
       (3718, 'São Bernardo do Campo [São Paulo]', '', 74),
       (3719, 'São Caetano do Sul [São Paulo]', '', 74),
       (3720, 'São Carlos [São Paulo]', '', 74),
       (3721, 'São João da Boa Vista [São Paulo]', '', 74),
       (3722, 'São Joaquim da Barra [São Paulo]', '', 74),
       (3723, 'São José do Rio Pardo [São Paulo]', '', 74),
       (3724, 'São José do Rio Preto [São Paulo]', '', 74),
       (3725, 'São José dos Campos [São Paulo]', '', 74),
       (3726, 'São Manuel [São Paulo]', '', 74),
       (3727, 'São Paulo [São Paulo]', '', 74),
       (3728, 'São Roque [São Paulo]', '', 74),
       (3729, 'São Sebastião [São Paulo]', '', 74),
       (3730, 'São Vicente [São Paulo]', '', 74),
       (3731, 'Serrana [São Paulo]', '', 74),
       (3732, 'Sertãozinho [São Paulo]', '', 74),
       (3733, 'Sorocaba [São Paulo]', '', 74),
       (3734, 'Sumaré [São Paulo]', '', 74),
       (3735, 'Suzano [São Paulo]', '', 74),
       (3736, 'Tabapuã [São Paulo]', '', 74),
       (3737, 'Taboão da Serra [São Paulo]', '', 74),
       (3738, 'Taquaritinga [São Paulo]', '', 74),
       (3739, 'Tatuí [São Paulo]', '', 74),
       (3740, 'Taubaté [São Paulo]', '', 74),
       (3741, 'Tremembé [São Paulo]', '', 74),
       (3742, 'Tupã [São Paulo]', '', 74),
       (3743, 'Ubatuba [São Paulo]', '', 74),
       (3744, 'Valinhos [São Paulo]', '', 74),
       (3745, 'Vargem Grande do Sul [São Paulo]', '', 74),
       (3746, 'Vargem Grande Paulista [São Paulo]', '', 74),
       (3747, 'Várzea Paulista [São Paulo]', '', 74),
       (3748, 'Vinhedo [São Paulo]', '', 74),
       (3749, 'Votorantim [São Paulo]', '', 74),
       (3750, 'Votuporanga [São Paulo]', '', 74),
       (3751, 'Aracaju [Sergipe]', '', 74),
       (3752, 'Araguaína [Tocantins]', '', 74),
       (3753, 'Palmas [Tocantins]', '', 74);

# ARG id types (ids 17 & 18 autogenerated)
INSERT INTO `pagomundo`.`id_type` (`name`, `code`, `country_id`, `natural_or_legal`)
VALUES ('DNI', null, 66, 'NATURAL');
INSERT INTO `pagomundo`.`id_type` (`name`, `code`, `country_id`, `natural_or_legal`)
VALUES ('CUIT', null, 66, 'BOTH');
# BRA id types (ids 19 & 20 autogenerated)
INSERT INTO `pagomundo`.`id_type` (`name`, `code`, `country_id`, `natural_or_legal`)
VALUES ('CPF', null, 74, 'NATURAL');
INSERT INTO `pagomundo`.`id_type` (`name`, `code`, `country_id`, `natural_or_legal`)
VALUES ('CNPJ', null, 74, 'LEGAL');

# ARG bank (id 31 autogenerated)
INSERT INTO `pagomundo`.`bank` (`name`, `country_id`)
VALUES ('Banco de Argentina', 66);
# BRA bank (id 32 autogenerated)
INSERT INTO `pagomundo`.`bank` (`name`, `country_id`)
VALUES ('Banco do Brasil', 74);

# ARG bankAccount (id 6 autogenerated)
INSERT INTO `bank_account` (`account_number`, `bank_commission`, `fx_commission`, `recharge_cost`, `country_id`,
                            `bank_id`)
VALUES ('Cuenta bancaria ARG', 0.02, 0.015, 2, 66, 31);
# BRA bankAccount (id 7 autogenerated)
INSERT INTO `bank_account` (`account_number`, `bank_commission`, `fx_commission`, `recharge_cost`, `country_id`,
                            `bank_id`)
VALUES ('Conta bancária BRA', 0.02, 0.015, 2, 74, 32);

#--- Reference data for ARG & BRA

#---- STP
alter table `pagomundo`.`integration_audit`
    modify `entity_id` bigint null;

alter table `pagomundo`.`integration_audit`
    add `status` varchar(255) null;

alter table `pagomundo`.`integration_audit`
    add `status_reason` longtext null;

alter table `pagomundo`.`integration_audit`
    add retries int(11) null;

alter table `pagomundo`.`integration_audit`
    add max_retries int(11) null;

#---- STP

#--- Nickname
ALTER TABLE `pagomundo`.`extended_user_relation`
    ADD COLUMN `nickname` varchar(255) null;
#--- Nickname

#--- Relacion transaction - ExtendedUserRelation
ALTER TABLE `transaction`
    ADD COLUMN `extended_user_relation_id` BIGINT(20) NULL DEFAULT NULL;

create index `fk_transaction_extended_user_relation_id`
    on `transaction` (extended_user_relation_id);

alter table `transaction`
    add constraint `fk_transaction_extended_user_relation_id`
        foreign key (extended_user_relation_id) references `extended_user_relation` (id);

-- Agregar relación de transacciones ya guardadas

-- Procedimiento de actualización de extended user relation en transaction

DELIMITER //
CREATE PROCEDURE updateExtendedUserRel()

BEGIN
    DECLARE finished INTEGER DEFAULT 0;
    DECLARE var_merchant_id BIGINT(20);
    DECLARE var_payee_id BIGINT(20);
    DECLARE var_extended_user_relation_id BIGINT(20);
    DECLARE transaction_cursor CURSOR FOR SELECT merchant_id, payee_id FROM transaction;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET finished = 1;

    OPEN transaction_cursor;

    loop_transaction:
        LOOP

            FETCH transaction_cursor INTO var_merchant_id,var_payee_id;

            IF finished = 1 THEN
                LEAVE loop_transaction;
            END IF;

            SELECT id
            INTO var_extended_user_relation_id
            FROM extended_user_relation
            WHERE extended_user_id = var_payee_id
              AND user_related_id = (SELECT user_id FROM extended_user WHERE id = var_merchant_id);

            UPDATE transaction
            SET extended_user_relation_id = var_extended_user_relation_id
            WHERE merchant_id = var_merchant_id
              AND payee_id = var_payee_id;


        END LOOP loop_transaction;

END//

-- Ejecuta el procedimiento anterior
CALL updateExtendedUserRel;

-- Elimina el procedimiento
DROP PROCEDURE IF EXISTS updateExtendedUserRel;

#--- Relación transaction - ExtendedUserRelation

#--- Flat commission
alter table extended_user
    add use_flat_commission boolean default false null;

update extended_user set extended_user.use_flat_commission = false where 1;

#--- Flat commission
#--- fix/28345-ApiPayeePersonaArgentina
UPDATE `pagomundo`.`id_type` t
SET t.`natural_or_legal` = 'LEGAL'
WHERE t.`id` = 18;

-- IMPORTANT: ES needs to be REFETCHED --

#--- fix/28345-ApiPayeePersonaArgentina

# -- Add bank_branch to extended_user

alter table extended_user
    add bank_branch varchar(255) null;

# -- Add bank_branch to extended_user

#--- Pagos Brasil
#--- Bancos Brasil
INSERT INTO bank (name, country_id)
VALUES ('BANCO A.J. RENNER S.A.', 74),
       ('Banco ABC Brasil S.A.', 74),
       ('Banco ABN Amro S.A.', 74),
       ('Banco Agiplan S.A.', 74),
       ('Banco Alfa S.A.', 74),
       ('Banco AndBank (Brasil) S.A.', 74),
       ('Banco Arbi S.A.', 74),
       ('Banco B3 S.A.', 74),
       ('Banco Bandepe S.A.', 74),
       ('BANCO BARI DE INVESTIMENTOS E FINANCIAMENTOS S.A.', 74),
       ('Banco Bmg S.A.', 74),
       ('Banco BNP Paribas Brasil S.A.', 74),
       ('Banco Bocom BBM S.A.', 74),
       ('Banco Bonsucesso S.A.', 74),
       ('Banco Bradescard S.A.', 74),
       ('Banco Bradesco BBI S.A.', 74),
       ('Banco Bradesco BERJ S.A.', 74),
       ('Banco Bradesco Financiamentos S.A.', 74),
       ('Banco Bradesco S.A.', 74),
       ('Banco BTG Pactual S.A.', 74),
       ('Banco C6 S.A.', 74),
       ('Banco Caixa Geral - Brasil S.A.', 74),
       ('BANCO CAPITAL S.A.', 74),
       ('Banco Cargill S.A.', 74),
       ('BANCO CEDULA S.A.', 74),
       ('Banco Cetelem S.A.', 74),
       ('Banco Cifra S.A.', 74),
       ('Banco Citibank', 74),
       ('BANCO CLASSICO S.A.', 74),
       ('Banco Cooperativo do Brasil S.A. - BANCOOB', 74),
       ('Banco Cooperativo Sicredi S.A.', 74),
       ('BANCO CRÉDIT AGRICOLE BRASIL S.A.', 74),
       ('Banco Credit Suisse (Brasil) S.A.', 74),
       ('Banco Crefisa S.A.', 74),
       ('Banco da Amazonia S.A.', 74),
       ('Banco da China Brasil S.A.', 74),
       ('Banco Daycoval S.A.', 74),
       ('Banco de Brasilia S.A. - BRB', 74),
       ('Banco de la Nacion Argentina', 74),
       ('Banco Digio S.A.', 74),
       ('Banco do Brasil S.A.', 74),
       ('Banco do Estado de Sergipe S.A. - BANESE', 74),
       ('Banco do Estado do Para S.A. - BANPARA', 74),
       ('Banco do Estado do Rio Grande do Sul S.A. - BANRISUL', 74),
       ('Banco do Nordeste do Brasil S.A.', 74),
       ('Banco Fator S.A.', 74),
       ('Banco Fibra S.A.', 74),
       ('BANCO FICSA S.A.', 74),
       ('Banco Finaxis S.A.', 74),
       ('Banco Guanabara S.A.', 74),
       ('Banco Inbursa S.A.', 74),
       ('Banco Industrial do Brasil S.A.', 74),
       ('BANCO INDUSVAL S.A.', 74),
       ('Banco Inter', 74),
       ('Banco Investcred Unibanco S.A.', 74),
       ('Banco Itaú BBA S.A.', 74),
       ('Banco Itaú Consignado S.A.', 74),
       ('Banco ItauBank S.A.', 74),
       ('Banco J. Safra S.A.', 74),
       ('BANCO J.P. MORGAN S.A.', 74),
       ('Banco John Deere S.A.', 74),
       ('Banco KDB do Brasil S.A.', 74),
       ('BANCO KEB HANA DO BRASIL S.A.', 74),
       ('Banco Luso Brasileiro S.A.', 74),
       ('Banco Máxima S.A.', 74),
       ('Banco Mercantil do Brasil S.A.', 74),
       ('Banco Mizuho do Brasil S.A.', 74),
       ('Banco Modal S.A.', 74),
       ('BANCO MORGAN STANLEY S.A.', 74),
       ('Banco MUFG Brasil S.A.', 74),
       ('BANCO NACIONAL DE DESENVOLVIMENTO ECONOMICO E SOCIAL', 74),
       ('Banco Neon', 74),
       ('Banco Olé Bonsucesso Consignado S.A.', 74),
       ('Banco Original', 74),
       ('Banco Original do Agronegócio S.A.', 74),
       ('Banco Ourinvest S.A.', 74),
       ('Banco Panamericano S.A.', 74),
       ('Banco Paulista S.A.', 74),
       ('Banco Pine S.A.', 74),
       ('Banco Rabobank International Brasil S.A.', 74),
       ('Banco Rendimento S.A.', 74),
       ('BANCO RIBEIRAO PRETO S.A.', 74),
       ('BANCO RODOBENS S.A.', 74),
       ('Banco Safra S.A.', 74),
       ('Banco Santander Brasil S.A.', 74),
       ('Banco Semear S.A.', 74),
       ('Banco Sistema S.A.', 74),
       ('Banco Smartbank S.A.', 74),
       ('BANCO SOCIETE GENERALE BRASIL S.A.', 74),
       ('Banco Sofisa', 74),
       ('Banco Sumitomo Mitsui Brasileiro S.A.', 74),
       ('BANCO TOPÁZIO S.A.', 74),
       ('BANCO TRIANGULO S.A.', 74),
       ('Banco Tricury S.A.', 74),
       ('Banco Votorantim S.A.', 74),
       ('Banco VR S.A.', 74),
       ('Banco Western Union do Brasil S.A.', 74),
       ('Banco Woori Bank do Brasil S.A.', 74),
       ('Banco XP S.A.', 74),
       ('BancoSeguro S.A.', 74),
       ('Banestes S.A. Banco do Estado do Espirito Santo', 74),
       ('Bank of America Merrill Lynch Banco Múltiplo S.A.', 74),
       ('BCV - BANCO DE CRÉDITO E VAREJO S.A.', 74),
       ('BEXS BANCO DE CÂMBIO S/A', 74),
       ('Bexs Corretora de Câmbio S/A', 74),
       ('BNY Mellon Banco S.A.', 74),
       ('BPP Instituição de Pagamento S.A.', 74),
       ('BR Partners Banco de Investimento S.A.', 74),
       ('Caixa Economica Federal - CEF', 74),
       ('China Construction Bank (Brasil) Banco Múltiplo S/A', 74),
       ('Citibank N.A.', 74),
       ('Commerzbank Brasil S.A. - Banco Múltiplo', 74),
       ('Confederacao Nacional das Cooperativas Centrais Unicreds', 74),
       ('Cooperativa Central de Credito Urbano - CECRED', 74),
       ('CREDICOAMO CREDITO RURAL COOPERATIVA', 74),
       ('CREDIT SUISSE HEDGING-GRIFFO CORRETORA DE VALORES S.A', 74),
       ('DEUTSCHE BANK S.A. - BANCO ALEMAO', 74),
       ('GOLDMAN SACHS DO BRASIL BANCO MULTIPLO S.A.', 74),
       ('Haitong Banco de Investimento do Brasil S.A.', 74),
       ('Hipercard Banco Múltiplo S.A.', 74),
       ('HSBC Bank Brasil S.A. - Banco Multiplo', 74),
       ('HSBC BRASIL S.A. - BANCO DE INVESTIMENTO', 74),
       ('ICBC do Brasil Banco Múltiplo S.A.', 74),
       ('ING Bank N.V.', 74),
       ('Intesa Sanpaolo Brasil S.A. - Banco Múltiplo', 74),
       ('Itaú Unibanco Holding S.A.', 74),
       ('Itau Unibanco S.A.', 74),
       ('JPMorgan Chase Bank National Association', 74),
       ('MONEYCORP BANCO DE CÂMBIO S.A.', 74),
       ('MS Bank S.A. Banco de Câmbio', 74),
       ('Novo Banco Continental S.A. - Banco Múltiplo', 74),
       ('Nu Pagamentos (Nubank)', 74),
       ('Omni Banco S.A.', 74),
       ('Pagseguro Internet S.A', 74),
       ('PARANÁ BANCO S.A.', 74),
       ('Plural S.A. Banco Múltiplo', 74),
       ('Scotiabank Brasil S.A. Banco Múltiplo', 74),
       ('STATE STREET BRASIL S.A. – BANCO COMERCIAL', 74),
       ('Travelex Banco de Câmbio S.A.', 74),
       ('UBS Brasil Banco de Investimento S.A.', 74),
       ('UBS Brasil Corretora de Câmbio Títulos e Valores Mobiliários S.A.', 74),
       ('Unicred Norte do Parana', 74);
#--- Bancos Brasil
#--- Pagos directos bancos
INSERT INTO direct_payment_bank (origin_bank_id, destiny_bank_id, destiny_bank_code)
VALUES (32, 33, 654),
       (32, 34, 246),
       (32, 35, 75),
       (32, 36, 121),
       (32, 37, 25),
       (32, 38, 65),
       (32, 39, 213),
       (32, 40, 96),
       (32, 41, 24),
       (32, 42, 330),
       (32, 43, 318),
       (32, 44, 752),
       (32, 45, 107),
       (32, 46, 218),
       (32, 47, 63),
       (32, 48, 36),
       (32, 49, 122),
       (32, 50, 394),
       (32, 51, 237),
       (32, 52, 208),
       (32, 53, 336),
       (32, 54, 473),
       (32, 55, 412),
       (32, 56, 40),
       (32, 57, 266),
       (32, 58, 739),
       (32, 59, 233),
       (32, 60, 745),
       (32, 61, 241),
       (32, 62, 756),
       (32, 63, 748),
       (32, 64, 222),
       (32, 65, 505),
       (32, 66, 69),
       (32, 67, 3),
       (32, 68, 83),
       (32, 69, 707),
       (32, 70, 70),
       (32, 71, 300),
       (32, 72, 335),
       (32, 73, 1),
       (32, 74, 47),
       (32, 75, 37),
       (32, 76, 41),
       (32, 77, 4),
       (32, 78, 265),
       (32, 79, 224),
       (32, 80, 626),
       (32, 81, 94),
       (32, 82, 612),
       (32, 83, 12),
       (32, 84, 604),
       (32, 85, 653),
       (32, 86, 77),
       (32, 87, 249),
       (32, 88, 184),
       (32, 89, 29),
       (32, 90, 479),
       (32, 91, 74),
       (32, 92, 376),
       (32, 93, 217),
       (32, 94, 76),
       (32, 95, 757),
       (32, 96, 600),
       (32, 97, 243),
       (32, 98, 389),
       (32, 99, 370),
       (32, 100, 746),
       (32, 101, 66),
       (32, 102, 456),
       (32, 103, 7),
       (32, 104, 735),
       (32, 105, 169),
       (32, 106, 212),
       (32, 107, 79),
       (32, 108, 712),
       (32, 109, 623),
       (32, 110, 611),
       (32, 111, 643),
       (32, 112, 747),
       (32, 113, 633),
       (32, 114, 741),
       (32, 115, 120),
       (32, 116, 422),
       (32, 117, 33),
       (32, 118, 743),
       (32, 119, 754),
       (32, 120, 630),
       (32, 121, 366),
       (32, 122, 637),
       (32, 123, 464),
       (32, 124, 82),
       (32, 125, 634),
       (32, 126, 18),
       (32, 127, 655),
       (32, 128, 610),
       (32, 129, 119),
       (32, 130, 124),
       (32, 131, 348),
       (32, 132, 81),
       (32, 133, 21),
       (32, 134, 755),
       (32, 135, 250),
       (32, 136, 144),
       (32, 137, 253),
       (32, 138, 17),
       (32, 139, 301),
       (32, 140, 126),
       (32, 141, 104),
       (32, 142, 320),
       (32, 143, 477),
       (32, 144, 163),
       (32, 145, 136),
       (32, 146, 85),
       (32, 147, 10),
       (32, 148, 11),
       (32, 149, 487),
       (32, 150, 64),
       (32, 151, 78),
       (32, 152, 62),
       (32, 153, 399),
       (32, 154, 269),
       (32, 155, 132),
       (32, 156, 492),
       (32, 157, 139),
       (32, 158, 652),
       (32, 159, 341),
       (32, 160, 488),
       (32, 161, 259),
       (32, 162, 128),
       (32, 163, 753),
       (32, 164, 260),
       (32, 165, 613),
       (32, 166, 290),
       (32, 167, 254),
       (32, 168, 125),
       (32, 169, 751),
       (32, 170, 14),
       (32, 171, 95),
       (32, 172, 129),
       (32, 173, 15),
       (32, 174, 84);
#--- Pagos directos bancos
#--- Pagos Brasil


# -- Add alias to extended_user
alter table extended_user
    add alias varchar(255) null;
# -- Add alias to extended_user
 
# -- Changing login of admins in jhi_user
UPDATE jhi_user SET login = email 
  WHERE id = 3 or id = 5; 
# -- Changing login of admins in jhi_user

# -- Add parameter pagomundo table 
ALTER TABLE jhi_user ADD COLUMN extra_url_parameter varchar(100) DEFAULT '';
# -- Add parameter pagomundo table jhi_user


# -- Create Table limit payment payee
CREATE TABLE threshold_payee(
  id integer not null AUTO_INCREMENT,
  country_id integer,
  threshold float,
  period enum('year', 'month'),
  PRIMARY KEY (id)
);

INSERT INTO threshold_payee(country_id, threshold, period)
VALUES (1, 30000, 'year'),
       (2, 3500, 'month'),
       (74, 3000, 'month'),
       (66, 4500, 'month');
# -- Create Table limit payment payee

# -- Create Davivienda -- #
insert into bank_account(account_number, bank_commission, fx_commission, recharge_cost, country_id, bank_id) 
  values('Davivienda', 0.02, 0.015, 2, 1, 17);


# -- Add column bex filename to transaction group table -- #
alter table transaction_group add column 
  file_name_bex varchar(255);


# -- Add chilean currency and enable country for payee registration -- #

INSERT INTO currency (name, code, country_id)
VALUES
('Chilean peso', 'CLP', 77);

UPDATE country
SET
    currency_name = 'Chilean Peso',
    currency_code = 'CLP',
    for_payee = 1
WHERE
    id = 77;

INSERT INTO id_type (
    name,
    country_id,
    natural_or_legal)
VALUES
    ('RUT', 77, 'NATURAL');


# -- Add chilean banks data

INSERT INTO bank (name, country_id) VALUES ('Banco de Chile- Edwards – Citi', 77);
INSERT INTO bank (name, country_id) VALUES ('Banco Internacional', 77);
INSERT INTO bank (name, country_id) VALUES ('Banco Estado', 77);
INSERT INTO bank (name, country_id) VALUES ('Banco Scotiabank', 77);
INSERT INTO bank (name, country_id) VALUES ('Banco de Crédito e Inversiones', 77);
INSERT INTO bank (name, country_id) VALUES ('Banco CorpBanca', 77);
INSERT INTO bank (name, country_id) VALUES ('Banco BICE', 77);
INSERT INTO bank (name, country_id) VALUES ('HSBC Bank (Chile)', 77);
INSERT INTO bank (name, country_id) VALUES ('Banco Santander', 77);
INSERT INTO bank (name, country_id) VALUES ('Banco Itaú', 77);
INSERT INTO bank (name, country_id) VALUES ('Banco RBS', 77);
INSERT INTO bank (name, country_id) VALUES ('Banco Security', 77);
INSERT INTO bank (name, country_id) VALUES ('Banco Falabella', 77);
INSERT INTO bank (name, country_id) VALUES ('Banco Rabobank', 77);
INSERT INTO bank (name, country_id) VALUES ('Banco BBVA', 77);
INSERT INTO bank (name, country_id) VALUES ('Banco del Desarrollo', 77);
INSERT INTO bank (name, country_id) VALUES ('Copeuch', 77);
INSERT INTO bank (name, country_id) VALUES ('Banco Ripley', 77);
INSERT INTO bank (name, country_id) VALUES ('Banco Consorcio', 77);
INSERT INTO bank (name, country_id) VALUES ('Banco Paris', 77);
INSERT INTO bank (name, country_id) VALUES ('Penta', 77);

INSERT INTO direct_payment_bank (origin_bank_id, destiny_bank_id, destiny_bank_code) VALUES (186, 175, 1);
INSERT INTO direct_payment_bank (origin_bank_id, destiny_bank_id, destiny_bank_code) VALUES (186, 176, 9);
INSERT INTO direct_payment_bank (origin_bank_id, destiny_bank_id, destiny_bank_code) VALUES (186, 177, 12);
INSERT INTO direct_payment_bank (origin_bank_id, destiny_bank_id, destiny_bank_code) VALUES (186, 178, 14);
INSERT INTO direct_payment_bank (origin_bank_id, destiny_bank_id, destiny_bank_code) VALUES (186, 179, 16);
INSERT INTO direct_payment_bank (origin_bank_id, destiny_bank_id, destiny_bank_code) VALUES (186, 180, 27);
INSERT INTO direct_payment_bank (origin_bank_id, destiny_bank_id, destiny_bank_code) VALUES (186, 181, 28);
INSERT INTO direct_payment_bank (origin_bank_id, destiny_bank_id, destiny_bank_code) VALUES (186, 182, 31);
INSERT INTO direct_payment_bank (origin_bank_id, destiny_bank_id, destiny_bank_code) VALUES (186, 183, 37);
INSERT INTO direct_payment_bank (origin_bank_id, destiny_bank_id, destiny_bank_code) VALUES (186, 184, 39);
INSERT INTO direct_payment_bank (origin_bank_id, destiny_bank_id, destiny_bank_code) VALUES (186, 185, 46);
INSERT INTO direct_payment_bank (origin_bank_id, destiny_bank_id, destiny_bank_code) VALUES (186, 186, 49);
INSERT INTO direct_payment_bank (origin_bank_id, destiny_bank_id, destiny_bank_code) VALUES (186, 187, 51);
INSERT INTO direct_payment_bank (origin_bank_id, destiny_bank_id, destiny_bank_code) VALUES (186, 188, 54);
INSERT INTO direct_payment_bank (origin_bank_id, destiny_bank_id, destiny_bank_code) VALUES (186, 189, 504);
INSERT INTO direct_payment_bank (origin_bank_id, destiny_bank_id, destiny_bank_code) VALUES (186, 190, 507);
INSERT INTO direct_payment_bank (origin_bank_id, destiny_bank_id, destiny_bank_code) VALUES (186, 191, 672);
INSERT INTO direct_payment_bank (origin_bank_id, destiny_bank_id, destiny_bank_code) VALUES (186, 192, 53);
INSERT INTO direct_payment_bank (origin_bank_id, destiny_bank_id, destiny_bank_code) VALUES (186, 193, 55);
INSERT INTO direct_payment_bank (origin_bank_id, destiny_bank_id, destiny_bank_code) VALUES (186, 194, 57);
INSERT INTO direct_payment_bank (origin_bank_id, destiny_bank_id, destiny_bank_code) VALUES (186, 195, 56);

INSERT INTO bank_account(account_number, bank_commission, fx_commission, recharge_cost, country_id, bank_id) 
VALUES('Bank Security', 0.02, 0.015, 2, 77, 186);

## -- Inserting Chile cities

CREATE TABLE `comunas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comuna` varchar(64) NOT NULL,
  `provincia_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=346 DEFAULT CHARSET=utf8;

INSERT INTO `comunas` (`id`,`comuna`,`provincia_id`)
VALUES
  (1,'Arica',1),
  (2,'Camarones',1),
  (3,'General Lagos',2),
  (4,'Putre',2),
  (5,'Alto Hospicio',3),
  (6,'Iquique',3),
  (7,'Camiña',4),
  (8,'Colchane',4),
  (9,'Huara',4),
  (10,'Pica',4),
  (11,'Pozo Almonte',4),
    (12,'Tocopilla',5),
    (13,'María Elena',5),
  (14,'Calama',6),
  (15,'Ollague',6),
  (16,'San Pedro de Atacama',6),
    (17,'Antofagasta',7),
  (18,'Mejillones',7),
  (19,'Sierra Gorda',7),
  (20,'Taltal',7),
  (21,'Chañaral',8),
  (22,'Diego de Almagro',8),
    (23,'Copiapó',9),
  (24,'Caldera',9),
  (25,'Tierra Amarilla',9),
    (26,'Vallenar',10),
  (27,'Alto del Carmen',10),
  (28,'Freirina',10),
  (29,'Huasco',10),
  (30,'La Serena',11),
    (31,'Coquimbo',11),
    (32,'Andacollo',11),
    (33,'La Higuera',11),
    (34,'Paihuano',11),
  (35,'Vicuña',11),
  (36,'Ovalle',12),
    (37,'Combarbalá',12),
    (38,'Monte Patria',12),
    (39,'Punitaqui',12),
  (40,'Río Hurtado',12),
  (41,'Illapel',13),
  (42,'Canela',13),
  (43,'Los Vilos',13),
  (44,'Salamanca',13),
  (45,'La Ligua',14),
    (46,'Cabildo',14),
  (47,'Zapallar',14),
    (48,'Papudo',14),
  (49,'Petorca',14),
  (50,'Los Andes',15),
  (51,'San Esteban',15),
    (52,'Calle Larga',15),
    (53,'Rinconada',15),
  (54,'San Felipe',16),
    (55,'Llaillay',16),
    (56,'Putaendo',16),
  (57,'Santa María',16),
  (58,'Catemu',16),
  (59,'Panquehue',16),
    (60,'Quillota',17),
    (61,'La Cruz',17),
  (62,'La Calera',17),
  (63,'Nogales',17),
    (64,'Hijuelas',17),
  (65,'Valparaíso',18), 
    (66,'Viña del Mar',18),
  (67,'Concón',18),
  (68,'Quintero',18),
    (69,'Puchuncaví',18),
  (70,'Casablanca',18),
  (71,'Juan Fernández',18),
  (72,'San Antonio',19),
    (73,'Cartagena',19),
  (74,'El Tabo',19),
  (75,'El Quisco',19),
  (76,'Algarrobo',19),
  (77,'Santo Domingo',19),
  (78,'Isla de Pascua',20),
  (79,'Quilpué',21),
  (80,'Limache',21),
  (81,'Olmué',21),
  (82,'Villa Alemana',21),
  (83,'Colina',22),
  (84,'Lampa',22),
  (85,'Tiltil',22),
  (86,'Santiago',23),
  (87,'Vitacura',23),
    (88,'San Ramón',23),
  (89,'San Miguel',23),
  (90,'San Joaquín',23),
    (91,'Renca',23),
  (92,'Recoleta',23),
    (93,'Quinta Normal',23),
  (94,'Quilicura',23),
    (95,'Pudahuel',23),
    (96,'Providencia',23),
  (97,'Peñalolén',23),
    (98,'Pedro Aguirre Cerda',23),
  (99,'Ñuñoa',23),
  (100,'Maipú',23),
  (101,'Macul',23),
  (102,'Lo Prado',23),
  (103,'Lo Espejo',23),
  (104,'Lo Barnechea',23),
  (105,'Las Condes',23),
  (106,'La Reina',23),
  (107,'La Pintana',23),
  (108,'La Granja',23),
  (109,'La Florida',23),
    (110,'La Cisterna',23),
    (111,'Independencia',23),
    (112,'Huechuraba',23),
  (113,'Estación Central',23),
    (114,'El Bosque',23),
    (115,'Conchalí',23),
    (116,'Cerro Navia',23),
    (117,'Cerrillos',23),
  (118,'Puente Alto',24),
  (119,'San José de Maipo',24),
    (120,'Pirque',24),
  (121,'San Bernardo',25),
  (122,'Buin',25),
    (123,'Paine',25),
  (124,'Calera de Tango',25),
  (125,'Melipilla',26),
  (126,'Alhué',26),
  (127,'Curacaví',26),
  (128,'María Pinto',26),
  (129,'San Pedro',26),
  (130,'Isla de Maipo',27),
    (131,'El Monte',27),
  (132,'Padre Hurtado',27),
  (133,'Peñaflor',27),
  (134,'Talagante',27),
  (135,'Codegua',28),
  (136,'Coínco',28),
  (137,'Coltauco',28),
  (138,'Doñihue',28),
  (139,'Graneros',28),
  (140,'Las Cabras',28),
  (141,'Machalí',28),
  (142,'Malloa',28),
  (143,'Mostazal',28),
  (144,'Olivar',28),
  (145,'Peumo',28),
  (146,'Pichidegua',28),
  (147,'Quinta de Tilcoco',28),
  (148,'Rancagua',28),
  (149,'Rengo',28),
  (150,'Requínoa',28),
  (151,'San Vicente de Tagua Tagua',28),
  (152,'Chépica',29),
  (153,'Chimbarongo',29),
  (154,'Lolol',29),
    (155,'Nancagua',29),
    (156,'Palmilla',29),
    (157,'Peralillo',29),
  (158,'Placilla',29),
  (159,'Pumanque',29),
  (160,'San Fernando',29),
  (161,'Santa Cruz',29),
  (162,'La Estrella',30),
  (163,'Litueche',30),
  (164,'Marchigüe',30),
  (165,'Navidad',30),
  (166,'Paredones',30),
  (167,'Pichilemu',30),
  (168,'Curicó',31),
  (169,'Hualañé',31),
  (170,'Licantén',31),
  (171,'Molina',31),
  (172,'Rauco',31),
  (173,'Romeral',31),
  (174,'Sagrada Familia',31),
  (175,'Teno',31),
  (176,'Vichuquén',31),
  (177,'Talca',32),
  (178,'San Clemente',32),
  (179,'Pelarco',32),
  (180,'Pencahue',32),
  (181,'Maule',32),
  (182,'San Rafael',32),
  (183,'Curepto',33),
  (184,'Constitución',32),
  (185,'Empedrado',32),
  (186,'Río Claro',32),
    (187,'Linares',33),
  (188,'San Javier',33),
  (189,'Parral',33),
  (190,'Villa Alegre',33),
  (191,'Longaví',33),
  (192,'Colbún',33),
  (193,'Retiro',33),
  (194,'Yerbas Buenas',33),
    (195,'Cauquenes',34),
  (196,'Chanco',34),
  (197,'Pelluhue',34),
  (198,'Bulnes',35),
  (199,'Chillán',35),
  (200,'Chillán Viejo',35),
  (201,'El Carmen',35),
  (202,'Pemuco',35),
  (203,'Pinto',35),
  (204,'Quillón',35),
  (205,'San Ignacio',35),
  (206,'Yungay',35),
  (207,'Cobquecura',36),
  (208,'Coelemu',36),
  (209,'Ninhue',36),
  (210,'Portezuelo',36),
  (211,'Quirihue',36),
  (212,'Ránquil',36),
  (213,'Treguaco',36),
  (214,'San Carlos',37),
  (215,'Coihueco',37),
  (216,'San Nicolás',37),
  (217,'Ñiquén',37),
  (218,'San Fabián',37),
  (219,'Alto Biobío',38),
  (220,'Antuco',38),
  (221,'Cabrero',38),
  (222,'Laja',38),
  (223,'Los Ángeles',38),
  (224,'Mulchén',38),
  (225,'Nacimiento',38),
  (226,'Negrete',38),
  (227,'Quilaco',38),
  (228,'Quilleco',38),
  (229,'San Rosendo',38),
  (230,'Santa Bárbara',38),
  (231,'Tucapel',38),
  (232,'Yumbel',38),
  (233,'Concepción',39),
  (234,'Coronel',39),
  (235,'Chiguayante',39),
  (236,'Florida',39),
  (237,'Hualpén',39),
  (238,'Hualqui',39),
  (239,'Lota',39),
  (240,'Penco',39),
  (241,'San Pedro de La Paz',39),
  (242,'Santa Juana',39),
  (243,'Talcahuano',39),
  (244,'Tomé',39),
  (245,'Arauco',40),
  (246,'Cañete',40),
  (247,'Contulmo',40),
  (248,'Curanilahue',40),
  (249,'Lebu',40),
  (250,'Los Álamos',40),
  (251,'Tirúa',40),
  (252,'Angol',41),
  (253,'Collipulli',41),
  (254,'Curacautín',41),
  (255,'Ercilla',41),
  (256,'Lonquimay',41),
  (257,'Los Sauces',41),
  (258,'Lumaco',41),
  (259,'Purén',41),
  (260,'Renaico',41),
  (261,'Traiguén',41),
  (262,'Victoria',41),
  (263,'Temuco',42),
  (264,'Carahue',42),
  (265,'Cholchol',42),
  (266,'Cunco',42),
  (267,'Curarrehue',42),
  (268,'Freire',42),
  (269,'Galvarino',42),
  (270,'Gorbea',42),
  (271,'Lautaro',42),
  (272,'Loncoche',42),
  (273,'Melipeuco',42),
  (274,'Nueva Imperial',42),
  (275,'Padre Las Casas',42),
  (276,'Perquenco',42),
  (277,'Pitrufquén',42),
  (278,'Pucón',42),
  (279,'Saavedra',42),
  (280,'Teodoro Schmidt',42),
  (281,'Toltén',42),
  (282,'Vilcún',42),
  (283,'Villarrica',42),
  (284,'Valdivia',43),
  (285,'Corral',43),
  (286,'Lanco',43),
  (287,'Los Lagos',43),
  (288,'Máfil',43),
  (289,'Mariquina',43),
  (290,'Paillaco',43),
  (291,'Panguipulli',43),
  (292,'La Unión',44),
  (293,'Futrono',44),
  (294,'Lago Ranco',44),
  (295,'Río Bueno',44),
  (297,'Osorno',45),
  (298,'Puerto Octay',45),
  (299,'Purranque',45),
  (300,'Puyehue',45),
  (301,'Río Negro',45),
  (302,'San Juan de la Costa',45),
  (303,'San Pablo',45),
  (304,'Calbuco',46),
  (305,'Cochamó',46),
  (306,'Fresia',46),
  (307,'Frutillar',46),
  (308,'Llanquihue',46),
  (309,'Los Muermos',46),
  (310,'Maullín',46),
  (311,'Puerto Montt',46),
  (312,'Puerto Varas',46),
  (313,'Ancud',47),
  (314,'Castro',47),
  (315,'Chonchi',47),
  (316,'Curaco de Vélez',47),
  (317,'Dalcahue',47),
  (318,'Puqueldón',47),
  (319,'Queilén',47),
  (320,'Quellón',47),
  (321,'Quemchi',47),
  (322,'Quinchao',47),
  (323,'Chaitén',48),
  (324,'Futaleufú',48),
  (325,'Hualaihué',48),
  (326,'Palena',48),
  (327,'Lago Verde',49),
  (328,'Coihaique',49),
  (329,'Aysén',50),
  (330,'Cisnes',50),
  (331,'Guaitecas',50),
  (332,'Río Ibáñez',51),
  (333,'Chile Chico',51),
  (334,'Cochrane',52),
  (335,'O\'Higgins',52),
  (336,'Tortel',52),
  (337,'Natales',53),
  (338,'Torres del Paine',53),
  (339,'Laguna Blanca',54),
  (340,'Punta Arenas',54),
  (341,'Río Verde',54),
  (342,'San Gregorio',54),
  (343,'Porvenir',55),
  (344,'Primavera',55),
  (345,'Timaukel',55),
  (346,'Cabo de Hornos',56),
  (347,'Antártica',56);

INSERT INTO city (name, country_id) 
SELECT comuna, 77 FROM 
comunas; 

DROP TABLE comunas;

alter table jhi_user modify login varchar(100);

alter table extended_user add column sent_abc boolean default false;

ALTER TABLE transaction ADD COLUMN must_notify_payee boolean default true; 

##-- PMIT-517_create_service_to_get_the_transaction_rate_of_remitee
INSERT INTO `pagomundo`.`bank_account` (`account_number`, `bank_commission`, `fx_commission`, `recharge_cost`, `country_id`, `bank_id`) VALUES ('Ripple Chile Remitee', '0', '0', '0', '77', '197');
INSERT INTO `pagomundo`.`bank_account` (`account_number`, `bank_commission`, `fx_commission`, `recharge_cost`, `country_id`, `bank_id`) VALUES ('Ripple Argentina Remitee', '0', '0', '0', '66', '199');
INSERT INTO `pagomundo`.`bank_account` (`account_number`, `bank_commission`, `fx_commission`, `recharge_cost`, `country_id`, `bank_id`) VALUES ('Ripple Brasil Remitee', '0', '0', '0', '74', '198');

## Peru Integration

update country set for_payee = true where id = 101;

update country set currency_name = 'Peruvian Sol', currency_code='PEN', currency_value='3.99', currency_base='USD', currency_last_updated='2021-11-01' where id =  101;

insert into id_type(name, country_id, natural_or_legal) values('NIDN', 101, 'BOTH');

INSERT INTO `pagomundo`.`bank` (`name`, `country_id`) VALUES ('BCP', 101);
INSERT INTO `pagomundo`.`bank` (`name`, `country_id`) VALUES ('BBVA', 101);
INSERT INTO `pagomundo`.`bank` (`name`, `country_id`) VALUES ('Interbank', 101);
INSERT INTO `pagomundo`.`bank` (`name`, `country_id`) VALUES ('Scotiabank', 101);
INSERT INTO `pagomundo`.`bank` (`name`, `country_id`) VALUES ('Ripple Peru', 101);

insert into direct_payment_bank(origin_bank_id, destiny_bank_id) values(204, 200);
insert into direct_payment_bank(origin_bank_id, destiny_bank_id) values(204, 201);
insert into direct_payment_bank(origin_bank_id, destiny_bank_id) values(204, 202);
insert into direct_payment_bank(origin_bank_id, destiny_bank_id) values(204, 203);

insert into bank_account(account_number, country_id, bank_id) values('Ripple Peru', 101, 204);

insert into city(name, country_id) values('Lima', 101);
insert into city(name, country_id) values('Callao', 101);
insert into city(name, country_id) values('Arequipa', 101);
insert into city(name, country_id) values('Trujillo', 101);
insert into city(name, country_id) values('Chiclayo', 101);
insert into city(name, country_id) values('Chimbote', 101);
insert into city(name, country_id) values('Piura', 101);
insert into city(name, country_id) values('Cusco', 101);
insert into city(name, country_id) values('Huancayo', 101);
insert into city(name, country_id) values('Iquitos', 101);
insert into city(name, country_id) values('Pucallpa', 101);
insert into city(name, country_id) values('Juliaca', 101);
insert into city(name, country_id) values('Tacna', 101);
insert into city(name, country_id) values('Ica', 101);
insert into city(name, country_id) values('Tarapoto', 101);
insert into city(name, country_id) values('Huacho', 101);
insert into city(name, country_id) values('Cajamarca', 101);
insert into city(name, country_id) values('Huanuco', 101);
insert into city(name, country_id) values('Ayacucho', 101);
insert into city(name, country_id) values('Paita', 101);
insert into city(name, country_id) values('Sullana', 101);
insert into city(name, country_id) values('Huaycan', 101);
insert into city(name, country_id) values('Barranca', 101);
insert into city(name, country_id) values('Puno', 101);
insert into city(name, country_id) values('Huaraz', 101);
insert into city(name, country_id) values('Pisco', 101);
insert into city(name, country_id) values('Talara', 101);
insert into city(name, country_id) values('Tumbes', 101);
insert into city(name, country_id) values('Puerto Maldonado', 101);
insert into city(name, country_id) values('Abancay', 101);
insert into city(name, country_id) values('Moquegua', 101);
insert into city(name, country_id) values('Cerro de Pasco', 101);
insert into city(name, country_id) values('Moyobamba', 101);
insert into city(name, country_id) values('Huancavelica', 101);
insert into city(name, country_id) values('Chachapoyas', 101);
insert into city(name, country_id) values('Chosica', 101);
insert into city(name, country_id) values('Andahuaylas', 101);
insert into city(name, country_id) values('Huaral', 101);
insert into city(name, country_id) values('Chulucanas', 101);
insert into city(name, country_id) values('Ilo', 101);
insert into city(name, country_id) values('Chincha Alta', 101);
insert into city(name, country_id) values('Yurimaguas', 101);
insert into city(name, country_id) values('Lambayeque', 101);
insert into city(name, country_id) values('Sicuani', 101);
insert into city(name, country_id) values('Chota', 101);
insert into city(name, country_id) values('Tarma', 101);
insert into city(name, country_id) values('Jaen', 101);
insert into city(name, country_id) values('Tingo Maria', 101);
insert into city(name, country_id) values('Bagua Grande', 101);
insert into city(name, country_id) values('Huaura', 101);
insert into city(name, country_id) values('Juanjui', 101);
insert into city(name, country_id) values('Viru', 101);
insert into city(name, country_id) values('Chancay', 101);
insert into city(name, country_id) values('Forbe Oroya', 101);
insert into city(name, country_id) values('Ferrenafe', 101);
insert into city(name, country_id) values('Sechura', 101);
insert into city(name, country_id) values('Pativilca', 101);
insert into city(name, country_id) values('Huamachuco', 101);
insert into city(name, country_id) values('Huanta', 101);
insert into city(name, country_id) values('Casma', 101);
insert into city(name, country_id) values('Tocache Nuevo', 101);
insert into city(name, country_id) values('Jauja', 101);
insert into city(name, country_id) values('Mollendo', 101);
insert into city(name, country_id) values('Pacasmayo', 101);
insert into city(name, country_id) values('San Vicente de Canete', 101);
insert into city(name, country_id) values('Otuzco', 101);
insert into city(name, country_id) values('Santo Tomas', 101);
insert into city(name, country_id) values('Caras', 101);
insert into city(name, country_id) values('Nazca', 101);
insert into city(name, country_id) values('Camana', 101);
insert into city(name, country_id) values('Juli', 101);
insert into city(name, country_id) values('Ayaviri', 101);
insert into city(name, country_id) values('Contamana', 101);
insert into city(name, country_id) values('Requena', 101);
insert into city(name, country_id) values('Zarumilla', 101);
insert into city(name, country_id) values('San Pedro de Lloc', 101);
insert into city(name, country_id) values('Puerto Pimentel', 101);
insert into city(name, country_id) values('Huarmey', 101);
insert into city(name, country_id) values('Ilave', 101);
insert into city(name, country_id) values('Puquio', 101);
insert into city(name, country_id) values('Junin', 101);
insert into city(name, country_id) values('San Ramon', 101);
insert into city(name, country_id) values('Lamas', 101);
insert into city(name, country_id) values('Motupe', 101);
insert into city(name, country_id) values('Cajabamba', 101);
insert into city(name, country_id) values('Chilca', 101);
insert into city(name, country_id) values('Oxapampa', 101);
insert into city(name, country_id) values('Santiago', 101);
insert into city(name, country_id) values('Salaverry', 101);
insert into city(name, country_id) values('Satipo', 101);
insert into city(name, country_id) values('Yungay', 101);
insert into city(name, country_id) values('Coracora', 101);
insert into city(name, country_id) values('Desaguadero', 101);
insert into city(name, country_id) values('Caballococha', 101);
insert into city(name, country_id) values('Urubamba', 101);
insert into city(name, country_id) values('Nauta', 101);
insert into city(name, country_id) values('Olmos', 101);
insert into city(name, country_id) values('Putina', 101);
insert into city(name, country_id) values('Tournavista', 101);
insert into city(name, country_id) values('Gueppi', 101);
insert into city(name, country_id) values('Rocafuerte', 101);
insert into city(name, country_id) values('Andoas', 101);
insert into city(name, country_id) values('Puca Urco', 101);
insert into city(name, country_id) values('Soldado Bartra', 101);

##Remitee Integration
ALTER TABLE `pagomundo`.`bank_account` 
ADD COLUMN `is_remitee` TINYINT(1) NULL DEFAULT 0 AFTER `bank_id`;

UPDATE `pagomundo`.`bank_account` SET `is_remitee` = '1' WHERE (`account_number` = '15');
UPDATE `pagomundo`.`bank_account` SET `is_remitee` = '1' WHERE (`account_number` = '16');
UPDATE `pagomundo`.`bank_account` SET `is_remitee` = '1' WHERE (`account_number` = '17');
UPDATE `pagomundo`.`bank_account` SET `is_remitee` = '1' WHERE (`account_number` = '18');


ALTER TABLE `pagomundo`.`direct_payment_bank` 
ADD COLUMN `destiny_bank_bicfi` VARCHAR(50) NULL AFTER `department_code`;

UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BSCHCLRM") WHERE destiny_bank_id =183;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BKSACLRM") WHERE destiny_bank_id =178;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BICECLRM") WHERE destiny_bank_id =181;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BICHCLRM") WHERE destiny_bank_id =176;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("ITAUCLRM") WHERE destiny_bank_id =184;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("CONBCLRM") WHERE destiny_bank_id =180;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("FALACLRM") WHERE destiny_bank_id =187;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BSCLCLRM") WHERE destiny_bank_id =186;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("RPLYCLRM") WHERE destiny_bank_id =192;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BCNPCLRM") WHERE destiny_bank_id =194;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("MNEXCLRM") WHERE destiny_bank_id =193;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000654") WHERE destiny_bank_id =33;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("ABCBBRSP") WHERE destiny_bank_id =34;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("ABNABRSP") WHERE destiny_bank_id =35;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000121") WHERE destiny_bank_id =36;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("AUFABRSP") WHERE destiny_bank_id =37;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000065") WHERE destiny_bank_id =38;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("ARBBBRS1") WHERE destiny_bank_id =39;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BBMFBRSP") WHERE destiny_bank_id =40;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000024") WHERE destiny_bank_id =41;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000330") WHERE destiny_bank_id =42;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BMBCBRSP") WHERE destiny_bank_id =43;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BNPABRSP") WHERE destiny_bank_id =44;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BBINBRR1") WHERE destiny_bank_id =45;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000218") WHERE destiny_bank_id =46;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000063") WHERE destiny_bank_id =47;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000036") WHERE destiny_bank_id =48;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000122") WHERE destiny_bank_id =49;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000394") WHERE destiny_bank_id =50;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BBDEBRSP") WHERE destiny_bank_id =51;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BPABBRRJ") WHERE destiny_bank_id =52;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("CSIXBRSP") WHERE destiny_bank_id =53;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("CGDIBRSP") WHERE destiny_bank_id =54;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000412") WHERE destiny_bank_id =55;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BCBZBRSP") WHERE destiny_bank_id =56;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000266") WHERE destiny_bank_id =57;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000739") WHERE destiny_bank_id =58;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000233") WHERE destiny_bank_id =59;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("CITIBRBR") WHERE destiny_bank_id =60;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000241") WHERE destiny_bank_id =61;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("CPBNBRDF") WHERE destiny_bank_id =62;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BCSIBRRS") WHERE destiny_bank_id =63;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BAGBBRSP") WHERE destiny_bank_id =64;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BIGABRRJ") WHERE destiny_bank_id =65;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BPBMBRSP") WHERE destiny_bank_id =66;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("AMABBRAB") WHERE destiny_bank_id =67;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BKCHBRSP") WHERE destiny_bank_id =68;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("DAYCBRSP") WHERE destiny_bank_id =69;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BRBSBRDF") WHERE destiny_bank_id =70;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("NACNBRSP") WHERE destiny_bank_id =71;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000335") WHERE destiny_bank_id =72;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BRASBRRJ") WHERE destiny_bank_id =73;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("SEEBBRR1") WHERE destiny_bank_id =74;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BEPABRAB") WHERE destiny_bank_id =75;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BRGSBRRS") WHERE destiny_bank_id =76;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BNBRBRCF") WHERE destiny_bank_id =77;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BFATBRS1") WHERE destiny_bank_id =78;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("FIBIBRSP") WHERE destiny_bank_id =79;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000626") WHERE destiny_bank_id =80;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000094") WHERE destiny_bank_id =81;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000612") WHERE destiny_bank_id =82;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BNIIBRSP") WHERE destiny_bank_id =83;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BIBRBRSP") WHERE destiny_bank_id =84;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("IDVLBRSP") WHERE destiny_bank_id =85;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("ITEMBRSP") WHERE destiny_bank_id =86;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000249") WHERE destiny_bank_id =87;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("SAFRBRSP") WHERE destiny_bank_id =91;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("CHASBRSP") WHERE destiny_bank_id =92;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000217") WHERE destiny_bank_id =93;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("KODBBRSP") WHERE destiny_bank_id =94;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("KOEXBRSP") WHERE destiny_bank_id =95;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BLBCBRSP") WHERE destiny_bank_id =96;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BMAXBRRJ") WHERE destiny_bank_id =97;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BMBRBRRB") WHERE destiny_bank_id =98;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("MHCBBRSP") WHERE destiny_bank_id =99;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("MODABRRJ") WHERE destiny_bank_id =100;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("MSDWBRSP") WHERE destiny_bank_id =101;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BOTKBRSX") WHERE destiny_bank_id =102;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("NACDBRRJ") WHERE destiny_bank_id =103;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000735") WHERE destiny_bank_id =104;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000169") WHERE destiny_bank_id =105;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BORIBRSP") WHERE destiny_bank_id =106;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000079") WHERE destiny_bank_id =107;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("OURIBRSP") WHERE destiny_bank_id =108;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000623") WHERE destiny_bank_id =109;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("PAULBRSP") WHERE destiny_bank_id =110;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("PBBRBRSP") WHERE destiny_bank_id =111;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("RABOBRSP") WHERE destiny_bank_id =112;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("RENDBRSP") WHERE destiny_bank_id =113;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BRPOBRS1") WHERE destiny_bank_id =114;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000120") WHERE destiny_bank_id =115;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("SAFRBRSP") WHERE destiny_bank_id =116;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BSCHBRSP") WHERE destiny_bank_id =117;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000743") WHERE destiny_bank_id =118;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000754") WHERE destiny_bank_id =119;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("SMRTBRSP") WHERE destiny_bank_id =120;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BSOGBRSP") WHERE destiny_bank_id =121;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BSBSBRSP") WHERE destiny_bank_id =122;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("SUBRBRSP") WHERE destiny_bank_id =123;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("TOPZBRRS") WHERE destiny_bank_id =124;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000634") WHERE destiny_bank_id =125;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000018") WHERE destiny_bank_id =126;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BAVOBRSP") WHERE destiny_bank_id =127;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000610") WHERE destiny_bank_id =128;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000119") WHERE destiny_bank_id =129;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("HVBKBRSP") WHERE destiny_bank_id =130;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BCXPBRSP") WHERE destiny_bank_id =131;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000081") WHERE destiny_bank_id =132;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BEESBRRJ") WHERE destiny_bank_id =133;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("MLBOBRSP") WHERE destiny_bank_id =134;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000250") WHERE destiny_bank_id =135;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BEXSBRSP") WHERE destiny_bank_id =136;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BEXSBRSP") WHERE destiny_bank_id =137;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BNYMBRRJ") WHERE destiny_bank_id =138;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000301") WHERE destiny_bank_id =139;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("PACVBRS1") WHERE destiny_bank_id =140;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("CEFXBRSP") WHERE destiny_bank_id =141;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BICBBRSP") WHERE destiny_bank_id =142;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("CITIBRSX") WHERE destiny_bank_id =143;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("COBABRSP") WHERE destiny_bank_id =144;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000136") WHERE destiny_bank_id =145;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000085") WHERE destiny_bank_id =146;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("CCCRBRR1") WHERE destiny_bank_id =147;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("HEDGBRS1") WHERE destiny_bank_id =148;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("DEUTBRSP") WHERE destiny_bank_id =149;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("GOLDBRSP") WHERE destiny_bank_id =150;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BESBBRSP") WHERE destiny_bank_id =151;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000062") WHERE destiny_bank_id =152;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000399") WHERE destiny_bank_id =153;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("HSBNBRSS") WHERE destiny_bank_id =154;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("ICBKBRSP") WHERE destiny_bank_id =155;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("INGBBRSP") WHERE destiny_bank_id =156;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BCITBRSP") WHERE destiny_bank_id =157;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("ITAUBRSP") WHERE destiny_bank_id =159;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000488") WHERE destiny_bank_id =160;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("MCBMBRSP") WHERE destiny_bank_id =161;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("MSBOBRPR") WHERE destiny_bank_id =162;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("COMEBRRS") WHERE destiny_bank_id =163;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000260") WHERE destiny_bank_id =164;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000613") WHERE destiny_bank_id =165;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000290") WHERE destiny_bank_id =166;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("PARABRPR") WHERE destiny_bank_id =167;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("PLURBRRJ") WHERE destiny_bank_id =168;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("NOSCBRSP") WHERE destiny_bank_id =169;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000014") WHERE destiny_bank_id =170;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("BKCOBRSP") WHERE destiny_bank_id =171;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("UBSWBRSP") WHERE destiny_bank_id =172;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LICTBRS1") WHERE destiny_bank_id =173;
UPDATE direct_payment_bank set destiny_bank_bicfi = TRIM("LP000084") WHERE destiny_bank_id =174;

#---PMIPA-185 Handle the id countries dinamically when we added a new one
ALTER TABLE `pagomundo`.`country` 
ADD COLUMN `lang_key` VARCHAR(5) NULL AFTER `currency_last_updated`,
ADD COLUMN `phone_code` VARCHAR(5) NULL AFTER `lang_key`;

UPDATE `pagomundo`.`country` SET `lang_key` = 'es', `phone_code` = '+57' WHERE (`id` = '1');
UPDATE `pagomundo`.`country` SET `lang_key` = 'es', `phone_code` = '+52' WHERE (`id` = '2');
UPDATE `pagomundo`.`country` SET `lang_key` = 'es', `phone_code` = '+54' WHERE (`id` = '66');
UPDATE `pagomundo`.`country` SET `lang_key` = 'pt', `phone_code` = '+55' WHERE (`id` = '74');
UPDATE `pagomundo`.`country` SET `lang_key` = 'es', `phone_code` = '+56' WHERE (`id` = '77');
UPDATE `pagomundo`.`country` SET `lang_key` = 'es', `phone_code` = '+51' WHERE (`id` = '101');

##--- Venezuela Integration

update country set for_payee = true where id = 115;

update country set currency_name = 'Bolivar', currency_code='VES', currency_value='4.54', currency_base='USD', currency_last_updated='2021-11-24' where id =  115;

insert into id_type(name, country_id, natural_or_legal) values('NIDN', 115, 'BOTH');

INSERT INTO `pagomundo`.`bank` (`name`, `country_id`) VALUES ('Banco de Venezuela', 115);
INSERT INTO `pagomundo`.`bank` (`name`, `country_id`) VALUES ('Banco Mercantil', 115);
INSERT INTO `pagomundo`.`bank` (`name`, `country_id`) VALUES ('Banco Provincial', 115);
INSERT INTO `pagomundo`.`bank` (`name`, `country_id`) VALUES ('Banesco', 115);
INSERT INTO `pagomundo`.`bank` (`name`, `country_id`) VALUES ('BOD', 115);
INSERT INTO `pagomundo`.`bank` (`name`, `country_id`) VALUES ('Banco Bicentenario', 115);
INSERT INTO `pagomundo`.`bank` (`name`, `country_id`) VALUES ('BanCaribe', 115);
INSERT INTO `pagomundo`.`bank` (`name`, `country_id`) VALUES ('Banco del Tesoro', 115);
INSERT INTO `pagomundo`.`bank` (`name`, `country_id`) VALUES ('Bancamiga', 115);
INSERT INTO `pagomundo`.`bank` (`name`, `country_id`) VALUES ('Banco Activo', 115);
INSERT INTO `pagomundo`.`bank` (`name`, `country_id`) VALUES ('Ripple Venezuela Remitee', 115);

insert into direct_payment_bank(origin_bank_id, destiny_bank_id) values(215, 205);
insert into direct_payment_bank(origin_bank_id, destiny_bank_id) values(215, 206);
insert into direct_payment_bank(origin_bank_id, destiny_bank_id) values(215, 207);
insert into direct_payment_bank(origin_bank_id, destiny_bank_id) values(215, 208);
insert into direct_payment_bank(origin_bank_id, destiny_bank_id) values(215, 209);
insert into direct_payment_bank(origin_bank_id, destiny_bank_id) values(215, 210);
insert into direct_payment_bank(origin_bank_id, destiny_bank_id) values(215, 211);
insert into direct_payment_bank(origin_bank_id, destiny_bank_id) values(215, 212);
insert into direct_payment_bank(origin_bank_id, destiny_bank_id) values(215, 213);
insert into direct_payment_bank(origin_bank_id, destiny_bank_id) values(215, 214);

insert into bank_account(account_number, country_id, bank_id) values('Ripple Venezuela Remitee', 115, 215);

insert into city(name, country_id) values('Libertador', 115);
insert into city(name, country_id) values('Autonomo Alto Orinoco', 115);
insert into city(name, country_id) values('Autonomo Atabapo', 115);
insert into city(name, country_id) values('Autonomo Atures', 115);
insert into city(name, country_id) values('Autonomo Autana', 115);
insert into city(name, country_id) values('Autonomo Maroa', 115);
insert into city(name, country_id) values('Autonomo Manapiare', 115);
insert into city(name, country_id) values('Autonomo Rio Negro', 115);
insert into city(name, country_id) values('Anaco', 115);
insert into city(name, country_id) values('Aragua', 115);
insert into city(name, country_id) values('Fernando De Pe&#209', 115);
insert into city(name, country_id) values('Francisco Del Carmen Carvajal', 115);
insert into city(name, country_id) values('Francisco De Miranda', 115);
insert into city(name, country_id) values('Guanta', 115);
insert into city(name, country_id) values('Independencia', 115);
insert into city(name, country_id) values('Juan Antonio Sotillo', 115);
insert into city(name, country_id) values('Juan Manuel Cajigal', 115);
insert into city(name, country_id) values('Jose Gregorio Monagas', 115);
insert into city(name, country_id) values('Libertad', 115);
insert into city(name, country_id) values('Manuel Ezequiel Bruzual', 115);
insert into city(name, country_id) values('Pedro Maria Freites', 115);
insert into city(name, country_id) values('Piritu', 115);
insert into city(name, country_id) values('San Jose De Guanipa', 115);
insert into city(name, country_id) values('San Juan De Capistrano', 115);
insert into city(name, country_id) values('Santa Ana', 115);
insert into city(name, country_id) values('Simon Bolivar', 115);
insert into city(name, country_id) values('Simon Rodriguez', 115);
insert into city(name, country_id) values('Sir Arthur Mc Gregor', 115);
insert into city(name, country_id) values('Turist Diego Bautista Urbaneja', 115);
insert into city(name, country_id) values('Achaguas', 115);
insert into city(name, country_id) values('Biruaca', 115);
insert into city(name, country_id) values('Mu&ntilde', 115);
insert into city(name, country_id) values('Paez', 115);
insert into city(name, country_id) values('Pedro Camejo', 115);
insert into city(name, country_id) values('Romulo Gallegos', 115);
insert into city(name, country_id) values('San Fernando', 115);
insert into city(name, country_id) values('Bolivar', 115);
insert into city(name, country_id) values('Jose Gregorio Monagas', 115);
insert into city(name, country_id) values('Girardot', 115);
insert into city(name, country_id) values('Jose Angel Lamas', 115);
insert into city(name, country_id) values('Jose Felix Ribas', 115);
insert into city(name, country_id) values('Jose Rafael Revenga', 115);
insert into city(name, country_id) values('Libertador', 115);
insert into city(name, country_id) values('Mario Brice&ntilde', 115);
insert into city(name, country_id) values('San Casimiro', 115);
insert into city(name, country_id) values('San Sebastian', 115);
insert into city(name, country_id) values('Santiago Mari&ntilde', 115);
insert into city(name, country_id) values('Santos Michelena', 115);
insert into city(name, country_id) values('Sucre', 115);
insert into city(name, country_id) values('Tovar', 115);
insert into city(name, country_id) values('Urdaneta', 115);
insert into city(name, country_id) values('Zamora', 115);
insert into city(name, country_id) values('Francisco Linares Alcantara', 115);
insert into city(name, country_id) values('Ocumare De La Costa De Oro', 115);
insert into city(name, country_id) values('Alberto Arvelo Torrealba', 115);
insert into city(name, country_id) values('Antonio Jose De Sucre', 115);
insert into city(name, country_id) values('Arismendi', 115);
insert into city(name, country_id) values('Barinas', 115);
insert into city(name, country_id) values('Bolivar', 115);
insert into city(name, country_id) values('Cruz Paredes', 115);
insert into city(name, country_id) values('Ezequiel Zamora', 115);
insert into city(name, country_id) values('Obispos', 115);
insert into city(name, country_id) values('Pedraza', 115);
insert into city(name, country_id) values('Rojas', 115);
insert into city(name, country_id) values('Sosa', 115);
insert into city(name, country_id) values('Andres Eloy Blanco', 115);
insert into city(name, country_id) values('Caroni', 115);
insert into city(name, country_id) values('Cede&#209', 115);
insert into city(name, country_id) values('El Callao', 115);
insert into city(name, country_id) values('Gran Sabana', 115);
insert into city(name, country_id) values('Heres', 115);
insert into city(name, country_id) values('Piar', 115);
insert into city(name, country_id) values('Raul Leoni', 115);
insert into city(name, country_id) values('Roscio', 115);
insert into city(name, country_id) values('Sifontes', 115);
insert into city(name, country_id) values('Sucre', 115);
insert into city(name, country_id) values('Padre Pedro Chien', 115);
insert into city(name, country_id) values('Bejuma', 115);
insert into city(name, country_id) values('Bolivar', 115);
insert into city(name, country_id) values('Diego Ibarra', 115);
insert into city(name, country_id) values('Guacara', 115);
insert into city(name, country_id) values('Juan Jose Mora', 115);
insert into city(name, country_id) values('Libertador', 115);
insert into city(name, country_id) values('Los Guayos', 115);
insert into city(name, country_id) values('Miranda', 115);
insert into city(name, country_id) values('Montalban', 115);
insert into city(name, country_id) values('Naguanagua', 115);
insert into city(name, country_id) values('Puerto Cabello', 115);
insert into city(name, country_id) values('San Diego', 115);
insert into city(name, country_id) values('San Joaquin', 115);
insert into city(name, country_id) values('Valencia', 115);
insert into city(name, country_id) values('Anzoategui', 115);
insert into city(name, country_id) values('Raul Leoni', 115);
insert into city(name, country_id) values('Girardot', 115);
insert into city(name, country_id) values('Lima Blanco', 115);
insert into city(name, country_id) values('Pao De San Juan Bautista', 115);
insert into city(name, country_id) values('Ricaurte', 115);
insert into city(name, country_id) values('Romulo Gallegos', 115);
insert into city(name, country_id) values('San Carlos', 115);
insert into city(name, country_id) values('Tinaco', 115);
insert into city(name, country_id) values('Antonio Diaz', 115);
insert into city(name, country_id) values('Casacoima', 115);
insert into city(name, country_id) values('Pedernales', 115);
insert into city(name, country_id) values('Tucupita', 115);
insert into city(name, country_id) values('Acosta', 115);
insert into city(name, country_id) values('Bolivar', 115);
insert into city(name, country_id) values('Buchivacoa', 115);
insert into city(name, country_id) values('Cacique Manaure', 115);
insert into city(name, country_id) values('Carirubana', 115);
insert into city(name, country_id) values('Colina', 115);
insert into city(name, country_id) values('Dabajuro', 115);
insert into city(name, country_id) values('Democracia', 115);
insert into city(name, country_id) values('Falcon', 115);
insert into city(name, country_id) values('Federacion', 115);
insert into city(name, country_id) values('Jacura', 115);
insert into city(name, country_id) values('Los Taques', 115);
insert into city(name, country_id) values('Mauroa', 115);
insert into city(name, country_id) values('Romulo Gallegos', 115);
insert into city(name, country_id) values('Monse&#209', 115);
insert into city(name, country_id) values('Palmasola', 115);
insert into city(name, country_id) values('Petit', 115);
insert into city(name, country_id) values('Piritu', 115);
insert into city(name, country_id) values('San Francisco', 115);
insert into city(name, country_id) values('Silva', 115);
insert into city(name, country_id) values('Sucre', 115);
insert into city(name, country_id) values('Tocopero', 115);
insert into city(name, country_id) values('Union', 115);
insert into city(name, country_id) values('Urumaco', 115);
insert into city(name, country_id) values('Zamora', 115);
insert into city(name, country_id) values('Camaguan', 115);
insert into city(name, country_id) values('Chaguaramas', 115);
insert into city(name, country_id) values('El Socorro', 115);
insert into city(name, country_id) values('San Geronimo De Guayabal', 115);
insert into city(name, country_id) values('Federacion', 115);
insert into city(name, country_id) values('Las Mercedes', 115);
insert into city(name, country_id) values('Julian Mellado', 115);
insert into city(name, country_id) values('Francisco De Miranda', 115);
insert into city(name, country_id) values('Jose Tadeo Monagas', 115);
insert into city(name, country_id) values('Ortiz', 115);
insert into city(name, country_id) values('Jose Felix Ribas', 115);
insert into city(name, country_id) values('Juan German Roscio', 115);
insert into city(name, country_id) values('San Jose De Guaribe', 115);
insert into city(name, country_id) values('Santa Maria De Ipire', 115);
insert into city(name, country_id) values('Pedro Zaraza', 115);
insert into city(name, country_id) values('Andres Eloy Blanco', 115);
insert into city(name, country_id) values('Crespo', 115);
insert into city(name, country_id) values('Iribarren', 115);
insert into city(name, country_id) values('Jimenez', 115);
insert into city(name, country_id) values('Moran', 115);
insert into city(name, country_id) values('Palavecino', 115);
insert into city(name, country_id) values('Simon Planas', 115);
insert into city(name, country_id) values('Torres', 115);
insert into city(name, country_id) values('Urdaneta', 115);
insert into city(name, country_id) values('Alberto Adriani', 115);
insert into city(name, country_id) values('Andres Bello', 115);
insert into city(name, country_id) values('Antonio Pinto Salinas', 115);
insert into city(name, country_id) values('Aricagua', 115);
insert into city(name, country_id) values('Arzobispo Chacon', 115);
insert into city(name, country_id) values('Campo Elias', 115);
insert into city(name, country_id) values('Jose Felix Ribas', 115);
insert into city(name, country_id) values('Cardenal Quintero', 115);
insert into city(name, country_id) values('Guaraque', 115);
insert into city(name, country_id) values('Julio Cesar Salas', 115);
insert into city(name, country_id) values('Justo Brice&#209', 115);
insert into city(name, country_id) values('Libertador', 115);
insert into city(name, country_id) values('Miranda', 115);
insert into city(name, country_id) values('Obispo Ramos De Lora', 115);
insert into city(name, country_id) values('Padre Noguera', 115);
insert into city(name, country_id) values('Pueblo Llano', 115);
insert into city(name, country_id) values('Rangel', 115);
insert into city(name, country_id) values('Rivas Davila', 115);
insert into city(name, country_id) values('Santos Marquina', 115);
insert into city(name, country_id) values('Sucre', 115);
insert into city(name, country_id) values('Tovar', 115);
insert into city(name, country_id) values('Tulio Febres Cordero', 115);
insert into city(name, country_id) values('Zea', 115);
insert into city(name, country_id) values('Acevedo', 115);
insert into city(name, country_id) values('Andres Bello', 115);
insert into city(name, country_id) values('Campo Elias', 115);
insert into city(name, country_id) values('Brion', 115);
insert into city(name, country_id) values('Buroz', 115);
insert into city(name, country_id) values('Carrizal', 115);
insert into city(name, country_id) values('Chacao', 115);
insert into city(name, country_id) values('Cristobal Rojas', 115);
insert into city(name, country_id) values('El Hatillo', 115);
insert into city(name, country_id) values('Guaicaipuro', 115);
insert into city(name, country_id) values('Independencia', 115);
insert into city(name, country_id) values('Tomas Lander', 115);
insert into city(name, country_id) values('Los Salias', 115);
insert into city(name, country_id) values('Paez', 115);
insert into city(name, country_id) values('Paz Castillo', 115);
insert into city(name, country_id) values('Pedro Gual', 115);
insert into city(name, country_id) values('Plaza', 115);
insert into city(name, country_id) values('Simon Bolivar', 115);
insert into city(name, country_id) values('Sucre', 115);
insert into city(name, country_id) values('Urdaneta', 115);
insert into city(name, country_id) values('Zamora', 115);
insert into city(name, country_id) values('Acosta', 115);
insert into city(name, country_id) values('Aguasay', 115);
insert into city(name, country_id) values('Brion', 115);
insert into city(name, country_id) values('Caripe', 115);
insert into city(name, country_id) values('Cede&#209', 115);
insert into city(name, country_id) values('Ezequiel Zamora', 115);
insert into city(name, country_id) values('Libertador', 115);
insert into city(name, country_id) values('Maturin', 115);
insert into city(name, country_id) values('Piar', 115);
insert into city(name, country_id) values('Punceres', 115);
insert into city(name, country_id) values('Santa Barbara', 115);
insert into city(name, country_id) values('Sotillo', 115);
insert into city(name, country_id) values('Uracoa', 115);
insert into city(name, country_id) values('Antolin Del Campo', 115);
insert into city(name, country_id) values('Arismendi', 115);
insert into city(name, country_id) values('Diaz', 115);
insert into city(name, country_id) values('Simon Bolivar', 115);
insert into city(name, country_id) values('Gomez', 115);
insert into city(name, country_id) values('Maneiro', 115);
insert into city(name, country_id) values('Marcano', 115);
insert into city(name, country_id) values('Mari&#209', 115);
insert into city(name, country_id) values('Peninsula De Macanao', 115);
insert into city(name, country_id) values('Tubores', 115);
insert into city(name, country_id) values('Villalba', 115);
insert into city(name, country_id) values('Agua Blanca', 115);
insert into city(name, country_id) values('Araure', 115);
insert into city(name, country_id) values('Esteller', 115);
insert into city(name, country_id) values('Guanare', 115);
insert into city(name, country_id) values('Guanarito', 115);
insert into city(name, country_id) values('Monse&#209', 115);
insert into city(name, country_id) values('Ospino', 115);
insert into city(name, country_id) values('Paez', 115);
insert into city(name, country_id) values('Papelon', 115);
insert into city(name, country_id) values('San Genaro De Boconoito', 115);
insert into city(name, country_id) values('San Rafael De Onoto', 115);
insert into city(name, country_id) values('Santa Rosalia', 115);
insert into city(name, country_id) values('Sucre', 115);
insert into city(name, country_id) values('Turen', 115);
insert into city(name, country_id) values('Andres Eloy Blanco', 115);
insert into city(name, country_id) values('Andres Mata', 115);
insert into city(name, country_id) values('Arismendi', 115);
insert into city(name, country_id) values('Peninsula De Macanao', 115);
insert into city(name, country_id) values('Bermudez', 115);
insert into city(name, country_id) values('Bolivar', 115);
insert into city(name, country_id) values('Cajigal', 115);
insert into city(name, country_id) values('Cruz Salmeron Acosta', 115);
insert into city(name, country_id) values('Libertador', 115);
insert into city(name, country_id) values('Mejia', 115);
insert into city(name, country_id) values('Montes', 115);
insert into city(name, country_id) values('Ribero', 115);
insert into city(name, country_id) values('Sucre', 115);
insert into city(name, country_id) values('Valdez', 115);
insert into city(name, country_id) values('Andres Bello', 115);
insert into city(name, country_id) values('Antonio Romulo Costa', 115);
insert into city(name, country_id) values('Ayacucho', 115);
insert into city(name, country_id) values('Bolivar', 115);
insert into city(name, country_id) values('Cardenas', 115);
insert into city(name, country_id) values('Cordoba', 115);
insert into city(name, country_id) values('Fernandez Feo', 115);
insert into city(name, country_id) values('Francisco De Miranda', 115);
insert into city(name, country_id) values('Garcia De Hevia', 115);
insert into city(name, country_id) values('Bermudez', 115);
insert into city(name, country_id) values('Independencia', 115);
insert into city(name, country_id) values('Jauregui', 115);
insert into city(name, country_id) values('Jose Maria Vargas', 115);
insert into city(name, country_id) values('Junin', 115);
insert into city(name, country_id) values('Libertad', 115);
insert into city(name, country_id) values('Libertador', 115);
insert into city(name, country_id) values('Lobatera', 115);
insert into city(name, country_id) values('Michelena', 115);
insert into city(name, country_id) values('Panamericano', 115);
insert into city(name, country_id) values('Pedro Maria Ure&#209', 115);
insert into city(name, country_id) values('Rafael Urdaneta', 115);
insert into city(name, country_id) values('Samuel Dario Maldonado', 115);
insert into city(name, country_id) values('San Cristobal', 115);
insert into city(name, country_id) values('Seboruco', 115);
insert into city(name, country_id) values('Simon Rodriguez', 115);
insert into city(name, country_id) values('Sucre', 115);
insert into city(name, country_id) values('Torbes', 115);
insert into city(name, country_id) values('Uribante', 115);
insert into city(name, country_id) values('Guasimo', 115);
insert into city(name, country_id) values('Andres Bello', 115);
insert into city(name, country_id) values('Bocono', 115);
insert into city(name, country_id) values('Bolivar', 115);
insert into city(name, country_id) values('Candelaria', 115);
insert into city(name, country_id) values('Carache', 115);
insert into city(name, country_id) values('Escuque', 115);
insert into city(name, country_id) values('Juan Vicente Campo Elias', 115);
insert into city(name, country_id) values('La Ceiba', 115);
insert into city(name, country_id) values('Miranda', 115);
insert into city(name, country_id) values('Monte Carmelo', 115);
insert into city(name, country_id) values('Motatan', 115);
insert into city(name, country_id) values('Pampan', 115);
insert into city(name, country_id) values('Pampanito', 115);
insert into city(name, country_id) values('Rafael Rangel', 115);
insert into city(name, country_id) values('San Rafael De Carvajal', 115);
insert into city(name, country_id) values('Sucre', 115);
insert into city(name, country_id) values('Trujillo', 115);
insert into city(name, country_id) values('Urdaneta', 115);
insert into city(name, country_id) values('Valera', 115);
insert into city(name, country_id) values('Andres Bello', 115);
insert into city(name, country_id) values('Bolivar', 115);
insert into city(name, country_id) values('Bruzual', 115);
insert into city(name, country_id) values('Cocorote', 115);
insert into city(name, country_id) values('Independencia', 115);
insert into city(name, country_id) values('Jose Antonio Paez', 115);
insert into city(name, country_id) values('La Trinidad', 115);
insert into city(name, country_id) values('Manuel Monge', 115);
insert into city(name, country_id) values('Nirgua', 115);
insert into city(name, country_id) values('San Felipe', 115);
insert into city(name, country_id) values('Sucre', 115);
insert into city(name, country_id) values('Pampan', 115);
insert into city(name, country_id) values('Veroes', 115);
insert into city(name, country_id) values('Almirante Padilla', 115);
insert into city(name, country_id) values('Baralt', 115);
insert into city(name, country_id) values('Cabimas', 115);
insert into city(name, country_id) values('Catatumbo', 115);
insert into city(name, country_id) values('Colon', 115);
insert into city(name, country_id) values('Francisco Javier Pulgar', 115);
insert into city(name, country_id) values('Jesus Enrique Lozada', 115);
insert into city(name, country_id) values('Jesus Maria Semprun', 115);
insert into city(name, country_id) values('La Ca&#209', 115);
insert into city(name, country_id) values('Lagunillas', 115);
insert into city(name, country_id) values('Machiques De Perija', 115);
insert into city(name, country_id) values('Mara', 115);
insert into city(name, country_id) values('Maracaibo', 115);
insert into city(name, country_id) values('Miranda', 115);
insert into city(name, country_id) values('Paez', 115);
insert into city(name, country_id) values('Rosario De Perija', 115);
insert into city(name, country_id) values('San Francisco', 115);
insert into city(name, country_id) values('Santa Rita', 115);
insert into city(name, country_id) values('Simon Bolivar', 115);
insert into city(name, country_id) values('Sucre', 115);
insert into city(name, country_id) values('Valmore Rodriguez', 115);
insert into city(name, country_id) values('Vargas', 115);
insert into city(name, country_id) values('Dependencias Federales', 115);
insert into city(name, country_id) values('Baruta', 115);
insert into city(name, country_id) values('Caracilo Parra Y Olmedo', 115);
insert into city(name, country_id) values('Leonardo Infante', 115);

##columnas para guadar token actual
ALTER TABLE `pagomundo`.`jhi_user` ADD COLUMN `current_token` VARCHAR(500) NULL AFTER `extra_url_parameter`;
ALTER TABLE `pagomundo`.`jhi_user` ADD COLUMN `token_validity` DATETIME NULL AFTER `current_token`;
