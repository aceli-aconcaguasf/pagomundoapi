-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: 10.104.5.173    Database: pagomundo
-- ------------------------------------------------------
-- Server version	5.7.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaction` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_created` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `status` varchar(255) NOT NULL,
  `status_reason` longtext,
  `amount_before_commission` decimal(21,2) DEFAULT NULL,
  `bank_commission` float DEFAULT NULL,
  `fx_commission` float DEFAULT NULL,
  `recharge_cost` float DEFAULT NULL,
  `amount_after_commission` decimal(21,2) DEFAULT NULL,
  `exchange_rate` float DEFAULT NULL,
  `amount_local_currency` decimal(21,2) DEFAULT NULL,
  `bank_reference` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `currency_id` bigint(20) DEFAULT NULL,
  `merchant_id` bigint(20) DEFAULT NULL,
  `payee_id` bigint(20) DEFAULT NULL,
  `admin_id` bigint(20) DEFAULT NULL,
  `bank_account_id` bigint(20) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `notes` longtext,
  `transaction_group_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_transaction_currency_id` (`currency_id`),
  KEY `fk_transaction_merchant_id` (`merchant_id`),
  KEY `fk_transaction_admin_id` (`admin_id`),
  KEY `fk_transaction_bank_account_id` (`bank_account_id`),
  KEY `fk_transaction_transaction_group_id_index_5` (`transaction_group_id`),
  KEY `fk_transaction_payee_id` (`payee_id`),
  CONSTRAINT `fk_transaction_admin_id` FOREIGN KEY (`admin_id`) REFERENCES `extended_user` (`id`),
  CONSTRAINT `fk_transaction_bank_account_id` FOREIGN KEY (`bank_account_id`) REFERENCES `bank_account` (`id`),
  CONSTRAINT `fk_transaction_currency_id` FOREIGN KEY (`currency_id`) REFERENCES `currency` (`id`),
  CONSTRAINT `fk_transaction_merchant_id` FOREIGN KEY (`merchant_id`) REFERENCES `extended_user` (`id`),
  CONSTRAINT `fk_transaction_payee_id` FOREIGN KEY (`payee_id`) REFERENCES `extended_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=210 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction`
--

LOCK TABLES `transaction` WRITE;
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-13 14:40:01
