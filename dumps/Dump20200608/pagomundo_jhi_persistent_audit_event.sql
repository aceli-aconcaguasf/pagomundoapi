-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: pagomundo
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `jhi_persistent_audit_event`
--

DROP TABLE IF EXISTS `jhi_persistent_audit_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jhi_persistent_audit_event` (
  `event_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `principal` varchar(50) NOT NULL,
  `event_date` timestamp NULL DEFAULT NULL,
  `event_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`event_id`),
  KEY `idx_persistent_audit_event` (`principal`,`event_date`)
) ENGINE=InnoDB AUTO_INCREMENT=4653 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_persistent_audit_event`
--

LOCK TABLES `jhi_persistent_audit_event` WRITE;
/*!40000 ALTER TABLE `jhi_persistent_audit_event` DISABLE KEYS */;
INSERT INTO `jhi_persistent_audit_event` VALUES (4637,'ag.ucaliri@gmail.com','2020-05-18 17:03:40','AUTHENTICATION_SUCCESS'),(4638,'ag.ucaliri@gmail.com','2020-05-18 17:04:11','AUTHENTICATION_SUCCESS'),(4639,'a.gucaliri@gmail.com','2020-05-18 17:04:35','AUTHENTICATION_SUCCESS'),(4640,'a.gucaliri@gmail.com','2020-05-18 17:08:59','AUTHENTICATION_SUCCESS'),(4641,'ag.ucaliri@gmail.com','2020-05-18 17:11:47','AUTHENTICATION_SUCCESS'),(4642,'a.gucaliri@gmail.com','2020-05-18 17:13:29','AUTHENTICATION_SUCCESS'),(4643,'a.gucaliri@gmail.com','2020-05-18 20:47:03','AUTHENTICATION_FAILURE'),(4644,'a.gucaliri@gmail.com','2020-05-18 20:47:12','AUTHENTICATION_SUCCESS'),(4645,'a.gucaliri@gmail.com','2020-05-27 15:32:33','AUTHENTICATION_SUCCESS'),(4646,'superadmin','2020-05-27 15:39:32','AUTHENTICATION_FAILURE'),(4647,'superadmin','2020-05-27 15:39:39','AUTHENTICATION_SUCCESS'),(4648,'superadmin','2020-05-27 15:40:04','AUTHENTICATION_SUCCESS'),(4649,'superadmin','2020-05-28 17:08:08','AUTHENTICATION_SUCCESS'),(4650,'superadmin','2020-06-02 21:01:49','AUTHENTICATION_SUCCESS'),(4651,'ag.ucaliri@gmail.com','2020-06-04 16:38:59','AUTHENTICATION_SUCCESS'),(4652,'a.gucaliri@gmail.com','2020-06-04 16:41:58','AUTHENTICATION_SUCCESS');
/*!40000 ALTER TABLE `jhi_persistent_audit_event` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-08 11:27:22
