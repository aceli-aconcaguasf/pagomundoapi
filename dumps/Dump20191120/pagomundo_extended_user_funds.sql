-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: 10.104.5.173    Database: pagomundo
-- ------------------------------------------------------
-- Server version	5.7.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `extended_user_funds`
--

DROP TABLE IF EXISTS `extended_user_funds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `extended_user_funds` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_created` datetime,
  `value` decimal(21,2) DEFAULT NULL,
  `balance_before` decimal(21,2) DEFAULT NULL,
  `balance_after` decimal(21,2) DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `extended_user_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_extended_user_funds_extended_user_id` (`extended_user_id`),
  KEY `fk_extended_user_funds_user_id` (`user_id`),
  CONSTRAINT `fk_extended_user_funds_extended_user_id` FOREIGN KEY (`extended_user_id`) REFERENCES `extended_user` (`id`),
  CONSTRAINT `fk_extended_user_funds_user_id` FOREIGN KEY (`user_id`) REFERENCES `jhi_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extended_user_funds`
--

LOCK TABLES `extended_user_funds` WRITE;
/*!40000 ALTER TABLE `extended_user_funds` DISABLE KEYS */;
INSERT INTO `extended_user_funds` VALUES (1,'2019-11-19 18:08:25',1500.00,0.00,1500.00,'Founded money. Admin: a.gucaliri@gmail.com',98,105),(2,'2019-11-19 18:08:31',-500.00,1500.00,1000.00,'Founded money. Admin: a.gucaliri@gmail.com',98,105),(3,'2019-11-19 18:24:13',50.00,0.00,50.00,'Founded money. Admin: a.gucaliri@gmail.com',223,105),(4,'2019-11-19 18:24:22',10000.00,50.00,10050.00,'Founded money. Admin: a.gucaliri@gmail.com',223,105),(5,'2019-11-19 18:35:05',1000.00,10050.00,11050.00,'Founded money. Admin: a.gucaliri@gmail.com',223,105),(6,'2019-11-19 19:50:48',5000.00,12350.00,17350.00,'Founded money. Admin: a.gucaliri@gmail.com',220,105);
/*!40000 ALTER TABLE `extended_user_funds` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-20 10:22:32
