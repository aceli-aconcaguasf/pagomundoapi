-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: 10.104.5.173    Database: pagomundo
-- ------------------------------------------------------
-- Server version	5.7.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `jhi_persistent_audit_evt_data`
--

DROP TABLE IF EXISTS `jhi_persistent_audit_evt_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jhi_persistent_audit_evt_data` (
  `event_id` bigint(20) NOT NULL,
  `name` varchar(150) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`event_id`,`name`),
  KEY `idx_persistent_audit_evt_data` (`event_id`),
  CONSTRAINT `fk_evt_pers_audit_evt_data` FOREIGN KEY (`event_id`) REFERENCES `jhi_persistent_audit_event` (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_persistent_audit_evt_data`
--

LOCK TABLES `jhi_persistent_audit_evt_data` WRITE;
/*!40000 ALTER TABLE `jhi_persistent_audit_evt_data` DISABLE KEYS */;
INSERT INTO `jhi_persistent_audit_evt_data` VALUES (599,'message','Bad credentials'),(599,'type','org.springframework.security.authentication.BadCredentialsException'),(601,'message','Bad credentials'),(601,'type','org.springframework.security.authentication.BadCredentialsException'),(604,'message','Bad credentials'),(604,'type','org.springframework.security.authentication.BadCredentialsException'),(605,'message','Bad credentials'),(605,'type','org.springframework.security.authentication.BadCredentialsException'),(606,'message','Bad credentials'),(606,'type','org.springframework.security.authentication.BadCredentialsException'),(607,'message','Bad credentials'),(607,'type','org.springframework.security.authentication.BadCredentialsException'),(608,'message','Bad credentials'),(608,'type','org.springframework.security.authentication.BadCredentialsException'),(619,'message','Bad credentials'),(619,'type','org.springframework.security.authentication.BadCredentialsException'),(644,'message','Credenciales erróneas'),(644,'type','org.springframework.security.authentication.BadCredentialsException'),(645,'message','Credenciales erróneas'),(645,'type','org.springframework.security.authentication.BadCredentialsException'),(651,'message','Credenciales erróneas'),(651,'type','org.springframework.security.authentication.BadCredentialsException'),(654,'message','Credenciales erróneas'),(654,'type','org.springframework.security.authentication.BadCredentialsException'),(655,'message','Credenciales erróneas'),(655,'type','org.springframework.security.authentication.BadCredentialsException'),(678,'message','Bad credentials'),(678,'type','org.springframework.security.authentication.BadCredentialsException'),(679,'message','Bad credentials'),(679,'type','org.springframework.security.authentication.BadCredentialsException'),(680,'message','Bad credentials'),(680,'type','org.springframework.security.authentication.BadCredentialsException'),(686,'message','Credenciales erróneas'),(686,'type','org.springframework.security.authentication.BadCredentialsException'),(689,'message','Credenciales erróneas'),(689,'type','org.springframework.security.authentication.BadCredentialsException'),(693,'message','Credenciales erróneas'),(693,'type','org.springframework.security.authentication.BadCredentialsException'),(720,'message','Bad credentials'),(720,'type','org.springframework.security.authentication.BadCredentialsException'),(752,'message','Credenciales erróneas'),(752,'type','org.springframework.security.authentication.BadCredentialsException'),(755,'message','Bad credentials'),(755,'type','org.springframework.security.authentication.BadCredentialsException');
/*!40000 ALTER TABLE `jhi_persistent_audit_evt_data` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-20 10:22:31
